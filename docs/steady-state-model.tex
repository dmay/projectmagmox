\documentclass[11pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage{geometry}                		% See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for rotated page geometry
\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps§ with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{amsmath}
%\usepackage{natbib}

%SetFonts

%SetFonts

\usepackage{subfigure}

\setlength{\parskip}{1em}
\setlength{\parindent}{2em}

\title{A Simple Model for Fluid Fluxes Emanating from Subducting Slab}
\author{Meng Tian}
%\date{}							% Activate to display a given date or no date

\begin{document}

\maketitle

\section{Model Derivation}
The governing equations for two-phase flow incorporating chemical reactions are (assuming constant fluid and solid densities):
\begin{equation} \label{eqn: equation1}
\frac{\partial \phi}{\partial t} + \nabla \cdot ( \boldsymbol{v_f}\phi )= \frac{\Gamma}{\rho_f}
\end{equation}
\begin{equation} \label{eqn: equation2}
-\frac{\partial \phi}{\partial t} + \nabla \cdot [ \boldsymbol{v_s}(1-\phi) ]= -\frac{\Gamma}{\rho_s}
\end{equation}
\begin{equation} \label{eqn: equation3}
\phi(\boldsymbol{v_f}-\boldsymbol{v_s}) = -\frac{k}{\mu}(\nabla P_f - \rho_f g)
\end{equation}
\begin{equation} \label{eqn: equation4}
-\nabla P_f + \nabla \cdot \eta (\nabla \boldsymbol{v_s} + (\nabla \boldsymbol{v_s})^T) + \nabla (\zeta - \frac{2}{3}\eta)\nabla \cdot \boldsymbol{v_s} + \phi\rho_fg +(1-\phi)\rho_s g = 0
\end{equation}
\begin{equation} \label{eqn: equation5}
\frac{\partial (\rho_f\phi c^i_f)}{\partial t} + \nabla \cdot (\rho_f \boldsymbol{v_f}\phi c^i_f) = {\Gamma}_i
\end{equation}
\begin{equation} \label{eqn: equation6}
\frac{\partial [\rho_s(1 - \phi) c^i_s]}{\partial t} + \nabla \cdot [\rho_s \boldsymbol{v_s}(1 - \phi) c^i_s] = -{\Gamma}_i
\end{equation}

Within the slab, assuming that compaction is negligible ($\nabla \cdot \boldsymbol{v_s} \approx 0$) and the porous reactive flow reaches a steady state, we have:
\begin{equation} \label{eqn: equation7}
\nabla \cdot ( \boldsymbol{v_f}\phi )= \frac{\Gamma}{\rho_f}
\end{equation}
\begin{equation} \label{eqn: equation8}
\begin{split}
\nabla \cdot [ \boldsymbol{v_s}(1-\phi) ] &=(1 - \phi)\nabla \cdot \boldsymbol{v_s} - \boldsymbol{v_s} \cdot \nabla \phi\\
&= - \boldsymbol{v_s} \cdot \nabla \phi = -\frac{\Gamma}{\rho_s}
\end{split}
\end{equation}

As this model aims at examining the path dependence of flux quantity and composition, flow paths will be set a priori, and thus momentum conservation equations are omitted in the following considerations. Chemical species conservation in the liquid and solid phases are:
\begin{equation} \label{eqn: equation9}
\nabla \cdot (\rho_f \boldsymbol{v_f}\phi c^i_f) = {\Gamma}_i
\end{equation}
\begin{equation} \label{eqn: equation10}
\nabla \cdot [\rho_s \boldsymbol{v_s}(1 - \phi) c^i_s] = -{\Gamma}_i
\end{equation}
Adding Equation~\eqref{eqn: equation9} and~\eqref{eqn: equation10}, we get:
\begin{equation} \label{eqn: equation11} 
\nabla \cdot [(\rho_f \boldsymbol{v_f}\phi c^i_f) + [\rho_s \boldsymbol{v_s}(1 - \phi) c^i_s]]= 0
\end{equation}
Given that $\nabla \cdot \boldsymbol{v_s} \approx 0$, Equation~\eqref{eqn: equation11} turns into:
\begin{equation} \label{eqn: equation12}
\nabla \cdot (\rho_f \boldsymbol{v_f}\phi c^i_f) + \boldsymbol{v_s} \cdot \nabla [\rho_s (1 - \phi) c^i_s]= 0
\end{equation}
Combining Equation~\eqref{eqn: equation7} and Equation~\eqref{eqn: equation8} to get $\boldsymbol{v_f}$ as a function of $\phi$ and $\boldsymbol{v_s}$:
\begin{equation} \label{eqn: equation13}
\rho_f \nabla \cdot ( \boldsymbol{v_f}\phi )= \rho_s \boldsymbol{v_s} \cdot \nabla \phi
\end{equation}
If flow direction is predefined, rearranging the Cartesian coordinates properly, $\boldsymbol{v_s}$ and $\boldsymbol{v_f}$ in 2D can be written as:
\begin{equation} \label{eqn: equation14}
\begin{split}
\boldsymbol{v_s} &= (v_s, 0) \\
\boldsymbol{v_f} &= v_f(\cos \theta, \sin \theta)
\end{split}
\end{equation}
where $x$ axis aligns with $\boldsymbol{v_s}$, and $\theta$ is the angle of $\boldsymbol{v_f}$ relative to the $x$ axis. With $v_s$ and $\theta$ prescribed, only $v_f$ is unknown for the velocity field, and Equation~\eqref{eqn: equation12} and~\eqref{eqn: equation13} become:
\begin{equation} \label{eqn: equation15}
\rho_f \cos \theta \frac{\partial (v_f \phi c^i_f)}{\partial x} + \rho_f \sin \theta \frac{\partial (v_f \phi c^i_f)}{\partial z}  +  \rho_s v_s \frac{\partial}{\partial x} \left[ (1 - \phi) c^i_s \right] = 0
\end{equation}
\begin{equation} \label{eqn: equation16}
\rho_f\left[ \cos \theta \frac{\partial (v_f \phi)}{\partial x} + \sin \theta \frac{\partial (v_f \phi)}{\partial z} \right] - \rho_s v_s \frac{\partial \phi}{\partial x} = 0
\end{equation}
For $i = H_2O$ and $CO_2$, Equation~\eqref{eqn: equation15} represents two equations, and a numerical procedure for solution can be sketched as:
\begin{equation*}
\begin{split}
\text{guessed $\phi$} &\rightarrow \text{solve for $v_f$ from Eq.~\eqref{eqn: equation16}} \\
                     &\rightarrow \text{known $v_s$, thermodynamics of relationship between $c^i_f$ and $c^i_s$} \\
                     &\rightarrow \text{solve for $c^i_f$ and $c^i_s$ from Eq.~\eqref{eqn: equation15}} \\
                     &\rightarrow \text{update $\phi$ using the thermodynamic module} \\
                     &\rightarrow \text{iteration}
\end{split}
\end{equation*}
However, such a steady-state solution is numerically not easily tractable, as $\overline{c^i}$ depends implicitly on $\phi$ and $c^i_f/c^i_s$ and direct solve might suffer from difficult convergence. Therefore, in solving Equation~\eqref{eqn: equation15} and~\eqref{eqn: equation16}, we keep the time dependence of bulk composition by adding Equation~\eqref{eqn: equation5} and~\eqref{eqn: equation6}:
\begin{equation}\label{eqn: equation17}
\frac{\partial [\rho_f \phi c^i_f + \rho_s(1 - \phi) c^i_s]}{\partial t} + \rho_f \nabla \cdot [\phi \boldsymbol{v_f} c^i_f] + \rho_s \boldsymbol{v_s} \cdot \nabla [(1 - \phi) c^i_s]= 0
\end{equation}
where $\overline{c^i} = \rho_f \phi c^i_f + \rho_s (1 - \phi) c^i_s$ is the \emph{volumetric} bulk composition. Note that the corresponding compositional variables used for thermodynamic module are \emph{weight} bulk composition, defined as:
\begin{equation} \label{eqn: equation18}
\overline{c^i_{th}} = \overline{c^i}/[\rho_f \phi + \rho_s (1 - \phi)]
\end{equation}
The retainment of this time dependence allows usage of Enthalpy Method to solve the governing equations until a steady state is achieved.

\section{Governing Equations}
Applying Equation \eqref{eqn: equation14} to Equation \eqref{eqn: equation17}, the governing equations for our simplified model are:
\begin{equation} \label{eqn: equation19}
\rho_f\left[ \cos \theta \frac{\partial (v_f \phi)}{\partial x} + \sin \theta \frac{\partial (v_f \phi)}{\partial z} \right] - \rho_s v_s \frac{\partial \phi}{\partial x} = 0
\end{equation}
\begin{equation} \label{eqn: equation20}
\frac{\partial \overline{c^i}}{\partial t} + \rho_f \cos \theta \frac{\partial (v_f \phi c^i_f)}{\partial x} + \rho_f \sin \theta \frac{\partial (v_f \phi c^i_f)}{\partial z}  +  \rho_s v_s \frac{\partial}{\partial x} \left[ (1 - \phi) c^i_s \right] = 0
\end{equation}
\begin{equation} \label{eqn: equation21}
\overline{c^i_{th}} = \frac{\overline{c^i}}{\rho_f \phi + \rho_s (1 - \phi)}
\end{equation}
\begin{equation} \label{eqn: equation22}
(\phi, c^{H_2O}_s, c^{H_2O}_f, c^{CO_2}_s, c^{CO_2}_f) = \Phi(P, T, \overline{c^{H_2O}_{th}}, \overline{c^{CO_2}_{th}})
\end{equation}
They are solved for a steady-state solution.

\section{Nondimensionalization}
As $\phi$ and $c$ (concentration of species) are between $0$ and $1$, they are not scaled. We scale lengths with slab thickness $L$, speeds with the solid subducting speed $v_s$, time with $L/v_s$. Introduce the density ratio $\rho_r=\rho_s/\rho_f$. Then the non-dimensional equations (where all the relevant variables are understood to be non-dimensional) are as follows:
\begin{equation} \label{eqn: equation23}
\cos \theta \frac{\partial (v_f \phi)}{\partial x} + \sin \theta \frac{\partial (v_f \phi)}{\partial z}  - \rho_r v_s \frac{\partial \phi}{\partial x} = 0
\end{equation}
\begin{equation} \label{eqn: equation24}
\frac{\partial \overline{c^i}}{\partial t} +  \cos \theta \frac{\partial (v_f \phi c^i_f)}{\partial x} + \sin \theta \frac{\partial (v_f \phi c^i_f)}{\partial z}  +   \rho_r v_s \frac{\partial}{\partial x} \left[ (1 - \phi) c^i_s \right] = 0
\end{equation}
where $\overline{c^i} = \phi c^i_f + \rho_r (1 - \phi) c^i_s$, and $\overline{c_{th}^i} = \rho_f \overline{c^i} /[\rho_f \phi + \rho_s (1 - \phi)]$
Note that we don't scale the variables used in the thermodynamic module because the module is supposed to be used as a blackbox with dimensional input.

\section{Discretization}
Given that $v_f$ is a scalar in this case (Equation \eqref{eqn: equation23}), we define it at the cell center of the grid, together with $\phi$, $c^i_f$,  $c^i_s$, and $\overline{c^i}$ (fig.1).
\begin{figure}[h!]
   \centering
   \includegraphics[scale = 0.7]{fig1CenteredGrid.pdf} % requires the graphicx package
   \caption{Sketches on discretization scheme}
   \label{fig:figure1}
\end{figure}
For a star stencil, Equation \eqref{eqn: equation23} is discretized as
\begin{equation} \label{eqn: equation25}
\begin{split}
\frac{\partial (v_f \phi)}{\partial x} = &[0.5*(v_f(j,i+1) + v_f(j,i))*0.5*(\phi(j,i+1) + \phi(j,i)) \\
                                                       &- 0.5*(v_f(j,i-1) + v_f(j,i))*0.5*(\phi(j,i-1) + \phi(j,i))]/h \\
\end{split}
\end{equation}
where the coefficient of $v_f(j,i)$ is $0.25*[\phi(j,i+1) - \phi(j,i-1)]$. The term $\partial (v_f \phi)/ \partial z$ can be similarly discretized, yielding an additional coefficient of $v_f(j,i)$ is $0.25*[\phi(j+1,i) - \phi(j-1,i)]$. In the case of smoothly-varying porosity in the slab, this coefficient can be very small, or even $0$ before devolatilization, rendering the diagonal entries in the matrix of linear system (almost) vanishing. This is why I believe some iterative solvers don't work, whereas a direct solver still works. 

To avoid this difficulty, an upwind scheme is used for discretization (fig.1b and 1c), where the coefficient of $v_f(j,i)$ is $\phi(j,i)$, i.e.,
\begin{equation} \label{eqn: equation26}
\frac{\partial (v_f \phi)}{\partial x} = [v_f(j,i) \phi(j,i) - v_f(j,i-1) \phi(j,i-1)]/h \quad \text{for $\cos \theta >0$}
\end{equation}

\begin{equation} \label{eqn: equation27}
\frac{\partial (v_f \phi)}{\partial x} = [v_f(j,i+1) \phi(j,i+1) - v_f(j,i) \phi(j,i)]/h \quad \text{for $\cos \theta <0$}
\end{equation}

\section{Adding time-dependence of $\phi$ using Enthalpy Method}
Time dependence is retained in Eq. \eqref{eqn: equation20}, and given that the analogous enthalpy method can be used to calculate $\frac{\partial \phi}{\partial t}$, we can add time-dependence back to Eq. \eqref{eqn: equation19}. Multiplying Eq. \eqref{eqn: equation1} and \eqref{eqn: equation2} with $\rho_f$ and $\rho_s$ respectively, and adding them up, we get:
\begin{equation} \label{eqn: equation28}
(\rho_f - \rho_s) \frac{\partial \phi}{\partial t} + \rho_f \nabla \cdot ( \boldsymbol{v_f}\phi ) +  \rho_s \nabla \cdot ( \boldsymbol{v_s}(1 - \phi) ) = 0
\end{equation}
Taking into account $\nabla \cdot \boldsymbol{v_s} \approx 0$, Eq. \eqref{eqn: equation28} becomes:
\begin{equation} \label{eqn: equation29}
(\rho_f - \rho_s) \frac{\partial \phi}{\partial t} + \rho_f \nabla \cdot ( \boldsymbol{v_f}\phi ) -  \rho_s \boldsymbol{v_s} \cdot \nabla \phi = 0
\end{equation}
In the slab-parallel coordinates, Eq. \eqref{eqn: equation29} is:
\begin{equation} \label{eqn: equation30}
\rho_f\left[ \cos \theta \frac{\partial (v_f \phi)}{\partial x} + \sin \theta \frac{\partial (v_f \phi)}{\partial z} \right] - \rho_s v_s \frac{\partial \phi}{\partial x} - \Delta \rho \frac{\partial \phi}{\partial t}= 0
\end{equation}
Therefore, the full time-dependence is switched on by replacing Eq. \eqref{eqn: equation19} with Eq. \eqref{eqn: equation30}.

%\bibliography{}
%\bibliographystyle{}

\end{document}  






























