# PETGS.PDE Helmholtz


## PDE "Helmholtz" 
Solves the PDE  

``
-\div( k(x) \grad(u) ) + \alpha u =  g(x)
``  

for the scalar unknown `u`.


The weak form of the above is given by  

~~~tex
\int k(x) \grad(v) . \grad(u) dV 
  + \int \alpha v u dV + \int v \vec q . \vec n dS 
  = \int v g(x) dV,
~~~

where `v` is the test function (defined in H_1), `\vec n` the outward pointing unit normal and  `\vec q = - k(x) grad(u)` is the flux.

The coefficients in the PDE `k, g, \alpha, \vec q` are all  represented by the [Coefficients](coefficient.md). 

In principal, the coefficients `k, g, \alpha, \vec q` may be non-linear functions of `u`. The user must explicitly set values (or methods) for the coefficients - otherwise a run-time error will occur.


## Defaults  
* `k ` and `\alpha = 1` are intialized to 1. Both coefficients object use the storage type `DOMAINWISE`.
* The intial value for the right hand side, `g` is 0. The Coefficient for `g` uses the storage type `DOMAINWISE`.
* The intial value for the flux is `\vec q = \vec 0`. The Coefficient for `\vec q` uses the storage type `DOMAINWISE`.


## Options  

The user may request to introduce the advective terms `\vec w . grad(u)` or `\div(\vec w u)` where `\vec w` is a velocity vector. In this case we define the following PDE:  

~~~tex
-\div( k(x) \grad(u) ) + \alpha u 
  + a \vec w . grad(u) + b div(\vec w) u
  =  g(x)
~~~
where `a,b \in {0,1}` and their values are defined based on whether `\vec w . grad(u)` is added (e.g. `a=1`, `b=0`) or `\div(\vec w u)` is added (e.g. `a=1`, `b=1`).

## Notes  

1. The velocity vector `(\vec w)` is not represented as a Coefficient, rather it is represented as a PETSc Vec defined on a DM.
2. The advection-diffusion option might want to be moved into a child object called AdvectionDiffusion as it really isn't Helmholtz anymore.


## Examples  

#### 1. Solve the Laplace equation

The PDE is defined as  

``
-\nabla^2 \phi = 0.
``  

Now we set `u = \phi`; `\alpha = 0`; `k = 1`; `g = 0`.

#### 2. Solve the time-dependent diffusion equation with backward-Euler time integration

The PDE is given by  

``
\frac{\partial T}{\partial t} = \div( \kappa \grad(T) ) + f(t).
``  

We then discretize in time to yield

``
\frac{T^{k+1} - T^k}{\Delta t} = \div( \kappa \grad(T) )^{k+1} + f(t),
``  

and with a little re-arranging obtain

``
T^{k+1} - \Delta t \div( \kappa \grad(T) )^{k+1} =  T^k + \Delta t f(t).
``  

Then make the following choices for our coefficients

~~~tex
u = T; 
\alpha = 1;
k = \kappa \Delta t;
g(x) = T^k + \Delta t f(t^{k+1})  
~~~

#### 3. Solve steady-state diffusion with non-zero Neumann BC

~~~tex
-\div( \grad(u) ) =  g(x), subject to the BCS
  u = u0 on \Omega_D and
  \vec q = \vec q0 on \Omega_N and  
~~~

See the test `src/tests/test_pde_helmholtz_neumann.c` for a demonstration. This problem is solved in the domain `[-1,1]^2` and the solution is defined by the method of manufactured solutions. The chosen solution is  
```
u(x,y) = sin(\pi x) cos(\pi y)
```  
Dirichlet boundary conditions are imposed on the left, right and bottom boundaries. The flux `q` is imposed along the upper surface. The imposed value for `q` is defined by computing the gradient of the chosen solution and multiplying by -1 (as `k = 1`).

When running this example, the following command line options are important:

~~~tex
-mxy (number of elements in x and y)
-pk  (polynomial degree of function space)
~~~

Upon completion the mesh resolution `h` will be reported, along with the `L_2` and `H_1` errors of the discrete solution.