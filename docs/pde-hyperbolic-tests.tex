\documentclass[aps,pre,reprint,groupedaddress,longbibliography,floatfix,,raggedbottom]{revtex4-1}

%\setcitestyle{authoryear}%Setting citation style. Changing braces to ()

\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{amssymb}
\usepackage{amsmath}
\DeclareMathOperator{\arccot}{arccot}
\usepackage{psfrag}
\usepackage{pstool,url}


\begin{document}


%Title of paper
\title{Tests for hyperbolic Conservative Semi-Lagrangian (CSL) scheme }

\author{David  \surname{Rees Jones}} 
\author{Dave  \surname{May}} 

\date{\today}

%\begin{abstract}
%\end{abstract}

\maketitle
\section{Summary} 
The purpose of this document is to (i) develop a series of tests for the CSL scheme of increasing complexity, (ii) develop a test for the accuracy of the melting rate reconstruction, (iii) present results regarding the order of accuracy of the scheme.

\section{Test problems}
\subsection{Analytical solutions to a class of hyperbolic problems}
Consider the one-dimensional hyperbolic equation for $\phi(x,t)$:
\begin{equation}  \label{eq:hyperbolic_1}
\frac{\partial \phi}{\partial t} + \frac{\partial }{\partial x} \left(f \phi \right) = g,
\end{equation}
where $f(x,t)$ is the advective velocity and $g(x,t)$ is a source term. 
Initial conditions are given by $\phi = \phi_0(x)$.
We suppose the equation is specified on the entire real axis $x \in \mathbb{R}$ and forego discussion of boundary conditions.

We first consider the special case that $f=f(x)$ only and $g=0$. Then
\begin{equation} \label{eq:hyperbolic_2}
\frac{\partial \phi}{\partial t} + f \frac{\partial \phi}{\partial x}  = -f' \phi,
\end{equation}
where $f'=df/dx$. 
If the velocity field were divergence-free ($f'=0$), the equation could be solved by standard Lagrangian methods. 
Thus we focus on cases in which $f' \neq 0$.

We solve equation~\eqref{eq:hyperbolic_2} by introducing the change of variables
\begin{equation*} 
(x,t) \mapsto (r,s),
\end{equation*}
where the transformation is defined as follows. 
First let
\begin{align}
&\frac{d t}{d s} = 1, \label{eq:char1} \\
&\frac{d x}{d s} = f, \label{eq:char2}
\end{align}
such that $x=r$ and $t=0$ when $s=0$. 
The first of these equations is always solved by $s = t$.
The second is solved for $r$, which satisfies the characteristic equation
\begin{equation} \label{eq:characteristic}
\frac{d t}{d s} \frac{\partial r}{\partial t} + \frac{d x}{d s} \frac{\partial r}{\partial x} = 0.
\end{equation}
With this transformation, equation~\eqref{eq:hyperbolic_2} becomes
\begin{equation} \label{eq:hyperbolic_3}
\frac{\partial \phi}{\partial s}  = -f' \phi =  - \frac{\partial f}{\partial s} \frac{\phi}{f},
\end{equation}
where the second equality follows from
\begin{equation}
f'=\frac{d f}{d x}=\frac{\partial f}{\partial s} \frac{d s}{d x} = \frac{\partial f}{\partial s} \frac{1}{f}.
\end{equation}
Then equation~\eqref{eq:hyperbolic_3}  has general solution
\begin{equation} \label{eq:hyperbolic_4}
\phi  = c_1 / f,
\end{equation}
where $c_1(r)$ is determined by the initial conditions. 
In particular:
\begin{equation} \label{eq:hyperbolic_solution}
\phi(r,s)  = \frac{ \phi_0(r) f(r,s=0) }{f(r,s)}.
\end{equation}
The general method is best illustrated by some specific examples that provide test cases for the CSL scheme.

\subsection{Test 1: constant, non-zero divergence}
In the case $f(x) = x$, the velocity has a constant, non-zero divergence. 
Then equation~\eqref{eq:char2} becomes
\begin{equation}
\frac{d x}{d s} = x \Rightarrow x = c_2 e^s,
\end{equation}
where $c_2$ is a constant determined by the initial condition $x=r$ when $s=0$. 
Thus $c_2 = r$ and hence (recalling that $s=t$)
\begin{equation}
r = x e^{-t}.
\end{equation}
The general solution
\begin{equation} \label{eq:hyperbolic_4A}
\phi  = \frac{ c_1 }{  f } =  \frac{ c_1 }{  r e^s },
\end{equation}
and $c_1  = r \phi_0(r)$. 
So we obtain the solution
\begin{equation} \label{eq:hyperbolic_solutionA}
\phi(r,s)  = \frac{ \phi_0(r) }{e^s},
\end{equation}
where $r=x e^t$, $s=t$.

\textbf{Test problem:}  We take an initial condition that decays rapidly in the far-field.
This allows us to apply homogenous Dirichlet boundary conditions at \mbox{$x = \pm L$}, with $L$ taken sufficiently large to ensure that the error in the boundary conditions is exponentially small. 
For example, take $\phi_0 (r) = e^{- 100 r^2}$, $L=10$ and $t \leq 1$. 

\subsection{Test 2: non-constant divergence}
A more challenging test is provided by $f(x) = \sin x$, since the divergence is not a simple polynomial function for which sufficiently high order interpolation operators would be exact. 
In this case equation~\eqref{eq:char2} becomes
\begin{equation}
\frac{d x}{d s} = \sin x \Rightarrow x = 2 \cot^{-1} \left(e^{c_2 - s} \right).
\end{equation}
Application of the initial condition gives
\begin{equation}
c_2 = \ln \left[ \cot (r/2) \right].
\end{equation}
We can rearrange to obtain an explicit expression for $r$ as follows
\begin{equation}
r = 2 \cot^{-1} \left(e^t \cot(x/2) \right).
\end{equation}
The general solution is
\begin{equation} \label{eq:hyperbolic_4B}
\phi  = \frac{ c_1 }{  f } =  \frac{ c_1 }{ \sin \left[2 \cot^{-1} \left(e^{-s} \cot(r/2) \right) \right]},
\end{equation}
and the initial condition gives $c_1  = \phi_0(r) \sin r $. 
So we obtain the solution
\begin{equation}
\phi(r,s)  = \frac{ \phi_0(r) \sin r  }{ \sin \left[2 \cot^{-1} \left(e^{-s} \cot(r/2) \right) \right]},
\end{equation}
or, more compactly,
\begin{equation} \label{eq:hyperbolic_solutionB}
\phi(x,t)  = \frac{ \phi_0(r) \sin r  }{ \sin x},
\end{equation}
where $r = 2 \cot^{-1} \left(e^t \cot(x/2) \right)$.

\textbf{Test problem:} A convenient test is to solve on the domain $x \in [-\pi,\pi]$ subject to the homogenous Dirichlet boundary conditions using the initial condition
\begin{equation} \label{eq:initialB}
\phi_0(x) = e^{-x^2}  \sin^2 x.
\end{equation}
This example satisfies the boundary conditions exactly for all time.
To see this note that $r \rightarrow \pm \pi$ as $x \rightarrow \pm \pi$ for all $t$.

\section{Test reconstruction of melt rate}
As well as solving the hyperbolic equations for the evolution of the composition, we also need to reconstruct the melting rate as an input for the energy conservation equation. 
This is currently cast as the difference between the porosity integrated over the arrival cells minus departure cells 
(ADD REFERENCE TO DOC OR REPEAT EQUATION). 
We here test our ability to reconstruct the melting rate by using an exact solution to equation~\eqref{eq:hyperbolic_1} with non-zero source term (which plays the role of the melting rate). 

The general procedure is to apply the same manipulations to equation~\eqref{eq:hyperbolic_1} as before with $f(x,t)=f(x)$, and $g(x,t)=g(t)$ now non-zero. We obtain 
\begin{equation} \label{eq:hyperbolic_g}
\frac{\partial ( \phi f)}{\partial s}  = g f,
\end{equation}
which simplifies to equation~\eqref{eq:hyperbolic_3} when $g=0$. Hence the general solution is given by
\begin{equation} \label{eq:hyperbolic_4}
\phi  = \frac{c_1(r)}{ f } + \frac{1}{f} \int_0^s f(s) g(s) \, ds.
\end{equation}

\textbf{Test problem 1:} Consider the case $f(x,t) = x$, $g(x,t) = 2 \sin t$. Then the general solution is given by 
\begin{equation} \label{eq:hyperbolic_solutionA_source}
\phi(x,t)  = e^{-t} \left[ { 1 + \phi_0(r) }\right] + \sin t - \cos t,
\end{equation}
where $r=x e^t$. 
I suggest substituting this exact solution and checking the accuracy of the discrete approximation to the source term (relative to the exact source term $g$). We should do this analysis away from boundaries or impose the exact solution as an inhomogenous boundary condition.

\textbf{Test problem 2:} 
(WARNING: ALGEBRA NEEDS CHECKING) 
Consider the case $f(x,t) = \sin x$, $g(x,t) = e^t$. 
Then the general solution is given by 
\begin{equation} \label{eq:hyperbolic_solutionB}
\phi(x,t)  = \frac{ \phi_0(r) \sin r  }{ \sin x} + \frac{\left(C^2 e^t + e^{-t} \right)}{2C^2} \left[ \ln \left(C^2 e^{2s} + 1 \right) \right]^t_0,
\end{equation}
where $r = 2 \cot^{-1} \left(e^t \cot(x/2) \right)$ and $C=e^{-t} \tan(x/2)$.

\section{Results}

\bibliographystyle{apsrev4-1}
\bibliography{../References/subduction}

\end{document}
