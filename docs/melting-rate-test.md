Title         : A benchmark for the melting rate calculation
Author        : Richard Katz
Author        : David Rees Jones


[TITLE]

# Overview 

We use a two-component thermodynamic model of melting to benchmark the melting rate calculation.

In the **first test**, we construct a pseudo-1D column model (i.e., a two-dimensional domain but with variation
in one dimension only.) The equations are the usual ones for the
mechanics coupled to an energy equation and a compositional equation.

Numerically, we solve the equations using an enthalpy-method-like
approach, with temperature as the energy variable. We use Dave's method
for computing the melting rate --- this is the output being tested.

Analytically, we can compute an approximate melting rate for comparison.
We hope that the approximation is a good one so that the approximation error is 
relatively small and we can demonstrate convergence.

The domain has a height $H$ and a vertical coordinate $z$ such that
$-H\le z\le 0$. 
We specify the temperature $T$ and composition $c_s$ at the bottom of the
domain ($z=-H$) such that it is on the solidus but the porosity is zero.
We also specify the solid velocity on the boundary: zero tangential
component and an inflow speed on the boundary of $w^s_0$.

In the **second test**, we construct a time-dependent problem with no spatial structure (zero-dimensional)
This allows us to test potential issues 
arising from temporal lagging in the solution of the energy equation. 

The key ingredient of the second test is to add a source term to the equation governing bulk species conservation 
in order to generate time-dependent behaviour. 
We choose a simple oscillatory form so that we can do the resulting integrations by hand.

# Test 1: Steady 1D column model

## Governing equations

The equations governing the mechanics are the usual ones (eqns. (3.40) in
my book) that use the extended Boussinesq approximation. For the
temperature we have the conservation of energy equation
~ Equation { #eq-energy }
\frac{\partial T}{\partial t} + \overline{\mathbf{v}}\cdot\nabla T = 
- L\Gamma/(\rho c_e)
~
where we have neglected thermal diffusion and the adiabatic gradient.
Conservation of bulk composition reads
~ Equation { #eq-composition }
\frac{\partial \overline{c}}{\partial t} + \nabla\cdot\overline{c\mathbf{v}} = 0,
~
where we have neglected diffusion in the liquid phase.

## Constitutive laws / material properties

We take the shear viscosity to be constant $\eta_0$ and the bulk
viscosity to a constant multiple of the shear viscosity
$\zeta_0=r\eta_0$. The mobility is given in the usual way as
$K_\phi = K_0(\phi/\phi_0)^n$. Densities are assumed constant and equal
between phases except in buoyancy terms, where $\Delta\rho$ is a constant.

The petrological model is of a solidus temperature that is linear with
pressure and with composition and a liquidus that is a constant offset
from it.  Hence
~ Equation { #eq-solidus }
T^S = T^S_0 + P/\gamma - Mc^s
~
where $T^S$ is the solidus temperature, $P=-\rho g z$ is the lithostatic
pressure, $\gamma$ is the Clapeyron slope and $M$ is the compositional
slope. In the presence of partial melt, the liquid and solid compositions
are related by
~ Equation { #eq-liquidus }
c^l = c^s + \Delta c
~
where $\Delta c$ is a constant.

## Computing porosity etc in the numerical model

At the conditions $T,P,\overline{c}$ we need to compute the diagnostic
variables $\phi,c^s,c^l$. To do so we use the solidus and liquidus
equations and the definition of bulk concentration
~ Equation { #eq-bulkcomp }
\overline{c} = \phi c^l + (1-\phi)c^s.
~
Then we have the solution $c^s = (P/\gamma + T^S_0 - T)/M$,
$c^l = c^s + \Delta c$ and $\phi = (\overline{c}-c^s)/\Delta c$. 

For the melting rate we can use the conservation of mass equation and rearrange to give
~ Equation { #eq-meltingrate }
\Gamma/\rho = \frac{D_s\phi}{Dt} - (1-\phi)\nabla\cdot\mathbf{v}^s.
~
Then the energy equation becomes
~ Equation { #eq-energy2 }
\frac{\partial T}{\partial t} + \overline{\mathbf{v}}\cdot\nabla T = 
- \frac{L}{c_e}\left[\frac{D_s\phi}{Dt} - (1-\phi)\nabla\cdot\mathbf{v}^s\right].
~
It is the numerical solution of this equation that we seek to test. 

## Configuring the numerical model

Choose a domain depth $H = 60$ km. At the inflow boundary, $z = -H$,
impose an inflow composition $c^s(z=-H) = c^s_0 = 0.5$ and an inflow
velocity $\mathbf{v}(z=-H) = w^s_0\hat{\mathbf{z}} = 3$ cm/yr . To ensure
that the porosity is zero at the bottom boundary, choose
~ Equation { #eq-T-bottom }
T(z=-H) = T^S_0 + \rho g H /\gamma - Mc^s_0.
~
At the top boundary, an outflow condition on the pressure will be needed, I think.

## An analytical solution for the melting rate in 1D, steady state

At steady state we have
~ Equation { #eq-energy-ss }
\Gamma/\rho = - \frac{c_e}{L}\overline{w}\frac{dT}{dz}
~
~ Equation { #eq-comp-ss}
\phi c^l w^l + (1-\phi) c^s w^s = c^s_0 w^s_0
~
~ Equation { #eq-mass-ss}
\overline{w} = \phi w^l + (1-\phi) w^s = w^s_0
~
~ Equation { #eq-mass-l }
\frac{d}{dz}\phi w^l = \Gamma/\rho  
~
Combining these results allows us to solve for $\Gamma/\rho$ as
~ Equation { #eq-meltingrate-soln }
\frac{\Gamma}{\rho} = \frac{w_0^s\rho g/\gamma}{L/c_e + M\Delta c}
~

## A note on thermal diffusion
Note that the melting rate $\Gamma/\rho$ is a constant, as is $\overline{w}$, 
so the temperature gradient is also a constant (Ribe calls this the 'wet adiabat').
Thus the diffusion term in the energy equation $k d^2T/dz^2 = 0$. 

Therefore, the analytical solution remains valid even in the presence of thermal diffusion.

# Test 2: A time-dependent, zero-dimensional problem

## Conservation equations
We conserve mass, energy, and chemical species in zero-dimensions
~ Equation { #eq-0d-mass }
\frac{\partial \phi}{\partial t}  = \frac{\Gamma}{\rho},
~
~ Equation { #eq-0d-energy }
\frac{\partial T}{\partial t}  =    - \frac{L}{c_e}\frac{\Gamma}{\rho},
~
~ Equation { #eq-0d-comp }
\frac{\partial \overline{c}}{\partial t}  = \alpha \omega \cos \omega t,
~
where $\alpha$ is the amplitude and $\omega$ is the frequency of the oscillatory source term.

## Melting model

The petrological model is the same as in the 1D test of Katz **(without pressure dependence)**.

We assume a solidus composition $c^s$ that is linear with
temperature and a liquidus that is a constant offset from it.  Hence
~ Equation { #eq-0d-solidus }
T = T^S_0  - Mc^s
~
where $T$ is the temperature, $T^S_0 $ is a constant,  and $M>0$ is the compositional
slope. In the presence of partial melt, the liquid and solid compositions
are related by
~ Equation { #eq-0d-liquidus }
c^l = c^s + \Delta c
~
where $\Delta c>0$ is a constant.

## Computing porosity etc in the numerical model

At the conditions $T,\overline{c}$ we need to compute the diagnostic
variables $\phi,c^s,c^l$. To do so we use the solidus and liquidus
equations and the definition of bulk concentration
~ Equation { #eq-0d-bulkcomp }
\overline{c} = \phi c^l + (1-\phi)c^s.
~
Then we have the solution
~ Equation { #eq-0d-comp-model}
c^s = (T^S_0 -T )/M, \quad
c^l = c^s + \Delta c, \quad
\phi = (\overline{c}-c^s)/\Delta c.
~

For the melting rate we can use the conservation of mass equation [#eq-0d-mass] 
and substitute into the  energy equation [#eq-0d-energy] to obtain
~ Equation { #eq-0d-energy2 }
\frac{\partial T}{\partial t} =
- \frac{L}{c_e}\frac{\partial \phi}{\partial t}.
~

## Analytical solution

At steady state we have an analytical solution to [#eq-0d-comp]
~ Equation { #eq-0d-comp-sol}
\overline{c}  = \overline{c}_0 + \alpha  \sin \omega t,
~
where $\overline{c}_0$ is the initial bulk composition.
This solution could be imposed and equations [#eq-0d-mass] and [#eq-0d-energy] solved using Dave's scheme for $\Gamma/\rho$. 
The resulting solution $T$ and $\Gamma/\rho$$ should then be compared to the following analytical solutons.

We next substitute into [#eq-0d-comp-model] to obtain
~ Equation
\Delta c \frac{\partial \phi}{\partial t} = \frac{\partial \overline{c}}{\partial t}  - \frac{\partial {c_s}}{\partial t}
=  \alpha \omega \cos \omega t +\frac{1}{M}  \frac{\partial T}{\partial t}.
~
Then substitute in energy conservation [#eq-0d-energy] and rearrange to obtain
~ Equation { #eq-0d-mass-sol }
\left[ \Delta c +\frac{L}{M c_e} \right] \frac{\partial \phi}{\partial t} = \alpha \omega \cos \omega t
~
Then [#eq-0d-mass] gives the melting rate
~ Equation { #eq-0d-meltingrate-soln }
\frac{\Gamma}{\rho} =  \frac{\alpha \omega \cos \omega t }{\Delta c + L/(M c_e)}.
~
Given an initial temperature $T_0$, the corresponding initial porosity is
~ Equation { #eq-0d-initialphi }
\phi_0=\frac{1}{\Delta c}\left(\overline{c}_0 -\frac{T^S_0 - T_0}{M} \right).
~
We can then integrate [#eq-0d-mass] to obtain
~ Equation { #eq-0d-phi }
\phi-\phi_0 = \frac{\alpha  \sin \omega t }{\Delta c + L/(M c_e)}.
~
Finally from [#eq-0d-solidus]
~ Equation { #eq-0d-T }
T=T^S_0 -M c_s = T^S_0 -M \left(\overline{c}-\Delta c \phi \right).
~

## Parameter values and analytical solution calculation
Parameter values must be chose carefully to ensure quantities remain in physically appropriate ranges.

The following snippet of matlab code specifies possible parameter values. It then solves and plots the analytical solution.

```
Delta_c= 0.3;
c_init = 0.4;
L_on_c = 300; %K
M = 600; %K
TS_0 = 1500;
T_init = 1300;
alpha = 0.02;
omega = 0.5;
t=linspace(0,4*pi,1e3);

cb=c_init+alpha*sin(omega*t);
Gamma_on_rho = alpha*omega*cos(omega*t)/(Delta_c+L_on_c/M);
phi_init=(c_init-(TS_0-T_init)/M)/Delta_c;
phi=phi_init+alpha*sin(omega*t)/(Delta_c+L_on_c/M);
cs=cb-phi*Delta_c;
cl=cs+Delta_c;
T=TS_0-M*cs;

figure(1); subplot(4,1,1); plot(t,phi)
figure(1); subplot(4,1,2); plot(t,cb,t,cs,t,cl)
figure(1); subplot(4,1,3); plot(t,Gamma_on_rho)
figure(1); subplot(4,1,4); plot(t,T)
```



[reference manual]: http://research.microsoft.com/en-us/um/people/daan/madoko/doc/reference.html  "Madoko reference manual"
