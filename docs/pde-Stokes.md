# PETGS.PDE Stokes


## PDE "Stokes" 
Solves the PDEs  

~~~tex
\div(2 \eta E(u)) - \grad(p) = fu,
~~~  
~~~tex
\div(u) = fp,
~~~  

for the vector `u` (velocity) and the scalar `p` (pressure). Here `E()` denotes the strain-rate tensor,   

~~~tex
E(u) = 1/2 ( grad(u) + grad(u)^T ).
~~~


The weak forms of the above equations is given by the following 

~~~tex
\int 2 \eta E(v) : E(u) dV 
  - \int p div(v) dV
  + \int v  \sigma  n dS 
  = -\int v . fu dV
~~~
~~~tex
- \int q div(u) dV 
  = -\int q fp dV
~~~

where `v \in H_1`, `q \in L_2` are the test functions for velocity and pressure, `n` the outward pointing unit normal and  

~~~tex
\sigma = 2 \eta E(u) - p I
~~~
is the stress.

The coefficients in the PDE `\eta, fu, fp` are all  represented by the Coefficients object with the storage type `DOMAINWISE`.

## Defaults  

* `fu` and `fp` are initilized to zero. 
* `\eta` is initialized to 1. 



## Notes  

1. `\sigma` is implicitly assumed to be zero.

## Examples  

#### 1. Flow in a box
See `src/tests/test_stokes.c`.

#### 2. Buoynancy driven flow in an annulus
See `src/tests/test_stokes_ann.c`.

#### 3. Flow in a subduction wedge
See `src/tests/vankeken-benchmarks/vk-case1b.c`. Implements the model defined here 

~~~tex
@article{vanKeken2008187,
title = "A community benchmark for subduction zone modeling ",
journal = "Physics of the Earth and Planetary Interiors ",
volume = "171",
number = "1–4",
pages = "187 - 197",
year = "2008",
note = "Recent Advances in Computational Geodynamics: 
        Theory, Numerics and Applications ",
issn = "0031-9201",
doi = "http://dx.doi.org/10.1016/j.pepi.2008.04.015",
author = "Peter E. van Keken and Claire Currie and Scott D. King
          and Mark D. Behn and Amandine Cagnioncle 
          and Jiangheng He and Richard F. Katz 
          and Shu-Chuan Lin and E. Marc Parmentier 
          and Marc Spiegelman and Kelin Wang",
}

~~~