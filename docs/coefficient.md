# PETGS.Coefficient


## Overview  

An abstract representation of coefficients in a PDE. We primarily solve PDEs using finite elements, thus the design of the Coefficient class is orientated around that spatial discretization.

Coefficents encapusulate the storage and evaluation of coefficients appearing within the weak form of a FE formulation -- which as such will be evaluated at a finte set of quadrature points.

### Storage methods

Storage methods available to include `Cellwise` or `Domainwise`. For a quadrature rule defining N points, `Cellwise` storage will only store N properties. Given the same rule and mesh containing M elements, `Domainwise` storage will store M * N quadrature point values.

### Evaluation methods

1. Set as domainwise constant
2. Set using a field interpolated from the FE mesh which the Coefficient is associated with
3. Set via calling a user-defined function

The user-defined function have the following prototype for domain-wise evaluation

~~~C
PetscErrorCode user_eval_domain(Coefficient c,Quadrature q,PetscInt regions[])
~~~

and cell-wise evaluation  

~~~C
PetscErrorCode user_eval_cell(Coefficient c,Quadrature q,PetscInt cell,PetscInt region)
~~~

*To ensure quadrature points define up-to-date values, we require users to exclusivrly interact with the quadrature point values via the functions `CoefficientEvaluate()`, `CoefficientGetQPointData()` and `CoefficientGetCellQPointValues()`. More specifically, the ONLY place where users are permitted to actually modify the values defined at a quadrature point are using the provided setters (e.g. `SetDomainwiseConstant()`), or through the user-defined call back functions they wrote.*


## C API  

~~~C
struct QPointData {  
  char      *name;
  PetscReal *data;  
  PetscInt  len,ncomponents;  
  void      fp_user()
  void      *ctx;
};
~~~


**CoefficientCreate**  

~~~C
PetscErrorCode CoefficientCreate(Coefficient *c)
~~~
Create a new instance of a coefficient
 
**CoefficientGetQPointData**  

~~~C
PetscErrorCode CoefficientGetQPointData(Coefficient c,const char name[],QPointData *data)
~~~  

Allocate and fill a helper containiner used to manage quadrature points over each element. Must call `CoefficientRestoreQPointData()` to free memory.

* Input
	- c - the Coefficient object
	- name - string used to identify quadrature values  
* Output
	- data - the quadrature container


**CoefficientRestoreQPointData**  

~~~C
PetscErrorCode CoefficientRestoreQPointData(Coefficient c,const char name[],QPointData *data)
~~~

Deallocate the quadrature containiner used to manage quadrature points over an element.


###CoefficientGetCellQPointValues###

Returns an array of numbers defining the quadrature point  quantities on a particular element. These values are read only and cannot be modified by the user.   

~~~C
#include <coefficient.h>  
PetscErrorCode CoefficientGetCellQPointValues(QPointData data,PetscInt e,const PetscReal *qvals[])
~~~
**Arguments**

+ Input
  * data - the quadrature container
  * e - the index of the element we seek quadrature values for  
- Output
	* qvals - the array of values defined for the quadrature points

**Notes**

* a note
 
## Examples / tests  

**1. Solve Laplace equation**  
The PDE is defined as  
``
-\nabla^2 \phi = 0.
``  

Now we set `u = \phi`; `k = 1`; `g = 0`.

