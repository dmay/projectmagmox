# PETGS.PDE Darcy


## PDE "Darcy" 
Solves the PDEs  

~~~tex
\mu uD + grad(p) = fu,
~~~  
~~~tex
-\div(uD) = fp,
~~~  

for the vector `uD` (Darcy flux) and the scalar `p` (pressure). 

Two variational forms can be used. The `L2-H1` form is given by:

~~~tex
\int \mu v . uD dV 
  + \int v . grad(p) dV
  = \int v . fu dV
~~~
~~~tex
\int grad(q) uD dV 
  = \int q (uN . n) dS
    + \int q fp dV
~~~

where `v \in L_2`, `q \in H_1` are the test functions for velocity and pressure, `n` the outward pointing unit normal from the boundary.

The `Hdiv-L2` form is given by:

~~~tex
\int \mu v . uD dV 
  - \int div(v) . p dV
  = -\int (v . n) pN dS 
  + \int v . fu dV
~~~
~~~tex
-\int q div(uD) dV = \int q fp dV
~~~

where `v \in H(div)`, `q \in L_2` are the test functions for velocity and pressure, `n` the outward pointing unit normal from the boundary.


The coefficients in the PDE `\mu, fu, fp` are all  represented by the Coefficients object with the storage type `DOMAINWISE`. Depending on the form chosen, the coefficients `uN` (Neumann Darcy flux (L2-H1)) or `pN` (Neumann pressure (Hdiv-L2)) will be defined.

## Boundary conditions

The different forms support different Dirichlet and Neumann boundary conditions.

* **L2-H1:**
One may specify Dirchlet boundary conditions on the pressure (`p`) and Neumann conditions on the Darcy flux (`uN`).

* **Hdiv-L2:**
One may specify Dirchlet boundary conditions on the Darcy flux (`uD`) and Neumann conditions on the pressure (`pN`).


## Defaults  

* `\mu` is initialized to one. 
* `fu` is initialized to zero.
* `fp` is initialized to zero.
* `uN` (if required) is initialized to zero.
* `pN` (if required) is initialized to zero.



## Examples  

#### 1. Darcy flow in a box
* See `src/tests/test_pde_darcy.c`
* See `src/tests/test_pde_darcy_mms.c`