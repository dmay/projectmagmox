#PETGS

## Requirements ##

1. PETSc version 3.7
2. MPI (if you wish to run parallel jobs)
3. METIS (if you wish to run parallel jobs)
4. HDF5 (optional) 

### Installing PETSc
A brief overview of how to install PETSc on your local machine (desktop or laptop) is provided below

1. Obtain the PETSc v3.7 source from [here](https://www.mcs.anl.gov/petsc/download).
2. Unpack the .tar.gz file and move into the directory petsc-3.7.7
3. Set the environment variable `PETSC_DIR`, e.g. in bash
```bash
terminal:~ $ export PETSC_DIR=${PWD}
```
4. A basic sequential (non-parallel) configuration of PETSc suitable for OSX,  
```bash
terminal:~ $ ./configure --with-debugging=yes \
--with-mpi=0 \
--download-suitesparse=yes
```
Then follow the instructions provided at the end of the configuration stage to compile the PETSc library.
5. A basic build suitable for parallel computations is as follows,  
```bash
terminal:~ $ ./configure --with-debugging=yes \
--download-mpich=yes \
--download-suitesparse=yes \
--download-metis=yes \
--download-pastix=yes --download-ptscotch=yes
```
Then follow the instructions provided at the end of the configuration stage to compile the PETSc library.

#### Notes
* Steps 4. and 5. will define a debug build of PETSc. This will run 2x-3x slower than an optimized build. If you want an optimized PETSc build, change the flag to `--with-debugging=no`. 
* If you are using a Linux machine, you may also be required to specify the flag `--download-blaslapack=yes`. 
* If you wish to specify the specific compiler to use, you can use the flags `--with-cc=/path/to/your/c-compiler` and `--with-fc=/path/to/your/fortran-compiler`.
* More information about installing PETSc can be found [here](https://www.mcs.anl.gov/petsc/documentation/installation.html).

## Installation ##

1. Make sure the environment variables `PETSC_DIR`, `PETSC_ARCH` have been set. Then type `make all`. A default config file will be created for you called `makefile.arch` - edit this and insert any special compiler flag appropriate for your system / compiler.

2. Upon a successful build, a directory will be created in your root directory called `${PETSC_ARCH}`.
Underneath you'll find

    * `${PETSC_ARCH}/bin`

    * `${PETSC_ARCH}/obj`

    * `${PETSC_ARCH}/lib`

All tests and other executables will reside within `${PETSC_ARCH}/bin`



## Verify your build ##

The test suite should be executed to verify you build was successful. To do that, execute the following command

```#!bash
make testvalidation
```
The first time you try to run the test suite, you will be promoted to download a python package called `pyTestHarness`. Just follow the instructions. More information about using the test harness can be found [here](tests/README.md).

