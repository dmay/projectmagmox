# PETGS.PDE StokesDarcy2Field


## PDE "StkDcy2F" 
Solves the PDEs  

~~~tex
\div(2 \eta Edev(u)) + grad(\zeta* div(u)) - \grad(p) = fu,
~~~  
~~~tex
-\div(u) + \div(k* \grad(p) - k* e_3) = fp,
~~~  

for the vector `u` (velocity) and the scalar `p` (pressure). Here `Edev()` denotes the deviatoric strain-rate,   

~~~tex
Edev(u) = 1/2 ( grad(u) + grad(u)^T ) - 1/3 div(u) I.
~~~

These equations are commonly referred to as the McKenzie equations by the geodynamics community. In this context, the quantity `u` is to be interpretted as the solid-velocity and `p` represents the fluid (liquid) pressure.


The weak forms of the above equations is given by the following 

~~~tex
\int 2 \eta Edev(v) : Edev(u) dV 
  + \int div(v) \zeta* div(u) dV 
  - \int p div(v) dV
  + \int v . \sigma n dS 
  = -\int v . fu dV
~~~
~~~tex
-\int q div(u) dV 
  - \int k* grad(q) . grad(p) dV 
  + \int q q_N . n dS
  = -\int grad(q) . k* e_3 dV
    + \int q fp dV
~~~

where `v \in H_1`, `q \in H_1` are the test functions for velocity and pressure, `n` the outward pointing unit normal from the boundary,   

~~~tex
\sigma = 2 \eta Edev(u) + \zeta* div(u) I - p I
~~~
is the stress, and

~~~tex
q_N = -k* ( grad(p) + e_3 )
~~~
is the Darcy velocity.


The coefficients in the PDE `\eta, \zeta*, k*, fu, fp` are all  represented by the Coefficients object with the storage type `DOMAINWISE`. `e_3` uses the storage type `CELLWISE`.

## Defaults  

* `fu` is initialized to zero.
* `eta` is initialized to 1. 
* `zeta*` and `k*` are initialized to zero. 
* `e_3` is initialized to the vector (0,1).
* `fp` is created but its storage type is not assigned -- thereby it does not consume significant memory. (Note to self: this is just an idea - in reality I allocate it)
* `q_N` is initialized to zero.

With these choices, the default behvaiour of PDE.StkDcy2F is identical to PDE.Stokes.


## Notes  

1. This PDE "inherits" from the Stokes PDE. New objects are introduced to store the coefficients `zeta*`, `k*` and to represent the Diriclet boundary conditions on `p`.
2. `\sigma` is implicitly assumed to be zero.


## Examples  

#### 1. Flow in a subduction wedge
See `src/tests/vankeken-twophase/vk-case1b.c`. This is a highly simplified model with linear flow laws and no time-dependence.