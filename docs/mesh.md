# Mesh generation tools


## Overview

SubFUSc includes example meshes, and tools to generate new meshes (for example for different geometries) and to refine existing meshes.

### Features

* Tool to specify simple types of subduction-zone geometry 
* Includes the external package [Triangle](https://www.cs.cmu.edu/~quake/triangle.html) [see reference below].
* Enhanced refinement rule extension to [Triangle](https://www.cs.cmu.edu/~quake/triangle.html).

### Reference
>Jonathan Richard Shewchuk, Triangle: Engineering a 2D Quality Mesh Generator and Delaunay Triangulator, in ``Applied Computational Geometry: Towards Geometric Engineering'' (Ming C. Lin and Dinesh Manocha, editors), volume 1148 of Lecture Notes in Computer Science, pages 203-222, Springer-Verlag, Berlin, May 1996. 


### Preliminaries

1. Obtain the repository
```git clone https://<USERNAME>@bitbucket.org/dmay/leasz.git```
and move into it the root directory  
``` cd leasz```
2. Set the environment variable `SUBFUSC_CORE_DIR`.
In bash do the following
```export SUBFUSC_CORE_DIR=${PWD}```  
in tcsh do this  
```setenv SUBFUSC_CORE_DIR ${PWD}```

## Beginner's Tutorial: How to make a new mesh
### First compile Triangle with custom refinement rules, and a viewer
1. From the root of the SubFUSc directory, navigate to the Triangle tool `cd ${SUBFUSC_CORE_DIR}/external-packages/triangle`.
2. To use the custom refinement rules / methods within Triangle, do the following:
` make -f makefile_refine`. 
3. Note that it may be necessary to first change the C compiler in makefile_refine to match the one on your system. It may also be necessary to change the XLIBS and XINCLUDES options. These limitations are inherited from Triangle.
4. See README (original Triangle readme) and README.txt (custom version) for further details.
5. You may also wish to compile our custom viewer, which generates Paraview files showing the mesh as well as boundary/region tags. 
	- `cd ${SUBFUSC_CORE_DIR}/external-packages/triangle-viewer`
	- `make triangle-vtk`.

### Specify the subduction zone geometry
1. From the root of the SubFUSc directory, navigate to the geometry specification tool `cd ${SUBFUSC_CORE_DIR}/src/models/meshes/kinematic_subduction/generator/`	
2. Geometries are specified within `.json` files. For example, create a new file from the reference geometry, and open the file in your favourite text editor. `cp subduction-ref-spec.json subduction-example-spec.json`.
3. Edit the `.json` file with your favourite text editor as desired. For example, we can reduce the plate thickness as follows:

~~~
{  
  "polyFileName": "subduction.poly",

  "subductionModel": {
    "lengthUnits": "km",
    "domainDepth": 600.0,
    "domainWidth": 660.0,
    "slabDipAngle": 45.0,
    "plateThickness": 20.0
  },

  "regionLabels": [
      { "index": 1, "name": "slab"} ,
      { "index": 2, "name": "wedge" },
      { "index": 3, "name": "plate" }
    ],

  "triangleOptions": {
     "args": "-pjq32.0a0.0007A"
  }
}
~~~
### Create polygon file of subduction zone geometry

1. Execute the python script to generate the geometry `python subduction-polygen.py subduction-example-spec.json`. 
2. The output file is a `.poly` file. In this example, the generated .poly file will be called subduction.poly (as determined by json field "polyFileName").


### Run Triangle to make and refine the mesh
1. Run `trianglecustom` once to generate the mesh `${SUBFUSC_CORE_DIR}/external-packages/triangle/trianglecustom -pjq32.0a0.0007A subduction.poly`
2. This generates files that begin `subduction.1`. 
3. To refine the mesh, we first need to specify a refinement rule, as described in `${SUBFUSC_CORE_DIR}/external-packages/triangle/README.txt`. In this example, this is done in the file `refine.poly` (given below), which must exist in the current directory. Here, we choose to refine in the interior of 2, user-specified, polygons (quads). We specify the size scale of elements within these polygons. 
4. Run `trianglecustom` to refine the mesh `${SUBFUSC_CORE_DIR}/external-packages/triangle/trianglecustom -jprq32.0u subduction.1`  
5. This generates files that begin `subduction.2`.
6. You may wish to move all the new mesh files into their own folder. For example: `mkdir ../example` then `mv subduction.2* ../example/` 

Example refinement rule (file name `refine.poly`):

~~~
 2 
 4    0.05 -0.05   0.69 -0.7   0.69 -0.55   0.25 -0.05     0.01 0.01 0.01 0.01
 4    0.05 -0.07   0.29 -0.3   0.28 -0.26   0.101 -0.07   0.005 0.005 0.005 0.005 

~~~

### Visualize the mesh
Triangle includes a viewer `showme` that can be called (from the example directory to which you moved the files previously):

~~~
${SUBFUSC_CORE_DIR}/external-packages/triangle/showme subduction.2
~~~

We also include a script `triangle-vtk` to generate Paraview files (vtu files), which allow you to visualize the mesh, as well as the tags applied to the regions and boundaries. You must first compile the viewer (as explained above, in the Triangle section). The viewer can be called as follows:

~~~
${SUBFUSC_CORE_DIR}/external-packages/triangle-viewer/triangle-vtk subduction.2
~~~

### Tell SubFUSc to use the new mesh file
For example add the command line option:   
`-subfusc_mesh_file src/models/meshes/kinematic_subduction/example/subduction.2`

You should also add command line options to specify geometrical properties of the mesh:

~~~
-subfusc.slab_dip     45.0
-subfusc.length_scale 600.0e3 
~~~
