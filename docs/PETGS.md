## Overview

This is a toolkit designed building for geomdynamic models in two-dimensional domains. 

### Features

* High-order finite element function spaces defined over triangles
* Classic and conservative semi-Lagrangian methods
* Support for solving coupled PDEs
* Support for solving different PDEs on sub-meshes

## Object Descriptions

* DMTet
* Quadrature
* [Coefficient](coefficient.md)
* PDE
	- [Helmholtz](pde-Helmholtz.md)
	- [Stokes](pde-Stokes.md)
	- [Stokes-Darcy2Field](pde-StokesDarcy2Field.md)
	- [Darcy](pde-Darcy.md)
	- AdvectionDiffusion
		* semi-Lagrangian (classic, conservative, conservative-fluxtype)