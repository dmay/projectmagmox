
import os
import re
import pyTestHarness.test as pth

def test_fictdomaincsl_t1():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmdasl.app -test 1 -model 6 -ignore_preamble')
  expected_file = os.path.join(thisdir,'fictdomaincsl_t1.expected')
  
  def comparefunc(unittest):
    unittest.compareUnixDiff()
  
  # Create unit test object
  test = pth.Test('pde_fictdomaincsl_1',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)

def test_fictdomaincsl_t2():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmdasl.app -test 1 -model 12 -ignore_preamble')
  expected_file = os.path.join(thisdir,'fictdomaincsl_t2.expected')
  
  def comparefunc(unittest):
    unittest.compareUnixDiff()
  
  # Create unit test object
  test = pth.Test('pde_fictdomaincsl_2',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)

