
import os
import pyTestHarness.unittest as pth
import pyTestHarness.launch as launch

# import all dmtet tests
import pde_t1


def execute():
  os.environ['PYTHONUNBUFFERED'] = str('1')
  
  registered_tests = [ pde_t1.test1c() ]
  
  launcher = launch.pthLaunch()
  launcher.setVerbosityLevel(0)
  launcher.executeTestSuite(registered_tests)
  launcher.verifyTestSuite(registered_tests)


if __name__ == "__main__":
  execute()
