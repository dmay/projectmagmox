
import os
import re
import pyTestHarness.test as pth

def test_fictdomaincsl_multiphase_t1():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmda_csl_multicomponent.app -nx 33 -ny 33 -ignore_preamble')
  expected_file = os.path.join(thisdir,'fictdomain_multiphase_t1.expected')
  
  def comparefunc(unittest):
    unittest.compareUnixDiff()
  
  # Create unit test object
  test = pth.Test('pde_fictdomaincsl_multiphase_1',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)
