
import os
import re
import pyTestHarness.test as pth

def test1c():
  thisdir = os.path.dirname(__file__)

  ranks = 1
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_pde.app -ignore_preamble -ksp_monitor_short')
  expected_file = os.path.join(thisdir,'pde_t1_c.expected')
  
  def comparefunc(unittest):
    unittest.compareUnixDiff()
 
  # Create unit test object
  test = pth.Test('pde_t1_c',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)

  return(test)
