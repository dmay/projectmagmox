
import os
import re
import pyTestHarness.test as pth

def test_p1():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_pde_helmholtz_neumann.app -ignore_preamble -view false -harness_viewer -pc_type lu -mxy 12 -pk 1 -meshes 4')
  expected_file = os.path.join(thisdir,'pde_helm_neumann_pk1.expected')
  
  def comparefunc(unittest):
    # mesh k
    for k in range(1,4):
      label = 'mesh-' + '%.4d'%k
      
      key = label + '-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
      key = label + '-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
  
  # Create unit test object
  test = pth.Test('pde_helm_neumann_pk1',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)

def test_p3():
  thisdir = os.path.dirname(__file__)

  ranks = 1
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_pde_helmholtz_neumann.app -ignore_preamble -view false -harness_viewer -pc_type lu -mxy 12 -pk 3 -meshes 3')
  expected_file = os.path.join(thisdir,'pde_helm_neumann_pk3.expected')
  
  def comparefunc(unittest):
    # mesh k
    for k in range(1,3):
      label = 'mesh-' + '%.4d'%k
      
      key = label + '-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
      key = label + '-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
 
  # Create unit test object
  test = pth.Test('pde_helm_neumann_pk3',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)

  return(test)
