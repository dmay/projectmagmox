
import os
import re
import pyTestHarness.test as pth

def test_vv_p2p1():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1

  args = '-ignore_preamble -stk_fieldsplit_u_pc_type lu -stk_fieldsplit_u_pc_factor_mat_solver_package petsc -stk_ksp_type fgmres -stk_pc_fieldsplit_schur_fact_type UPPER -stk_fieldsplit_p_ksp_type preonly -stk_fieldsplit_p_pc_type lu -stk_fieldsplit_p_pc_factor_mat_solver_package petsc -stk_ksp_rtol 1.0e-12 -mxy 6 -pk 1 -pk_eta -1 -pk_fu -1 -meshes 4 -harness_viewer -stk_pc_type fieldsplit -stk_pc_fieldsplit_dm_splits'
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_pde_stokes_mms.app ' + args)
  expected_file = os.path.join(thisdir,'pde_stokes_mms_vv_p2p1.expected')
  
  def comparefunc(unittest):
    # mesh k
    for k in range(1,4):
      label = 'mesh-' + '%.4d'%k
      
      key = label + '-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
      key = label + '-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
      key = label + '-pL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-pL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
  
  # Create unit test object
  test = pth.Test('pde_stokes_mms_vv_p2p1',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)

def test_vv_p4p3():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1
  
  args = '-ignore_preamble -stk_fieldsplit_u_pc_type lu -stk_fieldsplit_u_pc_factor_mat_solver_package petsc -stk_ksp_type fgmres -stk_pc_fieldsplit_schur_fact_type UPPER -stk_fieldsplit_p_ksp_type preonly -stk_fieldsplit_p_pc_type lu -stk_fieldsplit_p_pc_factor_mat_solver_package petsc -stk_ksp_rtol 1.0e-12 -mxy 4 -pk 3 -pk_eta -1 -pk_fu -1 -meshes 4 -harness_viewer'
  
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_pde_stokes_mms.app ' + args)
  expected_file = os.path.join(thisdir,'pde_stokes_mms_vv_p4p3.expected')
  
  def comparefunc(unittest):
    # mesh k
    for k in range(1,4):
      label = 'mesh-' + '%.4d'%k
      
      key = label + '-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
      key = label + '-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-uH1'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
      
      key = label + '-pL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-6)
      key = label + '-rate-pL2'
      unittest.compareFloatingPointAbsolute(key,1.0e-2)
  
  # Create unit test object
  test = pth.Test('pde_stokes_mms_vv_p4p3',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)
