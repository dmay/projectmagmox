
import os
import sys

pythpath = os.path.join(os.environ['PWD'] + '/tests/pythontestharness/lib')
sys.path.append(pythpath)

try:
  import pyTestHarness.unittest as pth
  import pyTestHarness.launch as launch
except ImportError:
  eview = '\n—————————————————————————————————————————————————————————————————————————————————————————————————\n'
  eview = eview + '  You need to install the PythonTestHarness to execute the test suite\n'
  eview = eview + '  Execute the following command\n'
  eview = eview + '    git clone https://dmay@bitbucket.org/dmay/pythontestharness.git tests/pythontestharness\n'
  eview = eview + '  then re-reun the command\n'
  eview = eview + '    make testvalidation\n'
  eview = eview + '—————————————————————————————————————————————————————————————————————————————————————————————————\n'
  raise ImportError(eview)

if not os.environ.get('PETSC_ARCH'):
  raise Exception('You must define PETSC_ARCH to correspond to a working PETSc build')

# Import separate tests
sys.path.append(os.path.join(os.environ['PWD'], 'tests/dmtet'))
import dmtet_unittests

sys.path.append(os.path.join(os.environ['PWD'], 'tests/pde'))
import pde_unittests


# Run the dmtet unit tests
dmtet_unittests.execute()
pde_unittests.execute()
