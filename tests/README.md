# TestHarness Overview
##  Preliminaries

1. Make sure the environment variables `PETSC_DIR` and `PETSC_ARCH` have been set
2. Compile the LEASZ library and test executables. To do this, execute `make all` in the root LEASZ directory.

## Basic Usage
### 1. Run all the tests

To launch the test suite you use:  
`make testvalidation`  

The very first time you execute the above command (e.g. after cloning the subfusc repository), you will be prompted to clone the pyTestHarness reposity. pyTestHarness[] is a small library intended to help define, execute and validate tests on desktops and supercomputers. Follow the intstructions and then execute `make testvalidation` again. pyTestHarness requires some configuration information about your system, however it will prompt you if it is required and guide you through the short configuration process.

When `make testvalidation` is executed, several things are reported. Under `test status`, the textual name of each registered test is provided (shown in square brackets), along with the status of the test. The status reported is either `passed`, `failed` or `skipped`. A test may be skipped if it has been deemed that LEASZ cannot execute the test (e.g. due to a missing library dependency). Lastly, under `test report` a summary is provided indicating the total number of tests which pass / fail. `SUCCESS` is reported if all tests were executed and all tests passed.

If all goes well, you should see something like this

```
[--------- test status ----------------------]  
 [dmtet_t1_a]   passed  
 [dmtet_t2_a]   passed
 [dm_nest_t1]   passed
 [dmtet_facet_integration_t1]   passed
 [dmtet_facet_integration_t2]   passed
 [pde_t1_c]   passed
 [pde_helm_neumann_pk1]   passed
 [pde_helm_neumann_pk3]   passed
 [pde_stokes_mms_vv_p2p1]   passed
 [pde_stokes_mms_vv_p4p3]   passed
 [pde_stokes_mms_vv_p2p1_commsize2]   passed

[--------- test report ----------------------]
  0011 tests registered
  0010 Sequential tests
  0001 MPI tests
  0011 of 0011 tests executed

 [status] SUCCESS: All registered tests passed
```

**The last line is important - it indicates all tests were executed and passed**

If you configured PETSc without MPI, you will see something like this

```
[--------- test report ----------------------]
  0011 tests registered
  0010 Sequential tests
  0001 MPI tests
  0010 of 0011 tests executed

 [status] SUCCESS (partial): All executed tests passed
          Warning: Not all tests were executed!
          Warning: 0001 MPI tests were skipped!
```

This result of the entire test suite is still deemed a success as all tests which **were possible** to execute were executed and their output was verified and deemed correct. `SUCCESS (partial)` is reported since all possible tests were passed, however some tests did not get executed.


## More Advanced Usage
### 1. Run all the tests

To have more control over what is being tested, we have to directly call the python file ourselves (rather than doing this through make). Specifically, we will require to launch the tests this way

```python tests/subfusc_harness.py```

**Note** You must execute this script from the root LEASZ directory.

As before, when executed, a list of the registered test names is reported. 



### 2. Options for subfusc_harness.py
#### _Run a test collection_
Several related tests have been grouped together (e.g. a collection). One may optional request to execute test collections using the options shown below. When multiple options are provided, test for each collection will be performed

| Option name       | Description |
|-------------------|-------------|
| `--execute_dmtet` | All DMTet tests (sequential and parallel) |
| `--execute_pde`   | All PDE tests (sequential and parallel) |
| `--execute_pde.csl`   | All conservative Semi Lagrangian tests |
| `--execute_dmtet.mpi`   | All MPI DMTet tests |
| `--execute_pde.mpi`   | All MPI PDE tests |
| `--execute_mpi`   | All MPI tests |



### 3. pyTestHarness Options
As before, what is discussed here is only valid when executing via

```python tests/subfusc_harness.py``` 

#### _General_

The most useful / relevant options are summarized below

| Option name       | Description |
|-------------------|-------------|
| `--help` | report the general harness options  |
| `--list` | reports the textual names associated with each registered test |
| `--test` | a comma seperated list of registered test names to execute. Only these test will get exectuted - all other registered tests are ignored|
| `--configure` | Re-run the configuration stage for pyTestHarness |

A summary of the general test harness options is displayed when `--help` is provided.

#### _Run an individual test_
The syntax to execute a single registered test is as follows:

```python tests/subfusc_harness.py --test <test_name_1>```

A comma seperated list can be used to launch multiple registered tests.

```python tests/subfusc_harness.py --test <test_name_1>,<test_name_2>```

Note that when using the `--test` option (or the short version `-t`), **only** those tests listed will be executed.
