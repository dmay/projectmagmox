
import os
import sys

pythpath = os.path.join(os.environ['PWD'] + '/tests/pythontestharness/lib')
sys.path.append(pythpath)

# check if pythonTestHarness is installed
try:
  import pyTestHarness
except Exception as e:
  eview = '\no ------------------------------------------------------------------------------------------------- o\n'
  eview = eview + '  You need to install the PythonTestHarness to execute the test suite\n'
  eview = eview + '  Execute the following command\n'
  eview = eview + '    git clone https://bitbucket.org/dmay/pythontestharness.git tests/pythontestharness\n'
  eview = eview + '  then re-reun the command\n'
  eview = eview + '    make testvalidation\n'
  eview = eview + 'o ------------------------------------------------------------------------------------------------- o\n'
  raise RuntimeError(eview)

import pyTestHarness.harness as harness
import pyTestHarness.unittest as pth
import pyTestHarness.launcher as launch

import subfusc_harness_default_configure as subfuscHelper

# check it a valid PETSC_ARCH can be found
if not os.environ.get('PETSC_ARCH'):
  raise Exception('You must define PETSC_ARCH to correspond to a working PETSc build')

# Import everything
# <dmtet>
sys.path.append(os.path.join(os.environ['PWD'], 'tests/dmtet'))
import dmtet_t1
import dmtet_t2
import dm_nest_t1
import dmtet_facet

# <pde>
sys.path.append(os.path.join(os.environ['PWD'], 'tests/pde'))
import pde_t1
import pde_helm_neumann
import pde_stokes_mms
import pde_csl
import pde_csl_multiphase

import pde_stokes_mms_mpi


def subfusc_harness():
  os.environ['PYTHONUNBUFFERED'] = str('1')

  args = sys.argv

  if '--default_configure' in args:
    conffile = subfuscHelper.getDefaultConfigureFileName()
    # check for existence, only create it if it does not exist
    if os.path.isfile(conffile) != True:
      subfuscHelper.writeDefaultConfigureFile()

  if '--default_configure_force' in args:
    conffile = subfuscHelper.getDefaultConfigureFileName()
    subfuscHelper.writeDefaultConfigureFile()
  

  # sequential tests
  dmtet_unittests = [ dmtet_t1.test1a(), dmtet_t2.test1a(),
                     dm_nest_t1.test1(),
                     dmtet_facet.test1(), dmtet_facet.test2() ]

  pde_unittests = [ pde_t1.test1c(),
                   pde_helm_neumann.test_p1(), pde_helm_neumann.test_p3(),
                   pde_stokes_mms.test_vv_p2p1(), pde_stokes_mms.test_vv_p4p3() ]
                   
  pde_csl_unittests = [ pde_csl.test_fictdomaincsl_t1(),
                       pde_csl.test_fictdomaincsl_t2(),
                       pde_csl_multiphase.test_fictdomaincsl_multiphase_t1() ]


  # mpi tests
  pde_mpi_unittests = [ pde_stokes_mms_mpi.test_vv_p2p1_cs2() ]

  registered_tests =                    dmtet_unittests + pde_unittests
  registered_tests = registered_tests                   + pde_mpi_unittests
  
  anyCollections = False
  if '--execute_dmtet' in args:
    if anyCollections == False:
      anyCollections = True
      registered_tests = []
    registered_tests += dmtet_unittests

  if '--execute_pde' in args:
    if anyCollections == False:
      anyCollections = True
      registered_tests = []
    registered_tests += pde_unittests + pde_mpi_unittests

  if '--execute_pde.csl' in args:
    if anyCollections == False:
      anyCollections = True
      registered_tests = []
    registered_tests += pde_csl_unittests

  if '--execute_mpi' in args:
    if anyCollections == False:
      anyCollections = True
      registered_tests = []
    registered_tests += pde_mpi_unittests

  if '--execute_dmtet.mpi' in args:
    raise RuntimeError('There are not pure DMTet MPI tests defined')

  if '--execute_pde.mpi' in args:
    if anyCollections == False:
      anyCollections = True
      registered_tests = []
    registered_tests += pde_mpi_unittests

  print('o ------------------------------------------------- o')
  print('o --------  SubFUSC Regression Test Suite  -------- o')
  print('o ------------------------------------------------- o')
  subfuscHarness = harness.pthHarness(registered_tests)
  subfuscHarness.setVerbosityLevel(0)
  subfuscHarness.execute()
  subfuscHarness.verify()
  #subfuscHarness.clean()



if __name__ == "__main__":
  subfusc_harness()
