
import os
import re
import pyTestHarness.test as pth

def test1():
  thisdir = os.path.dirname(__file__)

  ranks = 1
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmtet_bfacets.app -ignore_preamble -fid 0')

  expected_file = os.path.join(thisdir,'dmtet_facet_integration_t1.expected')

  def comparefunc(unittest):
    unittest.compareUnixDiff()
  
  # Create unit test object
  test = pth.Test('dmtet_facet_integration_t1',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)

  return(test)

def test2():
  thisdir = os.path.dirname(__file__)
  
  ranks = 1
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmtet_bfacets.app -ignore_preamble -fid 1')
  
  expected_file = os.path.join(thisdir,'dmtet_facet_integration_t2.expected')
  
  def comparefunc(unittest):
    unittest.compareUnixDiff()
  
  # Create unit test object
  test = pth.Test('dmtet_facet_integration_t2',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)
  
  return(test)
