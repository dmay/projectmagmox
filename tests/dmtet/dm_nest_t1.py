
import os
import re
import pyTestHarness.test as pth

def test1():
  thisdir = os.path.dirname(__file__)

  ranks = 1
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dm_4field.app -ignore_preamble')
  expected_file = os.path.join(thisdir,'dm_nest_t1.expected')
  
  def comparefunc(unittest):
    
    unittest.compareUnixDiff()
    
  # Create unit test object
  test = pth.Test('dm_nest_t1',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)

  return(test)
