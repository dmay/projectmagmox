
import os
import re
import pyTestHarness.test as pth

def test1a():
  thisdir = os.path.dirname(__file__)

  ranks = 1
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmtet.app')
  expected_file = os.path.join(thisdir,'dmtet_t1_a.expected')
  
  def comparefunc(unittest):
    
    key = re.escape('nrm(x-y)')
    unittest.compareFloatingPoint(key, 0.000000e+00)
    
  # Create unit test object
  test = pth.Test('dmtet_t1_a',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)

  return(test)
