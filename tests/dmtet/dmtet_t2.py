
import os
import re
import pyTestHarness.test as pth

def test1a():
  thisdir = os.path.dirname(__file__)

  ranks = 1
  launch = os.path.join(thisdir,'../../${PETSC_ARCH}/bin/test_dmtet_p2p1.app -ignore_preamble')
  expected_file = os.path.join(thisdir,'dmtet_t2_a.expected')
  
  def comparefunc(unittest):
    
    unittest.compareUnixDiff()
    
  # Create unit test object
  test = pth.Test('dmtet_t2_a',ranks,launch,expected_file)
  test.setVerifyMethod(comparefunc)
  test.setOutputPath(thisdir)

  return(test)
