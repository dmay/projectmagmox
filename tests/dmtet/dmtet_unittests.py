
import os
import pyTestHarness.unittest as pth
import pyTestHarness.launch as launch

# import all dmtet tests
from dmtet_t1 import test1a
import dmtet_t2
import dm_nest_t1

def execute():
  os.environ['PYTHONUNBUFFERED'] = str('1')
  
  registered_tests = [ test1a() , dmtet_t2.test1a() , dm_nest_t1.test1() ]
  
  launcher = launch.pthLaunch()
  launcher.setVerbosityLevel(0)
  launcher.executeTestSuite(registered_tests)
  launcher.verifyTestSuite(registered_tests)


if __name__ == "__main__":
  execute()
