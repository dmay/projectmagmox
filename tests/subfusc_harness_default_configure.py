
import os
from pyTestHarness.version import getVersion

def getDefaultConfigureFileName():
  confFilename = 'pthBatchQueuingSystem.conf'
  return confFilename

def writeDefaultConfigureFile():
  confFile = getDefaultConfigureFileName()
  print('[subfusc harness default conf file] Generating default pyTestHarness configuration file:',confFile)
  file = open(confFile,'w')
  major,minor,patch=getVersion()
  file.write('majorVersion=' + str(major) + '\n')
  file.write('minorVersion=' + str(minor) + '\n')
  file.write('patchVersion=' + str(patch) + '\n')
  file.write('queuingSystemType=none\n' )
  print('[subfusc harness default conf file] Assuming no queuing system')
  
  PETSC_DIR = os.getenv('PETSC_DIR')
  PETSC_ARCH = os.getenv('PETSC_ARCH')
  if PETSC_DIR: # some module systems only define PETSC_DIR
    petscLauncher = os.path.join(PETSC_DIR,PETSC_ARCH,'bin','mpiexec') + ' -n <ranks>'
    print('[subfusc harness default conf file] Detected a PETSC_DIR/PETSC_ARCH path:',os.path.join(PETSC_DIR,PETSC_ARCH))
    print('[subfusc harness default conf file] Using the launcher:',petscLauncher)
    file.write('mpiLaunch=' + ' ' + petscLauncher )
  else:
    file.write('mpiLaunch=none\n' )
  file.close()
