
#ifdef SINGLE
#define REAL float
#else /* not SINGLE */
#define REAL double
#endif /* not SINGLE */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef CPU86
#include <float.h>
#endif /* CPU86 */
#ifdef LINUX
#include <fpu_control.h>
#endif /* LINUX */

typedef REAL *vertex;


/********* User-defined triangle evaluation routine begins here      *********/
/**                                                                         **/
/**                                                                         **/

/*****************************************************************************/
/*                                                                           */
/*  triunsuitable()   Determine if a triangle is unsuitable, and thus must   */
/*                    be further refined.                                    */
/*                                                                           */
/*  You may write your own procedure that decides whether or not a selected  */
/*  triangle is too big (and needs to be refined).  There are two ways to do */
/*  this.                                                                    */
/*                                                                           */
/*  (1)  Modify the procedure `triunsuitable' below, then recompile          */
/*  Triangle.                                                                */
/*                                                                           */
/*  (2)  Define the symbol EXTERNAL_TEST (either by adding the definition    */
/*  to this file, or by using the appropriate compiler switch).  This way,   */
/*  you can compile triangle.c separately from your test.  Write your own    */
/*  `triunsuitable' procedure in a separate C file (using the same prototype */
/*  as below).  Compile it and link the object code with triangle.o.         */
/*                                                                           */
/*  This procedure returns 1 if the triangle is too large and should be      */
/*  refined; 0 otherwise.                                                    */
/*                                                                           */
/*****************************************************************************/

/*
  vertex triorg;                              // The triangle's origin vertex.
  vertex tridest;                        // The triangle's destination vertex.
  vertex triapex;                               // The triangle's apex vertex.
  REAL area;                                      // The area of the triangle.
*/

/* 
 Types
 
 Radial support: All triangle centroids located in |X - X_c| which have an edge > target_edge_length will be refined.
 
*/
 
typedef struct {
  int nsamples;
  double *coor;
  double *support,*target_edge_length; // type 0
} RefRule_Radial;

void LoadRefinementRule_RadialSupport(RefRule_Radial *rule)
{
  FILE *fp = NULL;
  int k,ns,type;
  
  rule->nsamples = 0;
  
  fp = fopen("refine.radial","r");
  if (fp == NULL) {
    printf("\n*** User requsted that a custom refinement rule be used ***\n");
    printf("*** Looked for file \"refine.radial\" - file not located ***\n");
    printf("*** Refinement rule \"radial support\" not being used ***\n");
    return;
  } else {
    printf("\n*** User requsted that a custom refinement rule \"radial support\"be used ***\n");
    printf("*** File sought must be called \"refine.radial\"\n");
    printf("*** File format: \n");
    printf("*** npoints \n");
    printf("*** index1 x_1 y_1 supportradius_1 edgesize_1 \n");
    printf("*** index2 x_2 y_2 supportradius_2 edgesize_2 \n");
    printf("*** Notes  (i) supports may overlap \n");
    printf("*** Notes (ii) any triangle centroid located within the support will use edgesize_i to determine if the triangle should be refined \n");
  }
  
  fscanf(fp,"%d",&ns);
  
  rule->nsamples = ns;
  rule->coor = malloc(sizeof(double)*2*ns);
  memset(rule->coor,0,sizeof(double)*2*ns);
  
  rule->support = malloc(sizeof(double)*ns);
  memset(rule->support,0,sizeof(double)*ns);
  
  rule->target_edge_length = malloc(sizeof(double)*ns);
  memset(rule->target_edge_length,0,sizeof(double)*ns);
  
  for (k=0; k<rule->nsamples; k++) {
    fscanf(fp,"%d %lf %lf %lf %lf",&type,&rule->coor[2*k],&rule->coor[2*k+1],&rule->support[k],&rule->target_edge_length[k]);
  }
  
  fclose(fp);
  
  printf("xxx === Summary of refinement rules [radial support] === xxx\n");
  for (k=0; k<rule->nsamples; k++) {
    printf("[%.4d] : ",k);
    printf("coor %+1.4e , %+1.4e ; support %+1.4e ; edge length %+1.4e\n",
             rule->coor[2*k],rule->coor[2*k+1],rule->support[k],rule->target_edge_length[k]);
  }
}

int TriUnsuitable_RadialSupport(RefRule_Radial *rule,double centroid[],REAL max_edge_length)
{
  int k,refine = 0;
  
  for (k=0; k<rule->nsamples; k++) {
    double *scoor,sep2;
    
    scoor = &rule->coor[2*k];
    sep2 =  (centroid[0] - scoor[0])*(centroid[0] - scoor[0]);
    sep2 += (centroid[1] - scoor[1])*(centroid[1] - scoor[1]);
    if (sep2 < rule->support[k]*rule->support[k]) {
      if (max_edge_length > rule->target_edge_length[k]) {
        refine = 1;
      }
    }
  }
  return(refine);
}

/* poly */

typedef struct {
  int npoly;
  double *coor; // npoly * 4 * 2
  double *target_edge_length; // npoly * 4
} RefRule_Poly;


void LoadRefinementRule_Poly(RefRule_Poly *rule)
{
  FILE *fp = NULL;
  int k,ns;
  
  rule->npoly = 0;
  
  fp = fopen("refine.poly","r");
  if (fp == NULL) {
    printf("\n*** User requsted that a custom refinement rule be used ***\n");
    printf("*** Looked for file \"refine.poly\" - file not located ***\n");
    printf("*** Refinement rule \"polygon\" not being used ***\n");
    return;
  } else {
    printf("\n*** User requsted that a custom refinement rule \"polygon\"be used ***\n");
    printf("*** File sought must be called \"refine.poly\"\n");
    printf("*** File format: \n");
    printf("*** npolygons \n");
    printf("*** nverticesA x_1 y_1 x_2 y_2 ... x_nerticesA y_nverticesA edgesize_1 ... edgesize_nverticesA \n");
    printf("*** nverticesB x_1 y_1 x_2 y_2 ... x_nerticesB y_nverticesB edgesize_1 ... edgesize_nverticesB \n");
    printf("*** Notes   (i) only support for polygons with 4 vertices is provided (e.g. quadralertials) \n");
    printf("*** Notes  (ii) polygon regions may overlap, multiple polygons can be included in the same file \n");
    printf("*** Notes (iii) any triangle centroid located within a polygon will use an interpolated edge size to determine if the triangle should be refined \n");
  }
  
  fscanf(fp,"%d",&ns);
  
  rule->npoly = ns;
  rule->coor = malloc(sizeof(double)*2*4*ns);
  memset(rule->coor,0,sizeof(double)*2*4*ns);
  
  rule->target_edge_length = malloc(sizeof(double)*4*ns);
  memset(rule->target_edge_length,0,sizeof(double)*4*ns);
  
  for (k=0; k<rule->npoly; k++) {
    int nvert;
    
    fscanf(fp,"%d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
           &nvert,
           &rule->coor[8*k+0],
           &rule->coor[8*k+1],
           &rule->coor[8*k+2],
           &rule->coor[8*k+3],
           &rule->coor[8*k+4],
           &rule->coor[8*k+5],
           &rule->coor[8*k+6],
           &rule->coor[8*k+7],
           &rule->target_edge_length[4*k+0],
           &rule->target_edge_length[4*k+1],
           &rule->target_edge_length[4*k+2],
           &rule->target_edge_length[4*k+3]);
    if (nvert != 4) {
      printf("*** Looked for file \"refine.poly\" - file located ***\n");
      printf("*** Error ***  only support for polygons with 4 vertices provided \n");
      printf("*** Aborting ***\n");
      exit(0);
    }
  }
  
  fclose(fp);
  
  printf("xxx === Summary of refinement rules [polygon] === xxx\n");
  for (k=0; k<rule->npoly; k++) {
    printf("[poly %.4d] : \n",k);
    
    printf("     [3](%+1.4e , %+1.4e) [2](%+1.4e , %+1.4e) :             [3]%+1.4e  [2]%+1.4e\n",
           rule->coor[8*k+6],rule->coor[8*k+7], rule->coor[8*k+4],rule->coor[8*k+5], rule->target_edge_length[4*k+3],rule->target_edge_length[4*k+2]);
    printf("coor [0](%+1.4e , %+1.4e) [1](%+1.4e , %+1.4e) : edge_length [0]%+1.4e  [1]%+1.4e\n",
           rule->coor[8*k+0],rule->coor[8*k+1], rule->coor[8*k+2],rule->coor[8*k+3], rule->target_edge_length[4*k+0],rule->target_edge_length[4*k+1]);
  }
}

int pnpoly(int nvert,double vert[],double coor[])
{
  int i, j, c = 0;
  double testx = coor[0];
  double testy = coor[1];
  
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((vert[2*i+1]>testy) != (vert[2*j+1]>testy)) &&
        (testx < (vert[2*j]-vert[2*i]) * (testy-vert[2*i+1]) / (vert[2*j+1]-vert[2*i+1]) + vert[2*i]) )
      c = !c;
  }
  return c;
}

// https://www.particleincell.com/2012/quad-interpolation/
void interpolate(double coor[],double vert[],double vals[],double *iv)
{
  double a[4],b[4],aa,bb,cc,det,xi,eta;
  int idx1 = 0;
  int idx2 = 1;
  int idx3 = 2;
  int idx4 = 3;
  
  a[0] =  vert[2*idx1+0];
  a[1] = -vert[2*idx1+0] + vert[2*idx2+0];
  a[2] = -vert[2*idx1+0]                                   + vert[2*idx4+0];
  a[3] =  vert[2*idx1+0] - vert[2*idx2+0] + vert[2*idx3+0] - vert[2*idx4+0];

  b[0] =  vert[2*idx1+1];
  b[1] = -vert[2*idx1+1] + vert[2*idx2+1];
  b[2] = -vert[2*idx1+1]                                   + vert[2*idx4+1];
  b[3] =  vert[2*idx1+1] - vert[2*idx2+1] + vert[2*idx3+1] - vert[2*idx4+1];

  aa = a[3]*b[2] - a[2]*b[3];
  bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + coor[0]*b[3] - coor[1]*a[3];
  cc = a[1]*b[0] -a[0]*b[1] + coor[0]*b[1] - coor[1]*a[1];
  
  if (fabs(aa) <1.0e-20) {
    /* affine transformation */
    eta = -cc / bb;
    xi = (coor[0]-a[0]-a[2]*eta)/(a[1]+a[3]*eta);
  } else {
    // compute m = (-b+sqrt(b^2-4ac))/(2a)
    det = sqrt(bb*bb - 4.0*aa*cc);
    eta = (-bb+det)/(2.0*aa);
    // compute xi
    xi = (coor[0]-a[0]-a[2]*eta)/(a[1]+a[3]*eta);
  }
  
  //printf("%1.4e %1.4e\n",xi,eta);
  *iv = (1.0-xi)*(1.0-eta)*vals[0] + (xi)*(1.0-eta)*vals[1] + (xi)*(eta)*vals[2] + (1.0-xi)*(eta)*vals[3];
}

int TriUnsuitable_Poly(RefRule_Poly *rule,double centroid[],REAL max_edge_length)
{
  int k,refine = 0;
  
  for (k=0; k<rule->npoly; k++) {
    double *vertlist;
    int c;
    
    vertlist = &rule->coor[8*k];
    c = pnpoly(4,vertlist,centroid);
    
    if (c > 0) { /* point inside polygon */
      double minedge,edge_interp;

      
      //printf("xc ( %1.4e , %1.4e )\n",centroid[0],centroid[1]);
      /*
      minedge = rule->target_edge_length[4*k];
      if (max_edge_length > minedge) {
        refine = 1;
      }
       */
      
      /* interpolate edge constraints */
      interpolate(centroid,vertlist,&rule->target_edge_length[4*k],&edge_interp);
      //printf("edge_interp = %1.4e\n",edge_interp);
      if (max_edge_length > edge_interp) {
        refine = 1;
      }
      
    }
  }
  return(refine);
}



RefRule_Radial rule_radial;
RefRule_Poly   rule_poly;

int triunsuitable(vertex triorg, vertex tridest, vertex triapex, REAL area)
{
  REAL dxoa, dxda, dxod;
  REAL dyoa, dyda, dyod;
  REAL oalen, dalen, odlen;
  REAL maxlen;
  static int beenhere = 0;
  int k,refine,refine_radial,refine_poly;
  double centroid[2];
  double maxedgelen;
  
  /* look for the refinement rule files and load it */
  if (beenhere == 0) {
    LoadRefinementRule_RadialSupport(&rule_radial);
    LoadRefinementRule_Poly(&rule_poly);
    
    beenhere = 1;
  }
  
  /* compute centroid */
  
  /* check if the centroid is inside your sample domain */
  
  /* apply rule */
  
  dxoa = triorg[0] - triapex[0];
  dyoa = triorg[1] - triapex[1];
  dxda = tridest[0] - triapex[0];
  dyda = tridest[1] - triapex[1];
  dxod = triorg[0] - tridest[0];
  dyod = triorg[1] - tridest[1];
  /* Find the squares of the lengths of the triangle's three edges. */
  oalen = dxoa * dxoa + dyoa * dyoa;
  dalen = dxda * dxda + dyda * dyda;
  odlen = dxod * dxod + dyod * dyod;
  /* Find the square of the length of the longest edge. */
  maxlen = (dalen > oalen) ? dalen : oalen;
  maxlen = (odlen > maxlen) ? odlen : maxlen;

  maxedgelen = sqrt(maxlen);
  
  centroid[0] = 0.3333 * (triorg[0] + triapex[0] + tridest[0]);
  centroid[1] = 0.3333 * (triorg[1] + triapex[1] + tridest[1]);

  /* Apply rules */
  refine = 0;
  refine_radial = TriUnsuitable_RadialSupport(&rule_radial,centroid,maxedgelen);
  if (refine_radial >= refine) { refine = refine_radial; }
  
  refine_poly = TriUnsuitable_Poly(&rule_poly,centroid,maxedgelen);
  if (refine_poly >= refine) { refine = refine_poly; }
  
  
  return(refine);
}

