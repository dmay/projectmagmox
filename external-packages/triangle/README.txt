Triangle allows you to control the size of triangles in your mesh in a couple of ways.
The most basic option is define a region, and specify the target triangle area within each region.
This doesn't allow much flexibility w.r.t. defining zones of regional refinement unless you
mess around and define geometry which contains lots of different sectors to which you assign
a region attributed + target area.

To help define local refinement (or local triangle edge lengths), you can implement your
own function to determine if a triangle should be refined. I've implemented two methods
(a) Define a coordinate Xd, attach to a coordinate a support radius (r) and target edge length (eT).
    All triangle centroids found within the support of Xd will be marked for refinement if
    the min edge length on that triangle is greater than eT.
(b) Define a polygonal via four vertices and attach to each vertex a target edge length.
    Triangle centroids located inside the polygonal compute a target edge length through 
    interpolating the vertex edge lengths. If the interpolated edge length is smaller than
    the min edge length of the triangle, the triangle is marked for refinement.

Invoking option (a) and or (b) doesn not require you alter your geometry (.poly) file.
The specification of these refinement rules uses additional input files which must be named
  refine.radial
  refine.poly
repsectively. Files can contain as many points (or polygons) as you like.

Notes:
* These refinement rules can only be applied to an existing triangulation. e.g. by using
  a command like
    ./triangle -jprq32.0u wedge.1
  Important arguments are 
    "r" --> Refine an existing mesh
    "u" --> Use a user defined refinement rule
    "p" --> Preserve the planar straight line segments defined in the original .poly file.
            If you don't include the "p" option, the refined mesh will NOT conform to any
            region domains which were in the original triangulation.

To use the custom refinement rules / methods within triangle, do the following:
  cp makefile_refine makefile
  make trianglecustom

If you wish to use the standard refinement rules / methods provided by triangle, either
(i) use the original make file and execute 
  make all
or, (ii) use makefile_refine and type
  make all 

Note that since you have changed the contents of the makefile (via the copy command), running git status
will tell your something like this

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   external-packages/triangle/makefile

Please execute: 
  git checkout external-packages/triangle/makefile
to remove your changes




