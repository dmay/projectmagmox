
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct {
  int nelements,nodes_per_el;
  int *element;
  int *element_tag;
  int nnodes;
  double *coords;
  int *bmarker;
  int nedges;
  int *edge;
  int *emarker;
} TriMesh;


int _Triangle_ReadEleFile(TriMesh *m,FILE *fp)
{
  int nel,npe,dummy,nattributes;
  int index,n0,n1,n2,tag,cnt,shift;
  
  fscanf(fp,"%d %d %d",&nel,&npe,&nattributes);
  m->nelements = nel;
  m->nodes_per_el = npe;
  printf("Triangle: Found %d elements\n",nel);
  printf("Triangle:   attributes per element %d\n",nattributes);

  if (nattributes > 1) {
    printf("Only support for reading 1 element attribute\n");
    exit(EXIT_FAILURE);
  }

  /* allocate space */
  m->element = malloc(sizeof(int)*npe*nel);
  m->element_tag = malloc(sizeof(int)*nel);
  
  /* scan and load */
  cnt = 0;
  shift = 0;
  while (cnt != nel) {
    
    if (nattributes == 0) {
      fscanf(fp,"%d %d %d %d",&index,&n0,&n1,&n2);
    } else if (nattributes == 1) {
      fscanf(fp,"%d %d %d %d %d",&index,&n0,&n1,&n2,&tag);
    }
    if (cnt == 0) {
      if (index == 1) shift = 1;
    }
    
    m->element[3*cnt+0] = n0 - shift;
    m->element[3*cnt+1] = n1 - shift;
    m->element[3*cnt+2] = n2 - shift;
    
    m->element_tag[cnt] = 0;
    if (nattributes == 1) {
      m->element_tag[cnt] = tag;
    }
    
    cnt++;
  }
  return(0);
}

int _Triangle_ReadNodeFile(TriMesh *m,FILE *fp)
{
  int nn,dim,nattr,boundarymarkers,bmarker;
  int index,cnt;
  double x,y,dummy_attr;
  
  fscanf(fp,"%d %d %d %d",&nn,&dim,&nattr,&boundarymarkers);
  m->nnodes = nn;

  printf("Triangle: Found %d nodes\n",nn);
  printf("Triangle:   attributes per node %d\n",nattr);
  if (boundarymarkers == 1) {
    printf("Triangle:   detected boundary markers\n");
  } else {
    printf("Triangle:   no boundary markers were detected\n");
  }

  /* allocate space */
  m->coords = malloc(sizeof(double)*dim*nn);
  m->bmarker = malloc(sizeof(int)*nn);
  
  if (nattr == 1) {
    printf("Warning: Detected one node attributes, however these will not be stored\n");
  }

  if (nattr > 1) {
    printf("Only support for reading 0 or 1 node attribute\n");
    exit(EXIT_FAILURE);
  }
  
  /* scan and load */
  cnt = 0;
  while (cnt != nn) {
    if (nattr == 0 ) {
      fscanf(fp,"%d %lf %lf %d",&index,&x,&y,&bmarker);
    } else if (nattr == 1) {
      fscanf(fp,"%d %lf %lf %lf %d",&index,&x,&y,&dummy_attr,&bmarker);
    }
    //printf("%lf %lf \n",x,y);
    
    m->coords[2*cnt+0] = x;
    m->coords[2*cnt+1] = y;
    m->bmarker[cnt] = bmarker;
    
    cnt++;
  }
  return(0);
}

int _Triangle_ReadEdgeFile(TriMesh *m,FILE *fp)
{
  int ne,dummy,boundarymarkers,bmarker;
  int index,n0,n1,n2,tag,cnt,shift;
  
  fscanf(fp,"%d %d",&ne,&boundarymarkers);
  m->nedges = ne;
  printf("Triangle: Found %d edges\n",ne);
  if (boundarymarkers == 1) {
    printf("Triangle:   detected boundary markers\n");
  } else {
    printf("Triangle:   no boundary markers were detected\n");
  }
  
  /* allocate space */
  m->edge = malloc(sizeof(int)*ne*2);
  m->emarker = malloc(sizeof(int)*ne);
  
  /* scan and load */
  cnt = 0;
  shift = 0;
  while (cnt != ne) {
    
    if (boundarymarkers == 0) {
      fscanf(fp,"%d %d %d",&index,&n0,&n1);
    } else if (boundarymarkers == 1) {
      fscanf(fp,"%d %d %d %d",&index,&n0,&n1,&bmarker);
    }
    if (cnt == 0) {
      if (index == 1) shift = 1;
    }
    
    
    m->edge[2*cnt+0] = n0 - shift;
    m->edge[2*cnt+1] = n1 - shift;
    
    m->emarker[cnt] = -20;
    if (boundarymarkers == 1) {
      m->emarker[cnt] = bmarker;
    }
    
    cnt++;
  }
  return(0);
}

int TriangleMeshView_asciiVTK(TriMesh *m,FILE *fp)
{
  int e,i;
  
  fprintf(fp,"<?xml version=\"1.0\"?> \n");
  fprintf(fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
  
  fprintf(fp,"  <UnstructuredGrid> \n");
  fprintf(fp,"    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\" \n",m->nnodes,m->nelements);
  fprintf(fp,"\n");

  fprintf(fp,"    <Cells> \n");
  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<m->nelements; e++) {
    fprintf(fp,"%d %d %d ",
                 m->element[3*e],m->element[3*e+1],m->element[3*e+2]);
    
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");

  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<m->nelements; e++) {
    fprintf(fp,"%d ",3*e+3);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");

  fprintf(fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<m->nelements; e++) {
    fprintf(fp,"5 ");
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </Cells> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    <CellData> \n");
  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"region_idx\" format=\"ascii\"> \n");
  for (e=0; e<m->nelements; e++) {
    fprintf(fp,"%d ",m->element_tag[e]);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </CellData> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    <Points> \n");
  fprintf(fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<m->nnodes; i++) {
    fprintf(fp,"%1.10e %1.10e 0.0 ",m->coords[2*i],m->coords[2*i+1]);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </Points> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    <PointData> \n");
  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"bndy_tag\" format=\"ascii\"> \n");
  for (i=0; i<m->nnodes; i++) {
    fprintf(fp,"%d ",m->bmarker[i]);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </PointData> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    </Piece> \n");
  fprintf(fp,"  </UnstructuredGrid> \n");
  fprintf(fp,"</VTKFile>");
  
  return(0);
}

int TriangleMeshViewEdge_asciiVTK(TriMesh *m,FILE *fp)
{
  int e,i;
  
  fprintf(fp,"<?xml version=\"1.0\"?> \n");
  fprintf(fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
  
  fprintf(fp,"  <UnstructuredGrid> \n");
  fprintf(fp,"    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\" \n",m->nnodes,m->nedges);
  fprintf(fp,"\n");
  
  fprintf(fp,"    <Cells> \n");
  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<m->nedges; e++) {
    fprintf(fp,"%d %d ",
            m->edge[2*e],m->edge[2*e+1]);
    
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  
  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<m->nedges; e++) {
    fprintf(fp,"%d ",2*e+2);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  
  fprintf(fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<m->nedges; e++) {
    fprintf(fp,"3 ");
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </Cells> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    <CellData> \n");
  fprintf(fp,"      <DataArray type=\"Int32\" Name=\"b_idx\" format=\"ascii\"> \n");
  for (e=0; e<m->nedges; e++) {
    fprintf(fp,"%d ",m->emarker[e]);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </CellData> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    <Points> \n");
  fprintf(fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<m->nnodes; i++) {
    fprintf(fp,"%1.10e %1.10e 0.0 ",m->coords[2*i],m->coords[2*i+1]);
  } fprintf(fp,"\n");
  fprintf(fp,"      </DataArray> \n");
  fprintf(fp,"    </Points> \n");
  
  fprintf(fp,"\n");
  
  fprintf(fp,"    </Piece> \n");
  fprintf(fp,"  </UnstructuredGrid> \n");
  fprintf(fp,"</VTKFile>");
  
  return(0);
}

int main(int nargs,char *args[])
{
  TriMesh mesh;
  char prefix[1024];
  char ele_filename[1024],node_filename[1024],edge_filename[1024],vfilename[1024];
  FILE *fp;
  int ierr;
  
  if (nargs != 2) {
    printf("Error: Expected prefix of triangle mesh file to be provided as a command line argument\n");
    printf("Usage:\n");
    printf("  ./triangle-vtk path/to/my/trianglefiles\n");
    printf("  This will look for the following files;\n");
    printf("    (  i) path/to/my/trianglefiles.ele\n");
    printf("    ( ii) path/to/my/trianglefiles.node\n");
    printf("    (iii) path/to/my/trianglefiles.edege\n");
    printf("  On output, the following two files will be created;\n");
    printf("    ( i) path/to/my/trianglefiles.vtu\n");
    printf("    (ii) path/to/my/trianglefiles-edge.vtu\n");
    exit(EXIT_FAILURE);
  } else {
    sprintf(prefix,"%s",args[1]);
  }
  
  sprintf(ele_filename,"%s.ele",prefix);
  fp = fopen(ele_filename,"r");
  if (!fp) { printf("Failed to open element file %s\n",ele_filename); exit(EXIT_FAILURE); }
  ierr = _Triangle_ReadEleFile(&mesh,fp);
  fclose(fp);

  sprintf(node_filename,"%s.node",prefix);
  fp = fopen(node_filename,"r");
  if (!fp) { printf("Failed to open node file %s\n",node_filename); exit(EXIT_FAILURE); }
  ierr = _Triangle_ReadNodeFile(&mesh,fp);
  fclose(fp);

  //sprintf(edge_filename,"%s.edge",prefix);
  //fp = fopen(edge_filename,"r");
  //if (!fp) { printf("Failed to open edge file %s\n",edge_filename); exit(EXIT_FAILURE); }
  //ierr = _Triangle_ReadEdgeFile(&mesh,fp);
  //fclose(fp);
  
  sprintf(vfilename,"%s.vtu",prefix);
  fp = fopen(vfilename,"w");
  if (!fp) { printf("Failed to open element file %s\n",vfilename); exit(EXIT_FAILURE); }
  ierr = TriangleMeshView_asciiVTK(&mesh,fp);
  fclose(fp);

  //sprintf(vfilename,"%s-edge.vtu",prefix);
  //fp = fopen(vfilename,"w");
  //if (!fp) { printf("Failed to open element edge file %s\n",vfilename); exit(EXIT_FAILURE); }
  //ierr = TriangleMeshViewEdge_asciiVTK(&mesh,fp);
  //fclose(fp);
  
  return(EXIT_SUCCESS);
}
