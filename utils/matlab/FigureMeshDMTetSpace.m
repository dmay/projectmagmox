% 
% Visualizes mesh
%



function FigureMeshDMTetSpace(coor,e2n_low)

figure(1)
trisurf(e2n_low,coor(:,1),coor(:,2),zeros(length(coor),1),'FaceColor','white');
colormap(jet(200));
axis equal
view(2);

end % /function
