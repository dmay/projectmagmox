
function G = LoadDMTetGeometry(filename)

%clear all;
%filename = '/Users/dmay/codes/leasz/dmp.h5';
%fprintf(1,'Loading DMTET file \"%s\"\n',filename);

nnodes = h5readatt(filename,'/DMTet/Geometry','/nnodes');
nelements = h5readatt(filename,'/DMTet/Geometry','/nelements');
nodes_per_element = h5readatt(filename,'/DMTet/Geometry','/nodes_per_element');

G.filename = filename;
G.nnodes = nnodes;
G.nelements = nelements;
G.nodes_per_element = nodes_per_element;

e2nH = h5read(filename,'/DMTet/Geometry/map_el_nd');
e2n = zeros(nelements,nodes_per_element);
for j=1:nodes_per_element
    e2n(:,j) = e2nH(j:nodes_per_element:end);
end
% convert from C to matlab indices starting from 1
e2n = e2n + 1;
G.e2n = e2n;

coorH = h5read(filename,'/DMTet/Geometry/coors');
coor = zeros(nnodes,2);
coor(:,1) = coorH(1:2:end);
coor(:,2) = coorH(2:2:end);
G.coorx = coor(:,1);
G.coory = coor(:,2);

regionH = h5read(filename,'/DMTet/Geometry/tags_el');
G.region = regionH;

%figure(1)
%%trisurf(e2n,coor(:,1),coor(:,2),zeros(length(coor),1),'FaceColor','white');
%trisurf(e2n,coor(:,1),coor(:,2),zeros(length(coor),1),regionH);
%colormap(jet(200));
%axis equal
%view(2);

end % /function

