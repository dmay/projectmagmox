
function fighandle = ViewDMTetGeometry(G,which,varargin)
  fighandle = [];
  if nargin < 2; helpfunc(f,@ViewDMTetGeometry); return; end;
  if ischar(G); G = loadPipeflowOutput(G); end
  if ~isstruct(G); error('Invalid output structure.'); end;

  flist(1).str={'mesh','m'};        flist(1).func=@plotMesh; 
  flist(end+1).str={'regions','r'}; flist(end).func=@plotRegions; 
  
  index = 0;
  for i=1:length(flist)
    if find(strmatch(which,flist(i).str,'exact'))
      index = i;
      break;
    end
  end
  
  if index ~= 0
    index = flist(index).func(G,varargin{:});
  else
    helpfunc(flist,@ViewDMTetGeometry);
  end
end

function h = plotRegions(G,varargin)
  h = trisurf(G.e2n,G.coorx,G.coory,zeros(length(G.coorx),1),G.region);
  colormap(jet(200));
  axis equal
  view(2);
end

function h = plotMesh(G,varargin)
  h = trisurf(G.e2n,G.coorx,G.coory,zeros(length(G.coorx),1),'FaceColor','white');
  colormap(jet(200));
  axis equal
  view(2);
end

function helpfunc(f,fname)
  
  display(sprintf('\n* Error - unknown plot keyword provided *'));
  display(sprintf('* %s(<struct or filename>, <plotname>, [optional arguments]) *\n',func2str(fname)));
  display('Select among the following plotnames:');
  for i=1:length(f);
    display(sprintf('plotname: %15s \t function: %30s ',f(i).str{1},func2str(f(i).func)));
  end
end
  