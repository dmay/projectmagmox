%
% Visualizes fields
%


function FigureFieldsDMTetSpace(filename,coor,e2n_low,nnodes,space_group,fieldsH,timestep_list)

field_group=space_group.Groups;

sz=size(fieldsH);
ntimesteps = sz(1);
nfields = sz(2);

for n = timestep_list
    if isempty(field_group.Groups)==1
        field_data=field_group.Datasets;
    else
        field_data=field_group.Groups(n).Datasets;
    end
    imcnt = 2;
    for f = 1:nfields
        fieldname = field_data(f).Name;
        if isempty(field_group.Groups)==1
            path = strcat(field_group.Name,'/' , fieldname);
        else
            path = strcat(field_group.Groups(1).Name,'/' , fieldname);
        end
        fieldH = fieldsH{n,f};
        field_L = length(fieldH);
        dof = field_L / nnodes;
        fprintf(1,'  Field[%d]: Loading vector \"%s\" [DOF %d]\n',f,fieldname,dof);
        
        if dof == 1
            figure(imcnt)
            trisurf(e2n_low,coor(:,1),coor(:,2),zeros(length(coor),1),fieldH,...
                'FaceColor','interp','EdgeColor','black');
            title(fieldname);
            colormap(jet(200));
            axis equal
            view(2);
            
            imcnt = imcnt + 1;
        else
            for d = 1:dof
                figure(imcnt)
                trisurf(e2n_low,coor(:,1),coor(:,2),zeros(length(coor),1),fieldH(d:2:end),...
                    'FaceColor','interp','EdgeColor','black');
                title(strcat(fieldname,'[',int2str(d),']'));
                colormap(jet(200));
                axis equal
                view(2);
                
                imcnt = imcnt + 1;
            end
        end
    end
end

end % /function
