%
% Loads a function space defined on a DMTet object
% Additionally any field vectors stored in the file
% are also loaded but not visualized
% Data is returned...
%

%
% Notes:
%  - Could modified the return of the loaded data (element-node map, coordinates, fields)
%

function [coor,e2n_low,nnodes,geometry_group,space_group,fieldsH]=LoadDMTetSpace(filename)
%clear all;
%filename = '/Users/dmay/codes/leasz/dmp.h5';
fprintf(1,'Loading DMTET file \"%s\"\n',filename);

basisorder = h5readatt(filename,'/DMTet','/basis_order');
fprintf(1,'  Found P_%d function space\n',basisorder);

dof = h5readatt(filename,'/DMTet','/dof');
nnodes = h5readatt(filename,'/DMTet/Space','/nnodes');
nelements = h5readatt(filename,'/DMTet/Space','/nelements');
nodes_per_element = h5readatt(filename,'/DMTet/Space','/nodes_per_element');

e2nH = h5read(filename,'/DMTet/Space/map_el_nd');
e2n = zeros(nelements,nodes_per_element);
for j=1:nodes_per_element
    e2n(:,j) = e2nH(j:nodes_per_element:end);
end
% convert from C to matlab indices starting from 1
e2n = e2n + 1;

% re-map high order space to low order space
if basisorder > 1
    fprintf(1,'Re-mapping P_%d function space to P_1 function space\n',basisorder);
    nsub = basisorder * basisorder; % *2 / 2
    nfull = nelements * nsub;
    
    np = basisorder + 1;
    e2n_low = zeros(nfull,3);
    
    ecnt = 1;
    for e=1:nelements
        
        map = zeros(30,30);
        idx = e2n(e,:);
        cnt = 1;
        for j=0:np-1
            for i=0:np-j-1
                map(i+1,j+1) = idx(cnt);
                cnt = cnt + 1;
            end
        end
        
        for j=0:np-1-1
            for i=0:np-j-1-1
                if i == np - j - 2
                    % last triangle
                    e2n_low(ecnt,1) = map(i+1,  j+1);
                    e2n_low(ecnt,2) = map(i+1+1,j+1);
                    e2n_low(ecnt,3) = map(i+1,  j+1+1);
                    ecnt = ecnt + 1;
                else
                    e2n_low(ecnt,1) = map(i+1,  j+1);
                    e2n_low(ecnt,2) = map(i+1+1,j+1);
                    e2n_low(ecnt,3) = map(i+1,  j+1+1);
                    ecnt = ecnt + 1;
                    
                    e2n_low(ecnt,1) = map(i+1+1,j+1);
                    e2n_low(ecnt,2) = map(i+1+1,j+1+1);
                    e2n_low(ecnt,3) = map(i+1,  j+1+1);
                    ecnt = ecnt + 1;
                end
            end
        end % /subelements
    end % /elements
    
else
    e2n_low = e2n;
end

coorH = h5read(filename,'/DMTet/Space/coors');
coor = zeros(nnodes,2);
coor(:,1) = coorH(1:2:end);
coor(:,2) = coorH(2:2:end);

% Load any fields
info = h5info(filename); % query file and put contents into a structure
geometry_group = info.Groups.Groups(1);
space_group = info.Groups.Groups(2);
field_group = space_group.Groups;
if isempty(field_group.Groups)==1
    ntimesteps=1;
    nfields = length(field_group.Datasets);
else
    ntimesteps = length(field_group.Groups);
    nfields = length(field_group.Groups(1).Datasets);
end

fieldsH=cell(ntimesteps,nfields);

for n = 1:ntimesteps
    if isempty(field_group.Groups)==1
        field_data=field_group.Datasets;
    else
        field_data=field_group.Groups(n).Datasets;
    end
    for f = 1:nfields
        fieldname = field_data(f).Name;
        if isempty(field_group.Groups)==1
            path = strcat(field_group.Name,'/' , fieldname);
        else
            path = strcat(field_group.Groups(n).Name,'/' , fieldname);
        end
        fieldsH{n,f} = h5read(filename,path);
    end
end


end % /function
