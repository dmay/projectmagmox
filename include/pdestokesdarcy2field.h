
#if !defined(__PDESTOKESDARCY2FIELD_H)
#define __PDESTOKESDARCY2FIELD_H

#include <petsc.h>
#include <petscdm.h>
#include <pde.h>
#include <coefficient.h>
#include <dmbcs.h>

PetscErrorCode PDEStokesDarcy2FieldSetData(PDE pde,DM dmu,DM dmp,BCList ubc,BCList pbc);
PetscErrorCode PDEStokesDarcy2FieldGetCoefficients(PDE pde,Coefficient *eta,Coefficient *fu,Coefficient *fp,Coefficient *qn);
PetscErrorCode PDEStokesDarcy2FieldGetDM(PDE pde,DM *dms);

PetscErrorCode StkDarcyFluidVelocityProjectU_RHS(PDE pde,DM dms,DM dmu,DM dmp,Vec Xp,Vec F);
PetscErrorCode StkDarcyFluidVelocityProjectP_RHS(PDE pde,DM dms,DM dmu,DM dmp,Vec Xu,Vec Xp,Vec F);
PetscErrorCode StkDarcyDivVelocityProjectP_RHS(PDE pde,DM dms,DM dmu,DM dmp,Vec Xu,Vec Xp,Vec F);

PetscErrorCode PDEStokesDarcy2FieldComputeFluidVelocityU(PDE pde,Vec *_uf);
PetscErrorCode PDEStokesDarcy2FieldComputeFluidVelocityP(PDE pde,Vec *_uf);

#endif
