
#if !defined(__ELEMENTCONTAINER_H)
#define __ELEMENTCONTAINER_H

#include <petsc.h>
#include <petscdm.h>
#include <quadrature.h>
#include <petscdmtet.h>

typedef struct _p_EContainer *EContainer;

struct _p_EContainer {
  Quadrature q;
  DMTetBasisType basistype;
  PetscInt nbasis,nqp;
  PetscReal **N;
  PetscReal **dNr1,**dNr2;
  PetscReal **dNx1,**dNx2;
  
  PetscInt  *buf_basis_index_a;
  PetscInt  *buf_basis_index_b;
  PetscInt  *buf_basis_index_c;
  
  PetscInt  *buf_basis_2index_a;
  PetscInt  *buf_basis_2index_b;
  PetscInt  *buf_basis_2index_c;
  
  PetscReal *buf_basis_scalar_a;
  PetscReal *buf_basis_scalar_b;
  PetscReal *buf_basis_scalar_c;
  
  PetscReal *buf_basis_2vector_a;
  PetscReal *buf_basis_2vector_b;
  PetscReal *buf_basis_2vector_c;
  
  PetscReal *buf_q_scalar_a;
  PetscReal *buf_q_scalar_b;
  PetscReal *buf_q_scalar_c;
};

PetscErrorCode EContainerCreate(DM,Quadrature,EContainer*);
PetscErrorCode EContainerCreate_Generic(DM dm,Quadrature q,EContainer *_e);
PetscErrorCode EContainerDestroy(EContainer*);
PetscErrorCode EContainerGeometryCreate(DM dm,Quadrature q,EContainer *_e);

#endif
