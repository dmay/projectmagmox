
#if !defined(_DMTETRT_H)
#define _DMTETRT_H

#include <petsc.h>
#include <petscdm.h>

PetscErrorCode DMTetGetBasisDOF_RT(DM dm,PetscInt e,PetscInt edof[]);
PetscErrorCode GetBasisSign_RT0(PetscInt gedof[],PetscInt sign[]);
PetscErrorCode EvaluateBasis_RT0(DM dm,PetscInt e,PetscReal ecoor[],PetscReal Nlocal[],PetscReal N[]);
PetscErrorCode EvaluateSolution_RT0(DM dm,PetscInt e,PetscReal ecoor[],PetscReal Nlocal[],PetscReal eval[],PetscReal phi[]);
PetscErrorCode EvaluateBasisDerivative_RT0(DM dm,PetscInt e,PetscReal ecoor[],
    PetscReal dNlocalx[],PetscReal dNx[],PetscReal dNlocaly[],PetscReal dNy[]);
PetscErrorCode EvaluateSolutionGradient_RT0(DM dm,PetscInt e,PetscReal ecoor[],
                                            PetscReal dNlocalx[],PetscReal dNlocaly[],
                                            PetscReal eval[],PetscReal grad_phi[]);

#endif
