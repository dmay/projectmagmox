
#if !defined(__CSLPOLY_H)
#define __CSLPOLY_H

#define CSLPOLY_EPS        1.0e-8
#define CSLPOLY_VEL_EPS    1.0e-12
#define CSLPOLY_MAX_FIELDS 20

#define CSLNodeIdx(s,i,j) ((i) + (j)*((s)->nx[0]))
#define CSLCellIdx(s,i,j) ((i) + (j)*((s)->mx[0]))

typedef enum { COOR_INTERIOR=0, COOR_EXTERIOR, COOR_DEACTIVE, COOR_UNINIT } CoorLocationType;
typedef enum { TAG_GREEN=0, TAG_RED, TAG_BLUE, TAG_YELLOW, TAG_GREY, TAG_UNINIT } TagType;
typedef enum { I_CSPLINE=0, I_CSPLINE_MONOTONE, I_CSPLINE_MONOTONE_BOUNDS, I_WENO4, I_WENO3 } InterpolationType;

typedef struct _p_CSLPoly         *CSLPoly;
typedef struct _p_BoundaryP       *BoundaryP;
typedef struct _p_CellP           *CellP;
typedef struct _p_CharacteristicP *CharacteristicP;

struct _p_BoundaryP {
  PetscReal  coor[2];
  PetscReal  velocity[2];
  PetscReal  normal[2];
  PetscInt   is_inflow; /* 1 = yes, 0 = no */
  PetscInt   containing_cell;
  PetscInt   closest_interior_cell;
  PetscInt   label;
  PetscInt   bcset;
  PetscReal *value;
  PetscReal *ivalue;
};

struct _p_CellP {
  PetscReal        coor[2];
  PetscInt         closest_facet;
  PetscInt         is_interior;
  CoorLocationType type; /* regular, exterior, deactive */
  TagType          tag;
};

struct _p_CharacteristicP {
  PetscInt   arrival_cell;
  PetscReal  arrival_coor[2];
  PetscReal  departure_coor[2];
  PetscInt   departure_cell;
  PetscReal  volume,volume0;
  TagType    tag;
  PetscInt   poly_facet_crossed;
};

struct _p_CSLPoly {
  /* polygon */
  PetscInt   poly_npoints;
  PetscReal *poly_coor;
  PetscInt  *poly_edge_label;
  
  /* facets */
  PetscInt   boundary_nfacets;
  BoundaryP *boundary_facet_list;
  PetscInt  *boundary_facet_offset; /* length poly_npoints + 1 */
  PetscReal *boundary_ds; /* length poly_npoints */
  
  /* target cell size (actual sizes are dx,dy) */
  PetscReal ds,dx[2];
  PetscReal gmin[2],gmax[2];
  PetscReal gmin_input[2],gmax_input[2];
  
  /* vertices */
  PetscInt          nvert,nx[2];
  PetscReal        *coor_v,*vel_v;
  PetscInt         *is_interior_v;
  CoorLocationType *type_v;
  
  /* cells */
  PetscInt ncells,mx[2],subcellmx[2];
  CellP   *cell_list;
  
  PetscReal *value_c,*divu_c;
  PetscReal *ivalue_c;
  
  void (*compute_div_u)(CSLPoly,PetscInt,PetscReal*,PetscReal*);
  
  /* other */
  InterpolationType interp_type;
  PetscInt nfields,nimplicitfields;
  PetscInt ncells_interior;
  PetscInt ncells_exterior;
  PetscInt interpolation_type; // cspline, weno4
  PetscInt characteristics_cache;
  PetscInt characteristic_npoints;
  PetscInt impose_global_value_bounds;
  PetscReal value_bound_min[CSLPOLY_MAX_FIELDS],value_bound_max[CSLPOLY_MAX_FIELDS];
  CharacteristicP *c;
  
  void *ictx;
  void (*ifields_evaluate_at_point)(CSLPoly,PetscReal*,PetscReal*,void*);
  void (*ifields_evaluate_at_boundarypoint)(CSLPoly,PetscInt,PetscReal*,PetscReal*,void*);
};


void CSLPolyCreate(CSLPoly *poly);
void CSLPolySetInterpolateType(CSLPoly p,InterpolationType t);
void CSLPolySetBlockSize(CSLPoly p,PetscInt nfields);
void CSLPolySetApplyGlobalBounds(CSLPoly p);
void CSLPolySetGlobalBounds(CSLPoly p,PetscInt f,PetscReal min,PetscReal max);
void CSLPolySetPolygonPoints(CSLPoly p,PetscInt npoints,PetscReal xy[],PetscInt edge_label[]);
void CSLPolySetDomain(CSLPoly p,PetscReal gmin[],PetscReal gmax[],PetscReal ds);
void CSLPolySetDomainDefault(CSLPoly p,PetscReal ds);
void CSLPolyMarkCells(CSLPoly p);
void CSLPolyMarkVertices(CSLPoly p);
void CSLPolyMarkInflowFacets(CSLPoly p);
void CSLPolySetUpMesh(CSLPoly p);
void CSLPolyLabelFacets(CSLPoly p);
void CSLPolyLabelCells(CSLPoly p);
void CSLPolyBoundaryPCreate(CSLPoly sl);
void CSLPolySetUpFields(CSLPoly p);
void CSLPolyReconstructExteriorCellFields(CSLPoly p);
void CSLPolyReconstructExteriorFacetFields(CSLPoly p);
void CSLPolyComputeCellAverageDivVelocity(CSLPoly s);
void CSLPolyUpdate(CSLPoly sl,PetscReal time,PetscReal dt);
void CSLPolyUpdate_Conservative(CSLPoly sl,PetscReal time,PetscReal dt);
void CSLPolyUpdate_Strang2Multiplicative(CSLPoly csl,PetscReal time,PetscReal dt);
void CSLPolyUpdateState(CSLPoly csl,PetscReal field_kp1[]);
void CSLPolyComputeStateUpdate_Strang2Multiplicative(CSLPoly csl,PetscReal time,PetscReal dt,PetscReal field_kp1[]);
void CSLPolyCellViewVTS(CSLPoly sl,const char fname[]);
void CSLPolyFacetViewVTU(CSLPoly sl,const char fname[]);
void CSLPolyGenericFacetBCIterator(CSLPoly p,PetscInt edge,void (*F)(const PetscReal*,const PetscReal*,const PetscReal *,PetscReal*,void*),void *ctx);
void CSLPolyApplyBC(CSLPoly p,PetscReal time,void (*func)(CSLPoly,PetscReal,void*),void* ctx);
void CSLPolyInitializeBC(CSLPoly p);
void CSLPolyVerifyBC(CSLPoly p);
void CSLPolySetDefaultDivergenceMethod(CSLPoly sl);

void CSLPolyInterpolateAtPoint(CSLPoly s,PetscReal coor[],PetscReal phi_p[]);
void CSLPolyInterpolateAtPointWithArray(CSLPoly s,InterpolationType interp_type,
                                   const PetscReal coor[],
                                   PetscInt nfields,const PetscReal field_c[],
                                   const PetscReal bounds_min[],const PetscReal bounds_max[],
                                   PetscReal field_p[],PetscInt *inside_domain,PetscInt *inside_csl_domain);
void CSLPolyInterpolateAtBoundaryPoint(CSLPoly s,PetscInt poly_facet,PetscReal coor[],PetscReal phi_p[]);

void CSLPolySetSubcellResolution(CSLPoly p,PetscInt mx,PetscInt my);

void CSLPolySetBlockSizeImplicit(CSLPoly p,PetscInt nfields);
void CSLPolySetImplicitFieldEvaluators(CSLPoly p,
                                       void (*ifields_evaluate_at_point)(CSLPoly,PetscReal*,PetscReal*,void*),
                                       void (*ifields_evaluate_at_boundarypoint)(CSLPoly,PetscInt,PetscReal*,PetscReal*,void*),
                                       void *ictx);
void CSLPolyGenericFacetBCIteratorI(CSLPoly p,PetscInt edge,void (*F)(const PetscReal*,const PetscReal*,const PetscReal *,PetscReal*,void*),void *ctx);
void CSLPolyUpdateI(CSLPoly sl,PetscReal time,PetscReal dt);
void CSLPolyUpdateI_Conservative(CSLPoly sl,PetscReal time,PetscReal dt);
void CSLPolyExtrapolateExteriorCellFields(CSLPoly p,PetscInt nfields,PetscReal field_c[]);
void CSLPolyExtrapolateExteriorVertexFields(CSLPoly p,PetscInt nfields,PetscReal field_v[]);
void CSLPolyGetCoordinatesVertex(CSLPoly p,const PetscReal **coor);
void CSLPolyGetCoordinateVertexPoint(CSLPoly p,PetscInt vidx,const PetscReal **coor);
void CSLPolyGetCoordinateCellCentroidPoint(CSLPoly p,PetscInt cidx,const PetscReal **coor);

void CSLPolySetCacheCharacteristics(CSLPoly p);
void CSLPolyGetCachedCharacteristics(CSLPoly p,PetscInt *nchar,CharacteristicP **c);

void CSLPolyGetPointLocationStatus(CSLPoly s,const PetscReal coor[],PetscInt *inside_domain,PetscInt *inside_csl_domain);


#endif
