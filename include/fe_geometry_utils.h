
#if !defined(__FEGEOMUTILS_H)
#define __FEGEOMUTILS_H

#include <petsc.h>


/*
 Reference element
 [2]
 |  \
 |    \
 |      \
 [0] --- [1]
 
 index  coor
 0      (0,0)
 1      (1,0)
 2      (0,1)
 
 */
inline void EvaluateBasis_P1(PetscReal xi[],PetscReal N[])
{
  N[0] = 1.0 - xi[0] - xi[1];
  N[1] = xi[0];
  N[2] = xi[1];
}

inline void EvaluateBasisDerivative_P1(PetscReal xi[],PetscReal GNxi[],PetscReal GNeta[])
{
  GNxi[0] = -1.0;
  GNxi[1] =  1.0;
  GNxi[2] =  0.0;
  
  GNeta[0] = -1.0;
  GNeta[1] =  0.0;
  GNeta[2] =  1.0;
}

inline void EvaluateElementJacobian(PetscInt nbasis,
                                    PetscReal *__restrict el_coords,
                                    PetscReal *__restrict GNIxi,PetscReal *__restrict GNIeta,PetscReal J[2][2])
{
  PetscInt k;
  
  J[0][0] = J[0][1] = 0.0;
  J[1][0] = J[1][1] = 0.0;
  
  for (k=0; k<nbasis; k++) {
    PetscReal xc = el_coords[2*k+0];
    PetscReal yc = el_coords[2*k+1];
    
    J[0][0] += GNIxi[k] * xc;
    J[0][1] += GNIxi[k] * yc;
    
    J[1][0] += GNIeta[k] * xc;
    J[1][1] += GNIeta[k] * yc;
  }
}

inline void EvaluateElementGeometry(PetscInt nqp,PetscInt nbasis,
                                    PetscReal *__restrict el_coords,
                                    PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                    PetscReal *__restrict detJ)
{
  PetscInt q;
  PetscReal J[2][2];
  
  for (q=0; q<nqp; q++) {
    EvaluateElementJacobian(nbasis,el_coords,GNIxi[q],GNIeta[q],J);
    detJ[q] = PetscAbsReal(J[0][0]*J[1][1] - J[0][1]*J[1][0]);
  }
}

inline void EvaluateBasisDerivatives(PetscInt nqp,PetscInt nbasis,
                                     PetscReal *__restrict el_coords,
                                     PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                     PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy)
{
  PetscInt k,q;
  PetscReal J[2][2],iJ[2][2],odJ,dJ;
  
  for (q=0; q<nqp; q++) {
    
    EvaluateElementJacobian(nbasis,el_coords,GNIxi[q],GNIeta[q],J);
    
    dJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
    
    odJ = 1.0/dJ;
    
    iJ[0][0] =  J[1][1] * odJ;
    iJ[0][1] = -J[0][1] * odJ;
    iJ[1][0] = -J[1][0] * odJ;
    iJ[1][1] =  J[0][0] * odJ;
    
    /* basis function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }
  }
}

static inline void EvaluateBasisGeometryDerivatives(PetscInt nqp,PetscInt nbasis,
                                                    PetscReal *__restrict el_coords,
                                                    PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                                    PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy,
                                                    PetscReal *__restrict detJ)
{
  PetscInt k,q;
  PetscReal J[2][2],iJ[2][2],odJ,dJ;
  
  for (q=0; q<nqp; q++) {
    
    EvaluateElementJacobian(nbasis,el_coords,GNIxi[q],GNIeta[q],J);
    
    dJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
    
    odJ = 1.0/dJ;
    
    iJ[0][0] =  J[1][1] * odJ;
    iJ[0][1] = -J[0][1] * odJ;
    iJ[1][0] = -J[1][0] * odJ;
    iJ[1][1] =  J[0][0] * odJ;
    
    /* basis function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }

    detJ[q] = PetscAbsReal(dJ);
  }
}

inline void EvaluateBasisGeometry_Affine(PetscReal *__restrict el_vert,
                                         PetscReal *__restrict detJ)
{
  PetscInt  i,j;
  PetscReal J[2][2],dJ;
  PetscReal _GNIxi[3],_GNIeta[3];
  PetscReal xi[] = {0.0,0.0};
  
  EvaluateBasisDerivative_P1(xi,_GNIxi,_GNIeta);
  EvaluateElementJacobian(3,el_vert,_GNIxi,_GNIeta,J);
  /* note - the funky scaling factor here is required as the reference cells
   in the rest of the code are from [-1,1] whilst EvaluateBasisDerivative_P1() uses [0,1] */
  for (i=0; i<2; i++) { for (j=0; j<2; j++) { J[i][j] *= 0.5; }}
  dJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
  *detJ = PetscAbsReal(dJ);
}

inline void EvaluateBasisDerivatives_Affine(PetscInt nqp,PetscInt nbasis,
                                            PetscReal *__restrict el_vert,
                                            PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                            PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy)
{
  PetscInt k,q,i,j;
  PetscReal J[2][2],iJ[2][2],odJ,dJ;
  PetscReal _GNIxi[3],_GNIeta[3];
  PetscReal xi[] = {0.0,0.0};
  
  EvaluateBasisDerivative_P1(xi,_GNIxi,_GNIeta);
  EvaluateElementJacobian(3,el_vert,_GNIxi,_GNIeta,J);
  /* note - the funky scaling factor here is required as the reference cells
   in the rest of the code are from [-1,1] whilst EvaluateBasisDerivative_P1() uses [0,1] */
  for (i=0; i<2; i++) { for (j=0; j<2; j++) { J[i][j] *= 0.5; }}
  dJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
  
  odJ = 1.0/dJ;
  iJ[0][0] =  J[1][1] * odJ;
  iJ[0][1] = -J[0][1] * odJ;
  iJ[1][0] = -J[1][0] * odJ;
  iJ[1][1] =  J[0][0] * odJ;
  
  for (q=0; q<nqp; q++) {
    /* basis function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }
  }
}

inline void EvaluateBasisGeometryDerivatives_Affine(PetscInt nqp,PetscInt nbasis,
                                                    PetscReal *__restrict el_vert,
                                                    PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                                    PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy,
                                                    PetscReal *__restrict detJ)
{
  PetscInt k,q,i,j;
  PetscReal J[2][2],iJ[2][2],odJ,dJ;
  PetscReal _GNIxi[3],_GNIeta[3];
  PetscReal xi[] = {0.0,0.0};
  
  EvaluateBasisDerivative_P1(xi,_GNIxi,_GNIeta);
  EvaluateElementJacobian(3,el_vert,_GNIxi,_GNIeta,J);
  /* note - the funky scaling factor here is required as the reference cells
   in the rest of the code are from [-1,1] whilst EvaluateBasisDerivative_P1() uses [0,1] */
  for (i=0; i<2; i++) { for (j=0; j<2; j++) { J[i][j] *= 0.5; }}
  dJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
  *detJ = PetscAbsReal(dJ);
  
  odJ = 1.0/dJ;
  iJ[0][0] =  J[1][1] * odJ;
  iJ[0][1] = -J[0][1] * odJ;
  iJ[1][0] = -J[1][0] * odJ;
  iJ[1][1] =  J[0][0] * odJ;
  
  for (q=0; q<nqp; q++) {
    /* basis function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }
  }
}

inline void EvaluateGeometrySurfaceJacobian_Affine(PetscReal *__restrict el_vert_face,
                                         PetscReal *__restrict detJ)
{
  PetscInt  vA,vB;
  PetscReal dx,dy,dl;

  vA = 0;
  vB = 1;
  dx = el_vert_face[2*vB  ] - el_vert_face[2*vA  ];
  dy = el_vert_face[2*vB+1] - el_vert_face[2*vA+1];

  dl = PetscSqrtReal(dx*dx + dy*dy); /* face length */
  
  /* scaling factor here is required as the reference cell is over [-1,1] */
  *detJ = 0.5 * dl;
}

inline void EvaluateBasisSurfaceJacobian_Affine(PetscInt nbasis_face,PetscReal *__restrict el_vert_face,
                                                PetscReal *__restrict detJ)
{
  PetscInt  vA,vB;
  PetscReal dx,dy,dl;
  
  vA = 0;
  vB = nbasis_face - 1;
  dx = el_vert_face[2*vB  ] - el_vert_face[2*vA  ];
  dy = el_vert_face[2*vB+1] - el_vert_face[2*vA+1];
  
  dl = PetscSqrtReal(dx*dx + dy*dy); /* face length */
  
  /* scaling factor here is required as the reference cell is over [-1,1] */
  *detJ = 0.5 * dl;
}

#endif
