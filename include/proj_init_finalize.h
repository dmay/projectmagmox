
#ifndef __proj_init_finalize_h__
#define __proj_init_finalize_h__

#include <petsc.h>

extern char project_output_path[PETSC_MAX_PATH_LEN];

PetscErrorCode projCheckCompilationFlags(PetscViewer v,const char flags[]);
PetscErrorCode projWritePreamble(PetscViewer v);
PetscErrorCode projCreateDirectory(const char dirname[]);
PetscErrorCode projGenerateFormattedTimestamp(char date_time[]);
PetscErrorCode projWriteOptionsFile(const char filename[]);
PetscErrorCode projGetOutputPath(const char **dname);
PetscErrorCode projInitialize(int *argc,char ***args,const char file[],const char help[]);
PetscErrorCode projFinalize(void);

#endif
