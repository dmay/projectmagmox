
#if !defined(_DMTET_DT_H)
#define _DMTET_DT_H

#include <petsc.h>

PetscErrorCode _PetscDTComputeJacobi(PetscReal a, PetscReal b, PetscInt n, PetscReal x, PetscReal *P);

PetscErrorCode _PetscDTComputeJacobiDerivative(PetscReal a, PetscReal b, PetscInt n, PetscReal x, PetscReal *P);

PetscErrorCode _PetscDTFactorial_Internal(PetscInt n, PetscReal *factorial);

PetscErrorCode _PetscDTGaussJacobiQuadrature1D_Internal(PetscInt npoints, PetscReal a, PetscReal b, PetscReal *x, PetscReal *w);

PetscErrorCode _PetscDTGaussJacobiQuadrature(PetscInt dim, PetscInt npoints, PetscReal a, PetscReal b, PetscReal *points[], PetscReal *weights[]);

PetscErrorCode PetscDTMapSquareToTriangle_Internal(PetscReal x, PetscReal y, PetscReal *xi, PetscReal *eta);

PetscErrorCode PetscDTMapCubeToTetrahedron_Internal(PetscReal x, PetscReal y, PetscReal z, PetscReal *xi, PetscReal *eta, PetscReal *zeta);

PetscErrorCode MatComputeConditionNumber(Mat A,PetscReal *cond);

#endif

