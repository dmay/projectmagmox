
#if !defined(__DMSL_H)
#define __DMSL_H

#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>

typedef struct _p_SLDMDA *SLDMDA;
typedef struct _p_DMSemiLagrangian *DMSemiLagrangian;
typedef struct _p_FVStorage *FVStorage;
typedef struct _p_FVSubCellStorage *FVSubCellStorage;
typedef struct _p_DeparturePoint DeparturePoint;

typedef enum { DPOINT_INIT = -1, DPOINT_DEPARTED_DOMAIN, DPOINT_DEPARTED_VALID_MASK, DPOINT_FINALIZED } DPointStatus;

typedef enum { SLTYPE_CLASSIC = 0,
               SLTYPE_FVCONSERVATIVE,
               SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE,
               SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE
} DASLType;

typedef enum { SLLOC_INIT = -1, SLLOC_OUTSIDE, SLLOC_INSIDE } SLLocationType;

//typedef enum { SLCELL_INIT = -1, SLCELL_INTERIOR, SLCELL_EXTERIOR, SLCELL_INTERFACE } SLCellType;
//typedef enum { SLVERTEX_INIT = -1, SLVERTEX_INTERIOR, SLVERTEX_DIRICHLET, SLVERTEX_NEUMANN_CANDIDATE, SLVERTEX_NEUMANN, SLVERTEX_EXTERIOR } SLVertexType;

typedef enum { SLCELL_INIT = -1, SLCELL_BLUE,   /* [0] centroid inside : all 4 vertices inside */
                                 SLCELL_YELLOW, /* [1] centroid inside : at least 1 vertex inside */
                                 SLCELL_ORANGE, /* [2] centroid outside : at least 1 vertex inside */
                                 SLCELL_GREY    /* [3] centroid outside : all 4 vertices outside */
} SLCellType;

typedef enum { SLBCELL_OUTFLOW = -1, SLBCELL_NONE, SLBCELL_INFLOW } SLBCellType;

typedef enum { SLVERTEX_INIT = -1, SLVERTEX_ACTIVE, SLVERTEX_REQUIRES_RECONSTRUCTION, SLVERTEX_DEACTIVE } SLVertexType;

struct _p_DMSemiLagrangian {
  DM     dm;
  Vec    phi_dep,velocity;
  PetscBool trajectory_disable_substep;
  PetscReal trajectory_cfl;
  DASLType  sl_type;
  SLDMDA dactx;
  FVStorage fv;
  PetscInt debug;
  const PetscReal *LA_phi_dep;
  PetscBool setup;
  PetscBool store_departure_points;
  DeparturePoint *dpoint;
  PetscInt       n_departure_points,n_departure_points_allocated;
};

//typedef FVFaceStorage PetscReal[2];
//typedef PetscReal FVFaceStorage;

struct _p_FVSubCellStorage {
  PetscBool valid;
  PetscInt parent_c,parent_ci,parent_cj;
  PetscInt  mx,my;
  PetscInt  *state; /* (-1) un-init; (0) outside; (1) active BC; (2) inside/deactive */
  PetscReal *centroid;
  PetscReal *phi;
  PetscInt  *bc_type; /* delete this member */
  PetscReal *cell_flux,*cell_normal,*cell_velocity; /* delete these members */
};

struct _p_FVStorage {
  DM dmcv;
  PetscInt  face_q_type; /* 0 gauss-legendre, 1 gauss-lobatto */
  PetscInt  face_nqp;
  PetscReal *face_q_w,*face_q_xi;
  
  //PetscInt time_q_type;  /* 0 gauss-legendre, 1 gauss-lobatto */
  //PetscInt time_nqp;
  //PetscReal *time_q_w,*time_q_xi;

  /* dirichlet / neumann bc implementation */
  SLLocationType *vertex_loc_type;
  SLVertexType   *vertex_type;
  SLLocationType *cell_loc_type;
  SLCellType     *cell_type;
  SLBCellType    *bcell_type;

  FVSubCellStorage *subcell;
  
  //PetscInt mn_normal_x[2];
  //FVFaceStorage *normal_x;
  
  //PetscInt mn_normal_y[2];
  //FVFaceStorage *normal_y;
  void *domain_ctx;
  PetscErrorCode (*inside_domain)(PetscReal*,PetscBool*,void*); /* args: coor[], is_inside?, user context */
  void *dirichlet_ctx;
  PetscErrorCode (*dirichlet_evaluator)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*); /* args: coor[], time, constrain?, value, user_context */
};


/* DMDA specific stuff */
typedef enum { DASLInterp_Q1=0, DASLInterp_CSpline, DASLInterp_QMonotone } DASLInterpType;

struct _p_SLDMDA {
  PetscInt  nx,ny,mx,my,nnodes,ncells;
  PetscReal dx,dy;
  PetscReal xrange[2],yrange[2];
  char      bctype[4];
  DASLInterpType interpolation_type;
  PetscScalar *LA_vel;
};

typedef enum { OPOINT_NONE = 0, OPOINT_VERTEX, OPOINT_CENTROID, OPOINT_GENERAL } OriginPointType;

struct _p_DeparturePoint {
  PetscInt        origin;
  OriginPointType origin_type;
  PetscReal       departure_coor[2];
  SLLocationType  type;
};

#define DANodeIdx(s,i,j) ((i) + (j)*((s)->nx))
#define DACellIdx(s,i,j) ((i) + (j)*((s)->mx))

PetscErrorCode DMSemiLagrangianDestroy(DMSemiLagrangian *_sl);
PetscErrorCode DMDASemiLagrangianCreate(MPI_Comm comm,PetscInt M,PetscInt N,PetscInt dof,
                                        PetscReal xr[],PetscReal yr[],DMSemiLagrangian *_sl);


/* should be private */
PetscErrorCode DMSemiLagrangianUpdate_DMDA(DMSemiLagrangian dsl,PetscReal dt,Vec phi);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_v0(DMSemiLagrangian dasl,PetscReal dt,Vec phi);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_v2(DMSemiLagrangian dasl,PetscReal dt,Vec phi);
PetscErrorCode DMDASemiLagrangianUpdate(DMSemiLagrangian dasl,PetscReal time,PetscReal dt,Vec phi);

PetscErrorCode DMDASemiLagrangianSetType(DMSemiLagrangian sl,DASLType type);
PetscErrorCode DMDASemiLagrangianCreateGlobalVector(DMSemiLagrangian sl,Vec *X);
PetscErrorCode DMDASemiLagrangianGetVertexDM(DMSemiLagrangian sl,DM *dm);
PetscErrorCode DMDASemiLagrangianGetSLDM(DMSemiLagrangian sl,DM *dm);
PetscErrorCode DMDASemiLagrangianGetVelocity(DMSemiLagrangian sl,Vec *v);
PetscErrorCode DMSemiLagrangianGetSolution(DMSemiLagrangian sl,Vec *v);
PetscErrorCode DMDASemiLagrangianInterpolateGetAccess(DMSemiLagrangian dasl);
PetscErrorCode DMDASemiLagrangianInterpolate(DMSemiLagrangian dasl,PetscReal coor[],PetscReal *val);
PetscErrorCode DMDASemiLagrangianInterpolateAtPoints(DMSemiLagrangian dasl,PetscInt np,PetscReal coor[],PetscReal val[]);
PetscErrorCode DMDASemiLagrangianInterpolateRestoreAccess(DMSemiLagrangian dasl);
PetscErrorCode DMDASemiLagrangianIterator(DMSemiLagrangian sl,Vec x,PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);
PetscErrorCode SLDMView(DMSemiLagrangian sl,const char prefix[]);
PetscErrorCode ConservativeSLView(DMSemiLagrangian sl,const char prefix[]);
PetscErrorCode DMSLSetup(DMSemiLagrangian sl);

PetscErrorCode DMDASemiLagrangian_FVConservative_SetDomainIdentifier(DMSemiLagrangian sl,PetscErrorCode (*func)(PetscReal*,PetscBool*,void*),void*);
PetscErrorCode DMDASemiLagrangian_FVConservative_SetDirichletEvaluator(DMSemiLagrangian sl,PetscErrorCode (*func)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*),void *ctx);

/*
// private
PetscErrorCode DMDASemiLagrangian_FVConservative_DirichletEvaluate(DMSemiLagrangian sl,PetscReal time);
PetscErrorCode DMDASemiLagrangian_FVConservative_RestrictVelocity(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_ReconstructVelocity(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_SetUpBoundary(DMSemiLagrangian sl);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify(DMSemiLagrangian dasl);
*/

PetscErrorCode DPointCreate(PetscInt n,OriginPointType otype,DeparturePoint **p);
PetscErrorCode DPointDestroy(DeparturePoint **p);
PetscErrorCode DPointReset(PetscInt n,OriginPointType otype,DeparturePoint p[]);
PetscErrorCode DMSemiLagrangianGetDeparturePoints(DMSemiLagrangian sl,PetscInt *n,DeparturePoint *p[]);
PetscErrorCode DMSemiLagrangianSetStoreDeparturePoints(DMSemiLagrangian sl,PetscBool value);
PetscErrorCode DMSemiLagrangianGetStoreDeparturePoints(DMSemiLagrangian sl,PetscBool *value);

#endif
