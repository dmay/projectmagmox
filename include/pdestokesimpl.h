
#if !defined(__PDESTOKESIMPL_H)
#define __PDESTOKESIMPL_H

#include <petsc.h>
#include <petscdm.h>
#include <coefficient.h>
#include <dmbcs.h>

typedef struct {
  DM          dmu,dmp,dms;
  BCList      u_dirichlet;
  Coefficient eta;
  Coefficient fu,fp;
  Coefficient q_N;
  
  PetscInt mu,mp,Mu,Mp; /* sizes of the local/global velocity/pressure spaces */
  Quadrature ref;
  PetscInt nubasis,npbasis;
  PetscReal **Nu,**dNudxi,**dNudeta,**dNudx,**dNudy,**Np; /* quadrature tabulations for A12/A21 operators */
} PDEStokes;

#endif

