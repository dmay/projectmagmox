
#if !defined(__BATCHELOR_CFLOW_H)
#define __BATCHELOR_CFLOW_H

void evaluate_BatchelorCornerFlowSoln(double alpha,double origin[],double coor[],double vel[]);

#endif

