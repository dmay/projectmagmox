
#if !defined(__INDEXUTILS_H)
#define __INDEXUTILS_H

#define matrixIJ(i,j,N) (i)*(N) + (j)


/*
 FIELD_ID: Manage indices related to fields with multiple DOFs.
 
 cx = coord[3*i+0];
 cy = coord[3*i+1];
 cz = coord[3*i+2];
 
 Can be replaced with
 
 cx = coord[fieldID(i,0,3)];
 cy = coord[fieldID(i,0,3)];
 cz = coord[fieldID(i,0,3)];
*/
#define FIELD_ID(i,d,ndof) (ndof)*(i) + (d)


/*
 FIELD_GET_VALS2/FIELD_GET_VALS3: Manage extracting values from a strided array.
 
 cx = coord[3*i+0];
 cy = coord[3*i+1];
 cz = coord[3*i+2];
 
 Can be replaced with
 
 PetscReal cx[3];
 
 FIELD_GET_VALS3(coord,i,cx);
*/
#define FIELD_GET_VALS2(field,i,val) { (val)[0] = (field)[2*(i)+0]; (val)[1] = (field)[2*(i)+1]; }
#define FIELD_GET_VALS3(field,i,val) { (val)[0] = (field)[3*(i)+0]; (val)[1] = (field)[3*(i)+1]; (val)[0] = (field)[3*(i)+2];}

#define FIELD_SET_VALS2(val,field,i) { (field)[2*(i)+0] = (val)[0]; (field)[2*(i)+1] = (val)[1]; }
#define FIELD_SET_VALS3(val,field,i) { (field)[3*(i)+0] = (val)[0]; (field)[3*(i)+1] = (val)[1]; (field)[3*(i)+2] = (val)[0];}


/*
 FIELD_GET_OFFSET: Field/element quantity get memory offset
*/
#define FIELD_GET_OFFSET(coeff,e,nitems,ncomponents,coeff_e) { (coeff_e) = &(coeff)[ (ncomponents)*((e)*(nitems)) ];  }

#endif
