
#if !defined(__MATNESTUTILS_H)
#define __MATNESTUTILS_H

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>

PetscErrorCode MatNestConfigureFusedISGetSubMatrix(Mat B);
PetscErrorCode MatNestAssembly(Mat B);
PetscErrorCode DMCompositeFillMatNest(Mat B);

#endif
