
#if !defined(__PETSCDMTET_H)
#define __PETSCDMTET_H

#include <petscdm.h>
#include <petscdmdatypes.h>

#if defined(PETSC_HAVE_HDF5)
#include "hdf5.h"
#endif

/* implementation specific definitions */
#define DMTET "tet" 

typedef enum { DMTET_UINIT=0, DMTET_CLEGENDRE, DMTET_CFETEKE, DMTET_DG, DMTET_CWARP_AND_BLEND, DMTET_RAVIART_THOMAS } DMTetBasisType;

typedef enum { MREADER_TRIANGLE=0, MREADER_TETGEN, MREADER_DIFFPACK, MREADER_GAMBIT, MREADER_T3D, MREADER_GENERATED, MREADER_HDF5, MREADER_PARTIONEDHDF5, MREADER_BASIC } MeshReaderType;

typedef enum { DMTET_UNINITIALIZED = 0, DMTET_CELL, DMTET_EDGE, DMTET_VERTEX, DMTET_BASIS, DMTET_UNKNOWN_PRIMITIVE } DMTetPrimitiveType;

extern const char *DMTetBasisTypeNames[];
extern const char *MeshReaderTypeNames[];

typedef struct _p_DMTetVertex *DMTetVertex;
typedef struct _p_DMTetFacet  *DMTetFacet;
typedef struct _p_DMTetCell   *DMTetCell;

struct _p_DMTetVertex {
  DM        dm;
  PetscReal lives_on_boundary;
  PetscReal normal[2];
  PetscReal tangent[2];
  PetscReal coor[2];
  PetscInt  vertex_index;
};

struct _p_DMTetFacet {
  DM        dm;
  PetscReal lives_on_boundary;
  PetscReal normal[2];
  PetscReal tangent[2];
  PetscReal x0[2],x1[2];
  PetscInt  facet_index;
};

struct _p_DMTetCell {
  DM        dm;
  PetscReal coor_centroid[2];
  PetscInt  cell_index;
};


#include <dmtet_rt.h>
#include <dmtet_bfacet.h>


/* implementation specific prototypes */
PetscErrorCode DMTetCreate(MPI_Comm comm, DM *dm);

PetscErrorCode DMTetSetBasisType(DM dm,DMTetBasisType type);
PetscErrorCode DMTetSetDof(DM dm,PetscInt dof);
PetscErrorCode DMTetSetBasisOrder(DM dm,PetscInt order);


PetscErrorCode DMTetGetBasisType(DM dm,DMTetBasisType *a);
PetscErrorCode DMTetGetDof(DM dm,PetscInt *a);
PetscErrorCode DMTetGetBasisOrder(DM dm,PetscInt *a);
PetscErrorCode DMTetGeometryElement(DM dm,PetscInt *nen,PetscInt *bpe,PetscInt **element,PetscInt *npe,PetscInt *nnodes,PetscReal **coords);
PetscErrorCode DMTetSpaceElement(DM dm,PetscInt *nen,PetscInt *bpe,PetscInt **element,PetscInt *npe,PetscInt *nnodes,PetscReal **coords);
PetscErrorCode DMTetSpaceGetElementDOF(DM dm,PetscInt e,PetscInt **elementdof);
PetscErrorCode DMTetGetBoundingBoxLocal(DM dm,PetscReal min[],PetscReal max[]);
PetscErrorCode DMTetGetBoundingBox(DM dm,PetscReal gmin[],PetscReal gmax[]);
PetscErrorCode DMTetGetMinBasisSeparationLocal(DM dm,PetscReal *gmin);
PetscErrorCode DMTetGetMinBasisSeparation(DM dm,PetscReal *gmin);
PetscErrorCode DMTetGetMinCoordSeparationLocal(DM dm,PetscReal *gmin);
PetscErrorCode DMTetGetMinCoordSeparation(DM dm,PetscReal *gmin);

PetscErrorCode DMTetGeometryGetAttributes(DM dm,PetscInt *nen,PetscInt **el_tag,PetscInt **node_tag);
PetscErrorCode DMTetSpaceGetAttributes(DM dm,PetscInt *nen,PetscInt **el_tag,PetscInt **node_tag);

PetscErrorCode DMTetView_HDF(DM dm,const char filename[]);
PetscErrorCode DMTetFieldSpaceView_HDF(DM dm,Vec field,const char fieldname[],const char filename[]);
PetscErrorCode DMTetSpaceAddGroupNameHDF5(const char filename[],const char groupname[]);


/* 2d prototypes */
PetscErrorCode DMTetCreate2dFromTriangle(MPI_Comm comm,const char filename[],PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm);
PetscErrorCode DMTetCreate2dSquareGeometry(MPI_Comm comm,PetscReal x0,PetscReal x1,PetscReal y0,PetscReal y1,PetscInt mx,PetscInt my,PetscBool right_oriented,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm);

PetscErrorCode DMTetCreateGeometryFromTriangle(DM dm,const char name[]);
PetscErrorCode DMTetCreateSquareGeometry(DM dm,PetscReal x0,PetscReal x1,PetscReal y0,PetscReal y1,PetscInt mx,PetscInt my,PetscBool right_oriented);

PetscErrorCode DMTetGenerateBasis2d(DM dm);
PetscErrorCode DMTetScaleCoordinates2d(DM dm,PetscReal xfac,PetscReal yfac);

PetscErrorCode DMTetGeometryView2d_VTK(DM dm,const char name[]);
PetscErrorCode DMTetBasisView2d_VTK(DM dm,const char name[]);
PetscErrorCode DMTetView_2d(DM dm,PetscViewer viewer);

PetscErrorCode DMTetComputeApproxCentroid2d(PetscReal coor[],PetscReal cx[]);
PetscErrorCode DMTetGeometryComputeElementDiameterBounds2d(DM dm,PetscReal *_hmin,PetscReal *_hmax);
PetscErrorCode DMTetGeometryComputeEnclosedSphereBounds2d(DM dm,PetscReal hbounds[]);
PetscErrorCode DMTetGeometryComputeElementDiameter2d(DM dm,PetscInt e,PetscReal *_hmin,PetscReal *_hmax);

PetscErrorCode DMTetSpaceGetSize(DM dm,PetscInt *M,PetscInt *dof);
PetscErrorCode DMTetSpaceGetLocalSize(DM dm,PetscInt *m,PetscInt *dof);
PetscErrorCode DMTetSpaceGetSubdomainSize(DM dm,PetscInt *m,PetscInt *dof);

PetscErrorCode DMTetTabulateBasisDerivatives(DM dm,PetscInt nqp,PetscReal xi[],
                                             PetscInt *nbasis,PetscReal ***GNix,PetscReal ***GNiy,PetscReal ***GNiz);
PetscErrorCode DMTetTabulateBasis(DM dm,PetscInt nqp,PetscReal xi[],
                                  PetscInt *nbasis,PetscReal ***Ni);

PetscErrorCode DMTetViewFields_VTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char prefix[]);
PetscErrorCode DMTetViewFields_VTU_b(DM dm,PetscInt nf,Vec field[],const char *fname[],const char prefix[]);
PetscErrorCode DMTetViewFields_PVTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char prefix[]);
PetscErrorCode DMTetViewFields_PVTU2(DM dm,PetscInt nf,Vec field[],const char *fname[],const char path[],const char prefix[],const char relPathToSource[]);
PetscErrorCode DMTetViewFieldsPV(DM dm,PetscInt nf,Vec field[],PetscBool binary,const char *fname[],const char prefix[]);
PetscErrorCode DMTetViewFieldsXDMF(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char prefix[]);
PetscErrorCode DMTetViewFieldsXDMFBinary(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char path[],const char prefix[],PetscBool use_common_mesh_file);
PetscErrorCode DMTetViewMeshTopologyXDMFBinary(DM dm,PetscReal time,const char path[],const char prefix[]);
PetscErrorCode DMTetViewMeshTopologyCommonXDMFBinary(DM dm,const char path[]);

PetscErrorCode DMTetCreateSubmeshByRegions(DM dm,PetscInt nr,PetscInt rlist[],DM *_sub);
PetscErrorCode DMTetSubmeshByRegions_CreateElementMaps(DM dm,PetscInt nr,PetscInt rlist[],PetscInt *L_p2s,PetscInt **_p2s,PetscInt *L_s2p,PetscInt **_s2p);

PetscErrorCode DMTetVecTraverseByRegionId(DM dm,Vec x,PetscInt regionid,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,void*),void *ctx);
PetscErrorCode DMTetVecTraverse(DM dm,Vec x,PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);
PetscErrorCode DMCreateInterpolationFromCoords(DM dmF,PetscInt bs,PetscInt npoints,const PetscScalar LA_coorT[],PetscBool interp_error,Mat *mat);
PetscErrorCode DMTetCreateRefinement(DM dmc,PetscInt ref_type,DM *_dmf);
PetscErrorCode DMTetCreateMixedSpaceMatrix(DM dm_w,DM dm_u,Mat *A);
PetscErrorCode DMTetCloneGeometry(DM dmc,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dmf);

PetscErrorCode DMGListCreate(DM dm,PetscInt id,const char labelName[]);
PetscErrorCode DMGListReset(DM dm,const char labelName[]);
PetscErrorCode DMGListView(DM dm);
PetscErrorCode DMGListGetIndices(DM dm,const char labelName[],PetscInt *len,const PetscInt *ix[]);
PetscErrorCode DMGListGetPrimitiveType(DM dm,const char labelName[],DMTetPrimitiveType *type);
PetscErrorCode DMGListGetIsSetUp(DM dm,const char labelName[],PetscBool *issetup);

PetscErrorCode DMTetBFacetListAssign_Default(DM dm,const char set_name[]);
PetscErrorCode DMTetBFacetListAssign_User(DM dm,const char set_name[],PetscErrorCode (*mark)(DMTetFacet,void*,PetscBool*),void *ctx);
PetscErrorCode DMTetBFacetBasisListAssign(DM dm,const char facet_set_name[],const char basis_facet_set_name[]);
PetscErrorCode DMTetBFacetBasisListAssignFromInflow(DM dm,const char set_name[],DM dm_velocity,Vec velocity);
PetscErrorCode DMTetBFacetBasisListAssignFromInflowSubset(DM dm,const char name_from[],const char name_to[],DM dm_velocity,Vec velocity);
PetscErrorCode DMTetGListMerge(DM dm,PetscInt nfrom,const char *name_from[],const char name_to[]);

PetscErrorCode DMTetCreatePartitioned(MPI_Comm comm,DM dm,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *dmp);

PetscErrorCode DMTetCreatePartitionedFromTriangle(MPI_Comm comm,const char filename[],PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm);
PetscErrorCode DMTetCreatePartitionedSquareGeometry(MPI_Comm comm,
                                                    PetscReal x0,PetscReal x1,PetscReal y0,PetscReal y1,
                                                    PetscInt mx,PetscInt my,
                                                    PetscBool right_oriented,
                                                    PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm);

PetscErrorCode DMTetShallowCopyGeometry(DM dmc,DM dm);
PetscErrorCode DMTetShallowCopySpace(DM dmc,DM dm);
PetscErrorCode DMTetCreateSharedGeometry(DM dmc,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dmf);
PetscErrorCode DMTetCreateSharedSpace(DM dmc,PetscInt dof,DM *_dmf);

PetscErrorCode DMTetCreateGlobalCoordinateVector(DM dm,Vec *c);

PetscErrorCode DMTetCreateInjectionScalarIS(DM dmTo,DM dmFrom,IS *isTo,IS *isFrom);

/* general purpose preallocator methods */
PetscErrorCode FARCMatPreallocationInit(Mat A);
PetscErrorCode FARCMatPreallocationSetValuesLocal(Mat A,PetscInt nr,PetscInt rows[],PetscInt nc,PetscInt cols[]);
PetscErrorCode FARCMatPreallocationSetValues(Mat A,PetscInt nr,PetscInt rows[],PetscInt nc,PetscInt cols[]);
PetscErrorCode FARCMatPreallocationFlush(Mat A);
PetscErrorCode FARCMatPreallocationInsertZeros(Mat A);
PetscErrorCode FARCMatPreallocationFinalize(Mat A);

PetscErrorCode FARCMatPreallocation(Mat A,DM dmw,DM dmu);


#endif

