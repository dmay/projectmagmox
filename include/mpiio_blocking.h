
#ifndef __mpiio_blocking_h__
#define __mpiio_blocking_h__

#include <petsc.h>

PetscErrorCode MPIWrite_Blocking(FILE *fp,void *data,long int len,size_t size,int root,PetscBool skip_header,MPI_Comm comm);
PetscErrorCode MPIRead_Blocking(FILE *fp,void **data,long int len,size_t size,int root,PetscBool skip_header,MPI_Comm comm);
PetscErrorCode MPIWrite_Blocking_PetscReal(FILE *fp,PetscReal data[],long int len,int root,PetscBool skip_header,PetscBool binary,MPI_Comm comm);
PetscErrorCode MPIWrite_Blocking_PetscInt(FILE *fp,PetscInt data[],long int len,int root,PetscBool skip_header,PetscBool binary,MPI_Comm comm);
PetscErrorCode MPIWriteUser_Blocking_PetscReal(FILE *fp,PetscReal data[],long int len,int root,
                                               PetscBool skip_header,PetscBool binary,MPI_Comm comm,
                                               PetscErrorCode (*user_writer)(FILE*,PetscReal*,PetscInt,void*),void *ctx);
PetscErrorCode MPIWriteUser_Blocking_PetscInt(FILE *fp,PetscInt data[],long int len,int root,
                                              PetscBool skip_header,PetscBool binary,MPI_Comm comm,
                                              PetscErrorCode (*user_writer)(FILE*,PetscInt*,PetscInt,void*),void *ctx);
#endif
