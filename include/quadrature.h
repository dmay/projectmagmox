
/*
 
 Quadrature defines a small object used to define quadrature rules; specifically
 they contain a set of weights and abscissa used to evaluate the intergrand.
 The Quadrature object also contain a mechanism to store unique values on each point.
 
 Notes:
 
 * Volume and surface quadrature rules are supported.
 
*/

#if !defined(_QUADRATURE_H)
#define _QUADRATURE_H

#include <petsc.h>
#include <petscdm.h>

typedef enum { QUADRATURE_LINE = 0, QUADRATURE_SURFACE, QUADRATURE_VOLUME, QUADRATURE_UNDEFINED } QuadratureGeometryType;
typedef enum { GAUSS_LEGENDRE_TENSOR = 0, GAUSS_LEGENDRE_WARPED_TENSOR } QuadratureType;

typedef struct _p_Quadrature *Quadrature;
typedef struct _p_VQuadraturePoint* VQuadraturePoint;
typedef struct _p_SQuadraturePoint* SQuadraturePoint;


typedef enum { GENERIC_POINT=-1, V_QUADRATURE_POINT, S_QUADRATURE_POINT,
               MESH_BASIS_POINT, CELL_CENTROID_POINT,
               UNINITIALIZED_POINT } EvaluationPointType;
typedef struct _p_EvaluationPoint* EvaluationPoint;

PetscErrorCode QuadratureCreate(Quadrature*);
PetscErrorCode QuadratureDestroy(Quadrature*);
PetscErrorCode QuadratureSetParameters(Quadrature,PetscInt,QuadratureGeometryType,QuadratureType,PetscInt); /* dim, geometry, type, order */
PetscErrorCode QuadratureSetProperty(Quadrature q,PetscInt nelements,const char qfieldname[],PetscInt ncomponents);
PetscErrorCode QuadratureSetUpFromDM(Quadrature q,DM dm);
PetscErrorCode QuadratureSetUpFromDM_Facet(Quadrature q,DM dm);
PetscErrorCode QuadratureSetUp(Quadrature q);
PetscErrorCode QuadratureGetRule(Quadrature q,PetscInt *n,PetscReal *xi[],PetscReal *w[]);

PetscErrorCode QuadratureGetProperty(Quadrature q,const char qfieldname[],PetscInt *nelements,PetscInt *nqp,PetscInt *ncomponents,PetscReal **data);
PetscErrorCode QuadratureGetElementValues(Quadrature q,PetscInt e,PetscInt ncomponents,PetscReal *qdata,PetscReal **qdata_e);

PetscErrorCode DMTetQuadratureCreate_2d(DM dm,PetscInt *_nqp,PetscReal **_w,PetscReal **_xi);

PetscErrorCode DMTetQuadratureRescaleToUnitTriangle(DM dm,PetscInt nqp,PetscReal w[],PetscReal xi[]);

PetscErrorCode QuadratureSetValues(Quadrature q,PetscInt ncomponents,PetscReal vals[],PetscReal *qdata);

PetscErrorCode QuadratureGetSizes(Quadrature q,PetscInt *d,PetscInt *o,PetscInt *n,PetscInt *e);
PetscErrorCode QuadratureGetGeometryType(Quadrature q,QuadratureGeometryType *type);
PetscErrorCode QuadratureComputeCoordinates(Quadrature q,DM dm);

PetscErrorCode EvaluationPointCreate(EvaluationPoint *_p);
PetscErrorCode EvaluationPointDestroy(EvaluationPoint *_p);
PetscErrorCode EvaluationPointSetup(EvaluationPoint p,EvaluationPointType type,PetscBool has_coords,PetscBool has_orientation,PetscInt values);
PetscErrorCode EvaluationPointReset(EvaluationPoint p);
PetscErrorCode SurfaceQuadratureIterator(Quadrature quadrature,DM dm,const char fieldname[],PetscReal time,PetscErrorCode (*fp_evaluator)(EvaluationPoint,PetscReal,PetscReal*,void*),void *ctx);
#endif
