
#if !defined(__PETSCUTILS_H)
#define __PETSCUTILS_H

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>

/* matnest_utils.c */
#include <matnest_utils.h>

/* pointwise.c */
PetscErrorCode VecPointwiseMultBS(Vec x,Vec y,Vec z);

/* mat_get_operation.c */
PetscErrorCode MatGetOperation(Mat mat,MatOperation op,void(**f)(void));

/* mat_duplicate_diag.c */
PetscErrorCode MatCreateDiagonal(Mat mat,MatType mtype,Mat *D);
PetscErrorCode MatDuplicateDiagonal(Mat mat,MatType mtype,MatDuplicateOption op,Mat *D);
PetscErrorCode MatCreateDiagonalFromVec(Vec vec,MatType mtype,MatDuplicateOption op,Mat *D);
PetscErrorCode MatDuplicateDiagonalReciprocal(Mat mat,MatType mtype,Mat *D);
PetscErrorCode MatCreateDiagonalFromVecReciprocal(Vec vec,MatType mtype,Mat *D);

/* pc_auxspace.c */
PetscErrorCode PCAuxSpaceSetDMs(PC pc,DM dm1,DM dm2);
PetscErrorCode PCAuxSpaceSetProjection(PC pc,Mat P);
PetscErrorCode PCAuxSpaceSetOperators(PC pc,Mat A,Mat B);
PetscErrorCode PCAuxSpaceGetOperators(PC pc,Mat *A,Mat *B);
PetscErrorCode PCAuxSpaceGetProjection(PC pc,Mat *P);

/* nullspace.c */
PetscErrorCode MatNullSpaceCreate_Constant(Vec x,MatNullSpace *nullspace);
PetscErrorCode NullSpaceConstant_SetVec(Vec x);
PetscErrorCode DMComposite_NullSpaceConstant_SetVec(DM dm,Vec constant,PetscInt nc,PetscInt const_fields[]);
PetscErrorCode SaddlePointSetConstantNullSpace_SNES(SNES snes);

/* matscatter_ops.c */
PetscErrorCode MatScatterSetUseInsertValues(Mat A,PetscBool value);

#endif

