
/*
 
 The PDE object is essentially a helper container to assist with
 * stroring shared coefficients
 * defining SNES objects
 * hooking up one syetem of equations with another
 
 Notes:
 
 * The user must call PDESetSolution().
 
 * PDEUpdateLocalSolution() will be called automatically from inside
 the (i) form function, or (ii) form jacobian functions.
 
 This mechanism will avoid having to call DMGlobalToLocal() multiple 
 times for different coefficients which might utilize the same fields.
 e.g. the iscosity depends on temperature, as might the right-hand-side.
 Both coefficients are related to PDEStokes.
 
*/

#if !defined(__PDE_H)
#define __PDE_H

#include <petsc.h>

PETSC_EXTERN PetscClassId PDE_CLASSID;

typedef struct _p_PDE *PDE;

typedef enum { PDEUNINIT = 0, PDESTOKES, PDEHELMHOLTZ, PDEADVDIFF, PDESTKDCY2F, PDEDARCY } PDEType;
extern const char *PDETypeNames[];

typedef enum { PRJTYPE_NATIVE = 0, PRJTYPE_GRADIENT, PRJTYPE_DIVERGENCE } ProjectorType;

#define PDE_MAX_COMMON 20

#include <coefficient.h>

PetscErrorCode PDECreate(MPI_Comm,PDE*);
PetscErrorCode PDEDestroy(PDE*);
PetscErrorCode PDEView(PDE,PetscViewer);
PetscErrorCode PDESetOptionsPrefix(PDE pde,const char prefix[]);

PetscErrorCode PDESetType(PDE,PDEType);
PetscErrorCode PDESetSolution(PDE,Vec);
PetscErrorCode PDESetLocalSolution(PDE,Vec);
PetscErrorCode PDESetLocalSolutions(PDE pde,PetscInt n,Vec x[]);
PetscErrorCode PDESetSharedPDE(PDE,PDE);
//PetscErrorCode PDEAttachCommonCoefficient(PDE,Coefficient);

PetscErrorCode PDEGetSolution(PDE,Vec*);
PetscErrorCode PDEGetLocalSolutions(PDE pde,PetscInt *n,Vec x[]);

PetscErrorCode PDEUpdateLocalSolution(PDE);
PetscErrorCode PDEConfigureSNES(PDE,SNES,Vec,Mat,Mat);
PetscErrorCode PDESNESSolve(PDE pde,SNES snes);

/* generic thing used by prohections */
PetscErrorCode AssembleBForm_VectorMassMatrixMAIJ(DM dm,PetscInt bs,MatReuse reuse,Mat *_A);
PetscErrorCode AssembleBForm_VectorMassMatrix(DM dm,PetscInt ndof,MatReuse reuse,Mat *_A);

PetscErrorCode DMTetProjectField(DM dmF,Vec vF,ProjectorType type,DM dmT,Vec *_vT);
PetscErrorCode DMTetProjectQuadratureField(Quadrature quadrature,const char field[],DM dmT,Vec *_vT);
PetscErrorCode AssembleBForm_VectorMixedMassMatrix(DM dm_w,DM dm_u,PetscInt ndof,Mat A);
PetscErrorCode AssembleBForm_VectorMassMatrixCellWise(DM dm,PetscInt ndof,MatReuse reuse,Mat *_A);
PetscErrorCode DMTetAssembleProjection(DM dmF,DM dmT,Mat *_P);

#endif
