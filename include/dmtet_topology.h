
#if !defined(__PETSCDMTET_TOPO_H)
#define __PETSCDMTET_TOPO_H

#include <petsc.h>
#include <dmtetimpl.h>

PetscErrorCode FEGTopologyCreate(FEGTopology *_t);
PetscErrorCode FEGTopologyDestroy(FEGTopology *_t);

PetscErrorCode FEGTopologySetup_E2E(FEGTopology t,DM_TET *dmtetctx);
PetscErrorCode FEGTopologySetup_V2V(FEGTopology t,DM_TET *dmtetctx);
PetscErrorCode FEGTopologySetup_V2E(FEGTopology t,DM_TET *dmtetctx);
PetscErrorCode DMTetTopologySetup(DM dm,PetscInt ttype);
PetscErrorCode DMTetGetFEGTopology(DM dm,FEGTopology *topo);

#endif
