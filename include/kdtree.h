
#if !defined(__PETGS_KDTREE_H)
#define __PETGS_KDTREE_H

typedef struct _p_kd_node_t* kd_node;
typedef struct _p_KDTree *KDTree;
struct _p_KDTree {
  PetscInt  npoints;
  kd_node   root,point;
  PetscInt  dim;
  PetscInt  visited;
  PetscInt  setup;
};

PetscErrorCode KDTreeCreate(PetscInt npoints,PetscInt dim,KDTree *_k);
PetscErrorCode KDTreeDestroy(KDTree *_k);
PetscErrorCode KDTreeInsertPoint(KDTree k,PetscInt index,PetscReal coor[]);
PetscErrorCode KDTreeSetup(KDTree kt);
PetscErrorCode KDTreeReset(KDTree kt);
PetscErrorCode KDTreeGetPoint(KDTree k,PetscInt index,kd_node *node);
PetscErrorCode KDTreeDistanceQuery(KDTree k,PetscReal coor[],PetscInt *nearest,PetscReal *sep);
PetscErrorCode KDTreeNDistanceQuery(KDTree k,PetscInt npoints,PetscReal coor[],PetscInt nearest[]);

#endif
