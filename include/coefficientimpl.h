
#if !defined(__COEFFICIENTIMPL_H)
#define __COEFFICIENTIMPL_H

#include <petsc.h>
#include <quadrature.h>
#include <pde.h>
#include <coefficient.h>

struct _p_Coefficient {
  CoefficientType type;

  /* value, data and methods for type = "domain_const" */
  PetscBool constant_set_by_user;
  PetscReal domain_const;
  void *domain_const_ctx;
  PetscErrorCode (*evaluate_domain_const)(Coefficient,PetscReal*,void*);
  PetscErrorCode (*destroy_user_ctx)(Coefficient);

  /* data and methods for type = "quadrature" */
  Quadrature quadrature; // can be volume or surface quadrature rule
  void *quadrature_ctx;
  PetscErrorCode (*evaluate_quadrature)(Coefficient,Quadrature,PetscInt*,void*);
  //PetscErrorCode (*destroy_quadrature)(Coefficient);
  PDE pde;
  PetscInt *region; /* element region list : shared pointer - not to be freed */
};


#endif
