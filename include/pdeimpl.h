
#if !defined(__PDEIMPL_H)
#define __PDEIMPL_H

#include <petsc.h>
#include <petsc/private/petscimpl.h>

#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>

#include <pde.h>
#include <coefficient.h>

#define MAX_VECS 10

typedef struct _PDEOps *PDEOps;
struct _PDEOps {
  PetscErrorCode (*configure_snes)(PDE,SNES,Vec,Mat,Mat);
  PetscErrorCode (*create)(PDE);
  PetscErrorCode (*update_local_solution)(PDE);
  PetscErrorCode (*form_function)(SNES,Vec,Vec,void*);
  PetscErrorCode (*form_jacobian)(SNES,Vec,Mat,Mat,void*);
  PetscErrorCode (*destroy)(PDE);
  PetscErrorCode (*view)(PDE,PetscViewer);
};

struct _p_PDE {
  PETSCHEADER(struct _PDEOps);
  char           *description;
  PDEType        type;
  void           *data;
  PetscInt       n;
  Vec            x,xlocal[10];
  PetscBool      local_vector_valid;
  //void           *user_ctx;
  //PetscErrorCode (*destroy_user_ctx)(void*);

  PetscInt       *cell_region_idx;
  //PetscInt *surface_region_idx; // this level of generality possibly not needed

  // Coefficients can be fetched from the PDE
  //PetscInt    ncoeff;
  //Coefficient common_coeff[20];
  PetscInt    npde;
  PDE         commom_pde[PDE_MAX_COMMON];
};


#endif
