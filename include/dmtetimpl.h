
#if !defined(_DMTETIMPL_H)
#define _DMTETIMPL_H

#include <petsc.h>
#include <petscvec.h>
#include <petscdmtet.h>
#include "petsc/private/dmimpl.h"
#include "data_bucket.h"

typedef struct _p_VLIndex *VLIndex;
//typedef struct _p_BFacetList *BFacetList; /* I want to expose this type outside of the implementation */
typedef struct _p_FEGTopology *FEGTopology;

typedef struct _p_DMTetGList  *DMTetGList;

typedef DataBucket DMTetListManager;

typedef struct _p_DMTetPartInfo *DMTetPartInfo;
typedef struct _p_DMTetPartition *DMTetPartition;

struct _p_DMTetPartInfo {
  DM dm;
  PetscInt nelements,nnodes,npartitions;
  PetscInt *epart;
  PetscInt *node_element_map_size,**node_element_map;
  PetscInt *element_element_map_size,**element_element_map;
  PetscInt refcnt;
};

struct _p_DMTetPartition {
  PetscInt dim,npe;
  PetscInt nelement_local,nelement_ghost;
  PetscInt *element_map_local;
  PetscInt *element_map_ghost;
  PetscMPIInt *element_rank_ghost; /* no need to store element_rank_local */
  PetscInt *element_gid_local,*element_gid_ghost;
  PetscInt nvert_local, nvert_ghost;
  PetscReal *vert_coor_local,*vert_coor_ghost;
  PetscInt *vert_gid_local,*vert_gid_ghost;
  PetscMPIInt rank;
  PetscInt refcnt;
};

typedef struct {
  PetscInt nelements,nelements_g;
  PetscInt nnodes,nnodes_l,nnodes_g;
  PetscInt *l2g;
  PetscInt refcnt;
} DMTetMeshMPI;

typedef struct {
  PetscInt nelements;
  PetscInt bpe; /* basis per element */
  PetscInt *element;
  PetscInt nnodes;
  PetscInt npe;
  PetscReal *coords;
  PetscInt  *element_tag;
  /*PetscBool active_element_dof;*/
  /*PetscInt *element_dof;*/
  PetscInt nactivedof;
  FEGTopology topology;
  BFacetList boundary_facets;
  PetscInt nghost_elements;
  PetscInt nghost_nodes;
  PetscInt refcnt;
} DMTetMesh;

typedef struct {
  PetscInt dof;
  PetscInt basis_order;
  DMTetBasisType basis_type;
  DMTetMesh *geometry;
  DMTetMesh *space;
  DMTetMeshMPI *mpi;
  IS is_local,is_global;
  VecScatter scat_l2g;
  DMTetListManager section;
  DM dmtet_sequential;
  DMTetPartInfo pinfo;
  DMTetPartition partition;
} DM_TET;

/*
 
 Face id convention:
 
     fid:2
   0 ----- 2
    \      |
     \     |
      \    | fid:1
 fid:0 \   |
        \  |
         \ |
          1
 
 face id 0 --> face associated with element edge given by vertices [0,1]
 face id 1 --> face associated with element edge given by vertices [1,2]
 face id 2 --> face associated with element edge given by vertices [2,0]
 
 Faces have an orientation based on the local vertex numbers.
 This is important to define outward pointing normals in a consistent manner
*/

/*
 Boundary Facet List
 
 facet_to_element[f]      = e: provides element index (e) which facet (f) is associated with
 facet_element_face_id[f] = fid provides face index fid = [0,1,2] for facet (f)
*/
struct _p_BFacetList {
  PetscBool setup;
  PetscInt nfacets;
  PetscInt *facet_to_element;
  PetscInt *facet_element_face_id;
  PetscReal *face_normal;
  PetscReal *face_tangent;
  PetscInt  nbasis_facet;
  PetscInt *facet0_eid;
  PetscInt *facet1_eid;
  PetscInt *facet2_eid;
};

/* 
 Variable Length Index list 
 
 Essentially a 2D array of integers used to store topological relationships
 between finite element quantities (elements, vertices)
   L:            length of array
   iL:           number of entries for value[i]
   value[i][j]:  i \in [0,L-1] ; j \in [0,iL[i]]
*/
struct _p_VLIndex {
  PetscInt L;
  PetscInt *iL;
  PetscInt **value;
};

/*
 Finite Element Geometry Topology
 
 This structure will always be allocated and live on the DMTET
 Its contents (e.g. element-to-element map) will be created on demand as
 the topological relationships can be 
  (i) expensive to generate and
 (ii) expensive to store
(iii) the topological relationships are not required for all operations
 
 Following from point (iii), here are some specific use-cases
 * the vertex-to-element map is required for patch based re-constructions
 * the element-to-element map is required to define boundary facets
 
 Notes:
 * By default the object will construct all topological relationships from
 the element-vertex relationship which is always available for a FE mesh
 * The topological relationships are defined for the FE geometry, and not
   for FE function spaces
*/
struct _p_FEGTopology {
  VLIndex element_to_element;
  VLIndex vert_to_element;
  VLIndex vert_to_vert;
  PetscBool has_valid_e2e;
  PetscBool has_valid_v2e;
  PetscBool has_valid_v2v;
};

struct _p_DMTetGList {
  DM        dm;
  PetscInt  label_id;
  char      label_name[PETSC_MAX_PATH_LEN]; /* facetList:slab_wedge */
  PetscInt  length;
  PetscInt  *indices;
  PetscInt  bounds[2];
  PetscBool issetup;
  DMTetPrimitiveType primitive_type;
};

#endif

