
#if !defined(_SPIEGELMAN87_H)
#define _SPIEGELMAN87_H

#include <petsc.h>

PetscErrorCode Spiegelman87Ridge_EvaluateSolidVelocity(PetscReal coor[],PetscReal _alpha,PetscReal _w0,PetscReal _U0,PetscReal vel[]);
PetscErrorCode Spiegelman87Ridge_EvaluateFluidVelocity(PetscReal coor[],PetscReal _alpha,PetscReal _w0,PetscReal _U0,PetscReal vel[]);
PetscErrorCode Spiegelman87Trench_EvaluateSolidVelocity(PetscReal coor[],PetscReal _beta,PetscReal _w0,PetscReal _U0,PetscReal vel[]);
PetscErrorCode Spiegelman87Trench_EvaluateFluidVelocity(PetscReal coor[],PetscReal _beta,PetscReal _w0,PetscReal _U0,PetscReal vel[]);

#endif
