
#if !defined(__PDESTOKES_H)
#define __PDESTOKES_H

#include <petsc.h>
#include <petscdm.h>
#include <pde.h>
#include <coefficient.h>
#include <dmbcs.h>

PetscErrorCode PDECreateStokes(MPI_Comm comm,DM dmu,DM dmp,BCList udirichlet,PDE *pde);
PetscErrorCode PDEStokesGetCoefficients(PDE pde,Coefficient *eta,Coefficient *fu,Coefficient *fp);
PetscErrorCode PDEStokesGetDM(PDE pde,DM *dms);
PetscErrorCode PDEStokesCreateOperator_MatNest(PDE pde,Mat *A);
PetscErrorCode PDEStokesGetBCLists(PDE pde,BCList *u,BCList *p);
PetscErrorCode PDEStokesSetData(PDE pde,DM dmu,DM dmp,BCList ubc);
PetscErrorCode PDEStokesFieldView_AsciiVTU(PDE pde,const char prefix[]);

#endif
