
#ifndef __DS_DATA_EXCHANGER_H__
#define __DS_DATA_EXCHANGER_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>

typedef enum { DSDEOBJECT_INITIALIZED=0, DSDEOBJECT_FINALIZED, DSDEOBJECT_STATE_UNKNOWN, DSDEOBJECT_FLUSHED } DSDEObjectState;

typedef enum { DSDE_TOPOLOGY_DYNAMIC=0, DSDE_TOPOLOGY_LOCKED } DSDETopologyMode;

typedef struct _p_DSDataEx* DSDataEx;
typedef struct _p_DSDataExPack* DSDataExPack;

struct  _p_DSDataExPack {
  PetscMPIInt src,dest;
  size_t      unit_message_size;
  PetscInt    nitems;
  void        *buffer;
};

struct  _p_DSDataEx {
	MPI_Comm     commref,comm;
	PetscMPIInt  rank;
  Mat          topo;
  PetscMPIInt  npacks;
  DSDataExPack *pack;
  size_t       unit_message_size;
  PetscMPIInt  nneighbours,*neighbour;
  
  PetscInt     *message_offsets; /* [n_neighbour_procs] */
  PetscInt     *messages_to_be_recvieved; /* [n_neighbour_procs] */
  PetscInt     recv_message_length;
  void         *recv_message;
  
  DSDEObjectState     topology_status;
  DSDEObjectState     packer_status;
  DSDEObjectState     communication_status;
  
  MPI_Status  *_stats;
  MPI_Request *_requests;
  
  PetscMPIInt      tag;
  DSDETopologyMode topology_mode;
  PetscBool        use_only_active_neighbours;
};

extern const char *dsde_status_names[];

PetscErrorCode DSDataExCreate(MPI_Comm comm,DSDataEx *dsde);
PetscErrorCode DSDataExDestroy(DSDataEx *dsde);
PetscErrorCode DSDataExPackInitialize(DSDataEx de,size_t unit_message_size);
PetscErrorCode DSDataExPackFinalize(DSDataEx de);
PetscErrorCode DSDataExPackData(DSDataEx de,PetscMPIInt dest,PetscInt n,void *data);
PetscErrorCode DSDataExBegin(DSDataEx de);
PetscErrorCode DSDataExEnd(DSDataEx de);
PetscErrorCode DSDataExPackFlush(DSDataEx de);

PetscErrorCode DSDataExTopologyReset(DSDataEx de);
PetscErrorCode DSDataExTopologySetMode(DSDataEx de,DSDETopologyMode mode);
PetscErrorCode DSDataExTopologySetUseOnlyActiveNeighbours(DSDataEx de,PetscBool use_active);

PetscErrorCode DSDataExGetRecvData(DSDataEx de,PetscInt *length,void **recv);
PetscErrorCode DSDataExGetRecvDataByRank(DSDataEx de,PetscMPIInt src,PetscInt *length,void **recv);
PetscErrorCode DSDataExTopologyGetNeighbours(DSDataEx de,PetscMPIInt *n,PetscInt *neigh[]);
PetscErrorCode DSDataExView(DSDataEx d);

#endif

