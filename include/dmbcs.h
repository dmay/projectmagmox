
#ifndef __dmbcs_h__
#define __dmbcs_h__


typedef enum { BCList_UNCONSTRAINED=-1, BCList_DIRICHLET=-2, BCList_DIRICHLET_CANDIDATE=-3  } BCListConstraintType;

typedef struct _p_BCList *BCList;

struct _p_BCList {
  MPI_Comm    comm;
	DM          dm;
	PetscInt    blocksize;
	PetscInt    N,L; /* L = N * blocksize */
	PetscInt    N_local,L_local;
	PetscScalar *vals_global;
	PetscInt    *dofidx_global;
	PetscInt    *dofidx_local;
	PetscScalar *vals_local;
	PetscScalar *scale_global;
	PetscBool   allEmpty;
};

PetscErrorCode BCListIsDirichlet(PetscInt value,PetscBool *flg);
PetscErrorCode BCListInitialize(BCList list);
PetscErrorCode BCListCreate(MPI_Comm comm,BCList *list);
PetscErrorCode BCListDestroy(BCList *list);
PetscErrorCode BCListSetSizes(BCList list,PetscInt bs,PetscInt N,PetscInt N_local);
PetscErrorCode BCListUpdateCache(BCList list);
PetscErrorCode BCListInitGlobal(BCList list);
PetscErrorCode BCListGlobalToLocal(BCList list);
PetscErrorCode BCListLocalToGlobal(BCList list);
PetscErrorCode DMDABCListCreate(DM da,BCList *list);
PetscErrorCode BCListResidualDirichlet(BCList list,Vec X,Vec F);

PetscErrorCode BCListGetGlobalValues(BCList list,PetscInt *n,PetscScalar **vals);
PetscErrorCode BCListRestoreGlobalIndices(BCList list,PetscInt *n,PetscInt **idx);
PetscErrorCode BCListGetGlobalIndices(BCList list,PetscInt *n,PetscInt **idx);
PetscErrorCode BCListGetDofIdx(BCList list,PetscInt *Lg,PetscInt **dofidx_global,PetscInt *Ll,PetscInt **dofidx_local);

PetscErrorCode BCListGetLocalValues(BCList list,PetscInt *n,PetscScalar **vals);
PetscErrorCode BCListGetLocalIndices(BCList list,PetscInt *n,PetscInt **idx);


PetscErrorCode BCListInsert(BCList list,Vec y);
PetscErrorCode BCListInsertLocal(BCList list,Vec y);

/* for mat mult */
PetscErrorCode BCListInsertZero(BCList list,Vec y);
PetscErrorCode BCListInsertLocalZero(BCList list,Vec y);
PetscErrorCode BCListInsertDirichlet_MatMult(BCList list,Vec X,Vec F);

/* for mat get diagonal */
PetscErrorCode BCListInsertValueIntoDirichletSlot(BCList list,PetscScalar value,Vec y);

PetscErrorCode BCListApplyDirichletMask(PetscInt N_EQNS, PetscInt gidx[],BCList list);
PetscErrorCode BCListRemoveDirichletMask(PetscInt N_EQNS, PetscInt gidx[],BCList list);
PetscErrorCode BCListInsertScaling(Mat A,PetscInt N_EQNS, PetscInt gidx[],BCList list);

PetscErrorCode DMBCListCreate(DM da,BCList *list);
PetscErrorCode BCListMerge(PetscInt n,BCList input[],BCList *list);
PetscErrorCode BCListReset(BCList list);

PetscErrorCode BCListZeroRows(BCList list,Mat A);
PetscErrorCode BCListZeroRowsColumns(BCList list,Mat A);

/* tet iterators */
PetscBool BCListEvaluator_constant( PetscScalar position[], PetscScalar *value, void *ctx );
PetscErrorCode BCListDefineDirichlet_TraverseEdges_DMTet(BCList list,DM dm,PetscInt marker,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,void*),void *ctx);
PetscErrorCode BCListDefineDirichlet_TraverseCells_DMTet(BCList list,DM dm,PetscInt marker,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,void*),void *ctx);
PetscErrorCode BCListDefineDirichlet_TraverseAllNodes_DMTet(BCList list,DM dm,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);
PetscErrorCode BCListDefineDirichlet_TraverseBoundaryNodes_DMTet(BCList list,DM dm,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);

PetscErrorCode DMTetCreateDirichletList_TraverseBoundaryNodes(DM dm,PetscBool (*boundary_query)(PetscScalar*,void*),void *ctx,BCList *list);
PetscErrorCode DMTetCreateDirichletList_TraverseAllNodes(DM dm,PetscBool (*boundary_query)(PetscScalar*,void*),void *ctx,BCList *list);
PetscErrorCode BCListDefineDirichletValue(BCList list,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);

PetscErrorCode DMTetCreateDirichletList_Default(DM dm,BCList *list);
PetscErrorCode BCListSetDirichletValue(BCList list,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),void *ctx);

PetscErrorCode DMTetBCListTraverse(BCList list,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),void *ctx);

PetscErrorCode DMTetBCListUserTraverse(BCList list,PetscInt dof_idx,
     PetscErrorCode (*eval)(DM,PetscInt,PetscScalar*,PetscInt*,PetscScalar*,void*),
     void *ctx);
PetscErrorCode DMTetBCListTraverseByRegionId(BCList list,PetscInt regionid,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),void *ctx);

PetscErrorCode DMTetFacetBCListTraverse(BCList list,PetscInt dof_idx,
                                        PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),
                                        void *ctx);
PetscErrorCode DMTetBCListTraverseDMTetGList(BCList bclist,PetscInt dof_idx,
                                             const char labelName[],
                                             PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),
                                             void *ctx);


#endif
