

#if !defined(_DMTETGEOMUTILS_H)
#define _DMTETGEOMUTILS_H

#include <petsc.h>
#include <petscmat.h>
#include <petscksp.h>
#include <dmtetimpl.h>
#include <dmtetdt.h>


PetscErrorCode GeomHelper_ComputeElememntBoundingBox(DM_TET *tet,PetscReal celldx[]);
PetscErrorCode GeomHelper_ComputeDomainBoundingBox(DM_TET *tet,PetscReal min[],PetscReal max[]);
PetscErrorCode GeomHelper_ComputeEdgeMidPoints(DM_TET *tet,PetscReal vert[],PetscReal e0[],PetscReal e1[],PetscReal e2[]);
PetscErrorCode PiolaTransformation_EvaluateJ_AbsDetJ(PetscReal ecoor[],PetscReal j[],PetscReal *dj);
PetscErrorCode PiolaTransformation_EvaluateAbsDetJ(PetscReal ecoor[],PetscReal *dj);
PetscErrorCode PiolaTransformation_EvaluateJ_DetJ(PetscReal ecoor[],PetscReal j[],PetscReal *dj);

#endif
