
#if !defined(_DMTETCOMMON_H)
#define _DMTETCOMMON_H

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdmtet.h>

/* dmtet.c */
PetscErrorCode _DMTetMeshCreate1(PetscInt nel,PetscInt bpe,DMTetMesh *m);
PetscErrorCode _DMTetMeshCreate2(PetscInt nnodes,PetscInt npe,PetscInt dim,DMTetMesh *m);
PetscErrorCode _DMTetMeshCreate1_with_ghost(PetscInt nel,PetscInt nel_ghost,PetscInt bpe,DMTetMesh *m);
PetscErrorCode _DMTetMeshCreate2_with_ghost(PetscInt nnodes,PetscInt nnodes_ghost,PetscInt npe,PetscInt dim,DMTetMesh *m);
PetscErrorCode _DMTetMeshCreate(DMTetMesh **_mesh);
PetscErrorCode _DMTetMeshDestroy(DMTetMesh **_mesh);

PetscErrorCode _DMTetMeshMPICreate(DMTetMeshMPI **_mpi);

PetscErrorCode DMSetUp_TET(DM dm);

PetscErrorCode DMTetView_2d(DM dm,PetscViewer viewer);
PetscErrorCode DMTetCreateGlobalVector_SEQ(DM dm,Vec *_x);
PetscErrorCode DMTetSEQ_GlobalToLocalBegin(DM dm,Vec g,InsertMode mode,Vec l);
PetscErrorCode DMTetSEQ_GlobalToLocalEnd(DM dm,Vec g,InsertMode mode,Vec l);
PetscErrorCode DMTetSEQ_LocalToGlobalBegin(DM dm,Vec l,InsertMode mode,Vec g);
PetscErrorCode DMTetSEQ_LocalToGlobalEnd(DM dm,Vec l,InsertMode mode,Vec g);

/* dmtet_mpi.c */
PetscErrorCode DMTetCreateLocalVector_MPI(DM dm, Vec *x);
PetscErrorCode DMTetCreateGlobalVector_MPI(DM dm, Vec *x);
PetscErrorCode DMTetMPI_GlobalToLocalBegin(DM dm,Vec g,InsertMode mode,Vec l);
PetscErrorCode DMTetMPI_GlobalToLocalEnd(DM dm,Vec g,InsertMode mode,Vec l);
PetscErrorCode DMTetMPI_LocalToGlobalBegin(DM dm,Vec l,InsertMode mode,Vec g);
PetscErrorCode DMTetMPI_LocalToGlobalEnd(DM dm,Vec l,InsertMode mode,Vec g);

/* dmtet_interpolation.c */
PetscErrorCode DMCreateInterpolation_DMTET(DM dmF,DM dmT,Mat *mat,Vec *vec);
PetscErrorCode DMCoarsen_Tet(DM dm,MPI_Comm comm,DM *_dm);

/* dmtet_matcreate.c */
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_Pk2(DM dm_w,DM dm_u,Mat *_A);
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_disc_Pk2(DM dm_w,DM dm_u,Mat *_A);
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_Pk2_disc(DM dm_w,DM dm_u,Mat *_A);
PetscErrorCode DMTetCreateMatrixDiscontinuousSpace_SEQ(DM dm,Mat *_A);
PetscErrorCode DMTetCreateMatrixContinuousSpace_SEQ(DM dm,Mat *_A);
PetscErrorCode DMTetCreateMatrixRTSpace_SEQ(DM dm,Mat *_A);

PetscErrorCode DMTetCreateMatrixContinuousSpace_MPI(DM dm,Mat *_A);
PetscErrorCode DMTetCreateMatrixDiscontinuousSpace_MPI(DM dm_w,Mat *A);

/* Coupling specific methods */
/* a) Coupling type cell-cell neighbours with scalar coupling only : suitable for a vector mass matrix */
PetscErrorCode DMTetCreateMatrix_ScalarCoupling(DM dm_w,Mat *_A);
/* b) Coupling type cell-cell neighbours with vector coupling : suitable for a stiffness mass matrix */
/* c) Coupling type cell local with scalar or vector coupling : suitable for a DG mass matrix */

PetscErrorCode FARCMatPreallocationInit(Mat A);
PetscErrorCode FARCMatPreallocationSetValuesLocal(Mat A,PetscInt nr,PetscInt rows[],PetscInt nc,PetscInt cols[]);
PetscErrorCode FARCMatPreallocationSetValues(Mat A,PetscInt nr,PetscInt rows[],PetscInt nc,PetscInt cols[]);
PetscErrorCode FARCMatPreallocationFlush(Mat A);
PetscErrorCode FARCMatPreallocationInsertZeros(Mat A);
PetscErrorCode FARCMatPreallocationFinalize(Mat A);

/* dmtet_list.c */
PetscErrorCode DMTetGListCreate(DMTetGList *set);
PetscErrorCode DMTetGListDestroy(DMTetGList *set);
PetscErrorCode DMTetGListInit(DMTetGList list);
PetscErrorCode DMTetGListReset(DMTetGList list);
PetscErrorCode DMTetGListSetSize(DMTetGList list,PetscInt length);
PetscErrorCode DMTetGListSetLabelId(DMTetGList list,PetscInt id);
PetscErrorCode DMTetGListSetLabelName(DMTetGList list,const char name[]);
PetscErrorCode DMGetDMTetGList(DM dm,const char labelName[],DMTetGList *set);
PetscErrorCode DMTetGListCreateDefault(DM dm,PetscInt length,DMTetGList *set);
PetscErrorCode DMTetGListGetIndices(DMTetGList list,PetscInt *length,const PetscInt *indices[]);
PetscErrorCode DMTetGListSetup(DMTetGList list);
PetscErrorCode DMTetGListCheckBounds(DMTetGList list,PetscInt min,PetscInt max);
PetscErrorCode DMTetGListSetDM(DMTetGList list,DM dm);
PetscErrorCode DMTetGListCheckCompatibility(DMTetGList list_from,DMTetPrimitiveType expected_primitive,DMTetGList list_to);
PetscErrorCode DMTetGListSetPrimitiveType(DMTetGList list,DMTetPrimitiveType type);
PetscErrorCode DMTetGListGetPrimitiveTypeAsString(DMTetPrimitiveType type,const char *name[]);
PetscErrorCode DMTetGListGetPrimitiveType(DMTetGList list,DMTetPrimitiveType *type);

/* dmtet_basis_cwarpandblend.c */
PetscErrorCode DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(PetscInt order,PetscInt *_npe,PetscReal **_xi);
PetscErrorCode DMTetGenerateBasis2d_CWARPANDBLEND(DM dm,DM_TET *tet,PetscInt order);

PetscErrorCode DMTetTabulateBasis2d_CWARPANDBLEND(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_Ni);
PetscErrorCode DMTetTabulateBasisDerivatives2d_CWARPANDBLEND(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_GNix,PetscReal ***_GNiy);

/* dmtet_basis_rt.c */
PetscErrorCode DMTetTabulateBasis2d_RaviartThomas(DM dm, PetscInt npoints,PetscReal xi[],PetscInt *_nbasis,PetscReal ***_N);
PetscErrorCode DMTetTabulateBasisDerivatives2d_RaviartThomas(DM dm,PetscInt npoints,PetscReal xi[],PetscInt *_nbasis,PetscReal ***_GNx,PetscReal ***_GNy);
PetscErrorCode DMTetGenerateBasis2d_RaviartThomas(DM dm,DM_TET *tet,PetscInt order);

/* dmtet_basis_dg.c */
PetscErrorCode DMTetTabulateBasis2d_DG(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_Ni);
PetscErrorCode DMTetTabulateBasisDerivatives2d_DG(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_GNx,PetscReal ***_GNy);
PetscErrorCode DMTetGenerateBasis2d_DG(DM dm,DM_TET *tet,PetscInt order);
PetscErrorCode DMTetGenerateBasis2d_DG_MPI(DM dm,DM_TET *tet,PetscInt order);

/* dmtet_partition.c */
PetscErrorCode DMTetPartInfoCreate(DMTetPartInfo *info);
PetscErrorCode DMTetPartInfoDestroy(DMTetPartInfo *info);
PetscErrorCode DMTetPartInfoSetup(DMTetPartInfo info,DM dm,MPI_Comm comm);
PetscErrorCode DMTetPartitionView2d_VTK(DM dm,PetscInt epart[],const char name[]);

PetscErrorCode DMTetPartitionCreate(MPI_Comm comm,DM dm_sequential,DMTetPartition *p);
PetscErrorCode DMTetPartitionDestroy(DMTetPartition *_p);

PetscErrorCode _DMTetGenerateBasis2d_CWARPANDBLEND_MPI(DM dm,PetscInt element_rank_ghost[],DMTetBasisType btype,PetscInt order,DM_TET *tet);

/* dmtet_2d.c */
PetscErrorCode DMTetGenerateBasis2d_MPI(DM dm);
PetscErrorCode DMTetGenerateBasis2d_SEQ(DM dm);

#endif
