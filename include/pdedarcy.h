
#if !defined(__PDEDARCY_H)
#define __PDEDARCY_H

#include <petsc.h>
#include <petscdm.h>
#include <pde.h>
#include <coefficient.h>
#include <dmbcs.h>

typedef enum { DARCY_BFORM_L2H1 = 0, DARCY_BFORM_HDIVL2 } DarcyBformType;

PetscErrorCode PDECreateDarcy(MPI_Comm comm,DM dmu,DM dmp,BCList udirichlet,BCList pdirichlet,PetscInt DarcyBformType,PDE *pde);
PetscErrorCode PDEDarcySetData(PDE pde,DM dmu,DM dmp,BCList ubc,BCList pbc,DarcyBformType form_type);
PetscErrorCode PDEDarcyL2H1SetData(PDE pde,DM dmu,DM dmp,BCList pbc);
PetscErrorCode PDEDarcyHdivL2SetData(PDE pde,DM dmu,DM dmp,BCList ubc);
PetscErrorCode PDEDarcyCreateOperator_MatNest(PDE pde,Mat *A);

PetscErrorCode PDEDarcyGetVCoefficients(PDE pde,Coefficient *a,Coefficient *f,Coefficient *g);
PetscErrorCode PDEDarcyGetSCoefficients(PDE pde,Coefficient *uN,Coefficient *pN);
PetscErrorCode PDEDarcyGetDM(PDE pde,DM *dms);

#endif
