
#if !defined(__QUADRATUREIMPL_H)
#define __QUADRATUREIMPL_H

#include <petsc.h>
#include <data_bucket.h>
#include <quadrature.h>

struct _p_Quadrature {
  PetscBool      setup;
  PetscInt       dim;
  QuadratureGeometryType gtype; /* dim=2 {line, surface-area} : dim=3 {line, surface-area, vol} */
  QuadratureType         type; /* GAUSS_LEGENDRE_TENSOR, GAUSS_LEGENDRE_WARPED_TENSOR */
  PetscInt       order,npoints;
  PetscReal      *q_xi_coor;
  PetscReal      *q_weight;
  PetscInt       nelements;
  DataBucket     properties;
  void           *data;
};

struct _p_VQuadraturePoint {
  PetscInt  element_id;
  PetscReal coor[2];
  PetscInt  nexpected_values;
  PetscBool values_set;
};

struct _p_SQuadraturePoint {
  PetscInt  element_id,face_id;
  PetscReal coor[2],normal[2],tangent[2];
  PetscInt  nexpected_values;
  PetscBool values_set;
};

struct _p_EvaluationPoint {
  PetscInt  element_id,face_id;
  PetscInt  region_tag;
  EvaluationPointType  point_type; /* volume quadrature (0); surface quadrature (1); basis function (2); cell centoid (3)*/
  PetscBool has_coords,has_orientation;
  PetscReal coor[2],normal[2],tangent[2];
  PetscInt  nexpected_values;
  PetscBool values_set;
};

#endif
