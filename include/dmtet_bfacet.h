
#if !defined(__PETSCDMTET_BFACET_H)
#define __PETSCDMTET_BFACET_H

#include <petsc.h>
#include <petscdm.h>

typedef struct _p_BFacetList *BFacetList;

PetscErrorCode BFacetListCreate(BFacetList *_f);
PetscErrorCode BFacetListDestroy(BFacetList *_f);

PetscErrorCode DMTetBFacetSetup(DM dm);

PetscErrorCode BFacetRestrictField(BFacetList f,PetscInt face_id,PetscInt ndof,PetscReal cell_val[],PetscReal face_val[]);

PetscErrorCode DMTetGetGeometryBFacetList(DM dm,BFacetList *f);
PetscErrorCode DMTetGetSpaceBFacetList(DM dm,BFacetList *f);
PetscErrorCode BFacetListGetOrientations(BFacetList f,PetscReal **normal,PetscReal **tangent);
PetscErrorCode BFacetListGetSizes(BFacetList f,PetscInt *nfacets,PetscInt *nbasis_facet);
PetscErrorCode BFacetListGetCellFaceBasisId(BFacetList f,PetscInt face_id,PetscInt **cell_face_nid);
PetscErrorCode BFacetListGetFacetId(BFacetList f,PetscInt **facet_element_face_id);
PetscErrorCode BFacetListGetFacetElementMap(BFacetList f,PetscInt **facet_to_element);
PetscErrorCode BFacetListIsSetup(BFacetList f,PetscBool *s);

#endif
