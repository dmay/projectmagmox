
#ifndef __proj_version_info_h__
#define __proj_version_info_h__

#define PROJ_DEVELOPMENT_VERSION


#define PROJ_VERSION_CNTR_REPO "git url: https://dmay@bitbucket.org/dmay/leasz.git"
#define PROJ_VERSION_CNTR_REVISION "commit hash: [out-of-date] Execute \"make releaseinfo\" to update to the most recent revision"
#define PROJ_VERSION_CNTR_LOG "log: [out-of-date] Execute \"make releaseinfo\" to update to the most recent revision"

#if defined(__has_include)
  #if __has_include("proj_git_version_info.h")
    #include "proj_git_version_info.h"
  #endif

  #if __has_include("proj_release_version_info.h")
    #include "proj_release_version_info.h"
  #endif
#endif

#endif

