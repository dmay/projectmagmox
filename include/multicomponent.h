
#if !defined(__MULTICOMPONENT_H)
#define __MULTICOMPONENT_H

#include <petsc.h>
#include <cslpoly.h>

#define PHASE_MAX        2
#define PHASE_IDX_SOLID  0
#define PHASE_IDX_LIQUID 1

typedef struct _p_TPMC *TPMC;
typedef struct _p_ComponentF *ComponentF;

struct _p_ComponentF {
  PetscInt         nphases;
  char             name[PETSC_MAX_PATH_LEN];
  PetscBool        *phase_active; /* size = [nphases] */
  PetscBool        solve;
};

struct _p_TPMC {
  PetscInt         nphases,ncomponents,component_index_eliminated;
  CSLPoly          *csl; /* size = [nphases] */
  char             **phase_names; /* size = [nphases] */
  ComponentF       *component; /* size = [nphases] */
  PetscBool        setup_phases;
  PetscBool        setup;
  PetscReal        *pressure_v,*temperature_v;
  PetscErrorCode   (*thermodyn_calc)(const PetscReal*,const PetscReal*,const PetscInt,const PetscReal*,PetscReal*,PetscReal*,PetscReal*,PetscBool*,void*);
  void             *thermodyn_ctx;
  
  PetscInt nactive_s,*active_set_s,*map_set_s; /* active_set[ncomponents], map_set[nactive] */
  PetscInt nactive_l,*active_set_l,*map_set_l;
  
  PetscInt  ncells,nvert;
  PetscReal *cbar_c; /* [ncomponents.ncells] */
  PetscReal *cs_c,*cl_c,*phi_c; /* [ncomponents.ncells], [ncomponents.ncells], [ncells] */
  PetscReal *gamma_c,*temperature_prev_v;
  PetscBool view_bulk,view_phase,view_vel,view_interior_v,view_interior_c,view_gamma,view_phi,view_T;
};

PetscErrorCode TPMCCreate(MPI_Comm comm,TPMC *m);
PetscErrorCode TPMCDestroy(TPMC *_m);
PetscErrorCode TPMCAddComponent(TPMC m,const char name[]);
PetscErrorCode TPMCComponentSetPhaseActive(TPMC m,PetscInt index,PetscBool active[]);
PetscErrorCode TPMCComponentEliminate(TPMC m,PetscInt index);
PetscErrorCode TPMCSetup_ActiveSets(TPMC m);
PetscErrorCode TPMCSetup(TPMC m);
PetscErrorCode TPMCView(TPMC m);
PetscErrorCode TPMCConfigureCSLPoly(TPMC m,PetscInt npoly,PetscReal polycoor[],PetscInt edgelabel[],PetscReal ds,PetscInt mcell_s,PetscInt mcell_l);
PetscErrorCode TPMCSetWeightedConcentrations_Interior(TPMC m);
PetscErrorCode TPMCFuseWeightedConcentrations(TPMC m);
PetscErrorCode TPMCApplyUnitySumPhaseComponents_Interior(TPMC m);
PetscErrorCode TPMCApplyUnitySumBulkComponents_Interior(TPMC m,PetscReal cbulk[]);
PetscErrorCode TPMCUpdateVelocity(TPMC m);
PetscErrorCode TPMCGenericFacetBCIterator(TPMC m,PetscInt edge,void (*F)(const PetscReal*,const PetscReal*,const PetscReal *,PetscReal*,void*),void *ctx);
PetscErrorCode TPMCInitializeBC(TPMC m);
PetscErrorCode TPMCVerifyBC(TPMC m);
PetscErrorCode TPMCSolve(TPMC m,PetscInt tk,PetscReal dt,PetscReal time_tk);
PetscErrorCode TPMCComputeStateUpdate(TPMC m,PetscInt tk,PetscReal dt,PetscReal time_tk,PetscReal cbulk[]);
PetscErrorCode TPMEquilibrateState(TPMC m,PetscReal cbulk[]);
PetscErrorCode TPMCInterpolateBulkConcentrationAtPoint(TPMC m,const PetscReal coor[],PetscReal cbar_p[],PetscBool *inside_poly,PetscBool *inside_box);

PetscErrorCode TPMCSetThermoDynamicMethod(TPMC m,
                                          PetscErrorCode (*f)(const PetscReal*,const PetscReal*,const PetscInt,const PetscReal*,PetscReal*,PetscReal*,PetscReal*,PetscBool*,void*),
                                          void *thermodyn_ctx);
PetscErrorCode TPMCViewAllVTS(TPMC m,const char fname[]);
PetscErrorCode TPMCViewAllVTSBinary(TPMC m,const char fname[]);
PetscErrorCode TPMCFacetViewAllVTU(TPMC m,const char fname[]);
PetscErrorCode TPMCEquilibrateInitialCondition(TPMC m);
PetscErrorCode TPMCEquilibrium(TPMC m,const PetscReal P,const PetscReal T,PetscReal cbulk[],
                               PetscReal cl[],PetscReal cs[],PetscReal *phi,PetscBool *valid);
PetscErrorCode TPMCEnforceUnitySumBulkComponentsAtPoint(TPMC m,PetscReal cbulk[]);
PetscErrorCode TPMCUpdateTemperatureHistory(TPMC m);
PetscErrorCode TPMCSetViewComponents(TPMC m,PetscBool value_bulk,PetscBool value_phase);
PetscErrorCode TPMCSetViewVelocities(TPMC m,PetscBool value);
PetscErrorCode TPMCSetViewInteriorPoints(TPMC m,PetscBool value_c,PetscBool value_v);
PetscErrorCode TPMCSetViewGammaPhi(TPMC m,PetscBool value_gamma,PetscBool value_phi);
PetscErrorCode TPMCSetViewTemperature(TPMC m,PetscBool value);
PetscErrorCode MCComputeResidual(TPMC m,PetscReal temperature_v[],PetscInt *_nk,PetscInt *_ncells,PetscReal *_norms[]);

PetscErrorCode TPMCSmoothLiquid(TPMC m,PetscReal kappa,PetscReal tf);
PetscErrorCode TPMCComputeCFLTimestep(TPMC m,PetscReal *_dt_l,PetscReal *_dt_s);

#endif
