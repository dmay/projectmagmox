
/*
 
 Coefficients represent data and methods to evaluate user defined 
 definitions of coefficients within a PDE. 

 Notes:
 
 * This method will be promoted to a Petsc object if we start "sharing"
 coefficients between different PDEs
 
*/

#if !defined(__COEFFICIENT_H)
#define __COEFFICIENT_H

typedef struct _p_Coefficient *Coefficient;

typedef enum { COEFF_NONE = 0, COEFF_DOMAIN_CONST, COEFF_QUADRATURE } CoefficientType;

#include <petsc.h>
#include <quadrature.h>
#include <pde.h>

PetscErrorCode CoefficientCreate(MPI_Comm,Coefficient*);
PetscErrorCode CoefficientDestroy(Coefficient*);
PetscErrorCode CoefficientSetType(Coefficient,CoefficientType);

PetscErrorCode CoefficientSetDomainConstant(Coefficient,PetscReal);
PetscErrorCode CoefficientSetComputeDomainConstant(Coefficient,PetscErrorCode (*evaluate_domain_const)(Coefficient,PetscReal*,void*),PetscErrorCode (*destroy_domain_const)(Coefficient),void *data);

PetscErrorCode CoefficientSetComputeQuadrature(Coefficient,PetscErrorCode (*evaluate_quadrature)(Coefficient,Quadrature,PetscInt*,void*),PetscErrorCode (*destroy_quadrature)(Coefficient),void *data);
//PetscErrorCode CoefficientSetQuadrature(Coefficient,Quadrature);
PetscErrorCode CoefficientGetQuadrature(Coefficient,Quadrature*);

PetscErrorCode CoefficientSetPDE(Coefficient,PDE);
PetscErrorCode CoefficientGetPDE(Coefficient,PDE*);

PetscErrorCode CoefficientEvaluate(Coefficient);
PetscErrorCode CoefficientGetDomainConstant(Coefficient c,PetscReal *val);
PetscErrorCode CoefficientSetComputeQuadratureNULL(Coefficient c);
PetscErrorCode CoefficientSetComputeQuadratureEmpty(Coefficient c);
PetscErrorCode CoefficientView_GP(Coefficient c,DM dm,const char prefix[]);
PetscErrorCode CoefficientSetRegionsFromDM(Coefficient c,DM dm);
PetscErrorCode CoefficientResetRegionsFromDM(Coefficient c,DM dm);
PetscErrorCode CoefficientGetRegions(Coefficient c,const PetscInt **r);

PetscErrorCode CoefficientQuadratureEvaluator_Empty(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context);
PetscErrorCode CoefficientQuadratureEvaluator_NULL(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context);

#endif
