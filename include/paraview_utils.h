
#if !defined(_PARAVIEW_UTILS_H)
#define _PARAVIEW_UTILS_H

#include <petsc.h>

PetscErrorCode ParaviewPVDOpen(const char pvdfilename[]);
PetscErrorCode ParaviewPVDAppend(const char pvdfilename[],PetscReal time,const char datafile[], const char DirectoryName[]);

#endif
