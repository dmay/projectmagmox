

#if !defined(__MMS_STOKES_EXP_ETA_H)
#define __MMS_STOKES_EXP_ETA_H

#include <petsc.h>

PetscErrorCode StokesSolution_EvaluateV(PetscReal x[],PetscReal uv[],void *ctx);
PetscErrorCode StokesSolution_EvaluateP(PetscReal x[],PetscReal p[],void *ctx);
PetscErrorCode StokesSolution_EvaluateViscosity(PetscReal x[],PetscReal eta[],void *ctx);
PetscErrorCode StokesSolution_EvaluateGradientV(PetscReal x[],PetscReal grad_uv[],void *ctx);
PetscErrorCode StokesSolution_EvaluateFu(PetscReal x[],PetscReal Fu[],void *ctx);
PetscErrorCode StokesSolution_EvaluateFp(PetscReal x[],PetscReal Fp[],void *ctx);
PetscErrorCode StokesSolution_EvaluateStress(PetscReal x[],PetscReal sigma[],void *ctx);

#endif
