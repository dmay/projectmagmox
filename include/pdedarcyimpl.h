
#if !defined(__PDEDARCYIMPL_H)
#define __PDEDARCYIMPL_H

#include <petsc.h>
#include <petscdm.h>
#include <pdedarcy.h>
#include <coefficient.h>
#include <dmbcs.h>
#include <element_container.h>

typedef struct {
  DM          dmu,dmp,dms;
  BCList      u_dirichlet,p_dirichlet;
  Coefficient a;
  Coefficient f,g;
  Coefficient p_neumann,u_neumann;
  PetscInt    mu,mp,Mu,Mp; /* sizes of the local/global velocity/pressure spaces */
  Quadrature  ref;
  PetscInt    nubasis,npbasis;
  EContainer  ec_u,ec_p,ec_g;
  DarcyBformType bform_type; /* (L2H1 = 0 , HdivL2 = 1) */
} PDEDarcy;

#endif

