
#if !defined(__INORMS_H)
#define __INORMS_H

typedef enum { Q_VOL=0, Q_L2, Q_H1, Q_H1SEMI, Q_NUM_ENTRIES } INormQuantities;

PetscErrorCode ComputeINorms(PetscReal E[],DM dm,Vec x,
                             PetscErrorCode (*eval_solution)(PetscReal*,PetscReal*,void*),
                             PetscErrorCode (*eval_solution_gradient)(PetscReal*,PetscReal*,void*),void *ctx);
PetscErrorCode IntegrateField(PetscReal E[],DM dm,Vec x);
PetscErrorCode IntegrateFieldL2(PetscReal E[],DM dm,Vec x);
PetscErrorCode IntegrateFieldH1(PetscReal E[],DM dm,Vec x);
PetscErrorCode IntegrateFieldHdiv(PetscReal E[],DM dm,Vec x);

#endif
