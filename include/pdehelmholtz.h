
#if !defined(__PDEHELMHOLTZ_H)
#define __PDEHELMHOLTZ_H

#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <dmbcs.h>
#include <pde.h>
#include <coefficient.h>

PetscErrorCode PDEHelmholtzSetData(PDE,DM,BCList);
PetscErrorCode PDEHelmholtzSetVelocity(PDE,DM,Vec);
PetscErrorCode PDECreateHelmholtz(MPI_Comm,DM,BCList,PDE*);
PetscErrorCode PDEHelmholtzGetCoefficient_g(PDE,Coefficient*);
PetscErrorCode PDEHelmholtzGetCoefficient_k(PDE,Coefficient*);
PetscErrorCode PDEHelmholtzGetCoefficient_alpha(PDE,Coefficient*);
PetscErrorCode PDEHelmholtzGetCoefficient_qN(PDE,Coefficient*);
PetscErrorCode PDEHelmholtzGetDM(PDE pde,DM *dm);
PetscErrorCode PDEHelmholtzSetAssumeVelocityIncompressible(PDE pde,PetscBool value);

PetscErrorCode FormFunction_PDEHelmholtz_PetrovGalerkin(SNES snes,Vec X,Vec F_p,DM dm_p);

#endif
