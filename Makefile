## ==============================================================================
##
##     makefile for PROJECT named PROJ
##
## ==============================================================================
.SECONDEXPANSION:		# to expand $$(@D)/.DIR
.SUFFIXES:	                # Clear .SUFFIXES because we don't use implicit rules
.DELETE_ON_ERROR:               # Delete likely-corrupt target file if rule fails

include $(PETSC_DIR)/$(PETSC_ARCH)/lib/petsc/conf/petscvariables

all : info libproj tests

# Compilation options are to be placed in makefile.arch
# if that (untracked) file does not exist, defaults are copied there
makefile.arch:
	-@echo "[PROJ config] using config/makefile.arch.default as makefile.arch"
	-@cp config/makefile.arch.default $@
-include makefile.arch

OBJDIR ?= $(PETSC_ARCH)/obj
LIBDIR ?= $(PETSC_ARCH)/lib
BINDIR ?= $(PETSC_ARCH)/bin

# directory that contains most recently-parsed makefile (current)
thisdir = $(addprefix $(dir $(lastword $(MAKEFILE_LIST))),$(1))
incsubdirs = $(addsuffix /local.mk,$(call thisdir,$(1)))

libproj-y.c :=
proj-tests-y.c :=
PROJ_INC := $(PETSC_CC_INCLUDES) -I${PWD}/include

# Recursively include files for all targets
include local.mk

#### Rules ####
ifeq ($(V),)
  quiet_HELP := "Use \"$(MAKE) V=1\" to see the verbose compile lines.\n"
  quiet = @printf $(quiet_HELP)$(eval quiet_HELP:=)"  %10s %s\n" "$1$2" "$@"; $($1)
else ifeq ($(V),0)		# Same, but do not print any help
  quiet = @printf "  %10s %s\n" "$1$2" "$@"; $($1)
else				# Show the full command line
  quiet = $($1)
endif

.PHONY: libproj tests

libproj = $(LIBDIR)/libproj.$(AR_LIB_SUFFIX)
libproj : $(libproj)

export SUBFUSC_LIB=$(PWD)/$(libproj)


$(libproj) : $(libproj-y.c:%.c=$(OBJDIR)/%.o)

info:
	-@echo "------------------------------------------------------------------------------"
	-@echo " PROJ"
	-@echo "    git url: https://bitbucket.org/dmay/leasz"
	-@echo "    [1] Update version information by executing \"make releaseinfo\""
	-@echo "    [2] Compile the library, tests by executing \"make all\""
	-@echo "    [3] Perform regression test suite by executing \"make testvalidation\""
	-@echo "------------------------------------------------------------------------------"

releaseinfo:
	-@echo "---------------------------------------------------------------------------------------------------------------"
	-@echo "  Generating version information in include/proj_git_version_info.h from: https://bitbucket.org/dmay/leasz"
	-@echo "  Version info will appear in: stdout; log files; generated PETSc summary/options files"
	-@echo "  You must execute \"make all\" to ensure that the generated version infomation will be displayed at run-time" 
	-@echo "---------------------------------------------------------------------------------------------------------------"
	$(shell python utils/mgmt/generate-git-info.py)
	$(shell mv proj_version_info.h include/proj_git_version_info.h)
	$(shell touch include/proj_version_info.h)
  # to trigger re-compilation / linking

%.$(AR_LIB_SUFFIX) : | $$(@D)/.DIR
	$(call quiet,AR) $(AR_FLAGS) $@ $^
	$(call quiet,RANLIB) $@

ifeq ($(PETSC_LANGUAGE),CXXONLY)
  cc_name := CC
else
  cc_name := CC
endif

# gcc/gfortran style dependency flags; these are set in petscvariables starting with petsc-3.5
C_DEPFLAGS ?= -MMD -MP
FC_DEPFLAGS ?= -MMD -MP

PROJ_COMPILE.c = $(call quiet,$(cc_name)) -c $(PCC_FLAGS) $(CCPPFLAGS) $(PROJ_CFLAGS) $(PROJ_INC) $(CFLAGS) $(C_DEPFLAGS)

tests: $(proj-tests-y.c:%.c=$(BINDIR)/%.app)
$(proj-tests-y.c:%.c=$(BINDIR)/%.app) : $(libproj)
.SECONDARY: $(proj-tests-y.c:%.c=$(OBJDIR)/%.o) # don't delete the intermediate files

testvalidation:
	@python tests/subfusc_harness.py

$(BINDIR)/%.app : $(OBJDIR)/%.o | $$(@D)/.DIR
	$(call quiet,PCC_LINKER) $(PROJ_CFLAGS) -o $@ $^ $(PETSC_SNES_LIB)
	@ln -sf $(abspath $@) $(BINDIR)

$(OBJDIR)/%.o: %.c | $$(@D)/.DIR
	$(PROJ_COMPILE.c) $(abspath $<) -o $@

%/.DIR :
	@mkdir -p $(@D)
	@touch $@

.PRECIOUS: %/.DIR
.SUFFIXES: # Clear .SUFFIXES because we don't use implicit rules
.DELETE_ON_ERROR:               # Delete likely-corrupt target file if rule fails

.PHONY: clean all print

clean:
	rm -rf $(OBJDIR) $(LIBDIR) $(BINDIR)

# make print VAR=the-variable
print:
	@echo $($(VAR))

srcs.c := $(libproj-y.c) $(proj-tests-y.c)
srcs.o := $(srcs.c:%.c=$(OBJDIR)/%.o)
srcs.d := $(srcs.o:%.o=%.d)
# Tell make that srcs.d are all up to date.  Without this, the include
# below has quadratic complexity, taking more than one second for a
# do-nothing build of PETSc (much worse for larger projects)
$(srcs.d) : ;

-include $(srcs.d)
