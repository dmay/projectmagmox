
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <dmbcs.h>
#include <element_container.h>
#include <fe_geometry_utils.h>

typedef struct {
  DM          dm;
  BCList      dirichletbc;
  Coefficient k,alpha;
  Coefficient g;
  Coefficient q_N;
  DM          dmV;
  Vec         velocity;
  PetscBool   assume_velocity_incompressible;
} PDEHelmholtz;

const char helmholtz_description[] =
"\"PDE Helmholtz\" solves the PDE \n"
"    -div ( k grad(u) ) + alpha u = g(x) \n"
"  subject to the boundary conditions u = u_D on the Dirichlet boundary \n"
"  and q_N = - k grad(u) on the Neumann boundary. \n"
"  The PDE is discretized using finite elements defined on a DMTET object. \n"
"  [Assumptions] \n"
"  * Only DMTET is supported. \n"
"  [User notes] \n"
"  * The function g(x) is defined via a Coefficient using point-wise defined \n"
"    quadrature point values. The quadrature field is named \"q_g0\". \n"
"  * The coefficient k and alpha are defined as a domain-wise constant.";


void ElementEvaluateGeometry_2d(PetscInt nqp,PetscInt nbasis,PetscReal el_coords[],
                                PetscReal **GNIxi,PetscReal **GNIeta,
                                PetscReal detJ[])
{
  PetscInt k,q;
  PetscReal J[2][2];
  
  for (q=0; q<nqp; q++) {
    //
    J[0][0] = J[0][1] = 0.0;
    J[1][0] = J[1][1] = 0.0;
    
    for (k=0; k<nbasis; k++) {
      PetscReal xc = el_coords[2*k+0];
      PetscReal yc = el_coords[2*k+1];
      
      J[0][0] += GNIxi[q][k] * xc ;
      J[0][1] += GNIxi[q][k] * yc ;
      
      J[1][0] += GNIeta[q][k] * xc ;
      J[1][1] += GNIeta[q][k] * yc ;
    }
    
    detJ[q] = J[0][0]*J[1][1] - J[0][1]*J[1][0];
  }
}

void ElementEvaluateDerivatives_2d(PetscInt nqp,PetscInt nbasis,PetscReal el_coords[],
                                   PetscReal **GNIxi,PetscReal **GNIeta,
                                   PetscReal **dNudx,PetscReal **dNudy)
{
  PetscInt k,q;
  PetscReal J[2][2],iJ[2][2],od,detJ;
  
  for (q=0; q<nqp; q++) {
    //
    J[0][0] = J[0][1] = 0.0;
    J[1][0] = J[1][1] = 0.0;
    
    for (k=0; k<nbasis; k++) {
      PetscReal xc = el_coords[2*k+0];
      PetscReal yc = el_coords[2*k+1];
      
      J[0][0] += GNIxi[q][k] * xc ;
      J[0][1] += GNIxi[q][k] * yc ;
      
      J[1][0] += GNIeta[q][k] * xc ;
      J[1][1] += GNIeta[q][k] * yc ;
    }
    
    detJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
    od = 1.0/detJ;
    
    iJ[0][0] =  J[1][1] * od;
    iJ[0][1] = -J[0][1] * od;
    iJ[1][0] = -J[1][0] * od;
    iJ[1][1] =  J[0][0] * od;
    
    /* shape function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtz_AssembleResidual_Local"
PetscErrorCode PDEHelmholtz_AssembleResidual_Local(PDEHelmholtz *data,PetscScalar x[],PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,nen,npe,dof;
  PetscInt e,i,q;
  PetscScalar *el_x,*el_F,*function;
  DM dm;
  Coefficient k,alpha,g;
  Quadrature quadrature;
  PetscReal kfac,alphafac;
  
  dm = data->dm;
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  g = data->g;
  ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);

  k = data->k;
  ierr = CoefficientGetDomainConstant(k,&kfac);CHKERRQ(ierr);

  alpha = data->alpha;
  ierr = CoefficientGetDomainConstant(alpha,&alphafac);CHKERRQ(ierr);

  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_x);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_F);
  PetscMalloc(sizeof(PetscReal)*nqp,&function);
  
  /* zero pass through */
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
      
      el_x[i] = x[nidx];
    }
    
    PetscMemzero(el_F,sizeof(PetscScalar)*nbasis*dof);
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp,gradx_qp[2];
      
      function[q] = coeff[e*nqp + q];
      
      /* get x at each quadrature point */
      x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp        += tab_N[q][i] * el_x[i];
        gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
        gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
      }
      
      for (i=0; i<nbasis; i++) {
        PetscScalar residual;
        
        residual = kfac*(tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1])
                   + alphafac * tab_N[q][i] * x_qp
                   - tab_N[q][i] * function[q];
        
        el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
      }
    }
    
    for (i=0; i<nbasis; i++) {
      F[idx[i]] += el_F[i];
    }
  }
  
  PetscFree(detJ);
  PetscFree(function);
  PetscFree(elcoords);
  PetscFree(el_x);
  PetscFree(el_F);
  
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzAdv_AssembleResidual_Local"
PetscErrorCode PDEHelmholtzAdv_AssembleResidual_Local(PDEHelmholtz *data,PetscScalar vel[],PetscScalar x[],PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,nen,npe,dof;
  PetscInt e,i,q;
  PetscScalar *el_x,*el_v,*el_F,*function;
  DM dm;
  Coefficient k,alpha,g;
  Quadrature quadrature;
  PetscReal kfac,alphafac;
  
  dm = data->dm;
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  g = data->g;
  ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  k = data->k;
  ierr = CoefficientGetDomainConstant(k,&kfac);CHKERRQ(ierr);
  
  alpha = data->alpha;
  ierr = CoefficientGetDomainConstant(alpha,&alphafac);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_x);
  PetscMalloc(sizeof(PetscReal)*2*nbasis,&el_v);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_F);
  PetscMalloc(sizeof(PetscReal)*nqp,&function);
  
  /* zero pass through */
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
      
      el_x[i] = x[nidx];
      el_v[2*i  ] = vel[2*nidx];
      el_v[2*i+1] = vel[2*nidx+1];
    }
    
    PetscMemzero(el_F,sizeof(PetscScalar)*nbasis*dof);
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    if (data->assume_velocity_incompressible) {
      for (q=0; q<nqp; q++) {
        PetscReal x_qp,gradx_qp[2],vel_q[2];
        
        function[q] = coeff[e*nqp + q];
        
        /* get x at each quadrature point */
        x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
        for (i=0; i<nbasis; i++) {
          x_qp        += tab_N[q][i] * el_x[i];
          gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
          gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
        }
        
        /* get velocity at each quadrature point */
        vel_q[0] = vel_q[1] = 0.0;
        for (i=0; i<nbasis; i++) {
          vel_q[0] += tab_N[q][i] * el_v[2*i  ];
          vel_q[1] += tab_N[q][i] * el_v[2*i+1];
        }
        
        for (i=0; i<nbasis; i++) {
          PetscScalar residual;
          
          residual = kfac*(tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1])
                     + alphafac * tab_N[q][i] * x_qp
                     - tab_N[q][i] * function[q];
          
          residual += tab_N[q][i] * (vel_q[0]*gradx_qp[0] + vel_q[1]*gradx_qp[1]);
          
          el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
        }
      }
    } else {
      for (q=0; q<nqp; q++) {
        PetscReal x_qp,gradx_qp[2],vel_q[2],div_vel_q;
        
        function[q] = coeff[e*nqp + q];
        
        /* get x at each quadrature point */
        x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
        for (i=0; i<nbasis; i++) {
          x_qp        += tab_N[q][i] * el_x[i];
          gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
          gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
        }

        /* get velocity at each quadrature point */
        div_vel_q = 0.0;
        vel_q[0] = vel_q[1] = 0.0;
        for (i=0; i<nbasis; i++) {
          vel_q[0] += tab_N[q][i] * el_v[2*i  ];
          vel_q[1] += tab_N[q][i] * el_v[2*i+1];
          div_vel_q += tab_dNdx[q][i] * el_v[2*i+0] + tab_dNdy[q][i] * el_v[2*i+1];
        }

        for (i=0; i<nbasis; i++) {
          PetscScalar residual;
          
          residual = kfac*(tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1])
                     + alphafac * tab_N[q][i] * x_qp
                     - tab_N[q][i] * function[q];
          
          residual += tab_N[q][i] * (vel_q[0]*gradx_qp[0] + vel_q[1]*gradx_qp[1]);
          residual += tab_N[q][i] * (div_vel_q * x_qp);
          
          el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
        }
      }
    }
    
    for (i=0; i<nbasis; i++) {
      F[idx[i]] += el_F[i];
    }
  }
  
  PetscFree(detJ);
  PetscFree(function);
  PetscFree(elcoords);
  PetscFree(el_x);
  PetscFree(el_v);
  PetscFree(el_F);
  
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtz_AssembleResidualNeumann_Local"
PetscErrorCode PDEHelmholtz_AssembleResidualNeumann_Local(PDEHelmholtz *data,PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis,nbasis_face;
  PetscReal *w,*xi;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,nen,npe;
  PetscInt e,i,q;
  PetscScalar *el_F;
  DM dm;
  Coefficient qN;
  Quadrature quadrature;
  EContainer elcontainer;
  PetscReal *_el_coor_face,dJs;
  PetscInt bf,nfacets;
  PetscReal *normal,**N_face;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscBool issetup;
  
  dm = data->dm;
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);

  /* return early if no facets have been defined */
  if (nfacets == 0) {
    PetscFunctionReturn(0);
  } else {
    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetOrientations(f,&normal,NULL);CHKERRQ(ierr);
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);

  
  qN = data->q_N;
  ierr = CoefficientGetQuadrature(qN,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N",&nfacets,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&elcontainer);CHKERRQ(ierr);
  _el_coor_face = elcontainer->buf_basis_2vector_a;
  N_face        = elcontainer->N;
  
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&el_F);CHKERRQ(ierr);

  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt *eidx,face_id,*eidx_face;
    PetscReal *normal_f;
    
    e = facet_to_element[bf];
    face_id = facet_element_face_id[bf];
  
    /* get basis dofs */
    eidx = &element[npe*e];
    
    /* face dof */
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = eidx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    ierr = BFacetRestrictField(f,face_id,2,elcoords,_el_coor_face);CHKERRQ(ierr);
    
    EvaluateBasisSurfaceJacobian_Affine(nbasis_face,_el_coor_face,&dJs);
    normal_f = &normal[2*bf];
    
    ierr = PetscMemzero(el_F,sizeof(PetscScalar)*nbasis);CHKERRQ(ierr);
    for (q=0; q<nqp; q++) {
      PetscReal *flux_q;
      
      flux_q = &coeff[2*bf*nqp + 2*q];
      
      for (i=0; i<nbasis_face; i++) {
        PetscScalar residual;
        
        residual = N_face[q][i] * (flux_q[0] * normal_f[0] +  flux_q[1] * normal_f[1]);
        
        el_F[i] += w[q] * ( residual ) * dJs;
      }
    }
    
    for (i=0; i<nbasis_face; i++) {
      PetscInt face_idx;

      face_idx = eidx_face[i];

      F[eidx[face_idx]] += el_F[i];
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  ierr = PetscFree(el_F);CHKERRQ(ierr);
  ierr = EContainerDestroy(&elcontainer);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunction_PDEHelmholtz"
PetscErrorCode FormFunction_PDEHelmholtz(SNES snes,Vec X,Vec F,void *ctx)
{
  PetscErrorCode ierr;
  PDE pde;
  PDEHelmholtz *data;
  Vec Xloc,Floc;
  PetscScalar *LA_Xloc,*LA_Floc;
  BCList bc = NULL;
  DM dm = NULL;

  ierr = SNESGetApplicationContext(snes,(void*)&pde);CHKERRQ(ierr);
  data = (PDEHelmholtz*)pde->data;
  dm = data->dm;
  bc = data->dirichletbc;
  
  Xloc = pde->xlocal[0];
  //ierr = DMGetLocalVector(dm,&Xloc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Floc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dm,X,INSERT_VALUES,Xloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,X,INSERT_VALUES,Xloc);CHKERRQ(ierr);
  pde->local_vector_valid = PETSC_TRUE;
  
  ierr = BCListInsertLocal(bc,Xloc);CHKERRQ(ierr);
  
  ierr = CoefficientEvaluate(data->k);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->alpha);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->q_N);CHKERRQ(ierr);
  
  ierr = VecGetArray(Xloc,&LA_Xloc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(Floc);CHKERRQ(ierr);
  ierr = VecGetArray(Floc,&LA_Floc);CHKERRQ(ierr);
  
  /* local residual evaluation */
  /* boundary terms */
  ierr = PDEHelmholtz_AssembleResidualNeumann_Local(data,LA_Floc);CHKERRQ(ierr);
  /* volume terms */
  if (!data->velocity) {
    ierr = PDEHelmholtz_AssembleResidual_Local(data,LA_Xloc,LA_Floc);CHKERRQ(ierr);
  } else {
    Vec Vloc;
    PetscScalar *LA_Vloc;
    
    ierr = DMGetLocalVector(data->dmV,&Vloc);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(data->dmV,data->velocity,INSERT_VALUES,Vloc);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(data->dmV,data->velocity,INSERT_VALUES,Vloc);CHKERRQ(ierr);
    
    ierr = VecGetArray(Vloc,&LA_Vloc);CHKERRQ(ierr);
    ierr = PDEHelmholtzAdv_AssembleResidual_Local(data,LA_Vloc,LA_Xloc,LA_Floc);CHKERRQ(ierr);
    ierr = VecRestoreArray(Vloc,&LA_Vloc);CHKERRQ(ierr);
    
    ierr = DMRestoreLocalVector(data->dmV,&Vloc);CHKERRQ(ierr);
  }
  ierr = VecRestoreArray(Floc,&LA_Floc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Xloc,&LA_Xloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dm,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dm,&Floc);CHKERRQ(ierr);
  //ierr = DMRestoreLocalVector(dm,&Xloc);CHKERRQ(ierr);
  
  /* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
  ierr = BCListResidualDirichlet(bc,X,F);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtz_AssembleResidual_Local_PetrovGalerkin"
PetscErrorCode PDEHelmholtz_AssembleResidual_Local_PetrovGalerkin(DM dm_p,PDEHelmholtz *data,PetscScalar x[],PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,*element_p,nen,bpe,bpe_p,dof;
  PetscInt e,i,q;
  PetscScalar *el_x,*el_F,*function;
  DM dm;
  Coefficient k,alpha,g;
  Quadrature quadrature;
  PetscReal kfac,alphafac;
  
  dm = data->dm;
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  g = data->g;
  ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  k = data->k;
  ierr = CoefficientGetDomainConstant(k,&kfac);CHKERRQ(ierr);
  
  alpha = data->alpha;
  ierr = CoefficientGetDomainConstant(alpha,&alphafac);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_x);
  PetscMalloc(sizeof(PetscReal)*nqp,&function);
  
  /* zero pass through */
  ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,0,0,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_p,&nen,&bpe_p,&element_p,0,0,0);CHKERRQ(ierr);
  PetscMalloc(sizeof(PetscReal)*bpe_p,&el_F);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[bpe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<bpe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
      
      el_x[i] = x[nidx];
    }
    
    PetscMemzero(el_F,sizeof(PetscScalar)*bpe_p*dof);
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp,gradx_qp[2];
      
      function[q] = coeff[e*nqp + q];
      
      /* get x at each quadrature point */
      x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
      for (i=0; i<bpe; i++) {
        x_qp        += tab_N[q][i] * el_x[i];
        gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
        gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
      }
      
      for (i=0; i<bpe_p; i++) {
        PetscScalar residual = 0.0;
        
        residual += kfac*(tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1]);
        residual += alphafac * tab_N[q][i] * x_qp - tab_N[q][i] * function[q];
        
        el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
      }
    }
    
    idx = &element_p[bpe_p*e];
    
    for (i=0; i<bpe_p; i++) {
      F[idx[i]] += el_F[i];
    }
  }
  
  PetscFree(detJ);
  PetscFree(function);
  PetscFree(elcoords);
  PetscFree(el_x);
  PetscFree(el_F);
  
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzAdv_AssembleResidual_Local_PetrovGalerkin"
PetscErrorCode PDEHelmholtzAdv_AssembleResidual_Local_PetrovGalerkin(DM dm_p,PDEHelmholtz *data,PetscScalar vel[],PetscScalar x[],PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,*element_p,nen,bpe,bpe_p,dof;
  PetscInt e,i,q;
  PetscScalar *el_x,*el_v,*el_F,*function;
  DM dm;
  Coefficient k,alpha,g;
  Quadrature quadrature;
  PetscReal kfac,alphafac;
  
  dm = data->dm;
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  g = data->g;
  ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  k = data->k;
  ierr = CoefficientGetDomainConstant(k,&kfac);CHKERRQ(ierr);
  
  alpha = data->alpha;
  ierr = CoefficientGetDomainConstant(alpha,&alphafac);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_x);
  PetscMalloc(sizeof(PetscReal)*2*nbasis,&el_v);
  PetscMalloc(sizeof(PetscReal)*nqp,&function);
  
  ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,NULL,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_p,&nen,&bpe_p,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);

  PetscMalloc(sizeof(PetscReal)*bpe_p,&el_F);

  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[bpe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<bpe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
      
      el_x[i] = x[nidx];
      el_v[2*i  ] = vel[2*nidx];
      el_v[2*i+1] = vel[2*nidx+1];
    }
    
    PetscMemzero(el_F,sizeof(PetscScalar)*bpe_p*dof);
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    if (data->assume_velocity_incompressible) {
      for (q=0; q<nqp; q++) {
        PetscReal x_qp,gradx_qp[2],vel_q[2];
        
        function[q] = coeff[e*nqp + q];
        
        /* get x at each quadrature point */
        x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
        for (i=0; i<bpe; i++) {
          x_qp        += tab_N[q][i] * el_x[i];
          gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
          gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
        }
        
        /* get velocity at each quadrature point */
        vel_q[0] = vel_q[1] = 0.0;
        for (i=0; i<bpe; i++) {
          vel_q[0] += tab_N[q][i] * el_v[2*i  ];
          vel_q[1] += tab_N[q][i] * el_v[2*i+1];
        }
        
        for (i=0; i<bpe_p; i++) {
          PetscScalar residual = 0.0;
          
          residual += kfac*(tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1]);
          residual += alphafac * tab_N[q][i] * x_qp - tab_N[q][i] * function[q];
          
          residual += tab_N[q][i] * (vel_q[0]*gradx_qp[0] + vel_q[1]*gradx_qp[1]);
          
          el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
        }
      }
    } else {
      for (q=0; q<nqp; q++) {
        PetscReal x_qp,gradx_qp[2],vel_q[2],div_vel_q;
        
        function[q] = coeff[e*nqp + q];
        
        /* get x at each quadrature point */
        x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
        for (i=0; i<bpe; i++) {
          x_qp        += tab_N[q][i] * el_x[i];
          gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
          gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
        }
        
        /* get velocity at each quadrature point */
        div_vel_q = 0.0;
        vel_q[0] = vel_q[1] = 0.0;
        for (i=0; i<bpe; i++) {
          vel_q[0] += tab_N[q][i] * el_v[2*i  ];
          vel_q[1] += tab_N[q][i] * el_v[2*i+1];
          div_vel_q += tab_dNdx[q][i] * el_v[2*i+0] + tab_dNdy[q][i] * el_v[2*i+1];
        }
        
        for (i=0; i<bpe_p; i++) {
          PetscScalar residual = 0.0;
          
          residual += kfac*(tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1]);
          residual += alphafac * tab_N[q][i] * x_qp - tab_N[q][i] * function[q];
          
          residual += tab_N[q][i] * (vel_q[0]*gradx_qp[0] + vel_q[1]*gradx_qp[1]);
          residual += tab_N[q][i] * (div_vel_q * x_qp);
          
          el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
        }
      }
    }
    
    idx = &element_p[bpe_p*e];
    for (i=0; i<bpe_p; i++) {
      F[idx[i]] += el_F[i];
    }
  }
  
  PetscFree(detJ);
  PetscFree(function);
  PetscFree(elcoords);
  PetscFree(el_x);
  PetscFree(el_v);
  PetscFree(el_F);
  
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunction_PDEHelmholtz_PetrovGalerkin"
PetscErrorCode FormFunction_PDEHelmholtz_PetrovGalerkin(SNES snes,Vec X,Vec F_p,DM dm_p)
{
  PetscErrorCode ierr;
  PDE pde;
  PDEHelmholtz *data;
  Vec Xloc,Floc;
  PetscScalar *LA_Xloc,*LA_Floc;
  DM dm = NULL;
  PetscInt pk,pk_p;
  
  ierr = SNESGetApplicationContext(snes,(void*)&pde);CHKERRQ(ierr);
  data = (PDEHelmholtz*)pde->data;
  dm = data->dm;
  
  /* Implementation limitation requires the degree of the function spaces matches */
  ierr = DMTetGetBasisOrder(dm,&pk);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm_p,&pk_p);CHKERRQ(ierr);
  if (pk != pk_p) SETERRQ2(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Test and trial function space degree must match. Test function pk = %D : Rrial function pk = %D",pk_p,pk);
  
  Xloc = pde->xlocal[0];
  ierr = DMGetLocalVector(dm_p,&Floc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dm,X,INSERT_VALUES,Xloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,X,INSERT_VALUES,Xloc);CHKERRQ(ierr);
  pde->local_vector_valid = PETSC_TRUE;
  
  ierr = CoefficientEvaluate(data->k);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->alpha);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->q_N);CHKERRQ(ierr);
  
  ierr = VecGetArray(Xloc,&LA_Xloc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(Floc);CHKERRQ(ierr);
  ierr = VecGetArray(Floc,&LA_Floc);CHKERRQ(ierr);
  
  /* local residual evaluation */
  /* boundary terms: Explictly drop these terms */
  //ierr = PDEHelmholtz_AssembleResidualNeumann_Local(data,LA_Floc);CHKERRQ(ierr);
  
  /* volume terms */
  if (!data->velocity) {
    ierr = PDEHelmholtz_AssembleResidual_Local_PetrovGalerkin(dm_p,data,LA_Xloc,LA_Floc);CHKERRQ(ierr);
  } else {
    Vec Vloc;
    PetscScalar *LA_Vloc;
    
    ierr = DMGetLocalVector(data->dmV,&Vloc);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(data->dmV,data->velocity,INSERT_VALUES,Vloc);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(data->dmV,data->velocity,INSERT_VALUES,Vloc);CHKERRQ(ierr);
    
    ierr = VecGetArray(Vloc,&LA_Vloc);CHKERRQ(ierr);
    ierr = PDEHelmholtzAdv_AssembleResidual_Local_PetrovGalerkin(dm_p,data,LA_Vloc,LA_Xloc,LA_Floc);CHKERRQ(ierr);
    ierr = VecRestoreArray(Vloc,&LA_Vloc);CHKERRQ(ierr);
    
    ierr = DMRestoreLocalVector(data->dmV,&Vloc);CHKERRQ(ierr);
  }
  ierr = VecRestoreArray(Floc,&LA_Floc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Xloc,&LA_Xloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(F_p);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dm_p,Floc,ADD_VALUES,F_p);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm_p,Floc,ADD_VALUES,F_p);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dm_p,&Floc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "FormBilinear_PDEHelmholtz"
PetscErrorCode FormBilinear_PDEHelmholtz(SNES snes,Vec x,Mat A,Mat B,void *ctx)
{
  PetscErrorCode ierr;
  PDE pde;
  PDEHelmholtz *data;
  PetscInt  e,nel,npe,i,j;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *coords,*Ke;
  PetscInt q,nqp,nb;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  IS ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM dm = NULL;
  PetscReal kfac,alphafac;
  
  ierr = SNESGetApplicationContext(snes,(void*)&pde);CHKERRQ(ierr);
  data = (PDEHelmholtz*)pde->data;
  dm = data->dm;
  bc = data->dirichletbc;
  ierr = SNESGetDM(snes,&dm);CHKERRQ(ierr);
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&npe,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  nbasis = npe;
  
  PetscMalloc1(2*nbasis,&elcoords);
  PetscMalloc1(nbasis,&eldofs);
  PetscMalloc1(nbasis*nbasis,&Ke);
  
  ierr = CoefficientEvaluate(data->k);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->alpha);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);

  ierr = CoefficientGetDomainConstant(data->k,&kfac);CHKERRQ(ierr);
  ierr = CoefficientGetDomainConstant(data->alpha,&alphafac);CHKERRQ(ierr);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = ISCreateStride(PETSC_COMM_WORLD,NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog, &GINDICES);CHKERRQ(ierr);
  ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  
  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    /* generate dofs */
    for (i=0; i<nbasis; i++) {
      const int NID = elnidx[i];
      
      eldofs[i] = GINDICES[NID];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = elnidx[i];
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Ni,*dNidx,*dNidy;
      
      /* get access to element->quadrature points */
      Ni    = tab_N[q];
      dNidx = tab_dNdx[q];
      dNidy = tab_dNdy[q];
      
      fac = PetscAbsReal(detJ[q]) * w[q];
      
      for (i=0; i<nbasis; i++) {
        for (j=0; j<nbasis; j++) {
          Ke[ i*nbasis + j ] += fac * (kfac*(dNidx[i]*dNidx[j] + dNidy[i]*dNidy[j]) + alphafac*Ni[i]*Ni[j]);
        }
      }
      
    } // quadrature
    
    //for (i=0; i<nbasis*nbasis; i++) {
    //  if (PetscAbsReal(Ke[i]) < 1.0e-12) {
    //    Ke[i] = 0.0;
    //  }
    //}
    ierr = MatSetValuesLocal(B,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = BCListInsertScaling(B,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = ISRestoreIndices(ltog, &GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  PetscFree(w);
  PetscFree(xi);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Ke);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormBilinear_PDEHelmholtzAdv"
PetscErrorCode FormBilinear_PDEHelmholtzAdv(SNES snes,Vec x,Mat A,Mat B,void *ctx)
{
  PetscErrorCode ierr;
  PDE pde;
  PDEHelmholtz *data;
  PetscInt  e,nel,npe,i,j;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *elcoords,*elv;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *coords,*Ke;
  PetscInt q,nqp,nb;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  IS ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM dm = NULL;
  PetscReal kfac,alphafac;
  Vec Vloc;
  PetscScalar *LA_Vloc;
  
  ierr = SNESGetApplicationContext(snes,(void*)&pde);CHKERRQ(ierr);
  data = (PDEHelmholtz*)pde->data;
  dm = data->dm;
  bc = data->dirichletbc;
  ierr = SNESGetDM(snes,&dm);CHKERRQ(ierr);
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&npe,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  nbasis = npe;
  
  PetscMalloc1(2*nbasis,&elcoords);
  PetscMalloc1(2*nbasis,&elv);
  PetscMalloc1(nbasis,&eldofs);
  PetscMalloc1(nbasis*nbasis,&Ke);
  
  ierr = CoefficientEvaluate(data->k);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->alpha);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);
  
  ierr = CoefficientGetDomainConstant(data->k,&kfac);CHKERRQ(ierr);
  ierr = CoefficientGetDomainConstant(data->alpha,&alphafac);CHKERRQ(ierr);
  
  ierr = DMGetLocalVector(data->dmV,&Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(data->dmV,data->velocity,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(data->dmV,data->velocity,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = VecGetArray(Vloc,&LA_Vloc);CHKERRQ(ierr);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = ISCreateStride(PETSC_COMM_WORLD,NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog, &GINDICES);CHKERRQ(ierr);
  ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  
  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    /* generate dofs */
    for (i=0; i<nbasis; i++) {
      const int NID = elnidx[i];
      
      eldofs[i] = GINDICES[NID];
    }
    
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = elnidx[i];
 
      /* get element coordinates */
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
 
      /* get element velocity */
      elv[2*i  ] = LA_Vloc[2*nidx  ];
      elv[2*i+1] = LA_Vloc[2*nidx+1];
    }
    
    /* compute derivatives */
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);
    
    if (data->assume_velocity_incompressible) {
      for (q=0; q<nqp; q++) {
        PetscReal fac;
        PetscReal *Ni,*dNidx,*dNidy;
        PetscReal vel_q[2];
        
        /* get access to element->quadrature points */
        Ni    = tab_N[q];
        dNidx = tab_dNdx[q];
        dNidy = tab_dNdy[q];
        
        vel_q[0] = vel_q[1] = 0.0;
        for (i=0; i<nbasis; i++) {
          vel_q[0] += Ni[i] * elv[2*i+0];
          vel_q[1] += Ni[i] * elv[2*i+1];
        }
        
        fac = PetscAbsReal(detJ[q]) * w[q];
        
        // \int (w dot grad(u)) v
        for (i=0; i<nbasis; i++) {
          for (j=0; j<nbasis; j++) {
            Ke[ i*nbasis + j ] += fac * ( kfac*(dNidx[i]*dNidx[j] + dNidy[i]*dNidy[j]) + alphafac*Ni[i]*Ni[j] );
            Ke[ i*nbasis + j ] += fac * Ni[i] * (vel_q[0]*dNidx[j] + vel_q[1]*dNidy[j]);
          }
        }
      } // quadrature
    } else {
      for (q=0; q<nqp; q++) {
        PetscReal fac;
        PetscReal *Ni,*dNidx,*dNidy;
        PetscReal vel_q[2],div_vel_q;
        
        /* get access to element->quadrature points */
        Ni    = tab_N[q];
        dNidx = tab_dNdx[q];
        dNidy = tab_dNdy[q];

        div_vel_q = 0.0;
        vel_q[0] = vel_q[1] = 0.0;
        for (i=0; i<nbasis; i++) {
          vel_q[0] += Ni[i] * elv[2*i+0];
          vel_q[1] += Ni[i] * elv[2*i+1];
          div_vel_q += dNidx[i] * elv[2*i+0] + dNidy[i] * elv[2*i+1];
        }
        
        fac = PetscAbsReal(detJ[q]) * w[q];
        
        // \int (w dot grad(u)) v
        for (i=0; i<nbasis; i++) {
          for (j=0; j<nbasis; j++) {
            Ke[ i*nbasis + j ] += fac * ( kfac*(dNidx[i]*dNidx[j] + dNidy[i]*dNidy[j]) + alphafac*Ni[i]*Ni[j] );
            Ke[ i*nbasis + j ] += fac * Ni[i] * (vel_q[0]*dNidx[j] + vel_q[1]*dNidy[j]);
            Ke[ i*nbasis + j ] += fac * Ni[i] * (div_vel_q*Ni[j]);
          }
        }
      } // quadrature
    }
    
    //for (i=0; i<nbasis*nbasis; i++) {
    //  if (PetscAbsReal(Ke[i]) < 1.0e-12) {
    //    Ke[i] = 0.0;
    //  }
    //}
    ierr = MatSetValuesLocal(B,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = BCListInsertScaling(B,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = ISRestoreIndices(ltog, &GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArray(Vloc,&LA_Vloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(data->dmV,&Vloc);CHKERRQ(ierr);
  
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  PetscFree(w);
  PetscFree(xi);
  PetscFree(detJ);
  PetscFree(elv);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Ke);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEConfigureSNES_Helmholtz"
PetscErrorCode PDEConfigureSNES_Helmholtz(PDE pde,SNES snes,Vec F,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  data = (PDEHelmholtz*)pde->data;
  
  ierr = SNESSetApplicationContext(snes,(void*)pde);CHKERRQ(ierr);
  //ierr = PetscObjectReference((PetscObject)pde);CHKERRQ(ierr);
  
  ierr = SNESSetFunction(snes,F,pde->ops->form_function,(void*)pde);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes,A,B,pde->ops->form_jacobian,(void*)pde);CHKERRQ(ierr);

  if (!pde->x) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must call PDESetSolution() first");
  }
  ierr = SNESSetSolution(snes,pde->x);CHKERRQ(ierr);
  
  if (!data->dm) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must provide a valid DM - Call PDEHelmholtzSetData()");
  } else { /* check its a DMTET */
    PetscBool istet = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)data->dm,DMTET,&istet);CHKERRQ(ierr);
    if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for DMTET");
  }
  ierr = SNESSetDM(snes,data->dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDestroy_Helmholtz"
PetscErrorCode PDEDestroy_Helmholtz(PDE pde)
{
  PetscErrorCode ierr;
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  data = (PDEHelmholtz*)pde->data;

  ierr = DMDestroy(&data->dm);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->k);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->alpha);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->g);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->q_N);CHKERRQ(ierr);
  if (data->velocity) { ierr = VecDestroy(&data->velocity);CHKERRQ(ierr); }
  if (data->dmV) { ierr = DMDestroy(&data->dmV);CHKERRQ(ierr); }
  ierr = PetscFree(data);CHKERRQ(ierr);
  pde->data = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEUpdateLocal_Helmholtz"
PetscErrorCode PDEUpdateLocal_Helmholtz(PDE pde)
{
  PetscErrorCode ierr;
  PDEHelmholtz *data = (PDEHelmholtz*)pde->data;
  Vec X;
  
  PetscFunctionBegin;
  
  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(data->dm,X,INSERT_VALUES,pde->xlocal[0]);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(data->dm,X,INSERT_VALUES,pde->xlocal[0]);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreate_Helmholtz"
PetscErrorCode PDECreate_Helmholtz(PDE pde)
{
  PetscErrorCode ierr;
  PDEHelmholtz *data;
  MPI_Comm comm;
  
  PetscFunctionBegin;
  ierr = PetscObjectChangeTypeName((PetscObject)pde,PDETypeNames[(int)PDEHELMHOLTZ]);CHKERRQ(ierr);
  ierr = PetscStrallocpy(helmholtz_description,&pde->description);CHKERRQ(ierr);
  
  ierr = PetscNewLog(pde,&data);CHKERRQ(ierr);
  data->dm = NULL;
  data->dirichletbc = NULL;
  pde->data = data;
  
  pde->ops->configure_snes = PDEConfigureSNES_Helmholtz;
  pde->ops->form_function = FormFunction_PDEHelmholtz;
  pde->ops->form_jacobian = FormBilinear_PDEHelmholtz;
  pde->ops->destroy               = PDEDestroy_Helmholtz;
  pde->ops->update_local_solution = PDEUpdateLocal_Helmholtz;
  
  /* create coefficients */
  ierr = PetscObjectGetComm((PetscObject)pde,&comm);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->k);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->alpha);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->g);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->q_N);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->k,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->alpha,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->g,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->q_N,pde);CHKERRQ(ierr);
  
  {
    Quadrature quadrature;
    
    ierr = CoefficientSetType(data->k,COEFF_DOMAIN_CONST);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(data->k,1.0);CHKERRQ(ierr);

    ierr = CoefficientSetType(data->alpha,COEFF_DOMAIN_CONST);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(data->alpha,1.0);CHKERRQ(ierr);

    ierr = CoefficientSetType(data->g,COEFF_QUADRATURE);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(data->g,&quadrature);CHKERRQ(ierr);
    
    ierr = CoefficientSetType(data->q_N,COEFF_QUADRATURE);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(data->q_N,&quadrature);CHKERRQ(ierr);
    /* set default method to jam zeros into the quadrature values */
    //ierr = CoefficientSetComputeQuadrature(data->q_N,CoefficientQuadratureEvaluator_NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadratureEmpty(data->q_N);CHKERRQ(ierr);

  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzSetData"
PetscErrorCode PDEHelmholtzSetData(PDE pde,DM dm,BCList list)
{
  PDEHelmholtz *data;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEHelmholtz*)pde->data;
  ierr = PetscObjectReference((PetscObject)dm);CHKERRQ(ierr);
  if (data->dm) {
    ierr = DMDestroy(&data->dm);CHKERRQ(ierr);
  }
  data->dm = dm;
  data->dirichletbc = list;
  
  {
    Quadrature quadrature;
    PetscInt nelements;
    
    ierr = CoefficientGetQuadrature(data->g,&quadrature);CHKERRQ(ierr);
    ierr = CoefficientSetRegionsFromDM(data->g,dm);CHKERRQ(ierr);
    
    ierr = QuadratureSetUpFromDM(quadrature,dm);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(dm,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"q_g0",1);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"q_g1",1);CHKERRQ(ierr);
  }
  {
    Quadrature quadrature;
    BFacetList bfacet_list;
    PetscInt nfacets;
    
    ierr = CoefficientGetQuadrature(data->q_N,&quadrature);CHKERRQ(ierr);
    
    ierr = QuadratureSetUpFromDM_Facet(quadrature,dm);CHKERRQ(ierr);

    ierr = DMTetGetGeometryBFacetList(dm,&bfacet_list);CHKERRQ(ierr);
    ierr = BFacetListGetSizes(bfacet_list,&nfacets,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nfacets,"q_N",2);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzSetVelocity"
PetscErrorCode PDEHelmholtzSetVelocity(PDE pde,DM dmV,Vec velocity)
{
  PDEHelmholtz *data;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEHelmholtz*)pde->data;
  ierr = PetscObjectReference((PetscObject)dmV);CHKERRQ(ierr);
  if (data->dmV) { ierr = DMDestroy(&data->dmV);CHKERRQ(ierr); }
  data->dmV = dmV;

  ierr = PetscObjectReference((PetscObject)velocity);CHKERRQ(ierr);
  if (data->velocity) { ierr = VecDestroy(&data->velocity);CHKERRQ(ierr); }
  data->velocity = velocity;
  data->assume_velocity_incompressible = PETSC_FALSE;
  
  pde->ops->form_function = FormFunction_PDEHelmholtz;
  pde->ops->form_jacobian = FormBilinear_PDEHelmholtzAdv;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzSetAssumeVelocityIncompressible"
PetscErrorCode PDEHelmholtzSetAssumeVelocityIncompressible(PDE pde,PetscBool value)
{
  PDEHelmholtz *data;
  PetscFunctionBegin;
  data = (PDEHelmholtz*)pde->data;
  data->assume_velocity_incompressible = value;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreateHelmholtz"
PetscErrorCode PDECreateHelmholtz(MPI_Comm comm,DM dm,BCList list,PDE *pde)
{
  PDE _pde;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PDECreate(comm,&_pde);CHKERRQ(ierr);
  ierr = PDESetType(_pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(_pde,dm,list);CHKERRQ(ierr);
  *pde = _pde;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzGetCoefficient_g"
PetscErrorCode PDEHelmholtzGetCoefficient_g(PDE pde,Coefficient *c)
{
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  if (!c) PetscFunctionReturn(0);
  data = (PDEHelmholtz*)pde->data;
  *c = data->g;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzGetCoefficient_k"
PetscErrorCode PDEHelmholtzGetCoefficient_k(PDE pde,Coefficient *c)
{
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  if (!c) PetscFunctionReturn(0);
  data = (PDEHelmholtz*)pde->data;
  *c = data->k;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzGetCoefficient_alpha"
PetscErrorCode PDEHelmholtzGetCoefficient_alpha(PDE pde,Coefficient *c)
{
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  if (!c) PetscFunctionReturn(0);
  data = (PDEHelmholtz*)pde->data;
  *c = data->alpha;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzGetCoefficient_qN"
PetscErrorCode PDEHelmholtzGetCoefficient_qN(PDE pde,Coefficient *c)
{
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  if (!c) PetscFunctionReturn(0);
  data = (PDEHelmholtz*)pde->data;
  *c = data->q_N;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzGetDM"
PetscErrorCode PDEHelmholtzGetDM(PDE pde,DM *dm)
{
  PDEHelmholtz *data;
  
  PetscFunctionBegin;
  if (!dm) PetscFunctionReturn(0);
  data = (PDEHelmholtz*)pde->data;
  *dm = data->dm;
  PetscFunctionReturn(0);
}
