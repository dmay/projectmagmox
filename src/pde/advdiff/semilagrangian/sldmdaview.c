

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>
#include <dmsl.h>

#undef __FUNCT__
#define __FUNCT__ "SLDMView"
PetscErrorCode SLDMView(DMSemiLagrangian sl,const char prefix[])
{
  PetscErrorCode ierr;
  DM da,cda;
  Vec coor;
  const PetscScalar *LA_coor,*LA_phi_dep,*LA_vel;
  PetscInt nx,ny,mx,my,i,j;
  FILE*	vtk_fp = NULL;
  char name[PETSC_MAX_PATH_LEN];
  PetscBool isda;
  FVStorage fv;
  PetscBool conservative;
  
  PetscFunctionBegin;

  da = sl->dm;
  PetscObjectTypeCompare((PetscObject)da,"da",&isda);
  if (!isda) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Only valid for DMDA");
  
  PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"%s.vts",prefix);
  if ((vtk_fp = fopen(name,"w")) == NULL)  {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name);
  }
  
  conservative = PETSC_FALSE;
  if (sl->sl_type == SLTYPE_FVCONSERVATIVE) { conservative = PETSC_TRUE; }
  if (sl->sl_type == SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE) { conservative = PETSC_TRUE; }
  if (sl->sl_type == SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE) { conservative = PETSC_TRUE; }
  fv = sl->fv;
  
  ierr = DMDAGetCorners(da,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  mx = nx - 1;
  my = ny - 1;
  
  ierr = DMGetCoordinateDM(da,&cda);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(sl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecGetArrayRead(sl->velocity,&LA_vel);CHKERRQ(ierr);
  
  
  
  if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
#ifdef WORDSIZE_BIGENDIAN
  fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
  fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF, vtk_fp, "  <StructuredGrid WholeExtent=\"%D %D %D %D %D %D\">\n", 0,nx-1, 0,ny-1, 0,0);
  PetscFPrintf(PETSC_COMM_SELF, vtk_fp, "    <Piece Extent=\"%D %D %D %D %D %D\">\n", 0,nx-1, 0,ny-1, 0,0);
  
  /* VTS COORD DATA */
  fprintf( vtk_fp, "    <Points>\n");
  fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp, "      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf( vtk_fp,"%1.6e %1.6e %1.6e ", LA_coor[2*(i+j*nx)], LA_coor[2*(i+j*nx)+1], 0.0 );
    }
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");
  fprintf( vtk_fp, "    </Points>\n");
  
  
  /* VTS NODAL DATA */
  fprintf( vtk_fp, "    <PointData>\n");

  /* velocity */
  fprintf( vtk_fp, "      <DataArray Name=\"velocity\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp,"      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf( vtk_fp,"%1.6e %1.6e %1.6e ", LA_vel[2*(i+j*nx)], LA_vel[2*(i+j*nx)+1], 0.0 );
    }
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");
  
  if (sl->sl_type == SLTYPE_CLASSIC) {
    fprintf( vtk_fp, "      <DataArray Name=\"phi\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf( vtk_fp,"%1.6e ", LA_phi_dep[i+j*nx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");
  }
  
  if (conservative) {
    fprintf( vtk_fp, "      <DataArray Name=\"vertex_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf( vtk_fp,"%d ", (int)fv->vertex_type[i+j*nx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");

    fprintf( vtk_fp, "      <DataArray Name=\"vertex_loc_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf( vtk_fp,"%d ", (int)fv->vertex_loc_type[i+j*nx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");
}

  fprintf( vtk_fp, "    </PointData>\n");

  /* VTS CELL DATA */
  fprintf( vtk_fp, "    <CellData>\n");

  if (conservative) {
    fprintf( vtk_fp, "      <DataArray Name=\"phi\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf( vtk_fp,"%1.6e ", LA_phi_dep[i+j*mx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");
  }
  
  if (conservative) {
    fprintf( vtk_fp, "      <DataArray Name=\"cell_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf( vtk_fp,"%d ", (int)fv->cell_type[i+j*mx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");

    fprintf( vtk_fp, "      <DataArray Name=\"bcell_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf( vtk_fp,"%d ", (int)fv->bcell_type[i+j*mx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");

    fprintf( vtk_fp, "      <DataArray Name=\"cell_loc_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf( vtk_fp,"%d ", (int)fv->cell_loc_type[i+j*mx]);
      }
    }
    fprintf( vtk_fp, "\n      </DataArray>\n");

  }
    
  fprintf( vtk_fp, "    </CellData>\n");

  
  /* VTS HEADER - CLOSE */
  fprintf( vtk_fp, "    </Piece>\n");
  fprintf( vtk_fp, "  </StructuredGrid>\n");
  fprintf( vtk_fp, "</VTKFile>\n");
  
  ierr = VecRestoreArrayRead(sl->velocity,&LA_vel);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(sl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);

  
  fclose( vtk_fp );
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ConservativeSLView"
PetscErrorCode ConservativeSLView(DMSemiLagrangian sl,const char prefix[])
{
  PetscErrorCode ierr;
  DM da,dacell,cda;
  Vec coor;
  const PetscScalar *LA_coor,*LA_phi_dep;
  PetscInt nx,ny,i,j,cnt;
  FILE*	vtk_fp = NULL;
  char name[PETSC_MAX_PATH_LEN];
  PetscBool isda;
  
  PetscFunctionBegin;
  
  da = sl->dm;
  PetscObjectTypeCompare((PetscObject)da,"da",&isda);
  if (!isda) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Only valid for DMDA");
  
  PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"%s.vtu",prefix);
  if ((vtk_fp = fopen(name,"w")) == NULL)  {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name);
  }
  
  if (sl->sl_type == SLTYPE_CLASSIC) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Only valid for ConservativeSL");
  }
  
  dacell = sl->fv->dmcv;
  ierr = DMDAGetCorners(dacell,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  
  ierr = DMGetCoordinateDM(dacell,&cda);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dacell,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(sl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  
  cnt = 0;
  for (i=0; i<nx*ny; i++) {
    if (sl->fv->subcell[i]->valid) {
      cnt = cnt + (sl->fv->subcell[i]->mx * sl->fv->subcell[i]->my);
    }
  }
  
  
  if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
#ifdef WORDSIZE_BIGENDIAN
  fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
  fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
  
  fprintf( vtk_fp, "  <UnstructuredGrid>\n");
  fprintf( vtk_fp, "    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n", nx*ny+cnt,nx*ny+cnt);
  
  fprintf( vtk_fp, "    <Cells>\n");

  fprintf( vtk_fp, "      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
  for (i=0; i<nx*ny+cnt; i++) {
    fprintf( vtk_fp,"%d ",i);
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");
  
  fprintf( vtk_fp, "      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
  for (i=0; i<nx*ny+cnt; i++) {
    fprintf( vtk_fp,"%d ",i+1);
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");
  
  // types //
  fprintf( vtk_fp, "      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
  for (i=0; i<nx*ny+cnt; i++) {
    fprintf( vtk_fp,"%d ",1); /* VTK_VERTEX */
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");
  fprintf( vtk_fp, "    </Cells>\n");

  
  
  /* coordinates */
  fprintf( vtk_fp, "    <Points>\n");
  fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp, "      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf( vtk_fp,"%1.6e %1.6e %1.6e ", LA_coor[2*(i+j*nx)], LA_coor[2*(i+j*nx)+1], 0.0 );
    }
  }
  for (i=0; i<nx*ny; i++) {
    if (sl->fv->subcell[i]->valid) {
      for (j=0; j<sl->fv->subcell[i]->mx * sl->fv->subcell[i]->my; j++) {
        fprintf( vtk_fp,"%1.6e %1.6e %1.6e ", sl->fv->subcell[i]->centroid[2*j], sl->fv->subcell[i]->centroid[2*j+1], 0.0 );
      }
    }
  }

  fprintf( vtk_fp, "\n      </DataArray>\n");
  fprintf( vtk_fp, "    </Points>\n");
  
  
  /* point data */
  fprintf( vtk_fp, "    <PointData>\n");

  fprintf( vtk_fp, "      <DataArray Name=\"phi\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf( vtk_fp,"      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf( vtk_fp,"%1.6e ", LA_phi_dep[i+j*nx]);
    }
  }

  for (i=0; i<nx*ny; i++) {
    if (sl->fv->subcell[i]->valid) {
      for (j=0; j<sl->fv->subcell[i]->mx * sl->fv->subcell[i]->my; j++) {
        fprintf( vtk_fp,"%1.6e ", sl->fv->subcell[i]->phi[j]);
      }
    }
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");

  
  fprintf( vtk_fp, "      <DataArray Name=\"bc_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf( vtk_fp,"      ");
  for (j=0; j<nx*ny; j++) {
    fprintf( vtk_fp,"%d ", -2);
  }
  for (i=0; i<nx*ny; i++) {
    if (sl->fv->subcell[i]->valid) {
      for (j=0; j<sl->fv->subcell[i]->mx * sl->fv->subcell[i]->my; j++) {
        fprintf( vtk_fp,"%d ", sl->fv->subcell[i]->bc_type[j] );
      }
    }
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");

  fprintf( vtk_fp, "      <DataArray Name=\"state\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf( vtk_fp,"      ");
  for (j=0; j<nx*ny; j++) {
    fprintf( vtk_fp,"%d ", -2);
  }
  for (i=0; i<nx*ny; i++) {
    if (sl->fv->subcell[i]->valid) {
      for (j=0; j<sl->fv->subcell[i]->mx * sl->fv->subcell[i]->my; j++) {
        fprintf( vtk_fp,"%d ", sl->fv->subcell[i]->state[j] );
      }
    }
  }
  fprintf( vtk_fp, "\n      </DataArray>\n");

  
  fprintf( vtk_fp, "    </PointData>\n");
  
  /* cell data */
  fprintf( vtk_fp, "    <CellData>\n");
  fprintf( vtk_fp, "    </CellData>\n");
  
  
  /* VTS HEADER - CLOSE */
  fprintf( vtk_fp, "    </Piece>\n");
  fprintf( vtk_fp, "  </UnstructuredGrid>\n");
  fprintf( vtk_fp, "</VTKFile>\n");
  
  ierr = VecRestoreArrayRead(sl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  
  fclose( vtk_fp );
  PetscFunctionReturn(0);
}
