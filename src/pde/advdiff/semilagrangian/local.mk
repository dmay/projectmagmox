libproj-y.c += $(call thisdir, \
   semilagrangian_da.c \
   dmsl.c \
   sldmdaview.c \
)

PROJ_INC += -I$(abspath $(call thisdir,.))

include $(call incsubdirs,ficticious_domain)
