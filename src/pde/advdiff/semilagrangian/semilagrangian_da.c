
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>
#include <dmsl.h>

#define SL_EPS 1.0e-8
#define SL_MIN_VELOCITY 1.0e-12

PetscErrorCode DMSemiLagrangianUpdate_DMDA(DMSemiLagrangian dsl,PetscReal dt,Vec phi);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_v2(DMSemiLagrangian dasl,PetscReal dt,Vec phi);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3(DMSemiLagrangian dasl,PetscReal time,PetscReal dt,Vec phi);
PetscErrorCode DMDASemiLagrangian_FVConservative_DirichletEvaluate(DMSemiLagrangian sl,PetscReal time);
PetscErrorCode DMDASemiLagrangian_FVConservative_RestrictVelocity(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_ReconstructVelocity(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_SetUpBoundary(DMSemiLagrangian sl);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify(DMSemiLagrangian dasl);


#undef __FUNCT__
#define __FUNCT__ "SLPointInsideDomain_DMDA"
PetscErrorCode SLPointInsideDomain_DMDA(SLDMDA s,PetscReal pos[],PetscBool *inside)
{
  *inside  = PETSC_TRUE;
  if (pos[0] < (s->xrange[0] - SL_EPS)) { *inside = PETSC_FALSE; PetscFunctionReturn(0); }
  if (pos[0] > (s->xrange[1] + SL_EPS)) { *inside = PETSC_FALSE; PetscFunctionReturn(0); }
  if (pos[1] < (s->yrange[0] - SL_EPS)) { *inside = PETSC_FALSE; PetscFunctionReturn(0); }
  if (pos[1] > (s->yrange[1] + SL_EPS)) { *inside = PETSC_FALSE; PetscFunctionReturn(0); }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePoint_RK1_DMDA"
PetscErrorCode SLComputeDeparturePoint_RK1_DMDA(SLDMDA s,PetscReal dt,PetscReal xn[],PetscReal xd[])
{
  PetscInt  ii,jj,nidx_ij;
  PetscReal vx[2];
  PetscReal xp[2],dt_min,xboundary,yboundary;
  PetscBool outside_domain;
  PetscInt  bflag_x,bflag_y;
  
  
  /* fetch velocity  - no need to interpolate */
  ii = (PetscInt)( (xn[0] - s->xrange[0])/s->dx );
  jj = (PetscInt)( (xn[1] - s->yrange[0])/s->dy );
  
  nidx_ij = DANodeIdx(s,ii,jj);
  
  vx[0] = s->LA_vel[2*nidx_ij+0];
  vx[1] = s->LA_vel[2*nidx_ij+1];
  
  xp[0] = xn[0];
  xp[1] = xn[1];
  
  /* note backwards advection */
  xp[0] = xp[0] - dt * vx[0];
  xp[1] = xp[1] - dt * vx[1];
  
  outside_domain = PETSC_FALSE;
  bflag_x = bflag_y = 0;
  
  if (xp[0] < s->xrange[0]) {
    bflag_x = -1;
    outside_domain = PETSC_TRUE;
  }
  if (xp[0] > s->xrange[1]) {
    bflag_x = 1;
    outside_domain = PETSC_TRUE;
  }
  if (xp[1] < s->yrange[0]) {
    bflag_y = -1;
    outside_domain = PETSC_TRUE;
  }
  if (xp[1] > s->yrange[1]) {
    bflag_y = 1;
    outside_domain = PETSC_TRUE;
  }
  
  /*
   Solve for
   xboundary = xd - dtx vx
   yboundary = yd - dty vy
   */
  if (outside_domain) {
    PetscReal xbound[2],ybound[2];
    
    xbound[0] = ybound[0] = -1.0e32;
    xbound[1] = ybound[1] = 1.0e32;
    
    switch (bflag_x) {
      case -1: /* left */
        xboundary = s->xrange[0];
        dt_min = (xn[0] - xboundary) / vx[0];
        xp[0] = xboundary;
        xp[1] = xn[1] - dt_min * vx[1];
        
        xbound[0] = xp[0];
        xbound[1] = xp[1];
        break;
      case  1: /* right */
        xboundary = s->xrange[1];
        dt_min = (xn[0] - xboundary) / vx[0];
        xp[0] = xboundary;
        xp[1] = xn[1] - dt_min * vx[1];
        
        xbound[0] = xp[0];
        xbound[1] = xp[1];
        break;
    }
    
    switch (bflag_y) {
      case -1: /* bottom */
        yboundary = s->yrange[0];
        dt_min = (xn[1] - yboundary) / vx[1];
        xp[0] = xn[0] - dt_min * vx[0];
        xp[1] = yboundary;
        
        ybound[0] = xp[0];
        ybound[1] = xp[1];
        break;
      case  1: /* top */
        yboundary = s->yrange[1];
        dt_min = (xn[1] - yboundary) / vx[1];
        xp[0] = xn[0] - dt_min * vx[0];
        xp[1] = yboundary;
        
        ybound[0] = xp[0];
        ybound[1] = xp[1];
        break;
    }
    
    if (bflag_x != 0) {
      if ( (xbound[0] >= s->xrange[0]) && (xbound[0] <= s->xrange[1]) ) {
        if ( (xbound[1] >= s->yrange[0]) && (xbound[1] <= s->yrange[1]) ) {
          xp[0] = xbound[0];
          xp[1] = xbound[1];
        }
      }
    }
    if (bflag_y != 0) {
      if ( (ybound[0] >= s->xrange[0]) && (ybound[0] <= s->xrange[1]) ) {
        if ( (ybound[1] >= s->yrange[0]) && (ybound[1] <= s->yrange[1]) ) {
          xp[0] = ybound[0];
          xp[1] = ybound[1];
        }
      }
    }
    
  }
  
  xd[0] = xp[0];
  xd[1] = xp[1];
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeVelocity_DMDA"
PetscErrorCode SLComputeVelocity_DMDA(SLDMDA s,PetscReal pos[],PetscReal vel[])
{
  PetscInt  ci,cj;
  PetscInt  nidx[4],k;
  PetscReal x0[2],xi[2],Ni[4];
  
  ci = (PetscInt)( (pos[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (pos[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  nidx[0] = DANodeIdx(s,ci,  cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci,  cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);
  
  x0[0] = s->xrange[0] + ci * s->dx;
  x0[1] = s->yrange[0] + cj * s->dy;
  
  /*
   [x - x0]/dx = [xi - (-1)]/2
   */
  
  xi[0] = 2.0*(pos[0]-x0[0])/s->dx - 1.0;
  xi[1] = 2.0*(pos[1]-x0[1])/s->dy - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  vel[0] = vel[1] = 0.0;
  for (k=0; k<4; k++) {
    vel[0] += Ni[k] * s->LA_vel[2*nidx[k]+0];
    vel[1] += Ni[k] * s->LA_vel[2*nidx[k]+1];
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePoint_RK4_DMDA"
PetscErrorCode SLComputeDeparturePoint_RK4_DMDA(SLDMDA s,double dt,double xn[],double xd[],DPointStatus *status)
{
  PetscErrorCode ierr;
  PetscReal k1[2],k2[2],k3[2],k4[2];
  PetscBool inside;
  PetscReal pos[2];
  
  pos[0] = xn[0];
  pos[1] = xn[1];
  ierr = SLComputeVelocity_DMDA(s,pos,k1);CHKERRQ(ierr); /* t = t_k */
  
  if (PetscSqrtReal(k1[0]*k1[0]+k1[1]*k1[1]) < SL_MIN_VELOCITY) {
    xd[0] = xn[0];
    xd[1] = xn[1];
    *status = DPOINT_FINALIZED;
    PetscFunctionReturn(0);
  }
  
  k1[0] = -k1[0];
  k1[1] = -k1[1];
  
  pos[0] = xn[0] + 0.5 * dt * k1[0];
  pos[1] = xn[1] + 0.5 * dt * k1[1];
  ierr = SLPointInsideDomain_DMDA(s,pos,&inside);CHKERRQ(ierr);
  if (!inside) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k2);CHKERRQ(ierr); /* t = t_k - 0.5*dt */
  k2[0] = -k2[0];
  k2[1] = -k2[1];
  
  pos[0] = xn[0] + 0.5 * dt * k2[0];
  pos[1] = xn[1] + 0.5 * dt * k2[1];
  ierr = SLPointInsideDomain_DMDA(s,pos,&inside);CHKERRQ(ierr);
  if (!inside) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k3);CHKERRQ(ierr); /* t = t_k - 0.5*dt */
  k3[0] = -k3[0];
  k3[1] = -k3[1];
  
  pos[0] = xn[0] + dt * k3[0];
  pos[1] = xn[1] + dt * k3[1];
  ierr = SLPointInsideDomain_DMDA(s,pos,&inside);CHKERRQ(ierr);
  if (!inside) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k4);CHKERRQ(ierr); /* t = t_k - dt */
  k4[0] = -k4[0];
  k4[1] = -k4[1];
  
  xd[0] = xn[0] + (dt/6.0)*( k1[0] + 2.0*k2[0] + 2.0*k3[0] + k4[0] );
  xd[1] = xn[1] + (dt/6.0)*( k1[1] + 2.0*k2[1] + 2.0*k3[1] + k4[1] );
  ierr = SLPointInsideDomain_DMDA(s,xd,&inside);CHKERRQ(ierr);
  if (!inside) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  
  *status = DPOINT_FINALIZED;
  //printf("%1.4e %1.4e \n",xd[0],xd[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePoint_DMDA"
PetscErrorCode SLComputeDeparturePoint_DMDA(SLDMDA s,PetscReal time_interval,PetscInt nsteps,PetscReal xn[],PetscReal xd[],PetscBool *deppoint_left_domain)
{
  PetscErrorCode ierr;
  PetscBool    point_inside;
  PetscInt     k;
  PetscReal    dt,xp[2];
  DPointStatus status;
  
  /* initialize position to be outside domain */
  xd[0] = -100.0 * s->xrange[0];
  xd[1] = -100.0 * s->yrange[0];
  *deppoint_left_domain = PETSC_FALSE;
  
  /* sub-divide interval into pieces */
  dt = time_interval / ( (PetscReal)nsteps );
  
  //printf("%1.4e %1.4e \n",xn[0],xn[1]);
  xp[0] = xn[0];
  xp[1] = xn[1];
  status = DPOINT_INIT;
  for (k=0; k<nsteps; k++) {
    ierr = SLComputeDeparturePoint_RK4_DMDA(s,dt,xp,xd,&status);CHKERRQ(ierr);
    
    if (status == DPOINT_DEPARTED_DOMAIN) { /* point left domain */
      *deppoint_left_domain = PETSC_TRUE;
      break;
    }
    xp[0] = xd[0];
    xp[1] = xd[1];
  }
  if (status == DPOINT_DEPARTED_DOMAIN) {
    ierr = SLComputeDeparturePoint_RK1_DMDA(s,dt,xp,xd);CHKERRQ(ierr);
    //printf("%1.4e %1.4e \n",xd[0],xd[1]);
  }
  //printf("\n\n");
  ierr = SLPointInsideDomain_DMDA(s,xd,&point_inside);CHKERRQ(ierr);
  if (!point_inside) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"ComputeDeparturePoint: Failed... point outside domain");
  }

  PetscFunctionReturn(0);
}

/*
 dt_cfl = min(ds) / max(norm(v))
*/
#undef __FUNCT__
#define __FUNCT__ "FDSPMComputeTimestep_CFL"
PetscErrorCode FDSPMComputeTimestep_CFL(SLDMDA s,PetscReal cfl_max,PetscScalar uh[],PetscReal *dt_cfl)
{
  PetscReal ds,vmax,vx,vy,vs;
  PetscInt k;
  
  ds = s->dx;
  if (s->dy < ds) {
    ds = s->dy;
  }
  
  vmax = 0.0;
  for (k=0; k<s->nnodes; k++) {
    vx = uh[2*k+0];
    vy = uh[2*k+1];
    vs = vx*vx + vy*vy;
    if (vs > vmax) { vmax = vs; }
  }
  if (vmax < 1.0e-20) {
    *dt_cfl = 1.0e32;
  } else {
    *dt_cfl = cfl_max * (ds / PetscSqrtReal(vmax));
  }

  PetscFunctionReturn(0);
}

PetscErrorCode SLInterpolateAtPointQ1_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M);
PetscErrorCode SLInterpolateAtPointCSpline_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *hd);
PetscErrorCode SLInterpolateAtPointQuasiMonotone_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M);

/*
 [input]
 dt : timestep
 phi[] : field
 [output]
 phi[] : field corrected for advection
*/
#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianUpdate_DMDA"
PetscErrorCode DMSemiLagrangianUpdate_DMDA(DMSemiLagrangian dsl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt nidx_ij,k;
  PetscReal xn[2],xd[2],phid;
  Vec coor;
  const PetscScalar *LA_phi;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscScalar *LA_phi_dep;
  PetscInt L,nsteps;
  static PetscInt fcnt = 0;
  FILE *fp = NULL;
  PetscInt debug = dsl->debug;
  
  if (debug) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sldp-%D.gp",fcnt);
    fp = fopen(name,"w");
    fprintf(fp,"# frame %d \n",fcnt);
  }
  
  ierr = VecGetSize(phi,&L);CHKERRQ(ierr);
  ierr = VecZeroEntries(dsl->phi_dep);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dsl->dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dsl->velocity,&LA_velocity);CHKERRQ(ierr);
  dsl->dactx->LA_vel = (PetscScalar*)LA_velocity;
  ierr = VecGetArray(dsl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);

  nsteps = 1;
  if (!dsl->trajectory_disable_substep) {
    PetscReal dt_cfl;
    
    ierr = FDSPMComputeTimestep_CFL(dsl->dactx,dsl->trajectory_cfl,(PetscScalar*)LA_velocity,&dt_cfl);CHKERRQ(ierr);
    if (dt > dt_cfl) {
      nsteps = (PetscInt)(dt / dt_cfl) + 1;
    }
  }
  printf("  #RK4 steps per characteristic = %d \n",nsteps);
  
  for (k=0; k<L; k++) {
    PetscBool dep_left;
    
    nidx_ij = k;
  
    /* determine departure point */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    ierr = SLComputeDeparturePoint_DMDA(dsl->dactx,dt,nsteps,xn,xd,&dep_left);CHKERRQ(ierr);
    
    /* interpolate phi[] at the departure point */
    phid = 0.0;
    switch (dsl->dactx->interpolation_type) {
      case DASLInterp_Q1:
        ierr = SLInterpolateAtPointQ1_DMDA(dsl->dactx,xd,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_CSpline:
        ierr = SLInterpolateAtPointCSpline_DMDA(dsl->dactx,xd,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_QMonotone:
        ierr = SLInterpolateAtPointQuasiMonotone_DMDA(dsl->dactx,xd,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
        break;
    }
    /* Update phi */
    LA_phi_dep[nidx_ij] = phid;
    
    if (debug > 0) {
      fprintf(fp,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e\n",xn[0],xn[1],LA_phi[k], xd[0],xd[1],LA_phi_dep[k]);
    }
  }
  
  ierr = VecRestoreArray(dsl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(dsl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecCopy(dsl->phi_dep,phi);CHKERRQ(ierr); /* phi <-- phi_departure */
  if (debug) {
    fcnt++;
    fclose(fp);
  }
  PetscFunctionReturn(0);
}

/*
 [input]
 xd[] : single departure point
 h[] : array of all heights
 [output]
 hd : interpolated height at departure point
*/
/* interpolate in y first, then x : arbitrary choice */
#undef __FUNCT__
#define __FUNCT__ "SLInterpolateAtPointQ1_DMDA"
PetscErrorCode SLInterpolateAtPointQ1_DMDA(SLDMDA s,PetscReal xd[],PetscScalar h[],PetscReal *_phi_M)
{
  PetscInt ci,cj,k;
  PetscInt nidx[4];
  PetscReal hel[4],Ni[4],hint,x0[2],xi[2];
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = DANodeIdx(s,ci  ,cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci  ,cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);

  x0[0] = s->xrange[0] + ci * s->dx;
  x0[1] = s->yrange[0] + cj * s->dy;

  xi[0] = 2.0*(xd[0]-x0[0])/s->dx - 1.0;
  xi[1] = 2.0*(xd[1]-x0[1])/s->dy - 1.0;

  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  for (k=0; k<4; k++) {
    hel[k] = h[nidx[k]];
  }
  
  hint = 0.0;
  for (k=0; k<4; k++) {
    hint += Ni[k] * hel[k];
  }
  *_phi_M = hint;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLInterpolateDOFAtPointQ1_DMDA"
PetscErrorCode SLInterpolateDOFAtPointQ1_DMDA(SLDMDA s,PetscReal xd[],PetscInt ndof,PetscScalar field[],PetscReal val[])
{
  PetscInt ci,cj,k,d;
  PetscInt nidx[4];
  PetscReal field_el[4],Ni[4],field_int,x0[2],xi[2];
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = DANodeIdx(s,ci  ,cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci  ,cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);
  
  x0[0] = s->xrange[0] + ci * s->dx;
  x0[1] = s->yrange[0] + cj * s->dy;
  
  xi[0] = 2.0*(xd[0]-x0[0])/s->dx - 1.0;
  xi[1] = 2.0*(xd[1]-x0[1])/s->dy - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  for (d=0; d<ndof; d++) {
    for (k=0; k<4; k++) {
      field_el[k] = field[ndof*nidx[k]+d];
    }
    field_int = 0.0;
    for (k=0; k<4; k++) {
      field_int += Ni[k] * field_el[k];
    }
    val[d] = field_int;
  }
  
  PetscFunctionReturn(0);
}

/* Maple generated code */
#undef __FUNCT__
#define __FUNCT__ "ComputeMatrixInverse_3x3"
PetscErrorCode ComputeMatrixInverse_3x3(PetscReal A[3][3],PetscReal B[3][3])
{
  PetscReal t4,t6,t8,t10,t12,t14,t17;
  
  t4 = A[2][0] * A[0][1];
  t6 = A[2][0] * A[0][2];
  t8 = A[1][0] * A[0][1];
  t10 = A[1][0] * A[0][2];
  t12 = A[0][0] * A[1][1];
  t14 = A[0][0] * A[1][2];
  t17 = 0.1e1 / (t4 * A[1][2] - t6 * A[1][1] - t8 * A[2][2] + t10 * A[2][1] + t12 * A[2][2] - t14 * A[2][1]);
  
  B[0][0] = (A[1][1] * A[2][2] - A[1][2] * A[2][1]) * t17;
  B[0][1] = -(A[0][1] * A[2][2] - A[0][2] * A[2][1]) * t17;
  B[0][2] = (A[0][1] * A[1][2] - A[0][2] * A[1][1]) * t17;
  B[1][0] = -(-A[2][0] * A[1][2] + A[1][0] * A[2][2]) * t17;
  B[1][1] = (-t6 + A[0][0] * A[2][2]) * t17;
  B[1][2] = -(-t10 + t14) * t17;
  B[2][0] = (-A[2][0] * A[1][1] + A[1][0] * A[2][1]) * t17;
  B[2][1] = -(-t4 + A[0][0] * A[2][1]) * t17;
  B[2][2] = (-t8 + t12) * t17;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CubicSpline_PrepareBasis"
PetscErrorCode CubicSpline_PrepareBasis(PetscReal xn[],PetscReal B[3][3])
{
  PetscReal A[3][3];
  PetscErrorCode ierr;
  
  A[0][0] = 2.0 / (xn[1] - xn[0]);
  A[0][1] = 1.0 / (xn[1] - xn[0]);
  A[0][2] = 0.0;
  
  A[1][0] = A[0][1];
  A[1][1] = 2.0 * ( 1.0/(xn[1]-xn[0]) + 1.0/(xn[2]-xn[1]) );
  A[1][2] = 1.0/(xn[2]-xn[1]);
  
  A[2][0] = 0.0;
  A[2][1] = A[1][2];
  A[2][2] = 2.0 / (xn[2] - xn[1]);
  
  ierr = ComputeMatrixInverse_3x3(A,B);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CubicSpline_Interpolate"
PetscErrorCode CubicSpline_Interpolate(PetscReal xi,PetscReal xn[],PetscReal B[3][3],PetscReal yn[],PetscReal *yi)
{
  PetscReal k[3],b[3];
  PetscReal x1x02,x2x12;
  PetscReal qa[2],qb[2];
  PetscInt  i;
  
  //if (xi < (xn[0] - SL_EPS)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"cubicspline: out of range xn[0]");
  //if (xi > (xn[2] + SL_EPS)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"cubicspline: out of range xn[2]");
  
  x1x02 = (xn[1] - xn[0])*(xn[1] - xn[0]);
  x2x12 = (xn[2] - xn[1])*(xn[2] - xn[1]);
  
  b[0] = 3.0 * (yn[1] - yn[0])/x1x02;
  b[2] = 3.0 * (yn[2] - yn[1])/x2x12;
  b[1] = b[0] + b[2];
  
  k[0] = k[1] = k[2] = 0.0;
  for (i=0; i<3; i++) {
    k[0] += B[0][i] * b[i];
    k[1] += B[1][i] * b[i];
    k[2] += B[2][i] * b[i];
  }
  
  qa[0] = k[0] * (xn[1] - xn[0]) - (yn[1] - yn[0]);
  qa[1] = k[1] * (xn[2] - xn[1]) - (yn[2] - yn[1]);
  
  qb[0] = -k[1] * (xn[1] - xn[0]) + (yn[1] - yn[0]);
  qb[1] = -k[2] * (xn[2] - xn[1]) + (yn[2] - yn[1]);
  
  if (xi < xn[1]) {
    PetscReal t = (xi - xn[0])/(xn[1] - xn[0]);
    
    *yi = (1.0-t) * yn[0] + t * yn[1] + t*(1.0-t)*(qa[0]*(1.0-t) + qb[0]*t);
  } else {
    PetscReal t = (xi - xn[1])/(xn[2] - xn[1]);
    
    *yi = (1.0-t) * yn[1] + t * yn[2] + t*(1.0-t)*(qa[1]*(1.0-t) + qb[1]*t);
  }
  
  PetscFunctionReturn(0);
}

/*
 [input]
 xd[] : single departure point
 h[] : array of all heights
 [output]
 hd : interpolated height at departure point
 */
/* interpolate in y first, then x : arbitrary choice */
#undef __FUNCT__
#define __FUNCT__ "SLInterpolateAtPointCSpline_DMDA"
PetscErrorCode SLInterpolateAtPointCSpline_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M)
{
  PetscErrorCode ierr;
  PetscInt ci,cj;
  PetscInt ni[3],nj[3],kk;
  PetscReal yj[3],xj[3];
  PetscReal invA[3][3],hinterp[3],hj[3];
  
  
  /* determine cell containing point */
  ci = (int)( (xd[0] - s->xrange[0])/s->dx );
  cj = (int)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  /* determine 3 nj values to use for cspline interpolation */
  nj[0] = cj-1;
  nj[1] = cj;
  nj[2] = cj+1;
  
  if (nj[0] < 0) {
    nj[0] = 0;
    nj[1] = 1;
    nj[2] = 2;
  } else if (nj[2] >= s->ny) {
    nj[0] = s->ny-3;
    nj[1] = s->ny-2;
    nj[2] = s->ny-1;
  }
  
  /* determine 3 ni values to use for cspline interpolation */
  ni[0] = ci-1;
  ni[1] = ci;
  ni[2] = ci+1;
  
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
  } else if (ni[2] >= s->nx) {
    ni[0] = s->nx-3;
    ni[1] = s->nx-2;
    ni[2] = s->nx-1;
  }
  
  for (kk=0; kk<3; kk++) {
    yj[kk] = s->yrange[0] + nj[kk] * s->dy;
    
    xj[kk] = s->xrange[0] + ni[kk] * s->dx;
  }
  
  if (xd[0] > (xj[2]+SL_EPS)) { printf("xd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  if (xd[0] < (xj[0]-SL_EPS)) { printf("xd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  
  if (xd[1] > (yj[2]+SL_EPS)) { printf("yd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  if (xd[1] < (yj[0]-SL_EPS)) { printf("yd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  
  /* interpolate three times to determin the points in the y direction */
  ierr = CubicSpline_PrepareBasis(yj,invA);CHKERRQ(ierr);
  
  for (kk=0; kk<3; kk++) {
    PetscInt nidx = DANodeIdx(s,ni[0],nj[kk]);
    
    hj[kk] = phi[nidx];
  }
  ierr = CubicSpline_Interpolate(xd[1],yj,invA,hj,&hinterp[0]);CHKERRQ(ierr);
  
  for (kk=0; kk<3; kk++) {
    PetscInt nidx = DANodeIdx(s,ni[1],nj[kk]);
    
    hj[kk] = phi[nidx];
  }
  ierr = CubicSpline_Interpolate(xd[1],yj,invA,hj,&hinterp[1]);CHKERRQ(ierr);
  
  for (kk=0; kk<3; kk++) {
    PetscInt nidx = DANodeIdx(s,ni[2],nj[kk]);
    
    hj[kk] = phi[nidx];
  }
  ierr = CubicSpline_Interpolate(xd[1],yj,invA,hj,&hinterp[2]);CHKERRQ(ierr);
  
  /* interpolate once in the x direction */
  ierr = CubicSpline_PrepareBasis(xj,invA);CHKERRQ(ierr);
  
  ierr = CubicSpline_Interpolate(xd[0],xj,invA,hinterp,_phi_M);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 [input]
 xd[] : single departure point
 h[] : array of all heights
 [output]
 hd : interpolated height at departure point
 */
/*
 Employs the method described in
 
 @inproceedings{paoli2006testing,
 title={Testing semi-Lagrangian schemes for two-phase flow applications},
 author={Paoli, R and Poinsot, T and Shariff, K},
 booktitle={Proceeding of the Summer program},
 year={2006},
 organization={Citeseer}
 }
 
 See equation (2.10)
 The essential idea is as follows
 [1] Compute the departure point x*
 [2] Compute a high-order approximation of \phi(x*) (using cubic splines), \phi_H(x*)
 [3] Compute the lower (\phi_{-}) / upper bounds (\phi_{+}) of \phi using the nodal values of \phi for the cell containint x*
 [4] Define \phi(x*) as
 \phi(x*) = \phi_{+} if \phi_H(x*) > \phi_{+}
 \phi(x*) = \phi_{-} if \phi_H(x*) < \phi_{-}
 \phi(x*) = \phi_H(x*) otherwise
*/
#undef __FUNCT__
#define __FUNCT__ "SLInterpolateAtPointQuasiMonotone_DMDA"
PetscErrorCode SLInterpolateAtPointQuasiMonotone_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,k;
  PetscInt nidx[4];
  PetscReal phi_i,phi_H,phi_minus,phi_plus,phi_M;
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = DANodeIdx(s,ci  ,cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci  ,cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);
  
  phi_minus = 1.0e20;
  phi_plus  = -1.0e20;
  for (k=0; k<4; k++) {
    phi_i = phi[ nidx[k] ];
    
    if (phi_i > phi_plus)  { phi_plus = phi_i; }
    if (phi_i < phi_minus) { phi_minus = phi_i; }
  }
  
  ierr = SLInterpolateAtPointCSpline_DMDA(s,xd,phi,&phi_H);CHKERRQ(ierr);
  
  if (phi_H > phi_plus) {
    
    phi_M = phi_plus;
    
  } else if (phi_H < phi_minus) {
    
    phi_M = phi_minus;
    
  } else {
    
    phi_M = phi_H;
    
  }
  
  *_phi_M = phi_M;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMDA_DMTET_SEQ"
PetscErrorCode DMCreateInterpolation_DMDA_DMTET_SEQ(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  DM_TET *tetT;
  PetscBool istet;
  Mat interp;
  PetscInt i,nelT,bpeT,nnodesF,nnodesT,*elementT,M,N,mx,my;
  PetscReal *coorT;
  PetscReal min[3],max[3],xrange[2],yrange[2],dx,dy;
  
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"Arg 2 must be type DMTET");
  
  tetT = (DM_TET*)dmT->data;
  
  /* check both using continuous basis */
  if (tetT->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 2 must use a continuous basis");
  
  ierr = DMDAGetInfo(dmF,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetBoundingBox(dmF,min,max);CHKERRQ(ierr);

  xrange[0] = min[0];
  yrange[0] = min[1];
  
  xrange[1] = max[0];
  yrange[1] = max[1];
  
  mx = M - 1;
  my = N - 1;
  nnodesF = M * N;
  
  dx = (xrange[1] - xrange[0])/((PetscReal)mx);
  dy = (yrange[1] - yrange[0])/((PetscReal)my);

  ierr = DMTetSpaceElement(dmT,&nelT,&bpeT,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);

  ierr = MatCreate(PetscObjectComm((PetscObject)dmT),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,PETSC_DECIDE,PETSC_DECIDE,tetT->space->nactivedof,nnodesF);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATSEQAIJ);CHKERRQ(ierr);
  ierr = MatSeqAIJSetPreallocation(interp,4,NULL);CHKERRQ(ierr);
  
  for (i=0; i<nnodesT; i++) {
    PetscReal *pos_i,xi[2],Ni[4],x0[2];
    PetscInt  elF[4];
    PetscInt  ci,cj;
    
    
    pos_i = &coorT[2*i];
    
    /* locate element and local coord within dmF */
    
    /* compute ci,cj under assumption of constant grid spacing dx, dy */
    ci = (PetscInt)( (pos_i[0] - xrange[0])/dx );
    cj = (PetscInt)( (pos_i[1] - yrange[0])/dy );
    if (ci == mx) ci--;
    if (cj == my) cj--;
    
    if (ci < 0) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"Point outside domain (ci < 0). Point must be found - extrapolation not supported");
    if (cj < 0) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"Point outside domain (cj < 0). Point must be found - extrapolation not supported");
    if (ci >= mx) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"Point outside domain (ci >= mx). Point must be found - extrapolation not supported");
    if (cj >= my) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"Point outside domain (cj >= my). Point must be found - extrapolation not supported");
    
    /* got node indices for this element for use with Q1 interpolation */
    elF[0] = (ci  ) + (cj  ) * M;
    elF[1] = (ci+1) + (cj  ) * M;
    elF[2] = (ci  ) + (cj+1) * M;
    elF[3] = (ci+1) + (cj+1) * M;
    
    /* compute xi */
    x0[0] = xrange[0] + ci * dx; /* global coord (x) of node 0 */
    x0[1] = yrange[0] + cj * dy;
    
    xi[0] = 2.0*(pos_i[0] - x0[0])/dx - 1.0;
    xi[1] = 2.0*(pos_i[1] - x0[1])/dy - 1.0;
    
    /* compute Ni */
    Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
    Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
    Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
    Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);

    ierr = MatSetValues(interp,1,&i,4,elF,Ni,INSERT_VALUES);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  
  PetscFunctionReturn(0);
}

#define CTYPE_VERTEX 0
#define CTYPE_CENTROID 1


/*
 [input]
 xd[] : single departure point
 h[] : array of all heights
 [output]
 hd : interpolated height at departure point
*/
/* interpolate in y first, then x : arbitrary choice */
#undef __FUNCT__
#define __FUNCT__ "DMDA_InterpolateAtPoint_CSpline"
PetscErrorCode DMDA_InterpolateAtPoint_CSpline(PetscInt center_type,PetscInt mn[],PetscReal dx[],PetscReal xrange[],PetscReal yrange[],
                                              PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M)
{
  PetscErrorCode ierr;
  PetscInt ci,cj;
  PetscInt ni[3],nj[3],kk;
  PetscReal yj[3],xj[3];
  PetscReal invA[3][3],hinterp[3],hj[3];
  
  
  /* determine cell containing point */
  ci = (int)( (xd[0] - xrange[0])/dx[0] );
  cj = (int)( (xd[1] - yrange[0])/dx[1] );
  if (center_type == CTYPE_VERTEX) {
    if (ci == mn[0]) ci--;
    if (cj == mn[1]) cj--;
  }

  /* determine 3 nj values to use for cspline interpolation */
  nj[0] = cj-1;
  nj[1] = cj;
  nj[2] = cj+1;
  
  if (nj[0] < 0) {
    nj[0] = 0;
    nj[1] = 1;
    nj[2] = 2;
  //} else if (nj[2] >= s->ny) {
  } else if (nj[2] >= mn[1]) {
    nj[0] = mn[1]-3;
    nj[1] = mn[1]-2;
    nj[2] = mn[1]-1;
  }
  
  /* determine 3 ni values to use for cspline interpolation */
  ni[0] = ci-1;
  ni[1] = ci;
  ni[2] = ci+1;
  
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
  //} else if (ni[2] >= s->nx) {
  } else if (ni[2] >= mn[0]) {
    ni[0] = mn[0]-3;
    ni[1] = mn[0]-2;
    ni[2] = mn[0]-1;
  }
  
  if (center_type == CTYPE_VERTEX) {
    for (kk=0; kk<3; kk++) {
      yj[kk] = yrange[0] + nj[kk] * dx[1];
      
      xj[kk] = xrange[0] + ni[kk] * dx[0];
    }
  } else {
    for (kk=0; kk<3; kk++) {
      yj[kk] = yrange[0] + nj[kk] * dx[1] + 0.5 * dx[1];
      
      xj[kk] = xrange[0] + ni[kk] * dx[0] + 0.5 * dx[0];
    }
  }
  
  //if (xd[0] > (xj[2]+SL_EPS)) { printf("xd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  //if (xd[0] < (xj[0]-SL_EPS)) { printf("xd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  
  //if (xd[1] > (yj[2]+SL_EPS)) { printf("yd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  //if (xd[1] < (yj[0]-SL_EPS)) { printf("yd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  
  /* interpolate three times to determin the points in the y direction */
  ierr = CubicSpline_PrepareBasis(yj,invA);CHKERRQ(ierr);
  
  for (kk=0; kk<3; kk++) {
    //PetscInt nidx = DANodeIdx(s,ni[0],nj[kk]);
    PetscInt nidx = ni[0] + nj[kk] * mn[0];
    
    hj[kk] = phi[nidx];
  }
  ierr = CubicSpline_Interpolate(xd[1],yj,invA,hj,&hinterp[0]);CHKERRQ(ierr);
  
  for (kk=0; kk<3; kk++) {
    //PetscInt nidx = DANodeIdx(s,ni[1],nj[kk]);
    PetscInt nidx = ni[1] + nj[kk] * mn[0];
    
    hj[kk] = phi[nidx];
  }
  ierr = CubicSpline_Interpolate(xd[1],yj,invA,hj,&hinterp[1]);CHKERRQ(ierr);
  
  for (kk=0; kk<3; kk++) {
    //PetscInt nidx = DANodeIdx(s,ni[2],nj[kk]);
    PetscInt nidx = ni[2] + nj[kk] * mn[0];
    
    hj[kk] = phi[nidx];
  }
  ierr = CubicSpline_Interpolate(xd[1],yj,invA,hj,&hinterp[2]);CHKERRQ(ierr);
  
  /* interpolate once in the x direction */
  ierr = CubicSpline_PrepareBasis(xj,invA);CHKERRQ(ierr);
  
  ierr = CubicSpline_Interpolate(xd[0],xj,invA,hinterp,_phi_M);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMFVConservative_SLInterpolateAtPointQuasiMonotone_DMDA"
PetscErrorCode DMFVConservative_SLInterpolateAtPointQuasiMonotone_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi_node[],PetscScalar phi[],PetscReal *_phi_M)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,k;
  PetscInt nidx[4];
  PetscReal phi_i,phi_H,phi_minus,phi_plus,phi_M;
  PetscReal dx[2];
  PetscInt nxny[2];
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = DANodeIdx(s,ci  ,cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci  ,cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);
  
  phi_minus = 1.0e20;
  phi_plus  = -1.0e20;
  for (k=0; k<4; k++) {
    phi_i = phi_node[ nidx[k] ];
    
    if (phi_i > phi_plus)  { phi_plus = phi_i; }
    if (phi_i < phi_minus) { phi_minus = phi_i; }
  }
  
  nxny[0] = s->mx;
  nxny[1] = s->my;
  dx[0] = s->dx;
  dx[1] = s->dy;
  ierr = DMDA_InterpolateAtPoint_CSpline(CTYPE_CENTROID,nxny,dx,s->xrange,s->yrange,
                                                 xd,phi,&phi_H);CHKERRQ(ierr);
  
  
  if (phi_H > phi_plus) {
    
    phi_M = phi_plus;
    
  } else if (phi_H < phi_minus) {
    
    phi_M = phi_minus;
    
  } else {
    
    phi_M = phi_H;
    
  }
  
  *_phi_M = phi_M;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CellBased_CSplineAtPointQuasiMonotone_DMDA"
PetscErrorCode CellBased_CSplineAtPointQuasiMonotone_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi_cell[],PetscReal *_phi_M)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,cni,cnj,cell_neighbour;
  PetscReal phi_n,phi_H,phi_minus,phi_plus,phi_M;
  PetscReal dx[2];
  PetscInt nxny[2];
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  /* get bounds from 8 neighbours */
  phi_minus = 1.0e20;
  phi_plus  = -1.0e20;
  for (cnj=cj-1; cnj<=cj+1; cnj++) {
    if (cnj < 0) continue;
    if (cnj > (s->my-1)) continue;
    
    for (cni=ci-1; cni<=ci+1; cni++) {
      if (cni < 0) continue;
      if (cni > (s->mx-1)) continue;

      cell_neighbour = DACellIdx(s,cni,cnj);
  
      phi_n = phi_cell[ cell_neighbour ];
    
      if (phi_n > phi_plus)  { phi_plus = phi_n; }
      if (phi_n < phi_minus) { phi_minus = phi_n; }
    }
  }
  
  nxny[0] = s->mx;
  nxny[1] = s->my;
  dx[0] = s->dx;
  dx[1] = s->dy;
  ierr = DMDA_InterpolateAtPoint_CSpline(CTYPE_CENTROID,nxny,dx,s->xrange,s->yrange,
                                         xd,phi_cell,&phi_H);CHKERRQ(ierr);
  
  
  if (phi_H > phi_plus) {
    
    phi_M = phi_plus;
    
  } else if (phi_H < phi_minus) {
    
    phi_M = phi_minus;
    
  } else {
    
    phi_M = phi_H;
    
  }
  
  *_phi_M = phi_M;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NodeReconstruction_CSplineAtPointQuasiMonotone_with_cell_data"
PetscErrorCode NodeReconstruction_CSplineAtPointQuasiMonotone_with_cell_data(DMSemiLagrangian dasl,Vec phi_c,Vec phi_n)
{
  PetscErrorCode ierr;
  Vec coor;
  const PetscScalar *LA_coor;
  const PetscScalar *LA_phi_cell;
  PetscScalar *LA_phi_node;
  PetscInt ni,nj,ci,cj,ci_s,ci_e,cj_s,cj_e;
  SLDMDA    sl;
  PetscInt nxny[2];
  PetscReal dx[2];
  PetscReal xn[2],phi_cij,phi_H,phi_minus,phi_plus,phi_M;
  
  sl = dasl->dactx;
  
  ierr = DMGetCoordinates(dasl->dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi_c,&LA_phi_cell);CHKERRQ(ierr);
  ierr = VecGetArray(phi_n,&LA_phi_node);CHKERRQ(ierr);

  nxny[0] = sl->mx;
  nxny[1] = sl->my;
  dx[0] = sl->dx;
  dx[1] = sl->dy;

  for (nj=0; nj<sl->ny; nj++) {
    cj_s = nj - 1;
    cj_e = nj;

    if (cj_s < 0)          { cj_s = 0; }
    if (cj_e > (sl->my-1)) { cj_e = sl->my-1; }
    
    for (ni=0; ni<sl->nx; ni++) {
      PetscInt nidx;
      
      ci_s = ni - 1;
      ci_e = ni;
      
      if (ci_s < 0)          { ci_s = 0; }
      if (ci_e > (sl->mx-1)) { ci_e = sl->mx-1; }

      nidx = DANodeIdx(sl,ni,nj);

      /* interpolate at point using cell data */
      xn[0] = LA_coor[2*nidx+0];
      xn[1] = LA_coor[2*nidx+1];
      
      ierr = DMDA_InterpolateAtPoint_CSpline(CTYPE_CENTROID,nxny,dx,sl->xrange,sl->yrange,
                                             xn,(PetscScalar*)LA_phi_cell,&phi_H);CHKERRQ(ierr);
      
      /* compute bounds using cell data */
      phi_minus = 1.0e20;
      phi_plus  = -1.0e20;
      for (cj=cj_s; cj<=cj_e; cj++) {
        for (ci=ci_s; ci<=ci_e; ci++) {
          PetscInt cell_neighbour_idx;
          
          cell_neighbour_idx = DACellIdx(sl,ci,cj);
          
          phi_cij = LA_phi_cell[ cell_neighbour_idx ];
          
          if (phi_cij > phi_plus)  { phi_plus = phi_cij; }
          if (phi_cij < phi_minus) { phi_minus = phi_cij; }
        }
      }
      
      /* apply bound limiters */
      if (phi_H > phi_plus) {
        phi_M = phi_plus;
      } else if (phi_H < phi_minus) {
        phi_M = phi_minus;
      } else {
        phi_M = phi_H;
      }
      
      LA_phi_node[nidx] = phi_M;
      
    }
  }
  
  
  ierr = VecRestoreArray(phi_n,&LA_phi_node);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi_c,&LA_phi_cell);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#if 0
#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_DMDA"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_DMDA(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,fi,fj;
  PetscReal xn[2],xd[2],phid;
  Vec coor;
  const PetscScalar *LA_phi;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscScalar *LA_phi_dep;
  PetscInt L,nsteps;
  static PetscInt fcnt = 0;
  FILE *fp = NULL;
  PetscInt debug = dasl->debug;
  SLDMDA    sl;
  FVStorage fv;
  Vec phi_node,phi_avg;
  const PetscScalar *LA_phi_node;
  
  sl = dasl->dactx;
  fv = dasl->fv;
  
  if (debug > 0) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sldp-%D.gp",fcnt);
    fp = fopen(name,"w");
    fprintf(fp,"# frame %d \n",fcnt);
  }
  
  ierr = DMCreateGlobalVector(dasl->dm,&phi_node);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dasl->dm,&phi_avg);CHKERRQ(ierr);
  
  ierr = VecGetSize(phi,&L);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dasl->dm,&coor);CHKERRQ(ierr);

  ierr = VecZeroEntries(dasl->phi_dep);CHKERRQ(ierr);
  ierr = VecCopy(phi,dasl->phi_dep);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;

  ierr = VecGetArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  
  for (cj=0; cj<sl->my; cj++) {
    for (ci=0; ci<sl->mx; ci++) {
      PetscInt cid_ij,nidx[4],k;
      PetscReal phi_cell;
      
      cid_ij = DACellIdx(sl,ci,cj);

      nidx[0] = DANodeIdx(sl,ci  ,cj);
      nidx[1] = DANodeIdx(sl,ci+1,cj);
      nidx[2] = DANodeIdx(sl,ci  ,cj+1);
      nidx[3] = DANodeIdx(sl,ci+1,cj+1);
      
      phi_cell = LA_phi_dep[cid_ij];

      for (k=0; k<4; k++) {
        ierr = VecSetValue(phi_avg,nidx[k],1.0,ADD_VALUES);CHKERRQ(ierr);
        ierr = VecSetValue(phi_node,nidx[k],phi_cell,ADD_VALUES);CHKERRQ(ierr);
      }
    }
  }
  ierr = VecAssemblyBegin(phi_avg);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_avg);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(phi_node);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_node);CHKERRQ(ierr);

  ierr = VecPointwiseDivide(phi_node,phi_node,phi_avg);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_avg);CHKERRQ(ierr);

  ierr = VecGetArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  
  
  nsteps = 1;
  if (!dasl->trajectory_disable_substep) {
    PetscReal dt_cfl;
    
    ierr = FDSPMComputeTimestep_CFL(dasl->dactx,dasl->trajectory_cfl,(PetscScalar*)LA_velocity,&dt_cfl);CHKERRQ(ierr);
    if (dt > dt_cfl) {
      nsteps = (PetscInt)(dt / dt_cfl) + 1;
    }
  }
  printf("  #RK4 steps per characteristic = %d \n",nsteps);
  
  /* compute face contributions */
  /* 
   For each face
    init face value to zero
    for each face quadrature point
      - get face quadrature coordinate (x)
      - compute \int_{t}^{t-dt} F dtau, where F = phi \vec v
      - sum contributions
  */
  
  /* process the x-normal faces */
  for (fj=0; fj<fv->mn_normal_x[1]; fj++) {
    for (fi=0; fi<fv->mn_normal_x[0]; fi++) {
      PetscInt fid,ni0,nj0,fq;
      PetscReal veld[2],flux_q[2],x0,y0[2];
      
      fid = fi + fj * dasl->fv->mn_normal_x[0];
  
      /* node index of cell->vertex[0] */
      ni0 = fi;
      nj0 = fj;

      x0    = sl->xrange[0] + ni0 * sl->dx;
      y0[0] = sl->yrange[0] + nj0 * sl->dy;
      y0[1] = y0[0] + sl->dy;
      
      flux_q[0] = flux_q[1] = 0.0;
      for (fq=0; fq<fv->face_nqp; fq++) {
        PetscReal dJ;
        PetscBool dep_left;
        
        xn[0] = x0;
        /* interpolate y coor */
        xn[1] = 0.5*(1.0 - fv->face_q_xi[fq])*y0[0] + 0.5*(1.0 + fv->face_q_xi[fq])*y0[1];
        
        ierr = SLComputeDeparturePoint_DMDA(sl,dt,nsteps,xn,xd,&dep_left);CHKERRQ(ierr);
        
        /* interpolate phi[] at the departure point */
        phid = 0.0;
        switch (dasl->dactx->interpolation_type) {
          case DASLInterp_Q1:
            SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"FVConservative only supports monotone");
            break;
          case DASLInterp_CSpline:
            SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"FVConservative only supports monotone");
            break;
            
          case DASLInterp_QMonotone:
            ierr = DMFVConservative_SLInterpolateAtPointQuasiMonotone_DMDA(sl,xd,(PetscScalar*)LA_phi_node,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
            break;
        }

        /* interpolate vel[] at the departure point */
        ierr = SLInterpolateDOFAtPointQ1_DMDA(sl,xd,2,(PetscScalar*)LA_velocity,veld);CHKERRQ(ierr);
        
        dJ = sl->dy / 2.0;
        flux_q[0] += fv->face_q_w[fq] * (phid * veld[0] * dt) * dJ;
        flux_q[1] += fv->face_q_w[fq] * (phid * veld[1] * dt) * dJ;
        
        if (debug > 0) {
          fprintf(fp,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e\n",xn[0],xn[1],0.0, xd[0],xd[1],phid);
        }
      }
      
      fv->normal_x[2*fid+0] = flux_q[0];
      fv->normal_x[2*fid+1] = flux_q[1];
    }
  }
  
  /* process the y-normal faces */
  for (fj=0; fj<fv->mn_normal_y[1]; fj++) {
    for (fi=0; fi<fv->mn_normal_y[0]; fi++) {
      PetscInt fid,ni0,nj0,fq;
      PetscReal veld[2],flux_q[2],x0[2],y0;
      
      fid = fi + fj * dasl->fv->mn_normal_y[0];
      
      /* node index of cell->vertex[0] */
      ni0 = fi;
      nj0 = fj;
      
      y0    = sl->yrange[0] + nj0 * sl->dy;
      x0[0] = sl->xrange[0] + ni0 * sl->dx;
      x0[1] = x0[0] + sl->dx;
      
      flux_q[0] = flux_q[1] = 0.0;
      for (fq=0; fq<fv->face_nqp; fq++) {
        PetscReal dJ;
        PetscBool dep_left;
        
        xn[1] = y0;
        /* interpolate y coor */
        xn[0] = 0.5*(1.0 - fv->face_q_xi[fq])*x0[0] + 0.5*(1.0 + fv->face_q_xi[fq])*x0[1];
        
        ierr = SLComputeDeparturePoint_DMDA(sl,dt,nsteps,xn,xd,&dep_left);CHKERRQ(ierr);
        
        /* interpolate phi[] at the departure point */
        phid = 0.0;
        switch (dasl->dactx->interpolation_type) {
          case DASLInterp_Q1:
            SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"FVConservative only supports monotone");
            break;
          case DASLInterp_CSpline:
            SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"FVConservative only supports monotone");
            break;
            
          case DASLInterp_QMonotone:
            ierr = DMFVConservative_SLInterpolateAtPointQuasiMonotone_DMDA(sl,xd,(PetscScalar*)LA_phi_node,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
            break;
        }
        
        /* interpolate vel[] at the departure point */
        ierr = SLInterpolateDOFAtPointQ1_DMDA(sl,xd,2,(PetscScalar*)LA_velocity,veld);CHKERRQ(ierr);
        
        dJ = sl->dx / 2.0;
        flux_q[0] += fv->face_q_w[fq] * (phid * veld[0] * dt) * dJ;
        flux_q[1] += fv->face_q_w[fq] * (phid * veld[1] * dt) * dJ;
        
        if (debug > 0) {
          fprintf(fp,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e\n",xn[0],xn[1],0.0, xd[0],xd[1],phid);
        }
      }
      
      fv->normal_y[2*fid+0] = flux_q[0];
      fv->normal_y[2*fid+1] = flux_q[1];
    }
  }

  
  
  
  /* sum surface intergrals */
  for (cj=0; cj<sl->my; cj++) {
    for (ci=0; ci<sl->mx; ci++) {
      PetscInt fid,cid_ij;
      PetscReal int_F_e[2],int_F_w[2],int_F_n[2],int_F_s[2];

      cid_ij = DACellIdx(sl,ci,cj);
      
      /* faces with normals in +/- x direction */
      fid = (ci+1+cj*fv->mn_normal_x[0]);
      int_F_e[0] = fv->normal_x[2*fid+0];
      int_F_e[1] = fv->normal_x[2*fid+1];
      
      fid = (ci+0+cj*fv->mn_normal_x[0]);
      int_F_w[0] = fv->normal_x[2*fid+0];
      int_F_w[1] = fv->normal_x[2*fid+1];

      /* faces with normals in +/- y direction */
      fid = (ci+(cj+1)*fv->mn_normal_y[0]);
      int_F_n[0] = fv->normal_y[2*fid+0];
      int_F_n[1] = fv->normal_y[2*fid+1];

      fid = (ci+(cj+0)*fv->mn_normal_y[0]);
      int_F_s[0] = fv->normal_y[2*fid+0];
      int_F_s[1] = fv->normal_y[2*fid+1];

      printf("flux %1.4e %1.4e %1.4e %1.4e\n",int_F_e[0] , int_F_w[0] , int_F_n[1] , int_F_s[1]);
      //printf("before %1.4e\n",LA_phi_dep[cid_ij]);
      LA_phi_dep[cid_ij] += (int_F_e[0] - int_F_w[0] + int_F_n[1] - int_F_s[1])/(sl->dx*sl->dy);
      //printf("after %1.4e\n",LA_phi_dep[cid_ij]);
    }
  }
  
#if 0
  for (k=0; k<L; k++) {
    PetscBool dep_left;
    
    nidx_ij = k;
    
    /* determine departure point */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    ierr = SLComputeDeparturePoint_DMDA(dsl->dactx,dt,nsteps,xn,xd,&dep_left);CHKERRQ(ierr);
    
    /* interpolate phi[] at the departure point */
    phid = 0.0;
    switch (dsl->dactx->interpolation_type) {
      case DASLInterp_Q1:
        ierr = SLInterpolateAtPointQ1_DMDA(dsl->dactx,xd,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_CSpline:
        ierr = SLInterpolateAtPointCSpline_DMDA(dsl->dactx,xd,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_QMonotone:
        ierr = SLInterpolateAtPointQuasiMonotone_DMDA(dsl->dactx,xd,(PetscScalar*)LA_phi,&phid);CHKERRQ(ierr);
        break;
    }
    /* Update phi */
    LA_phi_dep[nidx_ij] = phid;
    
    if (debug > 0) {
      fprintf(fp,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e\n",xn[0],xn[1],LA_phi[k], xd[0],xd[1],LA_phi_dep[k]);
    }
  }
#endif
  ierr = VecRestoreArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);

  ierr = VecRestoreArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_node);CHKERRQ(ierr);
  
  ierr = VecCopy(dasl->phi_dep,phi);CHKERRQ(ierr); /* phi <-- phi_departure */
  if (debug > 0) {
    fcnt++;
    fclose(fp);
  }
  PetscFunctionReturn(0);
}
#endif

/* evaluate the Q1 basis at a point */
void EvalBasis_Q1(PetscReal _xi[],PetscReal Ni[])
{
  PetscReal xi  = _xi[0];
  PetscReal eta = _xi[1];
  
  Ni[0] = 0.25*(1.0-xi)*(1.0-eta);
  Ni[1] = 0.25*(1.0+xi)*(1.0-eta);
  Ni[2] = 0.25*(1.0-xi)*(1.0+eta);
  Ni[3] = 0.25*(1.0+xi)*(1.0+eta);
}

/* evaluate derivatives of the Q1 basis at a point */
void EvalBasisDerivLocal_Q1(PetscReal _xi[],PetscReal GNs1[],PetscReal GNs2[])
{
  PetscReal xi  = _xi[0];
  PetscReal eta = _xi[1];
  
  GNs1[0] = -0.25*(1.0-eta);
  GNs1[1] =   0.25*(1.0-eta);
  GNs1[2] = -0.25*(1.0+eta);
  GNs1[3] =   0.25*(1.0+eta);
  
  GNs2[0] = -0.25*(1.0-xi);
  GNs2[1] = -0.25*(1.0+xi);
  GNs2[2] =   0.25*(1.0-xi);
  GNs2[3] =   0.25*(1.0+xi);
}

/* returns a signed determinant */
void EvalGeometry_Q1(PetscReal GNs1[],PetscReal GNs2[],PetscReal coords[],PetscReal *det_J)
{
  PetscReal J00,J01,J10,J11,J;
  PetscInt  i;
  
  J00 = J01 = J10 = J11 = 0.0;
  for (i = 0; i < 4; i++) {
    PetscReal cx = coords[ 2*i+0 ];
    PetscReal cy = coords[ 2*i+1 ];
    
    J00 = J00+GNs1[i]*cx;      /* J_xx = dx/dxi */
    J01 = J01+GNs1[i]*cy;      /* J_xy = dy/dxi */
    J10 = J10+GNs2[i]*cx;      /* J_yx = dx/deta */
    J11 = J11+GNs2[i]*cy;      /* J_yy = dy/deta */
  }
  J = (J00*J11)-(J01*J10);
  *det_J = J;
}


#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_v0"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_v0(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,k;
  PetscReal xn[2],xd[2],phid;
  Vec coor;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscScalar *LA_phi_dep;
  PetscInt L,nsteps;
  static PetscInt fcnt = 0;
  FILE *fp = NULL;
  PetscInt debug = dasl->debug;
  SLDMDA    sl;
  Vec phi_node,phi_avg;
  const PetscScalar *LA_phi_node;
  PetscReal *x_departure,*phi_departure;
  PetscBool *inflow_characteristic;
  
  sl = dasl->dactx;
  
  if (debug > 0) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sldp-%D.gp",fcnt);
    fp = fopen(name,"w");
    fprintf(fp,"# frame %d \n",fcnt);
  }
  
  ierr = DMCreateGlobalVector(dasl->dm,&phi_node);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dasl->dm,&phi_avg);CHKERRQ(ierr);
  ierr = VecGetSize(phi_node,&L);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(dasl->phi_dep);CHKERRQ(ierr);
  ierr = VecCopy(phi,dasl->phi_dep);CHKERRQ(ierr);
  
  ierr = VecGetArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  
  for (cj=0; cj<sl->my; cj++) {
    for (ci=0; ci<sl->mx; ci++) {
      PetscInt cid_ij,nidx[4],k;
      PetscReal phi_cell;
      
      cid_ij = DACellIdx(sl,ci,cj);
      
      nidx[0] = DANodeIdx(sl,ci  ,cj);
      nidx[1] = DANodeIdx(sl,ci+1,cj);
      nidx[2] = DANodeIdx(sl,ci  ,cj+1);
      nidx[3] = DANodeIdx(sl,ci+1,cj+1);
      
      phi_cell = LA_phi_dep[cid_ij];
      
      for (k=0; k<4; k++) {
        ierr = VecSetValue(phi_avg,nidx[k],1.0,ADD_VALUES);CHKERRQ(ierr);
        ierr = VecSetValue(phi_node,nidx[k],phi_cell,ADD_VALUES);CHKERRQ(ierr);
      }
    }
  }
  ierr = VecRestoreArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);

  ierr = VecAssemblyBegin(phi_avg);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_avg);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(phi_node);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_node);CHKERRQ(ierr);
  
  ierr = VecPointwiseDivide(phi_node,phi_node,phi_avg);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_avg);CHKERRQ(ierr);
  
  //VecView(phi_node,PETSC_VIEWER_STDOUT_WORLD);

  ierr = DMGetCoordinates(dasl->dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;
  
  ierr = VecGetArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  
  
  nsteps = 1;
  if (!dasl->trajectory_disable_substep) {
    PetscReal dt_cfl;
    
    ierr = FDSPMComputeTimestep_CFL(dasl->dactx,dasl->trajectory_cfl,(PetscScalar*)LA_velocity,&dt_cfl);CHKERRQ(ierr);
    if (dt > dt_cfl) {
      nsteps = (PetscInt)(dt / dt_cfl) + 1;
    }
  }
  printf("  #RK4 steps per characteristic = %d \n",nsteps);
  
  // compute and store departure points
  ierr = PetscMalloc1(L*2,&x_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&phi_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&inflow_characteristic);CHKERRQ(ierr);
  
  for (k=0; k<L; k++) {
    PetscBool dep_left;
    
    /* determine departure point */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    inflow_characteristic[k] = PETSC_FALSE;
    ierr = SLComputeDeparturePoint_DMDA(sl,dt,nsteps,xn,xd,&dep_left);CHKERRQ(ierr);
    if (dep_left) {
      inflow_characteristic[k] = PETSC_TRUE;
    }
    
    /* interpolate phi[] at the departure point */
    phid = 0.0;
    switch (dasl->dactx->interpolation_type) {
      case DASLInterp_Q1:
        ierr = SLInterpolateAtPointQ1_DMDA(sl,xd,(PetscScalar*)LA_phi_node,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_CSpline:
        ierr = SLInterpolateAtPointCSpline_DMDA(sl,xd,(PetscScalar*)LA_phi_node,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_QMonotone:
        ierr = SLInterpolateAtPointQuasiMonotone_DMDA(sl,xd,(PetscScalar*)LA_phi_node,&phid);CHKERRQ(ierr);
        break;
    }
    
    /* store xd,phi */
    x_departure[2*k+0] = xd[0];
    x_departure[2*k+1] = xd[1];
    phi_departure[k] = phid;
    
    if (debug > 0) {
      fprintf(fp,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e\n",xn[0],xn[1],0.0, xd[0],xd[1],phid);
    }
  }
  
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  
  ierr = VecGetArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);

  /* compute volume intergrals */
  for (cj=0; cj<sl->my; cj++) {
    for (ci=0; ci<sl->mx; ci++) {
      PetscInt cid_ij,nidx[4];
      PetscReal vol_a,int_phi_ak,avg_int_phi_ak;
      PetscReal xi[] = { -0.57735026919,-0.57735026919 , -0.57735026919,0.57735026919 , 0.57735026919,0.57735026919 , 0.57735026919,-0.57735026919 };
      PetscReal w[] = { 1.0, 1.0, 1.0, 1.0 };
      PetscInt q;
      PetscBool inflow_el[4];
      
      PetscReal xdep_el[2*4],phidep_el[4];
      
      cid_ij = DACellIdx(sl,ci,cj);

      nidx[0] = DANodeIdx(sl,ci  ,cj);
      nidx[1] = DANodeIdx(sl,ci+1,cj);
      nidx[2] = DANodeIdx(sl,ci  ,cj+1);
      nidx[3] = DANodeIdx(sl,ci+1,cj+1);
      for (k=0; k<4; k++) {
        xdep_el[2*k+0] = x_departure[2*nidx[k]+0];
        xdep_el[2*k+1] = x_departure[2*nidx[k]+1];
        phidep_el[k]   = phi_departure[nidx[k]];
        inflow_el[k]   = inflow_characteristic[nidx[k]];
      }
      
      /* skip update on cell if the face has two inflowing characteristics */
      if (ci == 0) { // west wall
        if (inflow_el[0] && inflow_el[2]) continue;
      }
      if (ci == (sl->mx-1)) { // east wall
        if (inflow_el[1] && inflow_el[3]) continue;
      }

      if (cj == 0) { // south wall
        if (inflow_el[0] && inflow_el[1]) continue;
      }
      if (cj == (sl->my-1)) { // north wall
        if (inflow_el[2] && inflow_el[3]) continue;
      }

      vol_a = 0.0;
      int_phi_ak = 0.0;
      for (q=0; q<4; q++) {
        PetscReal Ni[4],dNs1[4],dNs2[4];
        PetscReal phi_qp,dJ,xdep_q[] = {0.0,0.0};
        
        EvalBasis_Q1(&xi[2*q],Ni);
        EvalBasisDerivLocal_Q1(&xi[2*q],dNs1,dNs2);
        EvalGeometry_Q1(dNs1,dNs2,xdep_el,&dJ);
        
        phi_qp = 0.0;
        for (k=0; k<4; k++) {
          phi_qp += Ni[k] * phidep_el[k];
          xdep_q[0] += Ni[k] * xdep_el[2*k+0];
          xdep_q[1] += Ni[k] * xdep_el[2*k+1];
        }

        //printf("%1.4e %1.4e  %1.4e \n",xdep_q[0],xdep_q[1],phi_qp);
        
        
        int_phi_ak += w[q] * phi_qp * dJ;
        vol_a += w[q] * 1.0 * dJ;
      }
      
      
      avg_int_phi_ak = int_phi_ak/vol_a;

      //printf("vol %1.4e | %1.4e (phi-old) ** vol %1.4e | %1.4e (new)\n",vol_A,LA_phi_dep[cid_ij],vol_a,avg_int_phi_ak);

      if (vol_a < 1.0e-32) {
        PetscPrintf(PETSC_COMM_SELF,"[warning] semi-Lagrangian backtracked cell %D %D has twisted, or degenerate Q1 element\n",ci,cj);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"The SL cell is too deformed - suggest using a smaller time step");
      }
      
      LA_phi_dep[cid_ij] = avg_int_phi_ak;
      
    }
  }

  ierr = VecRestoreArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);

  ierr = VecDestroy(&phi_node);CHKERRQ(ierr);
  ierr = PetscFree(x_departure);CHKERRQ(ierr);
  ierr = PetscFree(phi_departure);CHKERRQ(ierr);
  ierr = PetscFree(inflow_characteristic);CHKERRQ(ierr);
  
  ierr = VecCopy(dasl->phi_dep,phi);CHKERRQ(ierr); /* phi <-- phi_departure */
  if (debug > 0) {
    fcnt++;
    fclose(fp);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_v2"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_v2(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,k;
  PetscReal xn[2],xd[2],phid;
  Vec coor;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  const PetscScalar *LA_phi;
  PetscScalar *LA_phi_dep;
  PetscInt L,nsteps;
  static PetscInt fcnt = 0;
  FILE *fp = NULL;
  PetscInt debug = dasl->debug;
  SLDMDA    sl;
  Vec phi_node,phi_node_avg,phi_avg;
  const PetscScalar *LA_phi_node;
  PetscReal *x_departure,*phi_departure;
  PetscBool *inflow_characteristic;
  PetscLogDouble t0,t1,t2,t3,t4;
  PetscReal field_int,domain_vol;
  PetscReal dV;
  //static PetscBool beenhere = PETSC_FALSE;
  //static PetscReal domain_residual_initial_value = 0;
  //PetscReal porosity,domain_ref = 0.0,domain_residual = 0.0;
  
  sl = dasl->dactx;
  
  if (debug > 1) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sldp-%D.gp",fcnt);
    fp = fopen(name,"w");
    fprintf(fp,"# frame %d \n",fcnt);

    /*
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sldp-log.gp",fcnt);
    if (!beenhere) {
      fp = fopen(name,"w");
      fprintf(fp,"# frame %d \n",fcnt);
    } else {
      fp = fopen(name,"a");
    }
    */
  }
  
  ierr = DMCreateGlobalVector(dasl->dm,&phi_node);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dasl->dm,&phi_node_avg);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dasl->dm,&phi_avg);CHKERRQ(ierr);
  ierr = VecGetSize(phi_node,&L);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(dasl->phi_dep);CHKERRQ(ierr);
  ierr = VecCopy(phi,dasl->phi_dep);CHKERRQ(ierr);
  
  ierr = VecGetArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  
  PetscTime(&t0);
  /* reconstruct nodal values using average */
  field_int = 0.0;
  domain_vol = 0.0;
  dV = sl->dx * sl->dy;
  for (cj=0; cj<sl->my; cj++) {
    for (ci=0; ci<sl->mx; ci++) {
      PetscInt cid_ij,nidx[4],k;
      PetscReal phi_cell;
      PetscReal ones[] = { 1.0, 1.0, 1.0, 1.0 },phi_node[4];
      
      cid_ij = DACellIdx(sl,ci,cj);
      
      nidx[0] = DANodeIdx(sl,ci  ,cj);
      nidx[1] = DANodeIdx(sl,ci+1,cj);
      nidx[2] = DANodeIdx(sl,ci  ,cj+1);
      nidx[3] = DANodeIdx(sl,ci+1,cj+1);
      
      phi_cell = LA_phi_dep[cid_ij];
      
      for (k=0; k<4; k++) {
        phi_node[k] = phi_cell;
      }
      ierr = VecSetValues(phi_avg,4,nidx,ones,ADD_VALUES);CHKERRQ(ierr);
      ierr = VecSetValues(phi_node_avg,4,nidx,phi_node,ADD_VALUES);CHKERRQ(ierr);
      
      //domain_ref += 1.0e-2 * sl->dx * sl->dy;
      //porosity = -(phi_cell - 1.0);
      //domain_residual += (porosity - 1.0e-2) * sl->dx * sl->dy;
      
      field_int  += phi_cell * dV;
      domain_vol += dV;
    }
  }
  ierr = VecRestoreArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(phi_avg);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_avg);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(phi_node_avg);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_node_avg);CHKERRQ(ierr);
  
  ierr = VecPointwiseDivide(phi_node_avg,phi_node_avg,phi_avg);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_avg);CHKERRQ(ierr);
  PetscTime(&t1);

  //domain_ref += 1.0e-2 * sl->dx * sl->dy;
  //printf("  \\int phi dV %+1.4e\n",domain_residual);
  //if (!beenhere) {
  //  domain_residual_initial_value = domain_residual;
  //}
  //if (debug > 0) {
  //  fprintf(fp,"%d %+1.12e %+1.12e\n",fcnt,domain_residual,100.0*domain_residual/domain_residual_initial_value);
  //}
  if (debug > 0)
  PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: \\int f(x) dV = %+1.12e : (1/|Omega|)\\int f(x) dV =  %+1.12e\n",field_int,field_int/domain_vol);
  
  if (debug > 0) {
    PetscReal min,max;
    
    VecMin(phi_node_avg,NULL,&min);
    VecMax(phi_node_avg,NULL,&max);
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: cell_avg [average]         %+1.4e (min) ; %+1.4e (max)\n",min,max);
  }

  /* reconstruct nodal values using monotone cspline with phi_node_avg as the limiter */
  ierr = DMGetCoordinates(dasl->dm,&coor);CHKERRQ(ierr);

#if 0
/////////
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi_node_avg,&LA_phi_node);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  for (k=0; k<L; k++) {
    PetscReal phi_r;
    
    /* node coordinate */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    /* Two choices 
     (i)  use the guaranteed monontone nodal values to define the limits; or
     (ii) use the cell avergaes directly to define the limits
    */
    //ierr = DMFVConservative_SLInterpolateAtPointQuasiMonotone_DMDA(sl,xn,(PetscScalar*)LA_phi_node,(PetscScalar*)LA_phi,&phi_r);CHKERRQ(ierr);
    ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,xn,(PetscScalar*)LA_phi,&phi_r);CHKERRQ(ierr);
    
    ierr = VecSetValue(phi_node,k,phi_r,INSERT_VALUES);CHKERRQ(ierr);
  }
  ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi_node_avg,&LA_phi_node);CHKERRQ(ierr);

  ierr = VecAssemblyBegin(phi_node);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(phi_node);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
/////////
#endif
  ierr = NodeReconstruction_CSplineAtPointQuasiMonotone_with_cell_data(dasl,phi,phi_node);CHKERRQ(ierr);
  
  PetscTime(&t2);

  if (debug > 0) {
    PetscReal min,max;
    
    //printf("phi [cspline-limit]\n");
    //VecView(phi_node,PETSC_VIEWER_STDOUT_WORLD);
    VecMin(phi_node_avg,NULL,&min);
    VecMax(phi_node,NULL,&max);
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: cell_avg [cspline-limiter] %+1.4e (min) ; %+1.4e (max)\n",min,max);
  }
  
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;
  
  ierr = VecGetArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  
  nsteps = 1;
  if (!dasl->trajectory_disable_substep) {
    PetscReal dt_cfl;
    
    ierr = FDSPMComputeTimestep_CFL(dasl->dactx,dasl->trajectory_cfl,(PetscScalar*)LA_velocity,&dt_cfl);CHKERRQ(ierr);
    if (dt > dt_cfl) {
      nsteps = (PetscInt)(dt / dt_cfl) + 1;
    }
  }
  if (debug > 0)
  PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: #RK4 steps per characteristic = %d \n",nsteps);
  
  // compute and store departure points
  ierr = PetscMalloc1(L*2,&x_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&phi_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&inflow_characteristic);CHKERRQ(ierr);
  
  for (k=0; k<L; k++) {
    PetscBool dep_left;
    
    /* determine departure point */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    inflow_characteristic[k] = PETSC_FALSE;
    ierr = SLComputeDeparturePoint_DMDA(sl,dt,nsteps,xn,xd,&dep_left);CHKERRQ(ierr);
    if (dep_left) {
      inflow_characteristic[k] = PETSC_TRUE;
    }
    
    /* interpolate phi[] at the departure point */
    phid = 0.0;
    switch (dasl->dactx->interpolation_type) {
      case DASLInterp_Q1:
        ierr = SLInterpolateAtPointQ1_DMDA(sl,xd,(PetscScalar*)LA_phi_node,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_CSpline:
        ierr = SLInterpolateAtPointCSpline_DMDA(sl,xd,(PetscScalar*)LA_phi_node,&phid);CHKERRQ(ierr);
        break;
        
      case DASLInterp_QMonotone:
        ierr = SLInterpolateAtPointQuasiMonotone_DMDA(sl,xd,(PetscScalar*)LA_phi_node,&phid);CHKERRQ(ierr);
        break;
    }
    
    /* store xd,phi */
    x_departure[2*k+0] = xd[0];
    x_departure[2*k+1] = xd[1];
    phi_departure[k] = phid;
    
    //if (debug > 0) {
    //  fprintf(fp,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e\n",xn[0],xn[1],0.0, xd[0],xd[1],phid);
    //}
  }
  
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  PetscTime(&t3);
  
  
  ierr = VecGetArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  
  /* compute volume intergrals */
  for (cj=0; cj<sl->my; cj++) {
    for (ci=0; ci<sl->mx; ci++) {
      PetscInt cid_ij,nidx[4];
      PetscReal vol_a,int_phi_ak,avg_int_phi_ak;
      PetscInt nqp = 4;
      PetscReal xi[] = { -0.57735026919,-0.57735026919 , -0.57735026919,0.57735026919 , 0.57735026919,0.57735026919 , 0.57735026919,-0.57735026919 };
      PetscReal w[] = { 1.0, 1.0, 1.0, 1.0 };
      /*
      PetscInt nqp = 9;
      PetscReal xi[] = {
        -0.774596669241483,-0.774596669241483 ,
        -0.774596669241483,0.0 ,
        -0.774596669241483,0.774596669241483 ,
        0.0,-0.774596669241483 ,
        0.0,0.0 ,
        0.0,0.774596669241483 ,
        0.774596669241483,-0.774596669241483 ,
        0.774596669241483,0.0 ,
        0.774596669241483,0.774596669241483 };
      PetscReal w[] = {
        0.3086419753086425 , 0.4938271604938276 , 0.3086419753086425 , 0.4938271604938276 , 0.7901234567901235 , 0.4938271604938276 , 0.3086419753086425 , 0.4938271604938276 , 0.3086419753086425 };
      */
      PetscInt q;
      PetscBool inflow_el[4],skip_int;
      PetscReal phi_avg = 0.0;
      
      PetscReal xdep_el[2*4],phidep_el[4];
      
      cid_ij = DACellIdx(sl,ci,cj);
      
      nidx[0] = DANodeIdx(sl,ci  ,cj);
      nidx[1] = DANodeIdx(sl,ci+1,cj);
      nidx[2] = DANodeIdx(sl,ci  ,cj+1);
      nidx[3] = DANodeIdx(sl,ci+1,cj+1);
      for (k=0; k<4; k++) {
        xdep_el[2*k+0] = x_departure[2*nidx[k]+0];
        xdep_el[2*k+1] = x_departure[2*nidx[k]+1];
        phidep_el[k]   = phi_departure[nidx[k]];
        inflow_el[k]   = inflow_characteristic[nidx[k]];
        phi_avg        += phidep_el[k];
      }
      phi_avg = phi_avg * 0.25;
      
      /* skip update on cell if backwards characteristics cross boundary */
      skip_int = PETSC_FALSE;
      for (k=0; k<4; k++) {
        if (inflow_el[k]) {
          skip_int = PETSC_TRUE;
          break;
        }
      }
      if (skip_int) {
        LA_phi_dep[cid_ij] = phi_avg;
        continue;
      }
      
      
      vol_a = 0.0;
      int_phi_ak = 0.0;
      for (q=0; q<nqp; q++) {
        PetscReal Ni[4],dNs1[4],dNs2[4];
        PetscReal phi_qp,dJ,xdep_q[] = {0.0,0.0};
        
        EvalBasis_Q1(&xi[2*q],Ni);
        EvalBasisDerivLocal_Q1(&xi[2*q],dNs1,dNs2);
        EvalGeometry_Q1(dNs1,dNs2,xdep_el,&dJ);
        
        phi_qp = 0.0;
        for (k=0; k<4; k++) {
          phi_qp    += Ni[k] * phidep_el[k];
          xdep_q[0] += Ni[k] * xdep_el[2*k+0];
          xdep_q[1] += Ni[k] * xdep_el[2*k+1];
        }

        //ierr = SLInterpolateAtPointQuasiMonotone_DMDA(sl,xdep_q,(PetscScalar*)LA_phi_node,&phi_qp);CHKERRQ(ierr);
        ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,xdep_q,(PetscScalar*)LA_phi,&phi_qp);CHKERRQ(ierr);
        //printf("%1.4e %1.4e  %1.4e \n",xdep_q[0],xdep_q[1],phi_qp);
        
        int_phi_ak += w[q] * phi_qp * dJ;
        vol_a      += w[q] * 1.0 * dJ;
      }
      
      avg_int_phi_ak = int_phi_ak/vol_a;
      
      //printf("vol %1.4e | %1.4e (phi-old) ** vol %1.4e | %1.4e (new)\n",vol_A,LA_phi_dep[cid_ij],vol_a,avg_int_phi_ak);
      
      /* mark inflow cells */
      if (vol_a < 1.0e-32) {
        PetscPrintf(PETSC_COMM_SELF,"[warning] FVConservativeSL: semi-Lagrangian backtracked cell %D %D has twisted, or degenerate Q1 element\n",ci,cj);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"FVConservativeSL: The SL cell is too deformed - suggest using a smaller time step");
      }
      
      LA_phi_dep[cid_ij] = avg_int_phi_ak;
      
    }
  }
  ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecRestoreArray(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(phi_node,&LA_phi_node);CHKERRQ(ierr);
  PetscTime(&t4);
  
  ierr = VecDestroy(&phi_node);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_node_avg);CHKERRQ(ierr);
  ierr = PetscFree(x_departure);CHKERRQ(ierr);
  ierr = PetscFree(phi_departure);CHKERRQ(ierr);
  ierr = PetscFree(inflow_characteristic);CHKERRQ(ierr);
  
  ierr = VecCopy(dasl->phi_dep,phi);CHKERRQ(ierr); /* phi <-- phi_departure */
  if (debug > 0) {
    fclose(fp);
    fcnt++;
  }
  //beenhere = PETSC_TRUE;
  if (debug > 0) {
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: reconstruct nodal avg      %1.4e (sec)\n",t1-t0);
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: reonstruct cspline-monotone %1.4e (sec)\n",t2-t1);
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: construct departure point  %1.4e (sec)\n",t3-t2);
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: compute cell average       %1.4e (sec)\n",t4-t3);
  }
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianUpdate"
PetscErrorCode DMDASemiLagrangianUpdate(DMSemiLagrangian dasl,PetscReal time,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  if (!dasl->setup) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Must call DMSLSetup() first");
    
  switch (dasl->sl_type) {
    case SLTYPE_CLASSIC:
      ierr = DMSemiLagrangianUpdate_DMDA(dasl,dt,phi);CHKERRQ(ierr);
      break;

    case SLTYPE_FVCONSERVATIVE:
      //ierr = DMFVConservativeSemiLagrangianUpdate_v0(dasl,dt,phi);CHKERRQ(ierr);
      ierr = DMFVConservativeSemiLagrangianUpdate_v2(dasl,dt,phi);CHKERRQ(ierr);
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
      ierr = DMFVConservativeSemiLagrangianUpdate_vX3(dasl,time,dt,phi);CHKERRQ(ierr);
      break;
      
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE:
      SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE unsupported");
      break;
      
    default:
      SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"Unknown semi-Lagrangian update type detected");
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianInterpolateGetAccess"
PetscErrorCode DMDASemiLagrangianInterpolateGetAccess(DMSemiLagrangian dasl)
{
  PetscErrorCode ierr;
  ierr = VecGetArrayRead(dasl->phi_dep,&dasl->LA_phi_dep);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianInterpolate"
PetscErrorCode DMDASemiLagrangianInterpolate(DMSemiLagrangian dasl,PetscReal coor[],PetscReal *val)
{
  PetscErrorCode ierr;
  SLDMDA    sl;
  
  if (!dasl->LA_phi_dep) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must call DMDASemiLagrangianInterpolateGetAccess() first");
  sl = dasl->dactx;
  switch (dasl->sl_type) {
    case SLTYPE_CLASSIC:
      switch (dasl->dactx->interpolation_type) {
        case DASLInterp_Q1:
          ierr = SLInterpolateAtPointQ1_DMDA(sl,coor,(PetscScalar*)dasl->LA_phi_dep,val);CHKERRQ(ierr);
          break;
          
        case DASLInterp_CSpline:
          ierr = SLInterpolateAtPointCSpline_DMDA(sl,coor,(PetscScalar*)dasl->LA_phi_dep,val);CHKERRQ(ierr);
          break;
          
        case DASLInterp_QMonotone:
          ierr = SLInterpolateAtPointQuasiMonotone_DMDA(sl,coor,(PetscScalar*)dasl->LA_phi_dep,val);CHKERRQ(ierr);
          break;
        default:
          SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"Unknown interpolation type detected");
          break;
      }

      break;
      
    case SLTYPE_FVCONSERVATIVE:
      ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor,(PetscScalar*)dasl->LA_phi_dep,val);CHKERRQ(ierr);
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
      ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor,(PetscScalar*)dasl->LA_phi_dep,val);CHKERRQ(ierr);
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE:
      SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE unsupported");
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"Unknown semi-Lagrangian type detected");
      break;

  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianInterpolateAtPoints"
PetscErrorCode DMDASemiLagrangianInterpolateAtPoints(DMSemiLagrangian dasl,PetscInt np,PetscReal coor[],PetscReal val[])
{
  PetscErrorCode ierr;
  PetscInt i;
  PetscReal *coor_i;
  SLDMDA    sl;
  
  if (!dasl->LA_phi_dep) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must call DMDASemiLagrangianInterpolateGetAccess() first");
  sl = dasl->dactx;
  switch (dasl->sl_type) {
    case SLTYPE_CLASSIC:
      switch (dasl->dactx->interpolation_type) {
        case DASLInterp_Q1:
          for (i=0; i<np; i++) {
            coor_i = &coor[2*i];
            ierr = SLInterpolateAtPointQ1_DMDA(sl,coor_i,(PetscScalar*)dasl->LA_phi_dep,&val[i]);CHKERRQ(ierr);
          }
          break;
          
        case DASLInterp_CSpline:
          for (i=0; i<np; i++) {
            coor_i = &coor[2*i];
            ierr = SLInterpolateAtPointCSpline_DMDA(sl,coor_i,(PetscScalar*)dasl->LA_phi_dep,&val[i]);CHKERRQ(ierr);
          }
          break;
          
        case DASLInterp_QMonotone:
          for (i=0; i<np; i++) {
            coor_i = &coor[2*i];
            ierr = SLInterpolateAtPointQuasiMonotone_DMDA(sl,coor_i,(PetscScalar*)dasl->LA_phi_dep,&val[i]);CHKERRQ(ierr);
          }
          break;
        default:
          SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"Unknown interpolation type detected");
          break;
      }
      
      break;
      
    case SLTYPE_FVCONSERVATIVE:
      for (i=0; i<np; i++) {
        coor_i = &coor[2*i];
        ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor_i,(PetscScalar*)dasl->LA_phi_dep,&val[i]);CHKERRQ(ierr);
      }
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
      for (i=0; i<np; i++) {
        coor_i = &coor[2*i];
        ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor_i,(PetscScalar*)dasl->LA_phi_dep,&val[i]);CHKERRQ(ierr);
      }
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE:
      SETERRQ(PetscObjectComm((PetscObject)dasl->dm),PETSC_ERR_SUP,"SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE unsupported");
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianInterpolateRestoreAccess"
PetscErrorCode DMDASemiLagrangianInterpolateRestoreAccess(DMSemiLagrangian dasl)
{
  PetscErrorCode ierr;
  ierr = VecRestoreArrayRead(dasl->phi_dep,&dasl->LA_phi_dep);CHKERRQ(ierr);
  dasl->LA_phi_dep = NULL;
  PetscFunctionReturn(0);
}

/*
 ComputeDeparturePoint_
 given xp0
 
 time = 0;
 for step=0; step<100000;
   compute dt based on cell wise-defined cfl containing xp
   if (dt+time > interval) dt = interval - time
 
   advance xp to xp(t-dt)
   if (xp_new outside domain or inside a mask cell) abort
 
   time = time + dt
   if (time >= interval) break;
 }
 
 if (aborted_rk4) {
   if (point_outside_domain)
     use rk1 to find intersection with boundary or valid cell
   if (point_inside_mask)
 
 
 }




*/

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePoint_RK1_BoundaryLimited_DMDA"
PetscErrorCode SLComputeDeparturePoint_RK1_BoundaryLimited_DMDA(
                    SLDMDA s,PetscReal dt,PetscReal xn[],
                             PetscReal xrange[],PetscReal yrange[],
                             PetscReal *dt_b,PetscReal xd[],PetscBool *boundary_limited)
{
  PetscReal vel[2];
  PetscReal xp[2],dt_min,dt_min_x,dt_min_y,xboundary = PETSC_MAX_REAL,yboundary = PETSC_MAX_REAL;
  PetscBool outside_domain;
  PetscInt  bflag_x,bflag_y;
  PetscErrorCode ierr;
  
  /* compute velocity at xn */
  ierr = SLComputeVelocity_DMDA(s,xn,vel);CHKERRQ(ierr);
  
  xp[0] = xn[0];
  xp[1] = xn[1];
  
  /* advect backwards */
  xp[0] = xp[0] - dt * vel[0];
  xp[1] = xp[1] - dt * vel[1];
  
  *boundary_limited = PETSC_FALSE;
  *dt_b = dt;
  outside_domain = PETSC_FALSE;
  bflag_x = bflag_y = 0;
  
  if (xp[0] < xrange[0]) {
    bflag_x = -1;
    outside_domain = PETSC_TRUE;
  }
  if (xp[0] > xrange[1]) {
    bflag_x = 1;
    outside_domain = PETSC_TRUE;
  }
  if (xp[1] < yrange[0]) {
    bflag_y = -1;
    outside_domain = PETSC_TRUE;
  }
  if (xp[1] > yrange[1]) {
    bflag_y = 1;
    outside_domain = PETSC_TRUE;
  }
  
  /*
   Solve for
   xboundary = xd - dtx vx
   yboundary = yd - dty vy
   */
  if (outside_domain) {
    *boundary_limited = PETSC_TRUE;

    dt_min_x = dt_min_y = 1.0e32;
    
    switch (bflag_x) {
      case -1: /* left */
        xboundary = xrange[0];
        dt_min_x = (xn[0] - xboundary) / vel[0];
        break;
      case  1: /* right */
        xboundary = xrange[1];
        dt_min_x = (xn[0] - xboundary) / vel[0];
        break;
    }
    
    switch (bflag_y) {
      case -1: /* bottom */
        yboundary = yrange[0];
        dt_min_y = (xn[1] - yboundary) / vel[1];
        break;
      case  1: /* top */
        yboundary = yrange[1];
        dt_min_y = (xn[1] - yboundary) / vel[1];
        break;
    }

    int err = 0;
    if (dt_min_x < 0.0) {printf("** error ** negative dt occuring (x-dir)\n");err=1;}
    if (dt_min_y < 0.0) {printf("** error ** negative dt occuring (y-dir)\n");err=1;}
    if (err==1) {
      printf("dt %1.4e\n",dt);
      printf("xn %1.4e %1.4e\n",xn[0],xn[1]);
      printf("xp %1.4e %1.4e\n",xp[0],xp[1]);
      printf("xrange [%1.4e %1.4e]\n",xrange[0],xrange[1]);
      printf("yrange [%1.4e %1.4e]\n",yrange[0],yrange[1]);
      
      if (bflag_x != 0) {
        printf("bflag_x %d \n",bflag_x);
        printf("xboundary %1.4e\n",xboundary);
        printf("dt_min_x %1.4e\n",dt_min_x);
      }
      if (bflag_y != 0) {
        printf("bflag_y %d \n",bflag_y);
        printf("yboundary %1.4e\n",yboundary);
        printf("dt_min_y %1.4e\n",dt_min_y);
      }
      exit(1);
    }
    
    /* the min dt in x,y indicates which boundary is crossed first */
    if (dt_min_x < dt_min_y) {
      dt_min = dt_min_x;
    } else {
      dt_min = dt_min_y;
    }
    xp[0] = xn[0] - dt_min * vel[0];
    xp[1] = xn[1] - dt_min * vel[1];
    *dt_b = dt_min;
  }
  
  xd[0] = xp[0];
  xd[1] = xp[1];
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeCellCFLTimestep_DMDA"
PetscErrorCode SLComputeCellCFLTimestep_DMDA(SLDMDA s,PetscReal pos[],PetscReal *dt_cfl)
{
  PetscInt  ci,cj;
  PetscInt  nidx[4],k;
  PetscReal vel[2],dt_vert,dt_min;
  
  ci = (PetscInt)( (pos[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (pos[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  nidx[0] = DANodeIdx(s,ci,  cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci,  cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);
  
  dt_min = 1.0e32;
  for (k=0; k<4; k++) {
    vel[0] = s->LA_vel[2*nidx[k]+0];
    dt_vert = s->dx / (PetscAbsReal(vel[0]) + 1.0e-20);
    if (dt_vert < dt_min) { dt_min = dt_vert; }
    
    vel[1] = s->LA_vel[2*nidx[k]+1];
    dt_vert = s->dy / (PetscAbsReal(vel[1]) + 1.0e-20);
    if (dt_vert < dt_min) { dt_min = dt_vert; }
    //printf("vel %1.4e %1.4e\n",vel[0],vel[1]);
  }
  *dt_cfl = dt_min;
  //printf("  cfl dt %1.4e \n",dt_min);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLPointLocation_DMDA"
PetscErrorCode SLPointLocation_DMDA(SLDMDA s,PetscReal pos[],PetscInt *cellid,PetscInt cellij[])
{
  PetscInt  ci,cj,cij;
  
  ci = (PetscInt)( (pos[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (pos[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  cij = DACellIdx(s,ci,cj);
  
  if (ci < 0) { cij = -1; ci = -1; }
  if (cj < 0) { cij = -1; cj = -1; }
  if (ci >= s->mx) { cij = -1; ci = -1; }
  if (cj >= s->my) { cij = -1; cj = -1; }
  
  *cellid = cij;
  if (cellij) {
    cellij[0] = ci;
    cellij[1] = cj;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePointMask_DMDA"
PetscErrorCode SLComputeDeparturePointMask_DMDA(SLDMDA s,PetscReal time_interval,PetscInt nsteps,PetscReal xn[],PetscReal xd_end[],PetscBool *deppoint_left_domain,PetscReal xd_b[])
{
  PetscErrorCode ierr;
  PetscBool    point_inside,boundary_limited;
  PetscInt     k;
  PetscReal    dt,xp[2],xd[2],xd_boundary[2],vel[2],time,dt_b;
  DPointStatus status;
  
  /* initialize position to be outside domain */
  xd[0] = -100.0 * s->xrange[0];
  xd[1] = -100.0 * s->yrange[0];
  xd_boundary[0] = -100.0 * s->xrange[0];
  xd_boundary[1] = -100.0 * s->yrange[0];
  *deppoint_left_domain = PETSC_FALSE;
  
  
  xp[0] = xn[0];
  xp[1] = xn[1];
  status = DPOINT_INIT;

  printf("in: %1.4e %1.4e \n",xp[0],xp[1]);
  
  time = 0.0;
  for (k=0; k<1000; k++) {
    
    ierr = SLComputeCellCFLTimestep_DMDA(s,xp,&dt);CHKERRQ(ierr);
    if (time + dt > time_interval) {
      dt = time_interval - time;
    }
    printf("  [%d] dt %1.4e target %1.4e\n",k,dt,time_interval);
    
    ierr = SLComputeDeparturePoint_RK4_DMDA(s,dt,xp,xd,&status);CHKERRQ(ierr);
    
    if (status == DPOINT_DEPARTED_DOMAIN) { /* point left domain */
      *deppoint_left_domain = PETSC_TRUE;
      break;
    }
    if (status == DPOINT_DEPARTED_VALID_MASK) { /* point left domain */
      *deppoint_left_domain = PETSC_TRUE;
      break;
    }
    
    /* If xp is valid (inside domain, outside a mask cell), update position and time */
    xp[0] = xd[0];
    xp[1] = xd[1];
    time = time + dt;
    
    if (time >= time_interval) break;
  }
  
  if (*deppoint_left_domain) {
    PetscReal xrange[2],yrange[2];
    
    ierr = SLComputeCellCFLTimestep_DMDA(s,xp,&dt);CHKERRQ(ierr);
    if (time + dt > time_interval) {
      dt = time_interval - time;
    }
    
    xrange[0] = s->xrange[0];
    xrange[1] = s->xrange[1];
    yrange[0] = s->yrange[0];
    yrange[1] = s->yrange[1];
    if (status == DPOINT_DEPARTED_DOMAIN) {
      ierr = SLComputeDeparturePoint_RK1_BoundaryLimited_DMDA(s,dt,xp,
                                                              xrange,yrange,
                                                              &dt_b,xd,&boundary_limited);CHKERRQ(ierr);
      printf("out-RK1: %1.4e %1.4e \n",xd[0],xd[1]);
    }

    if (status == DPOINT_DEPARTED_VALID_MASK) {
    //if (status == DPOINT_DEPARTED_DOMAIN) {
      PetscInt cellid,cellij[2];
      
      ierr = SLPointLocation_DMDA(s,xp,&cellid,cellij);CHKERRQ(ierr);
      if (cellid < 0) {
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"SLPointLocation_DMDA: Failed... point outside domain");
      }
      
      xrange[0] = s->xrange[0] + cellij[0] * s->dx;
      xrange[1] = s->xrange[0] + s->dx;
      yrange[0] = s->yrange[0] + cellij[1] * s->dy;
      yrange[1] = s->yrange[0] + s->dy;

      ierr = SLComputeDeparturePoint_RK1_BoundaryLimited_DMDA(s,dt,xp,
                                                              xrange,yrange,
                                                              &dt_b,xd,&boundary_limited);CHKERRQ(ierr);
    }
    
    xd_boundary[0] = xd[0];
    xd_boundary[1] = xd[1];
    
    /* Do a final check to ensure boundary location is correct */
    ierr = SLPointInsideDomain_DMDA(s,xd,&point_inside);CHKERRQ(ierr);
    if (!point_inside) {
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"ComputeDeparturePoint: Failed... point outside domain");
    }
    
    /* 
     To enable interpolating quantities (concentration,phi) on the boundary, we should either
     (i) return the boundary coordinate, or
     (ii) perform the last stage of advection outside of this function
    */
    if (time + dt_b > time_interval) {
      /* point moved too far - this should never happen */
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"ComputeDeparturePoint: Failed... boundary point advective step is larger than time interval");
    } else {
      /* move point for the time period (time_interval - time), using the current (boundary) velocity */
      /* accept xd as the next updated position */
      xp[0] = xd[0];
      xp[1] = xd[1];
      time = time + dt_b;
      
      /* compute velocity */
      ierr = SLComputeVelocity_DMDA(s,xp,vel);CHKERRQ(ierr);
      
      dt = time_interval - time;
      xp[0] = xp[0] - dt * vel[0];
      xp[1] = xp[1] - dt * vel[1];
      time = time + dt;
    }
    printf("out: %1.4e %1.4e \n",xp[0],xp[1]);
  }
  
  //printf("time interval %1.4e : time %1.4e\n",time_interval,time);
  
  printf("out: %1.4e %1.4e \n",xp[0],xp[1]);
  xd_end[0] = xp[0];
  xd_end[1] = xp[1];
  
  /* these only have meaning when deppoint_left_domain = PETSC_TRUE */
  xd_b[0] = xd_boundary[0];
  xd_b[1] = xd_boundary[1];
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLInterpolateVertexFieldQ1_DMDA"
PetscErrorCode SLInterpolateVertexFieldQ1_DMDA(SLDMDA s,PetscReal pos[],PetscReal vertex_field[],PetscReal field[])
{
  PetscInt  ci,cj;
  PetscInt  nidx[4],k;
  PetscReal x0[2],xi[2],Ni[4];
  
  ci = (PetscInt)( (pos[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (pos[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  nidx[0] = DANodeIdx(s,ci,  cj);
  nidx[1] = DANodeIdx(s,ci+1,cj);
  nidx[2] = DANodeIdx(s,ci,  cj+1);
  nidx[3] = DANodeIdx(s,ci+1,cj+1);
  
  x0[0] = s->xrange[0] + ci * s->dx;
  x0[1] = s->yrange[0] + cj * s->dy;
  
  /*
   [x - x0]/dx = [xi - (-1)]/2
   */
  
  xi[0] = 2.0*(pos[0]-x0[0])/s->dx - 1.0;
  xi[1] = 2.0*(pos[1]-x0[1])/s->dy - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  field[0] = 0.0;
  for (k=0; k<4; k++) {
    field[0] += Ni[k] * vertex_field[nidx[k]];
  }
  
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_vX"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt k;
  PetscReal xn[2],xd[2];
  Vec coor;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscInt L_vert;
  static PetscInt fcnt = 0;
  FILE *fp_traj = NULL;
  FILE *fp_dep_cell = NULL;
  PetscInt debug = dasl->debug;
  SLDMDA    sl;
  PetscReal *x_departure,*x_departure_b;
  PetscBool *inflow_characteristic;
  PetscLogDouble t0,t1;
  //static PetscBool beenhere = PETSC_FALSE;
  
  sl = dasl->dactx;
  
  if (debug > 1) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sl_traj-%D.gp",fcnt);
    fp_traj = fopen(name,"w");
    fprintf(fp_traj,"# frame %d \n",fcnt);

    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sl_depcell-%D.gp",fcnt);
    fp_dep_cell = fopen(name,"w");
    fprintf(fp_dep_cell,"# frame %d \n",fcnt);

  }

  L_vert = sl->nx * sl->ny;
  
  /* ======= Phase 1 ======= */
  /* compute and store departure points */
  ierr = DMGetCoordinates(dasl->dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;
  
  ierr = PetscMalloc1(L_vert*2,&x_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L_vert*2,&x_departure_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(L_vert,&inflow_characteristic);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (k=0; k<L_vert; k++) {
    PetscBool dep_left;
    PetscReal xd_boundary[2];
    
    /* determine departure point */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    dep_left = PETSC_FALSE;
    ierr = SLComputeDeparturePointMask_DMDA(sl,dt,1,xn,xd,&dep_left,xd_boundary);CHKERRQ(ierr);
    
    /* store xd,inflow status */
    x_departure[2*k+0] = xd[0];
    x_departure[2*k+1] = xd[1];
    inflow_characteristic[k] = dep_left;
    x_departure_b[2*k+0] = xd_boundary[0];
    x_departure_b[2*k+1] = xd_boundary[1];
  }
  PetscTime(&t1);
  
  /* ======= Phase 2 ======= */
  /*
   For all characteristics which exited the domain (inflowing characteristics), set the 
   field value using the provided vertex values for the field.
   We interpolate provided vertex values using the coordinates x_departure_b[] which denote
   the location where the characteristic crossed the boundary.
  */
#if 0
  /* convert flux vertex values into field, flux = c v */
  for (k=0; k<L_vert; k++) {
    field_value_b[k] = 0.0;
    
    field_value_b[k] = user_field_b[k];
  }
  
  /* convert fluxes */
  for (k=0; k<L_vert; k++) {
    if (vertex_bc[k] == FLUX) {
      PetscReal norm_F,norm_V;
      
      norm_F = PetscSqrtReal(user_flux_b[2*k]*user_flux_b[2*k] + user_flux_b[2*k+1]*user_flux_b[2*k+1])
      norm_V = PetscSqrtReal(LA_velocity[2*k]*LA_velocity[2*k] + LA_velocity[2*k+1]*LA_velocity[2*k+1])
      
      field_value_b[k] = norm_F / norm_V;
    }
  }

  for (k=0; k<L_vert; k++) {
    PetscReal field_b,coor[2];
    
    if (inflow_characteristic[k] == PETSC_FALSE) continue;
    
    coor[0] = x_departure_b[0];
    coor[1] = x_departure_b[1];
    
    ierr = SLInterpolateVertexFieldQ1_DMDA(sl,coor,field_value_b,&field_b);CHKERRQ(ierr);
    
  }
#endif
  
  
  /* departure coordinates */
  if (debug > 1) {
    for (k=0; k<L_vert; k++) {
      fprintf(fp_traj,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e  %1.4e\n",
              LA_coor[2*k+0],LA_coor[2*k+1],0.0, x_departure[2*k+0],x_departure[2*k+1],0.0,(double)inflow_characteristic[k]);
    }
  }
  
  /* departure cells */
  if (debug > 1) {
    int ci,cj,verts[4];

    for (cj=0; cj<sl->my; cj++) {
      for (ci=0; ci<sl->mx; ci++) {

        verts[0] = DANodeIdx(sl,ci,  cj);
        verts[1] = DANodeIdx(sl,ci+1,cj);
        verts[2] = DANodeIdx(sl,ci+1,cj+1);
        verts[3] = DANodeIdx(sl,ci,  cj+1);

        for (k=0; k<4; k++) {
          fprintf(fp_dep_cell,"%1.4e %1.4e \n",x_departure[2*verts[k]+0],x_departure[2*verts[k]+1]);
        }
        fprintf(fp_dep_cell,"%1.4e %1.4e \n\n",x_departure[2*verts[0]+0],x_departure[2*verts[0]+1]);
      }
    }
  }
  
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  
  /* Update */
  ierr = VecCopy(dasl->phi_dep,phi);CHKERRQ(ierr); /* phi <-- phi_departure */

  if (fp_traj) { fclose(fp_traj); }
  if (fp_dep_cell) { fclose(fp_dep_cell); }
  fcnt++;
  //beenhere = PETSC_TRUE;
  
  if (debug > 0) {
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: construct departure point  %1.4e (sec)\n",t1-t0);
    //PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: reconstruct nodal avg      %1.4e (sec)\n",t1-t0);
    //PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: reonstruct cspline-monotone %1.4e (sec)\n",t2-t1);
    //PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: compute cell average       %1.4e (sec)\n",t4-t3);
  }

  ierr = PetscFree(x_departure_b);CHKERRQ(ierr);
  ierr = PetscFree(x_departure);CHKERRQ(ierr);
  ierr = PetscFree(inflow_characteristic);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "SLPointLocation_SubCell"
PetscErrorCode SLPointLocation_SubCell(PetscReal xbounds[],PetscReal ybounds[],PetscInt mx,PetscInt my,PetscReal pos[],PetscInt *cellid,PetscInt cellij[])
{
  PetscInt  ci,cj,cij;
  PetscReal dx,dy;
  
  dx = (xbounds[1] - xbounds[0])/((PetscReal)mx);
  dy = (ybounds[1] - ybounds[0])/((PetscReal)my);
  
  ci = (PetscInt)( (pos[0] - xbounds[0])/dx );
  cj = (PetscInt)( (pos[1] - ybounds[0])/dy );
  if (ci == mx) ci--;
  if (cj == my) cj--;
  
  cij = ci + cj * mx;
  
  if (ci < 0) { cij = -1; ci = -1; }
  if (cj < 0) { cij = -1; cj = -1; }
  if (ci >= mx) { cij = -1; ci = -1; }
  if (cj >= my) { cij = -1; cj = -1; }
  
  *cellid = cij;
  if (cellij) {
    cellij[0] = ci;
    cellij[1] = cj;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLPointDomainQuery_DMDASubCell"
PetscErrorCode SLPointDomainQuery_DMDASubCell(SLDMDA s,FVStorage fv,PetscReal pos[],PetscBool *inside_domain,PetscBool *inside_mask)
{
  PetscErrorCode ierr;
  PetscInt cellid;
  
  *inside_domain  = PETSC_TRUE;
  *inside_mask  = PETSC_TRUE;
  ierr = SLPointInsideDomain_DMDA(s,pos,inside_domain);CHKERRQ(ierr);
  if (*inside_domain) {
    ierr = SLPointLocation_DMDA(s,pos,&cellid,NULL);CHKERRQ(ierr);
    if (fv->cell_type[cellid] == SLCELL_GREY) {
    //if ((fv->cell_type[cellid] == SLCELL_ORANGE) || (fv->cell_type[cellid] == SLCELL_GREY)) {
      *inside_mask = PETSC_FALSE;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePoint_RK2_DMDASubCell"
PetscErrorCode SLComputeDeparturePoint_RK2_DMDASubCell(SLDMDA s,FVStorage fv,double dt,double xn[],double xd[],DPointStatus *status)
{
  PetscErrorCode ierr;
  PetscReal k1[2],k2[2];
  PetscBool inside_domain,inside_mask;
  PetscReal pos[2];
  
  pos[0] = xn[0];
  pos[1] = xn[1];
  ierr = SLComputeVelocity_DMDA(s,pos,k1);CHKERRQ(ierr); /* t = t_k */
  
  if (PetscSqrtReal(k1[0]*k1[0]+k1[1]*k1[1]) < SL_MIN_VELOCITY) {
    xd[0] = xn[0];
    xd[1] = xn[1];
    *status = DPOINT_FINALIZED;
    PetscFunctionReturn(0);
  }
  
  k1[0] = -k1[0];
  k1[1] = -k1[1];
  
  pos[0] = xn[0] + 0.5 * dt * k1[0];
  pos[1] = xn[1] + 0.5 * dt * k1[1];
  ierr = SLPointDomainQuery_DMDASubCell(s,fv,pos,&inside_domain,&inside_mask);CHKERRQ(ierr);
  if (!inside_domain) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  if (!inside_mask)   { *status = DPOINT_DEPARTED_VALID_MASK; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k2);CHKERRQ(ierr); /* t = t_k - 0.5*dt */
  k2[0] = -k2[0];
  k2[1] = -k2[1];
  
  
  xd[0] = xn[0] + dt * k2[0];
  xd[1] = xn[1] + dt * k2[1];
  ierr = SLPointDomainQuery_DMDASubCell(s,fv,xd,&inside_domain,&inside_mask);CHKERRQ(ierr);
  if (!inside_domain) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  if (!inside_mask)   { *status = DPOINT_DEPARTED_VALID_MASK; PetscFunctionReturn(0); }
  
  *status = DPOINT_FINALIZED;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePoint_RK4_DMDASubCell"
PetscErrorCode SLComputeDeparturePoint_RK4_DMDASubCell(SLDMDA s,FVStorage fv,double dt,double xn[],double xd[],DPointStatus *status)
{
  PetscErrorCode ierr;
  PetscReal k1[2],k2[2],k3[2],k4[2];
  PetscBool inside_domain,inside_mask;
  PetscReal pos[2];
  
  pos[0] = xn[0];
  pos[1] = xn[1];
  ierr = SLComputeVelocity_DMDA(s,pos,k1);CHKERRQ(ierr); /* t = t_k */
  
  if (PetscSqrtReal(k1[0]*k1[0]+k1[1]*k1[1]) < SL_MIN_VELOCITY) {
    xd[0] = xn[0];
    xd[1] = xn[1];
    *status = DPOINT_FINALIZED;
    PetscFunctionReturn(0);
  }
  
  k1[0] = -k1[0];
  k1[1] = -k1[1];
  
  pos[0] = xn[0] + 0.5 * dt * k1[0];
  pos[1] = xn[1] + 0.5 * dt * k1[1];
  ierr = SLPointDomainQuery_DMDASubCell(s,fv,pos,&inside_domain,&inside_mask);CHKERRQ(ierr);
  if (!inside_domain) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  if (!inside_mask)   { *status = DPOINT_DEPARTED_VALID_MASK; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k2);CHKERRQ(ierr); /* t = t_k - 0.5*dt */
  k2[0] = -k2[0];
  k2[1] = -k2[1];
  
  pos[0] = xn[0] + 0.5 * dt * k2[0];
  pos[1] = xn[1] + 0.5 * dt * k2[1];
  ierr = SLPointDomainQuery_DMDASubCell(s,fv,pos,&inside_domain,&inside_mask);CHKERRQ(ierr);
  if (!inside_domain) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  if (!inside_mask)   { *status = DPOINT_DEPARTED_VALID_MASK; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k3);CHKERRQ(ierr); /* t = t_k - 0.5*dt */
  k3[0] = -k3[0];
  k3[1] = -k3[1];
  
  pos[0] = xn[0] + dt * k3[0];
  pos[1] = xn[1] + dt * k3[1];
  ierr = SLPointDomainQuery_DMDASubCell(s,fv,pos,&inside_domain,&inside_mask);CHKERRQ(ierr);
  if (!inside_domain) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  if (!inside_mask)   { *status = DPOINT_DEPARTED_VALID_MASK; PetscFunctionReturn(0); }
  ierr = SLComputeVelocity_DMDA(s,pos,k4);CHKERRQ(ierr); /* t = t_k - dt */
  k4[0] = -k4[0];
  k4[1] = -k4[1];
  
  xd[0] = xn[0] + (dt/6.0)*( k1[0] + 2.0*k2[0] + 2.0*k3[0] + k4[0] );
  xd[1] = xn[1] + (dt/6.0)*( k1[1] + 2.0*k2[1] + 2.0*k3[1] + k4[1] );
  ierr = SLPointDomainQuery_DMDASubCell(s,fv,xd,&inside_domain,&inside_mask);CHKERRQ(ierr);
  if (!inside_domain) { *status = DPOINT_DEPARTED_DOMAIN; PetscFunctionReturn(0); }
  if (!inside_mask)   { *status = DPOINT_DEPARTED_VALID_MASK; PetscFunctionReturn(0); }
  
  *status = DPOINT_FINALIZED;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "trial_SLComputeDeparturePointMask_DMDASubCell"
PetscErrorCode trial_SLComputeDeparturePointMask_DMDASubCell(SLDMDA s,FVStorage fv,PetscReal time_interval,PetscInt nsteps,PetscReal xn[],PetscReal xd_end[],PetscBool *deppoint_left_domain)
{
  PetscErrorCode ierr;
  PetscInt     k,cellid,cij[2];
  PetscReal    dt,xp[2],xd[2],time;
  DPointStatus status;
  //FILE *fp = NULL;
  
  /* initialize position to be outside domain */
  xd[0] = -100.0 * s->xrange[0];
  xd[1] = -100.0 * s->yrange[0];
  *deppoint_left_domain = PETSC_FALSE;


  //fp = fopen("characteristic.gp","w");
  
  xp[0] = xn[0];
  xp[1] = xn[1];
  status = DPOINT_INIT;
  
  ierr = SLPointLocation_DMDA(s,xn,&cellid,NULL);CHKERRQ(ierr);
  //fprintf(fp,"%+1.10e %+1.10e %d\n",xn[0],xn[1],cellid);
  //printf("  [dep update] initial condition %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xn[0],xn[1],cellid,(int)fv->subcell[cellid]->valid);

  //printf("  [dep update] initial condition %+1.14e %+1.14e : cell %d : [is sub-cell %d] : cell_type %d \n",
  //       xn[0],xn[1],cellid,(int)fv->subcell[cellid]->valid,(int)fv->cell_type[cellid]);
  
  time = 0.0;
  for (k=0; k<1000; k++) {
    //printf("  iteration [%d] \n",k);
    
    ierr = SLComputeCellCFLTimestep_DMDA(s,xp,&dt);CHKERRQ(ierr);
    dt = dt / 10.0;
    ierr = SLPointLocation_DMDA(s,xp,&cellid,cij);CHKERRQ(ierr);
    
    /* shorten dt by amount num subcells in x,y if this is a split cell */
    if (fv->subcell[cellid]->valid) {
      PetscReal dts;
      FVSubCellStorage subcell;
      PetscReal xbounds[2],ybounds[2];
      PetscInt sub_cellid;
      
      dts = dt / ((PetscReal)fv->subcell[cellid]->mx);
      if (fv->subcell[cellid]->my > fv->subcell[cellid]->mx) {
        dts = dt / ((PetscReal)fv->subcell[cellid]->my);
      }
      dt = dts;
      
      
      
      subcell = fv->subcell[cellid];
      
      xbounds[0] = s->xrange[0] + cij[0] * s->dx;
      xbounds[1] = xbounds[0] + s->dx;
      ybounds[0] = s->yrange[0] + cij[1] * s->dy;
      ybounds[1] = ybounds[0] + s->dy;
      
      ierr = SLPointLocation_SubCell(xbounds,ybounds,subcell->mx,subcell->my,xp,&sub_cellid,NULL);CHKERRQ(ierr);
      //printf("  sub_cellid = %d\n",sub_cellid);
      
      if (subcell->state[sub_cellid] == 0) {
        PetscReal sep,min_sep;
        PetscInt closest_cell;
        PetscInt sc;
        
        //SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"State = 0: Depature point landed in a sub-cell which is outside the domain ** panic **");
        //printf("State = 0: Depature point located in a sub-cell which is outside the domain ** find nearest valid Dirichlet sub-cell centoid ** \n");
        
        min_sep = 1.0e32;
        closest_cell = -1;
        for (sc=0; sc<subcell->mx*subcell->my; sc++) {
          sep  = (xp[0] - subcell->centroid[2*sc])*(xp[0] - subcell->centroid[2*sc]);
          sep += (xp[1] - subcell->centroid[2*sc+1])*(xp[1] - subcell->centroid[2*sc+1]);
          if (subcell->state[sc] == 1) {
            if (sep < min_sep) {
              min_sep = sep;
              closest_cell = sc;
            }
          }
        }
        if (closest_cell == -1) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Failed to locate an internal, Dirichlet sub-cell");
        
        xp[0] = subcell->centroid[ 2*closest_cell    ];
        xp[1] = subcell->centroid[ 2*closest_cell +1 ];
        break;
        
      }
      if (subcell->state[sub_cellid] == 2) {
        
      }
      /* If current coordinate on the characteristic is inside a sub-cell marked as Dirichlet - terminate characteristic integration */
      if (subcell->state[sub_cellid] == 1) {
        break;
      }
    }
    
    /* correct dt to ensure last step terminates at time = time_interval */
    if (time + dt > time_interval) {
      dt = time_interval - time;
    }
    //printf("  [%d] dt %1.4e target %1.4e\n",k,dt,time_interval);
    
    ierr = SLComputeDeparturePoint_RK4_DMDASubCell(s,fv,dt,xp,xd,&status);CHKERRQ(ierr);
    ierr = SLPointLocation_DMDA(s,xd,&cellid,NULL);CHKERRQ(ierr);
    //fprintf(fp,"%+1.10e %+1.10e %d\n",xd[0],xd[1],cellid);
    //printf("  [dep update] IN coor %+1.14e %+1.14e : OUT coor %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xp[0],xp[1],xd[0],xd[1],cellid,(int)fv->subcell[cellid]->valid);
    //printf("  [dep update] coor %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xd[0],xd[1],cellid,(int)fv->subcell[cellid]->valid);


    if (status == DPOINT_DEPARTED_DOMAIN) { /* point left DMDA domain */
      //printf("  xd is outside DMDA \n");
      *deppoint_left_domain = PETSC_TRUE;
      break;
    }
    if (status == DPOINT_DEPARTED_VALID_MASK) { /* point left mask domain */
      //printf("  xd is outside mask \n");
      *deppoint_left_domain = PETSC_TRUE;
      break;
    }
    
    /* If xp is valid (inside domain, outside a mask cell), update position and time */
    xp[0] = xd[0];
    xp[1] = xd[1];
    time = time + dt;
    //printf("  updating coordinate keeping %+1.14e %+1.14e\n",xp[0],xp[1]);
    
    if (time >= time_interval) break;
  }
  
  xd_end[0] = xp[0];
  xd_end[1] = xp[1];
  //fclose(fp);

  //printf("  final coordinate %+1.14e %+1.14e\n",xd_end[0],xd_end[1]);
  
  /* check that if point exited domain that it landed in a sub-divided cell on the last valid time intgration step */
  if (*deppoint_left_domain) {
    ierr = SLPointLocation_DMDA(s,xd_end,&cellid,NULL);CHKERRQ(ierr);
    
    if (!fv->subcell[cellid]->valid) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Departure point left domain but did not end up in a sub-cell");
  }
  
  PetscFunctionReturn(0);
}

void matrix_inverse_3x3(PetscReal A[3][3],PetscReal B[3][3])
{
  PetscReal t4, t6, t8, t10, t12, t14, t17;
  
  t4 = A[2][0] * A[0][1];
  t6 = A[2][0] * A[0][2];
  t8 = A[1][0] * A[0][1];
  t10 = A[1][0] * A[0][2];
  t12 = A[0][0] * A[1][1];
  t14 = A[0][0] * A[1][2];
  t17 = 0.1e1 / (t4 * A[1][2] - t6 * A[1][1] - t8 * A[2][2] + t10 * A[2][1] + t12 * A[2][2] - t14 * A[2][1]);
  
  B[0][0] = (A[1][1] * A[2][2] - A[1][2] * A[2][1]) * t17;
  B[0][1] = -(A[0][1] * A[2][2] - A[0][2] * A[2][1]) * t17;
  B[0][2] = (A[0][1] * A[1][2] - A[0][2] * A[1][1]) * t17;
  B[1][0] = -(-A[2][0] * A[1][2] + A[1][0] * A[2][2]) * t17;
  B[1][1] = (-t6 + A[0][0] * A[2][2]) * t17;
  B[1][2] = -(-t10 + t14) * t17;
  B[2][0] = (-A[2][0] * A[1][1] + A[1][0] * A[2][1]) * t17;
  B[2][1] = -(-t4 + A[0][0] * A[2][1]) * t17;
  B[2][2] = (-t8 + t12) * t17;
}

void matrix_solve_3x3(double A[3][3],double b[],double x[])
{
  PetscReal B[3][3];
  PetscInt i,j;
  
  matrix_inverse_3x3(A,B);
  for (i=0; i<3; i++) {
    x[i] = 0.0;
    
    for (j=0; j<3; j++) {
      x[i] += B[i][j] * b[j];
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "Reconstruct_LowOrderQ1"
PetscErrorCode Reconstruct_LowOrderQ1(PetscReal origin[],PetscReal ds,
                            PetscInt np,PetscReal coor[],PetscReal field[],
                            PetscInt np_out,PetscReal coor_out[],PetscReal field_out[])
{
  const PetscInt nbasis = 3;
  PetscReal A[nbasis][nbasis],coeff[nbasis],b[nbasis];
  PetscInt i,j,p;
  
  for (i=0; i<nbasis; i++) { for (j=0; j<nbasis; j++) { A[i][j] = 0.0; }}
  for (i=0; i<nbasis; i++) { b[i] = 0.0; }
  
  for (p=0; p<np; p++) {
    PetscReal basis[nbasis];
    
    basis[0] = 1.0;
    basis[1] = (coor[2*p+0] - origin[0])/ds;
    basis[2] = (coor[2*p+1] - origin[1])/ds;
    
    for (i=0; i<nbasis; i++) {
      for (j=0; j<nbasis; j++) {
        A[i][j] += basis[i] * basis[j];
      }
      b[i] += basis[i] * field[p];
    }
  }
  
  matrix_solve_3x3(A,b,coeff);
  
  
  for (p=0; p<np_out; p++) {
    PetscReal basis[nbasis];
    
    basis[0] = 1.0;
    basis[1] = (coor_out[2*p+0] - origin[0])/ds;
    basis[2] = (coor_out[2*p+1] - origin[1])/ds;
    
    field_out[p] = 0.0;
    for (i=0; i<nbasis; i++) {
      field_out[p] += basis[i] * coeff[i];
    }
    
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FictDomainCSL_InterpolateAtPointQuasiMonotone"
PetscErrorCode FictDomainCSL_InterpolateAtPointQuasiMonotone(SLDMDA s,FVStorage fv,PetscReal xd[],PetscInt low_order_interp_type,PetscScalar phi_cell[],PetscReal *_phi_M)
{
  PetscErrorCode ierr;
  PetscInt ci,cj,cidx,cni,cnj,cell_neighbour,cell_neighbours[9],cell_neighbours_ij[9][2],ii,jj;
  PetscReal phi_n,phi_H,phi_minus,phi_plus,phi_M;
  PetscReal dx[2];
  PetscInt nxny[2];
  SLCellType stencil[9],types[9];
  PetscBool use_low_order,apply_limiter;
  PetscInt npoints;
  PetscReal centroids[2*9],values[9];
  
  use_low_order = PETSC_FALSE;
  apply_limiter = PETSC_TRUE;
  
  for (ii=0; ii<9; ii++) {
    stencil[ii] = SLCELL_INIT;
    cell_neighbours_ij[ii][0] = -1;
    cell_neighbours_ij[ii][1] = -1;
    cell_neighbours[ii] = -1;
    types[ii] = SLCELL_INIT;
    values[ii] = 0.0;
  }
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  cidx = DACellIdx(s,ci,cj);
  
  /* get bounds from 8 neighbours + self */
  phi_minus = 1.0e20;
  phi_plus  = -1.0e20;
  for (cnj=cj-1; cnj<=cj+1; cnj++) {
    if (cnj < 0) continue;
    if (cnj > (s->my-1)) continue;
    
    for (cni=ci-1; cni<=ci+1; cni++) {
      if (cni < 0) continue;
      if (cni > (s->mx-1)) continue;
      
      cell_neighbour = DACellIdx(s,cni,cnj);
      
      ii = cni - (ci-1);
      jj = cnj - (cj-1);
      stencil[ii+jj*3] = fv->cell_type[cell_neighbour];
      cell_neighbours_ij[ii+jj*3][0] = cni;
      cell_neighbours_ij[ii+jj*3][1] = cnj;
      cell_neighbours[ii+jj*3]       = cell_neighbour;
      
      phi_n = phi_cell[ cell_neighbour ];
      
      if (fv->cell_type[cell_neighbour] != SLCELL_GREY) {
        if (phi_n > phi_plus)  { phi_plus = phi_n; }
        if (phi_n < phi_minus) { phi_minus = phi_n; }
      }
    }
  }
  
  nxny[0] = s->mx;
  nxny[1] = s->my;
  dx[0]   = s->dx;
  dx[1]   = s->dy;

  // 6 7 8
  // 3 4 5
  // 0 1 2
  npoints = 0;
  for (ii=0; ii<9; ii++) {
    //if ((stencil[ii] == SLCELL_ORANGE) || (stencil[ii] == SLCELL_GREY)) {
    if (stencil[ii] == SLCELL_GREY) {
      use_low_order = PETSC_TRUE;
    }
    if (stencil[ii] == SLCELL_ORANGE) {
      use_low_order = PETSC_TRUE;
    }
    if (stencil[ii] == SLCELL_YELLOW) {
      use_low_order = PETSC_TRUE;
    }
    if ((stencil[ii] == SLCELL_BLUE) || (stencil[ii] == SLCELL_YELLOW) || (stencil[ii] == SLCELL_ORANGE)) {
    //if ((stencil[ii] == SLCELL_BLUE) || (stencil[ii] == SLCELL_YELLOW)) {
    //if (stencil[ii] == SLCELL_BLUE) {
      centroids[2*npoints+0] = s->xrange[0] + 0.5 * dx[0] + cell_neighbours_ij[ii][0] * dx[0];
      centroids[2*npoints+1] = s->yrange[0] + 0.5 * dx[1] + cell_neighbours_ij[ii][1] * dx[1];
      values[npoints]        = phi_cell[ cell_neighbours[ii] ];
      types[npoints]         = stencil[ii];
      //cell_neighbours_f[npoints] = cell_neighbours[ii];
      
      npoints++;
    }
  }
  
  if (use_low_order) {
    if (low_order_interp_type > 3) { // (1) P1 : (2) sub-cell P0 : (3) P0
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Low order interpolation type is either 1 or 2");
    }
    if (low_order_interp_type == 1) {
      apply_limiter = PETSC_TRUE;
    }
    if (low_order_interp_type == 2) {
      apply_limiter = PETSC_FALSE;
    }
    if (low_order_interp_type == 3) {
      apply_limiter = PETSC_FALSE;
    }
  }
  
  if (!use_low_order) {
    ierr = DMDA_InterpolateAtPoint_CSpline(CTYPE_CENTROID,nxny,dx,s->xrange,s->yrange,
                                           xd,phi_cell,&phi_H);CHKERRQ(ierr);
  } else {
    
    if (low_order_interp_type == 1) {
      PetscReal origin[2],ds;
      
      if (npoints < 3) {
        printf("[Reconstruct_LowOrderP1] npoints = %d \n",npoints);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Insufficient points for reconstruction");
      }

      origin[0] = s->xrange[0] + ci * s->dx + 0.5 * s->dx;
      origin[1] = s->yrange[0] + cj * s->dy + 0.5 * s->dy;
      
      ds = dx[0];
      if (dx[1] < dx[0]) {
        ds = dx[1];
      }
      ierr = Reconstruct_LowOrderQ1(origin,ds,npoints,centroids,values,1,xd,&phi_H);CHKERRQ(ierr);
      /*
      for (ii=0; ii<npoints; ii++) {
        printf("  neighbours[%d] : values[] %+1.6e : types[] %d : cell_neighbours_f[] %d\n",ii,values[ii],(int)types[ii],cell_neighbours_f[ii]);
      }
      printf("interp phi_H = %+1.6e\n",phi_H);
      */
    }
    
    if (low_order_interp_type == 2) {
      FVSubCellStorage subcell;
      PetscReal        xbounds[2],ybounds[2];
      PetscInt         sub_cellid;
      
      if (!fv->subcell[cidx]->valid) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot employ sub-cell P0 interpolation as cell has not been sub-divided");
      
      subcell = fv->subcell[cidx];
      
      xbounds[0] = s->xrange[0] + ci * s->dx;
      xbounds[1] = xbounds[0]   + s->dx;
      ybounds[0] = s->yrange[0] + cj * s->dy;
      ybounds[1] = ybounds[0]   + s->dy;
      
      ierr = SLPointLocation_SubCell(xbounds,ybounds,subcell->mx,subcell->my,xd,&sub_cellid,NULL);CHKERRQ(ierr);
      
      phi_H = subcell->phi[ sub_cellid ];
    }

    if (low_order_interp_type == 3) {
      PetscReal sep,min_sep[2];
      PetscInt closest,closest_blue = -1,closest_yellow = -1;
      PetscReal avg = 0.0, avgcnt = 0.0;
      
      if (npoints == 0) {
        printf("[Reconstruct_P0] npoints = %d \n",npoints);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Insufficient points for reconstruction");
      }
      //printf("[Reconstruct_P0] npoints = %d \n",npoints);

      min_sep[0] = min_sep[1] = 1.0e32;
      closest_blue = closest_yellow = -1;
      for (ii=0; ii<npoints; ii++) {
        sep = (centroids[2*ii+0] - xd[0])*(centroids[2*ii+0] - xd[0]) + (centroids[2*ii+1] - xd[1])*(centroids[2*ii+1] - xd[1]);
        
        if (types[ii] == SLCELL_BLUE) {
          avg += values[ii];
          avgcnt += 1.0;
          if (sep < min_sep[0]) {
            min_sep[0]   = sep;
            closest_blue = ii;
          }
        }
        if (types[ii] == SLCELL_YELLOW) {
          if (sep < min_sep[1]) {
            min_sep[1] = sep;
            closest_yellow = ii;
          }
        }
      }

      closest = -1;
      //if (closest_yellow != -1) {
      //  closest = closest_yellow;
      //}
      if (closest_blue != -1) {
        closest = closest_blue;
      }
      if (closest == -1) {
        printf("[Reconstruct_P0] (%d %d == > %d) \n",ci,cj,cidx);
        for (ii=0; ii<9; ii++) {
          printf("  [neighbour %d] cellid %d : color %d\n",ii,cell_neighbours[ii],(int)stencil[ii]);
        }
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Failed to locate a BLUE or YELLOW cell for P0 reconstruction");
      }
      
      phi_H = values[closest];
      //printf("[Reconstruct_P0] (%d %d == > %d) avgval %1.4e cnt = %1.4e \n",ci,cj,cidx,avg,avgcnt);
      
      avg = avg / avgcnt;
      phi_H = avg;
    }

  }
  
  if (apply_limiter) {
    if (phi_H > phi_plus) {
      phi_M = phi_plus;
    } else if (phi_H < phi_minus) {
      phi_M = phi_minus;
    } else {
      phi_M = phi_H;
    }
  } else {
    phi_M = phi_H;
  }
  
  *_phi_M = phi_M;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FictDomainCSL_Reconstruct_CompactP0_ORANGE_OutFlow"
PetscErrorCode FictDomainCSL_Reconstruct_CompactP0_ORANGE_OutFlow(SLDMDA s,FVStorage fv,PetscReal xd[],PetscScalar phi_cell[],PetscReal *_phi_M)
{
  const PetscInt L = 48;
  PetscInt ci,cj,idx,cidx,cell_neighbour,ii;
  PetscReal phi_M;
  PetscInt candidates_i[48],candidates_j[48],cell_neighbours[48];
  SLCellType types[48];
  PetscInt npoints;
  PetscReal values[48];
  
  for (ii=0; ii<L; ii++) {
    candidates_i[ii] = -1;
    candidates_j[ii] = -1;
    types[ii] = SLCELL_INIT;
    cell_neighbours[ii] = -1;
    values[ii] = 0.0;
  }
  
  /* determine cell containing point */
  ci = (PetscInt)( (xd[0] - s->xrange[0])/s->dx );
  cj = (PetscInt)( (xd[1] - s->yrange[0])/s->dy );
  if (ci == s->mx) ci--;
  if (cj == s->my) cj--;
  
  cidx = DACellIdx(s,ci,cj);

  // ii = 0; ii < 8
  idx = 0;  candidates_i[idx] = (ci-1); candidates_j[idx] = (cj-1);
  idx = 1;  candidates_i[idx] = (ci  ); candidates_j[idx] = (cj-1);
  idx = 2;  candidates_i[idx] = (ci+1); candidates_j[idx] = (cj-1);

  idx = 3;  candidates_i[idx] = (ci-1); candidates_j[idx] = (cj);
  idx = 4;  candidates_i[idx] = (ci+1); candidates_j[idx] = (cj);

  idx = 5;  candidates_i[idx] = (ci-1); candidates_j[idx] = (cj+1);
  idx = 6;  candidates_i[idx] = (ci  ); candidates_j[idx] = (cj+1);
  idx = 7;  candidates_i[idx] = (ci+1); candidates_j[idx] = (cj+1);

  // ii = 8; ii < 24
  idx = 8;  candidates_i[idx] = (ci-2); candidates_j[idx] = (cj-2);
  idx = 9;  candidates_i[idx] = (ci-1); candidates_j[idx] = (cj-2);
  idx = 10; candidates_i[idx] = (ci  ); candidates_j[idx] = (cj-2);
  idx = 11; candidates_i[idx] = (ci+1); candidates_j[idx] = (cj-2);
  idx = 12; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj-2);

  idx = 13; candidates_i[idx] = (ci-2); candidates_j[idx] = (cj+2);
  idx = 14; candidates_i[idx] = (ci-1); candidates_j[idx] = (cj+2);
  idx = 15; candidates_i[idx] = (ci  ); candidates_j[idx] = (cj+2);
  idx = 16; candidates_i[idx] = (ci+1); candidates_j[idx] = (cj+2);
  idx = 17; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj+2);

  idx = 18; candidates_i[idx] = (ci-2); candidates_j[idx] = (cj-1);
  idx = 19; candidates_i[idx] = (ci-2); candidates_j[idx] = (cj  );
  idx = 20; candidates_i[idx] = (ci-2); candidates_j[idx] = (cj+1);

  idx = 21; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj-1);
  idx = 22; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj  );
  idx = 23; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj+1);

  // ii = 24; ii < 48
  idx = 24;  candidates_i[idx] = (ci-3); candidates_j[idx] = (cj-3);
  idx = 25;  candidates_i[idx] = (ci-2); candidates_j[idx] = (cj-3);
  idx = 26;  candidates_i[idx] = (ci-1); candidates_j[idx] = (cj-3);
  idx = 27; candidates_i[idx] = (ci  ); candidates_j[idx] = (cj-3);
  idx = 28; candidates_i[idx] = (ci+1); candidates_j[idx] = (cj-3);
  idx = 29; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj-3);
  idx = 30; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj-3);
  
  idx = 31; candidates_i[idx] = (ci-3); candidates_j[idx] = (cj+3);
  idx = 32; candidates_i[idx] = (ci-2); candidates_j[idx] = (cj+3);
  idx = 33; candidates_i[idx] = (ci-1); candidates_j[idx] = (cj+3);
  idx = 34; candidates_i[idx] = (ci  ); candidates_j[idx] = (cj+3);
  idx = 35; candidates_i[idx] = (ci+1); candidates_j[idx] = (cj+3);
  idx = 36; candidates_i[idx] = (ci+2); candidates_j[idx] = (cj+3);
  idx = 37; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj+3);
  
  idx = 38; candidates_i[idx] = (ci-3); candidates_j[idx] = (cj-2);
  idx = 39; candidates_i[idx] = (ci-3); candidates_j[idx] = (cj-1);
  idx = 40; candidates_i[idx] = (ci-3); candidates_j[idx] = (cj  );
  idx = 41; candidates_i[idx] = (ci-3); candidates_j[idx] = (cj+1);
  idx = 42; candidates_i[idx] = (ci-3); candidates_j[idx] = (cj+2);
  
  idx = 43; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj-2);
  idx = 44; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj-1);
  idx = 45; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj  );
  idx = 46; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj+1);
  idx = 47; candidates_i[idx] = (ci+3); candidates_j[idx] = (cj+2);
  
  
  /* get bounds from 8 neighbours + self */
  npoints = 0;
  for (ii=0; ii<8; ii++) {
    if (candidates_j[ii] < 0) continue;
    if (candidates_j[ii] > (s->my-1)) continue;
    if (candidates_i[ii] < 0) continue;
    if (candidates_i[ii] > (s->mx-1)) continue;

    cell_neighbour = candidates_i[ii] + candidates_j[ii] * s->mx;
    
    if (fv->cell_type[cell_neighbour] != SLCELL_BLUE) continue;
    
    types[ii]           = fv->cell_type[cell_neighbour];
    cell_neighbours[ii] = cell_neighbour;
    values[ii]          = phi_cell[cell_neighbour];
    npoints++;
  }
  
  /* consider larger region */
  if (npoints == 0) {
    for (ii=8; ii<24; ii++) {
      if (candidates_j[ii] < 0) continue;
      if (candidates_j[ii] > (s->my-1)) continue;
      if (candidates_i[ii] < 0) continue;
      if (candidates_i[ii] > (s->mx-1)) continue;
      
      cell_neighbour = candidates_i[ii] + candidates_j[ii] * s->mx;
      
      if (fv->cell_type[cell_neighbour] != SLCELL_BLUE) continue;
      
      types[ii]           = fv->cell_type[cell_neighbour];
      cell_neighbours[ii] = cell_neighbour;
      values[ii]          = phi_cell[cell_neighbour];
      npoints++;
    }
  }
  if (npoints == 0) {
    for (ii=24; ii<48; ii++) {
      if (candidates_j[ii] < 0) continue;
      if (candidates_j[ii] > (s->my-1)) continue;
      if (candidates_i[ii] < 0) continue;
      if (candidates_i[ii] > (s->mx-1)) continue;
      
      cell_neighbour = candidates_i[ii] + candidates_j[ii] * s->mx;
      
      if (fv->cell_type[cell_neighbour] != SLCELL_BLUE) continue;
      
      types[ii]           = fv->cell_type[cell_neighbour];
      cell_neighbours[ii] = cell_neighbour;
      values[ii]          = phi_cell[cell_neighbour];
      npoints++;
    }
  }
  
  if (npoints == 0) {
    printf("[Orange cell][Reconstruct_P0] npoints = %d \n",npoints);
    printf("[Reconstruct_P0] (%d %d == > %d) \n",ci,cj,cidx);
    for (ii=0; ii<L; ii++) {
      printf("  [neighbour %d] cellid %d : color %d\n",ii,cell_neighbours[ii],(int)types[ii]);
    }
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Failed to locate a BLUE cell for P0 reconstruction using %D nearest neighbours",L);
  }
  
  PetscReal avg = 0.0, avgcnt = 0.0;
  
  for (ii=0; ii<L; ii++) {
    if (cell_neighbours[ii] == -1) continue;
    
    avg += values[ii];
    avgcnt += 1.0;
  }
  
  avg = avg / avgcnt;
  phi_M = avg;
  
  *_phi_M = phi_M;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePointMask_DMDASubCell"
PetscErrorCode SLComputeDeparturePointMask_DMDASubCell(SLDMDA s,FVStorage fv,PetscReal time_interval,PetscInt nsteps,PetscReal xn[],PetscReal xd_end[],PetscBool *deppoint_left_domain,int log)
{
  PetscErrorCode ierr;
  PetscInt     k,cellid,cij[2],nintervals;
  PetscReal    dt,xp[2],xd[2],time;
  DPointStatus status;
  //FILE         *fp = NULL;
  
  /* initialize position to be outside domain */
  xd[0] = -100.0 * s->xrange[0];
  xd[1] = -100.0 * s->yrange[0];
  *deppoint_left_domain = PETSC_FALSE;
  
  //fp = fopen("characteristic.gp","w");
  
  xp[0] = xn[0];
  xp[1] = xn[1];
  status = DPOINT_INIT;
  
  ierr = SLPointLocation_DMDA(s,xn,&cellid,NULL);CHKERRQ(ierr);
  //fprintf(fp,"%+1.10e %+1.10e %d\n",xn[0],xn[1],cellid);
  if (log == 1) {
    printf("  [dep update] initial condition %+1.14e %+1.14e : cell %d : [is sub-cell %d] : cell_type %d \n",
           xn[0],xn[1],cellid,(int)fv->subcell[cellid]->valid,(int)fv->cell_type[cellid]);
  }
  
  nintervals = 0;
  time = 0.0;
  for (k=0; k<1000; k++) {
    if (log == 1) printf("  iteration [%d] \n",k);
    
    ierr = SLComputeCellCFLTimestep_DMDA(s,xp,&dt);CHKERRQ(ierr);
    dt = dt / 2.0;
    ierr = SLPointLocation_DMDA(s,xp,&cellid,cij);CHKERRQ(ierr);
    
    /* shorten dt by amount num subcells in x,y if this is a split cell */
    if (fv->subcell[cellid]->valid) {
      PetscReal dts;
      
      dts = dt / ((PetscReal)fv->subcell[cellid]->mx);
      if (fv->subcell[cellid]->my > fv->subcell[cellid]->mx) {
        dts = dt / ((PetscReal)fv->subcell[cellid]->my);
      }
      dts = dts / 2.0;
      dt = dts;
    }
    
    /* correct dt to ensure last step terminates at time = time_interval */
    if (time + dt > time_interval) {
      dt = time_interval - time;
    }
    if (log == 1) printf("  [%d] dt %1.4e target %1.4e\n",k,dt,time_interval);
    
    ierr = SLComputeDeparturePoint_RK2_DMDASubCell(s,fv,dt,xp,xd,&status);CHKERRQ(ierr);
    //ierr = SLComputeDeparturePoint_RK4_DMDASubCell(s,fv,dt,xp,xd,&status);CHKERRQ(ierr);
    ierr = SLPointLocation_DMDA(s,xd,&cellid,NULL);CHKERRQ(ierr);
    //fprintf(fp,"%+1.10e %+1.10e %d\n",xd[0],xd[1],cellid);
    if (log == 1) {
      printf("  [dep update] IN coor %+1.14e %+1.14e : OUT coor %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xp[0],xp[1],xd[0],xd[1],cellid,(int)fv->subcell[cellid]->valid);
      printf("  [dep update] coor %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xd[0],xd[1],cellid,(int)fv->subcell[cellid]->valid);
    }
    
    if ( (status == DPOINT_DEPARTED_DOMAIN) || (status == DPOINT_DEPARTED_VALID_MASK)){
      PetscInt sub_cellid;
      FVSubCellStorage subcell;
      PetscReal xbounds[2],ybounds[2];
      
      /* examine coordinates of the original point coordinate (xp), not the advected coordinate (xd) */
      ierr = SLPointLocation_DMDA(s,xp,&cellid,NULL);CHKERRQ(ierr);
      if (!fv->subcell[cellid]->valid) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Departure point left domain but did not end up in a sub-cell");

      *deppoint_left_domain = PETSC_TRUE;

      subcell = fv->subcell[cellid];
      
      xbounds[0] = s->xrange[0] + cij[0] * s->dx;
      xbounds[1] = xbounds[0] + s->dx;
      ybounds[0] = s->yrange[0] + cij[1] * s->dy;
      ybounds[1] = ybounds[0] + s->dy;
      
      ierr = SLPointLocation_SubCell(xbounds,ybounds,subcell->mx,subcell->my,xp,&sub_cellid,NULL);CHKERRQ(ierr);
      
      //if (subcell->state[sub_cellid] == 0)
      {
        PetscReal sep,min_sep;
        PetscInt closest_cell;
        PetscInt sc;
        
        //SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"State = 0: Depature point landed in a sub-cell which is outside the domain ** panic **");
        if (log == 1) printf("State = 0: Depature point located in a sub-cell which is outside the domain ** find nearest valid Dirichlet sub-cell centoid ** \n");
        
        min_sep = 1.0e32;
        closest_cell = -1;
        for (sc=0; sc<subcell->mx*subcell->my; sc++) {
          sep  = (xp[0] - subcell->centroid[2*sc])*(xp[0] - subcell->centroid[2*sc]);
          sep += (xp[1] - subcell->centroid[2*sc+1])*(xp[1] - subcell->centroid[2*sc+1]);
          if ( (subcell->state[sc] == 1) || (subcell->state[sc] == 0)) {
          //if (subcell->state[sc] == 1) {
            if (sep < min_sep) {
              min_sep = sep;
              closest_cell = sc;
            }
          }
        }
        if (closest_cell == -1) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Failed to locate an internal, Dirichlet sub-cell");
        
        xp[0] = subcell->centroid[ 2*closest_cell    ];
        xp[1] = subcell->centroid[ 2*closest_cell +1 ];
        break;
      }
      
      if (subcell->state[sub_cellid] == 2) {
      }
      /* If current coordinate on the characteristic is inside a sub-cell marked as Dirichlet - terminate characteristic integration */
      if (subcell->state[sub_cellid] == 1) {
      }
    }
    
    /* If xp is valid (inside domain), accept the step and update position and time */
    xp[0] = xd[0];
    xp[1] = xd[1];
    time = time + dt;
    nintervals++;
    //printf("  updating coordinate keeping %+1.14e %+1.14e\n",xp[0],xp[1]);
    
    if (time >= time_interval) break;
  }
  
  xd_end[0] = xp[0];
  xd_end[1] = xp[1];
  //fclose(fp);
  
  //printf("nintervals = %d\n",nintervals);
  if (log == 1) {
    printf("  final coordinate  %+1.14e %+1.14e [left domain? %d]\n",xd_end[0],xd_end[1],(int)(*deppoint_left_domain));
  }
  /* check that if point exited domain that it landed in a sub-divided cell on the last valid time intgration step */
  if (*deppoint_left_domain) {
    ierr = SLPointLocation_DMDA(s,xd_end,&cellid,NULL);CHKERRQ(ierr);
    
    if (!fv->subcell[cellid]->valid) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Departure point left domain but did not end up in a sub-cell");
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SLComputeDeparturePointMask_DMDABCellType"
PetscErrorCode SLComputeDeparturePointMask_DMDABCellType(SLDMDA s,FVStorage fv,PetscReal xn[],PetscBool *deppoint_left_domain)
{
  PetscErrorCode ierr;
  PetscInt       k,cellid,cij[2];
  PetscReal      dt,xp[2],xd[2],time;
  DPointStatus   status;
  PetscInt       log = 0;
  
  /* initialize position to be outside domain */
  xd[0] = -100.0 * s->xrange[0];
  xd[1] = -100.0 * s->yrange[0];
  *deppoint_left_domain = PETSC_FALSE;
  
  xp[0] = xn[0];
  xp[1] = xn[1];
  status = DPOINT_INIT;
  
  time = 0.0;
  for (k=0; k<100000; k++) {
    if (log == 1) printf("  iteration [%d] \n",k);
    
    ierr = SLPointLocation_DMDA(s,xp,&cellid,cij);CHKERRQ(ierr);
    
    /* add special early abort if the velocity at xp[] is approximately zero */
    {
      PetscReal velocity[2];
      
      ierr = SLComputeVelocity_DMDA(s,xp,velocity);CHKERRQ(ierr); /* t = t_k */
      if (PetscSqrtReal(velocity[0]*velocity[0]+velocity[1]*velocity[1]) < SL_MIN_VELOCITY) {
        *deppoint_left_domain = PETSC_FALSE;
        break;
      }
    }
    
    ierr = SLComputeCellCFLTimestep_DMDA(s,xp,&dt);CHKERRQ(ierr);
    dt = dt / PETSC_PI; /* choose a non-integer number to avoid corner cases where a point might land exactly on the vertex of a blue cell */
  
    ierr = SLComputeDeparturePoint_RK4_DMDASubCell(s,fv,dt,xp,xd,&status);CHKERRQ(ierr);
    ierr = SLPointLocation_DMDA(s,xd,&cellid,NULL);CHKERRQ(ierr);

    if (log == 1) {
      printf("  [dep update] IN coor %+1.14e %+1.14e : OUT coor %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xp[0],xp[1],xd[0],xd[1],cellid,(int)fv->subcell[cellid]->valid);
      printf("  [dep update] coor %+1.14e %+1.14e : cell %d : [is sub-cell %d]\n",xd[0],xd[1],cellid,(int)fv->subcell[cellid]->valid);
    }
    
    if ( (status == DPOINT_DEPARTED_DOMAIN) || (status == DPOINT_DEPARTED_VALID_MASK)){
      *deppoint_left_domain = PETSC_TRUE;
      break;
    }
    if (fv->cell_type[cellid] == SLCELL_BLUE) {
      break;
    }
    
    /* If xp is valid (inside yellow or orange cell), accept the step and update position and time */
    xp[0] = xd[0];
    xp[1] = xd[1];
    time = time + dt;
  }
  if (log == 1) {
    printf("  final coordinate  %+1.14e %+1.14e [left domain? %d]\n",xp[0],xp[1],(int)(*deppoint_left_domain));
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify(DMSemiLagrangian dasl)
{
  PetscErrorCode ierr;
  PetscInt k;
  PetscReal xn[2],rn[2];
  Vec coor;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscInt   L_cell;
  SLDMDA    sl;
  FVStorage fv;
  PetscLogDouble t0,t1;
  
  sl = dasl->dactx;
  fv = dasl->fv;
  
  L_cell = sl->mx * sl->my;
  
  /* ======= Phase 1 ======= */
  /* compute and store departure points */
  ierr = DMGetCoordinates(fv->dmcv,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;
  
  srand(0);
  PetscTime(&t0);
  for (k=0; k<L_cell; k++) {
    PetscBool dep_left;

    fv->bcell_type[k] = SLBCELL_NONE;
    
    // Update those with labels SLCELL_BLUE or SLCELL_YELLOW
    if (fv->cell_type[k] == SLCELL_BLUE) { continue; }
    if (fv->cell_type[k] == SLCELL_GREY) { continue; }

    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];

    rn[0] = rand()/((PetscReal)RAND_MAX);
    rn[1] = rand()/((PetscReal)RAND_MAX);

    /* perform a tiny random shift of the centoid coords 
       to avoid corner cases where a point might land exactly on the vertex of a blue cell
       and be thus falsely classified a boundary as an outflow centroid
    */
    xn[0] = xn[0] + 1.0e-4 * (2.0 * rn[0] -1.0) * sl->dx;
    xn[1] = xn[1] + 1.0e-4 * (2.0 * rn[1] -1.0) * sl->dy;

    /* determine departure point */
    dep_left = PETSC_FALSE;
    ierr = SLComputeDeparturePointMask_DMDABCellType(sl,fv,xn,&dep_left);CHKERRQ(ierr);

    /* ======= Phase 2 ======= */
    /*
     Classify as inflow or outflow
    */
    if (dep_left) {
      fv->bcell_type[k] = SLBCELL_INFLOW;
    } else {
      fv->bcell_type[k] = SLBCELL_OUTFLOW;
    }

  }
  PetscTime(&t1);
  
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_vX3_BLUE"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3_BLUE(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt k;
  PetscReal xn[2],xd[2];
  Vec coor;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscInt L_cell;
  static PetscInt fcnt = 0;
  FILE *fp_traj = NULL;
  FILE *fp_dep_cell = NULL;
  //PetscInt debug = 2;//dasl->debug;
  PetscInt debug = dasl->debug;
  SLDMDA    sl;
  FVStorage fv;
  PetscReal *x_departure,*phi_departure;
  PetscBool *inflow_characteristic;
  PetscLogDouble t0,t1,t2,t3;
  PetscScalar *LA_phi;
  const PetscScalar *LA_phi_dep;
  //static PetscBool beenhere = PETSC_FALSE;
  
  sl = dasl->dactx;
  fv = dasl->fv;
  
  if (debug > 1) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sl_traj-%D.gp",fcnt);
    fp_traj = fopen(name,"w");
    fprintf(fp_traj,"# frame %d \n",fcnt);
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sl_depcell-%D.gp",fcnt);
    fp_dep_cell = fopen(name,"w");
    fprintf(fp_dep_cell,"# frame %d \n",fcnt);
  }
  
  L_cell = sl->mx * sl->my;
  
  /* ======= Phase 1 ======= */
  /* compute and store departure points */
  ierr = DMGetCoordinates(fv->dmcv,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;
  
  ierr = PetscMalloc1(L_cell*2,&x_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L_cell,&inflow_characteristic);CHKERRQ(ierr);
  ierr = PetscMalloc1(L_cell,&phi_departure);CHKERRQ(ierr);
  ierr = PetscMemzero(phi_departure,L_cell*sizeof(PetscReal));CHKERRQ(ierr);

  /* check pointer is valid */
  if (dasl->store_departure_points) {
    if (!dasl->dpoint) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must call DMSemiLagrangianSetStoreDeparturePoints(,dmsl,PETSC_TRUE); first");
    ierr = DPointReset(dasl->n_departure_points_allocated,OPOINT_CENTROID,dasl->dpoint);CHKERRQ(ierr);
    dasl->n_departure_points = 0;
  }

  PetscTime(&t0);
  for (k=0; k<L_cell; k++) {
    PetscBool dep_left;
    int log;
    
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];

    x_departure[2*k+0] = xn[0];
    x_departure[2*k+1] = xn[1];
    inflow_characteristic[k] = PETSC_FALSE;
    
    // Update those with labels SLCELL_BLUE or SLCELL_YELLOW
    //if (fv->cell_type[k] == SLCELL_BLUE) { continue; }
    //if (fv->cell_type[k] == SLCELL_ORANGE) { continue; }
    if (fv->cell_type[k] == SLCELL_GREY) { continue; }
    
    /* determine departure point */
    log = 0;
    //if (k == 123621) { log = 1; }
    if (log == 1) {
      printf("****========== characteristic for cell centroid %d ==========****\n",k);
    }
    
    dep_left = PETSC_FALSE;
    ierr = SLComputeDeparturePointMask_DMDASubCell(sl,fv,dt,1,xn,xd,&dep_left,log);CHKERRQ(ierr);
    
    /* store xd,inflow status */
    x_departure[2*k+0] = xd[0];
    x_departure[2*k+1] = xd[1];
    inflow_characteristic[k] = dep_left;
    
    /* store departure points (if requested) */
    if (dasl->store_departure_points) {
      PetscInt index = dasl->n_departure_points;
      
      dasl->dpoint[index].origin = k;
      dasl->dpoint[index].departure_coor[0] = xd[0];
      dasl->dpoint[index].departure_coor[1] = xd[1];
      switch (dep_left) {
        case PETSC_TRUE:
          dasl->dpoint[index].type = SLLOC_OUTSIDE;
          break;
        case PETSC_FALSE:
          dasl->dpoint[index].type = SLLOC_INSIDE;
          break;
      }
      dasl->n_departure_points++;
    }
    
  }
  PetscTime(&t1);
  
  /* ======= Phase 2 ======= */
  /*
   For all characteristics which exited the domain (inflowing characteristics),
   set the field value using the provided sub-cell values for the field.
   We use closest point interpolation.
  */

  ierr = VecGetArrayRead(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  for (k=0; k<L_cell; k++) {
    PetscReal coor[2];
    PetscInt cellid,cij[2],sub_cellid;
    PetscReal xbounds[2],ybounds[2],phi_H;
    FVSubCellStorage subcell;
    
    if (inflow_characteristic[k] == PETSC_FALSE) continue;

    coor[0] = x_departure[2*k+0];
    coor[1] = x_departure[2*k+1];
    
    ierr = SLPointLocation_DMDA(sl,coor,&cellid,cij);CHKERRQ(ierr);

    subcell = fv->subcell[cellid];
    
    xbounds[0] = sl->xrange[0] + cij[0] * sl->dx;
    xbounds[1] = xbounds[0] + sl->dx;
    ybounds[0] = sl->yrange[0] + cij[1] * sl->dy;
    ybounds[1] = ybounds[0] + sl->dy;

    ierr = SLPointLocation_SubCell(xbounds,ybounds,subcell->mx,subcell->my,coor,&sub_cellid,NULL);CHKERRQ(ierr);
    phi_H = subcell->phi[ sub_cellid ];
    
    // Perform C-Spline within the sub-cell
    //ierr = DMDA_InterpolateAtPoint_CSpline(CTYPE_CENTROID,nxny_sub,dx_sub,xbounds,ybounds,coor,subcell->phi,&phi_H);CHKERRQ(ierr);
    // Perform P0 over a set of nearby elements
    //ierr = FictDomainCSL_Reconstruct_CompactP0_ORANGE_OutFlow(sl,fv,coor,(PetscScalar*)LA_phi_dep,&phi_H);CHKERRQ(ierr);
    //printf("* [p0] %1.4e : [cspine] %1.4e \n",subcell->phi[ sub_cellid ],phi_H);
    
    phi_departure[k] = phi_H;
  }
  PetscTime(&t2);
  ierr = VecRestoreArrayRead(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  
  
  ierr = VecGetArray(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  for (k=0; k<L_cell; k++) {
    PetscReal phi_interp,coor[2];
    PetscInt cellid;

#if 0
    if (inflow_characteristic[k] == PETSC_TRUE) {
      LA_phi[k] = phi_departure[k];
    } else {
      
      coor[0] = x_departure[2*k+0];
      coor[1] = x_departure[2*k+1];

      ierr = SLPointLocation_DMDA(sl,coor,&cellid,NULL);CHKERRQ(ierr);

      phi_interp = LA_phi_dep[k];

  #if 0
      if (inflow_characteristic[ cellid ] == PETSC_TRUE) {
        
        ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,2,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        
      } else {
      
        if (fv->cell_type[cellid] == SLCELL_BLUE) {
          //ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
          ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,1,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        } else if (fv->cell_type[cellid] == SLCELL_YELLOW) {
          //phi_interp = LA_phi_dep[k];
          printf("======== which cell %d\n",k);
          ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,3,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);

        } else if (fv->cell_type[cellid] == SLCELL_ORANGE) {
          ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,1,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        }

      }
  #endif

      printf("======== cell %d (color %d) ---> dep point maps to cell %d (color %d) : departed %d \n",k,fv->cell_type[k],cellid,fv->cell_type[cellid],(int)inflow_characteristic[k]);

      if (inflow_characteristic[ cellid ] == PETSC_TRUE) {
        printf(" inflow[%d] \n",cellid);
        printf("======== [inflow] which cell %d ---> dep point maps to cell %d : interp %+1.8e\n",k,cellid,phi_interp);
        ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,2,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        
      } else {
        
        if (fv->cell_type[k] == SLCELL_YELLOW) {
          if (k == cellid) { phi_interp = LA_phi_dep[k]; }
          else {
            ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,3,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
          //printf("======== [yellow] which cell %d ---> dep point maps to cell %d : interp %+1.8e\n",k,cellid,phi_interp);
          }
        } else if (fv->cell_type[k] == SLCELL_ORANGE) {
          if (k == cellid) { phi_interp = LA_phi_dep[k]; }
          else {
            ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,3,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
          //printf("======== [orange] which cell %d ---> dep point maps to cell %d : interp %+1.8e\n",k,cellid,phi_interp);
          }
        } else {
        
          if (fv->cell_type[cellid] == SLCELL_BLUE) {
            //ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
            ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,1,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
          } else if (fv->cell_type[cellid] == SLCELL_YELLOW) {
            ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,1,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
            
          } else if (fv->cell_type[cellid] == SLCELL_ORANGE) {
            ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,1,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
          }

        }
      }
      
      phi_departure[k] = phi_interp;
      // copy values
      LA_phi[k] = phi_interp;
    }
#endif
    
#if 1
    
    phi_interp = LA_phi_dep[k];

    coor[0] = x_departure[2*k+0];
    coor[1] = x_departure[2*k+1];
    
    ierr = SLPointLocation_DMDA(sl,coor,&cellid,NULL);CHKERRQ(ierr);
    
    if (inflow_characteristic[k] == PETSC_FALSE) {

      //if (k == 32139) printf("======== [characteristic in domain] which cell %d ---> dep point maps to cell %d : interp %+1.8e\n",k,cellid,phi_interp);

      if (fv->bcell_type[cellid] == SLBCELL_INFLOW) {
        // closest point using sub-cell data //
        ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,2,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
      }
      else if (fv->bcell_type[k] == SLBCELL_OUTFLOW) {
        if (fv->cell_type[cellid] != SLCELL_BLUE) {
          ierr = FictDomainCSL_Reconstruct_CompactP0_ORANGE_OutFlow(sl,fv,coor,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        } else {
          // average blue neighbour values
          ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,3,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        }
      } else {
        if (fv->cell_type[cellid] != SLCELL_GREY) {
          //if (k == 32139) printf("Doing P1 for if (k == 32139) \n");
          ierr = FictDomainCSL_InterpolateAtPointQuasiMonotone(sl,fv,coor,1,(PetscScalar*)LA_phi_dep,&phi_interp);CHKERRQ(ierr);
        }
      }
      phi_departure[k] = phi_interp;
    } else {
      phi_interp = phi_departure[k];
    }

    // copy values
    LA_phi[k] = phi_interp;
////
#endif
    
  }
  ierr = VecRestoreArrayRead(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  ierr = VecRestoreArray(phi,&LA_phi);CHKERRQ(ierr);
  

  
  /* departure coordinates */
  if (debug > 1) {
    ierr = VecGetArrayRead(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
    for (k=0; k<L_cell; k++) {
      fprintf(fp_traj,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e  %1.4e\n",
              LA_coor[2*k+0],LA_coor[2*k+1],LA_phi_dep[k], x_departure[2*k+0],x_departure[2*k+1],phi_departure[k],(double)inflow_characteristic[k]);
    }
    ierr = VecRestoreArrayRead(dasl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);
  }
  PetscTime(&t3);
  
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  if (fp_traj) { fclose(fp_traj); }
  if (fp_dep_cell) { fclose(fp_dep_cell); }
  fcnt++;
  //beenhere = PETSC_TRUE;
  
  if (debug > 0) {
    PetscPrintf(PETSC_COMM_SELF,"  [FictD-CSL] : construct departure point  %1.4e (sec)\n",t1-t0);
    PetscPrintf(PETSC_COMM_SELF,"  [FictD-CSL] : reconstruction <Dirichlet> %1.4e (sec)\n",t2-t1);
    PetscPrintf(PETSC_COMM_SELF,"  [FictD-CSL] : reconstruction <interior>  %1.4e (sec)\n",t3-t2);
  }
  
  ierr = PetscFree(x_departure);CHKERRQ(ierr);
  ierr = PetscFree(inflow_characteristic);CHKERRQ(ierr);
  ierr = PetscFree(phi_departure);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_vX3_DIRICHLET"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3_DIRICHLET(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt k,i;
  PetscReal xn[2];
  Vec coor;
  const PetscScalar *LA_coor;
  PetscInt L_cell;
  SLDMDA    sl;
  FVStorage fv;
  PetscScalar *LA_phi;
  
  sl = dasl->dactx;
  fv = dasl->fv;
  
  L_cell = sl->mx * sl->my;
  
  ierr = DMGetCoordinates(fv->dmcv,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetArray(phi,&LA_phi);CHKERRQ(ierr);
  for (k=0; k<L_cell; k++) {
    PetscInt cellid,cij[2];
    FVSubCellStorage subcell;
    PetscInt closest_cell;
    PetscReal sep,min_sep;
    
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    if (fv->cell_type[k] == SLCELL_BLUE) { continue; }
    if (fv->cell_type[k] == SLCELL_ORANGE) { continue; }
    if (fv->cell_type[k] == SLCELL_GREY) { continue; }

    ierr = SLPointLocation_DMDA(sl,xn,&cellid,cij);CHKERRQ(ierr);
    
    subcell = fv->subcell[cellid];
    
    min_sep = 1.0e32;
    closest_cell = -1;
    for (i=0; i<subcell->mx*subcell->my; i++) {
      if (subcell->state[i] == 1) {
        sep  = (xn[0] - subcell->centroid[2*i])  *(xn[0] - subcell->centroid[2*i]);
        sep += (xn[1] - subcell->centroid[2*i+1])*(xn[1] - subcell->centroid[2*i+1]);
        if (sep < min_sep) {
          min_sep = sep;
          closest_cell = i;
        }
      }
    }
    //printf("subcell[%d]->valid = %d: closest cell %d\n",cellid,(int)subcell->valid,closest_cell);
    if (closest_cell == -1) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Failed to locate an internal, Dirichlet sub-cell");
    
    LA_phi[k] = subcell->phi[ closest_cell ];
  }
  ierr = VecRestoreArray(phi,&LA_phi);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMFVConservativeSemiLagrangianUpdate_vX3"
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3(DMSemiLagrangian dasl,PetscReal time,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscLogDouble t0,t1,t2,t3,t4,t5,t6;
  PetscInt debug = dasl->debug;
  
  ierr = VecZeroEntries(phi);CHKERRQ(ierr);

  /* asseme fictitous domain does not change between repeated calls - if it does, setup should be called again */
  /* assme velocity will change, thus redefine it on the fictitious domain */
  PetscTime(&t0);
  ierr = DMDASemiLagrangian_FVConservative_RestrictVelocity(dasl);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = DMDASemiLagrangian_FVConservative_ReconstructVelocity(dasl);CHKERRQ(ierr);
  PetscTime(&t2);
  ierr = DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify(dasl);CHKERRQ(ierr);
  PetscTime(&t3);
  ierr = DMDASemiLagrangian_FVConservative_DirichletEvaluate(dasl,time);CHKERRQ(ierr);
  PetscTime(&t4);
  
  /* enforce Dirichlet conditions into the input vector phi */
  ierr = DMFVConservativeSemiLagrangianUpdate_vX3_DIRICHLET(dasl,dt,phi);CHKERRQ(ierr);
  //ierr = DMFVConservativeSemiLagrangianUpdate_vX3_DIRICHLET(dasl,dt,dasl->phi_dep);CHKERRQ(ierr);
  PetscTime(&t5);

  ierr = DMFVConservativeSemiLagrangianUpdate_vX3_BLUE(dasl,dt,phi);CHKERRQ(ierr);
  PetscTime(&t6);
  

  if (debug > 0) {
    PetscPrintf(PETSC_COMM_WORLD,"  [FictD-CSL] : DMDASemiLagrangian_FVConservative_RestrictVelocity      %1.2e (sec)\n",t1-t0);
    PetscPrintf(PETSC_COMM_WORLD,"  [FictD-CSL] : DMDASemiLagrangian_FVConservative_ReconstructVelocity   %1.2e (sec)\n",t2-t1);
    PetscPrintf(PETSC_COMM_WORLD,"  [FictD-CSL] : DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify  %1.2e (sec)\n",t3-t2);
    PetscPrintf(PETSC_COMM_WORLD,"  [FictD-CSL] : DMDASemiLagrangian_FVConservative_DirichletEvaluate     %1.2e (sec)\n",t4-t3);
    PetscPrintf(PETSC_COMM_WORLD,"  [FictD-CSL] : DMFVConservativeSemiLagrangianUpdate_vX3_DIRICHLET      %1.2e (sec)\n",t5-t4);
    PetscPrintf(PETSC_COMM_WORLD,"  [FictD-CSL] : DMFVConservativeSemiLagrangianUpdate_vX3_BLUE           %1.2e (sec)\n",t6-t5);
  }
   
  ierr = VecCopy(phi,dasl->phi_dep);CHKERRQ(ierr); /* phi_departure <--  phi */
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__DMFVConservativeSemiLagrangianUpdate_vX3a"
PetscErrorCode __DMFVConservativeSemiLagrangianUpdate_vX3a(DMSemiLagrangian dasl,PetscReal dt,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt k;
  PetscReal xn[2],xd[2];
  Vec coor;
  const PetscScalar *LA_velocity;
  const PetscScalar *LA_coor;
  PetscInt L_vert;
  static PetscInt fcnt = 0;
  FILE *fp_traj = NULL;
  FILE *fp_dep_cell = NULL;
  PetscInt debug = 2;//dasl->debug;
  SLDMDA    sl;
  FVStorage fv;
  PetscReal *x_departure,*phi_departure;
  PetscBool *inflow_characteristic;
  PetscLogDouble t0,t1;
  const PetscScalar *LA_phi;
  //static PetscBool beenhere = PETSC_FALSE;

  sl = dasl->dactx;
  fv = dasl->fv;
  
  if (debug > 1) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sl_traj-%D.gp",fcnt);
    fp_traj = fopen(name,"w");
    fprintf(fp_traj,"# frame %d \n",fcnt);
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"sl_depcell-%D.gp",fcnt);
    fp_dep_cell = fopen(name,"w");
    fprintf(fp_dep_cell,"# frame %d \n",fcnt);
  }
  
  L_vert = sl->nx * sl->ny;
  
  /* ======= Phase 1 ======= */
  /* compute and store departure points */
  ierr = DMGetCoordinates(dasl->dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  sl->LA_vel = (PetscScalar*)LA_velocity;
  
  ierr = PetscMalloc1(L_vert*2,&x_departure);CHKERRQ(ierr);
  ierr = PetscMalloc1(L_vert,&inflow_characteristic);CHKERRQ(ierr);
  ierr = PetscMalloc1(L_vert,&phi_departure);CHKERRQ(ierr);
  ierr = PetscMemzero(phi_departure,L_vert*sizeof(PetscReal));CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (k=0; k<L_vert; k++) {
    PetscBool dep_left;
    
    /* determine departure point */
    xn[0] = LA_coor[2*k+0];
    xn[1] = LA_coor[2*k+1];
    
    if (fv->vertex_type[k] == SLVERTEX_DEACTIVE) {
      /* do not advect this characteristic */
      x_departure[2*k+0] = xn[0];
      x_departure[2*k+1] = xn[1];
      inflow_characteristic[k] = PETSC_FALSE;
      continue;
    }
    
    dep_left = PETSC_FALSE;
    ierr = SLComputeDeparturePointMask_DMDASubCell(sl,fv,dt,1,xn,xd,&dep_left,0);CHKERRQ(ierr);
    
    /* store xd,inflow status */
    x_departure[2*k+0] = xd[0];
    x_departure[2*k+1] = xd[1];
    inflow_characteristic[k] = dep_left;
  }
  PetscTime(&t1);
  
  /* ======= Phase 2 ======= */
  /*
   For all characteristics which exited the domain (inflowing characteristics),
   set the field value using the provided sub-cell values for the field.
   We use closest point interpolation.
   */
  
  for (k=0; k<L_vert; k++) {
    PetscReal coor[2];
    PetscInt cellid,cij[2],sub_cellid;
    PetscReal xbounds[2],ybounds[2];
    FVSubCellStorage subcell;
    
    if (inflow_characteristic[k] == PETSC_FALSE) continue;
    
    coor[0] = x_departure[2*k+0];
    coor[1] = x_departure[2*k+1];
    
    ierr = SLPointLocation_DMDA(sl,coor,&cellid,cij);CHKERRQ(ierr);
    
    subcell = fv->subcell[cellid];
    
    xbounds[0] = sl->xrange[0] + cij[0] * sl->dx;
    xbounds[1] = xbounds[0] + sl->dx;
    ybounds[0] = sl->yrange[0] + cij[1] * sl->dy;
    ybounds[1] = ybounds[0] + sl->dy;
    
    ierr = SLPointLocation_SubCell(xbounds,ybounds,subcell->mx,subcell->my,coor,&sub_cellid,NULL);CHKERRQ(ierr);
    phi_departure[k] = subcell->phi[ sub_cellid ];
  }
  
  ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  for (k=0; k<L_vert; k++) {
    PetscReal phi_interp,coor[2];
    
    if (inflow_characteristic[k] == PETSC_TRUE) continue;
    
    coor[0] = x_departure[2*k+0];
    coor[1] = x_departure[2*k+1];
    
    ierr = CellBased_CSplineAtPointQuasiMonotone_DMDA(sl,coor,(PetscScalar*)LA_phi,&phi_interp);CHKERRQ(ierr);
    
    phi_departure[k] = phi_interp;
  }
  ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  
  /* departure coordinates */
  if (debug > 1) {
    ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
    for (k=0; k<L_vert; k++) {
      fprintf(fp_traj,"%1.4e %1.4e %1.4e   %1.4e %1.4e %1.4e  %1.4e\n",
              LA_coor[2*k+0],LA_coor[2*k+1],LA_phi[k], x_departure[2*k+0],x_departure[2*k+1],phi_departure[k],(double)inflow_characteristic[k]);
    }
    ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  }
  
  /* departure cells */
  if (debug > 1) {
    int ci,cj,verts[4];
    
    for (cj=0; cj<sl->my; cj++) {
      for (ci=0; ci<sl->mx; ci++) {
        
        verts[0] = DANodeIdx(sl,ci,  cj);
        verts[1] = DANodeIdx(sl,ci+1,cj);
        verts[2] = DANodeIdx(sl,ci+1,cj+1);
        verts[3] = DANodeIdx(sl,ci,  cj+1);
        
        for (k=0; k<4; k++) {
          fprintf(fp_dep_cell,"%1.4e %1.4e \n",x_departure[2*verts[k]+0],x_departure[2*verts[k]+1]);
        }
        fprintf(fp_dep_cell,"%1.4e %1.4e \n\n",x_departure[2*verts[0]+0],x_departure[2*verts[0]+1]);
      }
    }
  }
  
  ierr = VecRestoreArrayRead(dasl->velocity,&LA_velocity);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  /* Update */
  ierr = VecCopy(dasl->phi_dep,phi);CHKERRQ(ierr); /* phi <-- phi_departure */
  
  if (fp_traj) { fclose(fp_traj); }
  if (fp_dep_cell) { fclose(fp_dep_cell); }
  fcnt++;
  //beenhere = PETSC_TRUE;
  
  if (debug > 0) {
    PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: construct departure point  %1.4e (sec)\n",t1-t0);
    //PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: reconstruct nodal avg      %1.4e (sec)\n",t1-t0);
    //PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: reonstruct cspline-monotone %1.4e (sec)\n",t2-t1);
    //PetscPrintf(PETSC_COMM_SELF,"  FVConservativeSL: compute cell average       %1.4e (sec)\n",t4-t3);
  }
  
  ierr = PetscFree(x_departure);CHKERRQ(ierr);
  ierr = PetscFree(inflow_characteristic);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

