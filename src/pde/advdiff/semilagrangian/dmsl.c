

#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <dmsl.h>

PetscErrorCode DMDASemiLagrangianDestroy_FVConservative(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_SubCellsSetDefault(DMSemiLagrangian sl,PetscInt mxs,PetscInt mys);
PetscErrorCode DMDASemiLagrangian_FVConservative_SetDirichletTagDefault(DMSemiLagrangian sl);
PetscErrorCode GetSubCellNeighbourStatePatch(SLDMDA dactx,FVStorage fv,FVSubCellStorage subcell,PetscInt list[]);
PetscErrorCode DMDASemiLagrangian_FVConservative_DirichletEvaluate(DMSemiLagrangian sl,PetscReal time);
PetscErrorCode DMDASemiLagrangian_FVConservative_RestrictVelocity(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_ReconstructVelocity(DMSemiLagrangian sl);
PetscErrorCode DMDASemiLagrangian_FVConservative_SetUpBoundary(DMSemiLagrangian sl);
PetscErrorCode DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify(DMSemiLagrangian dasl);

#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianDestroy"
PetscErrorCode DMSemiLagrangianDestroy(DMSemiLagrangian *_sl)
{
  PetscErrorCode ierr;
  DMSemiLagrangian sl;
  
  if (!_sl) PetscFunctionReturn(0);
  
  sl = *_sl;
  ierr = DMDestroy(&sl->dm);CHKERRQ(ierr);
  ierr = VecDestroy(&sl->phi_dep);CHKERRQ(ierr);
  ierr = VecDestroy(&sl->velocity);CHKERRQ(ierr);
  /* release internal memory for DA implementation */
  if (sl->dactx) {
    ierr = PetscFree(sl->dactx);CHKERRQ(ierr);
    sl->dactx = NULL;
  }
  if (sl->fv) {
    ierr = DMDASemiLagrangianDestroy_FVConservative(sl);CHKERRQ(ierr);
  }
  if (sl->dpoint) {
    ierr = DPointDestroy(&sl->dpoint);CHKERRQ(ierr);
  }
  ierr = PetscFree(sl);CHKERRQ(ierr);
  *_sl = NULL;
  PetscFunctionReturn(0);
}

PetscErrorCode DMCreateInterpolation_DMDA_DMTET_SEQ(DM dmF,DM dmT,Mat *mat,Vec *vec);

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianCreate"
PetscErrorCode DMDASemiLagrangianCreate(
                MPI_Comm comm,PetscInt M,PetscInt N,PetscInt dof,
                PetscReal xr[],PetscReal yr[],DMSemiLagrangian *_sl)
{
  DMSemiLagrangian sl;
  SLDMDA dactx;
  DM da;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(comm,PETSC_ERR_SUP,"Currently this is only supported for comm.size = 1");
  ierr = PetscMalloc1(1,&sl);CHKERRQ(ierr);
  ierr = PetscMemzero(sl,sizeof(SLDMDA));CHKERRQ(ierr);
  sl->debug = 0;
  sl->setup = PETSC_FALSE;
  
  ierr = DMDACreate2d(comm,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,M,N,PETSC_DECIDE,PETSC_DECIDE,dof,1,NULL,NULL,&da);CHKERRQ(ierr);
  
  ierr = DMDASetUniformCoordinates(da,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  da->ops->createinterpolation = DMCreateInterpolation_DMDA_DMTET_SEQ;
  
  sl->dm = da;
  
  ierr = DMCreateGlobalVector(da,&sl->phi_dep);CHKERRQ(ierr);
  /*
  PetscInt mv,Mv,bs;
  ierr = VecGetSize(sl->phi_last,&Mv);CHKERRQ(ierr);
  ierr = VecGetLocalSize(sl->phi_last,&mv);CHKERRQ(ierr);
  ierr = VecGetBlockSize(sl->phi_last,&bs);CHKERRQ(ierr);
  ierr = VecCreate(comm,&sl->velocity);CHKERRQ(ierr);
  ierr = VecSetSizes(sl->velocity,2*(mv/bs),2*(Mv/bs));
  ierr = VecSetBlockSize(sl->velocity,2);CHKERRQ(ierr);
  ierr = VecSetFromOptions(sl->velocity);CHKERRQ(ierr);
  */
  {
    DM cdm;
    
    ierr = DMGetCoordinateDM(da,&cdm);CHKERRQ(ierr);
    ierr = DMDASetFieldName(cdm,0,"x");CHKERRQ(ierr);
    ierr = DMDASetFieldName(cdm,1,"y");CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(cdm,&sl->velocity);CHKERRQ(ierr);
  }

  sl->trajectory_disable_substep = PETSC_FALSE;
  PetscOptionsGetBool(NULL,NULL,"-sl_trajectory_disable_substep",&sl->trajectory_disable_substep,NULL);
  sl->trajectory_cfl = 1.0;
  PetscOptionsGetReal(NULL,NULL,"-sl_trajectory_cfl",&sl->trajectory_cfl,NULL);
  
  ierr = PetscMalloc1(1,&dactx);CHKERRQ(ierr);
  dactx->nx = M;
  dactx->ny = N;
  dactx->mx = M-1;
  dactx->my = N-1;
  dactx->nnodes = M * N;
  dactx->ncells = (M-1) * (N-1);
  dactx->xrange[0] = xr[0];
  dactx->xrange[1] = xr[1];
  dactx->yrange[0] = yr[0];
  dactx->yrange[1] = yr[1];
  dactx->dx = (xr[1]-xr[0])/((PetscReal)dactx->mx);
  dactx->dy = (yr[1]-yr[0])/((PetscReal)dactx->my);
  dactx->interpolation_type = DASLInterp_QMonotone;
  dactx->LA_vel = NULL;
  sl->dactx = dactx;
  
  sl->sl_type = SLTYPE_CLASSIC;
  sl->store_departure_points = PETSC_FALSE;
  sl->dpoint = NULL;
  
  *_sl = sl;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianCreateGlobalVector"
PetscErrorCode DMDASemiLagrangianCreateGlobalVector(DMSemiLagrangian sl,Vec *X)
{
  PetscErrorCode ierr;
  
  switch (sl->sl_type) {
    case SLTYPE_CLASSIC:
      ierr = DMCreateGlobalVector(sl->dm,X);CHKERRQ(ierr);
      break;
    case SLTYPE_FVCONSERVATIVE:
      ierr = DMCreateGlobalVector(sl->fv->dmcv,X);CHKERRQ(ierr);
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
      ierr = DMCreateGlobalVector(sl->fv->dmcv,X);CHKERRQ(ierr);
      break;
      
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Unsupported Semi-Lagrangian type provided");
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianGetVertexDM"
PetscErrorCode DMDASemiLagrangianGetVertexDM(DMSemiLagrangian sl,DM *dm)
{
  if (!dm) PetscFunctionReturn(0);
  *dm = sl->dm;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianGetSLDM"
PetscErrorCode DMDASemiLagrangianGetSLDM(DMSemiLagrangian sl,DM *dm)
{
  if (!dm) PetscFunctionReturn(0);
  switch (sl->sl_type) {
    case SLTYPE_CLASSIC:
      *dm = sl->dm;
      break;
    case SLTYPE_FVCONSERVATIVE:
      *dm = sl->fv->dmcv;
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
      *dm = sl->fv->dmcv;
      break;
      
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Unsupported Semi-Lagrangian type provided");
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianGetVelocity"
PetscErrorCode DMDASemiLagrangianGetVelocity(DMSemiLagrangian sl,Vec *v)
{
  if (!v) PetscFunctionReturn(0);
  *v = sl->velocity;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianGetSolution"
PetscErrorCode DMSemiLagrangianGetSolution(DMSemiLagrangian sl,Vec *v)
{
  if (!v) PetscFunctionReturn(0);
  *v = sl->phi_dep;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianDestroy_FVConservative"
PetscErrorCode DMDASemiLagrangianDestroy_FVConservative(DMSemiLagrangian sl)
{
  PetscErrorCode ierr;

  ierr = DMDestroy(&sl->fv->dmcv);CHKERRQ(ierr);
  
  ierr = PetscFree(sl->fv->face_q_w);CHKERRQ(ierr);
  ierr = PetscFree(sl->fv->face_q_xi);CHKERRQ(ierr);

#if 0
  ierr = PetscFree(sl->fv->normal_x);CHKERRQ(ierr);
  ierr = PetscFree(sl->fv->normal_y);CHKERRQ(ierr);
#endif
  ierr = PetscFree(sl->fv);CHKERRQ(ierr);
  sl->fv = NULL;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianSetUp_FVConservative"
PetscErrorCode DMDASemiLagrangianSetUp_FVConservative(DMSemiLagrangian sl)
{
  PetscErrorCode ierr;
  FVStorage fv;
  PetscReal dx,dy;
  PetscInt  i,L;
  
  ierr = PetscMalloc1(1,&fv);CHKERRQ(ierr);
  ierr = PetscMemzero(fv,sizeof(struct _p_FVStorage));CHKERRQ(ierr);
  fv->domain_ctx = NULL;
  fv->inside_domain = NULL;
  fv->dirichlet_ctx = NULL;
  fv->dirichlet_evaluator = NULL;
  sl->fv = fv;
  
  ierr = DMDACreate2d(PetscObjectComm((PetscObject)sl->dm),DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,sl->dactx->mx,sl->dactx->my,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&fv->dmcv);CHKERRQ(ierr);

  if (sl->phi_dep) {
    ierr = VecDestroy(&sl->phi_dep);CHKERRQ(ierr);
  }
  ierr = DMCreateGlobalVector(fv->dmcv,&sl->phi_dep);CHKERRQ(ierr);
  
  dx = sl->dactx->dx;
  dy = sl->dactx->dy;
  ierr = DMDASetUniformCoordinates(fv->dmcv,sl->dactx->xrange[0]+0.5*dx,sl->dactx->xrange[1]-0.5*dx,sl->dactx->yrange[0]+0.5*dy,sl->dactx->yrange[1]-0.5*dy,0,0);CHKERRQ(ierr);
  
  fv->face_q_type = 0;
  fv->face_nqp = 1;
  ierr = PetscMalloc1(fv->face_nqp,&fv->face_q_w);CHKERRQ(ierr);
  ierr = PetscMalloc1(fv->face_nqp,&fv->face_q_xi);CHKERRQ(ierr);
  fv->face_q_w[0] = 2.0;
  fv->face_q_xi[0] = 0.0;

  L = sl->dactx->nx * sl->dactx->ny;
  ierr = PetscMalloc1(L,&fv->vertex_loc_type);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&fv->vertex_type);CHKERRQ(ierr);
  for (i=0; i<L; i++) {
    fv->vertex_type[i] = SLVERTEX_INIT;
  }

  L = sl->dactx->mx * sl->dactx->my;
  ierr = PetscMalloc1(L,&fv->cell_loc_type);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&fv->cell_type);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&fv->bcell_type);CHKERRQ(ierr);
  for (i=0; i<L; i++) {
    fv->cell_loc_type[i] = SLLOC_INIT;
    fv->cell_type[i] = SLCELL_INIT;
    fv->bcell_type[i] = SLBCELL_NONE;
  }
  
  PetscPrintf(PETSC_COMM_WORLD,"[CSL] creating empty sub-cells\n");
  ierr = PetscMalloc1(L,&fv->subcell);CHKERRQ(ierr);
  for (i=0; i<L; i++) {
    ierr = PetscMalloc1(1,&fv->subcell[i]);CHKERRQ(ierr);
    ierr = PetscMemzero(fv->subcell[i],sizeof(struct _p_FVSubCellStorage));CHKERRQ(ierr);
    fv->subcell[i]->valid = PETSC_FALSE;
  }

  PetscPrintf(PETSC_COMM_WORLD,"[CSL] defining default sub-cells\n");
  ierr = DMDASemiLagrangian_FVConservative_SubCellsSetDefault(sl,4,4);CHKERRQ(ierr);
  ierr = DMDASemiLagrangian_FVConservative_SetDirichletTagDefault(sl);CHKERRQ(ierr);
  
#if 0
  fv->mn_normal_x[0] = sl->dactx->nx;
  fv->mn_normal_x[1] = sl->dactx->my;
  ierr = PetscMalloc1(fv->mn_normal_x[0]*fv->mn_normal_x[1]*2,&fv->normal_x);CHKERRQ(ierr);

  fv->mn_normal_y[0] = sl->dactx->mx;
  fv->mn_normal_y[1] = sl->dactx->ny;
  ierr = PetscMalloc1(fv->mn_normal_y[0]*fv->mn_normal_y[1]*2,&fv->normal_y);CHKERRQ(ierr);
#endif
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FVConservative_SubCellReset"
PetscErrorCode FVConservative_SubCellReset(FVSubCellStorage cell)
{
  PetscErrorCode ierr;

  if (!cell->valid) PetscFunctionReturn(0);
  
  ierr = PetscFree(cell->state);CHKERRQ(ierr);
  ierr = PetscFree(cell->centroid);CHKERRQ(ierr);
  ierr = PetscFree(cell->phi);CHKERRQ(ierr);
  ierr = PetscFree(cell->bc_type);CHKERRQ(ierr);
  ierr = PetscFree(cell->cell_flux);CHKERRQ(ierr);
  ierr = PetscFree(cell->cell_normal);CHKERRQ(ierr);
  ierr = PetscFree(cell->cell_velocity);CHKERRQ(ierr);
  cell->mx = 0;
  cell->my = 0;
  cell->valid = PETSC_FALSE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FVConservative_SubCellSetUp"
PetscErrorCode FVConservative_SubCellSetUp(FVSubCellStorage cell,PetscInt mx,PetscInt my,PetscReal xr[],PetscReal yr[])
{
  PetscInt i,j,d,L;
  PetscReal dx,dy,sdx,sdy;
  PetscErrorCode ierr;

  if (cell->valid) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"SubCell is already setup");
  }

  cell->mx = mx;
  cell->my = my;

  L = mx * my;
  ierr = PetscMalloc1(L,&cell->state);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*L,&cell->centroid);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&cell->phi);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&cell->bc_type);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&cell->cell_flux);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*L,&cell->cell_normal);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*L,&cell->cell_velocity);CHKERRQ(ierr);
  for (i=0; i<L; i++) {
    cell->state[i]   = -1;
    cell->bc_type[i] = -1;

    cell->cell_flux[i] = 0.0;
    cell->phi[i] = 0.0;
    for (d=0; d<2; d++) {
      cell->centroid[2*i+d] = 0.0;
      cell->cell_normal[2*i+d] = 0.0;
      cell->cell_velocity[2*i+d] = 0.0;
    }
  }
  dx = xr[1] - xr[0];
  dy = yr[1] - yr[0];
  sdx = dx / ((PetscReal)mx);
  sdy = dy / ((PetscReal)my);
  for (j=0; j<my; j++) {
    for (i=0; i<mx; i++) {
      PetscInt sid = i + j * mx;
      
      cell->centroid[2*sid+0] = xr[0] + 0.5*sdx + i*sdx;
      cell->centroid[2*sid+1] = yr[0] + 0.5*sdy + j*sdy;
    }
  }
  
  cell->valid = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SubCellSetup"
PetscErrorCode DMDASemiLagrangian_FVConservative_SubCellSetup(DMSemiLagrangian sl,PetscInt ci,PetscInt cj,PetscInt mxsub,PetscInt mysub)
{
  
  PetscInt cellid;
  SLDMDA sldmda;
  FVStorage fv;
  FVSubCellStorage subcell;
  PetscReal xr[2],yr[2];
  PetscErrorCode ierr;
  
  sldmda = sl->dactx;
  fv     = sl->fv;
  cellid = ci + sldmda->mx * cj;
  
  subcell = fv->subcell[cellid];
  
  if (subcell->valid) { ierr = FVConservative_SubCellReset(subcell);CHKERRQ(ierr); }
  
  xr[0] = sldmda->xrange[0] + 0.5 * sldmda->dx + ci * sldmda->dx - 0.5 * sldmda->dx;
  xr[1] = xr[0] + sldmda->dx;
  yr[0] = sldmda->yrange[0] + 0.5 * sldmda->dy + cj * sldmda->dy - 0.5 * sldmda->dy;
  yr[1] = yr[0] + sldmda->dy;
  
  ierr = FVConservative_SubCellSetUp(subcell,mxsub,mysub,xr,yr);CHKERRQ(ierr);
  subcell->parent_c = cellid;
  subcell->parent_ci = ci;
  subcell->parent_cj = cj;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SubCellResetAll"
PetscErrorCode DMDASemiLagrangian_FVConservative_SubCellResetAll(DMSemiLagrangian sl)
{
  
  SLDMDA sldmda;
  FVStorage fv;
  FVSubCellStorage subcell;
  PetscInt i,L;
  PetscErrorCode ierr;
  
  sldmda = sl->dactx;
  fv     = sl->fv;

  L = sldmda->mx * sldmda->my;
  for (i=0; i<L; i++) {
    subcell = fv->subcell[i];
    ierr = FVConservative_SubCellReset(subcell);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SubCellsSetDefault"
PetscErrorCode DMDASemiLagrangian_FVConservative_SubCellsSetDefault(DMSemiLagrangian sl,PetscInt mxs,PetscInt mys)
{
  SLDMDA sldmda;
  PetscErrorCode ierr;
  PetscInt ci,cj;
  
  sldmda = sl->dactx;

  // bottom
  cj = 0;
  for (ci=0; ci<sldmda->mx; ci++) {
    ierr = DMDASemiLagrangian_FVConservative_SubCellSetup(sl,ci,cj,mxs,mys);CHKERRQ(ierr);
  }
  
  // top
  cj = sldmda->my - 1;
  for (ci=0; ci<sldmda->mx; ci++) {
    ierr = DMDASemiLagrangian_FVConservative_SubCellSetup(sl,ci,cj,mxs,mys);CHKERRQ(ierr);
  }
  
  // left
  ci = 0;
  for (cj=1; cj<sldmda->my-1; cj++) {
    ierr = DMDASemiLagrangian_FVConservative_SubCellSetup(sl,ci,cj,mxs,mys);CHKERRQ(ierr);
  }

  // right
  ci = sldmda->mx-1;
  for (cj=1; cj<sldmda->my-1; cj++) {
    ierr = DMDASemiLagrangian_FVConservative_SubCellSetup(sl,ci,cj,mxs,mys);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SetDirichletTagDefault"
PetscErrorCode DMDASemiLagrangian_FVConservative_SetDirichletTagDefault(DMSemiLagrangian sl)
{
  PetscInt cellid;
  SLDMDA sldmda;
  FVStorage fv;
  FVSubCellStorage subcell;
  PetscInt ci,cj,si,sj,s;
  
  sldmda = sl->dactx;
  fv     = sl->fv;

  for (cj=0; cj<sldmda->my; cj++) {
    for (ci=0; ci<sldmda->mx; ci++) {
      cellid = ci + sldmda->mx * cj;
      fv->cell_type[cellid] = SLCELL_BLUE;
    }
  }
  
  // bottom
  cj = 0;
  for (ci=0; ci<sldmda->mx; ci++) {
    cellid = ci + sldmda->mx * cj;
    subcell = fv->subcell[cellid];
    
    for (s=0; s<subcell->mx*subcell->my; s++) { subcell->state[s] = 2; }
    sj = 0;
    for (si=0; si<subcell->mx; si++) {
      subcell->bc_type[si+sj*subcell->mx] = 1;
      subcell->state[si+sj*subcell->mx] = 1;
    }
    fv->cell_type[cellid] = SLCELL_YELLOW;
  }
  
  // top
  cj = sldmda->my - 1;
  for (ci=0; ci<sldmda->mx; ci++) {
    cellid = ci + sldmda->mx * cj;
    subcell = fv->subcell[cellid];
    
    for (s=0; s<subcell->mx*subcell->my; s++) { subcell->state[s] = 2; }
    sj = subcell->my-1;
    for (si=0; si<subcell->mx; si++) {
      subcell->bc_type[si+sj*subcell->mx] = 1;
      subcell->state[si+sj*subcell->mx] = 1;
    }
    fv->cell_type[cellid] = SLCELL_YELLOW;
  }
  
  // left
  ci = 0;
  for (cj=0; cj<sldmda->my; cj++) {
    cellid = ci + sldmda->mx * cj;
    subcell = fv->subcell[cellid];

    for (s=0; s<subcell->mx*subcell->my; s++) { subcell->state[s] = 2; }
    si = 0;
    for (sj=0; sj<subcell->my; sj++) {
      subcell->bc_type[si+sj*subcell->mx] = 1;
      subcell->state[si+sj*subcell->mx] = 1;
    }
    fv->cell_type[cellid] = SLCELL_YELLOW;
  }
  
  // right
  ci = sldmda->mx-1;
  for (cj=0; cj<sldmda->my; cj++) {
    cellid = ci + sldmda->mx * cj;
    subcell = fv->subcell[cellid];
    
    for (s=0; s<subcell->mx*subcell->my; s++) { subcell->state[s] = 2; }
    si = subcell->mx - 1;
    for (sj=0; sj<subcell->my; sj++) {
      subcell->bc_type[si+sj*subcell->mx] = 1;
      subcell->state[si+sj*subcell->mx] = 1;
    }
    fv->cell_type[cellid] = SLCELL_YELLOW;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianSetType"
PetscErrorCode DMDASemiLagrangianSetType(DMSemiLagrangian sl,DASLType type)
{
  PetscErrorCode ierr;
  switch (type) {
    case SLTYPE_CLASSIC:
      break;
    case SLTYPE_FVCONSERVATIVE:
      ierr = DMDASemiLagrangianSetUp_FVConservative(sl);CHKERRQ(ierr);
      sl->sl_type = type;
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
      ierr = DMDASemiLagrangianSetUp_FVConservative(sl);CHKERRQ(ierr);
      sl->sl_type = type;
      break;
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Unsupported Semi-Lagrangian type provided");
      break;
  }
      
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianSetStoreDeparturePoints"
PetscErrorCode DMSemiLagrangianSetStoreDeparturePoints(DMSemiLagrangian sl,PetscBool value)
{
  sl->store_departure_points = value;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianGetStoreDeparturePoints"
PetscErrorCode DMSemiLagrangianGetStoreDeparturePoints(DMSemiLagrangian sl,PetscBool *value)
{
  *value = sl->store_departure_points;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DPointReset"
PetscErrorCode DPointReset(PetscInt n,OriginPointType otype,DeparturePoint p[])
{
  PetscInt i;
  
  for (i=0; i<n; i++) {
    p[i].origin = -1;
    p[i].origin_type = otype;
    p[i].departure_coor[0] = p[i].departure_coor[1] = PETSC_MIN_REAL;
    p[i].type = SLLOC_INIT;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianGetDeparturePoints"
PetscErrorCode DMSemiLagrangianGetDeparturePoints(DMSemiLagrangian sl,PetscInt *n,DeparturePoint *p[])
{

  if (sl->store_departure_points) {
    if (n) { *n = sl->n_departure_points; }
    if (p) { *p = sl->dpoint; }
  } else {
    PetscPrintf(PETSC_COMM_WORLD,"DMSemiLagrangianGetDeparturePoints: Returned values are NULL as DMSemiLagrangianSetStoreDeparturePoints(sl,PETSC_TRUE); was not called\n");
    if (n) { *n = 0; }
    if (p) { *p = NULL; }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DPointCreate"
PetscErrorCode DPointCreate(PetscInt n,OriginPointType otype,DeparturePoint **p)
{
  PetscErrorCode ierr;
  DeparturePoint *dp;

  ierr = PetscMalloc1(n,&dp);CHKERRQ(ierr);
  ierr = PetscMemzero(dp,n*sizeof(struct _p_DeparturePoint));CHKERRQ(ierr);

  ierr = DPointReset(n,otype,dp);CHKERRQ(ierr);
  
  *p = dp;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DPointDestroy"
PetscErrorCode DPointDestroy(DeparturePoint **p)
{
  PetscErrorCode ierr;
  DeparturePoint *dp;
  
  if (!p) PetscFunctionReturn(0);
  if (!*p) PetscFunctionReturn(0);
  dp = *p;
  ierr = PetscFree(dp);CHKERRQ(ierr);
  *p = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSLSetup"
PetscErrorCode DMSLSetup(DMSemiLagrangian sl)
{
  PetscErrorCode ierr;
  
  if (sl->setup) PetscFunctionReturn(0);

  switch (sl->sl_type) {
    case SLTYPE_CLASSIC:
      /* nothing to do */
      if (sl->store_departure_points) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"SL (classic) does not support caching departure points");
      break;
    case SLTYPE_FVCONSERVATIVE:
      /* nothing to do */
      if (sl->store_departure_points) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"SL (FV-conservative) does not support caching departure points");
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE:
    {

      if (!sl->fv) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"FV storage is not defined");
      if (!sl->fv->subcell) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"FV->subcell storage is not defined");

      if (!sl->fv->inside_domain) {
        //SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A method must be provided to determine if you are inside the fictitious domain");
        PetscPrintf(PETSC_COMM_WORLD,"*** warning *** No method must be provided to determine if you are inside the fictitious domain\n");
        PetscPrintf(PETSC_COMM_WORLD,"*** warning *** Assuming your domain matches the DMDA\n");
        PetscPrintf(PETSC_COMM_WORLD,"*** warning *** Call DMDASemiLagrangian_FVConservative_SetDomainIdentifier() to define a fictitious domain\n");
      }
      if (!sl->fv->dirichlet_evaluator) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A method must be provided to specify the Dirichlet boundary conditions");
      
      if (sl->store_departure_points) {
        sl->n_departure_points_allocated = sl->dactx->ncells;
        ierr = DPointCreate(sl->n_departure_points_allocated,OPOINT_CENTROID,&sl->dpoint);CHKERRQ(ierr);
        sl->n_departure_points = 0;
      }
      
      /* set flags which mark cell/nodes associated with the fictitious domain */
      ierr = DMDASemiLagrangian_FVConservative_SetUpBoundary(sl);CHKERRQ(ierr);

      // setup required so that the initial condition can be visualized //
      ierr = DMDASemiLagrangian_FVConservative_RestrictVelocity(sl);CHKERRQ(ierr);
      ierr = DMDASemiLagrangian_FVConservative_ReconstructVelocity(sl);CHKERRQ(ierr);
      ierr = DMFVConservativeSemiLagrangianUpdate_vX3_BCellClassify(sl);CHKERRQ(ierr);
      ierr = DMDASemiLagrangian_FVConservative_DirichletEvaluate(sl,0.0);CHKERRQ(ierr);
    }
      break;
    case SLTYPE_FICTDOMAIN_CONSERVATIVE_CELLWISE:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Cellwise CSL is not implemented");
      break;
      
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid semi-Lagrangian method must be provided");
      break;
  }
  
  sl->setup = PETSC_TRUE;
  PetscFunctionReturn(0);
}

PetscErrorCode SLInterpolateAtPointQ1_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M);
PetscErrorCode SLInterpolateAtPointCSpline_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *hd);
PetscErrorCode SLInterpolateAtPointQuasiMonotone_DMDA(SLDMDA s,PetscReal xd[],PetscScalar phi[],PetscReal *_phi_M);

/*
 Iterates over vertices in the DMDA and fills values into a temporary vertex vector (field).
 User field is then consistently interpolated onto x.
 Consistency is defined by 
   (i)  evaluating input vector x at the correctly location in space (cell versus vertex)
   (ii) using the interpolation method defined on the DMSemiLagrangian object
*/
#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangianIterator"
PetscErrorCode DMDASemiLagrangianIterator(DMSemiLagrangian sl,
                                          Vec x,
                                          PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),
                                          void *ctx)
{
  PetscErrorCode ierr;
  DM dm,dmsl_field = NULL;
  Vec coor;
  PetscScalar *_field,*_x;
  const PetscScalar *_coor;
  const PetscScalar *coor_v;
  Vec field;
  PetscInt i,N,b,bs;
  PetscScalar *values;
  SLDMDA    sldmda;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(PetscObjectComm((PetscObject)x),PETSC_ERR_USER,"A valid evaluation function must be provided");

  ierr = DMDASemiLagrangianGetVertexDM(sl,&dm);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm,&field);CHKERRQ(ierr);
  ierr = VecGetSize(field,&N);CHKERRQ(ierr);
  ierr = VecGetBlockSize(field,&bs);CHKERRQ(ierr);
  N = N / bs;
  
  ierr = PetscMalloc1(bs,&values);CHKERRQ(ierr);
  
  ierr = DMGetCoordinates(dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&_coor);CHKERRQ(ierr);
  ierr = VecGetArray(field,&_field);CHKERRQ(ierr);
  for (i=0; i<N; i++) {
    coor_v = &_coor[2*i];
    
    ierr = eval((PetscScalar*)coor_v,values,ctx);CHKERRQ(ierr);
    for (b=0; b<bs; b++) {
      _field[bs*i+b] = values[b];
    }
  }
  ierr = VecRestoreArray(field,&_field);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&_coor);CHKERRQ(ierr);
  ierr = PetscFree(values);CHKERRQ(ierr);

  ierr = VecZeroEntries(x);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianGetSLDM(sl,&dmsl_field);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dmsl_field,&coor);CHKERRQ(ierr);
  ierr = VecGetSize(coor,&N);CHKERRQ(ierr);
  N = N / 2;
  ierr = VecGetArrayRead(coor,&_coor);CHKERRQ(ierr);
  sldmda = sl->dactx;

  ierr = VecGetArray(x,&_x);CHKERRQ(ierr);
  ierr = VecGetArray(field,&_field);CHKERRQ(ierr);
  for (i=0; i<N; i++) {
    PetscReal *coor_i,val_interp;
    
    coor_i = (PetscReal*)&_coor[2*i];
    
    switch (sl->dactx->interpolation_type) {
      case DASLInterp_Q1:
        ierr = SLInterpolateAtPointQ1_DMDA(sldmda,coor_i,_field,&val_interp);CHKERRQ(ierr);
        break;
      case DASLInterp_CSpline:
        ierr = SLInterpolateAtPointCSpline_DMDA(sldmda,coor_i,_field,&val_interp);CHKERRQ(ierr);
        break;
      case DASLInterp_QMonotone:
          ierr = SLInterpolateAtPointQuasiMonotone_DMDA(sldmda,coor_i,_field,&val_interp);CHKERRQ(ierr);
        break;
    }
    _x[i] = val_interp;
  }
  ierr = VecRestoreArray(field,&_field);CHKERRQ(ierr);
  ierr = VecRestoreArray(x,&_x);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&_coor);CHKERRQ(ierr);
  
  
  
  
  ierr = VecDestroy(&field);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SetDomainIdentifier"
PetscErrorCode DMDASemiLagrangian_FVConservative_SetDomainIdentifier(DMSemiLagrangian sl,PetscErrorCode (*func)(PetscReal*,PetscBool*,void*),void *ctx)
{
  FVStorage fv;
  
  fv     = sl->fv;
  fv->domain_ctx    = ctx;
  fv->inside_domain = func;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SetDirichletEvaluator"
PetscErrorCode DMDASemiLagrangian_FVConservative_SetDirichletEvaluator(DMSemiLagrangian sl,PetscErrorCode (*func)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*),void *ctx)
{
  FVStorage fv;
  
  fv     = sl->fv;
  fv->dirichlet_ctx       = ctx;
  fv->dirichlet_evaluator = func;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SetCellVertexType"
PetscErrorCode DMDASemiLagrangian_FVConservative_SetCellVertexType(DMSemiLagrangian sl)
{
  FVStorage fv;
  DM dacell,davert;
  PetscInt mx,my,ncells,nx,ny,nverts,i;
  PetscErrorCode ierr;
  
  fv     = sl->fv;

  dacell = sl->fv->dmcv;
  davert = sl->dm;
  
  ierr = DMDAGetCorners(dacell,NULL,NULL,NULL,&mx,&my,NULL);CHKERRQ(ierr);
  ncells = mx * my;
 
  ierr = DMDAGetCorners(davert,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  nverts = nx * ny;

  /* init */
  for (i=0; i<ncells; i++) {
    fv->cell_type[i] = SLCELL_INIT;
  }
  for (i=0; i<nverts; i++) {
    fv->vertex_type[i] = SLVERTEX_INIT;
  }
  
  /* mark cells */
  for (i=0; i<ncells; i++) {
    PetscInt ci,cj,k,cnt;
    PetscInt vert[4];
    
    cj = i/mx;
    ci = i - cj*mx;
    
    
    vert[0] = fv->vertex_loc_type[ci+cj*nx];
    vert[1] = fv->vertex_loc_type[(ci+1)+cj*nx];
    vert[2] = fv->vertex_loc_type[ci+(cj+1)*nx];
    vert[3] = fv->vertex_loc_type[(ci+1)+(cj+1)*nx];
    
    cnt = 0;
    for (k=0; k<4; k++) {
      if (vert[k] == SLLOC_INSIDE) {
        cnt++;
      }
    }
    
    if (fv->cell_loc_type[i] == SLLOC_INSIDE) { // blue or yellow

      if (cnt == 0) {
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot have 4 vertices outside and centroid marked as inside. Likely cause is the geometry of your domain is not resolved by the DMDA");
      } else if (cnt == 4) {
        fv->cell_type[i] = SLCELL_BLUE;
        
        if (ci == 0) { fv->cell_type[i] = SLCELL_YELLOW; }
        if (cj == 0) { fv->cell_type[i] = SLCELL_YELLOW; }
        if (ci == (mx-1)) { fv->cell_type[i] = SLCELL_YELLOW; }
        if (cj == (my-1)) { fv->cell_type[i] = SLCELL_YELLOW; }
        
      } else {
        fv->cell_type[i] = SLCELL_YELLOW;
      }
      
    } else { // orange or grey
      
      if (cnt == 0) {
        fv->cell_type[i] = SLCELL_GREY;
      } else {
        fv->cell_type[i] = SLCELL_ORANGE;
      }
      
    }
  }

  /* mark vertices */
  for (i=0; i<nverts; i++) {
    fv->vertex_type[i] = SLVERTEX_ACTIVE;
    if (fv->vertex_loc_type[i] == SLLOC_OUTSIDE) {
      fv->vertex_type[i] = SLVERTEX_DEACTIVE;
    }
  }

  /* vertices of orange and yellow cells will require velocities to be reconstructed at their location */
  for (i=0; i<ncells; i++) {
    PetscInt ci,cj,k;
    PetscInt vert[4],vid[4];
    
    cj = i/mx;
    ci = i - cj*mx;
    
    if (fv->cell_type[i] == SLCELL_GREY) continue;
    if (fv->cell_type[i] == SLCELL_BLUE) continue;
    
    vid[0] = ci+cj*nx;         vert[0] = fv->vertex_loc_type[ vid[0] ];
    vid[1] = (ci+1)+cj*nx;     vert[1] = fv->vertex_loc_type[ vid[1] ];
    vid[2] = ci+(cj+1)*nx;     vert[2] = fv->vertex_loc_type[ vid[2] ];
    vid[3] = (ci+1)+(cj+1)*nx; vert[3] = fv->vertex_loc_type[ vid[3] ];
    
    for (k=0; k<4; k++) {
      if (vert[k] == SLLOC_OUTSIDE) {
        fv->vertex_type[ vid[k] ] = SLVERTEX_REQUIRES_RECONSTRUCTION;
      }
    }
    
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_SetUpBoundary"
PetscErrorCode DMDASemiLagrangian_FVConservative_SetUpBoundary(DMSemiLagrangian sl)
{
  SLDMDA sldmda;
  FVStorage fv;
  DM dacell,davert,cda;
  Vec coor;
  const PetscReal *LA_coor;
  PetscInt mx,my,ncells,nx,ny,nverts,i,mx_sub,my_sub,ncells_sub,mx_patch,my_patch;
  PetscErrorCode ierr;
  
  sldmda = sl->dactx;
  fv     = sl->fv;

  
  dacell = sl->fv->dmcv;
  davert = sl->dm;

  if (!fv->inside_domain) {
    ierr = DMDAGetCorners(dacell,NULL,NULL,NULL,&mx,&my,NULL);CHKERRQ(ierr);
    ncells = mx * my;
    for (i=0; i<ncells; i++) {
      fv->cell_loc_type[i] = SLLOC_INSIDE;
    }
  
    ierr = DMDAGetCorners(davert,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
    nverts = nx * ny;
    for (i=0; i<nverts; i++) {
      fv->vertex_loc_type[i] = SLLOC_INSIDE;
      
      fv->vertex_type[i] = SLVERTEX_ACTIVE;
    }
    
    PetscFunctionReturn(0);
  }

  PetscPrintf(PETSC_COMM_WORLD,"[CSL] resetting fictitious boundary\n");
  ierr = DMDASemiLagrangian_FVConservative_SubCellResetAll(sl);CHKERRQ(ierr);

  /* tag centroids */
  ierr = DMDAGetCorners(dacell,NULL,NULL,NULL,&mx,&my,NULL);CHKERRQ(ierr);
  ncells = mx * my;
  
  ierr = DMGetCoordinateDM(dacell,&cda);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dacell,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  for (i=0; i<ncells; i++) {
    PetscReal pos[2];
    PetscBool inside = PETSC_FALSE;
    
    pos[0] = LA_coor[2*i];
    pos[1] = LA_coor[2*i+1];
    ierr = fv->inside_domain(pos,&inside,fv->domain_ctx);CHKERRQ(ierr);
    if (!inside) {
      fv->cell_loc_type[i] = SLLOC_OUTSIDE;
    } else {
      fv->cell_loc_type[i] = SLLOC_INSIDE;
    }
  }
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);

  /* tag vertices */
  ierr = DMDAGetCorners(davert,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  nverts = nx * ny;
  
  ierr = DMGetCoordinateDM(davert,&cda);CHKERRQ(ierr);
  ierr = DMGetCoordinates(davert,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  for (i=0; i<nverts; i++) {
    PetscReal pos[2];
    PetscBool inside = PETSC_FALSE;
    
    pos[0] = LA_coor[2*i];
    pos[1] = LA_coor[2*i+1];
    ierr = fv->inside_domain(pos,&inside,fv->domain_ctx);CHKERRQ(ierr);
    if (!inside) {
      fv->vertex_loc_type[i] = SLLOC_OUTSIDE;
    } else {
      fv->vertex_loc_type[i] = SLLOC_INSIDE;
    }
  }
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);

  /* determine cells to sub-divide */
  PetscBool *sub_divide;
  
  PetscMalloc1(ncells,&sub_divide);
  for (i=0; i<ncells; i++) { sub_divide[i] = PETSC_FALSE; }
  
  /* 
   Rules defining when to sub-divide a cell:
   (i) some vertices inside and some vertices outside
   (ii) centroid inside but has a von-Neumann neighbour (3x3) which is outside
   (iii) centroid is inside but a cell is located on boundary of the DMDA
  */
  
  /* tag sub-cells */
  for (i=0; i<ncells; i++) {
    PetscInt ci,cj,ii,jj,kk,vcnt;
    
    cj = i/mx;
    ci = i - cj*mx;

    PetscInt vert[4];

    vert[0] = fv->vertex_loc_type[ci+cj*nx];
    vert[1] = fv->vertex_loc_type[(ci+1)+cj*nx];
    vert[2] = fv->vertex_loc_type[ci+(cj+1)*nx];
    vert[3] = fv->vertex_loc_type[(ci+1)+(cj+1)*nx];


    if (fv->cell_loc_type[i] == SLLOC_INSIDE) {
      vcnt = 0;
      for (kk=0; kk<4; kk++) {
        if (vert[kk] == SLLOC_INSIDE) {
          vcnt++;
        }
      }
      if (vcnt != 4) {
        sub_divide[i] = PETSC_TRUE;
        continue;
      }
    }

    
    if (fv->cell_loc_type[i] == SLLOC_OUTSIDE) {
      for (kk=0; kk<4; kk++) {
        if (vert[kk] == SLLOC_INSIDE) {
          sub_divide[i] = PETSC_TRUE;
          break;
        }
      }
    }
    if (sub_divide[i]) continue;
    
    PetscInt neigh[9];
    for (jj=0; jj<3; jj++) {
      for (ii=0; ii<3; ii++) {
        PetscInt nci,ncj;
        PetscBool valid = PETSC_TRUE;
        
        nci = ci - 1 + ii;
        ncj = cj - 1 + jj;
        if (nci < 0) valid = PETSC_FALSE;
        if (ncj < 0) valid = PETSC_FALSE;
        if (nci >= mx) valid = PETSC_FALSE;
        if (ncj >= my) valid = PETSC_FALSE;
        
        if (valid) {
          neigh[ii+jj*3] = nci + ncj * mx;
        } else {
          neigh[ii+jj*3] = -1;
        }
      }
    }
    
    // (ii) //
    if (fv->cell_loc_type[i] == SLLOC_INSIDE) {
      for (kk=0; kk<9; kk++) {
        if (neigh[kk] >= 0) {
          if (fv->cell_loc_type[neigh[kk]] == SLLOC_OUTSIDE) {
            sub_divide[i] = PETSC_TRUE;
            break;
          }
        }
      }
    }
    if (sub_divide[i]) continue;
    
    // (iii) //
    if (fv->cell_loc_type[i] == SLLOC_INSIDE) {
      for (kk=0; kk<9; kk++) {
        if (neigh[kk] == -1) {
          sub_divide[i] = PETSC_TRUE;
          break;
        }
      }
    }
    if (sub_divide[i]) continue;
    
  }
  
  /* split all tagged cells */
  mx_sub = 4;
  my_sub = 4;

  for (i=0; i<ncells; i++) {
    PetscInt ci,cj;
    
    cj = i/mx;
    ci = i - cj*mx;
    if (sub_divide[i]) {
      ierr = DMDASemiLagrangian_FVConservative_SubCellSetup(sl,ci,cj,mx_sub,my_sub);CHKERRQ(ierr);
    }
  }

  /* process cells */
  /* first pass - mark state as outside (0) or dirichlet candidate (2) */
  
  ncells_sub = mx_sub * my_sub;
  for (i=0; i<ncells; i++) {
    PetscInt ci,cj,kk;
    
    cj = i/mx;
    ci = i - cj*mx;
    if (sub_divide[i]) {
      
      for (kk=0; kk<ncells_sub; kk++) {
        PetscReal pos[2];
        PetscBool inside = PETSC_FALSE;
        
        pos[0] = sl->fv->subcell[i]->centroid[2*kk];
        pos[1] = sl->fv->subcell[i]->centroid[2*kk+1];
        ierr = fv->inside_domain(pos,&inside,fv->domain_ctx);CHKERRQ(ierr);
        if (inside) {
          sl->fv->subcell[i]->state[kk] = 2;
        } else {
          sl->fv->subcell[i]->state[kk] = 0;
        }
      }
      
      if (ci == 0) {
        PetscInt ii,jj;
        ii = 0;
        for (jj=0; jj<my_sub; jj++) {
          sl->fv->subcell[i]->state[ii+jj*mx_sub] = 1;
        }
      }
      if (cj == 0) {
        PetscInt ii,jj;
        jj = 0;
        for (ii=0; ii<mx_sub; ii++) {
          sl->fv->subcell[i]->state[ii+jj*mx_sub] = 1;
        }
      }
      if (ci == (mx-1)) {
        PetscInt ii,jj;
        ii = mx_sub-1;
        for (jj=0; jj<my_sub; jj++) {
          sl->fv->subcell[i]->state[ii+jj*mx_sub] = 1;
        }
      }
      if (cj == (my-1)) {
        PetscInt ii,jj;
        jj = my_sub-1;
        for (ii=0; ii<mx_sub; ii++) {
          sl->fv->subcell[i]->state[ii+jj*mx_sub] = 1;
        }
      }
      
    }
  }

  /* second pass - mark state as dirichlet (1) if a subcell has a neighbour sub-cell or regular cell which is outside */
  mx_patch = mx_sub + 2;
  my_patch = my_sub + 2;
  
  PetscInt *patch_list;
  
  PetscMalloc1(mx_patch*my_patch,&patch_list);
  
  for (i=0; i<ncells; i++) {
    PetscInt li,lj,ii,jj,k,ni,nj,cell_state;
    FVSubCellStorage subcell;

    
    subcell = sl->fv->subcell[i];
    
    if (sub_divide[i]) {
      
      /*{
        PetscInt c;
        for (c=0; c<mx_sub*my_sub;c++) {
          printf("state[%d] = %d\n",c,subcell->state[c]);
        }
      }*/
      
      // compute the patch
      ierr = GetSubCellNeighbourStatePatch(sldmda,fv,subcell,patch_list);CHKERRQ(ierr);
      /*{
        PetscInt c;
        for (c=0; c<mx_patch*my_patch;c++) {
          printf("patch_state[%d] = %d\n",c,patch_list[c]);
        }
      }*/
      
      // traverse the patch interior
      for (lj=1; lj<my_patch-1; lj++) {
        for (li=1; li<mx_patch-1; li++) {
          ii = li - 1;
          jj = lj - 1;
          
          PetscInt neigh_subcell_state[9];
          
          for (nj=0; nj<3; nj++) {
            for (ni=0; ni<3; ni++) {
              PetscInt nci,ncj;
              
              nci = li - 1 + ni;
              ncj = lj - 1 + nj;
              
              neigh_subcell_state[ni + nj*3] = patch_list[nci + ncj*mx_patch];
            }
          }

          /*{
            PetscInt c;
            for (c=0; c<9;c++) {
              printf("subcell[%d,%d] neighbour_state[%d] = %d\n",ii,jj,c,neigh_subcell_state[c]);
            }
          }*/

          
          cell_state = subcell->state[ii + jj*mx_sub];
          if (cell_state == 0) continue;

          for (k=0; k<9; k++) {
            if (neigh_subcell_state[k] == 0) {
              subcell->state[ii + jj*mx_sub] = 1;
              //printf("found dirichlet \n");
            }
          }
          
        }
      }
      
    }
  }

  ierr = DMDASemiLagrangian_FVConservative_SetCellVertexType(sl);CHKERRQ(ierr);
  
  ierr = PetscFree(patch_list);CHKERRQ(ierr);
  ierr = PetscFree(sub_divide);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* 
 list is (nsubcells+2) x (nsubcells+2) long
 (-1) un-init; (0) outside; (1) active; (2) inside/deactive
*/
#undef __FUNCT__
#define __FUNCT__ "GetSubCellNeighbourStatePatch"
PetscErrorCode GetSubCellNeighbourStatePatch(SLDMDA dactx,FVStorage fv,FVSubCellStorage subcell,PetscInt list[])
{
  PetscInt li,lj,i,ii,jj,mx,my,smx,smy,L,ci,cj;
  FVSubCellStorage subcell_neigh,subcell_center;

  
  smx = subcell->mx;
  smy = subcell->my;
  
  mx = smx+2;
  my = smy+2;
  L = (smx+2) * (smy+2);
  for (i=0; i<L; i++) {
    list[i] = -1;
  }
  
  /* copy interior state values */
  for (lj=1; lj<my-1; lj++) {
    for (li=1; li<mx-1; li++) {
      list[li+lj*mx] = subcell->state[(li-1)+(lj-1)*smx];
    }
  }
  
  ci = subcell->parent_ci;
  cj = subcell->parent_cj;
  
  // compute cell neighbours
  PetscInt neigh[9];
  
  for (jj=0; jj<3; jj++) {
    for (ii=0; ii<3; ii++) {
      PetscInt nci,ncj;
      PetscBool valid = PETSC_TRUE;
      
      nci = ci - 1 + ii;
      ncj = cj - 1 + jj;
      if (nci < 0) valid = PETSC_FALSE;
      if (ncj < 0) valid = PETSC_FALSE;
      if (nci >= dactx->mx) valid = PETSC_FALSE;
      if (ncj >= dactx->my) valid = PETSC_FALSE;
      
      if (valid) {
        neigh[ii+jj*3] = nci + ncj * dactx->mx;
      } else {
        neigh[ii+jj*3] = -1;
      }
    }
  }
  subcell_center = fv->subcell[neigh[4]];

  /*
   6 7 8
   3 4 5
   0 1 2
  */

  // check left (3)
  if (neigh[3] != -1) {
    subcell_neigh = fv->subcell[neigh[3]];
    
    if (subcell_neigh->valid) {
      ii = subcell_neigh->mx-1;
      for (jj=0; jj<subcell_neigh->my; jj++) {
        
        li = 0; // left
        lj = jj + 1;
        list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
      }
    } else {
      if (fv->cell_loc_type[neigh[3]] == SLLOC_OUTSIDE) {
        for (jj=0; jj<subcell_center->my; jj++) {
          
          li = 0; // left
          lj = jj + 1;
          list[li+lj*mx] = 0; /* mark as outside */
        }
      }
    }
  }

  // check right (5)
  if (neigh[5] != -1) {
    subcell_neigh = fv->subcell[neigh[5]];
    
    if (subcell_neigh->valid) {
      ii = 0;
      for (jj=0; jj<subcell_neigh->my; jj++) {
        
        li = mx - 1; // right
        lj = jj + 1;
        list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
      }
    } else {
      if (fv->cell_loc_type[neigh[5]] == SLLOC_OUTSIDE) {
        for (jj=0; jj<subcell_center->my; jj++) {
          
          li = mx - 1; // right
          lj = jj + 1;
          list[li+lj*mx] = 0; /* mark as outside */
        }
      }
    }
  }

  
  // check bottom (1) //
  if (neigh[1] != -1) {
    subcell_neigh = fv->subcell[neigh[1]];
    
    if (subcell_neigh->valid) {
      jj = subcell_neigh->my - 1;
      for (ii=0; ii<subcell_neigh->mx; ii++) {
        
        li = ii + 1;
        lj = 0; // bottom
        list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
      }
    } else {
      if (fv->cell_loc_type[neigh[1]] == SLLOC_OUTSIDE) {
        for (ii=0; ii<subcell_center->mx; ii++) {
          
          li = ii + 1;
          lj = 0; // bottom
          list[li+lj*mx] = 0; /* mark as outside */
        }
      }
    }
  }
  
  // check top (7) //
  if (neigh[7] != -1) {
    subcell_neigh = fv->subcell[neigh[7]];
    
    if (subcell_neigh->valid) {
      jj = 0;
      for (ii=0; ii<subcell_neigh->mx; ii++) {
        
        li = ii + 1;
        lj = my - 1; // top
        list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
      }
    } else {
      if (fv->cell_loc_type[neigh[7]] == SLLOC_OUTSIDE) {
        for (ii=0; ii<subcell_center->mx; ii++) {
          
          li = ii + 1;
          lj = my - 1; // top
          list[li+lj*mx] = 0; /* mark as outside */
        }
      }
    }
  }


  // corners
  // check top-left (6)
  if (neigh[6] != -1) {
    subcell_neigh = fv->subcell[neigh[6]];
    
    if (subcell_neigh->valid) {
      ii = subcell_neigh->mx-1;
      jj = 0;

      li = 0;
      lj = my - 1;
      list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
    } else {
      if (fv->cell_loc_type[neigh[6]] == SLLOC_OUTSIDE) {
        ii = subcell_center->mx-1;
        jj = 0;
        
        li = 0;
        lj = my - 1;
        list[li+lj*mx] = 0; /* mark as outside */
      }
    }
  }

  // check bottom-left (0)
  if (neigh[0] != -1) {
    subcell_neigh = fv->subcell[neigh[0]];
    
    if (subcell_neigh->valid) {
      ii = subcell_neigh->mx-1;
      jj = subcell_neigh->my-1;
      
      li = 0;
      lj = 0;
      list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
    } else {
      if (fv->cell_loc_type[neigh[0]] == SLLOC_OUTSIDE) {
        ii = subcell_center->mx-1;
        jj = subcell_center->my-1;
        
        li = 0;
        lj = 0;
        list[li+lj*mx] = 0; /* mark as outside */
      }
    }
  }

  // check top-right (8)
  if (neigh[8] != -1) {
    subcell_neigh = fv->subcell[neigh[8]];
    
    if (subcell_neigh->valid) {
      ii = 0;
      jj = 0;
      
      li = mx-1;
      lj = my-1;
      list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
    } else {
      if (fv->cell_loc_type[neigh[8]] == SLLOC_OUTSIDE) {
        ii = 0;
        jj = 0;
        
        li = mx-1;
        lj = my-1;
        list[li+lj*mx] = 0; /* mark as outside */
      }
    }
  }

  // check bottom-right (2)
  if (neigh[2] != -1) {
    subcell_neigh = fv->subcell[neigh[2]];
    
    if (subcell_neigh->valid) {
      ii = 0;
      jj = subcell_neigh->my-1;
      
      li = mx-1;
      lj = 0;
      list[li+lj*mx] = subcell_neigh->state[ ii + jj*subcell_neigh->mx];
    } else {
      if (fv->cell_loc_type[neigh[2]] == SLLOC_OUTSIDE) {
        ii = 0;
        jj = subcell_center->my-1;
        
        li = mx-1;
        lj = 0;
        list[li+lj*mx] = 0; /* mark as outside */
      }
    }
  }
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_DirichletEvaluate"
PetscErrorCode DMDASemiLagrangian_FVConservative_DirichletEvaluate(DMSemiLagrangian sl,PetscReal time)
{
  SLDMDA sldmda;
  FVStorage fv;
  PetscInt c,sc;
  PetscReal *LA_phi_dep;
  PetscErrorCode ierr;
  
  sldmda = sl->dactx;
  fv     = sl->fv;

  ierr = VecGetArray(sl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);

  for (c=0; c<sldmda->mx*sldmda->my; c++) {
    FVSubCellStorage subcell;
    PetscInt nsub;
    
    subcell = sl->fv->subcell[c];
    if (subcell->valid) {
   
      nsub = subcell->mx * subcell->my;
      
      for (sc=0; sc<nsub; sc++) {
        PetscBool constrain;
        PetscReal value;
        
        if (subcell->state[sc] >= 0) {
          subcell->phi[sc] = LA_phi_dep[c]; /* insert a default value based on the initial condition / previous step */
          
          ierr = fv->dirichlet_evaluator(&subcell->centroid[2*sc],time,&constrain,&value,fv->dirichlet_ctx);CHKERRQ(ierr);
          if (constrain) {
            subcell->phi[sc] = value;
          }
        }
        
      }
    }
    if (fv->cell_type[c] == SLCELL_GREY) {
      LA_phi_dep[c] = 0.0;
    }
    
  }
  ierr = VecRestoreArray(sl->phi_dep,&LA_phi_dep);CHKERRQ(ierr);

  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_RestrictVelocity"
PetscErrorCode DMDASemiLagrangian_FVConservative_RestrictVelocity(DMSemiLagrangian sl)
{
  SLDMDA sldmda;
  FVStorage fv;
  PetscInt v;
  PetscReal *LA_field;
  PetscErrorCode ierr;
  
  sldmda = sl->dactx;
  fv     = sl->fv;
  
  ierr = VecGetArray(sl->velocity,&LA_field);CHKERRQ(ierr);
  
  for (v=0; v<sldmda->nx*sldmda->ny; v++) {
    if (fv->vertex_type[v] != SLVERTEX_ACTIVE) {
      LA_field[2*v+0] = 0.0;
      LA_field[2*v+1] = 0.0;
    }
  }
  ierr = VecRestoreArray(sl->velocity,&LA_field);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASemiLagrangian_FVConservative_ReconstructVelocity"
PetscErrorCode DMDASemiLagrangian_FVConservative_ReconstructVelocity(DMSemiLagrangian sl)
{
  SLDMDA sldmda;
  FVStorage fv;
  PetscInt v,vi,vj;
  PetscReal *LA_field;
  PetscErrorCode ierr;
  
  sldmda = sl->dactx;
  fv     = sl->fv;
  
  ierr = VecGetArray(sl->velocity,&LA_field);CHKERRQ(ierr);
  
  
  for (vj=0; vj<sldmda->ny; vj++) {
    for (vi=0; vi<sldmda->nx; vi++) {
      v = vi + vj * sldmda->nx;

      if (fv->vertex_type[v] == SLVERTEX_ACTIVE) { continue; }
      if (fv->vertex_type[v] == SLVERTEX_REQUIRES_RECONSTRUCTION) {
        PetscInt cnt = 0;
        PetscInt ii,jj,k,cni,cnj,cn,neigh[9];
        PetscReal vavg[] = { 0.0, 0.0 };
        
        for (jj=0; jj<3; jj++) {
          for (ii=0; ii<3; ii++) {
            neigh[ii+jj*3] = -1;

            cni = vi - 1 + ii;
            if (cni < 0)           continue;
            if (cni >= sldmda->nx) continue;

            cnj = vj - 1 + jj;
            if (cnj < 0)           continue;
            if (cnj >= sldmda->ny) continue;

            cn = cni + cnj * sldmda->nx;
          
            neigh[ii+jj*3] = cn;
          }
        }
        
        for (k=0; k<9; k++) {
          if (neigh[k] >= 0) {
            if (fv->vertex_type[ neigh[k] ] == SLVERTEX_ACTIVE) {
              vavg[0] += LA_field[2*neigh[k] + 0];
              vavg[1] += LA_field[2*neigh[k] + 1];
              cnt++;
            }
          }
        }
        
        if (cnt == 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Zero active nodes detected during reconstruction - this should never happen");
        
        vavg[0] = vavg[0] / ((PetscReal)cnt);
        vavg[1] = vavg[1] / ((PetscReal)cnt);
        
        LA_field[2*v+0] = vavg[0];
        LA_field[2*v+1] = vavg[1];
        
      }
    }
  }
  ierr = VecRestoreArray(sl->velocity,&LA_field);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
