/*
 
 - Bug fixes in CSLPolyMarkVertices()
 - Added VTK viewer
 - Added global bounds limiter
 - Added facet labels
 
 - Added support for facet iterators
 - Added support for marking inflow bcs and reporting if any inflow facets have not had a bc defined
 - Added support for imposing boundary conditions
 - Added support for updating outflow facets post CSL update
 - Added line_intersect_poly
 - Added array to store facet-poly sizes
*/


#include <petsc.h>
#ifdef HAVE_OMP
#include <omp.h>
#endif
#include "cslpoly.h"
#include "dmtetdt.h"

const char nan_tagp[] = {'\0'};

void compute_divu_default(CSLPoly sl,PetscInt cellid,PetscReal x[],PetscReal *divu);

/*
 The polgon is oriented in a counter-clock wise direction.
 Returns 1 if p[] is located inside the doman
 Returns 0 if p[] is located outside the domain
 
 http://paulbourke.net/geometry/polygonmesh/
 
 https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
*/
void point_in_polygon(PetscInt nvert, const PetscReal vert[], const PetscReal p[],PetscInt *inside)
{
  PetscInt i,j,c = 0;
  
  for (i=0, j=nvert-1; i<nvert; j=i++) {
    if ( ((vert[2*i+1] > p[1]) != (vert[2*j+1] > p[1])) &&
        (p[0] < (vert[2*j] - vert[2*i]) * (p[1] - vert[2*i+1]) / (vert[2*j+1] - vert[2*i+1]) + vert[2*i]) )
      c = !c;
  }
  *inside = c;
}

/*
 Determine the intersection poPetscInt of two line segments
 Return FALSE if the lines don't intersect

 Dource from
 
 http://paulbourke.net/geometry/pointlineplane/pdb.c
*/

#define LINE_INTERSECT_EPS 1.0e-8

static inline void line_intersect(const PetscReal P1[],const PetscReal P2[],const PetscReal P3[],const PetscReal P4[],
                    PetscReal X[],PetscInt *intersect)
{
  PetscReal mua,mub;
  PetscReal denom,numera,numerb;
  PetscReal x1,y1 , x2,y2 , x3,y3 , x4,y4;
  PetscReal x,y;
  
  *intersect = 0;
  
  x1 = P1[0]; y1 = P1[1];
  x2 = P2[0]; y2 = P2[1];
  x3 = P3[0]; y3 = P3[1];
  x4 = P4[0]; y4 = P4[1];
  
  denom  = (y4-y3) * (x2-x1) - (x4-x3) * (y2-y1);
  numera = (x4-x3) * (y1-y3) - (y4-y3) * (x1-x3);
  numerb = (x2-x1) * (y1-y3) - (y2-y1) * (x1-x3);
  
  /* Are the line coincident? */
  if (PetscAbsReal(numera) < LINE_INTERSECT_EPS && PetscAbsReal(numerb) < LINE_INTERSECT_EPS && PetscAbsReal(denom) < LINE_INTERSECT_EPS) {
    x = 0.5*(x1 + x2);
    y = 0.5*(y1 + y2);
    X[0] = x;
    X[1] = y;
    return;
  }
  
  /* Are the line parallel */
  if (PetscAbsReal(denom) < LINE_INTERSECT_EPS) {
    x = 0;
    y = 0;
    X[0] = x;
    X[1] = y;
    return;
  }
  
  /* Is the intersection along the the segments */
  mua = numera / denom;
  mub = numerb / denom;
  if (mua < 0 || mua > 1 || mub < 0 || mub > 1) {
    x = 0;
    y = 0;
    X[0] = x;
    X[1] = y;
    return;
  }
  x = x1 + mua * (x2 - x1);
  y = y1 + mua * (y2 - y1);
  X[0] = x;
  X[1] = y;
  *intersect = 1;
}

void line_intersect_poly(const PetscReal P1[],const PetscReal P2[],
                         PetscInt npoints,const PetscReal poly_coor[],
                         PetscReal X[],PetscInt *poly_facet,PetscInt *intersect)
{
  PetscInt p;
  
  *intersect = 0;
  for (p=0; p<npoints; p++) {
    PetscInt p3 = p;
    PetscInt p4 = (p+1)%npoints;
    const PetscReal *P3 = &poly_coor[2*p3];
    const PetscReal *P4 = &poly_coor[2*p4];
    
    //printf("poly %d\n",p);
    //printf("  P1 %+1.4e %+1.4e : P2 %+1.4e %+1.4e : P3 %+1.4e %+1.4e : P4 %+1.4e %+1.4e\n",
    //       P1[0],P1[1],P2[0],P2[1], P3[0],P3[1],P4[0],P4[1]);
    line_intersect(P1,P2, P3,P4,X,intersect);
    if (*intersect == 1) { break; }
  }
  if (poly_facet) {
    *poly_facet = p;
  }
}

/* Maple generated code */
static inline void ComputeMatrixInverse_3x3(PetscReal A[3][3],PetscReal B[3][3])
{
  PetscReal t4,t6,t8,t10,t12,t14,t17;
  
  t4 = A[2][0] * A[0][1];
  t6 = A[2][0] * A[0][2];
  t8 = A[1][0] * A[0][1];
  t10 = A[1][0] * A[0][2];
  t12 = A[0][0] * A[1][1];
  t14 = A[0][0] * A[1][2];
  t17 = 0.1e1 / (t4 * A[1][2] - t6 * A[1][1] - t8 * A[2][2] + t10 * A[2][1] + t12 * A[2][2] - t14 * A[2][1]);
  
  B[0][0] = (A[1][1] * A[2][2] - A[1][2] * A[2][1]) * t17;
  B[0][1] = -(A[0][1] * A[2][2] - A[0][2] * A[2][1]) * t17;
  B[0][2] = (A[0][1] * A[1][2] - A[0][2] * A[1][1]) * t17;
  B[1][0] = -(-A[2][0] * A[1][2] + A[1][0] * A[2][2]) * t17;
  B[1][1] = (-t6 + A[0][0] * A[2][2]) * t17;
  B[1][2] = -(-t10 + t14) * t17;
  B[2][0] = (-A[2][0] * A[1][1] + A[1][0] * A[2][1]) * t17;
  B[2][1] = -(-t4 + A[0][0] * A[2][1]) * t17;
  B[2][2] = (-t8 + t12) * t17;
}

static inline void CubicSpline_PrepareBasis(PetscReal xn[],PetscReal B[3][3])
{
  PetscReal A[3][3];
  
  A[0][0] = 2.0 / (xn[1] - xn[0]);
  A[0][1] = 1.0 / (xn[1] - xn[0]);
  A[0][2] = 0.0;
  
  A[1][0] = A[0][1];
  A[1][1] = 2.0 * ( 1.0/(xn[1]-xn[0]) + 1.0/(xn[2]-xn[1]) );
  A[1][2] = 1.0/(xn[2]-xn[1]);
  
  A[2][0] = 0.0;
  A[2][1] = A[1][2];
  A[2][2] = 2.0 / (xn[2] - xn[1]);
  
  ComputeMatrixInverse_3x3(A,B);
}

static void CubicSpline_Interpolate(PetscReal xi,PetscReal xn[],PetscReal B[3][3],PetscReal yn[],PetscReal *yi)
{
  PetscReal k[3],b[3];
  PetscReal x1x02,x2x12;
  PetscReal qa[2],qb[2];
  PetscInt i;
  
  x1x02 = (xn[1] - xn[0])*(xn[1] - xn[0]);
  x2x12 = (xn[2] - xn[1])*(xn[2] - xn[1]);
  
  b[0] = 3.0 * (yn[1] - yn[0])/x1x02;
  b[2] = 3.0 * (yn[2] - yn[1])/x2x12;
  b[1] = b[0] + b[2];
  
  k[0] = k[1] = k[2] = 0.0;
  for (i=0; i<3; i++) {
    k[0] += B[0][i] * b[i];
    k[1] += B[1][i] * b[i];
    k[2] += B[2][i] * b[i];
  }
  
  qa[0] = k[0] * (xn[1] - xn[0]) - (yn[1] - yn[0]);
  qa[1] = k[1] * (xn[2] - xn[1]) - (yn[2] - yn[1]);
  
  qb[0] = -k[1] * (xn[1] - xn[0]) + (yn[1] - yn[0]);
  qb[1] = -k[2] * (xn[2] - xn[1]) + (yn[2] - yn[1]);
  
  if (xi < xn[1]) {
    PetscReal t = (xi - xn[0])/(xn[1] - xn[0]);
    
    *yi = (1.0-t) * yn[0] + t * yn[1] + t*(1.0-t)*(qa[0]*(1.0-t) + qb[0]*t);
  } else {
    PetscReal t = (xi - xn[1])/(xn[2] - xn[1]);
    
    *yi = (1.0-t) * yn[1] + t * yn[2] + t*(1.0-t)*(qa[1]*(1.0-t) + qb[1]*t);
  }
}

static void weno4_McD(PetscReal xs[],PetscReal fs[],PetscReal x,PetscReal *f)
{
  PetscReal x_i,f_im1,f_i,f_ip1,f_ip2,dx;
  PetscReal w1,p1,w2,p2;
  PetscReal C_1,C_2,IS_1,IS_2,alpha_1,alpha_2;
  
  
  dx = xs[1] - xs[0];
  
  x_i = xs[1];
  f_im1 = fs[0];
  f_i = fs[1];
  f_ip1 = fs[2];
  f_ip2 = fs[3];
  
  p1 = f_i + (f_ip1-f_im1)*(x-x_i)/(2.0*dx)
  + (f_ip1-2.0*f_i+f_im1)*(x-x_i)*(x-x_i)/(2.0*dx*dx);
  
  p2 = f_i + (-f_ip2+4.0*f_ip1-3.0*f_i)*(x-x_i)/(2.0*dx)
  + (f_ip2-2.0*f_ip1+f_i)*(x-x_i)*(x-x_i)/(2.0*dx*dx);
  
  C_1 = (xs[3] - x)/(3.0*dx);
  C_2 = (x - xs[0])/(3.0*dx);
  
  IS_1 = (26.0*f_ip1*f_im1 - 52.0*f_i*f_im1   - 76.0*f_ip1*f_i + 25.0*f_ip1*f_ip1 + 64.0*f_i*f_i + 13.0*f_im1*f_im1)/12.0;
  IS_2 = (26.0*f_ip2*f_i   - 52.0*f_ip2*f_ip1 - 76.0*f_ip1*f_i + 25.0*f_i*f_i     + 64.0*f_ip1*f_ip1 + 13.0*f_ip2*f_ip2)/12.0;
  
  alpha_1 = C_1 / ((1.0e-6 + IS_1)*(1.0e-6 + IS_1));
  alpha_2 = C_2 / ((1.0e-6 + IS_2)*(1.0e-6 + IS_2));
  
  w1 = alpha_1/(alpha_1 + alpha_2);
  w2 = alpha_2/(alpha_1 + alpha_2);
  
  *f = w1*p1 + w2*p2;
}

/*
 u_i-1        u_i        u_i+1
 | --------- | --------- | --------- |
 
 */
//
// see https://math.la.asu.edu/~gardner/weno.pdf for further explaination
//
void weno3(PetscReal *__restrict xs,PetscReal *__restrict fs,PetscReal x,PetscReal *f)
{
  PetscReal f_im1,f_i,f_ip1,dx;
  PetscReal w1,p1,w2,p2;
  PetscReal C_1,C_2,IS_1,IS_2,alpha_1,alpha_2;
  
  f_im1 = fs[0];
  f_i   = fs[1];
  f_ip1 = fs[2];
  
  /* linear function between x_i-1 and x_i */
  dx = xs[1] - xs[0];
  p1 = f_im1 + (f_i - f_im1)*(x - xs[0])/dx;
  /* linear function between x_i and x_i+1 */
  dx = xs[2] - xs[1];
  p2 = f_i + (f_ip1 - f_i)*(x - xs[1])/dx;
  
  C_1 = 1.0/3.0;
  C_2 = 2.0/3.0;
  
  IS_1 = (f_i - f_im1)*(f_i - f_im1);
  IS_2 = (f_ip1 - f_i)*(f_ip1 - f_i);
  
  alpha_1 = C_1 / ((1.0e-6 + IS_1)*(1.0e-6 + IS_1));
  alpha_2 = C_2 / ((1.0e-6 + IS_2)*(1.0e-6 + IS_2));
  
  w1 = alpha_1/(alpha_1 + alpha_2);
  w2 = alpha_2/(alpha_1 + alpha_2);
  *f = w1*p1 + w2*p2;
}

/*
 [input]
 xd[] : single departure point
 h[] : array of all heights
 [output]
 hd : interpolated height at departure point
 */
/* interpolate in y first, then x : arbitrary choice */
static void SLInterpolateAtPoint_CSpline(CSLPoly s,const PetscReal xd[],PetscInt nfields,const PetscReal phi[],PetscReal phi_M[])
{
  PetscInt ci,cj,f;
  PetscInt ni[3],nj[3],kk;
  PetscReal yj[3],xj[3];
  PetscReal invAx[3][3],invAy[3][3],hinterp[3],hj[3];
  
  
  /* determine cell containing poPetscInt */
  ci = (PetscInt)( (xd[0] - s->gmin[0])/s->dx[0] );
  cj = (PetscInt)( (xd[1] - s->gmin[1])/s->dx[1] );
  if (ci == s->mx[0]) ci--;
  if (cj == s->mx[1]) cj--;
  
  /* determine 3 nj values to use for cspline interpolation */
  nj[0] = cj-1;
  nj[1] = cj;
  nj[2] = cj+1;
  
  if (nj[0] < 0) {
    nj[0] = 0;
    nj[1] = 1;
    nj[2] = 2;
  } else if (nj[2] >= s->mx[1]) {
    nj[0] = s->mx[1]-3;
    nj[1] = s->mx[1]-2;
    nj[2] = s->mx[1]-1;
  }
  
  /* determine 3 ni values to use for cspline interpolation */
  ni[0] = ci-1;
  ni[1] = ci;
  ni[2] = ci+1;
  
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
  } else if (ni[2] >= s->mx[0]) {
    ni[0] = s->mx[0]-3;
    ni[1] = s->mx[0]-2;
    ni[2] = s->mx[0]-1;
  }
  
  for (kk=0; kk<3; kk++) {
    yj[kk] = s->gmin[1] + nj[kk] * s->dx[1] + 0.5 * s->dx[1];
    xj[kk] = s->gmin[0] + ni[kk] * s->dx[0] + 0.5 * s->dx[0];
  }
  
  if (xd[0] > (xj[2]+CSLPOLY_EPS)) { printf("xd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  if (xd[0] < (xj[0]-CSLPOLY_EPS)) { printf("xd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  
  if (xd[1] > (yj[2]+CSLPOLY_EPS)) { printf("yd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  if (xd[1] < (yj[0]-CSLPOLY_EPS)) { printf("yd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  
  
  /* interpolate three times to determin the points in the y direction */
  CubicSpline_PrepareBasis(yj,invAy);
  CubicSpline_PrepareBasis(xj,invAx);

  for (f=0; f<nfields; f++) {

    for (kk=0; kk<3; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[0],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    CubicSpline_Interpolate(xd[1],yj,invAy,hj,&hinterp[0]);
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[1],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    CubicSpline_Interpolate(xd[1],yj,invAy,hj,&hinterp[1]);
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[2],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    CubicSpline_Interpolate(xd[1],yj,invAy,hj,&hinterp[2]);;
    
    /* interpolate once in the x direction */
    CubicSpline_Interpolate(xd[0],xj,invAx,hinterp,&phi_M[f]);
  }
  
}

void SLInterpolateAtPoint_CSplineBounds(CSLPoly s,const PetscReal xd[],PetscInt nfields,const PetscReal phi[],PetscReal phi_min[],PetscReal phi_max[],PetscReal phi_M[])
{
  PetscInt ci,cj,f;
  PetscInt ni[3],nj[3],kk;
  PetscReal yj[3],xj[3];
  PetscReal invAx[3][3],invAy[3][3],hinterp[3],hj[3];
  
  
  /* determine cell containing poPetscInt */
  ci = (PetscInt)( (xd[0] - s->gmin[0])/s->dx[0] );
  cj = (PetscInt)( (xd[1] - s->gmin[1])/s->dx[1] );
  if (ci == s->mx[0]) ci--;
  if (cj == s->mx[1]) cj--;
  
  /* determine 3 nj values to use for cspline interpolation */
  nj[0] = cj-1;
  nj[1] = cj;
  nj[2] = cj+1;
  
  if (nj[0] < 0) {
    nj[0] = 0;
    nj[1] = 1;
    nj[2] = 2;
  } else if (nj[2] >= s->mx[1]) {
    nj[0] = s->mx[1]-3;
    nj[1] = s->mx[1]-2;
    nj[2] = s->mx[1]-1;
  }
  
  /* determine 3 ni values to use for cspline interpolation */
  ni[0] = ci-1;
  ni[1] = ci;
  ni[2] = ci+1;
  
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
  } else if (ni[2] >= s->mx[0]) {
    ni[0] = s->mx[0]-3;
    ni[1] = s->mx[0]-2;
    ni[2] = s->mx[0]-1;
  }
  
  for (kk=0; kk<3; kk++) {
    yj[kk] = s->gmin[1] + nj[kk] * s->dx[1] + 0.5 * s->dx[1];
    xj[kk] = s->gmin[0] + ni[kk] * s->dx[0] + 0.5 * s->dx[0];
  }
  
  if (xd[0] > (xj[2]+CSLPOLY_EPS)) { printf("error[spline]: xd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  if (xd[0] < (xj[0]-CSLPOLY_EPS)) { printf("error[spline]: xd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  
  if (xd[1] > (yj[2]+CSLPOLY_EPS)) { printf("error[spline]: yd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  if (xd[1] < (yj[0]-CSLPOLY_EPS)) { printf("error[spline]: yd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  
  
  /* interpolate three times to determin the points in the y direction */
  CubicSpline_PrepareBasis(yj,invAy);
  CubicSpline_PrepareBasis(xj,invAx);
  
  for (f=0; f<nfields; f++) {
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[0],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
      
      if (hj[kk] > phi_max[f]) { phi_max[f] = hj[kk]; }
      if (hj[kk] < phi_min[f]) { phi_min[f] = hj[kk]; }
    }
    CubicSpline_Interpolate(xd[1],yj,invAy,hj,&hinterp[0]);
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[1],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
      
      if (hj[kk] > phi_max[f]) { phi_max[f] = hj[kk]; }
      if (hj[kk] < phi_min[f]) { phi_min[f] = hj[kk]; }
    }
    CubicSpline_Interpolate(xd[1],yj,invAy,hj,&hinterp[1]);
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[2],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
      
      if (hj[kk] > phi_max[f]) { phi_max[f] = hj[kk]; }
      if (hj[kk] < phi_min[f]) { phi_min[f] = hj[kk]; }
    }
    CubicSpline_Interpolate(xd[1],yj,invAy,hj,&hinterp[2]);;
    
    /* interpolate once in the x direction */
    CubicSpline_Interpolate(xd[0],xj,invAx,hinterp,&phi_M[f]);
  }
  
}

static void SLInterpolateAtPoint_CSplineQuasiMonotone(CSLPoly s,const PetscReal xd[],PetscInt nfields,const PetscReal phi_cell[],PetscReal phi_M[])
{
  PetscInt f,ci,cj,cni,cnj,cell_neighbour;
  PetscReal phi_n,phi_H[CSLPOLY_MAX_FIELDS],phi_minus[CSLPOLY_MAX_FIELDS],phi_plus[CSLPOLY_MAX_FIELDS];
  
  /* determine cell containing poPetscInt */
  ci = (PetscInt)( (xd[0] - s->gmin[0])/s->dx[0] );
  cj = (PetscInt)( (xd[1] - s->gmin[1])/s->dx[1] );
  if (ci == s->mx[0]) ci--;
  if (cj == s->mx[1]) cj--;
  
  /* get bounds from 8 neighbours */
  for (f=0; f<nfields; f++) {
    phi_minus[f] = 1.0e20;
    phi_plus[f]  = -1.0e20;
  }
  
  for (cnj=cj-1; cnj<=cj+1; cnj++) {
    if (cnj < 0) continue;
    if (cnj > (s->mx[1]-1)) continue;
    
    for (cni=ci-1; cni<=ci+1; cni++) {
      if (cni < 0) continue;
      if (cni > (s->mx[0]-1)) continue;
      
      cell_neighbour = CSLCellIdx(s,cni,cnj);
      
      for (f=0; f<nfields; f++) {
        phi_n = phi_cell[ nfields * cell_neighbour + f];
        
        if (phi_n > phi_plus[f])  { phi_plus[f] = phi_n; }
        if (phi_n < phi_minus[f]) { phi_minus[f] = phi_n; }
      }
    }
  }
  
  SLInterpolateAtPoint_CSpline(s,xd,nfields,phi_cell,phi_H);

  for (f=0; f<nfields; f++) {
    if (phi_H[f] > phi_plus[f]) {
      phi_M[f] = phi_plus[f];
    } else if (phi_H[f] < phi_minus[f]) {
      phi_M[f] = phi_minus[f];
    } else {
      phi_M[f] = phi_H[f];
    }
  }
}

static void SLInterpolateAtPoint_CSplineQuasiMonotoneBounds(CSLPoly s,const PetscReal xd[],PetscInt nfields,const PetscReal phi_cell[],PetscReal phi_M[])
{
  PetscInt f;
  PetscReal phi_H[CSLPOLY_MAX_FIELDS],phi_minus[CSLPOLY_MAX_FIELDS],phi_plus[CSLPOLY_MAX_FIELDS];
  
  /* initialize */
  for (f=0; f<nfields; f++) {
    phi_minus[f] = 1.0e20;
    phi_plus[f]  = -1.0e20;
  }
  
  SLInterpolateAtPoint_CSplineBounds(s,xd,nfields,phi_cell,phi_minus,phi_plus,phi_H);
  
  for (f=0; f<nfields; f++) {
    if (phi_H[f] > phi_plus[f]) {
      phi_M[f] = phi_plus[f];
    } else if (phi_H[f] < phi_minus[f]) {
      phi_M[f] = phi_minus[f];
    } else {
      phi_M[f] = phi_H[f];
    }
  }
}

static void SLInterpolateAtPoint_W4(CSLPoly s,const PetscReal xd[],PetscInt nfields,const PetscReal phi[],PetscReal phi_M[])
{
  PetscInt ci,cj,cidx,f;
  PetscInt ni[4],nj[4],kk;
  PetscReal yj[4],xj[4];
  PetscReal hinterp[4],hj[4];
  CellP cell;
  
  /* determine cell containing poPetscInt */
  ci = (PetscInt)( (xd[0] - s->gmin[0])/s->dx[0] );
  cj = (PetscInt)( (xd[1] - s->gmin[1])/s->dx[1] );
  if (ci == s->mx[0]) ci--;
  if (cj == s->mx[1]) cj--;
  
  cidx = CSLCellIdx(s,ci,cj);
  cell = s->cell_list[cidx];
  
  /*
   
         i-2       i-1        i        i+1
     |----o----|----o----|----o----|----o----|
   
   
                   i-1        i        i+1       i+2
               |----o----|----o----|----o----|----o----|
  */
  /* determine 4 nj values to use for weno */
  if (xd[1] < cell->coor[1]) {
    nj[0] = cj-2;
    nj[1] = cj-1;
    nj[2] = cj;
    nj[3] = cj+1;
  } else {
    nj[0] = cj-1;
    nj[1] = cj;
    nj[2] = cj+1;
    nj[3] = cj+2;
  }

  if (nj[0] < 0) {
    printf("error[weno4] nj[-] out of range\n");
    exit(1);
  } else if (nj[3] >= s->mx[1]) {
    printf("error[weno4] nj[+] out of range\n");
    exit(1);
  }
  
  /* determine 4 ni values to use for weno */
  if (xd[0] < cell->coor[0]) {
    ni[0] = ci-2;
    ni[1] = ci-1;
    ni[2] = ci;
    ni[3] = ci+1;
  } else {
    ni[0] = ci-1;
    ni[1] = ci;
    ni[2] = ci+1;
    ni[3] = ci+2;
  }
  
  if (ni[0] < 0) {
    printf("error[weno4]: ni[-] out of range\n");
    exit(1);
  } else if (ni[3] >= s->mx[0]) {
    printf("error[weno4]: ni[+] out of range\n");
    exit(1);
  }
  
  for (kk=0; kk<4; kk++) {
    xj[kk] = s->gmin[0] + ni[kk] * s->dx[0] + 0.5 * s->dx[0];
    yj[kk] = s->gmin[1] + nj[kk] * s->dx[1] + 0.5 * s->dx[1];
  }
  
  if (xd[0] > (xj[2]+CSLPOLY_EPS)) { printf("error[weno4]: xd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  if (xd[0] < (xj[0]-CSLPOLY_EPS)) { printf("error[weno4]: xd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],xj[0],xj[1],xj[2],ni[0],ni[1],ni[2]); exit(0); }
  
  if (xd[1] > (yj[2]+CSLPOLY_EPS)) { printf("error[weno4]: yd>2 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  if (xd[1] < (yj[0]-CSLPOLY_EPS)) { printf("error[weno4]: yd<0 (%1.6e,%1.6e): %1.6e %1.6e %1.6e - %d %d %d\n",xd[0],xd[1],yj[0],yj[1],yj[2],nj[0],nj[1],nj[2]); exit(0); }
  
  
  /* interpolate three times to determin the points in the y direction */
  
  for (f=0; f<nfields; f++) {
    
    for (kk=0; kk<4; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[0],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    weno4_McD(yj,hj,xd[1],&hinterp[0]);
    
    for (kk=0; kk<4; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[1],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    weno4_McD(yj,hj,xd[1],&hinterp[1]);
    
    for (kk=0; kk<4; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[2],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    weno4_McD(yj,hj,xd[1],&hinterp[2]);

    for (kk=0; kk<4; kk++) {
      PetscInt nidx = CSLCellIdx(s,ni[3],nj[kk]);
      
      hj[kk] = phi[nfields*nidx+f];
    }
    weno4_McD(yj,hj,xd[1],&hinterp[3]);

    /* interpolate once in the x direction */
    weno4_McD(xj,hinterp,xd[0],&phi_M[f]);
  }
  
}

//static void SLInterpolateAtPoint_W4(CSLPoly s,const PetscReal xd[],PetscInt nfields,const PetscReal phi[],PetscReal phi_M[])

void SLInterpolateAtPoint_WENO3(CSLPoly s,PetscInt celli,PetscInt cellj,const PetscReal xd[],PetscInt nfields,const PetscReal phi[],PetscReal phi_M[])
{
  PetscReal xs[3],ys[3],fs[3],fsx[3];
  PetscInt pidi[3],pidj[3],k,f;
  
  pidi[0] = celli-1;
  pidi[1] = celli;
  pidi[2] = celli+1;
  
  /* adjust if near end points */
  if (pidi[0] <= 0) {
    pidi[0] = 0;
    pidi[1] = 1;
    pidi[2] = 2;
  }
  if (pidi[2] >= (s->mx[0]-1)) {
    pidi[0] = s->mx[0]-3;
    pidi[1] = s->mx[0]-2;
    pidi[2] = s->mx[0]-1;
  }
  
  pidj[0] = cellj-1;
  pidj[1] = cellj;
  pidj[2] = cellj+1;
  
  /* adjust if near end points */
  if (pidj[0] <= 0) {
    pidj[0] = 0;
    pidj[1] = 1;
    pidj[2] = 2;
  }
  if (pidj[2] >= (s->mx[1]-1)) {
    pidj[0] = s->mx[1]-3;
    pidj[1] = s->mx[1]-2;
    pidj[2] = s->mx[1]-1;
  }
  
  for (k=0; k<3; k++) {
    xs[k] = s->gmin[0] + pidi[k] * s->dx[0] + 0.5 * s->dx[0];
    ys[k] = s->gmin[1] + pidj[k] * s->dx[1] + 0.5 * s->dx[1];
  }
  
  
  for (f=0; f<nfields; f++) {
    // j - 1
    for (k=0; k<3; k++) {
      PetscInt idx = CSLCellIdx(s,pidi[k],pidj[0]);
      
      fs[k] = phi[ nfields*idx + f ];
    }
    weno3(xs,fs,xd[0],&fsx[0]);
    
    // j
    for (k=0; k<3; k++) {
      PetscInt idx = CSLCellIdx(s,pidi[k],pidj[1]);
      
      fs[k] = phi[ nfields*idx + f ];
    }
    weno3(xs,fs,xd[0],&fsx[1]);
    
    // j + 1
    for (k=0; k<3; k++) {
      PetscInt idx = CSLCellIdx(s,pidi[k],pidj[2]);
      
      fs[k] = phi[ nfields*idx + f ];
    }
    weno3(xs,fs,xd[0],&fsx[2]);
    
    // inteprolate in y
    weno3(ys,fsx,xd[1],&phi_M[f]);
  }
}

void CSLPolyInterpolateAtPoint(CSLPoly s,PetscReal coor[],PetscReal phi_p[])
{
  PetscInt inside,f;
  
  point_in_polygon(s->poly_npoints,s->poly_coor,coor,&inside);
  if (inside == 1) {
    switch (s->interp_type) {
      case I_CSPLINE:
        SLInterpolateAtPoint_CSpline(s,coor,s->nfields,s->value_c,phi_p);
        break;
        
      case I_CSPLINE_MONOTONE:
        SLInterpolateAtPoint_CSplineQuasiMonotone(s,coor,s->nfields,s->value_c,phi_p);
        break;

      case I_CSPLINE_MONOTONE_BOUNDS:
        SLInterpolateAtPoint_CSplineQuasiMonotoneBounds(s,coor,s->nfields,s->value_c,phi_p);
        break;

      case I_WENO4:
        SLInterpolateAtPoint_W4(s,coor,s->nfields,s->value_c,phi_p);
        break;

      case I_WENO3:
      {
        PetscInt ci,cj;
        
        ci = (PetscInt)( (coor[0] - s->gmin[0])/s->dx[0] );
        cj = (PetscInt)( (coor[1] - s->gmin[1])/s->dx[1] );
        if (ci == s->mx[0]) ci--;
        if (cj == s->mx[1]) cj--;
        
        SLInterpolateAtPoint_WENO3(s,ci,cj,coor,s->nfields,s->value_c,phi_p);
      }
        break;

      default:
        printf("error: unknown interpolation type specified\n");
        exit(1);
        break;
    }
    
    if (s->impose_global_value_bounds == 1) {
      for (f=0; f<s->nfields; f++) {
        if (phi_p[f] > s->value_bound_max[f]) {
          phi_p[f] = s->value_bound_max[f];
        } else if (phi_p[f] < s->value_bound_min[f]) {
          phi_p[f] = s->value_bound_min[f];
        }
      }
    }
    
  } else {
    for (f=0; f<s->nfields; f++) {
      phi_p[f] = nan(nan_tagp);
    }
    printf("error[interpolate_at_point]: poPetscInt outside domain\n");
    printf("  coor %+1.4e %+1.4e\n",coor[0],coor[1]);
    exit(10);
  }
}

void CSLPolyGetPointLocationStatus(CSLPoly s,
                                  const PetscReal coor[],
                                  PetscInt *inside_domain,
                                  PetscInt *inside_csl_domain)
{
  PetscInt inside_poly,inside_box;
  
  inside_poly = 0;
  inside_box = 1;
  point_in_polygon(s->poly_npoints,s->poly_coor,coor,&inside_poly);
  
  /* if not inside the polygon, check if coor[] is at least inside the CSL domain */
  if (inside_poly == 0) {
    inside_box = 1;
    if (coor[0] < s->gmin_input[0]) { inside_box = 0; }
    if (coor[1] < s->gmin_input[1]) { inside_box = 0; }
    if (coor[0] > s->gmax_input[0]) { inside_box = 0; }
    if (coor[1] > s->gmax_input[1]) { inside_box = 0; }
  }
  
  /* if not inside the box - return */
  if (inside_box == 0) {
    *inside_domain     = 0;
    *inside_csl_domain = 0;
    return;
  }
  
  if (inside_box == 1) {
    *inside_domain     = inside_poly;
    *inside_csl_domain = 1;
  }
}

void CSLPolyInterpolateAtPointWithArray(CSLPoly s,InterpolationType interp_type,
                                   const PetscReal coor[],
                                   PetscInt nfields,const PetscReal field_c[],
                                   const PetscReal bounds_min[],const PetscReal bounds_max[],
                                   PetscReal field_p[],PetscInt *inside_domain,PetscInt *inside_csl_domain)
{
  PetscInt inside_poly,inside_box,f;
  
  inside_poly = 0;
  inside_box = 1;
  point_in_polygon(s->poly_npoints,s->poly_coor,coor,&inside_poly);

  /* if not inside the polygon, check if coor[] is at least inside the CSL domain */
  if (inside_poly == 0) {
    inside_box = 1;
    if (coor[0] < s->gmin_input[0]) { inside_box = 0; }
    if (coor[1] < s->gmin_input[1]) { inside_box = 0; }
    if (coor[0] > s->gmax_input[0]) { inside_box = 0; }
    if (coor[1] > s->gmax_input[1]) { inside_box = 0; }
  }
  
  /* if not inside the box - return */
  if (inside_box == 0) {
    *inside_domain     = inside_poly;
    *inside_csl_domain = 0;
    return;
  }
  
  /*
   - Interpolate to poPetscInt if its inside the box.
     It is not a requirement that coor[] live inside the polygon.
   - I advocate this choice as the point_in_poly() method will often return 0 when
     points live exactly on the vertices and or edges of the polygon.
  */
  if (inside_box == 1) {
    *inside_domain     = inside_poly;
    *inside_csl_domain = 1;
    
    for (f=0; f<nfields; f++) {
      field_p[f] = 0.0;
    }

    switch (interp_type) {
      case I_CSPLINE:
        SLInterpolateAtPoint_CSpline(s,coor,nfields,field_c,field_p);
        break;
        
      case I_CSPLINE_MONOTONE:
        SLInterpolateAtPoint_CSplineQuasiMonotone(s,coor,nfields,field_c,field_p);
        break;
        
      case I_CSPLINE_MONOTONE_BOUNDS:
        SLInterpolateAtPoint_CSplineQuasiMonotoneBounds(s,coor,nfields,field_c,field_p);
        break;
        
      case I_WENO4:
        SLInterpolateAtPoint_W4(s,coor,nfields,field_c,field_p);
        break;

      case I_WENO3:
      {
        PetscInt ci,cj;
        
        ci = (PetscInt)( (coor[0] - s->gmin[0])/s->dx[0] );
        cj = (PetscInt)( (coor[1] - s->gmin[1])/s->dx[1] );
        if (ci == s->mx[0]) ci--;
        if (cj == s->mx[1]) cj--;
        
        SLInterpolateAtPoint_WENO3(s,ci,cj,coor,nfields,field_c,field_p);
      }
        break;

      default:
        printf("error: unknown interpolation type specified\n");
        exit(1);
        break;
    }
    
    if (bounds_min) {
      for (f=0; f<nfields; f++) {
        if (field_p[f] < bounds_min[f]) {
          field_p[f] = bounds_min[f];
        }
      }
    }
    if (bounds_max) {
      for (f=0; f<nfields; f++) {
        if (field_p[f] > bounds_max[f]) {
          field_p[f] = bounds_max[f];
        }
      }
    }
  }
}

/* Boundary interpolation methods */

static void SLInterpolateAtBoundaryPoint_CSplineBounds(CSLPoly s,PetscInt poly_facet,PetscReal xd[],PetscReal phi_min[],PetscReal phi_max[],PetscReal phi_M[])
{
  PetscInt cis,f;
  PetscInt ni[3],kk;
  PetscReal xj[3],dx,dy,ds,L;
  PetscReal invAx[3][3],hj[3];
  PetscInt offset,nsegments;
  
  offset    = s->boundary_facet_offset[poly_facet];
  nsegments = s->boundary_facet_offset[poly_facet+1] - offset;
  ds = s->boundary_ds[poly_facet];
  
  /* determine facet containing poPetscInt */
  dx = xd[0] - s->poly_coor[2*poly_facet+0];
  dy = xd[1] - s->poly_coor[2*poly_facet+1];
  L = PetscSqrtReal(dx*dx+dy*dy);
  
  cis = (PetscInt)( L / ds );
  if (cis == nsegments) cis--;

  if (cis < 0) {
    printf("error[SLInterpolateAtBoundaryPoint_CSplineBounds]: cis < 0\n");
    exit(1);
  }
  if (cis >= nsegments) {
    printf("error[SLInterpolateAtBoundaryPoint_CSplineBounds]: cis >= nsegments\n");
    exit(1);
  }
  
  /* determine 3 values to use for cspline interpolation */
  ni[0] = cis-1;
  ni[1] = cis;
  ni[2] = cis+1;
  
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
  } else if (ni[2] >= s->mx[0]) {
    ni[0] = nsegments-3;
    ni[1] = nsegments-2;
    ni[2] = nsegments-1;
  }
  
  /* use a locally defined coordinate system from 0 to length of the poly edge */
  for (kk=0; kk<3; kk++) {
    xj[kk] = 0.0 + ni[kk] * ds + 0.5 * ds;
  }
  
  CubicSpline_PrepareBasis(xj,invAx);

  for (kk=0; kk<3; kk++) {
    ni[kk] += offset;
  }

  for (f=0; f<s->nfields; f++) {
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = ni[kk];
      
      hj[kk] = s->boundary_facet_list[nidx]->value[f];
      
      if (hj[kk] > phi_max[f]) { phi_max[f] = hj[kk]; }
      if (hj[kk] < phi_min[f]) { phi_min[f] = hj[kk]; }
    }
    CubicSpline_Interpolate(L,xj,invAx,hj,&phi_M[f]);
  }
}

static void SLInterpolateAtBoundaryPoint_CSplineQuasiMonotoneBounds(CSLPoly s,PetscInt poly_facet,PetscReal xd[],PetscReal phi_M[])
{
  PetscInt f;
  PetscReal phi_H[CSLPOLY_MAX_FIELDS],phi_minus[CSLPOLY_MAX_FIELDS],phi_plus[CSLPOLY_MAX_FIELDS];
  
  /* initialize */
  for (f=0; f<s->nfields; f++) {
    phi_minus[f] = 1.0e20;
    phi_plus[f]  = -1.0e20;
  }
  
  SLInterpolateAtBoundaryPoint_CSplineBounds(s,poly_facet,xd,phi_minus,phi_plus,phi_H);
  /*
  for (f=0; f<s->nfields; f++) {
    phi_M[f] = phi_H[f];
  }
  */
  for (f=0; f<s->nfields; f++) {
    if (phi_H[f] > phi_plus[f]) {
      phi_M[f] = phi_plus[f];
    } else if (phi_H[f] < phi_minus[f]) {
      phi_M[f] = phi_minus[f];
    } else {
      phi_M[f] = phi_H[f];
    }
  }
}

static void SLInterpolateAtBoundaryPoint_CSpline(CSLPoly s,PetscInt poly_facet,PetscReal xd[],PetscReal phi_M[])
{
  PetscInt f;
  PetscReal phi_minus[CSLPOLY_MAX_FIELDS],phi_plus[CSLPOLY_MAX_FIELDS];
  
  /* initialize */
  for (f=0; f<s->nfields; f++) {
    phi_minus[f] = 1.0e20;
    phi_plus[f]  = -1.0e20;
  }
  
  /* ignore / don't use the bounds returned */
  SLInterpolateAtBoundaryPoint_CSplineBounds(s,poly_facet,xd,phi_minus,phi_plus,phi_M);
}

static void SLInterpolateAtBoundaryPoint_W4(CSLPoly s,PetscInt poly_facet,PetscReal xd[],PetscReal phi_M[])
{
  PetscInt cis,f;
  PetscInt ni[4],kk;
  PetscReal hj[4],xj[4],dx,dy,ds,L,Lf;
  PetscInt offset,nsegments;
  
  offset    = s->boundary_facet_offset[poly_facet];
  nsegments = s->boundary_facet_offset[poly_facet+1] - offset;
  ds = s->boundary_ds[poly_facet];
  
  /* determine facet containing poPetscInt */
  dx = xd[0] - s->poly_coor[2*poly_facet+0];
  dy = xd[1] - s->poly_coor[2*poly_facet+1];
  L = PetscSqrtReal(dx*dx+dy*dy); /* local edge coordinate to interpolation poPetscInt */
  
  cis = (PetscInt)( L / ds );
  if (cis == nsegments) cis--;
  
  if (cis < 0) {
    printf("error[SLInterpolateAtBoundaryPoint_W4]: cis < 0\n");
    exit(1);
  }
  if (cis >= nsegments) {
    printf("error[SLInterpolateAtBoundaryPoint_W4]: cis >= nsegments\n");
    exit(1);
  }

  dx = s->boundary_facet_list[cis]->coor[0] - s->poly_coor[2*poly_facet+0];
  dy = s->boundary_facet_list[cis]->coor[1] - s->poly_coor[2*poly_facet+1];
  Lf = PetscSqrtReal(dx*dx+dy*dy); /* local edge coordinate to facet centroid poPetscInt */
  
  /* determine 4 values to use for weno interpolation */
  if (L < Lf) {
    ni[0] = cis-2;
    ni[1] = cis-1;
    ni[2] = cis;
    ni[3] = cis+1;
  } else {
    ni[0] = cis-1;
    ni[1] = cis;
    ni[2] = cis+1;
    ni[3] = cis+2;
  }
  /* change stencil if end points hang outside the valid range */
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
    ni[3] = 3;
  } else if (ni[3] > (nsegments-1)) {
    ni[0] = nsegments-4;
    ni[1] = nsegments-3;
    ni[2] = nsegments-2;
    ni[3] = nsegments-1;
  }
  
  /* use a locally defined coordinate system from 0 to length of the poly edge */
  for (kk=0; kk<4; kk++) {
    xj[kk] = 0.0 + ni[kk] * ds + 0.5 * ds;
  }
  
  for (kk=0; kk<4; kk++) {
    ni[kk] += offset;
  }
  
  for (f=0; f<s->nfields; f++) {
    
    for (kk=0; kk<4; kk++) {
      PetscInt nidx = ni[kk];
      
      hj[kk] = s->boundary_facet_list[nidx]->value[f];
    }
    weno4_McD(xj,hj,L,&phi_M[f]);
  }
}

static void SLInterpolateAtBoundaryPoint_WENO3(CSLPoly s,PetscInt poly_facet,PetscReal xd[],PetscReal phi_M[])
{
  PetscInt cis,f;
  PetscInt ni[3],kk;
  PetscReal hj[3],xj[3],dx,dy,ds,L;
  PetscInt offset,nsegments;
  
  offset    = s->boundary_facet_offset[poly_facet];
  nsegments = s->boundary_facet_offset[poly_facet+1] - offset;
  ds = s->boundary_ds[poly_facet];
  
  /* determine facet containing poPetscInt */
  dx = xd[0] - s->poly_coor[2*poly_facet+0];
  dy = xd[1] - s->poly_coor[2*poly_facet+1];
  L = PetscSqrtReal(dx*dx+dy*dy); /* local edge coordinate to interpolation poPetscInt */
  
  cis = (PetscInt)( L / ds );
  if (cis == nsegments) cis--;
  
  if (cis < 0) {
    printf("error[SLInterpolateAtBoundaryPoint_WENO3]: cis < 0\n");
    exit(1);
  }
  if (cis >= nsegments) {
    printf("error[SLInterpolateAtBoundaryPoint_WENO3]: cis >= nsegments\n");
    exit(1);
  }
  
  dx = s->boundary_facet_list[cis]->coor[0] - s->poly_coor[2*poly_facet+0];
  dy = s->boundary_facet_list[cis]->coor[1] - s->poly_coor[2*poly_facet+1];
  
  /* determine 3 values to use for weno interpolation */
  ni[0] = cis-1;
  ni[1] = cis;
  ni[2] = cis+1;

  /* change stencil if end points hang outside the valid range */
  if (ni[0] < 0) {
    ni[0] = 0;
    ni[1] = 1;
    ni[2] = 2;
  } else if (ni[2] > (nsegments-1)) {
    ni[0] = nsegments-3;
    ni[1] = nsegments-2;
    ni[2] = nsegments-1;
  }
  
  /* use a locally defined coordinate system from 0 to length of the poly edge */
  for (kk=0; kk<3; kk++) {
    xj[kk] = 0.0 + ni[kk] * ds + 0.5 * ds;
  }
  
  for (kk=0; kk<3; kk++) {
    ni[kk] += offset;
  }
  
  for (f=0; f<s->nfields; f++) {
    
    for (kk=0; kk<3; kk++) {
      PetscInt nidx = ni[kk];
      
      hj[kk] = s->boundary_facet_list[nidx]->value[f];
    }
    weno3(xj,hj,L,&phi_M[f]);
  }
}

void CSLPolyInterpolateAtBoundaryPoint(CSLPoly s,PetscInt poly_facet,PetscReal coor[],PetscReal phi_p[])
{
  PetscInt f;
  
  switch (s->interp_type) {
    case I_CSPLINE:
      SLInterpolateAtBoundaryPoint_CSpline(s,poly_facet,coor,phi_p);
      break;
      
    case I_CSPLINE_MONOTONE:
      SLInterpolateAtBoundaryPoint_CSplineQuasiMonotoneBounds(s,poly_facet,coor,phi_p);
      break;
      
    case I_CSPLINE_MONOTONE_BOUNDS:
      SLInterpolateAtBoundaryPoint_CSplineQuasiMonotoneBounds(s,poly_facet,coor,phi_p);
      break;
      
    case I_WENO4:
      SLInterpolateAtBoundaryPoint_W4(s,poly_facet,coor,phi_p);
      break;
      
    case I_WENO3:
      SLInterpolateAtBoundaryPoint_WENO3(s,poly_facet,coor,phi_p);
      break;
      
    default:
      printf("error: unknown interpolation type specified\n");
      exit(1);
      break;
  }
  
  if (s->impose_global_value_bounds == 1) {
    for (f=0; f<s->nfields; f++) {
      if (phi_p[f] > s->value_bound_max[f]) {
        phi_p[f] = s->value_bound_max[f];
      } else if (phi_p[f] < s->value_bound_min[f]) {
        phi_p[f] = s->value_bound_min[f];
      }
    }
  }
}

void CSLPolyCreate(CSLPoly *poly)
{
  CSLPoly p;
  PetscInt f;
  int commsize;
  
  MPI_Comm_size(PETSC_COMM_WORLD,&commsize);
  if (commsize != 1) {
    printf("[CSLPolyCreate] ERROR: Currently this is only supported for comm.size = 1");
    MPI_Abort(PETSC_COMM_WORLD,1);
  }
  
  p = (CSLPoly)malloc(sizeof(struct _p_CSLPoly));
  memset(p,0,sizeof(struct _p_CSLPoly));
  
  p->poly_npoints = 0;
  p->poly_coor = NULL;
  p->poly_edge_label = NULL;
  
  p->boundary_nfacets = 0;
  p->boundary_facet_list = NULL;
  p->boundary_facet_offset = NULL;
  p->boundary_ds = NULL;
  
  p->ds = 0.0;
  p->dx[0] = 0.0;
  p->dx[1] = 0.0;
  
  p->subcellmx[0] = 1;
  p->subcellmx[1] = 1;
  
  p->nfields = 0;
  p->nimplicitfields = 0;
  
  p->cell_list = NULL;
  p->c = NULL;
  
  p->interp_type = I_CSPLINE_MONOTONE;
  
  p->impose_global_value_bounds = 0;
  for (f=0; f<CSLPOLY_MAX_FIELDS; f++) {
    p->value_bound_min[f] = -1.0e32;
    p->value_bound_max[f] = 1.0e32;
  }
  
  p->compute_div_u = NULL;
  
  p->value_c = NULL;
  p->ivalue_c = NULL;
  p->divu_c = NULL;
  p->ictx = NULL;
  p->ifields_evaluate_at_point = NULL;
  p->ifields_evaluate_at_boundarypoint = NULL;
  
  *poly = p;
}

void CSLPolySetInterpolateType(CSLPoly p,InterpolationType t)
{
  p->interp_type = t;
}

void CSLPolySetBlockSize(CSLPoly p,PetscInt nfields)
{
  p->nfields = nfields;
  if (nfields >= CSLPOLY_MAX_FIELDS) {
    printf("error: only permitted to have 20 fields\n");
    exit(1);
  }
}

void CSLPolySetBlockSizeImplicit(CSLPoly p,PetscInt nfields)
{
  p->nimplicitfields = nfields;
  if (nfields >= CSLPOLY_MAX_FIELDS) {
    printf("error: only permitted to have 20 implicit fields\n");
    exit(1);
  }
}

void CSLPolySetImplicitFieldEvaluators(CSLPoly p,
                                       void (*ifields_evaluate_at_point)(CSLPoly,PetscReal*,PetscReal*,void*),
                                       void (*ifields_evaluate_at_boundarypoint)(CSLPoly,PetscInt,PetscReal*,PetscReal*,void*),
                                       void *ictx)
{
  p->ictx = ictx;
  p->ifields_evaluate_at_point = ifields_evaluate_at_point;
  p->ifields_evaluate_at_boundarypoint = ifields_evaluate_at_boundarypoint;
}

void CSLPolySetApplyGlobalBounds(CSLPoly p)
{
  p->impose_global_value_bounds = 1;
}

void CSLPolySetGlobalBounds(CSLPoly p,PetscInt f,PetscReal min,PetscReal max)
{
  p->value_bound_min[f] = min;
  p->value_bound_max[f] = max;
}

/*
 points should be orieneted counter clock-wise
 points should be unique
 copies input points
 frees any previously set points
*/
void CSLPolySetPolygonPoints(CSLPoly p,PetscInt npoints,PetscReal xy[],PetscInt edge_label[])
{
  if (p->poly_coor) { free(p->poly_coor); }
  if (p->poly_edge_label) { free(p->poly_edge_label); }
  p->poly_npoints = npoints;

  p->poly_coor = (PetscReal*)malloc(sizeof(PetscReal)*npoints*2);
  memcpy(p->poly_coor,xy,sizeof(PetscReal)*npoints*2);
  
  p->poly_edge_label = (PetscInt*)malloc(sizeof(PetscInt)*npoints);
  if (edge_label) {
    memcpy(p->poly_edge_label,edge_label,sizeof(PetscInt)*npoints);
  } else {
    memset(p->poly_edge_label,0,sizeof(PetscInt)*npoints);
  }
}

void CSLPolySetDomain(CSLPoly p,PetscReal gmin[],PetscReal gmax[],PetscReal ds)
{
  PetscInt d;
  
  p->ds = ds;
  for (d=0; d<2; d++) {
    p->gmin_input[d] = gmin[d];
    p->gmax_input[d] = gmax[d];
  }
}

void CSLPolySetDomainDefault(CSLPoly p,PetscReal ds)
{
  PetscInt k,d;
  PetscReal gmin[2],gmax[2],dx[2];
  
  gmin[0] = 1.0e32;
  gmin[1] = 1.0e32;
  gmax[0] = -1.0e32;
  gmax[1] = -1.0e32;
  
  for (k=0; k<p->poly_npoints; k++) {
    for (d=0; d<2; d++) {
      if (p->poly_coor[2*k+d] < gmin[d]) { gmin[d] = p->poly_coor[2*k+d]; };
      if (p->poly_coor[2*k+d] > gmax[d]) { gmax[d] = p->poly_coor[2*k+d]; };
    }
  }
  
  for (d=0; d<2; d++) {
    dx[d] = gmax[d] - gmin[d];
  }
  for (d=0; d<2; d++) {
    gmin[d] = gmin[d] - 1.0e-10 * dx[d];
    gmax[d] = gmax[d] + 1.0e-10 * dx[d];
  }
  
  CSLPolySetDomain(p,gmin,gmax,ds);
  
}

static void _CSLPolyVertexCreateInit(CSLPoly p)
{
  PetscInt i,j,k;
  
  p->coor_v = (PetscReal*)malloc(sizeof(PetscReal)*2*p->nvert);
  p->is_interior_v = (PetscInt*)malloc(sizeof(PetscInt)*p->nvert);
  p->type_v = (CoorLocationType*)malloc(sizeof(CoorLocationType)*p->nvert);
  
  for (j=0; j<p->nx[1]; j++) {
    for (i=0; i<p->nx[0]; i++) {
      k = i + j * p->nx[0];
      p->coor_v[2*k+0] = p->gmin[0] + i * p->dx[0];
      p->coor_v[2*k+1] = p->gmin[1] + j * p->dx[1];
      p->is_interior_v[k] = -1;
      p->type_v[k] = COOR_UNINIT;
    }
  }
}

static void _CSLPolyCellCreateInit(CSLPoly p)
{
  PetscInt i,j,k;
  
  p->cell_list = (CellP*)malloc(sizeof(CellP)*p->ncells);
  
  for (j=0; j<p->mx[1]; j++) {
    for (i=0; i<p->mx[0]; i++) {
      CellP cell;
      
      cell = (CellP)malloc(sizeof(struct _p_CellP));
      
      k = i + j * p->mx[0];
      cell->coor[0] = p->gmin[0] + 0.5 * p->dx[0] + i * p->dx[0];
      cell->coor[1] = p->gmin[1] + 0.5 * p->dx[1] + j * p->dx[1];

      cell->closest_facet = -1;
      cell->is_interior = -1;
      cell->type = COOR_UNINIT;
      cell->tag = TAG_UNINIT;

      p->cell_list[k] = cell;
    }
  }
}

/*
 Mark everything as COOR_DEACTIVE
 Mark everything inside as COOR_INTERIOR
 Traverse stencil of each COOR_INTERIOR cell and relabel any COOR_DEACTIVE ones as COOR_EXTERIOR
 Values in the exterior cells are required for efficient 5x5 stencil operations.
 Their values will be determined via extrapolation
*/
void CSLPolyMarkCells(CSLPoly p)
{
  PetscInt c,ci,cj;

  for (c=0; c<p->ncells; c++) {
    p->cell_list[c]->is_interior = -1;
    p->cell_list[c]->type = COOR_DEACTIVE;
  }
  
  /* mark everything as inside or outside */
  for (c=0; c<p->ncells; c++) {
    PetscInt inside;
    PetscReal *coor;
    
    coor = p->cell_list[c]->coor;
    point_in_polygon(p->poly_npoints, p->poly_coor, coor, &inside);
    p->cell_list[c]->is_interior = inside;
  }
  
  //typedef enum { COOR_INTERIOR=0, COOR_EXTERIOR, COOR_DEACTIVE, COOR_UNINIT } CoorLocationType;

  for (c=0; c<p->ncells; c++) {
    if (p->cell_list[c]->is_interior == 1) {
      p->cell_list[c]->type = COOR_INTERIOR;
    }
  }
  
  for (cj=0; cj<p->mx[1]; cj++) {
    for (ci=0; ci<p->mx[0]; ci++) {
      PetscInt ki,kj,cid;
      
      c = ci + cj * p->mx[0];
      
      /* all interior points must be -2 from the boundary */
      if (p->cell_list[c]->type == COOR_INTERIOR) {
        
        for (ki=ci-2; ki<=ci+2; ki++) {
          for (kj=cj-2; kj<=cj+2; kj++) {
            
            if (ki < 0)         { printf("ki[-] error\n"); exit(1); }
            if (ki >= p->mx[0]) { printf("ki[+] error\n"); exit(1); }
            if (kj < 0)         { printf("kj[-] error\n"); exit(1); }
            if (kj >= p->mx[1]) { printf("kj[+] error\n"); exit(1); }
            
            cid = ki + kj * p->mx[0];
            
            if (p->cell_list[cid]->type == COOR_DEACTIVE) {
              p->cell_list[cid]->type = COOR_EXTERIOR;
            }
          }
        }
      }
    }
  }
  
  p->ncells_interior = 0;
  p->ncells_exterior = 0;
  for (c=0; c<p->ncells; c++) {
    if (p->cell_list[c]->type == COOR_INTERIOR) {
      p->ncells_interior++;
    }
    if (p->cell_list[c]->type == COOR_EXTERIOR) {
      p->ncells_exterior++;
    }
  }

}

/*
 Mark all vertices as COOR_DEACTIVE
 Mark everything inside as COOR_INTERIOR
 Traverse all cells marked as COOR_INTERIOR and check their vertices
 Any vertices marked as COOR_DEACTIVE are relabelled as COOR_EXTERIOR
 Velocities will be reconstructed at those marked as COOR_EXTERIOR
*/
void CSLPolyMarkVertices(CSLPoly p)
{
  PetscInt v,ci,cj;
  
  for (v=0; v<p->nvert; v++) {
    p->type_v[v] = COOR_DEACTIVE;
  }
  
  /* mark everything as inside or outside */
  for (v=0; v<p->nvert; v++) {
    PetscInt inside;
    PetscReal *coor;
    
    coor = &p->coor_v[2*v];
    point_in_polygon(p->poly_npoints, p->poly_coor, coor, &inside);
    p->is_interior_v[v] = inside;
  }
  
  for (v=0; v<p->nvert; v++) {
    if (p->is_interior_v[v] == 1) {
      p->type_v[v] = COOR_INTERIOR;
    }
  }
  
  for (cj=0; cj<p->mx[1]; cj++) {
    for (ci=0; ci<p->mx[0]; ci++) {
      PetscInt c,vlist[4];
      
      c = ci + cj * p->mx[0];
      
      /* all interior points must be -2 from the boundary */
      if (p->cell_list[c]->type == COOR_INTERIOR) {

        
        vlist[0] = ci     + cj     * p->nx[0];
        vlist[1] = (ci+1) + cj     * p->nx[0];
        vlist[2] = (ci+1) + (cj+1) * p->nx[0];
        vlist[3] = ci     + (cj+1) * p->nx[0];
        //printf("c %d ci %d,%d : n %d %d %d %d \n",c,ci,cj,vlist[0],vlist[1],vlist[2],vlist[3]);
        
        for (v=0; v<4; v++) {
          if (p->type_v[vlist[v]] == COOR_DEACTIVE) {
            p->type_v[vlist[v]] = COOR_EXTERIOR;
          }
          
        }
        
      }
    }
  }
  
}

void CSLPolySetUpMesh(CSLPoly p)
{
  PetscInt d,mx[2];
  PetscReal dx[2];
  
  for (d=0; d<2; d++) {
    mx[d] = (PetscInt)((p->gmax_input[d] - p->gmin_input[d])/p->ds);
    //mx[d]++;
    if (mx[d] == 0) { mx[d]++; }
    dx[d] = (p->gmax_input[d] - p->gmin_input[d])/((PetscReal)(mx[d]));
    
    p->dx[d] = dx[d];
  }

  for (d=0; d<2; d++) {
    p->gmin[d] = p->gmin_input[d] - 2.0 * dx[d];
    p->gmax[d] = p->gmax_input[d] + 2.0 * dx[d];
  }
  
  for (d=0; d<2; d++) {
    p->mx[d] = mx[d] + 4; /* 2 extra on either side */
    p->nx[d] = p->mx[d] + 1;
  }

  p->ncells = p->mx[0] * p->mx[1];
  p->nvert = p->nx[0] * p->nx[1];
  
  printf("#[CSLPolySetUpMesh]\n");
  printf("#  ds %+1.12e \n",p->ds);
  printf("#  dx %+1.12e : dy %+1.12e\n",p->dx[0],p->dx[1]);
  printf("#  domain [%+1.4e , %+1.4e] x [%+1.4e , %+1.4e]\n",p->gmin_input[0],p->gmax_input[0],p->gmin_input[1],p->gmax_input[1]);
  printf("#  domain_buffer [%+1.4e , %+1.4e] x [%+1.4e , %+1.4e]\n",p->gmin[0],p->gmax[0],p->gmin[1],p->gmax[1]);
  printf("#  mx %d : my %d\n",p->mx[0],p->mx[1]);
 
  
  /* allocate and define vertices */
  _CSLPolyVertexCreateInit(p);
  
  /* allocate and initilize cells */
  _CSLPolyCellCreateInit(p);
}

void CSLPolyBoundaryPDestroy(PetscInt *nseg,BoundaryP **flist)
{
  PetscInt s,nsegments;
  BoundaryP *list;
  
  nsegments = *nseg;
  list = *flist;
  for (s=0; s<nsegments; s++) {
    if (list[s]) {
      if (list[s]->value) { free(list[s]->value); }
      if (list[s]->ivalue) { free(list[s]->ivalue); }
      free(list[s]);
      list[s] = NULL;
    }
  }
  free(list);
  *nseg = 0;
  *flist = NULL;
}

/*
 Compute nearest interior cells from facet
*/
static void _CSLPolyLabelFacets(CSLPoly p)
{
  PetscInt c,s;
 
  for (s=0; s<p->boundary_nfacets; s++) {
    PetscReal min_sep = 1.0e32;
    PetscReal sep,dx,dy;
    PetscInt min_c = -1;
    PetscReal *coor;
    PetscInt ci,cj,cidx;
    
    coor = p->boundary_facet_list[s]->coor;
    
    ci = (PetscInt)((coor[0] - p->gmin[0])/p->dx[0]);
    cj = (PetscInt)((coor[1] - p->gmin[1])/p->dx[1]);
    if (ci == p->mx[0]) { ci--; }
    if (cj == p->mx[1]) { cj--; }
    cidx = ci + cj * p->mx[0];
    
    p->boundary_facet_list[s]->containing_cell = cidx;
    
    for (c=0; c<p->ncells; c++) {
      
      if (p->cell_list[c]->type != COOR_INTERIOR) continue;
      
      dx = p->boundary_facet_list[s]->coor[0] - p->cell_list[c]->coor[0];
      dy = p->boundary_facet_list[s]->coor[1] - p->cell_list[c]->coor[1];
      sep = dx*dx + dy*dy;
      
      if (sep < min_sep) {
        min_sep = sep;
        min_c = c;
      }
    }
    
    p->boundary_facet_list[s]->closest_interior_cell = min_c;
  }
}

void CSLPolyLabelFacets(CSLPoly p)
{
  PetscInt c,s;
  
  for (s=0; s<p->boundary_nfacets; s++) {
    PetscReal min_sep = 1.0e32;
    PetscReal sep,dx,dy;
    PetscInt min_c = -1;
    PetscReal *coor;
    PetscInt ci,cj,cidx;
    PetscInt cii,cjj;
    
    coor = p->boundary_facet_list[s]->coor;
    
    ci = (PetscInt)((coor[0] - p->gmin[0])/p->dx[0]);
    cj = (PetscInt)((coor[1] - p->gmin[1])/p->dx[1]);
    if (ci == p->mx[0]) { ci--; }
    if (cj == p->mx[1]) { cj--; }
    cidx = ci + cj * p->mx[0];
    
    p->boundary_facet_list[s]->containing_cell = cidx;
    
    for (cii=ci-10; cii<=ci+10; cii++) {
      for (cjj=cj-10; cjj<=cj+10; cjj++) {
      
        if (cii < 0) continue;
        if (cjj < 0) continue;
        if (cii >= p->mx[0]) continue;
        if (cjj >= p->mx[1]) continue;
        
        c = cii + cjj * p->mx[0];
        
        if (p->cell_list[c]->type != COOR_INTERIOR) continue;
      
        dx = p->boundary_facet_list[s]->coor[0] - p->cell_list[c]->coor[0];
        dy = p->boundary_facet_list[s]->coor[1] - p->cell_list[c]->coor[1];
        sep = dx*dx + dy*dy;
      
        if (sep < min_sep) {
          min_sep = sep;
          min_c = c;
        }
      }
    }
    
    p->boundary_facet_list[s]->closest_interior_cell = min_c;
  }
}


/* 
 Compute nearest facet for exterior cells
*/
void CSLPolyLabelCells(CSLPoly p)
{
  PetscInt c,s;
  
  for (c=0; c<p->ncells; c++) {
    PetscReal min_sep = 1.0e32;
    PetscReal sep,dx,dy;
    PetscInt min_s = -1;
    
    if (p->cell_list[c]->type != COOR_EXTERIOR) continue;
    
    for (s=0; s<p->boundary_nfacets; s++) {
      
      dx = p->boundary_facet_list[s]->coor[0] - p->cell_list[c]->coor[0];
      dy = p->boundary_facet_list[s]->coor[1] - p->cell_list[c]->coor[1];
      sep = dx*dx + dy*dy;
      
      if (sep < min_sep) {
        min_sep = sep;
        min_s = s;
      }
    }
    
    p->cell_list[c]->closest_facet = min_s;
    
  }
}

static void interpolate_billinear_v(CSLPoly p,PetscInt cellid,PetscReal coor[],PetscReal v[])
{
  PetscInt nidx[4];
  PetscInt k,ci,cj;
  PetscReal xi[2],x0[2],Ni[4];
  
  cj = cellid/p->mx[0];
  ci = cellid - cj * p->mx[0];

  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = CSLNodeIdx(p,ci  ,cj);
  nidx[1] = CSLNodeIdx(p,ci+1,cj);
  nidx[2] = CSLNodeIdx(p,ci  ,cj+1);
  nidx[3] = CSLNodeIdx(p,ci+1,cj+1);
  
  x0[0] = p->gmin[0] + ci * p->dx[0];
  x0[1] = p->gmin[1] + cj * p->dx[1];
  
  xi[0] = 2.0*(coor[0]-x0[0])/p->dx[0] - 1.0;
  xi[1] = 2.0*(coor[1]-x0[1])/p->dx[1] - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  v[0] = v[1] = 0.0;
  for (k=0; k<4; k++) {
    v[0] += Ni[k] * p->vel_v[2*nidx[k]+0];
    v[1] += Ni[k] * p->vel_v[2*nidx[k]+1];
  }
  
}
                             
/*
 Only makes sense once a velocity field has been defined
*/
void CSLPolyMarkInflowFacets(CSLPoly p)
{
  PetscInt s;
  
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (s=0; s<p->boundary_nfacets; s++) {
    PetscInt cellid;
    PetscReal *coor;
    PetscReal vel[2],v_dot_n;
    
    coor = p->boundary_facet_list[s]->coor;
    cellid = p->boundary_facet_list[s]->containing_cell;
    
    /* interpolate velocity */
    interpolate_billinear_v(p,cellid,coor,vel);
    
    p->boundary_facet_list[s]->velocity[0] = vel[0];
    p->boundary_facet_list[s]->velocity[1] = vel[1];
    
    v_dot_n  = vel[0] * p->boundary_facet_list[s]->normal[0];
    v_dot_n += vel[1] * p->boundary_facet_list[s]->normal[1];
    
    if (v_dot_n < 0.0) {
      p->boundary_facet_list[s]->is_inflow = 1;
    } else {
      p->boundary_facet_list[s]->is_inflow = 0;
    }
  }
}

static void __BoundaryPCreate(PetscReal ds,PetscInt np,PetscReal polyvert[],PetscInt polyedgelabel[],PetscInt *nseg,BoundaryP **flist)
{
  BoundaryP *list,*tmp,facet;
  PetscInt nsegments,p;
  
  list = (BoundaryP*)malloc(sizeof(BoundaryP));
  list[0] = NULL;
  
  nsegments = 0;
  for (p=0; p<np; p++) {
    PetscInt p0,p1,ns,s;
    PetscReal dx,dy,dl,nn[2],xx[2],mag;
    
    p0 = p;
    p1 = (p+1)%np;
    
    dl = 0.0;
    dl  = (polyvert[2*p1+0]-polyvert[2*p0+0]) * (polyvert[2*p1+0]-polyvert[2*p0+0]);
    dl += (polyvert[2*p1+1]-polyvert[2*p0+1]) * (polyvert[2*p1+1]-polyvert[2*p0+1]);
    dl = PetscSqrtReal(dl);

    dx = polyvert[2*p1+0] - polyvert[2*p0+0];
    dy = polyvert[2*p1+1] - polyvert[2*p0+1];
    mag = PetscSqrtReal(dx*dx+dy*dy);
    
    nn[0] =  dy/mag; // outward pointing normals
    nn[1] = -dx/mag;
    
    if (ds > dl) {
      ns = 1;
    } else {
      ns = (PetscInt)(dl/ds);
      if (ns <= 0) { ns = 1; }
    }
    
    dx = (polyvert[2*p1+0]-polyvert[2*p0+0]) / ((PetscReal)ns);
    dy = (polyvert[2*p1+1]-polyvert[2*p0+1]) / ((PetscReal)ns);
    
    for (s=0; s<ns; s++) {
      
      xx[0] = polyvert[2*p0+0] + 0.5*dx + dx * s;
      xx[1] = polyvert[2*p0+1] + 0.5*dy + dy * s;
      
      
      facet = (BoundaryP)malloc(sizeof(struct _p_BoundaryP));
      
      facet->coor[0] = xx[0];
      facet->coor[1] = xx[1];
      facet->normal[0] = nn[0];
      facet->normal[1] = nn[1];
      facet->label = polyedgelabel[p];
      facet->bcset = 0;
      //printf("%d %d %f %f %f %f\n",p,s,xx[0],xx[1],nn[0],nn[1]);
      
      tmp = (BoundaryP*)realloc(list,(nsegments+1)*sizeof(BoundaryP));
      list = tmp;
      list[nsegments] = facet;
      
      nsegments++;
      
    }
    
  }
  
  *flist = list;
  *nseg = nsegments;
}

void CSLPolyBoundaryPCreate(CSLPoly sl)
{
  BoundaryP *list,*tmp,facet;
  PetscInt nsegments,p;
  PetscReal ds;
  PetscInt np;
  PetscReal *polyvert;
  PetscInt *polyedgelabel;

  ds = sl->ds;
  np = sl->poly_npoints;
  polyvert = sl->poly_coor;
  polyedgelabel = sl->poly_edge_label;

  sl->boundary_facet_offset = (PetscInt*)malloc(sizeof(PetscInt)*(np+1));
  for (p=0; p<np+1; p++) {
    sl->boundary_facet_offset[p] = 0;
  }
  sl->boundary_ds = (PetscReal*)malloc(sizeof(PetscReal)*np);
  for (p=0; p<np; p++) {
    sl->boundary_ds[p] = 0.0;
  }

  
  list = (BoundaryP*)malloc(sizeof(BoundaryP));
  list[0] = NULL;
  
  nsegments = 0;
  for (p=0; p<np; p++) {
    PetscInt p0,p1,ns,s;
    PetscReal dx,dy,dl,nn[2],xx[2],mag;
    
    p0 = p;
    p1 = (p+1)%np;
    
    dl = 0.0;
    dl  = (polyvert[2*p1+0]-polyvert[2*p0+0]) * (polyvert[2*p1+0]-polyvert[2*p0+0]);
    dl += (polyvert[2*p1+1]-polyvert[2*p0+1]) * (polyvert[2*p1+1]-polyvert[2*p0+1]);
    dl = PetscSqrtReal(dl);
    
    dx = polyvert[2*p1+0] - polyvert[2*p0+0];
    dy = polyvert[2*p1+1] - polyvert[2*p0+1];
    mag = PetscSqrtReal(dx*dx+dy*dy);
    
    nn[0] =  dy/mag; // outward pointing normals
    nn[1] = -dx/mag;
    
    if (ds > dl) {
      ns = 1;
    } else {
      ns = (PetscInt)(dl/ds);
      if (ns <= 0) { ns = 1; }
    }
    
    dx = (polyvert[2*p1+0]-polyvert[2*p0+0]) / ((PetscReal)ns);
    dy = (polyvert[2*p1+1]-polyvert[2*p0+1]) / ((PetscReal)ns);
    
    sl->boundary_ds[p] = PetscSqrtReal(dx*dx+dy*dy);
    
    for (s=0; s<ns; s++) {
      
      xx[0] = polyvert[2*p0+0] + 0.5*dx + dx * s;
      xx[1] = polyvert[2*p0+1] + 0.5*dy + dy * s;
      
      
      facet = (BoundaryP)malloc(sizeof(struct _p_BoundaryP));
      
      facet->coor[0] = xx[0];
      facet->coor[1] = xx[1];
      facet->normal[0] = nn[0];
      facet->normal[1] = nn[1];
      facet->label = polyedgelabel[p];

      facet->bcset = 0;
      facet->is_inflow = -1;
      facet->containing_cell = -1;
      facet->closest_interior_cell = -1;
      facet->value = NULL;
      facet->ivalue = NULL;
      //printf("%d %d %f %f %f %f\n",p,s,xx[0],xx[1],nn[0],nn[1]);
      
      tmp = (BoundaryP*)realloc(list,(nsegments+1)*sizeof(BoundaryP));
      list = tmp;
      list[nsegments] = facet;
      
      nsegments++;
    }
    sl->boundary_facet_offset[p+1] = nsegments;
    
  }
  
  sl->boundary_facet_list = list;
  sl->boundary_nfacets = nsegments;

  /* report */
  printf("[BoundaryPView]\n");
  printf("  nedges %d\n",np);
  printf("  nsegments %d\n",nsegments);
  for (p=0; p<np; p++) {
    printf("  [boundary segment %.3d]\n",p);
    printf("    nsegments %d\n",sl->boundary_facet_offset[p+1]-sl->boundary_facet_offset[p]);
    printf("    offset    %d\n",sl->boundary_facet_offset[p]);
    printf("    ds        %+1.4e\n",sl->boundary_ds[p]);
  }
}

void CSLPolySetUpFields(CSLPoly p)
{
  PetscInt s;

  p->vel_v = (PetscReal*)malloc(sizeof(PetscReal)*2*p->nvert);
  memset(p->vel_v,0,sizeof(PetscReal)*2*p->nvert);

  for (s=0; s<p->boundary_nfacets; s++) {
    p->boundary_facet_list[s]->value = (PetscReal*)malloc(sizeof(PetscReal)*p->nfields);
    memset(p->boundary_facet_list[s]->value,0,sizeof(PetscReal)*p->nfields);
  }

  p->value_c = (PetscReal*)malloc(sizeof(PetscReal)*p->nfields*p->ncells);
  memset(p->value_c,0,sizeof(PetscReal)*p->nfields*p->ncells);

  if (p->nimplicitfields > 0) {
    p->ivalue_c = (PetscReal*)malloc(sizeof(PetscReal)*p->nimplicitfields*p->ncells);
    memset(p->ivalue_c,0,sizeof(PetscReal)*p->nimplicitfields*p->ncells);

    for (s=0; s<p->boundary_nfacets; s++) {
      p->boundary_facet_list[s]->ivalue = (PetscReal*)malloc(sizeof(PetscReal)*p->nimplicitfields);
      memset(p->boundary_facet_list[s]->ivalue,0,sizeof(PetscReal)*p->nimplicitfields);
    }
  }
  
  p->divu_c = (PetscReal*)malloc(sizeof(PetscReal)*p->ncells);
  memset(p->divu_c,0,sizeof(PetscReal)*p->ncells);
  
  if (p->nimplicitfields > 0) {
    if (!p->ifields_evaluate_at_point) {
      printf("error[CSLPolySetUpFields]: A method for evaluating implicit fields has not been set\n");
      printf("error[CSLPolySetUpFields]: You must call CSLPolySetImplicitFieldEvaluators()\n");
      exit(1);
    }
    if (!p->ifields_evaluate_at_boundarypoint) {
      printf("error[CSLPolySetUpFields]: A method for evaluating implicit fields at boundaries has not been set\n");
      printf("error[CSLPolySetUpFields]: You must call CSLPolySetImplicitFieldEvaluators()\n");
      exit(1);
    }
  }
}

void CSLPolyReconstructExteriorCellFields(CSLPoly p)
{
  PetscInt c;
  
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<p->ncells; c++) {
    CellP     cell;
    BoundaryP facet;
    PetscInt  cn,f,facetid;
    
    cell = p->cell_list[c];
    
    if (cell->type != COOR_EXTERIOR) continue;
    
    facetid = cell->closest_facet;
    if (facetid < 0 || facetid >= p->boundary_nfacets) {
      printf("[CSLPolyReconstructExteriorCellFields]: reconstruction error occured on cell %d : closest_facet is not initialized\n",c);
      exit(1);
    }
    facet = p->boundary_facet_list[facetid];
    
    if (facet->is_inflow == 1) {
      /* use values from the nearest facet */
      if (!facet->value) {
        printf("[CSLPolyReconstructExteriorCellFields]: reconstruction error occured on cell %d : facet->value[] is not allocated\n",c);
        exit(1);
      }
      for (f=0; f<p->nfields; f++) {
        p->value_c[p->nfields*c+f] = facet->value[f];
      }
      for (f=0; f<p->nimplicitfields; f++) {
        p->ivalue_c[p->nimplicitfields*c+f] = facet->ivalue[f];
      }
    } else if (facet->is_inflow == 0) {
      /* use values from the nearest facets nearest interior cell */
      cn = facet->closest_interior_cell;
      if (cn < 0) {
        printf("[CSLPolyReconstructExteriorCellFields]: reconstruction error occured on cell %d : closest_interior_cell is not initialized\n",c);
        exit(1);
      }
      for (f=0; f<p->nfields; f++) {
        p->value_c[p->nfields*c+f] = p->value_c[p->nfields*cn+f];
      }
      for (f=0; f<p->nimplicitfields; f++) {
        p->ivalue_c[p->nimplicitfields*c+f] = p->ivalue_c[p->nimplicitfields*cn+f];
      }
      
    } else {
      printf("[CSLPolyReconstructExteriorCellFields]: reconstruction error occured on cell %d : facet is not initialized\n",c);
      exit(1);
    }
  }
}

void CSLPolyReconstructExteriorFacetFields(CSLPoly p)
{
  PetscInt s;
  
  /* extrapolate from the interior */
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (s=0; s<p->boundary_nfacets; s++) {
    if (p->boundary_facet_list[s]->is_inflow == 0) {
      PetscInt f,cellid;
      
      /* get index of nearest interior cell */
      cellid = p->boundary_facet_list[s]->closest_interior_cell;
      
      for (f=0; f<p->nfields; f++) {
        p->boundary_facet_list[s]->value[f] = p->value_c[p->nfields*cellid + f];
      }
      for (f=0; f<p->nimplicitfields; f++) {
        p->boundary_facet_list[s]->ivalue[f] = p->ivalue_c[p->nimplicitfields*cellid + f];
      }
    }
  }
  
}

void CSLPolyComputeCellAverageDivVelocity(CSLPoly s)
{
  PetscInt nx,mx,my,i,j,vidx[2];
  PetscReal dx,dy,dv;
  const PetscReal *LA_vel;
  PetscReal *LA_div;
  
  dx = s->dx[0];
  dy = s->dx[1];
  dv = dx * dy;

  mx = s->mx[0];
  my = s->mx[1];

  nx = s->nx[0];
  
  LA_vel = (const PetscReal*)s->vel_v;
  LA_div = (PetscReal*)s->divu_c;
  for (i=0; i<mx*my; i++) {
    LA_div[i] = 0.0;
  }
  
  for (j=0; j<my; j++) {
    for (i=0; i<mx; i++) {
      PetscReal int_div_v;
      PetscInt cellid;
      
      /* apply divergence theorem to evaluate \PetscInt div(v) dV */
      /* estimate surface intergrals via a 1-poPetscInt quadrature rule */
      int_div_v = 0.0;
      
      vidx[0] = (i  ) + (j+1)*nx;
      vidx[1] = (i+1) + (j+1)*nx;
      int_div_v += 0.5 * (LA_vel[2*vidx[0]+1] + LA_vel[2*vidx[1]+1]) * dx; /* u_y(north) */
      
      vidx[0] = (i+1) + (j  )*nx;
      vidx[1] = (i+1) + (j+1)*nx;
      int_div_v += 0.5 * (LA_vel[2*vidx[0]+0] + LA_vel[2*vidx[1]+0]) * dy; /* u_x(east) */
      
      vidx[0] = (i  ) + (j  )*nx;
      vidx[1] = (i+1) + (j  )*nx;
      int_div_v -= 0.5 * (LA_vel[2*vidx[0]+1] + LA_vel[2*vidx[1]+1]) * dx; /* u_y(south) */
      
      vidx[0] = (i  ) + (j  )*nx;
      vidx[1] = (i  ) + (j+1)*nx;
      int_div_v -= 0.5 * (LA_vel[2*vidx[0]+0] + LA_vel[2*vidx[1]+0]) * dy; /* u_x(west) */
      
      cellid = i + j * mx;
      LA_div[cellid] = int_div_v;
    }
  }
  
  for (i=0; i<mx*my; i++) {
    LA_div[i] = LA_div[i]/dv;
  }
  
  CSLPolyExtrapolateExteriorCellFields(s,1,s->divu_c);
}

void PolyView_gnuplot(PetscInt np,PetscReal xy[],const char name[])
{
  PetscInt i;
  FILE *fp;
  
  fp = fopen(name,"w");
  for (i=0; i<=np; i++) {
    PetscInt index = i%np;
    fprintf(fp,"%1.4e %1.4e 0.0 1.0\n",xy[2*index],xy[2*index+1]);
  }
  fprintf(fp,"\n\n");
  fclose(fp);
}

void RasterView_gnuplot(PetscInt nx,PetscInt ny,PetscReal coor[],PetscInt inside[],const char name[])
{
  FILE *fp;
  PetscInt i,j;

  fp = fopen(name,"w");
  
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscReal xp,yp;
      PetscInt idx;
      
      idx = i+j*nx;
      xp = coor[2*idx];
      yp = coor[2*idx+1];
      
      fprintf(fp,"%f %f %f \n",xp,yp,(double)inside[idx]);
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}

void RasterViewField_gnuplot(PetscInt nx,PetscInt ny,PetscReal coor[],PetscInt nf,PetscReal field[],PetscInt inside[],const char name[])
{
  FILE *fp;
  PetscInt i,j,f;
  
  fp = fopen(name,"w");
  
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscReal xp,yp;
      PetscInt idx;
      
      idx = i+j*nx;
      xp = coor[2*idx];
      yp = coor[2*idx+1];
      
      fprintf(fp,"%f %f %f ",xp,yp,(double)inside[idx]);
      for (f=0; f<nf; f++) {
        fprintf(fp,"%f ",field[nf*idx+f]);
      } fprintf(fp,"\n");
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}

void CellView_gnuplot(CSLPoly p,const char name[])
{
  FILE *fp;
  PetscInt i,j;
  CellP cell;
  
  fp = fopen(name,"w");
  
  for (j=0; j<p->mx[1]; j++) {
    for (i=0; i<p->mx[0]; i++) {
      PetscReal xp,yp;
      PetscInt idx;
      
      idx = i+j*p->mx[0];
      
      cell = p->cell_list[idx];
      xp = cell->coor[0];
      yp = cell->coor[1];
      
      fprintf(fp,"%f %f %f %d %d\n",xp,yp,p->value_c[p->nfields*idx+0],cell->is_interior,(int)cell->type);
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}

void CellFieldView_gnuplot(CSLPoly p,const char name[])
{
  FILE *fp;
  PetscInt i,j;
  CellP cell;
  
  fp = fopen(name,"w");
  
  for (j=0; j<p->mx[1]; j++) {
    for (i=0; i<p->mx[0]; i++) {
      PetscReal xp,yp;
      PetscInt idx;
      
      idx = i+j*p->mx[0];
      
      cell = p->cell_list[idx];
      xp = cell->coor[0];
      yp = cell->coor[1];
      if (cell->is_interior == 1) {
        fprintf(fp,"%f %f %f %d\n",xp,yp,p->value_c[p->nfields*idx+0],(int)cell->type);
      }
    }
  }
  fclose(fp);
}

void FacetView_gnuplot(CSLPoly p,const char name[])
{
  FILE *fp;
  PetscInt s;
  BoundaryP facet;
  
  fp = fopen(name,"w");
  
  for (s=0; s<p->boundary_nfacets; s++) {
    PetscReal xp,yp;
    
    facet = p->boundary_facet_list[s];
    xp = facet->coor[0];
    yp = facet->coor[1];
    
    fprintf(fp,"%f %f %d %f %f %f %f %f\n",xp,yp,facet->is_inflow,
            facet->normal[0],facet->normal[1],
            facet->velocity[0],facet->velocity[1],  facet->value[0]);
  }
  fclose(fp);
}

void compute_divu_default(CSLPoly sl,PetscInt cellid,PetscReal x[],PetscReal *divu)
{
  PetscInt inside_domain,inside_csl_domain;
  
  CSLPolyInterpolateAtPointWithArray(sl,sl->interp_type,(const PetscReal*)x,
                                          1,(const PetscReal*)sl->divu_c,
                                          NULL,NULL,
                                          divu,&inside_domain,&inside_csl_domain);
  if (inside_csl_domain == 0) {
    printf("error[compute_divu_default]: coor %+1.4e %+1.4e is outside the CSL box\n",x[0],x[1]);
    exit(1);
  }
}

void CSLPolySetDefaultDivergenceMethod(CSLPoly sl)
{
  sl->compute_div_u = compute_divu_default;
}

/*
 Advect a characteristic backwards over a time period dt using RK1.
 The method ensures the point is inside the domain as inferred from the point_in_polygon() test.
 Notes:
 - Uses a local (cell wise) CFL number of 0.5 to advect characteristic (phase 1)
 - If characteristic leaves the domain, reject the last step and proceed
   to advect point using local CFL number of 0.05. Call this phase 2.
 - Continue to advance point through phase 2 until either time=dt or point
   leaves the domain. If it leaves the domain, reject the last step.
 - Mark characteristic with TAG_YELLOW if it left the domain, otherwise mark it with TAG_GREEN
 - Integration of phase 1 or phase 2 is terminated if ||v||_2 < 1.0e-12
*/
static void advect_point_rk1(CharacteristicP point,PetscReal dt,CSLPoly sl)
{
  PetscInt d;
  PetscReal dt0,dt1;
  PetscReal time ;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel;
  
  time = 0.0;
  ds = sl->dx[0];
  if (sl->dx[1] < sl->dx[0]) { ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  
  dt0 = 0.0;
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) {
      dt0 = 0.5 * dtcfl[0];
    } else {
      dt0 = 0.5 * dtcfl[1];
    }
    if ((time+dt0) > dt) { dt0 = dt - time; emergency = 1; }
    
    /* x' = x - dt0 * v(x) */
    for (d=0; d<2; d++) {
      x1[d] = x0[d] - dt0 * vel_i[d];
    }
    
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    
    if (inside == 0) {
      break;
    }
    
    /* update */
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;

    cellid = ci + cj * sl->mx[0];
    
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    
    time += dt0;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    
    return;
  }

  /* update */
  ci = (PetscInt)( (x0[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x0[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  
  cellid = ci + cj * sl->mx[0];

  
  dt1 = 0.0;
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);

    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) {
      dt1 = 0.05 * dtcfl[0];
    } else {
      dt1 = 0.05 * dtcfl[1];
    }
    if ((time+dt1) > dt) { dt1 = dt - time; emergency = 1; }

    /* x' = x - dt0 * v(x) */
    for (d=0; d<2; d++) {
      x1[d] = x0[d] - dt1 * vel_i[d];
    }
    
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    
    if (inside == 0) {
      break;
    }
    
    /* update */
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
      
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    
    time += dt1;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 0) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x0[d];
    }
    ci = (PetscInt)( (x0[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x0[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
  } else {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
  }
}

/*
 Advect a characteristic coordinate and the cell volume backwards over a time period dt using RK1.
 e.g. solves
   dx/dt = v
   d |omega| /dt = |omega| div(u)
 The method ensures the point is inside the domain as inferred from the point_in_polygon() test.
 Notes:
 - div(u) is required to specified via a function pointer - this will be changed to use an interpolated div(u) value
 - Uses a local (cell wise) CFL number of 0.5 to advect characteristic (phase 1)
 - If characteristic leaves the domain, reject the last step and proceed
   to advect point using local CFL number of 0.05. Call this phase 2.
 - Continue to advance point through phase 2 until either time=dt or point
   leaves the domain. If it leaves the domain, reject the last step.
 - Mark characteristic with TAG_YELLOW if it left the domain, otherwise mark it with TAG_GREEN
 - Integration of phase 1 or phase 2 is terminated if ||v||_2 < 1.0e-12
*/
static void advect_point_rk1_divu(CharacteristicP point,PetscReal dt,CSLPoly sl)
{
  PetscInt d;
  PetscReal dt0,dt1;
  PetscReal time ;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0},dV = 0.0,divu_p;
  PetscInt cellid,ci,cj;
  PetscReal ds,dV0,fvel;
  
  time = 0.0;
  ds = sl->dx[0];
  if (sl->dx[1] < sl->dx[0]) { ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  dV0 = point->volume;
  
  dt0 = 0.0;
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;

    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    sl->compute_div_u(sl,cellid,x0,&divu_p);

    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) {
      dt0 = 0.5 * dtcfl[0];
    } else {
      dt0 = 0.5 * dtcfl[1];
    }
    if ((time+dt0) > dt) { dt0 = dt - time; emergency = 1; }

    dV = dV0 * PetscExpReal( - divu_p * (time+dt0));
    if (dV < 0.0) {
      PetscReal dt_star;
      /* Modify time step assuming volume changed from dV0 to dV linearly over dt0 */
      /* Identify timestep dt^* where J changes from a positive number to J^* = 0.0 */
      /*
       (J^1 - J^0) / (J^* - J^0) = dt0/dt^*
      */
      
      dt_star = - dt0 * ( dV0 ) / (dV - dV0);
      dt0 = dt_star;
      
      dV = 0.0; /* force the volume to be exactly 0.0 */
      emergency = 1; /* force loop to exit */
      printf("[1] exiting[volume became negative]\n");
      exit(10);
    }
    
    /* x' = x - dt0 * v(x) */
    for (d=0; d<2; d++) {
      x1[d] = x0[d] - dt0 * vel_i[d];
    }
    
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    
    if (inside == 0) {
      break;
    }
    
    /* update */
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    dV0 = dV;

    time += dt0;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  //printf("dV = %+1.4e : PetscInt %+1.4e\n",dV,point->volume);
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    point->volume = dV;
    
    return;
  }
  
  /* update */
  ci = (PetscInt)( (x0[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x0[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  
  cellid = ci + cj * sl->mx[0];
  
  
  dt1 = 0.0;
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);

    sl->compute_div_u(sl,cellid,x0,&divu_p);

    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) {
      dt1 = 0.05 * dtcfl[0];
    } else {
      dt1 = 0.05 * dtcfl[1];
    }
    if ((time+dt1) > dt) { dt1 = dt - time; emergency = 1; }
    
    dV = dV0 * PetscExpReal( - divu_p * (time+dt1));
    if (dV < 0.0) {
      PetscReal dt_star;
      /* Modify time step assuming volume changed from dV0 to dV linearly over dt1 */
      /* Identify timestep dt^* where J changes from a positive number to J^* = 0.0 */
      /*
       (J^1 - J^0) / (J^* - J^0) = dt1/dt^*
       */
      
      dt_star = - dt1 * ( dV0 ) / (dV - dV0);
      dt1 = dt_star;
      
      dV = 0.0; /* force the volume to be exactly 0.0 */
      emergency = 1; /* force loop to exit */
      printf("[2] exiting[volume became negative]\n");
      exit(10);
    }

    /* x' = x - dt0 * v(x) */
    for (d=0; d<2; d++) {
      x1[d] = x0[d] - dt1 * vel_i[d];
    }
    
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    
    if (inside == 0) {
      break;
    }
    
    /* update */
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    dV0 = dV;
    
    time += dt1;

    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 0) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x0[d];
    }
    ci = (PetscInt)( (x0[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x0[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
    point->volume = dV;
  } else {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    point->volume = dV;
  }
}

/*
 Advect a characteristic backwards over a time period dt using RK1.
 The method ensures the point is inside the domain as inferred from the point_in_polygon() test.
 Notes:
 - Uses a local (cell wise) CFL number of 2.5 to advect characteristic
 - If characteristic leaves the domain, reject the last step and proceed
   to identify the crossing point defining the intersection of the characteristic and the boundary polygon.
   The final location of such characteristics are located exactly on a polygon line segment.
 - Mark characteristic with TAG_YELLOW if it left the domain, otherwise mark it with TAG_GREEN
 - Integration is terminated if ||v||_2 < 1.0e-12
*/
static void advect_point_rk1_line_intersect(CharacteristicP point,PetscReal dt,CSLPoly sl,PetscInt *intersect)
{
  PetscInt d;
  PetscReal dt0;
  PetscReal time;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel;
  PetscInt nsteps = 0;
  
  time = 0.0;
  ds = sl->dx[0];
  if (sl->dx[1] < sl->dx[0]) { ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  
  dt0 = 0.0;
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) {
      dt0 = 2.5 * dtcfl[0];
    } else {
      dt0 = 2.5 * dtcfl[1];
    }
    if ((time+dt0) > dt) { dt0 = dt - time; emergency = 1; }
    
    /* x' = x - dt0 * v(x) */
    for (d=0; d<2; d++) {
      x1[d] = x0[d] - dt0 * vel_i[d];
    }
    
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    if (inside == 0) break;
    
    /* update */
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    
    time += dt0;
    nsteps++;

    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    
    *intersect = 0;
  } else {
    PetscInt found_intersection = 0;
    PetscReal X[2];
    
    line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                        sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                        X,&point->poly_facet_crossed,&found_intersection);

    if (found_intersection == 0) {
      printf("Failed to detemine intersection between outgoing characteristic and polygon\n");
      exit(1);
    }
    
    for (d=0; d<2; d++) {
      point->departure_coor[d] = X[d];
    }
    
    ci = (PetscInt)( (X[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (X[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
    
    *intersect = 1;
  }
}

/*
 Advect a characteristic coordinate and the cell volume backwards over a time period dt using RK1.
 e.g. solves
   dx/dt = v
   d |omega| /dt = |omega| div(u)
 Notes:
 - Uses a local (cell wise) CFL number of 2.5 to advect characteristic
 - If characteristic leaves the domain, reject the last step and proceed
   to identify the crossing point defining the intersection of the characteristic and the boundary polygon.
   The final location of such characteristics are located exactly on a polygon line segment.
 - Mark characteristic with TAG_YELLOW if it left the domain, otherwise mark it with TAG_GREEN
 - Integration is terminated if ||v||_2 < 1.0e-12
 */
static void advect_point_rk1_divu_line_intersect(CharacteristicP point,PetscReal dt,CSLPoly sl,PetscInt *intersect,PetscBool trace)
{
  PetscInt d;
  PetscReal dt0;
  PetscReal time;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel,dV = 0.0,dV0,divu_p = 0.0;
  PetscInt nsteps = 0;
  
  time = 0.0;
  ds = sl->dx[0];
  if (sl->dx[1] < sl->dx[0]) { ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  dV0 = point->volume;
  
  dt0 = 0.0;
  
  if (trace) {
    printf("# charateristic trace : using dt = %+1.4e\n",dt);
    printf("0 %d %+1.4e   %+1.4e %+1.4e  %+1.8e 0.0\n",nsteps,time,x0[0],x0[1],dV0);
  }
  
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    sl->compute_div_u(sl,cellid,x0,&divu_p);
    
    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) { dt0 = 0.5 * dtcfl[0]; }
    else {                     dt0 = 0.5 * dtcfl[1]; }
    if ((time+dt0) > dt)     { dt0 = dt - time;        emergency = 1; }

    if (trace) { printf("#   using dt0 = %+1.4e\n",dt0); }
    
    dV = dV0 * PetscExpReal( - divu_p * (dt0));
    if (dV < 0.0) {
      PetscReal dt_star;
      /* Modify time step assuming volume changed from dV0 to dV linearly over dt0 */
      /* Identify timestep dt^* where J changes from a positive number to J^* = 0.0 */
      /*
       (J^1 - J^0) / (J^* - J^0) = dt0/dt^*
       */
      
      dt_star = - dt0 * ( dV0 ) / (dV - dV0);
      dt0 = dt_star;
      
      dV = 0.0; /* force the volume to be exactly 0.0 */
      emergency = 1; /* force loop to exit */
      printf("[3] exiting[volume became negative]\n");
      exit(10);
    }

    /* x' = x - dt0 * v(x) */
    for (d=0; d<2; d++) {
      x1[d] = x0[d] - dt0 * vel_i[d];
    }
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    if (inside == 0) break;
    
    /* update */
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    dV0 = dV;

    time += dt0;
    nsteps++;
    
    if (trace) { printf("1 %d %+1.4e   %+1.4e %+1.4e  %+1.8e %+1.8e\n",nsteps,time,x1[0],x1[1],dV,divu_p); }
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    point->volume = dV;//dV00;//dV;
    
    *intersect = 0;
  } else {
    PetscInt found_intersection = 0;
    PetscReal X[2],dt_star;
    
    line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                        sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                        X,&point->poly_facet_crossed,&found_intersection);
    
    if (found_intersection == 0) {
      printf("Failed to detemine intersection between outgoing characteristic and polygon\n");
      exit(1);
    }

    ci = (PetscInt)( (X[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (X[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];

    // dV0 in memory is that from the failed step which exited the domain
    // re-use the computed value of divu_p which is still in memory (corresponds to the foot of the characteristic which exited the domain)
    //sl->compute_div_u(sl,cellid,X,&divu_p);

    /*
     Determine the time step required to hit the boundary
     X[0] = x0[0] + dt^* vel_i[0] ==> solve for dt^*
     X[d] = x0[d] - dt^* vel_i[d];
    */
    dt_star = PetscAbsReal( -(X[0] - x0[0])/vel_i[0] ); /* take abs here */
    dV = dV0 * PetscExpReal( - divu_p * (dt_star)); /* update dV using previous value for dV0 AND the truncated time step dt* */
    time += dt_star;
    nsteps++;
    if (trace) { printf("#   [update to land on boundary] using dt* = %+1.4e\n",dt_star); }
    
    if (trace) { printf("2 %d %+1.4e   %+1.4e %+1.4e  %+1.4e\n",nsteps,time,X[0],X[1],dV); }
    
    /* compute the contribution from moving the point outside the domain */
    dt_star = dt - time;
    if (dt_star < 0.0) {
      dt_star = 0.0;
    }
    if (trace) { printf("#   [update to infinity] using dt* = %+1.4e\n",dt_star); }
    
    dV0 = dV;
    sl->compute_div_u(sl,cellid,X,&divu_p);
    dV = dV0 * PetscExpReal( - divu_p * (dt_star)); /* update dV using previous value for dV0 AND any remainder required to reach dt */
    
    if (trace) { printf("3 %d %+1.4e   %+1.4e %+1.4e  %+1.4e\n",nsteps,time,X[0],X[1],dV); }

    if (dV < 0.0) {
      dV = 0.0; /* assume that if the sign changed occured when crossing boundary - set dV = 0.0 */
      printf("[4] exiting[volume became negative]\n");
      exit(10);
    }
    
    for (d=0; d<2; d++) {
      point->departure_coor[d] = X[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
    point->volume = dV;//dV00;//dV;
    
    *intersect = 1;
  }
}

static void advect_point_rk2_divu_line_intersect(CharacteristicP point,PetscReal dt,CSLPoly sl,PetscInt *intersect,PetscBool trace)
{
  PetscInt d;
  PetscReal dt0;
  PetscReal time;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel,dV = 0.0,dV0,divu_p = 0.0;
  PetscInt nsteps = 0;
  
  time = 0.0;
  ds = sl->dx[0];
  if (sl->dx[1] < sl->dx[0]) { ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  dV0 = point->volume;
  
  dt0 = 0.0;
  
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    PetscReal k1[2],k2[2],H,xtmp[2];
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    /* compute a time-step */
    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) { dt0 = 0.01 * dtcfl[0]; }
    else {                     dt0 = 0.01 * dtcfl[1]; }
    if ((time+dt0) > dt)     { dt0 = dt - time;        emergency = 1; }

    
    /* update div from time to time + dt0 */
    sl->compute_div_u(sl,cellid,x0,&divu_p);
    dV = dV0 * PetscExpReal( - divu_p * (dt0));
    
    /* update position from time to time + dt0 using RK2 */
    
    H = -dt0;
    
    /* k1 = H * v(x) */
    for (d=0; d<2; d++) {
      k1[d] = H * vel_i[d];
    }

    /* k2 = H * v(x + 0.5.k1) */
    for (d=0; d<2; d++) {
      xtmp[d] = x0[d] + 0.5 * k1[d];
    }
    point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
    if (inside == 0) {
      for (d=0; d<2; d++) {
        x1[d] = x0[d] - dt0 * vel_i[d];
      }
      break;
    }
    
    ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    cellid = ci + cj * sl->mx[0];
    interpolate_billinear_v(sl,cellid,xtmp,vel_i);
    for (d=0; d<2; d++) {
      k2[d] = H * vel_i[d];
    }

    for (d=0; d<2; d++) {
      x1[d] = x0[d] + k2[d];
    }
    point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
    if (inside == 0) break;
    
    ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    cellid = ci + cj * sl->mx[0];
    
    /* update for next step */
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    dV0 = dV;
    
    time += dt0;
    nsteps++;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    point->volume = dV;
    
    *intersect = 0;
  } else {
    PetscInt found_intersection = 0;
    PetscReal X[2],dt_star;
    
    line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                        sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                        X,&point->poly_facet_crossed,&found_intersection);
    
    if (found_intersection == 0) {
      printf("Failed to detemine intersection between outgoing characteristic and polygon\n");
      exit(1);
    }
    
    ci = (PetscInt)( (X[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (X[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    // dV0 in memory is that from the failed step which exited the domain
    // re-use the computed value of divu_p which is still in memory (corresponds to the foot of the characteristic which exited the domain)
    //sl->compute_div_u(sl,cellid,X,&divu_p);
    
    /*
     Determine the time step required to hit the boundary
     X[0] = x0[0] + dt^* vel_i[0] ==> solve for dt^*
     X[d] = x0[d] - dt^* vel_i[d];
     */
    dt_star = PetscAbsReal( -(X[0] - x0[0])/vel_i[0] ); /* take abs here */
    dV = dV0 * PetscExpReal( - divu_p * (dt_star)); /* update dV using previous value for dV0 AND the truncated time step dt* */
    time += dt_star;
    nsteps++;
    
    /* compute the contribution from moving the point outside the domain */
    dt_star = dt - time;
    if (dt_star < 0.0) {
      dt_star = 0.0;
    }
    
    dV0 = dV;
    sl->compute_div_u(sl,cellid,X,&divu_p);
    dV = dV0 * PetscExpReal( - divu_p * (dt_star)); /* update dV using previous value for dV0 AND any remainder required to reach dt */
    
    if (dV < 0.0) {
      dV = 0.0; /* assume that if the sign changed occured when crossing boundary - set dV = 0.0 */
      printf("[4] exiting[volume became negative]\n");
      exit(10);
    }
    
    for (d=0; d<2; d++) {
      point->departure_coor[d] = X[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
    point->volume = dV;
    
    *intersect = 1;
  }
}

void cRK1(CSLPoly sl,PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
          PetscReal x1[],PetscInt *_cellid)
{
  PetscInt d,inside,ci,cj;
  
  *_cellid = -1;

  for (d=0; d<2; d++) {
    x1[d] = x0[d] - dt * vel0[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
  if (inside == 0) return;
  
  /* update cell id */
  ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  
  *_cellid = ci + cj * sl->mx[0];
}

void cRK2(CSLPoly sl,PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
          PetscReal x1[],PetscInt *_cellid)
{
  PetscInt d,inside,ci,cj;
  PetscReal H,xtmp[2],k1[2],k2[2],vel[2];
  PetscInt cellid;
  
  *_cellid = -1;
  
  H = -dt;
  
  /* k1 = H * v(x0) */
  for (d=0; d<2; d++) {
    k1[d] = H * vel0[d];
  }
  
  /* k2 = H * v(x0 + 0.5.k1) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k1[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v(sl,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k2[d] = H * vel[d];
  }
  
  for (d=0; d<2; d++) {
    x1[d] = x0[d] + k2[d];
  }
  point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  *_cellid = ci + cj * sl->mx[0];
}

void cRK3(CSLPoly sl,PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
          PetscReal x1[],PetscInt *_cellid)
{
  PetscInt d,inside,ci,cj;
  PetscReal H,xtmp[2],k1[2],k2[2],k3[2],vel[2];
  PetscInt cellid;
  
  *_cellid = -1;
  
  H = -dt;
  
  /* k1 = v(x0) */
  for (d=0; d<2; d++) {
    k1[d] = vel0[d];
  }
  
  /* k2 = v(x0 + 0.5.H.k1) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * H * k1[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v(sl,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k2[d] = vel[d];
  }
  
  /* k3 = v(x0 - H.k1 + 2.H.k2) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] -H * k1[d] + 2.0 * H * k2[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v(sl,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k3[d] = vel[d];
  }
  
  
  for (d=0; d<2; d++) {
    x1[d] = x0[d] + (H/6.0) * (k1[d] + 4.0*k2[d] + k3[d]);
  }
  point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  *_cellid = ci + cj * sl->mx[0];
}

void cRK4(CSLPoly sl,PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
          PetscReal x1[],PetscInt *_cellid)
{
  PetscInt d,inside,ci,cj;
  PetscReal H,xtmp[2],k1[2],k2[2],k3[2],k4[2],vel[2];
  PetscInt cellid;
  
  *_cellid = -1;
  
  H = -dt;
  
  /* k1 = H * v(x0) */
  for (d=0; d<2; d++) {
    k1[d] = H * vel0[d];
  }
  
  /* k2 = H * v(x0 + 0.5.k1) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k1[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v(sl,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k2[d] = H * vel[d];
  }

  /* k3 = H * v(x0 + 0.5.k2) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k2[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v(sl,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k3[d] = H * vel[d];
  }

  /* k4 = H * v(x0 + k3) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + k3[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v(sl,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k4[d] = H * vel[d];
  }

  
  for (d=0; d<2; d++) {
    x1[d] = x0[d] + (1.0/6.0) * (k1[d] + k4[d]) + (1.0/3.0) * (k2[d] + k3[d]);
  }
  point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  *_cellid = ci + cj * sl->mx[0];
}

static void advect_point_rk1234_divu_line_intersect(CharacteristicP point,PetscInt order,PetscReal dt,CSLPoly sl,PetscInt *intersect,PetscBool trace)
{
  PetscInt d;
  PetscReal dt0;
  PetscReal time;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel,dV = 0.0,dV0,divu_p = 0.0;
  PetscInt nsteps = 0;
  
  time = 0.0;
  ds = sl->dx[0];
  if (sl->dx[1] < sl->dx[0]) { ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  dV0 = point->volume;
  
  dt0 = 0.0;
  
  inside = 1;
  while (time <= dt) {
    PetscReal dtcfl[2];
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v(sl,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    /* compute a time-step */
    dtcfl[0] = ds/PetscAbsReal(vel_i[0]);
    dtcfl[1] = ds/PetscAbsReal(vel_i[1]);
    
    if (dtcfl[0] < dtcfl[1]) { dt0 = 0.25 * dtcfl[0]; }
    else {                     dt0 = 0.25 * dtcfl[1]; }
    if ((time+dt0) > dt)     { dt0 = dt - time;        emergency = 1; }
    
    
    /* update div from time to time + dt0 */
    sl->compute_div_u(sl,cellid,x0,&divu_p);
    dV = dV0 * PetscExpReal( - divu_p * (dt0));
    
    /* update position from time to time + dt0 using RK2 */
    switch (order) {
      case 1:
        cRK1(sl,dt0,(const PetscReal*)x0,(const PetscReal*)vel_i,x1,&cellid);
        break;
      case 2:
        cRK2(sl,dt0,(const PetscReal*)x0,(const PetscReal*)vel_i,x1,&cellid);
        break;
      case 3:
        cRK3(sl,dt0,(const PetscReal*)x0,(const PetscReal*)vel_i,x1,&cellid);
        break;
      case 4:
        cRK4(sl,dt0,(const PetscReal*)x0,(const PetscReal*)vel_i,x1,&cellid);
        break;
    }
    if (cellid == -1) {
      for (d=0; d<2; d++) {
        x1[d] = x0[d] - dt0 * vel_i[d];
      }
      inside = 0;
      break;
    }
    
    /* update for next step */
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    dV0 = dV;
    
    time += dt0;
    nsteps++;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    point->volume = dV;
    
    *intersect = 0;
  } else {
    PetscInt found_intersection = 0;
    PetscReal X[2],dt_star;
    
    line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                        sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                        X,&point->poly_facet_crossed,&found_intersection);
    
    if (found_intersection == 0) {
      printf("Failed to detemine intersection between outgoing characteristic and polygon\n");
      exit(1);
    }
    
    ci = (PetscInt)( (X[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (X[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    // dV0 in memory is that from the failed step which exited the domain
    // re-use the computed value of divu_p which is still in memory (corresponds to the foot of the characteristic which exited the domain)
    //sl->compute_div_u(sl,cellid,X,&divu_p);
    
    /*
     Determine the time step required to hit the boundary
     X[0] = x0[0] + dt^* vel_i[0] ==> solve for dt^*
     X[d] = x0[d] - dt^* vel_i[d];
     */
    dt_star = PetscAbsReal( -(X[0] - x0[0])/vel_i[0] ); /* take abs here */
    dV = dV0 * PetscExpReal( - divu_p * (dt_star)); /* update dV using previous value for dV0 AND the truncated time step dt* */
    time += dt_star;
    nsteps++;
    
    /* compute the contribution from moving the point outside the domain */
    dt_star = dt - time;
    if (dt_star < 0.0) {
      dt_star = 0.0;
    }
    
    dV0 = dV;
    sl->compute_div_u(sl,cellid,X,&divu_p);
    dV = dV0 * PetscExpReal( - divu_p * (dt_star)); /* update dV using previous value for dV0 AND any remainder required to reach dt */
    
    if (dV < 0.0) {
      dV = 0.0; /* assume that if the sign changed occured when crossing boundary - set dV = 0.0 */
      printf("[4] exiting[volume became negative]\n");
      exit(10);
    }
    
    for (d=0; d<2; d++) {
      point->departure_coor[d] = X[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
    point->volume = dV;
    
    *intersect = 1;
  }
}

void CSLPolyUpdate(CSLPoly sl,PetscReal time,PetscReal dt)
{
  PetscInt c,nc,nfields;
  PetscReal *value_k;
  
  
  nfields = sl->nfields;
  nc = sl->ncells_interior;

  value_k = (PetscReal*)malloc(sizeof(PetscReal)*nfields*sl->ncells);
  for (c=0; c<sl->ncells; c++) {
    PetscInt f;
    for (f=0; f<nfields; f++) {
      value_k[nfields*c+f] = sl->value_c[nfields*c+f];
    }
  }

  if (sl->c != NULL) {
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  }
  
  if (sl->c == NULL) {
    CharacteristicP traj_c;

    sl->c = (CharacteristicP*)malloc(sizeof(CharacteristicP)*nc);
    for (c=0; c<nc; c++) {
      traj_c = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
      sl->c[c] = traj_c;
    }
  }
  
  /* initialize */
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nc; c++) {
    CharacteristicP traj_c;

    traj_c = sl->c[c];

    traj_c->arrival_coor[0] = traj_c->arrival_coor[1] = 0.0;
    traj_c->arrival_cell = -1;
    traj_c->departure_coor[0] = traj_c->departure_coor[1] = 0.0;
    traj_c->departure_cell = -1;
    traj_c->volume = 0.0;
    traj_c->tag = TAG_UNINIT;
    traj_c->poly_facet_crossed = -1;
  }
  
  nc = 0;
  for (c=0; c<sl->ncells; c++) {
    CharacteristicP traj_c;
    
    if (sl->cell_list[c]->type != COOR_INTERIOR) continue;
    
    traj_c = sl->c[nc];

    traj_c->arrival_coor[0] = sl->cell_list[c]->coor[0];
    traj_c->arrival_coor[1] = sl->cell_list[c]->coor[1];
    traj_c->arrival_cell = c;
    traj_c->tag = TAG_GREEN;
    
    nc++;
  }
  
  /* advect */
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nc; c++) {
    PetscInt intersect;
    CharacteristicP traj_c;
    
    traj_c = sl->c[c];
    
    //advect_point_rk1(traj_c,dt,sl);
    advect_point_rk1_line_intersect(traj_c,dt,sl,&intersect);
  }

  /* init */
  //memset(value,0,sizeof(PetscReal)*sl->nfields*sl->ncells);
  
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nc; c++) {
    PetscReal phi[CSLPOLY_MAX_FIELDS];
    PetscReal *coor_c;
    PetscInt cellid,f;
    CharacteristicP traj_c;
    
    traj_c = sl->c[c];
    coor_c = traj_c->departure_coor;
    
    /* interpolate */
    if (traj_c->tag == TAG_YELLOW) {
#if 0
      PetscInt dcell,closest_facet;
      PetscReal phi_i[CSLPOLY_MAX_FIELDS];
      
      /* hacky shit which will be removed */
      if (coor_c[1] < 0.5) {
        func_5_3dof(coor_c,NULL,NULL,phi,NULL);
      } else {
        for (f=0; f<nfields; f++) {
          phi[f] = 0.0;
        }
      }
      
      
      printf("boundary coor %+1.4e %+1.4e\n",coor_c[0],coor_c[1]);
      printf("  exact  value %+1.8e %+1.8e %+1.8e\n",phi[0],phi[1],phi[2]);
      CSLPolyInterpolateAtBoundaryPoint(sl,traj_c->poly_facet_crossed,coor_c,phi_i);
      printf("  interp value %+1.8e %+1.8e %+1.8e\n",phi_i[0],phi_i[1],phi_i[2]);
#endif
      CSLPolyInterpolateAtBoundaryPoint(sl,traj_c->poly_facet_crossed,coor_c,phi);
      
      //SLInterpolateAtPoint(sl,coor_c,phi);
      /*
      dcell = traj_c->departure_cell;
      closest_facet = sl->cell_list[dcell]->closest_facet;
      for (f=0; f<nfields; f++) {
        phi[f] = sl->boundary_facet_list[closest_facet]->value[f];
      }
      */
      /*
      PetscInt ci,cj,ii,jj,cf;
      PetscReal minsep=1.0e32;
      
      closest_facet = -1;
      dcell = traj_c->departure_cell;
      jj = dcell/sl->mx[0];
      ii = dcell - jj*sl->mx[0];
      
      for (cj=jj-1; cj<=jj+1; cj++) {
        for (ci=ii-1; ci<ii+1; ci++) {
          PetscInt thiscell = ci + cj*sl->mx[0];
          PetscReal sep = 0.0;
          PetscReal *coorC,*coorF;
          
          if (sl->cell_list[dcell]->type == COOR_EXTERIOR) {
            cf = sl->cell_list[thiscell]->closest_facet;
            if (cf == -1) { continue; }
            
            coorF = sl->boundary_facet_list[cf]->coor;
            coorC = sl->cell_list[thiscell]->coor;
            
            sep += (coorC[0] - coorF[0])*(coorC[0] - coorF[0]);
            sep += (coorC[1] - coorF[1])*(coorC[1] - coorF[1]);
            if (sep < minsep) {
              minsep = sep;
              closest_facet = sl->cell_list[thiscell]->closest_facet;
            }
          }
        }
      }
      if (closest_facet == -1) {
        printf("closest_facet for boundary value interp failed\n");
        exit(1);
      }
      for (f=0; f<nfields; f++) {
        phi[f] = sl->boundary_facet_list[closest_facet]->value[f];
      }
      */
    } else {
      CSLPolyInterpolateAtPoint(sl,coor_c,phi);
    }
    //printf("%d %+1.3e %+1.3e %+1.3e %+1.3e %+1.2e %+1.3e\n",traj_c->arrival_cell,traj_c->arrival_coor[0],traj_c->arrival_coor[1],traj_c->departure_coor[0],traj_c->departure_coor[1],(double)traj_c->tag,phi[0]);
    
    /* insert new values */
    cellid = traj_c->arrival_cell;
    for (f=0; f<nfields; f++) {
      value_k[nfields*cellid+f] = phi[f];
    }
    //memcpy(&value_k[nfields*cellid],phi,sizeof(double)*nfields);
    
  }

  /* insert old values */
  //for (c=0; c<sl->ncells; c++) {
  //  if (sl->cell_list[c]->type != COOR_INTERIOR) {
  //    for (f=0; f<nfields; f++) {
  //      value[nfields*c+f] = value_k[nfields*c+f];
  //    }
  //  }
  //}

  /* update */
  //memcpy(sl->value_c,value_k,sizeof(double)*nfields*sl->ncells);
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nfields*sl->ncells; c++) {
    sl->value_c[c] = value_k[c];
  }
  
  if (sl->characteristics_cache == 0) {
    sl->characteristic_npoints = 0;
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  } else {
    sl->characteristic_npoints = nc;
  }
  free(value_k);
  //exit(0);
}

void CSLPolyUpdateI(CSLPoly sl,PetscReal time,PetscReal dt)
{
  PetscInt c,nc,nfields,nifields;
  PetscReal *value_k = NULL,*ivalue_k = NULL;
  
  
  nfields = sl->nfields;
  nifields = sl->nimplicitfields;
  nc = sl->ncells_interior;
  
  value_k = (PetscReal*)malloc(sizeof(PetscReal)*nfields*sl->ncells);
  for (c=0; c<sl->ncells; c++) {
    PetscInt f;
    for (f=0; f<nfields; f++) {
      value_k[nfields*c+f] = sl->value_c[nfields*c+f];
    }
  }

  if (nifields > 0) {
    ivalue_k = (PetscReal*)malloc(sizeof(PetscReal)*nifields*sl->ncells);
    for (c=0; c<sl->ncells; c++) {
      PetscInt f;
      for (f=0; f<nifields; f++) {
        ivalue_k[nifields*c+f] = sl->ivalue_c[nifields*c+f];
      }
    }
  }

  if (sl->c != NULL) {
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  }
  
  if (sl->c == NULL) {
    CharacteristicP traj_c;
    
    sl->c = (CharacteristicP*)malloc(sizeof(CharacteristicP)*nc);
    for (c=0; c<nc; c++) {
      traj_c = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
      sl->c[c] = traj_c;
    }
  }
  
  /* initialize */
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nc; c++) {
    CharacteristicP traj_c;
    
    traj_c = sl->c[c];
    
    traj_c->arrival_coor[0] = traj_c->arrival_coor[1] = 0.0;
    traj_c->arrival_cell = -1;
    traj_c->departure_coor[0] = traj_c->departure_coor[1] = 0.0;
    traj_c->departure_cell = -1;
    traj_c->volume = 0.0;
    traj_c->tag = TAG_UNINIT;
    traj_c->poly_facet_crossed = -1;
  }
  
  nc = 0;
  for (c=0; c<sl->ncells; c++) {
    CharacteristicP traj_c;
    
    if (sl->cell_list[c]->type != COOR_INTERIOR) continue;
    
    traj_c = sl->c[nc];
    
    traj_c->arrival_coor[0] = sl->cell_list[c]->coor[0];
    traj_c->arrival_coor[1] = sl->cell_list[c]->coor[1];
    traj_c->arrival_cell = c;
    traj_c->tag = TAG_GREEN;
    
    nc++;
  }
  
  /* advect */
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nc; c++) {
    PetscInt intersect;
    CharacteristicP traj_c;
    
    traj_c = sl->c[c];
    
    advect_point_rk1_line_intersect(traj_c,dt,sl,&intersect);
  }
  
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nc; c++) {
    PetscReal phi[CSLPOLY_MAX_FIELDS];
    PetscReal *coor_c;
    PetscInt cellid,f;
    CharacteristicP traj_c;
    
    traj_c = sl->c[c];
    coor_c = traj_c->departure_coor;
    
    /* interpolate */
    if (traj_c->tag == TAG_YELLOW) {
      CSLPolyInterpolateAtBoundaryPoint(sl,traj_c->poly_facet_crossed,coor_c,phi);
    } else {
      CSLPolyInterpolateAtPoint(sl,coor_c,phi);
    }
    
    /* insert new values */
    cellid = traj_c->arrival_cell;
    for (f=0; f<nfields; f++) {
      value_k[nfields*cellid+f] = phi[f];
    }
  }

  if (ivalue_k) {
    for (c=0; c<nc; c++) {
      PetscReal iphi[CSLPOLY_MAX_FIELDS];
      PetscReal *coor_c;
      PetscInt cellid,f;
      CharacteristicP traj_c;
      
      traj_c = sl->c[c];
      coor_c = traj_c->departure_coor;
      
      /* interpolate */
      if (traj_c->tag == TAG_YELLOW) {
        //CSLPolyInterpolateAtBoundaryPoint(sl,traj_c->poly_facet_crossed,coor_c,phi);
        sl->ifields_evaluate_at_boundarypoint(sl,traj_c->poly_facet_crossed,coor_c,iphi,sl->ictx);
      } else {
        //CSLPolyInterpolateAtPoint(sl,coor_c,phi);
        sl->ifields_evaluate_at_point(sl,coor_c,iphi,sl->ictx);
      }
      
      /* insert new values */
      cellid = traj_c->arrival_cell;
      for (f=0; f<nifields; f++) {
        ivalue_k[nifields*cellid+f] = iphi[f];
      }
    }
  }
  
  /* update */
#ifdef HAVE_OMP
#pragma omp parallel for
#endif
  for (c=0; c<nfields*sl->ncells; c++) {
    sl->value_c[c] = value_k[c];
  }
  if (ivalue_k) {
    for (c=0; c<nifields*sl->ncells; c++) {
      sl->ivalue_c[c] = ivalue_k[c];
    }
  }

  if (sl->characteristics_cache == 0) {
    sl->characteristic_npoints = 0;
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  } else {
    sl->characteristic_npoints = nc;
  }
  if (value_k) { free(value_k); }
  if (ivalue_k) { free(ivalue_k); }
}

void CSLPolyUpdate_Conservative(CSLPoly sl,PetscReal time,PetscReal dt)
{
  CharacteristicP traj_c;
  PetscInt c,f,nc,nfields;
  PetscReal *value_k;
  PetscInt s,smx,smy,sm,si,sj;
  CharacteristicP *sub_c;
  PetscReal sdx[2],dV_a,dV_d,buffer[CSLPOLY_MAX_FIELDS];
  PetscInt order = 1;
  
  PetscOptionsGetInt(NULL,NULL,"-csl_rk",&order,NULL);
  if (!sl->compute_div_u) {
    printf("error[CSLPolyUpdate_Conservative]: Require a method to evaluate div_u\n");
    exit(1);
  }
  
  smx = sl->subcellmx[0];
  smy = sl->subcellmx[1];
  sm = smx * smy;
  
  sdx[0] = sl->dx[0] / ((PetscReal)smx);
  sdx[1] = sl->dx[1] / ((PetscReal)smy);
  dV_a = sl->dx[0] * sl->dx[1];
  
  
  nfields = sl->nfields;
  nc = sl->ncells_interior;
  
  value_k = (PetscReal*)malloc(sizeof(PetscReal)*nfields*sl->ncells);
  for (c=0; c<sl->ncells; c++) {
    for (f=0; f<nfields; f++) {
      value_k[nfields*c+f] = sl->value_c[nfields*c+f];
    }
  }
  
  /* prepare the cell characteristics */
  if (sl->c != NULL) {
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  }
  
  if (sl->c == NULL) {
    sl->c = (CharacteristicP*)malloc(sizeof(CharacteristicP)*nc);
    for (c=0; c<nc; c++) {
      traj_c = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
      sl->c[c] = traj_c;
    }
  }
  
  /* prepare the sub-cell characteristics */
  sub_c = (CharacteristicP*)malloc(sizeof(CharacteristicP)*sm);
  for (s=0; s<sm; s++) {
    sub_c[s] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  }

  /* initialize the cell characteristics */
  for (c=0; c<nc; c++) {
    traj_c = sl->c[c];
    
    traj_c->arrival_coor[0] = traj_c->arrival_coor[1] = 0.0;
    traj_c->arrival_cell = -1;
    traj_c->departure_coor[0] = traj_c->departure_coor[1] = 0.0;
    traj_c->departure_cell = -1;
    traj_c->volume = 0.0;
    traj_c->tag = TAG_UNINIT;
  }
  
  nc = 0;
  for (c=0; c<sl->ncells; c++) {
    
    if (sl->cell_list[c]->type != COOR_INTERIOR) continue;
    
    traj_c = sl->c[nc];
    
    traj_c->arrival_coor[0] = sl->cell_list[c]->coor[0];
    traj_c->arrival_coor[1] = sl->cell_list[c]->coor[1];
    traj_c->arrival_cell = c;
    traj_c->tag = TAG_GREEN;
    
    nc++;
  }
  
  /* advect */
  for (c=0; c<nc; c++) {
    traj_c = sl->c[c];
    
    advect_point_rk1(traj_c,dt,sl);
  }
  
  
  nc = 0;
  for (c=0; c<sl->ncells; c++) {
    CharacteristicP traj_cell;
    PetscBool trace = PETSC_FALSE;
    
    if (sl->cell_list[c]->type != COOR_INTERIOR) continue;
    
    //if (c == 552) { trace = PETSC_TRUE; }
    
    /* init this cells values */
    //for (f=0; f<nfields; f++) {
    //  value_k[nfields*c+f] = 0.0;
    //}

    
    /* initialize the cell characteristics */
    for (s=0; s<sm; s++) {
      traj_c = sub_c[s];
      
      traj_c->arrival_coor[0] = traj_c->arrival_coor[1] = 0.0;
      traj_c->arrival_cell = c;
      traj_c->departure_coor[0] = traj_c->departure_coor[1] = 0.0;
      traj_c->departure_cell = -1;
      traj_c->volume0 = sdx[0] * sdx[1];
      traj_c->volume  = sdx[0] * sdx[1];
      traj_c->tag = TAG_GREEN;
    }

#if 0
    for (sj=0; sj<smy; sj++) {
      for (si=0; si<smx; si++) {
        PetscReal x0,y0;
        PetscInt inside;
        PetscInt s = si + sj*smx;
        
        traj_c = sub_c[s];
        
        x0 =  sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
        y0 =  sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
        
        traj_c->arrival_coor[0] = x0 + 0.5 * sdx[0] + si * sdx[0];
        traj_c->arrival_coor[1] = y0 + 0.5 * sdx[1] + sj * sdx[1];
        
        //printf("[cell %d] coor %+1.4e,%+1.4e vol %+1.4e \n",c,traj_c->arrival_coor[0],traj_c->arrival_coor[1],traj_c->volume);

        point_in_polygon(sl->poly_npoints,sl->poly_coor,traj_c->arrival_coor,&inside);
        if (inside == 0) {
          traj_c->tag = TAG_RED;
        }
      }
    }
#endif
    
#if 1
    {
      PetscInt nqp1 = smx;
      PetscInt nqp2 = smy;
      PetscReal *xi1,*w1;
      PetscReal *xi2,*w2;
      PetscInt inside,k;
      PetscReal x0[2],Ni[4],coor_v[2*4],coor_q[2];
      
      _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
      _PetscDTGaussJacobiQuadrature(1,nqp2,-1.0,1.0,&xi2,&w2);

      x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
      x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];

      coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
      coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
      coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
      coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];

      for (sj=0; sj<smy; sj++) {
        for (si=0; si<smx; si++) {
          PetscInt s = si + sj*smx;

          Ni[0] = 0.25 * (1.0 - xi1[si]) * (1.0 - xi2[sj]);
          Ni[1] = 0.25 * (1.0 + xi1[si]) * (1.0 - xi2[sj]);
          Ni[2] = 0.25 * (1.0 - xi1[si]) * (1.0 + xi2[sj]);
          Ni[3] = 0.25 * (1.0 + xi1[si]) * (1.0 + xi2[sj]);

          coor_q[0] = coor_q[1] = 0.0;
          for (k=0; k<4; k++) {
            coor_q[0] += Ni[k] * coor_v[2*k+0];
            coor_q[1] += Ni[k] * coor_v[2*k+1];
          }

          traj_c = sub_c[s];
          
          traj_c->arrival_coor[0] = coor_q[0];
          traj_c->arrival_coor[1] = coor_q[1];
          traj_c->volume0 = 0.25 * w1[si] * w2[sj] * (sl->dx[0]*sl->dx[1]);
          traj_c->volume  = 0.25 * w1[si] * w2[sj] * (sl->dx[0]*sl->dx[1]);
          //printf("[cell %d] coor %+1.4e,%+1.4e vol %+1.4e \n",c,traj_c->arrival_coor[0],traj_c->arrival_coor[1],traj_c->volume);
          point_in_polygon(sl->poly_npoints,sl->poly_coor,traj_c->arrival_coor,&inside);
          if (inside == 0) {
            traj_c->tag = TAG_RED;
          }
        }
      }
      
      PetscFree(xi1);
      PetscFree(xi2);
      PetscFree(w1);
      PetscFree(w2);
    }
#endif
     
    dV_a = 0.0;
    dV_d = 0.0;
    for (f=0; f<nfields; f++) {
      buffer[f] = 0.0;
    }

    for (s=0; s<sm; s++) {
      PetscReal phi[CSLPOLY_MAX_FIELDS];
      PetscReal *coor_c;
      PetscInt intersect;
      
      traj_c = sub_c[s];
      if (traj_c->tag == TAG_RED) { continue; }
      
      /* advect */
      //advect_point_rk1_divu_line_intersect(traj_c,dt,sl,&intersect,trace);
      //advect_point_rk2_divu_line_intersect(traj_c,dt,sl,&intersect,trace);
      advect_point_rk1234_divu_line_intersect(traj_c,order,dt,sl,&intersect,trace);

      /* interpolate */
      coor_c = traj_c->departure_coor;
      
      if (traj_c->tag == TAG_YELLOW) {
        CSLPolyInterpolateAtBoundaryPoint(sl,traj_c->poly_facet_crossed,coor_c,phi);
      } else {
        CSLPolyInterpolateAtPoint(sl,coor_c,phi);
      }
      
      /* sum values for the arrival cell */
      for (f=0; f<nfields; f++) {
        buffer[f] += phi[f] * PetscAbsReal(traj_c->volume);
      }
      dV_a += traj_c->volume0;
      dV_d += PetscAbsReal(traj_c->volume);
    }
    
    if (dV_a < 1.0e-20) {
      printf("arrival cell < 1.0e-20!\n");
      exit(0);
    }
    /* normalize by the arrival cell volume */
    for (f=0; f<nfields; f++) {
      if (trace) {printf("    buffer[%d] = %+1.4e\n",f,buffer[f]);}
      buffer[f] = buffer[f] / dV_a;
      if (trace) {printf("    buffer[%d]/dV_a = %+1.4e\n",f,buffer[f]);}
    }
    
    for (f=0; f<nfields; f++) {
      value_k[nfields*c+f] = buffer[f];
    }
    
    traj_cell = sl->c[nc];
    traj_cell->volume0 = dV_a;
    traj_cell->volume  = dV_d;
    
    if (trace) {
      printf("    dv_a = %+1.4e\n",dV_a);
      printf("    dv_d = %+1.4e\n",dV_d);
      printf("    dv_a/dv_d = %+1.4e\n",dV_a/dV_d);
    }
    
    nc++;
  }

  /* update internal state */
  memcpy(sl->value_c,value_k,sizeof(PetscReal)*nfields*sl->ncells);
  
  if (sl->characteristics_cache == 0) {
    sl->characteristic_npoints = 0;
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  } else {
    sl->characteristic_npoints = nc;
  }

  for (s=0; s<sm; s++) {
    free(sub_c[s]);
  }
  free(sub_c);

  free(value_k);
}

void CSLPolyUpdateI_Conservative(CSLPoly sl,PetscReal time,PetscReal dt)
{
  CharacteristicP traj_c;
  PetscInt c,f,nc,nfields,nifields;
  PetscReal *value_k = NULL, *ivalue_k = NULL;
  PetscInt s,smx,smy,sm,si,sj;
  CharacteristicP *sub_c;
  PetscReal sdx[2],dV_a,dV_d,buffer[CSLPOLY_MAX_FIELDS];
  
  if (!sl->compute_div_u) {
    printf("error[CSLPolyUpdate_Conservative]: Require a method to evaluate div_u\n");
    exit(1);
  }
  
  smx = sl->subcellmx[0];
  smy = sl->subcellmx[1];
  sm = smx * smy;
  
  sdx[0] = sl->dx[0] / ((PetscReal)smx);
  sdx[1] = sl->dx[1] / ((PetscReal)smy);
  dV_a = sl->dx[0] * sl->dx[1];
  
  
  nfields = sl->nfields;
  nifields = sl->nimplicitfields;
  nc = sl->ncells_interior;
  
  value_k = (PetscReal*)malloc(sizeof(PetscReal)*nfields*sl->ncells);
  for (c=0; c<sl->ncells; c++) {
    for (f=0; f<nfields; f++) {
      value_k[nfields*c+f] = sl->value_c[nfields*c+f];
    }
  }
  if (nifields > 0) {
    ivalue_k = (PetscReal*)malloc(sizeof(PetscReal)*nifields*sl->ncells);
    for (c=0; c<sl->ncells; c++) {
      for (f=0; f<nifields; f++) {
        ivalue_k[nifields*c+f] = sl->ivalue_c[nifields*c+f];
      }
    }
  }
  
  /* prepare the cell characteristics */
  if (sl->c != NULL) {
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  }
  
  if (sl->c == NULL) {
    sl->c = (CharacteristicP*)malloc(sizeof(CharacteristicP)*nc);
    for (c=0; c<nc; c++) {
      traj_c = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
      sl->c[c] = traj_c;
    }
  }
  
  /* prepare the sub-cell characteristics */
  sub_c = (CharacteristicP*)malloc(sizeof(CharacteristicP)*sm);
  for (s=0; s<sm; s++) {
    sub_c[s] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  }
  
  /* initialize the cell characteristics */
  for (c=0; c<nc; c++) {
    traj_c = sl->c[c];
    
    traj_c->arrival_coor[0] = traj_c->arrival_coor[1] = 0.0;
    traj_c->arrival_cell = -1;
    traj_c->departure_coor[0] = traj_c->departure_coor[1] = 0.0;
    traj_c->departure_cell = -1;
    traj_c->volume = 0.0;
    traj_c->tag = TAG_UNINIT;
  }
  
  nc = 0;
  for (c=0; c<sl->ncells; c++) {
    
    if (sl->cell_list[c]->type != COOR_INTERIOR) continue;
    
    traj_c = sl->c[nc];
    
    traj_c->arrival_coor[0] = sl->cell_list[c]->coor[0];
    traj_c->arrival_coor[1] = sl->cell_list[c]->coor[1];
    traj_c->arrival_cell = c;
    traj_c->tag = TAG_GREEN;
    
    nc++;
  }
  
  /* advect */
  for (c=0; c<nc; c++) {
    traj_c = sl->c[c];
    
    advect_point_rk1(traj_c,dt,sl);
  }
  
  nc = 0;
  for (c=0; c<sl->ncells; c++) {
    CharacteristicP traj_cell;
    PetscBool trace = PETSC_FALSE;
    
    if (sl->cell_list[c]->type != COOR_INTERIOR) continue;
    
    //if (c == 11932) { trace = PETSC_TRUE; }
    
    /* init this cells values */
    //for (f=0; f<nfields; f++) {
    //  value_k[nfields*c+f] = 0.0;
    //}
    
    
    /* initialize the cell characteristics */
    for (s=0; s<sm; s++) {
      traj_c = sub_c[s];
      
      traj_c->arrival_coor[0] = traj_c->arrival_coor[1] = 0.0;
      traj_c->arrival_cell = c;
      traj_c->departure_coor[0] = traj_c->departure_coor[1] = 0.0;
      traj_c->departure_cell = -1;
      traj_c->volume = sdx[0] * sdx[1];
      traj_c->tag = TAG_GREEN;
    }
    
    for (sj=0; sj<smy; sj++) {
      for (si=0; si<smx; si++) {
        PetscReal x0,y0;
        PetscInt inside;
        PetscInt s = si + sj*smx;
        
        traj_c = sub_c[s];
        
        x0 =  sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
        y0 =  sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
        
        traj_c->arrival_coor[0] = x0 + 0.5 * sdx[0] + si * sdx[0];
        traj_c->arrival_coor[1] = y0 + 0.5 * sdx[1] + sj * sdx[1];
        
        point_in_polygon(sl->poly_npoints,sl->poly_coor,traj_c->arrival_coor,&inside);
        if (inside == 0) {
          traj_c->tag = TAG_RED;
        }
      }
    }
    
    dV_a = 0.0;
    dV_d = 0.0;
    for (s=0; s<sm; s++) {
      PetscInt intersect;
      
      traj_c = sub_c[s];
      if (traj_c->tag == TAG_RED) { continue; }
      
      /* advect */
      advect_point_rk1_divu_line_intersect(traj_c,dt,sl,&intersect,trace);
      
      dV_a += sdx[0] * sdx[1];
      dV_d += PetscAbsReal(traj_c->volume);
    }

    for (f=0; f<nfields; f++) {
      buffer[f] = 0.0;
    }
    for (s=0; s<sm; s++) {
      PetscReal phi[CSLPOLY_MAX_FIELDS];
      PetscReal *coor_c;
      
      traj_c = sub_c[s];
      if (traj_c->tag == TAG_RED) { continue; }
      
      /* interpolate */
      coor_c = traj_c->departure_coor;
      
      if (traj_c->tag == TAG_YELLOW) {
        CSLPolyInterpolateAtBoundaryPoint(sl,traj_c->poly_facet_crossed,coor_c,phi);
      } else {
        CSLPolyInterpolateAtPoint(sl,coor_c,phi);
      }
      
      /* sum values for the arrival cell */
      for (f=0; f<nfields; f++) {
        buffer[f] += phi[f] * PetscAbsReal(traj_c->volume);
      }
    }

    /* normalize by the arrival cell volume */
    for (f=0; f<nfields; f++) {
      buffer[f] = buffer[f] / dV_a;
    }
    
    for (f=0; f<nfields; f++) {
      value_k[nfields*c+f] = buffer[f];
    }
    
    if (ivalue_k) {
      for (f=0; f<nifields; f++) {
        buffer[f] = 0.0;
      }
      for (s=0; s<sm; s++) {
        PetscReal iphi[CSLPOLY_MAX_FIELDS];
        PetscReal *coor_c;
        
        traj_c = sub_c[s];
        if (traj_c->tag == TAG_RED) { continue; }
        
        /* interpolate */
        coor_c = traj_c->departure_coor;

        /* interpolate */
        if (traj_c->tag == TAG_YELLOW) {
          sl->ifields_evaluate_at_boundarypoint(sl,traj_c->poly_facet_crossed,coor_c,iphi,sl->ictx);
        } else {
          sl->ifields_evaluate_at_point(sl,coor_c,iphi,sl->ictx);
        }
        
        /* sum values for the arrival cell */
        for (f=0; f<nifields; f++) {
          buffer[f] += iphi[f] * PetscAbsReal(traj_c->volume);
        }
      }
      /* normalize by the departure cell volume */
      for (f=0; f<nifields; f++) {
        buffer[f] = buffer[f] / dV_a;
      }

      for (f=0; f<nifields; f++) {
        ivalue_k[nifields*c+f] = buffer[f];
      }
    }

    traj_cell = sl->c[nc];
    traj_cell->volume0 = dV_a;
    traj_cell->volume  = dV_d;
    
    nc++;
  }
  
  /* update internal state */
  memcpy(sl->value_c,value_k,sizeof(PetscReal)*nfields*sl->ncells);
  if (ivalue_k) {
    memcpy(sl->ivalue_c,ivalue_k,sizeof(PetscReal)*nifields*sl->ncells);
  }
  
  if (sl->characteristics_cache == 0) {
    sl->characteristic_npoints = 0;
    for (c=0; c<nc; c++) {
      free(sl->c[c]);
    }
    free(sl->c);
    sl->c = NULL;
  } else {
    sl->characteristic_npoints = nc;
  }
  
  for (s=0; s<sm; s++) {
    free(sub_c[s]);
  }
  free(sub_c);
  
  if (value_k) { free(value_k); }
  if (ivalue_k) { free(ivalue_k); }
}

void CSLPolyCellViewVTS(CSLPoly sl,const char fname[])
{
  PetscInt nx,ny,mx,my,i,j,f;
  FILE*	vtk_fp = NULL;
  char fieldname[1024];

  
  if ((vtk_fp = fopen(fname,"w")) == NULL)  {
    printf("Cannot open file %s",fname);
    exit(1);
  }

  nx = sl->nx[0];
  ny = sl->nx[1];
  mx = nx - 1;
  my = ny - 1;
  
  fprintf(vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
  fprintf(vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
  
  fprintf(vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", 0,nx-1, 0,ny-1, 0,0);
  fprintf(vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", 0,nx-1, 0,ny-1, 0,0);
  
  /* VTS COORD DATA */
  fprintf( vtk_fp, "    <Points>\n");
  fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp, "      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->coor_v[2*(i+j*nx)], sl->coor_v[2*(i+j*nx)+1], 0.0 );
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");
  fprintf(vtk_fp, "    </Points>\n");
  
  
  /* VTS NODAL DATA */
  fprintf(vtk_fp, "    <PointData>\n");
  
  /* velocity */
  fprintf(vtk_fp, "      <DataArray Name=\"velocity\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->vel_v[2*(i+j*nx)], sl->vel_v[2*(i+j*nx)+1], 0.0 );
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");

  fprintf(vtk_fp, "      <DataArray Name=\"vert_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf(vtk_fp,"%d ", (int)sl->type_v[i+j*nx]);
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");
  
  fprintf(vtk_fp, "      <DataArray Name=\"vert_is_interior\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf(vtk_fp,"%d ", (int)sl->is_interior_v[i+j*nx]);
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");
  
  fprintf(vtk_fp, "    </PointData>\n");
  
  /* VTS CELL DATA */
  fprintf(vtk_fp, "    <CellData>\n");
  
  for (f=0; f<sl->nfields; f++) {
    sprintf(fieldname,"field_%d",f);
    fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",fieldname);
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", sl->value_c[sl->nfields*(i+j*mx)+f]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }

  for (f=0; f<sl->nimplicitfields; f++) {
    sprintf(fieldname,"ifield_%d",f);
    fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",fieldname);
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", sl->ivalue_c[sl->nimplicitfields*(i+j*mx)+f]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }

  fprintf(vtk_fp, "      <DataArray Name=\"div_vel\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (j=0; j<my; j++) {
    for (i=0; i<mx; i++) {
      fprintf(vtk_fp,"%1.6e ", sl->divu_c[i+j*mx]);
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");

  fprintf(vtk_fp, "      <DataArray Name=\"cell_type\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (j=0; j<my; j++) {
    for (i=0; i<mx; i++) {
      fprintf(vtk_fp,"%d ", (int)sl->cell_list[i+j*mx]->type);
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");
  
  fprintf(vtk_fp, "      <DataArray Name=\"cell_is_interior\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (j=0; j<my; j++) {
    for (i=0; i<mx; i++) {
      fprintf(vtk_fp,"%d ", (int)(int)sl->cell_list[i+j*mx]->is_interior);
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");
    
  fprintf(vtk_fp, "    </CellData>\n");
  
  
  /* VTS HEADER - CLOSE */
  fprintf(vtk_fp, "    </Piece>\n");
  fprintf(vtk_fp, "  </StructuredGrid>\n");
  fprintf(vtk_fp, "</VTKFile>\n");
  
  fclose(vtk_fp);
}

void CSLPolyFacetViewVTU(CSLPoly sl,const char fname[])
{
  PetscInt i;
  FILE*	vtk_fp = NULL;
  
  if ((vtk_fp = fopen(fname,"w")) == NULL)  {
    printf("Cannot open file %s",fname);
    exit(1);
  }
  
  fprintf(vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
  fprintf(vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
  fprintf(vtk_fp, "  <UnstructuredGrid>\n");
  fprintf( vtk_fp,"  <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",sl->boundary_nfacets,sl->boundary_nfacets);

  fprintf(vtk_fp,"    <Cells>\n");
  fprintf(vtk_fp,"      <DataArray Name=\"connectivity\" type=\"Int32\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", i);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"      <DataArray Name=\"offsets\" type=\"Int32\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", i+1);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"      <DataArray Name=\"types\" type=\"UInt8\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", 1);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"    </Cells>\n");

  /* VTS COORD DATA */
  fprintf( vtk_fp,"    <Points>\n");
  fprintf( vtk_fp,"      <DataArray Name=\"Points\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->boundary_facet_list[i]->coor[0], sl->boundary_facet_list[i]->coor[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"    </Points>\n");
  
  /* VTS NODAL DATA */
  fprintf(vtk_fp,"    <PointData>\n");
  
  fprintf(vtk_fp,"      <DataArray Name=\"velocity\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->boundary_facet_list[i]->velocity[0], sl->boundary_facet_list[i]->velocity[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"normal\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->boundary_facet_list[i]->normal[0], sl->boundary_facet_list[i]->normal[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"is_inflow\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", (int)sl->boundary_facet_list[i]->is_inflow);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"label\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", (int)sl->boundary_facet_list[i]->label);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"    </PointData>\n");
  
  fprintf(vtk_fp,"    <CellData>\n");
  fprintf(vtk_fp,"    </CellData>\n");
  
  /* VTS HEADER - CLOSE */
  fprintf(vtk_fp,"    </Piece>\n");
  fprintf(vtk_fp,"  </UnstructuredGrid>\n");
  fprintf(vtk_fp,"</VTKFile>\n");
  
  fclose(vtk_fp);
}

void CSLPolyGenericFacetBCIterator(CSLPoly p,PetscInt edge,void (*F)(const PetscReal*,const PetscReal*,const PetscReal *,PetscReal*,void*),void *ctx)
{
  PetscInt s;
  PetscReal *vv;
  
  /* call user function for inflowing boundaries */
  for (s=0; s<p->boundary_nfacets; s++) {
    const PetscReal *coor;
    const PetscReal *normal;
    const PetscReal *velocity;
    
    if (p->boundary_facet_list[s]->is_inflow != 1) continue;
    
    if (p->boundary_facet_list[s]->label != edge) continue;
    coor     = p->boundary_facet_list[s]->coor;
    normal   = p->boundary_facet_list[s]->normal;
    velocity = p->boundary_facet_list[s]->velocity;
    vv = p->boundary_facet_list[s]->value;
    
    F(coor,normal,velocity,vv,ctx);
    p->boundary_facet_list[s]->bcset = 1;
  }
}

void CSLPolyGenericFacetBCIteratorI(CSLPoly p,PetscInt edge,void (*F)(const PetscReal*,const PetscReal*,const PetscReal *,PetscReal*,void*),void *ctx)
{
  PetscInt s;
  PetscReal *vv;
  
  /* call user function for inflowing boundaries */
  for (s=0; s<p->boundary_nfacets; s++) {
    const PetscReal *coor;
    const PetscReal *normal;
    const PetscReal *velocity;
    
    if (p->boundary_facet_list[s]->is_inflow != 1) continue;
    
    if (p->boundary_facet_list[s]->label != edge) continue;
    coor     = p->boundary_facet_list[s]->coor;
    normal   = p->boundary_facet_list[s]->normal;
    velocity = p->boundary_facet_list[s]->velocity;
    vv = p->boundary_facet_list[s]->ivalue;
    
    F(coor,normal,velocity,vv,ctx);
    p->boundary_facet_list[s]->bcset = 1;
  }
}

void CSLPolyApplyBC(CSLPoly p,PetscReal time,void (*func)(CSLPoly,PetscReal,void*),void* ctx)
{
  PetscInt s;
  
  /* init all flags to 0 */
  for (s=0; s<p->boundary_nfacets; s++) {
    p->boundary_facet_list[s]->bcset = 0;
  }
  
  func(p,time,ctx);
  
  /* check inflow conditions are specified by the user */
  for (s=0; s<p->boundary_nfacets; s++) {
    BoundaryP facet = p->boundary_facet_list[s];
    
    if (facet->is_inflow != 1) continue;
    if (facet->bcset == 0) {
      printf("error: inflow bc not set : edge label %d : coor %+1.4e %+1.4e \n",facet->label,facet->coor[0],facet->coor[1]);
      exit(1);
    }
  }
  
}

void CSLPolyInitializeBC(CSLPoly p)
{
  PetscInt s;
  
  /* init all flags to 0 */
  for (s=0; s<p->boundary_nfacets; s++) {
    p->boundary_facet_list[s]->bcset = 0;
  }
}

void CSLPolyVerifyBC(CSLPoly p)
{
  PetscInt s;
  
  /* check inflow conditions are specified by the user */
  for (s=0; s<p->boundary_nfacets; s++) {
    BoundaryP facet = p->boundary_facet_list[s];
    
    if (facet->is_inflow != 1) continue;
    if (facet->bcset == 0) {
      printf("error: inflow bc not set : edge label %d : coor %+1.4e %+1.4e \n",facet->label,facet->coor[0],facet->coor[1]);
      exit(1);
    }
  }
}

void CSLPolySetSubcellResolution(CSLPoly p,PetscInt mx,PetscInt my)
{
  p->subcellmx[0] = mx;
  p->subcellmx[1] = my;
}

void CSLPolyGetCellStencil(CSLPoly p,PetscInt ci,PetscInt cj,PetscInt s[])
{
  PetscInt cni,cnj,k,ii,jj,cell_neighbour;
  
  for (k=0; k<9; k++) {
    s[k] = -1;
  }
  
  for (jj=0; jj<3; jj++) {
    cnj = cj - 1 + jj;
    
    if (cnj < 0) continue;
    if (cnj > (p->mx[1]-1)) continue;
    
    for (ii=0; ii<3; ii++) {
      cni = ci - 1 + ii;

      if (cni < 0) continue;
      if (cni > (p->mx[0]-1)) continue;
      
      cell_neighbour = CSLCellIdx(p,cni,cnj);
      s[ii+jj*3] = cell_neighbour;
    }
  }
}

void CSLPolyGetVertexStencil(CSLPoly p,PetscInt ci,PetscInt cj,PetscInt s[])
{
  PetscInt cni,cnj,k,ii,jj,cell_neighbour;
  
  for (k=0; k<9; k++) {
    s[k] = -1;
  }
  
  for (jj=0; jj<3; jj++) {
    cnj = cj - 1 + jj;
    
    if (cnj < 0) continue;
    if (cnj > (p->nx[1]-1)) continue;
    
    for (ii=0; ii<3; ii++) {
      cni = ci - 1 + ii;
      
      if (cni < 0) continue;
      if (cni > (p->nx[0]-1)) continue;
      
      cell_neighbour = CSLNodeIdx(p,cni,cnj);
      s[ii+jj*3] = cell_neighbour;
    }
  }
}

/*
 Averages interior values to exterior cells.
 This is intended only for cell fields which are not updated via the CSL scheme.
 Examples of this include div(u) -> divu_c.
 The averaging does not require facet values and thus differs from the CSLPolyReconstructExteriorCellFields().
 Its purpose is to enable consistent CSpine or Weno4 interpolation of auxillary fields to be 
 conducted within the interior without edge effects.
*/
void CSLPolyExtrapolateExteriorCellFields(CSLPoly p,PetscInt nfields,PetscReal field_c[])
{
  PetscInt *tag_c; /* 1 for interior cells : 2 for buffer with at least 1 interior neighbour: 3 for buffer with only buffer neighbours */
  PetscInt k,f,ncells;
  PetscInt ci,cj,stencil[9];
  PetscInt it,maxit = 4;
  PetscInt *sum_c;
  PetscReal *avg_value_c;
  
  
  ncells = p->ncells;
  PetscMalloc1(ncells,&tag_c);
  PetscMalloc1(ncells,&sum_c);
  PetscMalloc1(ncells*nfields,&avg_value_c);
  
  for (k=0; k<ncells; k++) {
    sum_c[k] = 0;
    tag_c[k] = -1;
    if (p->cell_list[k]->is_interior == 1) {
      tag_c[k] = 0;
    } else {
      /* zero buffer fields */
      for (f=0; f<nfields; f++) {
        field_c[nfields*k + f] = 0.0;
        avg_value_c[nfields*k + f] = 0.0;
      }
    }
  }
  
  for (it=0; it<maxit; it++) {
    
    for (cj=0; cj<p->mx[1]; cj++) {
      for (ci=0; ci<p->mx[0]; ci++) {
        PetscInt cell_center,cell_neighbour;
        
        cell_center = CSLCellIdx(p,ci,cj);
        if (tag_c[cell_center] != -1) { continue; }
        
        CSLPolyGetCellStencil(p,ci,cj,stencil);
        
        for (k=0; k<9; k++) {
          cell_neighbour = stencil[k];
          
          /* skip stencil points outside CSL domain */
          if (cell_neighbour == -1) { continue; }
          
          /* skip counting if the neigbour is in fact the current ci,cj cell */
          if (cell_center == cell_neighbour) { continue; }

          if (tag_c[cell_neighbour] == -1) { continue; }

          /* skip counting if neighbour doesn't have tag_c[] = it */
          if (tag_c[cell_neighbour] <= it) {

            sum_c[cell_center]++; // avg
            //sum_c[cell_center] = 1; // max
            
            tag_c[cell_center] = it + 1;
            
            for (f=0; f<nfields; f++) {
              avg_value_c[nfields*cell_center + f] += field_c[nfields*cell_neighbour + f]; // avg
              
              //if (field_c[nfields*cell_neighbour + f] > avg_value_c[nfields*cell_center + f]) {
              //  avg_value_c[nfields*cell_center + f] = field_c[nfields*cell_neighbour + f]; // max
              //}
              
            }
          }
        }
      }
    }
    
    /* do average of all cells with tag_c[] != -1 (exterior) or 0 (interior) */
    for (k=0; k<ncells; k++) {
      if (tag_c[k] == (it+1)) {
        
        if (sum_c[k] == 0) {
          printf("[error]: ====== CSLPolyExtrapolateExteriorCellFields ======\n");
          printf("[error]: field extrapolation for cell %d failed\n",k);
          printf("[error]: denominator is zero \n");
          printf("[error]: [iteration %d]: updating cell k %d \n",it,k);
          printf("[error]: replacing %+1.4e with nearest(max) %+1.4e/0\n",field_c[nfields*k + 0],avg_value_c[nfields*k + 0]);
          exit(1);
        }
        
        //printf("[iteration %d]: updating cell k %d \n",it,k);
        for (f=0; f<nfields; f++) {
          //if (f==0) printf("  replacing %+1.4e with nearest(max) %+1.4e\n",field_c[nfields*k + f],avg_value_c[nfields*k + f]);
          field_c[nfields*k + f] = avg_value_c[nfields*k + f] / ((PetscReal)sum_c[k]);
        }
      }
    }
    
  }
  
  PetscFree(sum_c);
  PetscFree(tag_c);
  PetscFree(avg_value_c);
}

void CSLPolyExtrapolateExteriorVertexFields(CSLPoly p,PetscInt nfields,PetscReal field_v[])
{
  PetscInt *tag_v; /* 1 for interior cells : 2 for buffer with at least 1 interior neighbour: 3 for buffer with only buffer neighbours */
  PetscInt k,f,nverts;
  PetscInt ci,cj,stencil[9];
  PetscInt it,maxit = 2;
  PetscInt *sum_v;
  PetscReal *avg_value_v;
  
  
  nverts = p->nvert;
  PetscMalloc1(nverts,&tag_v);
  PetscMalloc1(nverts,&sum_v);
  PetscMalloc1(nverts*nfields,&avg_value_v);
  
  for (k=0; k<nverts; k++) {
    sum_v[k] = 0;
    tag_v[k] = -1;
    if (p->is_interior_v[k] == 1) {
      tag_v[k] = 0;
    } else {
      /* zero buffer fields */
      for (f=0; f<nfields; f++) {
        field_v[nfields*k + f] = 0.0;
        avg_value_v[nfields*k + f] = 0.0;
      }
    }
  }
  
  for (it=0; it<maxit; it++) {
    
    for (cj=0; cj<p->nx[1]; cj++) {
      for (ci=0; ci<p->nx[0]; ci++) {
        PetscInt vert_center,vert_neighbour;
        
        vert_center = CSLNodeIdx(p,ci,cj);
        if (tag_v[vert_center] != -1) { continue; }
        
        CSLPolyGetVertexStencil(p,ci,cj,stencil);
        
        for (k=0; k<9; k++) {
          vert_neighbour = stencil[k];
          
          /* skip stencil points outside CSL domain */
          if (vert_neighbour == -1) { continue; }
          
          /* skip counting if the neigbour is in fact the current ci,cj cell */
          if (vert_center == vert_neighbour) { continue; }
          
          if (tag_v[vert_neighbour] == -1) { continue; }
          
          /* skip counting if neighbour doesn't have tag_c[] = it */
          if (tag_v[vert_neighbour] <= it) {
            
            sum_v[vert_center]++; // avg
            //sum_c[cell_center] = 1; // max
            
            tag_v[vert_center] = it + 1;
            
            for (f=0; f<nfields; f++) {
              avg_value_v[nfields*vert_center + f] += field_v[nfields*vert_neighbour + f]; // avg
              
            }
          }
        }
      }
    }
    
    /* do average of all cells with tag_c[] != -1 (exterior) or 0 (interior) */
    for (k=0; k<nverts; k++) {
      if (tag_v[k] == (it+1)) {
        
        if (sum_v[k] == 0) {
          printf("[error]: ====== CSLPolyExtrapolateExteriorVertexFields ======\n");
          printf("[error]: field extrapolation for vertex %d failed\n",k);
          printf("[error]: denominator is zero \n");
          printf("[error]: [iteration %d]: updating vertex k %d \n",it,k);
          printf("[error]: replacing %+1.4e with nearest(max) %+1.4e/0\n",field_v[nfields*k + 0],avg_value_v[nfields*k + 0]);
          exit(1);
        }

        for (f=0; f<nfields; f++) {
          field_v[nfields*k + f] = avg_value_v[nfields*k + f] / ((PetscReal)sum_v[k]);
        }
      }
    }
    
  }
  
  PetscFree(sum_v);
  PetscFree(tag_v);
  PetscFree(avg_value_v);
}

void CSLPolyGetCoordinatesVertex(CSLPoly p,const PetscReal **coor)
{
  if (coor) {
    *coor = p->coor_v;
  }
}

void CSLPolyGetCoordinateVertexPoint(CSLPoly p,PetscInt vidx,const PetscReal **coor)
{
  if (coor) {
    if (vidx >= p->nvert) {
      printf("error[CSLPolyGetCoordinateVertexPoint] vidx %d must be < %d\n",vidx,p->nvert);
      exit(1);
    }
    *coor = &p->coor_v[2*vidx];
  }
}

void CSLPolyGetCoordinateCellCentroidPoint(CSLPoly p,PetscInt cidx,const PetscReal **coor)
{
  if (coor) {
    if (cidx >= p->ncells) {
      printf("error[CSLPolyGetCoordinateCellCentroidPoint] cidx %d must be < %d\n",cidx,p->ncells);
      exit(1);
    }
    *coor = p->cell_list[cidx]->coor;
  }
}

void CSLPolySetCacheCharacteristics(CSLPoly p)
{
  p->characteristics_cache = 1;
}

void CSLPolyGetCachedCharacteristics(CSLPoly p,PetscInt *nchar,CharacteristicP **c)
{
  if (p->characteristics_cache == 1) {
    *nchar = p->characteristic_npoints;
    *c = p->c;
  } else {
    *nchar = 0;
    *c = NULL;
  }
}


void interpolate_billinear_v__(CSLPoly p,const PetscReal vel_v[],PetscInt cellid,PetscReal coor[],PetscReal v[])
{
  PetscInt nidx[4];
  PetscInt k,ci,cj;
  PetscReal xi[2],x0[2],Ni[4];
  
  cj = cellid/p->mx[0];
  ci = cellid - cj * p->mx[0];
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = CSLNodeIdx(p,ci  ,cj);
  nidx[1] = CSLNodeIdx(p,ci+1,cj);
  nidx[2] = CSLNodeIdx(p,ci  ,cj+1);
  nidx[3] = CSLNodeIdx(p,ci+1,cj+1);
  
  x0[0] = p->gmin[0] + ci * p->dx[0];
  x0[1] = p->gmin[1] + cj * p->dx[1];
  
  xi[0] = 2.0*(coor[0]-x0[0])/p->dx[0] - 1.0;
  xi[1] = 2.0*(coor[1]-x0[1])/p->dx[1] - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  v[0] = v[1] = 0.0;
  for (k=0; k<4; k++) {
    v[0] += Ni[k] * vel_v[2*nidx[k]+0];
    v[1] += Ni[k] * vel_v[2*nidx[k]+1];
  }
  
}

void cRK4__(CSLPoly sl,const PetscReal vel_v[],PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
            PetscReal x1[],PetscInt *_cellid)
{
  PetscInt d,inside,ci,cj;
  PetscReal H,xtmp[2],k1[2],k2[2],k3[2],k4[2],vel[2];
  PetscInt cellid;
  
  *_cellid = -1;
  
  H = -dt;
  
  /* k1 = H * v(x0) */
  for (d=0; d<2; d++) {
    k1[d] = H * vel0[d];
  }
  
  /* k2 = H * v(x0 + 0.5.k1) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k1[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v__(sl,vel_v,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k2[d] = H * vel[d];
  }
  
  /* k3 = H * v(x0 + 0.5.k2) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k2[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v__(sl,vel_v,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k3[d] = H * vel[d];
  }
  
  /* k4 = H * v(x0 + k3) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + k3[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_billinear_v__(sl,vel_v,cellid,xtmp,vel);
  for (d=0; d<2; d++) {
    k4[d] = H * vel[d];
  }
  
  
  for (d=0; d<2; d++) {
    x1[d] = x0[d] + (1.0/6.0) * (k1[d] + k4[d]) + (1.0/3.0) * (k2[d] + k3[d]);
  }
  point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  *_cellid = ci + cj * sl->mx[0];
}

void interpolate_w4_v__(CSLPoly p,const PetscReal vel_v[],PetscInt cellid,PetscReal coor[],PetscReal v[],PetscInt direction)
{
  PetscInt nidx[4];
  PetscInt k,ci,cj;
  PetscReal xs[4],fs[4];
  
  cj = cellid/p->mx[0];
  ci = cellid - cj * p->mx[0];

  switch (direction) {
    case 0:
      nidx[0] = ci-1;
      nidx[1] = ci  ;
      nidx[2] = ci+1;
      nidx[3] = ci+2;
      
      for (k=0; k<4; k++) {
        xs[k] = p->gmin[ direction ] + nidx[k] * p->dx[ direction ];
      }
      
      for (k=0; k<4; k++) {
        PetscReal v0,v1;
        PetscInt n0,n1;
        
        n0 = CSLNodeIdx(p,nidx[k],cj);
        n1 = CSLNodeIdx(p,nidx[k],cj+1);
        
        v0 = vel_v[2*n0 + 0];
        v1 = vel_v[2*n1 + 0];
        fs[k] = 0.5 * (v0 + v1);
      }
      
      weno4_McD( xs , fs , coor[direction] , &v[direction] );
      v[1] = 0.0;
      break;

    case 1:
      nidx[0] = cj-1;
      nidx[1] = cj  ;
      nidx[2] = cj+1;
      nidx[3] = cj+2;
      
      for (k=0; k<4; k++) {
        xs[k] = p->gmin[ direction ] + nidx[k] * p->dx[ direction ];
      }
      
      for (k=0; k<4; k++) {
        PetscReal v0,v1;
        PetscInt n0,n1;
        
        n0 = CSLNodeIdx(p,ci  ,nidx[k]);
        n1 = CSLNodeIdx(p,ci+1,nidx[k]);
        
        v0 = vel_v[2*n0 + 1];
        v1 = vel_v[2*n1 + 1];
        fs[k] = 0.5 * (v0 + v1);
      }
      
      v[0] = 0.0;
      weno4_McD( xs , fs , coor[direction] , &v[direction] );
      break;
      
    default:
      break;
  }
}

void cRK4__w4(CSLPoly sl,const PetscReal vel_v[],PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
            PetscReal x1[],PetscInt *_cellid,PetscInt direction)
{
  PetscInt d,inside,ci,cj;
  PetscReal H,xtmp[2],k1[2],k2[2],k3[2],k4[2],vel[2];
  PetscInt cellid;
  
  *_cellid = -1;
  
  H = -dt;
  
  /* k1 = H * v(x0) */
  for (d=0; d<2; d++) {
    k1[d] = H * vel0[d];
  }
  
  /* k2 = H * v(x0 + 0.5.k1) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k1[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_w4_v__(sl,vel_v,cellid,xtmp,vel,direction);
  for (d=0; d<2; d++) {
    k2[d] = H * vel[d];
  }
  
  /* k3 = H * v(x0 + 0.5.k2) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + 0.5 * k2[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_w4_v__(sl,vel_v,cellid,xtmp,vel,direction);
  for (d=0; d<2; d++) {
    k3[d] = H * vel[d];
  }
  
  /* k4 = H * v(x0 + k3) */
  for (d=0; d<2; d++) {
    xtmp[d] = x0[d] + k3[d];
  }
  
  point_in_polygon(sl->poly_npoints, sl->poly_coor, xtmp, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (xtmp[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (xtmp[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  cellid = ci + cj * sl->mx[0];
  
  interpolate_w4_v__(sl,vel_v,cellid,xtmp,vel,direction);
  for (d=0; d<2; d++) {
    k4[d] = H * vel[d];
  }
  
  
  for (d=0; d<2; d++) {
    x1[d] = x0[d] + (1.0/6.0) * (k1[d] + k4[d]) + (1.0/3.0) * (k2[d] + k3[d]);
  }
  point_in_polygon(sl->poly_npoints, sl->poly_coor, x1, &inside);
  if (inside == 0) return;
  
  ci = (PetscInt)( (x1[0] - sl->gmin[0])/sl->dx[0] );
  cj = (PetscInt)( (x1[1] - sl->gmin[1])/sl->dx[1] );
  if (ci == sl->mx[0]) ci--;
  if (cj == sl->mx[1]) cj--;
  *_cellid = ci + cj * sl->mx[0];
}


#if 0
void impose_vertex_velocity_1(CSLPoly p)
{
  PetscInt v;
  
  for (v=0; v<p->nvert; v++) {
    PetscReal *coor;
    
    coor = &p->coor_v[2*v];
    
    p->vel_v[2*v+0] = -0.1;
    p->vel_v[2*v+1] = 1.01;
  }
}

void impose_facet_bc_1(CSLPoly p,PetscInt inflow)
{
  PetscInt s,f;

  /* call user function for inflowing boundaries */
  if (inflow == 1) {
    for (s=0; s<p->boundary_nfacets; s++) {
      PetscReal *coor;
      
      coor = p->boundary_facet_list[s]->coor;
      
      if (p->boundary_facet_list[s]->is_inflow == 1) {
        for (f=0; f<p->nfields; f++) {
          p->boundary_facet_list[s]->value[f] = 1.0;
        }
      }
    }
  }

  /* extrapolate from the interior */
  if (inflow == 0) {
    for (s=0; s<p->boundary_nfacets; s++) {
      PetscReal *coor;
      
      coor = p->boundary_facet_list[s]->coor;
      
      if (p->boundary_facet_list[s]->is_inflow == 0) {
        PetscInt cellid;
        
        /* get index of nearest interior cell */
        cellid = p->boundary_facet_list[s]->closest_interior_cell;
        
        for (f=0; f<p->nfields; f++) {
          p->boundary_facet_list[s]->value[f] = p->value_c[p->nfields*cellid + f];
        }
      }
    }
  }
}

PetscInt main_ex2(void)
{
  CSLPoly sl;
  PetscReal *xy;
  PetscInt np;
  
  np = 4;
  xy = malloc(sizeof(PetscReal)*2*np);
  xy[2*0+0] = 0.0;  xy[2*0+1] = 0.0;
  xy[2*1+0] = 1.3;  xy[2*1+1] = 0.1;
  xy[2*2+0] = 1.5;  xy[2*2+1] = 2.0;
  xy[2*3+0] = 0.1;  xy[2*3+1] = 0.4;
  
  
  CSLPolyCreate(&sl);
  CSLPolySetBlockSize(sl,1);
  CSLPolySetPolygonPoints(sl,np,xy,NULL);
  CSLPolySetDomainDefault(sl,0.01);
  
  /* setup - one time */
  CSLPolySetUpMesh(sl);
  BoundaryPCreate(sl->ds,sl->poly_npoints,sl->poly_coor,sl->poly_edge_label,&sl->boundary_nfacets,&sl->boundary_facet_list);
  
  CSLPolySetUpFields(sl);
  
  CSLPolyMarkCells(sl);
  CSLPolyMarkVertices(sl);
  
  printf("<start facet label>\n");
  CSLPolyLabelFacets(sl);
  printf("<end facet label>\n");
  printf("<start cell label>\n");
  CSLPolyLabelCells(sl);
  printf("<end cell label>\n");
  
  /* setup - time dependent */
  impose_vertex_velocity_1(sl);
  CSLPolyMarkInflowFacets(sl);

  impose_facet_bc_1(sl,1);
  impose_facet_bc_1(sl,0);
  CSLPolyReconstructExteriorCellFields(sl);
  
  return(0);
}

void func_3(PetscReal x[],PetscReal v[])
{
  v[0] = sin(1.2*M_PI*x[0])*cos(M_PI*x[1]*3.0);
}

void func_3_2(PetscReal x[],PetscReal v[])
{
  v[0] = 1.0;
  if (x[0] < 0.4) {
    v[0] = 0.0;
  }
  if (x[1] > 0.7) {
    v[0] = 0.0;
  }
}

void impose_vertex_velocity_3(CSLPoly p)
{
  PetscInt v;
  
  for (v=0; v<p->nvert; v++) {
    PetscReal *coor;
    
    coor = &p->coor_v[2*v];
    
    p->vel_v[2*v+0] = -0.05; // -0.6
    p->vel_v[2*v+1] = 1.01; // 1.01
  }
}


void impose_ic_3(CSLPoly p,void (*F)(PetscReal*,PetscReal*))
{
  PetscInt c,f;
  PetscReal vv[10];
  
  for (c=0; c<p->ncells; c++) {
    PetscReal *coor;
    
    coor = p->cell_list[c]->coor;
    if (p->cell_list[c]->type == COOR_INTERIOR) {

      F(coor,vv);
      for (f=0; f<p->nfields; f++) {
        p->value_c[p->nfields*c+f] = vv[f];
      }
    }
  }
}

void impose_facet_bc_3(CSLPoly p,PetscInt inflow,void (*F)(PetscReal*,PetscReal*))
{
  PetscInt s,f;
  PetscReal vv[10];
  
  /* call user function for inflowing boundaries */
  if (inflow == 1) {
    for (s=0; s<p->boundary_nfacets; s++) {
      PetscReal *coor;
      
      coor = p->boundary_facet_list[s]->coor;
      
      if (p->boundary_facet_list[s]->is_inflow == 1) {
        
        F(coor,vv);
        for (f=0; f<p->nfields; f++) {
          p->boundary_facet_list[s]->value[f] = vv[f];
        }
      }
    }
  }
  
  /* extrapolate from the interior */
  if (inflow == 0) {
    for (s=0; s<p->boundary_nfacets; s++) {
      PetscReal *coor;
      
      coor = p->boundary_facet_list[s]->coor;
      
      if (p->boundary_facet_list[s]->is_inflow == 0) {
        PetscInt cellid;
        
        /* get index of nearest interior cell */
        cellid = p->boundary_facet_list[s]->closest_interior_cell;
        
        for (f=0; f<p->nfields; f++) {
          p->boundary_facet_list[s]->value[f] = p->value_c[p->nfields*cellid + f];
        }
      }
    }
  }
}

void impose_facet_bc_4(CSLPoly p,PetscInt inflow,PetscInt edge,void (*F)(PetscReal*,PetscReal*))
{
  PetscInt s,f;
  PetscReal vv[10];
  
  /* call user function for inflowing boundaries */
  if (inflow == 1) {
    for (s=0; s<p->boundary_nfacets; s++) {
      PetscReal *coor;
      
      if (p->boundary_facet_list[s]->label != edge) continue;
      coor = p->boundary_facet_list[s]->coor;
      
      if (p->boundary_facet_list[s]->is_inflow == 1) {
        
        F(coor,vv);
        for (f=0; f<p->nfields; f++) {
          p->boundary_facet_list[s]->value[f] = vv[f];
        }
      }
    }
  }
  
  /* extrapolate from the interior */
  if (inflow == 0) {
    for (s=0; s<p->boundary_nfacets; s++) {
      PetscReal *coor;
      
      if (p->boundary_facet_list[s]->label != edge) continue;
      coor = p->boundary_facet_list[s]->coor;
      
      if (p->boundary_facet_list[s]->is_inflow == 0) {
        PetscInt cellid;
        
        /* get index of nearest interior cell */
        cellid = p->boundary_facet_list[s]->closest_interior_cell;
        
        for (f=0; f<p->nfields; f++) {
          p->boundary_facet_list[s]->value[f] = p->value_c[p->nfields*cellid + f];
        }
      }
    }
  }
}

PetscInt main_ex3(void)
{
  CSLPoly sl;
  PetscReal *xy;
  PetscInt np;
  
  PetscInt nx,i,j;
  PetscReal dx,dy;
  PetscReal *meshcoor;
  void (*ff)(PetscReal*,PetscReal*) = func_3;
  
  
  np = 4;
  xy = malloc(sizeof(PetscReal)*2*np);
  xy[2*0+0] = 0.0;  xy[2*0+1] = 0.0;
  xy[2*1+0] = 1.0;  xy[2*1+1] = 0.0;
  xy[2*2+0] = 1.0;  xy[2*2+1] = 1.0;
  xy[2*3+0] = 0.0;  xy[2*3+1] = 1.0;
  
  
  CSLPolyCreate(&sl);
  CSLPolySetBlockSize(sl,1);
  //CSLPolySetInterpolateType(sl,I_CSPLINE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE);
  CSLPolySetInterpolateType(sl,I_WENO4);
  CSLPolySetPolygonPoints(sl,np,xy,NULL);
  CSLPolySetDomainDefault(sl,0.04);
  
  /* setup - one time */
  CSLPolySetUpMesh(sl);
  BoundaryPCreate(sl->ds,sl->poly_npoints,sl->poly_coor,sl->poly_edge_label,&sl->boundary_nfacets,&sl->boundary_facet_list);
  
  CSLPolySetUpFields(sl);
  
  CSLPolyMarkCells(sl);
  CSLPolyMarkVertices(sl);
  
  printf("#<start facet label>\n");
  CSLPolyLabelFacets(sl);
  printf("#<end facet label>\n");
  printf("#<start cell label>\n");
  CSLPolyLabelCells(sl);
  printf("#<end cell label>\n");
  
  /* setup - time dependent */
  impose_vertex_velocity_3(sl);
  CSLPolyMarkInflowFacets(sl);
  
  impose_facet_bc_3(sl,1,ff);
  impose_facet_bc_3(sl,0,ff);
  
  //impose_ic_3(sl,ff);
  CSLPolyReconstructExteriorCellFields(sl);
  
  //CellView_gnuplot(sl,"cell.gp");
  //FacetView_gnuplot(sl,"facet.gp");
  
  nx = 337;
  
  dx = (sl->gmax[0]-sl->gmin[0])/((PetscReal)(nx-1));
  dy = (sl->gmax[1]-sl->gmin[1])/((PetscReal)(nx-1));
  meshcoor = (PetscReal*)malloc(2*nx*nx*sizeof(PetscReal));
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i + j *nx;
      meshcoor[2*idx+0] = sl->gmin[0] + i*dx;
      meshcoor[2*idx+1] = sl->gmin[1] + j*dy;
    }
  }
  
  /*
   FILE *fp;
   PetscReal min_p = 1.0e32;
   fp = fopen("interp.gp","w");
   for (j=0; j<nx; j++) {
   for (i=0; i<nx; i++) {
   PetscReal *coor;
   PetscInt idx;
   PetscReal phi_p;
   
   idx = i + j *nx;
   coor = &meshcoor[2*idx];
   
   SLInterpolateAtPoint(sl,coor,&phi_p);
   if (phi_p < min_p) { min_p = phi_p; }
   fprintf(fp,"%+1.6e %+1.6e %+1.6e\n",coor[0],coor[1],phi_p);
   } fprintf(fp,"\n");
   }
   fclose(fp);
   printf("#min(interp) %+1.8e\n",min_p);
   */
  PetscReal *f1;
  
  f1 = (PetscReal*)malloc(sizeof(PetscReal)*sl->ncells);
  memcpy(f1,sl->value_c,sizeof(PetscReal)*sl->ncells);
  
  
  //CellFieldView_gnuplot(sl,"c0.gp");
  //CellView_gnuplot(sl,"cb0.gp");
  CellViewVTS(sl,"c0.vts");
  FacetViewVTS(sl,"f0.vtu");
  for (PetscInt k=1; k<2000; k++) {
    char name[129];
    
    impose_facet_bc_3(sl,1,ff);
    impose_facet_bc_3(sl,0,ff);
    
    CSLPolyReconstructExteriorCellFields(sl);
    
    CSLPolyUpdate(sl,0.0,0.001,NULL);
    
    impose_facet_bc_3(sl,0,ff);
    
    if (k%5==0) {
      //sprintf(name,"c%d.gp",k);
      //CellFieldView_gnuplot(sl,name);
      
      //sprintf(name,"cb%d.gp",k);
      //CellView_gnuplot(sl,name);
      
      sprintf(name,"c%d.vts",k);
      CellViewVTS(sl,name);
      
      printf("step %d : time %+1.4e\n",k,0.01*k);
    }
  }
  
  return(0);
}

PetscInt main_ex4(void)
{
  CSLPoly sl;
  PetscReal *xy;
  PetscInt np;
  
  PetscInt nx,i,j;
  PetscReal dx,dy;
  PetscReal *meshcoor;
  void (*ff)(PetscReal*,PetscReal*) = func_3;
  PetscInt edge,edge_label[8];
  
  np = 8;
  xy = malloc(sizeof(PetscReal)*2*np);
  xy[2*0+0] = 0.0;  xy[2*0+1] = 0.0; edge_label[0] = 1;
  xy[2*1+0] = 0.5;  xy[2*1+1] = 0.0; edge_label[1] = 1;
  xy[2*2+0] = 1.0;  xy[2*2+1] = 0.0; edge_label[2] = 1;
  
  xy[2*3+0] = 1.0;  xy[2*3+1] = 0.5; edge_label[3] = 0;
  xy[2*4+0] = 1.0;  xy[2*4+1] = 1.0; edge_label[4] = 0;
  
  xy[2*5+0] = 0.5;  xy[2*5+1] = 1.0; edge_label[5] = 0;
  xy[2*6+0] = 0.0;  xy[2*6+1] = 1.0; edge_label[6] = 0;
  
  xy[2*7+0] = 0.0;  xy[2*7+1] = 0.5; edge_label[7] = 0;
  
  
  CSLPolyCreate(&sl);
  CSLPolySetBlockSize(sl,1);
  //CSLPolySetInterpolateType(sl,I_CSPLINE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE);
  CSLPolySetInterpolateType(sl,I_WENO4);
  CSLPolySetPolygonPoints(sl,np,xy,edge_label);
  CSLPolySetDomainDefault(sl,0.005);
  
  /* setup - one time */
  CSLPolySetUpMesh(sl);
  BoundaryPCreate(sl->ds,sl->poly_npoints,sl->poly_coor,sl->poly_edge_label,&sl->boundary_nfacets,&sl->boundary_facet_list);
  
  CSLPolySetUpFields(sl);
  
  CSLPolyMarkCells(sl);
  CSLPolyMarkVertices(sl);
  
  printf("#<start facet label>\n");
  CSLPolyLabelFacets(sl);
  printf("#<end facet label>\n");
  printf("#<start cell label>\n");
  CSLPolyLabelCells(sl);
  printf("#<end cell label>\n");
  
  /* setup - time dependent */
  impose_vertex_velocity_3(sl);
  CSLPolyMarkInflowFacets(sl);
  
  edge = 0;
  //impose_facet_bc_4(sl,1,edge,ff);
  //impose_facet_bc_4(sl,0,edge,ff);
  
  edge = 1;
  impose_facet_bc_4(sl,1,edge,ff);
  impose_facet_bc_4(sl,0,edge,ff);
  
  //impose_ic_3(sl,ff);
  CSLPolyReconstructExteriorCellFields(sl);
  
  //CellView_gnuplot(sl,"cell.gp");
  //FacetView_gnuplot(sl,"facet.gp");
  
  nx = 337;
  
  dx = (sl->gmax[0]-sl->gmin[0])/((PetscReal)(nx-1));
  dy = (sl->gmax[1]-sl->gmin[1])/((PetscReal)(nx-1));
  meshcoor = (PetscReal*)malloc(2*nx*nx*sizeof(PetscReal));
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i + j *nx;
      meshcoor[2*idx+0] = sl->gmin[0] + i*dx;
      meshcoor[2*idx+1] = sl->gmin[1] + j*dy;
    }
  }
  
  /*
   FILE *fp;
   PetscReal min_p = 1.0e32;
   fp = fopen("interp.gp","w");
   for (j=0; j<nx; j++) {
   for (i=0; i<nx; i++) {
   PetscReal *coor;
   PetscInt idx;
   PetscReal phi_p;
   
   idx = i + j *nx;
   coor = &meshcoor[2*idx];
   
   SLInterpolateAtPoint(sl,coor,&phi_p);
   if (phi_p < min_p) { min_p = phi_p; }
   fprintf(fp,"%+1.6e %+1.6e %+1.6e\n",coor[0],coor[1],phi_p);
   } fprintf(fp,"\n");
   }
   fclose(fp);
   printf("#min(interp) %+1.8e\n",min_p);
   */
  PetscReal *f1;
  
  f1 = (PetscReal*)malloc(sizeof(PetscReal)*sl->ncells);
  memcpy(f1,sl->value_c,sizeof(PetscReal)*sl->ncells);
  
  
  //CellFieldView_gnuplot(sl,"c0.gp");
  //CellView_gnuplot(sl,"cb0.gp");
  CellViewVTS(sl,"c0.vts");
  FacetViewVTS(sl,"f0.vtu");
  
  for (PetscInt k=1; k<2000; k++) {
    char name[129];
    
    edge = 0;
    //impose_facet_bc_4(sl,1,edge,ff);
    //impose_facet_bc_4(sl,0,edge,ff);
    
    edge = 1;
    impose_facet_bc_4(sl,1,edge,ff);
    impose_facet_bc_4(sl,0,edge,ff);
    
    CSLPolyReconstructExteriorCellFields(sl);
    
    CSLPolyUpdate(sl,0.0,0.001,NULL);
    
    edge = 0;
    //impose_facet_bc_4(sl,0,edge,ff);
    
    edge = 1;
    impose_facet_bc_4(sl,0,edge,ff);
    
    if (k%5==0) {
      //sprintf(name,"c%d.gp",k);
      //CellFieldView_gnuplot(sl,name);
      
      //sprintf(name,"cb%d.gp",k);
      //CellView_gnuplot(sl,name);
      
      sprintf(name,"c%d.vts",k);
      CellViewVTS(sl,name);
      
      printf("step %d : time %+1.4e\n",k,0.01*k);
    }
  }
  
  return(0);
}

/* dirichlet example */
void func_5_3dof(const PetscReal x[],const PetscReal n[],const PetscReal vel[],PetscReal field[],void *ctx)
{
  field[0] = sin(1.2*M_PI*x[0])*cos(M_PI*x[1]*3.0);
  field[1] = 0.5*sin(1.2*M_PI*x[0])*cos(M_PI*x[1]*3.0);
  field[2] = 1.0e-3*sin(1.2*M_PI*x[0])*cos(M_PI*x[1]*3.0);
}

void func_5_null_3dof(const PetscReal x[],const PetscReal n[],const PetscReal vel[],PetscReal field[],void *ctx)
{
  field[0] = 0.0;
  field[1] = 0.0;
  field[2] = 0.0;
}

/*
 flux[] = field velocity[]
 
 bc = flux[].normal[] / velocity[].normal[]
*/
void func_5_neumann_3dof(const PetscReal x[],const PetscReal n[],const PetscReal vel[],PetscReal field[],void *ctx)
{
  PetscReal flux[2],vdotn;
  
  flux[0] = -1.0;
  flux[1] = -1.0;
  vdotn = n[0]*vel[0] + n[1]*vel[1];
  
  field[0] = (flux[0]*n[0] + flux[1]*n[1]) / vdotn;
  field[1] = 0.5*(flux[0]*n[0] + flux[1]*n[1]) / vdotn;
  field[2] = 1.0e-3*(flux[0]*n[0] + flux[1]*n[1]) / vdotn;
}

void func_5_neumann_2_3dof(const PetscReal x[],const PetscReal n[],const PetscReal vel[],PetscReal field[],void *ctx)
{
  PetscReal fn,flux[2],vdotn;
  
  fn = -1.0;
  flux[0] = fn * n[0];
  flux[1] = fn * n[1];
  vdotn = n[0]*vel[0] + n[1]*vel[1];
  
  field[0] = (flux[0]*n[0] + flux[1]*n[1]) / vdotn;
  field[1] = 0.5*(flux[0]*n[0] + flux[1]*n[1]) / vdotn;
  field[2] = 1.0e-3*(flux[0]*n[0] + flux[1]*n[1]) / vdotn;
  //printf("fn %+1.4e <imposed>: fn* %+1.4e <reconstructed from field[]>\n",fn,flux[0]*n[0]+flux[1]*n[1]);
}

void impose_vertex_velocity_5(CSLPoly p)
{
  PetscInt v;
  
  for (v=0; v<p->nvert; v++) {
    PetscReal *coor;
    
    coor = &p->coor_v[2*v];
    
    p->vel_v[2*v+0] = -4.05; // -0.6
    p->vel_v[2*v+1] = 1.01; // 1.01
  }
}

void impose_vertex_velocity_v6(CSLPoly p)
{
  PetscInt v;
  
  for (v=0; v<p->nvert; v++) {
    PetscReal *coor;
    
    coor = &p->coor_v[2*v];
    
    p->vel_v[2*v+0] = -4.05 * (coor[0]*coor[0]); // -0.6
    p->vel_v[2*v+1] = 1.01 * (coor[1]*coor[1]); // 1.01
  }
}

void compute_div_u_v6(CSLPoly p,PetscInt cellid,PetscReal coor[],PetscReal *divu)
{
  *divu = -4.05*2.0*coor[0] + 1.01*2.0*coor[1];
}

void bc_ex5(CSLPoly p,PetscReal time,void *ctx)
{
  PetscInt edge;
  void (*ff)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*) = func_5_3dof;
  //void (*ff)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*) = func_5_neumann_2_3dof;

  edge = 1;
  GenericFacetBCIterator(p,edge,ff,NULL);
  
  edge = 2;
  GenericFacetBCIterator(p,edge,func_5_null_3dof,NULL);
}
            

PetscInt main_ex5(void)
{
  const PetscReal eps = 1.0e-10;
  CSLPoly sl;
  PetscReal *xy;
  PetscInt np;
  
  PetscInt nx,i,j,k;
  PetscReal dx,dy,dt,time;
  PetscReal *meshcoor;
  PetscInt edge_label[8];
  void (*define_velocity)(CSLPoly) = impose_vertex_velocity_v6;
  void (*compute_div_u)(CSLPoly,int,PetscReal*,PetscReal*) = compute_div_u_v6;
  char name[129];
  
  np = 8;
  xy = malloc(sizeof(PetscReal)*2*np);
  xy[2*0+0] = -eps;  xy[2*0+1] = -eps; edge_label[0] = 1;
  xy[2*1+0] = 0.5;  xy[2*1+1] = -eps; edge_label[1] = 1;
  xy[2*2+0] = 1.0+eps;  xy[2*2+1] = -eps; edge_label[2] = 1;
  
  xy[2*3+0] = 1.0+eps;  xy[2*3+1] = 0.5; edge_label[3] = 2;
  xy[2*4+0] = 1.0+eps;  xy[2*4+1] = 1.0+eps; edge_label[4] = 0;
  
  xy[2*5+0] = 0.5;  xy[2*5+1] = 1.0+eps; edge_label[5] = 0;
  xy[2*6+0] = 0.0;  xy[2*6+1] = 1.0+eps; edge_label[6] = 0;
  
  xy[2*7+0] = -eps;  xy[2*7+1] = 0.5; edge_label[7] = 0;
  
  
  CSLPolyCreate(&sl);
  CSLPolySetBlockSize(sl,3);
  //CSLPolySetInterpolateType(sl,I_CSPLINE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(sl,I_WENO4);
  
  //CSLPolySetApplyGlobalBounds(sl);
  //CSLPolySetGlobalBounds(sl,0,0.0,1.0e32);
  //CSLPolySetGlobalBounds(sl,1,0.0,1.0e32);
  //CSLPolySetGlobalBounds(sl,2,0.0,1.0e32);
  
  CSLPolySetPolygonPoints(sl,np,xy,edge_label);
  {
    PetscReal gmin[] = { 0.0, 0.0 };
    PetscReal gmax[] = { 1.0, 1.0 };
    
    //CSLPolySetDomainDefault(sl,0.005);
    CSLPolySetDomain(sl,gmin,gmax,0.001);
  }
  
  sl->compute_div_u = compute_div_u;
  
  /* setup - one time */
  CSLPolySetUpMesh(sl);
  //BoundaryPCreate(sl->ds,sl->poly_npoints,sl->poly_coor,sl->poly_edge_label,&sl->boundary_nfacets,&sl->boundary_facet_list);
  BoundaryPCreate2(sl);
  
  CSLPolySetUpFields(sl);
  
  CSLPolyMarkCells(sl);
  CSLPolyMarkVertices(sl);
  
  printf("#<start facet label>\n");
  CSLPolyLabelFacets(sl);
  printf("#<end facet label>\n");
  printf("#<start cell label>\n");
  CSLPolyLabelCells(sl);
  printf("#<end cell label>\n");
  
  /* setup - time dependent */
  define_velocity(sl);
  CSLPolyMarkInflowFacets(sl);
  
  CSLPolyApplyBC(sl,0.0,bc_ex5,NULL);
  
  CSLPolyReconstructExteriorCellFields(sl);
  CSLPolyReconstructExteriorFacetFields(sl);
  
  //CellView_gnuplot(sl,"cell.gp");
  //FacetView_gnuplot(sl,"facet.gp");
  
  nx = 337;
  
  dx = (sl->gmax[0]-sl->gmin[0])/((PetscReal)(nx-1));
  dy = (sl->gmax[1]-sl->gmin[1])/((PetscReal)(nx-1));
  meshcoor = (PetscReal*)malloc(2*nx*nx*sizeof(PetscReal));
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i + j *nx;
      meshcoor[2*idx+0] = sl->gmin[0] + i*dx;
      meshcoor[2*idx+1] = sl->gmin[1] + j*dy;
    }
  }
  
  
  //CellFieldView_gnuplot(sl,"c0.gp");
  //CellView_gnuplot(sl,"cb0.gp");
  //CellViewVTS(sl,"c0.vts");
  //FacetViewVTS(sl,"f0.vtu");
  
  dt = 1.0e-1/5.0;
  time = dt;
  for (k=1; k<=25; k++) {
    
    //if (k < 10) { dt = 1.0e-3; }
    //else { dt = 1.0e-1; }
    
    define_velocity(sl);
    CSLPolyMarkInflowFacets(sl);

    CSLPolyApplyBC(sl,time,bc_ex5,NULL);

    CSLPolyReconstructExteriorCellFields(sl);
    CSLPolyReconstructExteriorFacetFields(sl);
    
    CSLPolyUpdate(sl,time,dt,NULL);
    //CSLPolyUpdate_Conservative(sl,time,dt);
    
    CSLPolyReconstructExteriorFacetFields(sl);

    if (k%5==0) {
      //sprintf(name,"c%d.gp",k);
      //CellFieldView_gnuplot(sl,name);
      
      //sprintf(name,"cb%d.gp",k);
      //CellView_gnuplot(sl,name);
      
      sprintf(name,"c%.6d.vts",k);
      CellViewVTS(sl,name);
      
      printf("step %d : time %+1.4e\n",k,time);
    }
    time += dt;
  }
  sprintf(name,"c%.6d.vts",k);
  CellViewVTS(sl,name);
  
  return(0);
}

PetscInt xmain(void)
{
  
  //main_ex2();
  //main_ex3();
  
  //main_ex4(); // complete usage but not using nice interfaces
  main_ex5(); // clean usage
  
  //main_ex6(); // holes
  
  return(0);
}
#endif
