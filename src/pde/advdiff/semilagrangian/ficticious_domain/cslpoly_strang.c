
#include <petsc.h>
#ifdef HAVE_OMP
#include <omp.h>
#endif
#include "cslpoly.h"
#include "dmtetdt.h"


void SLInterpolateAtPoint_WENO3(CSLPoly s,PetscInt celli,PetscInt cellj,const PetscReal xd[],PetscInt nfields,const PetscReal phi[],PetscReal phi_M[]);

void point_in_polygon(PetscInt nvert, const PetscReal vert[], const PetscReal p[],PetscInt *inside);

void interpolate_billinear_v__(CSLPoly p,const PetscReal vel_v[],PetscInt cellid,PetscReal coor[],PetscReal v[]);

void cRK4__(CSLPoly sl,const PetscReal vel_v[],PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
            PetscReal x1[],PetscInt *_cellid);

void line_intersect_poly(const PetscReal P1[],const PetscReal P2[],
                         PetscInt npoints,const PetscReal poly_coor[],
                         PetscReal X[],PetscInt *poly_facet,PetscInt *intersect);

void csl_sweep_x(CSLPoly csl,PetscReal *__restrict field2,PetscReal *__restrict field2n,PetscInt j,
                 PetscReal *__restrict vx_v,PetscReal time,PetscReal dt);

void csl_sweep_y(CSLPoly csl,PetscReal *__restrict field2,PetscReal *__restrict field2n,PetscInt i,
                 PetscReal *__restrict vy_v,PetscReal time,PetscReal dt);

void CSLGetIntervalX(CSLPoly c,PetscReal x,PetscInt *interval)
{
  PetscInt ei;
  
  ei = (PetscInt)((x - c->gmin[0])/c->dx[0]);
  if (ei == c->mx[0]) {
    ei--;
  }
  *interval = ei;
}

void CSLGetIntervalY(CSLPoly c,PetscReal y,PetscInt *interval)
{
  PetscInt ei;
  
  ei = (PetscInt)((y - c->gmin[1])/c->dx[1]);
  if (ei == c->mx[1]) {
    ei--;
  }
  *interval = ei;
}


void integrate_xinterval_weno3(CSLPoly csl,PetscReal xa,PetscReal xb,int cellj,
                               PetscInt nfields,const PetscReal *field,PetscReal II[],PetscReal *V)
{
  const PetscInt COOR_DIR = 0;
  PetscInt interval_a,interval_b;
  PetscReal ivalue[CSLPOLY_MAX_FIELDS],vol,w_value[CSLPOLY_MAX_FIELDS];
  PetscReal x0,x1,y0,y1,q_x[2],dx,dy;
  PetscInt i,qi,qj,q_npoints,f;
  //PetscReal *q_coor,*q_weight;
  PetscReal q_coor[1],q_weight[1];
  PetscReal dV,dJ;
  
  //q_npoints = DEFAULT_NQUAD;
  //_PetscDTGaussJacobiQuadrature(1, q_npoints, -1.0, 1.0, &q_coor, &q_weight);
  q_npoints = 1;
  q_weight[0] = 2.0;
  q_coor[0] = 0.0;
  
  if (xa > xb) {
    printf("integrate_xinterval_weno3: input must satisfy xa <= xb\n");
    exit(0);
  }
  
  CSLGetIntervalX(csl,xa,&interval_a);
  CSLGetIntervalX(csl,xb,&interval_b);
  
  if (xa < csl->gmin[COOR_DIR]) interval_a = 0;
  if (xa > csl->gmax[COOR_DIR]) interval_a = csl->mx[COOR_DIR]-1;
  if (xb < csl->gmin[COOR_DIR]) interval_b = 0;
  if (xb > csl->gmax[COOR_DIR]) interval_b = csl->mx[COOR_DIR]-1;

  y0 = csl->gmin[1] + cellj * csl->dx[1];
  y1 = y0 + csl->dx[1];
  dy = y1 - y0;
  
  dV = csl->dx[0] * csl->dx[1];
  
  for (f=0; f<nfields; f++) {
    ivalue[f] = 0.0;
  }
  vol = 0.0;
  
  if (interval_a == interval_b) {
    
    x0 = xa;
    x1 = xb;
    dx = x1 - x0;
    
    dJ = 0.25 * dx * dy;
    for (qj=0; qj<q_npoints; qj++) {
      for (qi=0; qi<q_npoints; qi++) {
        q_x[0] = 0.5*(1.0 - q_coor[qi]) * x0 + 0.5*(1.0 + q_coor[qi]) * x1;
        q_x[1] = 0.5*(1.0 - q_coor[qj]) * y0 + 0.5*(1.0 + q_coor[qj]) * y1;
        
        //_interpolate_weno3_2d(csl,interval_a,cellj,q_x[0],q_x[1],&w_value);
        SLInterpolateAtPoint_WENO3(csl,interval_a,cellj,(const PetscReal*)q_x,nfields,field,w_value);

        for (f=0; f<nfields; f++) {
          ivalue[f] += q_weight[qi]*q_weight[qj] * w_value[f] * dJ;
        }
        vol    += q_weight[qi]*q_weight[qj] * 1.0 * dJ;
      }
    }
    
  } else if (interval_b - interval_a >= 1) {
    PetscInt ni,nj;
    
    x0 = xa;
    
    ni = interval_a + 1;
    nj = cellj;
    x1 = csl->coor_v[ 2*(ni+nj*csl->nx[0]) + COOR_DIR];
    
    dx = x1 - x0;
    
    dJ = 0.25 * dx * dy;
    for (qj=0; qj<q_npoints; qj++) {
      for (qi=0; qi<q_npoints; qi++) {
        q_x[0] = 0.5*(1.0 - q_coor[qi]) * x0 + 0.5*(1.0 + q_coor[qi]) * x1;
        q_x[1] = 0.5*(1.0 - q_coor[qj]) * y0 + 0.5*(1.0 + q_coor[qj]) * y1;
        
        SLInterpolateAtPoint_WENO3(csl,interval_a,cellj,(const PetscReal*)q_x,nfields,field,w_value);
        
        for (f=0; f<nfields; f++) {
          ivalue[f] += q_weight[qi]*q_weight[qj] * w_value[f] * dJ;
        }
        vol    += q_weight[qi]*q_weight[qj] * 1.0 * dJ;
      }
    }
    
    for (i=interval_a+1; i<interval_b; i++) {
      PetscInt cellidx;
      
      cellidx = i + cellj * csl->mx[0];
      for (f=0; f<nfields; f++) {
        ivalue[f] += field[nfields*cellidx + f] * dV;
      }
      vol    += dV;
    }
    
    ni = interval_b;
    nj = cellj;
    
    x0 = csl->coor_v[ 2*(ni+nj*csl->nx[0]) + COOR_DIR];
    
    x1 = xb;
    
    dx = x1 - x0;
    
    dJ = 0.25 * dx * dy;
    for (qj=0; qj<q_npoints; qj++) {
      for (qi=0; qi<q_npoints; qi++) {
        q_x[0] = 0.5*(1.0 - q_coor[qi]) * x0 + 0.5*(1.0 + q_coor[qi]) * x1;
        q_x[1] = 0.5*(1.0 - q_coor[qj]) * y0 + 0.5*(1.0 + q_coor[qj]) * y1;
        
        //_interpolate_weno3_2d(csl,interval_b,cellj,q_x[0],q_x[1],&w_value);
        SLInterpolateAtPoint_WENO3(csl,interval_b,cellj,(const PetscReal*)q_x,nfields,field,w_value);

        for (f=0; f<nfields; f++) {
          ivalue[f] += q_weight[qi]*q_weight[qj] * w_value[f] * dJ;
        }
        vol    += q_weight[qi]*q_weight[qj] * 1.0 * dJ;
      }
    }
  }
  
  for (f=0; f<nfields; f++) {
    II[f] = ivalue[f];
  }
  *V = vol;
  
  //free(q_coor);
  //free(q_weight);
}

void integrate_yinterval_weno3(CSLPoly csl,PetscReal ya,PetscReal yb,int celli,
                               PetscInt nfields,const PetscReal *field,PetscReal II[],PetscReal *V)
{
  const PetscInt COOR_DIR = 1;
  PetscInt interval_a,interval_b;
  PetscReal ivalue[CSLPOLY_MAX_FIELDS],vol,w_value[CSLPOLY_MAX_FIELDS];
  PetscReal x0,x1,y0,y1,q_x[2],dx,dy;
  PetscInt j,qi,qj,q_npoints,f;
  //PetscReal *q_coor,*q_weight;
  PetscReal q_coor[1],q_weight[1];
  
  //q_npoints = DEFAULT_NQUAD;
  //_PetscDTGaussJacobiQuadrature(1, q_npoints, -1.0, 1.0, &q_coor, &q_weight);
  q_npoints = 1;
  q_weight[0] = 2.0;
  q_coor[0] = 0.0;
  
  CSLGetIntervalY(csl,ya,&interval_a);
  CSLGetIntervalY(csl,yb,&interval_b);
  
  if (ya < csl->gmin[COOR_DIR]) interval_a = 0;
  if (ya > csl->gmax[COOR_DIR]) interval_a = csl->mx[COOR_DIR]-1;
  if (yb < csl->gmin[COOR_DIR]) interval_b = 0;
  if (yb > csl->gmax[COOR_DIR]) interval_b = csl->mx[COOR_DIR]-1;
  
  x0 = csl->gmin[0] + celli * csl->dx[0];
  x1 = x0 + csl->dx[0];
  dx = x1 - x0;
  
  for (f=0; f<nfields; f++) {
    ivalue[f] = 0.0;
  }
  vol = 0.0;
  
  if (interval_a == interval_b) {
    
    y0 = ya;
    y1 = yb;
    dy = y1 - y0;
    
    for (qj=0; qj<q_npoints; qj++) {
      for (qi=0; qi<q_npoints; qi++) {
        q_x[0] = 0.5*(1.0 - q_coor[qi]) * x0 + 0.5*(1.0 + q_coor[qi]) * x1;
        q_x[1] = 0.5*(1.0 - q_coor[qj]) * y0 + 0.5*(1.0 + q_coor[qj]) * y1;
        
        //_interpolate_weno3_2d_j(csl,celli,interval_a,q_x[0],q_x[1],&w_value);
        SLInterpolateAtPoint_WENO3(csl,celli,interval_a,(const PetscReal*)q_x,nfields,field,w_value);
        
        for (f=0; f<nfields; f++) {
          ivalue[f] += q_weight[qi]*q_weight[qj] * w_value[f] * (0.25*dx*dy);
        }
        vol    += q_weight[qi]*q_weight[qj] * 1.0 * (0.25*dx*dy);
      }
    }
    
  } else if (interval_b - interval_a >= 1) {
    PetscInt ni,nj;
    
    y0 = ya;

    ni = celli;
    nj = interval_a + 1;
    y1 = csl->coor_v[ 2*(ni+nj*csl->nx[0]) + COOR_DIR];
    
    dy = y1 - y0;
    
    for (qj=0; qj<q_npoints; qj++) {
      for (qi=0; qi<q_npoints; qi++) {
        q_x[0] = 0.5*(1.0 - q_coor[qi]) * x0 + 0.5*(1.0 + q_coor[qi]) * x1;
        q_x[1] = 0.5*(1.0 - q_coor[qj]) * y0 + 0.5*(1.0 + q_coor[qj]) * y1;
        
        //_interpolate_weno3_2d_j(csl,celli,interval_a,q_x[0],q_x[1],&w_value);
        SLInterpolateAtPoint_WENO3(csl,celli,interval_a,(const PetscReal*)q_x,nfields,field,w_value);
        
        for (f=0; f<nfields; f++) {
          ivalue[f] += q_weight[qi]*q_weight[qj] * w_value[f] * (0.25*dx*dy);
        }
        vol    += q_weight[qi]*q_weight[qj] * 1.0 * (0.25*dx*dy);
      }
    }
    
    for (j=interval_a+1; j<interval_b; j++) {
      PetscInt cellidx;
      
      cellidx = celli + j * csl->mx[0];
      for (f=0; f<nfields; f++) {
        ivalue[f] += field[nfields*cellidx + f] * csl->dx[0] * csl->dx[1];
      }
      
      vol    += csl->dx[0] * csl->dx[1];
    }
    
    ni = celli;
    nj = interval_b;
    y0 = csl->coor_v[ 2*(ni+nj*csl->nx[0]) + COOR_DIR];
    
    y1 = yb;
    
    dy = y1 - y0;
    
    for (qj=0; qj<q_npoints; qj++) {
      for (qi=0; qi<q_npoints; qi++) {
        q_x[0] = 0.5*(1.0 - q_coor[qi]) * x0 + 0.5*(1.0 + q_coor[qi]) * x1;
        q_x[1] = 0.5*(1.0 - q_coor[qj]) * y0 + 0.5*(1.0 + q_coor[qj]) * y1;
        
        //_interpolate_weno3_2d_j(csl,celli,interval_b,q_x[0],q_x[1],&w_value);
        SLInterpolateAtPoint_WENO3(csl,celli,interval_b,(const PetscReal*)q_x,nfields,field,w_value);
        
        for (f=0; f<nfields; f++) {
          ivalue[f] += q_weight[qi]*q_weight[qj] * w_value[f] * (0.25*dx*dy);
        }
        vol    += q_weight[qi]*q_weight[qj] * 1.0 * (0.25*dx*dy);
      }
    }
    
  }
  
  for (f=0; f<nfields; f++) {
    II[f] = ivalue[f];
  }
  *V = vol;
  
  //free(q_coor);
  //free(q_weight);
}

static void advect_point_1d(
                    CharacteristicP point,
                    PetscReal xterminal[],
                    PetscInt *intersect,
                    PetscInt order,
                    const PetscReal vel_v[],
                    PetscReal dt,CSLPoly sl,
                    PetscInt direction,
                    PetscBool trace)
{
  PetscInt d;
  PetscReal dt0;
  PetscReal time;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel;
  PetscInt nsteps = 0;
  
  time = 0.0;
  
  if (direction == 0) { ds = sl->dx[0];}
  else {                ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  
  dt0 = 0.0;
  
  inside = 1;
  while (time <= dt) {
    PetscReal dtcfl;
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_billinear_v__(sl,vel_v,cellid,x0,vel_i);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    /* compute a time-step */
    if (direction == 0) { dtcfl = ds/PetscAbsReal(vel_i[0]); }
    else {                dtcfl = ds/PetscAbsReal(vel_i[1]); }
    
    dt0 = 0.25 * dtcfl;
    if ((time+dt0) > dt)     { dt0 = dt - time;        emergency = 1; }
    
    
    /* update position from time to time + dt0 using RK4 */
    cRK4__(sl,vel_v,dt0,(const PetscReal*)x0,(const PetscReal*)vel_i,x1,&cellid);

    if (cellid == -1) {
      /* if point outside polygon - take an Euler step  */
      for (d=0; d<2; d++) {
        x1[d] = x0[d] - dt0 * vel_i[d];
        /* note time is not advanced */
      }
      inside = 0;
      break;
    }
    
    /* update for next step */
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    
    time += dt0;
    nsteps++;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
      xterminal[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    
    *intersect = 0;
  } else {
    PetscInt found_intersection = 0;
    PetscReal X[2],dt_star;
    
    line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                        sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                        X,&point->poly_facet_crossed,&found_intersection);
    
    if (found_intersection == 0) {
      PetscInt found,s;
      
      printf("x0 %+1.12e %+1.12e : x1 %+1.12e %+1.12e\n",x0[0],x0[1],x1[0],x1[1]);
      printf("Failed to detemine intersection between outgoing characteristic and polygon - attempting shift\n");
      
      found = 0;
      for (s=0; s<10; s++) {
        
        for (d=0; d<2; d++) {
          x1[d] = x1[d] - dt0 * vel_i[d];
        }
        line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                            sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                            X,&point->poly_facet_crossed,&found);
        if (found == 1) break;
      }
      if (found == 0) {
        printf("vel %+1.12e %+1.12e : dt0 %+1.12e\n",vel_i[0],vel_i[1],dt0);
        printf("x0 %+1.12e %+1.12e : x1 %+1.12e %+1.12e\n",x0[0],x0[1],x1[0],x1[1]);
        printf("Failed to detemine intersection between outgoing characteristic and polygon after 10 shifts\n");
        exit(1);
      }
    }
    
    //printf("  [advect] x0 of coordinadate which departed %+1.8e %+1.8e\n",x0[0],x0[1]);
    //printf("  [advect] boundary coordinate %+1.8e %+1.8e\n",X[0],X[1]);
    
    ci = (PetscInt)( (X[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (X[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    /*
     Determine the time step required to hit the boundary
     X[0] = x0[0] + dt^* vel_i[0] ==> solve for dt^*
     X[d] = x0[d] - dt^* vel_i[d];
    */
    if (direction == 0) {
      dt_star = PetscAbsReal( -(X[0] - x0[0])/(vel_i[0]+CSLPOLY_VEL_EPS) ); /* take abs here */
    } else {
      dt_star = PetscAbsReal( -(X[1] - x0[1])/(vel_i[1]+CSLPOLY_VEL_EPS) ); /* take abs here */
    }
    time += dt_star;
    nsteps++;
    //printf("  [advect] dt required to shift to boundary %+1.8e\n",dt_star);
    
    /* compute the contribution from moving the point outside the domain */
    dt_star = dt - time;
    //printf("  [advect] dt required to finish time sequence %+1.8e\n",dt_star);
    interpolate_billinear_v__(sl,vel_v,cellid,X,vel_i);
    for (d=0; d<2; d++) {
      xterminal[d] = X[d] - dt_star * vel_i[d];
    }
    //printf("  [advect] terminal coordinate %+1.8e %+1.8e\n",xterminal[0],xterminal[1]);
    
    for (d=0; d<2; d++) {
      point->departure_coor[d] = X[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;

    time += dt_star;
    nsteps++;

    //printf("  point terminating outside domain: time interval = %+1.8e : time advanced %+1.8e\n",dt,time);
    
    *intersect = 1;
  }
}

void interpolate_w4_v__(CSLPoly p,const PetscReal vel_v[],PetscInt cellid,PetscReal coor[],PetscReal v[],PetscInt direction);
void cRK4__w4(CSLPoly sl,const PetscReal vel_v[],PetscReal dt,const PetscReal x0[],const PetscReal vel0[],
              PetscReal x1[],PetscInt *_cellid,PetscInt direction);

static void advect_point_1d_weno4(
                            CharacteristicP point,
                            PetscReal xterminal[],
                            PetscInt *intersect,
                            PetscInt order,
                            const PetscReal vel_v[],
                            PetscReal dt,CSLPoly sl,
                            PetscInt direction,
                            PetscBool trace)
{
  PetscInt d;
  PetscReal dt0;
  PetscReal time;
  PetscInt inside = 1;
  PetscReal x0[2],x1[2],vel_i[2]={0,0};
  PetscInt cellid,ci,cj;
  PetscReal ds,fvel;
  PetscInt nsteps = 0;
  
  time = 0.0;
  
  if (direction == 0) { ds = sl->dx[0];}
  else {                ds = sl->dx[1]; }
  
  for (d=0; d<2; d++) {
    x0[d] = point->arrival_coor[d];
  }
  cellid = point->arrival_cell;
  
  dt0 = 0.0;
  
  inside = 1;
  while (time <= dt) {
    PetscReal dtcfl;
    PetscInt emergency = 0;
    
    /* interp v */
    interpolate_w4_v__(sl,vel_v,cellid,x0,vel_i,direction);
    fvel = PetscSqrtReal(vel_i[0]*vel_i[0] + vel_i[1]*vel_i[1]);
    
    /* compute a time-step */
    if (direction == 0) { dtcfl = ds/PetscAbsReal(vel_i[0]); }
    else {                dtcfl = ds/PetscAbsReal(vel_i[1]); }
    
    dt0 = 0.25 * dtcfl;
    if ((time+dt0) > dt)     { dt0 = dt - time;        emergency = 1; }
    
    
    /* update position from time to time + dt0 using RK4 */
    cRK4__w4(sl,vel_v,dt0,(const PetscReal*)x0,(const PetscReal*)vel_i,x1,&cellid,direction);
    
    if (cellid == -1) {
      /* if point outside polygon - take an Euler step  */
      for (d=0; d<2; d++) {
        x1[d] = x0[d] - dt0 * vel_i[d];
        /* note time is not advanced */
      }
      inside = 0;
      break;
    }
    
    /* update for next step */
    for (d=0; d<2; d++) {
      x0[d] = x1[d];
    }
    
    time += dt0;
    nsteps++;
    
    if (fvel < CSLPOLY_VEL_EPS) { emergency = 1; }
    if (emergency == 1) break;
  }
  
  if (inside == 1) {
    for (d=0; d<2; d++) {
      point->departure_coor[d] = x1[d];
      xterminal[d] = x1[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_GREEN;
    
    *intersect = 0;
  } else {
    PetscInt found_intersection = 0;
    PetscReal X[2],dt_star;
    
    line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                        sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                        X,&point->poly_facet_crossed,&found_intersection);
    
    if (found_intersection == 0) {
      PetscInt found,s;
      
      printf("x0 %+1.12e %+1.12e : x1 %+1.12e %+1.12e\n",x0[0],x0[1],x1[0],x1[1]);
      printf("Failed to detemine intersection between outgoing characteristic and polygon - attempting shift\n");
      
      found = 0;
      for (s=0; s<10; s++) {
        
        for (d=0; d<2; d++) {
          x1[d] = x1[d] - dt0 * vel_i[d];
        }
        line_intersect_poly((const PetscReal*)x0,(const PetscReal*)x1,
                            sl->poly_npoints,(const PetscReal*)sl->poly_coor,
                            X,&point->poly_facet_crossed,&found);
        if (found == 1) break;
      }
      if (found == 0) {
        printf("vel %+1.12e %+1.12e : dt0 %+1.12e\n",vel_i[0],vel_i[1],dt0);
        printf("x0 %+1.12e %+1.12e : x1 %+1.12e %+1.12e\n",x0[0],x0[1],x1[0],x1[1]);
        printf("Failed to detemine intersection between outgoing characteristic and polygon after 10 shifts\n");
        exit(1);
      }
    }
    
    //printf("  [advect] x0 of coordinadate which departed %+1.8e %+1.8e\n",x0[0],x0[1]);
    //printf("  [advect] boundary coordinate %+1.8e %+1.8e\n",X[0],X[1]);
    
    ci = (PetscInt)( (X[0] - sl->gmin[0])/sl->dx[0] );
    cj = (PetscInt)( (X[1] - sl->gmin[1])/sl->dx[1] );
    if (ci == sl->mx[0]) ci--;
    if (cj == sl->mx[1]) cj--;
    
    cellid = ci + cj * sl->mx[0];
    
    /*
     Determine the time step required to hit the boundary
     X[0] = x0[0] + dt^* vel_i[0] ==> solve for dt^*
     X[d] = x0[d] - dt^* vel_i[d];
     */
    if (direction == 0) {
      dt_star = PetscAbsReal( -(X[0] - x0[0])/(vel_i[0]+CSLPOLY_VEL_EPS) ); /* take abs here */
    } else {
      dt_star = PetscAbsReal( -(X[1] - x0[1])/(vel_i[1]+CSLPOLY_VEL_EPS) ); /* take abs here */
    }
    time += dt_star;
    nsteps++;
    //printf("  [advect] dt required to shift to boundary %+1.8e\n",dt_star);
    
    /* compute the contribution from moving the point outside the domain */
    dt_star = dt - time;
    //printf("  [advect] dt required to finish time sequence %+1.8e\n",dt_star);
    interpolate_w4_v__(sl,vel_v,cellid,X,vel_i,direction);
    for (d=0; d<2; d++) {
      xterminal[d] = X[d] - dt_star * vel_i[d];
    }
    //printf("  [advect] terminal coordinate %+1.8e %+1.8e\n",xterminal[0],xterminal[1]);
    
    for (d=0; d<2; d++) {
      point->departure_coor[d] = X[d];
    }
    point->departure_cell = cellid;
    point->tag = TAG_YELLOW;
    
    time += dt_star;
    nsteps++;
    
    //printf("  point terminating outside domain: time interval = %+1.8e : time advanced %+1.8e\n",dt,time);
    
    *intersect = 1;
  }
}

void working_csl_sweep_x(CSLPoly csl,PetscReal *__restrict field2,PetscReal *__restrict field2n,PetscInt j,
                 PetscReal *__restrict vx_v,PetscReal time,PetscReal dt)
{
  PetscInt  i,f;
  PetscReal x0[2],ivalue[CSLPOLY_MAX_FIELDS],vol;
  PetscReal coor_y;
  CharacteristicP traj_cell[2];
  PetscReal xterminalL[2],xterminalR[2];
  PetscReal xA,xB;
  PetscReal valueL[CSLPOLY_MAX_FIELDS],valueR[CSLPOLY_MAX_FIELDS];
  
  traj_cell[0] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  traj_cell[1] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  
  if (field2n == field2) {
    printf("[csl_sweep_x] output cannot be the same pointer as the input\n");
    exit(1);
  }
  
  
  for (i=0; i<csl->mx[0]; i++) {
    PetscInt insideL,insideR;
    PetscInt cellidx = i + j * csl->mx[0];
    
    
    if (csl->cell_list[cellidx]->type != COOR_INTERIOR) {
      continue;
    }

    /* cell vertices and centroid */
    x0[0] = csl->gmin[0] + i*csl->dx[0];
    x0[1] = x0[0] + csl->dx[0];
    coor_y = csl->cell_list[cellidx]->coor[1];

    /* left */
    traj_cell[0]->arrival_cell   = cellidx;
    traj_cell[0]->departure_cell = -1;

    traj_cell[0]->arrival_coor[0] = x0[0];
    traj_cell[0]->arrival_coor[1] = coor_y;

    /* right */
    traj_cell[1]->arrival_cell   = cellidx;
    traj_cell[1]->departure_cell = -1;
    
    traj_cell[1]->arrival_coor[0] = x0[1];
    traj_cell[1]->arrival_coor[1] = coor_y;
    

    /* check if points are in the domain */
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[0]->arrival_coor,&insideL);
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[1]->arrival_coor,&insideR);

    if (insideL == 0 && insideR == 0) {
      printf("left/right points are BOTH outside the polygon - refine the CSL mesh as this should never happen\n");
      exit(0);
    }
    
    //printf("i %d [j %d]:\n",i,j);
    //printf("  arrival cell: [%+1.12e , %+1.12e]\n",x0[0],x0[1]);
    
    if (insideL == 0) {
      PetscInt found_intersection,poly_facet_crossed,pin;
      PetscReal Xtrial[2];
      PetscReal X[2],vL[2];
      
      //printf("  left  x0 %+1.12e <%d>\n",x0[0],insideL);
      /* find intersection between left-right points */
      line_intersect_poly((const PetscReal*)traj_cell[0]->arrival_coor,(const PetscReal*)traj_cell[1]->arrival_coor,
                          csl->poly_npoints,(const PetscReal*)csl->poly_coor,
                          X,&poly_facet_crossed,&found_intersection);
      
      if (found_intersection == 0) {
        printf("[x]<left> Failed to detemine intersection between outgoing characteristic and polygon\n");
        exit(1);
      }
      
      /* interpolate fields at X */
      CSLPolyInterpolateAtBoundaryPoint(csl,poly_facet_crossed,X,valueL);
      
      /* compute velocity at cell left point */
      interpolate_billinear_v__(csl,vx_v,cellidx,X,vL);

      for (PetscInt d=0; d<2; d++) {
        Xtrial[d] = traj_cell[0]->arrival_coor[d] - dt * vL[d];
      }
      point_in_polygon(csl->poly_npoints,csl->poly_coor,Xtrial,&pin);
      if (pin == 0) {
        PetscInt ci,cj;
        
        ci = (PetscInt)( (X[0] - csl->gmin[0])/csl->dx[0] );
        cj = (PetscInt)( (X[1] - csl->gmin[1])/csl->dx[1] );
        if (ci == csl->mx[0]) ci--;
        if (cj == csl->mx[1]) cj--;

        traj_cell[0]->departure_cell = ci + cj * csl->mx[0];
        traj_cell[0]->tag = TAG_YELLOW;
        traj_cell[0]->poly_facet_crossed = poly_facet_crossed;

        /* put departure_coor[] at insersection */
        traj_cell[0]->departure_coor[0] = X[0];
        traj_cell[0]->departure_coor[1] = X[1];
        
        /* define x_terminal using full Euler step */
        xterminalL[0] = Xtrial[0];
        xterminalL[1] = Xtrial[1];
        //printf("  updating xterminalL \n");
      } else {
        PetscInt ci,cj;
        
        ci = (PetscInt)( (Xtrial[0] - csl->gmin[0])/csl->dx[0] );
        cj = (PetscInt)( (Xtrial[1] - csl->gmin[1])/csl->dx[1] );
        if (ci == csl->mx[0]) ci--;
        if (cj == csl->mx[1]) cj--;
        
        traj_cell[0]->departure_cell = ci + cj * csl->mx[0];
        traj_cell[0]->tag = TAG_GREEN;
        traj_cell[0]->poly_facet_crossed = -1;
        
        for (PetscInt d=0; d<2; d++) {
          traj_cell[0]->departure_coor[d] = Xtrial[d];
          xterminalL[d]                   = Xtrial[d];
        }
      }
      
    } else {
      PetscInt intersectL;

      advect_point_1d(traj_cell[0],
                      xterminalL,
                      &intersectL,
                      4,
                      vx_v,
                      dt,csl,0,
                      PETSC_FALSE);
      
      if (intersectL == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[0]->poly_facet_crossed,traj_cell[0]->departure_coor,valueL);
      }
    }

    if (insideR == 0) {
      PetscInt found_intersection,poly_facet_crossed,pin;
      PetscReal Xtrial[2];
      PetscReal X[2],vR[2];
      
      //printf("  right x0 %+1.12e <%d>\n",x0[1],insideR);

      /* find intersection between left-right points */
      line_intersect_poly((const PetscReal*)traj_cell[0]->arrival_coor,(const PetscReal*)traj_cell[1]->arrival_coor,
                          csl->poly_npoints,(const PetscReal*)csl->poly_coor,
                          X,&poly_facet_crossed,&found_intersection);
      
      if (found_intersection == 0) {
        printf("[x]<right> Failed to detemine intersection between outgoing characteristic and polygon\n");
        exit(1);
      }
      
      /* interpolate fields at X */
      CSLPolyInterpolateAtBoundaryPoint(csl,poly_facet_crossed,X,valueR);
      
      /* compute velocity at cell left point */
      interpolate_billinear_v__(csl,vx_v,cellidx,X,vR);
      
      for (PetscInt d=0; d<2; d++) {
        Xtrial[d] = traj_cell[1]->arrival_coor[d] - dt * vR[d];
      }
      point_in_polygon(csl->poly_npoints,csl->poly_coor,Xtrial,&pin);
      if (pin == 0) {
        PetscInt ci,cj;
        
        ci = (PetscInt)( (X[0] - csl->gmin[0])/csl->dx[0] );
        cj = (PetscInt)( (X[1] - csl->gmin[1])/csl->dx[1] );
        if (ci == csl->mx[0]) ci--;
        if (cj == csl->mx[1]) cj--;
        
        traj_cell[1]->departure_cell = ci + cj * csl->mx[0];
        traj_cell[1]->tag = TAG_YELLOW;
        traj_cell[1]->poly_facet_crossed = poly_facet_crossed;
        
        /* put departure_coor[] at insersection */
        traj_cell[1]->departure_coor[0] = X[0];
        traj_cell[1]->departure_coor[1] = X[1];
        
        /* define x_terminal using full Euler step */
        xterminalR[0] = Xtrial[0];
        xterminalR[1] = Xtrial[1];
        //printf("  updating xterminalR \n");
      } else {
        PetscInt ci,cj;
        
        ci = (PetscInt)( (Xtrial[0] - csl->gmin[0])/csl->dx[0] );
        cj = (PetscInt)( (Xtrial[1] - csl->gmin[1])/csl->dx[1] );
        if (ci == csl->mx[0]) ci--;
        if (cj == csl->mx[1]) cj--;
        
        traj_cell[1]->departure_cell = ci + cj * csl->mx[0];
        traj_cell[1]->tag = TAG_GREEN;
        traj_cell[1]->poly_facet_crossed = -1;
        
        for (PetscInt d=0; d<2; d++) {
          traj_cell[1]->departure_coor[d] = Xtrial[d];
          xterminalR[d]                   = Xtrial[d];
        }
      }

    } else {
      PetscInt intersectR;
      
      advect_point_1d(traj_cell[1],
                      xterminalR,
                      &intersectR,
                      4,
                      vx_v,
                      dt,csl,0,
                      PETSC_FALSE);
      
      if (intersectR == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[1]->poly_facet_crossed,traj_cell[1]->departure_coor,valueR);
      }
    }

    /* four cases */
    /* 
     A] Both points terminated inside the domain
     B] Left point outside, right point inside
     C] Right point outside, left point inside
     D] Both points outside
        sub-cases (i) both points exited through the same boundary
                  (ii) points exited through different boundaries
    */
    
    for (f=0; f<csl->nfields; f++) {
      ivalue[f] = 0.0;
    }
    vol = 0.0;
    
    if (traj_cell[0]->tag == TAG_GREEN && traj_cell[1]->tag == TAG_GREEN) { /* case A */
      xA = traj_cell[0]->departure_coor[0];
      xB = traj_cell[1]->departure_coor[0];
      //printf("  A fully interior interval [%+1.12e %+1.12e]\n",xA,xB);
      
      if (xA > xB) {
        PetscReal delta_sep;
        delta_sep = xB - xA;
        if (PetscAbsReal(delta_sep) < 1.0e-3 * csl->dx[0]) {
          xB = xA;
        } else {
          printf("  [strang-x] characteristics have crossed - this is not permitted - aborting\n");
          exit(1);
        }
      }
      
      integrate_xinterval_weno3(csl,xA,xB,j, csl->nfields,field2,ivalue,&vol);

    } else if (traj_cell[0]->tag == TAG_YELLOW && traj_cell[1]->tag == TAG_GREEN) { /* case B */
      PetscReal delta;
      
      xA = traj_cell[0]->departure_coor[0];
      xB = traj_cell[1]->departure_coor[0];
      //printf("  B partial interior interval [%+1.12e %+1.12e]\n",xA,xB);
      //printf("  B complete interval [%+1.12e : %+1.12e %+1.12e ]\n",xterminalL[0],xA,xB);
      
      if (xA > xB) {
        PetscReal delta_sep;
        delta_sep = xB - xA;
        if (PetscAbsReal(delta_sep) < 1.0e-3 * csl->dx[0]) {
          xB = xA;
        } else {
          printf("  [strang-x] characteristics have crossed - this is not permitted - aborting\n");
          exit(1);
        }
      }
      
      integrate_xinterval_weno3(csl,xA,xB,j, csl->nfields,field2,ivalue,&vol);
      
      delta = PetscAbsReal(xterminalL[0] - traj_cell[0]->departure_coor[0]);
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueL[f] * delta * csl->dx[1];
      }
      vol += delta * csl->dx[1];
      
    } else if (traj_cell[0]->tag == TAG_GREEN && traj_cell[1]->tag == TAG_YELLOW) { /* case C */
      PetscReal delta;
      
      xA = traj_cell[0]->departure_coor[0];
      xB = traj_cell[1]->departure_coor[0];
      //printf("  C partial interior interval [%+1.12e %+1.12e]\n",xA,xB);
      //printf("  C complete interval [%+1.12e %+1.12e : %+1.12e]\n",xA,xB,xterminalR[0]);
      
      if (xA > xB) {
        PetscReal delta_sep;
        delta_sep = xB - xA;
        if (PetscAbsReal(delta_sep) < 1.0e-3 * csl->dx[0]) {
          xB = xA;
        } else {
          printf("  [strang-x] characteristics have crossed - this is not permitted - aborting\n");
          exit(1);
        }
      }
      
      integrate_xinterval_weno3(csl,xA,xB,j, csl->nfields,field2,ivalue,&vol);
      
      delta = PetscAbsReal(xterminalR[0] - traj_cell[1]->departure_coor[0]);
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueR[f] * delta * csl->dx[1];
      }
      vol += delta * csl->dx[1];
      
      
    } else { /* case D */
      PetscReal delta;
      
      if (traj_cell[0]->poly_facet_crossed == traj_cell[1]->poly_facet_crossed) {

        //printf("  D (i) fully exterior interval [%+1.12e %+1.12e] intersection %+1.12e\n",xterminalL[0],xterminalR[0],traj_cell[0]->departure_coor[0]);

        delta = PetscAbsReal(xterminalL[0] - xterminalR[0]);
        for (f=0; f<csl->nfields; f++) {
          ivalue[f] += valueL[f] * delta * csl->dx[1];
        }
        vol += delta * csl->dx[1];
        
      } else {
        printf("  D (ii)  cell index %d\n",cellidx);
        printf("error: unhandled case of depature cell points crossing different boundary segments\n");
        printf("error: aborting\n");
        exit(0);
      }
    }
    
    
    
    
    
    /* store */
    for (f=0; f<csl->nfields; f++) {
      field2n[csl->nfields*cellidx + f] = ivalue[f]/(csl->dx[0]*csl->dx[1]);
    }
  }
  
  free(traj_cell[0]);
  free(traj_cell[1]);
}

void broken_csl_sweep_y(CSLPoly csl,PetscReal *__restrict field2,PetscReal *__restrict field2n,PetscInt i,
                 PetscReal *__restrict vy_v,PetscReal time,PetscReal dt)
{
  PetscInt  j,f;
  PetscReal x0[2],ivalue[CSLPOLY_MAX_FIELDS],vol,dV;
  PetscReal coor_x;
  CharacteristicP traj_cell[2];
  PetscReal xterminalS[2],xterminalN[2];
  PetscInt intersectS,intersectN;
  PetscReal yA,yB;
  PetscReal valueS[CSLPOLY_MAX_FIELDS],valueN[CSLPOLY_MAX_FIELDS];
  
  traj_cell[0] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  traj_cell[1] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  
  if (field2n == field2) {
    printf("[csl_sweep_y] output cannot be the same pointer as the input\n");
    exit(1);
  }
  
  for (j=0; j<csl->mx[1]; j++) {
    PetscInt insideS,insideN;
    PetscInt cellidx = i + j * csl->mx[0];
    
    
    if (csl->cell_list[cellidx]->type != COOR_INTERIOR) {
      continue;
    }

    dV = csl->dx[0] * csl->dx[1];

    x0[0] = csl->gmin[1] + j*csl->dx[1];
    x0[1] = x0[0] + csl->dx[1];
    
    coor_x = csl->cell_list[cellidx]->coor[0];
    
    /* bottom/south */
    traj_cell[0]->arrival_cell   = cellidx;
    traj_cell[0]->departure_cell = -1;
    
    traj_cell[0]->arrival_coor[0] = coor_x;
    traj_cell[0]->arrival_coor[1] = x0[0];
    
    /* top/north */
    traj_cell[1]->arrival_cell   = cellidx;
    traj_cell[1]->departure_cell = -1;
    
    traj_cell[1]->arrival_coor[0] = coor_x;
    traj_cell[1]->arrival_coor[1] = x0[1];
    
    
    /* check if points are in the domain */
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[0]->arrival_coor,&insideS);
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[1]->arrival_coor,&insideN);
    
    if (insideS == 0 && insideN == 0) {
      printf("south/north points are BOTH outside the polygon - refine the CSL mesh as this should never happen\n");
      exit(0);
    }
    
    //printf("j %d [i %d]:\n",j,i);
    //printf("  south  x0 %+1.12e <%d>\n",x0[0],insideS);
    //printf("  north  x0 %+1.12e <%d>\n",x0[1],insideN);
    
    if (insideS == 0) {
      PetscInt found_intersection,poly_facet_crossed;
      PetscReal X[2];
      
      /* find intersection between left-right points */
      line_intersect_poly((const PetscReal*)traj_cell[0]->arrival_coor,(const PetscReal*)traj_cell[1]->arrival_coor,
                          csl->poly_npoints,(const PetscReal*)csl->poly_coor,
                          X,&poly_facet_crossed,&found_intersection);
      
      if (found_intersection == 0) {
        printf("[y] Failed to detemine intersection between outgoing characteristic and polygon\n");
        exit(1);
      }
      
      /* put departure_coor[] at insersection */
      traj_cell[0]->arrival_coor[0] = X[0];
      traj_cell[0]->arrival_coor[1] = X[1] + 1.0e-10*csl->dx[1];
    }
    {
      advect_point_1d(traj_cell[0],
                      xterminalS,
                      &intersectS,
                      4,
                      vy_v,
                      dt,csl,1,
                      PETSC_FALSE);
      
      if (intersectS == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[0]->poly_facet_crossed,traj_cell[0]->departure_coor,valueS);
      }
    }
    
    if (insideN == 0) {
      PetscInt found_intersection,poly_facet_crossed;
      PetscReal X[2];
      
      /* find intersection between left-right points */
      line_intersect_poly((const PetscReal*)traj_cell[0]->arrival_coor,(const PetscReal*)traj_cell[1]->arrival_coor,
                          csl->poly_npoints,(const PetscReal*)csl->poly_coor,
                          X,&poly_facet_crossed,&found_intersection);
      
      if (found_intersection == 0) {
        printf("[y] Failed to detemine intersection between outgoing characteristic and polygon\n");
        exit(1);
      }
      
      /* put departure_coor[] at insersection */
      traj_cell[1]->arrival_coor[0] = X[0];
      traj_cell[1]->arrival_coor[1] = X[1] - 1.0e-10*csl->dx[1];
    }
    {
      advect_point_1d(traj_cell[1],
                      xterminalN,
                      &intersectN,
                      4,
                      vy_v,
                      dt,csl,1,
                      PETSC_FALSE);
      if (intersectN == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[1]->poly_facet_crossed,traj_cell[1]->departure_coor,valueN);
      }
    }
    
    dV = ( csl->dx[0] ) * ( traj_cell[1]->arrival_coor[1] - traj_cell[0]->arrival_coor[1] );
    
    yA = traj_cell[0]->departure_coor[1];
    yB = traj_cell[1]->departure_coor[1];
    
    //
    if (yA > yB) {
      PetscReal delta_sep;
      
      //printf("  [strang-y] characteristics have crossed - this is not permitted - trying to correct\n");
      delta_sep = yB - yA;
      if (PetscAbsReal(delta_sep) < 1.0e-3 * csl->dx[1]) {
        yB = yA;
      } else {
        printf("  [strang-y] characteristics have crossed - this is not permitted - aborting\n");
        printf("    [y] interval [%+1.12e %+1.12e]\n",yA,yB);
        printf("    [y] delta %+1.12e\n",delta_sep);
        printf("    [y] dy %+1.12e\n",csl->dx[1]);
        exit(1);
      }
    }
    //
     
    integrate_yinterval_weno3(csl,yA,yB,i,
                              csl->nfields,field2,ivalue,&vol);
    //printf("  [y] ival %+1.14e : vol %+1.14e\n",ivalue[0],vol);
    
    /* add a little more */
    if (intersectS == 1) {
      PetscReal delta;
      
      delta = PetscAbsReal(xterminalS[1] - traj_cell[0]->departure_coor[1]);
      // <sum> ivalue[f] += delta * field2[f][cellidx] * csl->dx[1];
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueS[f] * delta * csl->dx[0];
      }
      vol += delta * csl->dx[0];
    }
    if (intersectN == 1) {
      PetscReal delta;
      
      delta = PetscAbsReal(xterminalN[1] - traj_cell[1]->departure_coor[1]);
      // <sum> ivalue[f] += delta * field2[f][cellidx] * csl->dx[1];
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueN[f] * delta * csl->dx[0];
      }
      vol += delta * csl->dx[0];
    }
    
    /* store */
    for (f=0; f<csl->nfields; f++) {
      field2n[csl->nfields*cellidx + f] = ivalue[f]/dV;
    }
  }
  
  free(traj_cell[0]);
  free(traj_cell[1]);
}

void CSLPolyUpdate_Strang2Multiplicative(CSLPoly csl,PetscReal time,PetscReal dt)
{
  PetscReal *field2_half,*field2_buffer,dthalf;
  PetscInt i,j,n;
  PetscReal *vx,*vy;
  
  if (csl->interp_type != I_WENO3) {
    printf("error[CSLPolyUpdate_Strang2Multiplicative]: Must use I_WENO3\n");
    exit(1);
  }
  if (csl->nimplicitfields > 0) {
    printf("error[CSLPolyUpdate_Strang2Multiplicative]: No support for implicit fields\n");
    exit(1);
  }
  
  dthalf = dt * 0.5;
  
  field2_half = (PetscReal*)malloc(sizeof(PetscReal)*csl->ncells*csl->nfields);
  field2_buffer = (PetscReal*)malloc(sizeof(PetscReal)*csl->ncells*csl->nfields);
  
  memcpy(field2_half,csl->value_c,sizeof(PetscReal)*csl->ncells*csl->nfields);
  memcpy(field2_buffer,csl->value_c,sizeof(PetscReal)*csl->ncells*csl->nfields);
  
  // copy two velocity fields, zeroing out vy and vx respectively
  vx = (PetscReal*)malloc(sizeof(PetscReal)*2*csl->nvert);
  vy = (PetscReal*)malloc(sizeof(PetscReal)*2*csl->nvert);

  memcpy(vx,csl->vel_v,sizeof(PetscReal)*2*csl->nvert);
  memcpy(vy,csl->vel_v,sizeof(PetscReal)*2*csl->nvert);

  for (n=0; n<csl->nvert; n++) {
    vx[2*n+1] = 0.0;
    vy[2*n+0] = 0.0;
  }
  
  
  /* There is a question here as to whether the BC's should be updated after each stage - i think they should be */
  
  /* update i planes */
  //printf("**************** X sweep - stage 1 ****************\n");
  for (j=0; j<csl->mx[1]; j++) {
    csl_sweep_x(csl,csl->value_c,field2_half,j,vx,time,dthalf);
  }
#if 0
  if (csl->impose_global_value_bounds) {
    for (i=0; i<csl->ncells; i++) {
      for (f=0; f<csl->nfields; f++) {

        if (field2_half[csl->nfields*i + f] < csl->value_bound_min[f]) {
          field2_half[csl->nfields*i + f] = csl->value_bound_min[f];
        }
        if (field2_half[csl->nfields*i + f] > csl->value_bound_max[f]) {
          field2_half[csl->nfields*i + f] = csl->value_bound_max[f];
        }
        
      }
    }
  }
#endif
  /*
  {
    PetscReal *tmp;
    
    tmp = csl->value_c;
    csl->value_c = field2_half;
    CSLPolyReconstructExteriorCellFields(csl);
    csl->value_c = tmp;
  }
  */
  
  /* update j planes */
  for (i=0; i<csl->mx[0]; i++) {
    csl_sweep_y(csl,field2_half,field2_buffer,i,vy,time,dt);
  }
  //memcpy(field2_buffer,field2_half,sizeof(PetscReal)*csl->ncells*csl->nfields);
#if 0
  if (csl->impose_global_value_bounds) {
    for (i=0; i<csl->ncells; i++) {
      for (f=0; f<csl->nfields; f++) {
        
        if (field2_buffer[csl->nfields*i + f] < csl->value_bound_min[f]) {
          field2_buffer[csl->nfields*i + f] = csl->value_bound_min[f];
        }
        if (field2_buffer[csl->nfields*i + f] > csl->value_bound_max[f]) {
          field2_buffer[csl->nfields*i + f] = csl->value_bound_max[f];
        }
        
      }
    }
  }
#endif
  
  /*
  {
    PetscReal *tmp;
    
    tmp = csl->value_c;
    csl->value_c = field2_buffer;
    CSLPolyReconstructExteriorCellFields(csl);
    csl->value_c = tmp;
  }
  */
  /* update i planes */
  //printf("**************** X sweep - stage 2 ****************\n");
  for (j=0; j<csl->mx[1]; j++) {
    csl_sweep_x(csl,field2_buffer,csl->value_c,j,vx,time+dthalf,dthalf);
  }
#if 0
  if (csl->impose_global_value_bounds) {
    for (i=0; i<csl->ncells; i++) {
      for (f=0; f<csl->nfields; f++) {
        
        if (csl->value_c[csl->nfields*i + f] < csl->value_bound_min[f]) {
          csl->value_c[csl->nfields*i + f] = csl->value_bound_min[f];
        }
        if (csl->value_c[csl->nfields*i + f] > csl->value_bound_max[f]) {
          csl->value_c[csl->nfields*i + f] = csl->value_bound_max[f];
        }
        
      }
    }
  }
#endif
  
  free(vx);
  free(vy);
  free(field2_buffer);
  free(field2_half);
}

void CSLPolyUpdateState(CSLPoly csl,PetscReal field_kp1[])
{
  PetscInt c;
  
  for (c=0; c<csl->ncells*csl->nfields; c++) {
    csl->value_c[c] = field_kp1[c];
  }
}

void CSLPolyComputeStateUpdate_Strang2Multiplicative(CSLPoly csl,PetscReal time,PetscReal dt,PetscReal field_kp1[])
{
  PetscReal *field2_half,*field2_buffer,dthalf;
  PetscInt i,j,n;
  PetscReal *vx,*vy;
  
  if (csl->interp_type != I_WENO3) {
    printf("error[CSLPolyComputeStateUpdate_Strang2Multiplicative]: Must use I_WENO3\n");
    exit(1);
  }
  if (csl->nimplicitfields > 0) {
    printf("error[CSLPolyComputeStateUpdate_Strang2Multiplicative]: No support for implicit fields\n");
    exit(1);
  }
  
  dthalf = dt * 0.5;
  
  field2_half = (PetscReal*)malloc(sizeof(PetscReal)*csl->ncells*csl->nfields);
  field2_buffer = (PetscReal*)malloc(sizeof(PetscReal)*csl->ncells*csl->nfields);
  
  memcpy(field2_half,csl->value_c,sizeof(PetscReal)*csl->ncells*csl->nfields);
  memcpy(field2_buffer,csl->value_c,sizeof(PetscReal)*csl->ncells*csl->nfields);
  
  // copy two velocity fields, zeroing out vy and vx respectively
  vx = (PetscReal*)malloc(sizeof(PetscReal)*2*csl->nvert);
  vy = (PetscReal*)malloc(sizeof(PetscReal)*2*csl->nvert);
  
  memcpy(vx,csl->vel_v,sizeof(PetscReal)*2*csl->nvert);
  memcpy(vy,csl->vel_v,sizeof(PetscReal)*2*csl->nvert);
  
  for (n=0; n<csl->nvert; n++) {
    vx[2*n+1] = 0.0;
    vy[2*n+0] = 0.0;
  }
  
  /* update i planes */
  for (j=0; j<csl->mx[1]; j++) {
    csl_sweep_x(csl,csl->value_c,field2_half,j,vx,time,dthalf);
  }
  
  /* update j planes */
  for (i=0; i<csl->mx[0]; i++) {
    csl_sweep_y(csl,field2_half,field2_buffer,i,vy,time,dt);
  }
  
  /* update i planes */
  for (j=0; j<csl->mx[1]; j++) {
    csl_sweep_x(csl,field2_buffer,field_kp1,j,vx,time+dthalf,dthalf);
  }
  
  free(vx);
  free(vy);
  free(field2_buffer);
  free(field2_half);
}


void advect_exterior_vertices(CharacteristicP traj_cell,
                              PetscReal xterminal[],PetscReal valueB[],
                              CSLPoly csl,
                              const PetscReal vel_v[],PetscReal dt,
                              PetscInt cellidx,
                              PetscReal c0[],PetscReal c1[])
{
  PetscInt  found_intersection,poly_facet_crossed,pin;
  PetscReal Xtrial[2];
  PetscReal XB[2],vB[2];
  PetscInt  d,ci,cj;
  
  //printf("  left  x0 %+1.12e <%d>\n",x0[0],insideL);
  /* find intersection between left-right points */
  line_intersect_poly((const PetscReal*)c0,(const PetscReal*)c1,
                      csl->poly_npoints,(const PetscReal*)csl->poly_coor,
                      XB,&poly_facet_crossed,&found_intersection);
  
  if (found_intersection == 0) {
    printf("[advect_exterior_vertices] Failed to detemine intersection between outgoing characteristic and polygon\n");
    exit(1);
  }
  
  /* interpolate fields at X */
  CSLPolyInterpolateAtBoundaryPoint(csl,poly_facet_crossed,XB,valueB);
  //printf(" <exterior> %+1.12e valueB\n",valueB[0]);
  
  /* compute velocity at cell left point */
  interpolate_billinear_v__(csl,vel_v,cellidx,XB,vB);
  
  for (d=0; d<2; d++) {
    Xtrial[d] = traj_cell->arrival_coor[d] - dt * vB[d];
  }
  point_in_polygon(csl->poly_npoints,csl->poly_coor,Xtrial,&pin);
  if (pin == 0) {
    ci = (PetscInt)( (XB[0] - csl->gmin[0])/csl->dx[0] );
    cj = (PetscInt)( (XB[1] - csl->gmin[1])/csl->dx[1] );
    if (ci == csl->mx[0]) ci--;
    if (cj == csl->mx[1]) cj--;
    
    traj_cell->departure_cell     = ci + cj * csl->mx[0];
    traj_cell->tag                = TAG_YELLOW;
    traj_cell->poly_facet_crossed = poly_facet_crossed;
    
    /* put departure_coor[] at insersection */
    traj_cell->departure_coor[0] = XB[0];
    traj_cell->departure_coor[1] = XB[1];
    
    /* define x_terminal using full Euler step */
    xterminal[0] = Xtrial[0];
    xterminal[1] = Xtrial[1];
    //printf("  updating xterminalL \n");
  } else {
    ci = (PetscInt)( (Xtrial[0] - csl->gmin[0])/csl->dx[0] );
    cj = (PetscInt)( (Xtrial[1] - csl->gmin[1])/csl->dx[1] );
    if (ci == csl->mx[0]) ci--;
    if (cj == csl->mx[1]) cj--;
    
    traj_cell->departure_cell     = ci + cj * csl->mx[0];
    traj_cell->tag                = TAG_GREEN;
    traj_cell->poly_facet_crossed = -1;
    
    for (d=0; d<2; d++) {
      traj_cell->departure_coor[d] = Xtrial[d];
      xterminal[d]                 = Xtrial[d];
    }
  }
}



void csl_sweep_x(CSLPoly csl,PetscReal *__restrict field2,PetscReal *__restrict field2n,PetscInt j,
                             PetscReal *__restrict vx_v,PetscReal time,PetscReal dt)
{
  PetscInt  i,f;
  PetscReal x0[2],ivalue[CSLPOLY_MAX_FIELDS],vol;
  PetscReal coor_y;
  CharacteristicP traj_cell[2];
  PetscReal xterminalL[2],xterminalR[2];
  PetscReal xA,xB;
  PetscReal valueL[CSLPOLY_MAX_FIELDS],valueR[CSLPOLY_MAX_FIELDS];
  
  traj_cell[0] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  traj_cell[1] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  
  if (field2n == field2) {
    printf("[csl_sweep_x] output cannot be the same pointer as the input\n");
    exit(1);
  }
  
  
  for (i=0; i<csl->mx[0]; i++) {
    PetscInt insideL,insideR;
    PetscInt cellidx = i + j * csl->mx[0];
    
    if (csl->cell_list[cellidx]->type != COOR_INTERIOR) {
      continue;
    }
    
    /* cell vertices and centroid */
    x0[0] = csl->gmin[0] + i*csl->dx[0];
    x0[1] = x0[0] + csl->dx[0];
    coor_y = csl->cell_list[cellidx]->coor[1];
    
    /* left */
    traj_cell[0]->arrival_cell   = cellidx;
    traj_cell[0]->departure_cell = -1;
    
    traj_cell[0]->arrival_coor[0] = x0[0];
    traj_cell[0]->arrival_coor[1] = coor_y;
    
    /* right */
    traj_cell[1]->arrival_cell   = cellidx;
    traj_cell[1]->departure_cell = -1;
    
    traj_cell[1]->arrival_coor[0] = x0[1];
    traj_cell[1]->arrival_coor[1] = coor_y;
    
    for (f=0; f<csl->nfields; f++) {
      valueL[f] = 0.0;
      valueR[f] = 0.0;
    }
    
    /* check if points are in the domain */
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[0]->arrival_coor,&insideL);
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[1]->arrival_coor,&insideR);
    
    if (insideL == 0 && insideR == 0) {
      printf("[csl_sweep_x] left/right cell points are both outside the polygon - refine the CSL mesh as this should never happen\n");
      printf("[csl_sweep_x] skipping update of cell %d\n",cellidx);
      continue;
    }
    
    if (insideL == 0) {
      advect_exterior_vertices(traj_cell[0],xterminalL,valueL,
                               csl,vx_v,dt,cellidx,traj_cell[0]->arrival_coor,traj_cell[1]->arrival_coor);
    } else {
      PetscInt intersectL;
      
      advect_point_1d(traj_cell[0],xterminalL,&intersectL,4,vx_v,dt,csl,0,PETSC_FALSE);
      //advect_point_1d_weno4(traj_cell[0],xterminalL,&intersectL,4,vx_v,dt,csl,0,PETSC_FALSE);
      
      if (intersectL == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[0]->poly_facet_crossed,traj_cell[0]->departure_coor,valueL);
      }
    }
    
    if (insideR == 0) {
      advect_exterior_vertices(traj_cell[1],xterminalR,valueR,
                               csl,vx_v,dt,cellidx,traj_cell[0]->arrival_coor,traj_cell[1]->arrival_coor);
    } else {
      PetscInt intersectR;
      
      advect_point_1d(traj_cell[1],xterminalR,&intersectR,4,vx_v,dt,csl,0,PETSC_FALSE);
      //advect_point_1d_weno4(traj_cell[1],xterminalR,&intersectR,4,vx_v,dt,csl,0,PETSC_FALSE);
      if (intersectR == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[1]->poly_facet_crossed,traj_cell[1]->departure_coor,valueR);
      }
    }
    
    /* four cases */
    /*
     A] Both points terminated inside the domain
     B] Left point outside, right point inside
     C] Right point outside, left point inside
     D] Both points outside
     sub-cases (i) both points exited through the same boundary
     (ii) points exited through different boundaries
     */
    
    for (f=0; f<csl->nfields; f++) {
      ivalue[f] = 0.0;
    }
    vol = 0.0;

    xA = traj_cell[0]->departure_coor[0];
    xB = traj_cell[1]->departure_coor[0];
    
    if (xA > xB) {
      PetscReal delta_sep;
      delta_sep = xB - xA;
      if (PetscAbsReal(delta_sep) < 1.0e-3 * csl->dx[0]) {
        xB = xA;
      } else {
        printf("  [strang-x] characteristics have crossed - this is not permitted - aborting\n");
        exit(1);
      }
    }

    if (traj_cell[0]->tag == TAG_GREEN && traj_cell[1]->tag == TAG_GREEN) { /* case A */
      //printf("  A fully interior interval [%+1.12e %+1.12e]\n",xA,xB);
      integrate_xinterval_weno3(csl,xA,xB,j, csl->nfields,field2,ivalue,&vol);
    } else if (traj_cell[0]->tag == TAG_YELLOW && traj_cell[1]->tag == TAG_GREEN) { /* case B */
      PetscReal delta;
      
      //printf("  B partial interior interval [%+1.12e %+1.12e]\n",xA,xB);
      //printf("  B complete interval [%+1.12e : %+1.12e %+1.12e ]\n",xterminalL[0],xA,xB);
      
      integrate_xinterval_weno3(csl,xA,xB,j, csl->nfields,field2,ivalue,&vol);
      
      delta = PetscAbsReal(xterminalL[0] - traj_cell[0]->departure_coor[0]);
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueL[f] * delta * csl->dx[1];
      }
      vol += delta * csl->dx[1];
      
    } else if (traj_cell[0]->tag == TAG_GREEN && traj_cell[1]->tag == TAG_YELLOW) { /* case C */
      PetscReal delta;
      
      //printf("  C partial interior interval [%+1.12e %+1.12e]\n",xA,xB);
      //printf("  C complete interval [%+1.12e %+1.12e : %+1.12e]\n",xA,xB,xterminalR[0]);
      
      integrate_xinterval_weno3(csl,xA,xB,j, csl->nfields,field2,ivalue,&vol);
      
      delta = PetscAbsReal(xterminalR[0] - traj_cell[1]->departure_coor[0]);
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueR[f] * delta * csl->dx[1];
      }
      vol += delta * csl->dx[1];
      
      
    } else { /* case D */
      PetscReal delta;
      
      if (traj_cell[0]->poly_facet_crossed == traj_cell[1]->poly_facet_crossed) {
        
        //printf("  D (i) fully exterior interval [%+1.12e %+1.12e] intersection %+1.12e\n",xterminalL[0],xterminalR[0],traj_cell[0]->departure_coor[0]);
        
        delta = PetscAbsReal(xterminalL[0] - xterminalR[0]);
        for (f=0; f<csl->nfields; f++) {
          ivalue[f] += valueL[f] * delta * csl->dx[1];
        }
        vol += delta * csl->dx[1];
        
      } else {
        printf("  D (ii)  cell index %d\n",cellidx);
        printf("error: unhandled case of depature cell points crossing different boundary segments\n");
        printf("error: aborting\n");
        exit(0);
      }
    }
    
    /* store */
    for (f=0; f<csl->nfields; f++) {
      field2n[csl->nfields*cellidx + f] = ivalue[f]/(csl->dx[0]*csl->dx[1]);
    }
  }
  
  free(traj_cell[0]);
  free(traj_cell[1]);
}

void csl_sweep_y(CSLPoly csl,PetscReal *__restrict field2,PetscReal *__restrict field2n,PetscInt i,
                 PetscReal *__restrict vy_v,PetscReal time,PetscReal dt)
{
  PetscInt  j,f;
  PetscReal x0[2],ivalue[CSLPOLY_MAX_FIELDS],vol;
  PetscReal coor_x;
  CharacteristicP traj_cell[2];
  PetscReal xterminalS[2],xterminalN[2];
  PetscReal xA,xB;
  PetscReal valueS[CSLPOLY_MAX_FIELDS],valueN[CSLPOLY_MAX_FIELDS];
  
  traj_cell[0] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  traj_cell[1] = (CharacteristicP)malloc(sizeof(struct _p_CharacteristicP));
  
  if (field2n == field2) {
    printf("[csl_sweep_y] output cannot be the same pointer as the input\n");
    exit(1);
  }
  
  
  for (j=0; j<csl->mx[1]; j++) {
    PetscInt insideS,insideN;
    PetscInt cellidx = i + j * csl->mx[0];
    
    if (csl->cell_list[cellidx]->type != COOR_INTERIOR) {
      continue;
    }
    
    /* cell vertices and centroid */
    x0[0] = csl->gmin[1] + j*csl->dx[1];
    x0[1] = x0[0] + csl->dx[1];
    
    coor_x = csl->cell_list[cellidx]->coor[0];
    
    /* bottom/south */
    traj_cell[0]->arrival_cell   = cellidx;
    traj_cell[0]->departure_cell = -1;
    
    traj_cell[0]->arrival_coor[0] = coor_x;
    traj_cell[0]->arrival_coor[1] = x0[0];
    
    /* top/north */
    traj_cell[1]->arrival_cell   = cellidx;
    traj_cell[1]->departure_cell = -1;
    
    traj_cell[1]->arrival_coor[0] = coor_x;
    traj_cell[1]->arrival_coor[1] = x0[1];
    
    for (f=0; f<csl->nfields; f++) {
      valueS[f] = 0.0;
      valueN[f] = 0.0;
    }
    
    /* check if points are in the domain */
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[0]->arrival_coor,&insideS);
    point_in_polygon(csl->poly_npoints,csl->poly_coor,traj_cell[1]->arrival_coor,&insideN);
    
    if (insideS == 0 && insideN == 0) {
      printf("[csl_sweep_y] south/north cell points are both outside the polygon - refine the CSL mesh as this should never happen\n");
      printf("[csl_sweep_y] skipping update of cell %d\n",cellidx);
      continue;
    }
    
    if (insideS == 0) {
      advect_exterior_vertices(traj_cell[0],xterminalS,valueS,
                               csl,vy_v,dt,cellidx,traj_cell[0]->arrival_coor,traj_cell[1]->arrival_coor);
    } else {
      PetscInt intersectS;
      
      advect_point_1d(traj_cell[0],xterminalS,&intersectS,4,vy_v,dt,csl,1,PETSC_FALSE);
      //advect_point_1d_weno4(traj_cell[0],xterminalS,&intersectS,4,vy_v,dt,csl,1,PETSC_FALSE);
      if (intersectS == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[0]->poly_facet_crossed,traj_cell[0]->departure_coor,valueS);
      }
    }
    
    if (insideN == 0) {
      advect_exterior_vertices(traj_cell[1],xterminalN,valueN,
                               csl,vy_v,dt,cellidx,traj_cell[0]->arrival_coor,traj_cell[1]->arrival_coor);
    } else {
      PetscInt intersectN;
      
      advect_point_1d(traj_cell[1],xterminalN,&intersectN,4,vy_v,dt,csl,1,PETSC_FALSE);
      //advect_point_1d_weno4(traj_cell[1],xterminalN,&intersectN,4,vy_v,dt,csl,1,PETSC_FALSE);
      if (intersectN == 1) {
        CSLPolyInterpolateAtBoundaryPoint(csl,traj_cell[1]->poly_facet_crossed,traj_cell[1]->departure_coor,valueN);
      }
    }
    
    /* four cases */
    /*
     A] Both points terminated inside the domain
     B] Left point outside, right point inside
     C] Right point outside, left point inside
     D] Both points outside
     sub-cases (i) both points exited through the same boundary
     (ii) points exited through different boundaries
     */
    
    for (f=0; f<csl->nfields; f++) {
      ivalue[f] = 0.0;
    }
    vol = 0.0;
    
    xA = traj_cell[0]->departure_coor[1];
    xB = traj_cell[1]->departure_coor[1];
    
    if (xA > xB) {
      PetscReal delta_sep;
      delta_sep = xB - xA;
      if (PetscAbsReal(delta_sep) < 1.0e-3 * csl->dx[1]) {
        xB = xA;
      } else {
        printf("  [strang-x] characteristics have crossed - this is not permitted - aborting\n");
        exit(1);
      }
    }
    
    if (traj_cell[0]->tag == TAG_GREEN && traj_cell[1]->tag == TAG_GREEN) { /* case A */
      //printf("  A fully interior interval [%+1.12e %+1.12e]\n",xA,xB);
      integrate_yinterval_weno3(csl,xA,xB,i, csl->nfields,field2,ivalue,&vol);
    } else if (traj_cell[0]->tag == TAG_YELLOW && traj_cell[1]->tag == TAG_GREEN) { /* case B */
      PetscReal delta;
      
      //printf("  B partial interior interval [%+1.12e %+1.12e]\n",xA,xB);
      //printf("  B complete interval [%+1.12e : %+1.12e %+1.12e ]\n",xterminalL[0],xA,xB);
      
      integrate_yinterval_weno3(csl,xA,xB,i, csl->nfields,field2,ivalue,&vol);
      
      delta = PetscAbsReal(xterminalS[1] - traj_cell[0]->departure_coor[1]);
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueS[f] * delta * csl->dx[0];
      }
      vol += delta * csl->dx[0];
      
    } else if (traj_cell[0]->tag == TAG_GREEN && traj_cell[1]->tag == TAG_YELLOW) { /* case C */
      PetscReal delta;
      
      //printf("  C partial interior interval [%+1.12e %+1.12e]\n",xA,xB);
      //printf("  C complete interval [%+1.12e %+1.12e : %+1.12e]\n",xA,xB,xterminalR[0]);
      
      integrate_yinterval_weno3(csl,xA,xB,i, csl->nfields,field2,ivalue,&vol);
      
      delta = PetscAbsReal(xterminalN[1] - traj_cell[1]->departure_coor[1]);
      for (f=0; f<csl->nfields; f++) {
        ivalue[f] += valueN[f] * delta * csl->dx[0];
      }
      vol += delta * csl->dx[0];
      
      
    } else { /* case D */
      PetscReal delta;
      
      if (traj_cell[0]->poly_facet_crossed == traj_cell[1]->poly_facet_crossed) {
        
        //printf("  D (i) fully exterior interval [%+1.12e %+1.12e] intersection %+1.12e\n",xterminalL[0],xterminalR[0],traj_cell[0]->departure_coor[0]);
        
        delta = PetscAbsReal(xterminalS[1] - xterminalN[1]);
        for (f=0; f<csl->nfields; f++) {
          ivalue[f] += valueS[f] * delta * csl->dx[0];
        }
        vol += delta * csl->dx[0];
        
      } else {
        printf("  D (ii)  cell index %d\n",cellidx);
        printf("error: unhandled case of depature cell points crossing different boundary segments\n");
        printf("error: aborting\n");
        exit(0);
      }
    }
    
    /* store */
    for (f=0; f<csl->nfields; f++) {
      field2n[csl->nfields*cellidx + f] = ivalue[f]/(csl->dx[0]*csl->dx[1]);
    }
  }
  
  free(traj_cell[0]);
  free(traj_cell[1]);
}

