
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <dmbcs.h>

typedef struct {
  DM          dm;
  BCList      dirichletbc;
  PetscReal   dt;
  Vec         X_old;
} PDEADFD;


const char adfd_description[] =
"\"PDE AdvDiff FD\" solves the PDE \n"
"    A(x) DG/Dt = div ( k(x) grad(G) ) + F(x) \n"
"  subject to the boundary conditions u = u_D on the Dirichlet boundary \n"
"  and q_N = - k grad(u) = 0 on the Neumann boundary. \n"
"  The PDE is discretized using finite differences defined on a DMDA object. \n"
"  [Assumptions] \n"
"  * Only DMDA is supported. \n"
"  [User notes] \n"
"  * The function F(x) is defined via a Coefficient using point-wise defined \n"
"    quadrature point values. The quadrature field is named \"q_g0\". \n"
"  * The coefficient A(x), k(x) is defined as a domain-wise constant.";

#define KAPPA 1.0e-1


/*

          Tijp1
            |
            |
Tim1j ---- Tij ---- Tip1j
            |
            |
            Tijm1


 kR = 0.5(ij ip1j)
 
 qE = -kE*(Tip1j - Tij)/dx
 qW = -kW*(Tij - Tim1j)/dx
 
 -dq/dx = (qE - qW)/dx
       = (kE*Tip1j - kE*Tij - kW*Tij + kW*Tim1j) / dx.dx
       = kE/dx.dx       -(kE+kW)/dx.dx          + kW/dx.dx

 qN = -kN*(Tijp1 - Tij)/dy
 qS = -kS*(Tij - Tijm1)/dy

 -dq/dy = (qN - qS)/dy
       = (kN*Tijp1 - kN*Tij - kS*Tij + kS*Tijm1) / dy.dy
       = kN/dy.dy       -(kN+kS)/dy.dy          + kS/dy.dy

 DT/Dt = -div(q) + f
 
 Tij^{k+1} - Tij = -dt.div(q^{k+1}) + dt.f
 
*/

/*
 
 I'll label the stencil this way
 
    0
 3  4  1
    2
 
*/
#define STC_NORTH 0
#define STC_EAST  1
#define STC_SOUTH 2
#define STC_WEST  3
#define STC_C     4


#undef __FUNCT__
#define __FUNCT__ "FormFunction_PDEADFD"
PetscErrorCode FormFunction_PDEADFD(SNES snes,Vec X,Vec F,void *ctx)
{
  PDE            pde = (PDE)ctx;
  PDEADFD        *data;
  PetscErrorCode ierr;
  PetscInt       i,j,k,mx,my,xm,ym,xs,ys;
  PetscScalar    Hx,Hy,HxdHy,HydHx,vol;
  const PetscScalar    **LA_X,**LA_Xold;
  PetscScalar          **LA_F;
  DM             da,dac;
  Vec            Xlocal,coor;
  PetscReal      kN,kE,kS,kW,kij,min[3],max[3],dt;
  PetscScalar    coeff[5];
  MatStencil     col[5];
  const DMDACoor2d **LA_coor;
  
  PetscFunctionBeginUser;
  data = (PDEADFD*)pde->data;
  ierr = SNESGetDM(snes,&da);CHKERRQ(ierr);
  ierr = DMDAGetBoundingBox(da,min,max);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,0,&mx,&my,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  Hx        = (max[0]-min[0]) / (PetscReal)(mx-1);
  Hy        = (max[1]-min[1]) / (PetscReal)(my-1);
  HxdHy     = Hx / Hy;
  HydHx     = Hy / Hx;
  vol       = Hx * Hy;
  ierr = DMDAGetCorners(da,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);

  ierr = DMGetLocalVector(da,&Xlocal);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,X,INSERT_VALUES,Xlocal);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,X,INSERT_VALUES,Xlocal);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(da, Xlocal, &LA_X);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, F, &LA_F);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(da, data->X_old, &LA_Xold);CHKERRQ(ierr);

  ierr = DMGetCoordinateDM(da,&dac);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da,&coor);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(dac,coor,&LA_coor);CHKERRQ(ierr);
  
  dt = data->dt;
  kij = KAPPA;
  kN = kE = kS = kW = KAPPA;
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      PetscBool interior = PETSC_TRUE;
      PetscReal F_ij,Xold_ij,Q_ij,X_ij,phi_ij;
      
      Q_ij    = 0.0;
      Xold_ij = LA_Xold[j][i];
      X_ij    = LA_X[j][i];
      
      phi_ij = 0.0;

      if (j==0) phi_ij = 1.0;
      if (j==my-1) phi_ij = 0.0;

      if ((i == 0) || (j == 0) || (i == (mx-1)) || (j == (my-1))) {
        interior = PETSC_FALSE;
      }
      
      /* Neumann constraints */
      col[0].i = i;  col[0].j = j;
      if (i == (mx-1)) {
        coeff[0] = 0.0*vol + 1.0 * 2.0 * kij * HydHx;
        coeff[1] =         - 1.0 * 2.0 * kij * HydHx;
        col[1].i = i-1;  col[1].j = j;

        F_ij =  coeff[0] * LA_X[ col[0].j ][ col[0].i ];
        F_ij += coeff[1] * LA_X[ col[1].j ][ col[1].i ];
        LA_F[j][i] += F_ij;
      }
      
      if (i == 0) {
        coeff[0] = 0.0*vol + 1.0 * 2.0 * kij * HydHx;
        coeff[1] =         - 1.0 * 2.0 * kij * HydHx;
        col[1].i = i+1;  col[1].j = j;
        
        F_ij =  coeff[0] * LA_X[ col[0].j ][ col[0].i ];
        F_ij += coeff[1] * LA_X[ col[1].j ][ col[1].i ];
        LA_F[j][i] += F_ij;
      }
/*
      if (j == 0) {
        coeff[0] = 0.0*vol + 1.0 * 2.0 * kij * HxdHy;
        coeff[1] =         - 1.0 * 2.0 * kij * HxdHy;
        col[1].i = i;  col[1].j = j+1;
        
        F_ij =  coeff[0] * LA_X[ col[0].j ][ col[0].i ];
        F_ij += coeff[1] * LA_X[ col[1].j ][ col[1].i ];
        LA_F[j][i] += F_ij;
      }
      
      if (j == (my-1)) {
        coeff[0] = 0.0*vol + 1.0 * 2.0 * kij * HxdHy;
        coeff[1] =         - 1.0 * 2.0 * kij * HxdHy;
        col[1].i = i;  col[1].j = j-1;
        
        F_ij =  coeff[0] * LA_X[ col[0].j ][ col[0].i ];
        F_ij += coeff[1] * LA_X[ col[1].j ][ col[1].i ];
        LA_F[j][i] += F_ij;
      }
*/
      if (interior) {
        coeff[STC_NORTH] = -dt * kN*HxdHy;
        col[STC_NORTH].i = i; col[STC_NORTH].j = j+1;
        
        coeff[STC_EAST] = -dt * kN*HydHx;
        col[STC_EAST].i = i+1; col[STC_EAST].j = j;
        
        coeff[STC_SOUTH] = -dt * kS*HxdHy;
        col[STC_SOUTH].i = i; col[STC_SOUTH].j = j-1;
        
        coeff[STC_WEST] = -dt * kW*HydHx;
        col[STC_WEST].i = i-1; col[STC_WEST].j = j;
        
        coeff[STC_C] = 1.0*vol + dt * (kE+kW)*HydHx + dt * (kN+kS)*HxdHy;
        col[STC_C].i = i; col[STC_C].j = j;

        /*
          Tij^{k+1} + dt.div(q^{k+1}) - Tij =  + dt.f
         */
        F_ij = 0.0;
        for (k=0; k<5; k++) {
          F_ij += coeff[k] * LA_X[ col[k].j ][ col[k].i ];
        }
        
        LA_F[j][i] = F_ij - Xold_ij * vol - dt * Q_ij * vol;
      }

      /* Dirichlet */
      /*
      if (!interior) {
        coeff[0] = 1.0*vol + dt * kij*(HydHx + HxdHy);
        
        LA_F[j][i] = coeff[0] * (X_ij - phi_ij);
        interior = PETSC_FALSE;
      }
      */
      
      if (j == 0) {
        coeff[0] = 1.0*vol + dt * kij*(HydHx + HxdHy);
        LA_F[j][i] = coeff[0] * (X_ij - phi_ij);
      }
      if (j == (my-1)) {
        coeff[0] = 1.0*vol + dt * kij*(HydHx + HxdHy);
        LA_F[j][i] = coeff[0] * (X_ij - phi_ij);
      }
      

      
    }
  }
  
  ierr = DMDAVecRestoreArrayRead(dac,coor,&LA_coor);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(da, data->X_old, &LA_Xold);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, F, &LA_F);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(da, Xlocal, &LA_X);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&Xlocal);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(F);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(F);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ComputeJacobian_ADFD"
PetscErrorCode ComputeJacobian_ADFD(SNES snes,Vec x,Mat A,Mat B,void *ctx)
{
  PDE            pde = (PDE)ctx;
  PDEADFD        *data;
  PetscErrorCode ierr;
  PetscInt       i,j,mx,my,xm,ym,xs,ys;
  PetscScalar    val[5];
  PetscReal      Hx,Hy,HydHx,HxdHy,vol;
  MatStencil     row,col[5];
  DM             da;
  PetscReal      kN,kE,kS,kW,kij,min[3],max[3],dt;
  
  PetscFunctionBeginUser;
  data = (PDEADFD*)pde->data;
  ierr = SNESGetDM(snes,&da);CHKERRQ(ierr);
  ierr = DMDAGetBoundingBox(da,min,max);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,0,&mx,&my,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  Hx        = (max[0]-min[0]) / (PetscReal)(mx-1);
  Hy        = (max[1]-min[1]) / (PetscReal)(my-1);
  HxdHy     = Hx / Hy;
  HydHx     = Hy / Hx;
  vol       = Hx * Hy;
  ierr = DMDAGetCorners(da,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  dt = data->dt;
  kij = KAPPA;
  kN = kE = kS = kW = KAPPA;

  /* insert Neumann constraints */
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      row.i = i;
      row.j = j;

      if (i == (mx-1)) {
        val[0] = 0.0*vol + 1.0 * 2.0 * kij * HydHx;
        col[0].i = i;
        col[0].j = j;

        val[1] =         - 1.0 * 2.0 * kij * HydHx;
        col[1].i = i-1;
        col[1].j = j;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,ADD_VALUES);CHKERRQ(ierr);
      }

      if (i == 0) {
        val[0] = 0.0*vol + 1.0 * 2.0 * kij * HydHx;
        col[0].i = i;
        col[0].j = j;
        
        val[1] =         - 1.0 * 2.0 * kij * HydHx;
        col[1].i = i+1;
        col[1].j = j;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,ADD_VALUES);CHKERRQ(ierr);
      }

      /*
      if (j == 0) {
        val[0] = 0.0*vol + 1.0 * 2.0 * kij * HxdHy;
        col[0].i = i;
        col[0].j = j;
        
        val[1] =         - 1.0 * 2.0 * kij * HxdHy;
        col[1].i = i;
        col[1].j = j+1;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,ADD_VALUES);CHKERRQ(ierr);
      }

      if (j == (my-1)) {
        val[0] = 0.0*vol + 1.0 * 2.0 * kij * HxdHy;
        col[0].i = i;
        col[0].j = j;
        
        val[1] =         - 1.0 * 2.0 * kij * HxdHy;
        col[1].i = i;
        col[1].j = j-1;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,ADD_VALUES);CHKERRQ(ierr);
      }
      */
    }
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      PetscBool interior = PETSC_TRUE;
      row.i = i;
      row.j = j;
      
      if ((i == 0) || (j == 0) || (i == (mx-1)) || (j == (my-1))) {
        interior = PETSC_FALSE;
      }
      
      if (interior) {
        /*
         -dq/dx = (qE - qW)/dx
                = (kE*Tip1j - kE*Tij - kW*Tij + kW*Tim1j) / dx.dx
                = kE/dx.dx       -(kE+kW)/dx.dx          + kW/dx.dx
         
         qN = -kN*(Tijp1 - Tij)/dy
         qS = -kS*(Tij - Tijm1)/dy
         
         -dq/dy = (qN - qS)/dy
                = (kN*Tijp1 - kN*Tij - kS*Tij + kS*Tijm1) / dy.dy
                = kN/dy.dy       -(kN+kS)/dy.dy          + kS/dy.dy
        
         Tij^{k+1} - Tij = -dt.div(q^{k+1}) + dt.f
         
        */
        val[STC_NORTH] = -dt * kN*HxdHy;
        col[STC_NORTH].i = i; col[STC_NORTH].j = j+1;
        
        val[STC_EAST] = -dt * kN*HydHx;
        col[STC_EAST].i = i+1; col[STC_EAST].j = j;
        
        val[STC_SOUTH] = -dt * kS*HxdHy;
        col[STC_SOUTH].i = i; col[STC_SOUTH].j = j-1;
        
        val[STC_WEST] = -dt * kW*HydHx;
        col[STC_WEST].i = i-1; col[STC_WEST].j = j;

        val[STC_C] = 1.0*vol + dt * (kE+kW)*HydHx + dt * (kN+kS)*HxdHy;
        col[STC_C].i = i; col[STC_C].j = j;
        ierr = MatSetValuesStencil(B,1,&row,5,col,val,INSERT_VALUES);CHKERRQ(ierr);
      }
    }
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* Dirichlet */
#if 0
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      row.i = i;
      row.j = j;
      
      val[0] = 1.0*vol + dt * kij*(HydHx + HxdHy);
      col[0].i = i;   col[0].j = j;

      val[1] = 0.0;
      /*
      if (i == (mx-1)) {
        col[1].i = i-1; col[1].j = j;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,INSERT_VALUES);CHKERRQ(ierr);
      }
      
      if (i == 0) {
        col[1].i = i+1; col[1].j = j;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,INSERT_VALUES);CHKERRQ(ierr);
      }
      */
      
      if (j == 0) {
        col[1].i = i;  col[1].j = j+1;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,INSERT_VALUES);CHKERRQ(ierr);
      }
      
      if (j == (my-1)) {
        col[1].i = i; col[1].j = j-1;
        ierr = MatSetValuesStencil(B,1,&row,2,col,val,INSERT_VALUES);CHKERRQ(ierr);
      }
    }
  }
#endif
  
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      PetscInt idx = i + j * xm;

      val[0] = 1.0*vol + dt * kij*(HydHx + HxdHy);
      if (j == 0) {
        ierr = MatZeroRows(B,1,&idx,val[0],NULL,NULL);CHKERRQ(ierr);
      }
      if (j == (my-1)) {
        ierr = MatZeroRows(B,1,&idx,val[0],NULL,NULL);CHKERRQ(ierr);
      }
    }
  }
  
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  //MatView(B,PETSC_VIEWER_STDOUT_WORLD);
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEUpdateLocal_ADFD"
PetscErrorCode PDEUpdateLocal_ADFD(PDE pde)
{
  PetscErrorCode ierr;
  PDEADFD *data = (PDEADFD*)pde->data;
  Vec X;
  
  PetscFunctionBegin;
  
  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(data->dm,X,INSERT_VALUES,pde->xlocal[0]);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(data->dm,X,INSERT_VALUES,pde->xlocal[0]);CHKERRQ(ierr);
  
  /* update time history */
  ierr = VecCopy(X,data->X_old);CHKERRQ(ierr); /* x --> x_old */
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDestroy_ADFD"
PetscErrorCode PDEDestroy_ADFD(PDE pde)
{
  PetscErrorCode ierr;
  PDEADFD *data;
  
  PetscFunctionBegin;
  data = (PDEADFD*)pde->data;
  
  ierr = DMDestroy(&data->dm);CHKERRQ(ierr);
  if (data->dirichletbc) {
    ierr = BCListDestroy(&data->dirichletbc);CHKERRQ(ierr);
  }
  ierr = VecDestroy(&data->X_old);CHKERRQ(ierr);
  
  ierr = PetscFree(data);CHKERRQ(ierr);
  pde->data = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEConfigureSNES_ADFD"
PetscErrorCode PDEConfigureSNES_ADFD(PDE pde,SNES snes,Vec F,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PDEADFD *data;
  
  PetscFunctionBegin;
  data = (PDEADFD*)pde->data;
  
  ierr = SNESSetApplicationContext(snes,(void*)pde);CHKERRQ(ierr);
  
  ierr = SNESSetFunction(snes,F,pde->ops->form_function,(void*)pde);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes,A,B,pde->ops->form_jacobian,(void*)pde);CHKERRQ(ierr);

  ierr = MatSetOption(B,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  if (A != B) {
    ierr = MatSetOption(A,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  }
  
  if (!pde->x) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must call PDESetSolution() first");
  }
  ierr = SNESSetSolution(snes,pde->x);CHKERRQ(ierr);
  
  ierr = PDEUpdateLocal_ADFD(pde);CHKERRQ(ierr); /* x --> x_old */
  
  if (!data->dm) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must provide a valid DM - Call PDEADFDSetData()");
  } else { /* check its a DMDA */
    PetscBool isdmda = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)data->dm,DMDA,&isdmda);CHKERRQ(ierr);
    if (!isdmda) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for DMDA");
  }
  ierr = SNESSetDM(snes,data->dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreate_ADFD"
PetscErrorCode PDECreate_ADFD(PDE pde)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize;
  PDEADFD *data;
  
  PetscFunctionBegin;
  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"The PDE implementation PDEADVDIFF is incomplete and is not correct for cases with Neumann boundary conditions");
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)pde),&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Currently this is only supported for comm.size = 1");
  ierr = PetscObjectChangeTypeName((PetscObject)pde,PDETypeNames[(int)PDEADVDIFF]);CHKERRQ(ierr);
  ierr = PetscStrallocpy(adfd_description,&pde->description);CHKERRQ(ierr);
  
  ierr = PetscNewLog(pde,&data);CHKERRQ(ierr);
  data->dm = NULL;
  data->dirichletbc = NULL;
  pde->data = data;
  
  pde->ops->configure_snes = PDEConfigureSNES_ADFD;
  pde->ops->form_function = FormFunction_PDEADFD;
  pde->ops->form_jacobian = ComputeJacobian_ADFD;
  pde->ops->destroy               = PDEDestroy_ADFD;
  pde->ops->update_local_solution = PDEUpdateLocal_ADFD;
  
  /* create coefficients */
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEADFDSetData"
PetscErrorCode PDEADFDSetData(PDE pde,DM dm,BCList list)
{
  PDEADFD *data;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEADFD*)pde->data;
  ierr = PetscObjectReference((PetscObject)dm);CHKERRQ(ierr);
  if (data->dm) {
    ierr = DMDestroy(&data->dm);CHKERRQ(ierr);
  }
  data->dm = dm;
  data->dirichletbc = list;
  ierr = DMCreateGlobalVector(dm,&data->X_old);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEADFDSetTimestep"
PetscErrorCode PDEADFDSetTimestep(PDE pde,PetscReal dt)
{
  PDEADFD *data;
  
  PetscFunctionBegin;
  data = (PDEADFD*)pde->data;
  data->dt = dt;
  
  PetscFunctionReturn(0);
}

