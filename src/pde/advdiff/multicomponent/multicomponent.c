

#include <petsc.h>
#include <cslpoly.h>
#include <multicomponent.h>

//#define MC_DEBUG
#define MC_DEBUG_NAN_CHECKER
//#define HAVE_GAMMA_SUPPORT

PetscErrorCode ComponentFDestroy(ComponentF *_c);
void porosity_ifield_evaluate_at_point(CSLPoly csl_liquid,PetscReal coor[],PetscReal ivalues[],void *data);
void porosity_ifield_evaluate_at_boundarypoint(CSLPoly csl_liquid,PetscInt facet_intersected,PetscReal coor[],PetscReal ivalues[],void *data);
PetscErrorCode TPMCCheckFieldForNAN(TPMC m,PetscBool iscelldata,const char stage[],PetscInt N,PetscInt nf,const PetscReal field[]);
PetscErrorCode TPMCEnforceFieldPositivity(TPMC m,PetscInt N,PetscReal field[],PetscBool *enforcement_occurred);

static void TPMCInterpolatePT(TPMC m,PetscReal coor[],PetscReal P[],PetscReal T[])
{
  PetscInt nidx[4];
  PetscInt k,ci,cj;
  PetscReal xi[2],x0[2],Ni[4];
  CSLPoly p;
  
  p = m->csl[0];
  
  ci = (PetscInt)( (coor[0] - p->gmin[0])/p->dx[0] );
  cj = (PetscInt)( (coor[1] - p->gmin[1])/p->dx[1] );
  if (ci == p->mx[0]) ci--;
  if (cj == p->mx[1]) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = CSLNodeIdx(p,ci  ,cj);
  nidx[1] = CSLNodeIdx(p,ci+1,cj);
  nidx[2] = CSLNodeIdx(p,ci  ,cj+1);
  nidx[3] = CSLNodeIdx(p,ci+1,cj+1);
  
  x0[0] = p->gmin[0] + ci * p->dx[0];
  x0[1] = p->gmin[1] + cj * p->dx[1];
  
  xi[0] = 2.0*(coor[0]-x0[0])/p->dx[0] - 1.0;
  xi[1] = 2.0*(coor[1]-x0[1])/p->dx[1] - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  P[0] = T[0] = 0.0;
  for (k=0; k<4; k++) {
    P[0] += Ni[k] * m->pressure_v[ nidx[k] ];
    T[0] += Ni[k] * m->temperature_v[ nidx[k] ];
  }
}

static void TPMCInterpolatePTprevious(TPMC m,PetscReal coor[],PetscReal P[],PetscReal T[])
{
  PetscInt nidx[4];
  PetscInt k,ci,cj;
  PetscReal xi[2],x0[2],Ni[4];
  CSLPoly p;
  
  p = m->csl[0];
  
  ci = (PetscInt)( (coor[0] - p->gmin[0])/p->dx[0] );
  cj = (PetscInt)( (coor[1] - p->gmin[1])/p->dx[1] );
  if (ci == p->mx[0]) ci--;
  if (cj == p->mx[1]) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = CSLNodeIdx(p,ci  ,cj);
  nidx[1] = CSLNodeIdx(p,ci+1,cj);
  nidx[2] = CSLNodeIdx(p,ci  ,cj+1);
  nidx[3] = CSLNodeIdx(p,ci+1,cj+1);
  
  x0[0] = p->gmin[0] + ci * p->dx[0];
  x0[1] = p->gmin[1] + cj * p->dx[1];
  
  xi[0] = 2.0*(coor[0]-x0[0])/p->dx[0] - 1.0;
  xi[1] = 2.0*(coor[1]-x0[1])/p->dx[1] - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  P[0] = T[0] = 0.0;
  for (k=0; k<4; k++) {
    P[0] += Ni[k] * m->pressure_v[ nidx[k] ];
    T[0] += Ni[k] * m->temperature_prev_v[ nidx[k] ];
  }
}


#undef __FUNCT__
#define __FUNCT__ "_TPMCCreate"
PetscErrorCode _TPMCCreate(MPI_Comm comm,TPMC *m)
{
  TPMC m_;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&m_);CHKERRQ(ierr);
  ierr = PetscMemzero(m_,sizeof(struct _p_TPMC));CHKERRQ(ierr);
  m_->csl = NULL;
  m_->phase_names = NULL;
  m_->component = NULL;
  m_->component_index_eliminated = -1;
  m_->setup_phases = PETSC_FALSE;
  m_->setup = PETSC_FALSE;
  m_->nphases = 0;
  m_->ncomponents = 0;
  m_->nactive_s = 0;
  m_->active_set_s = NULL;
  m_->map_set_s = NULL;
  m_->nactive_l = 0;
  m_->active_set_l = NULL;
  m_->map_set_l = NULL;
  
  m_->view_bulk = PETSC_TRUE;
  m_->view_phase = PETSC_TRUE;
  m_->view_vel = PETSC_TRUE;
  m_->view_interior_v = PETSC_FALSE;
  m_->view_interior_c = PETSC_TRUE;
  m_->view_gamma = PETSC_TRUE;
  m_->view_phi = PETSC_TRUE;
  m_->view_T = PETSC_TRUE;
  
  *m  = m_;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_TPMCSetPhases"
PetscErrorCode _TPMCSetPhases(TPMC m,PetscInt nphases,const char *names[])
{
  PetscInt i;
  PetscErrorCode ierr;
  
  if (m->setup) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot define phases after MPMCSetup() has been called");
  if (m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot define phases after MPMCSetPhases() has been called");
  if (m->phase_names) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot re-define phases after MPMCSetPhases() has been called");
  
  m->nphases = nphases;
  ierr = PetscMalloc1(nphases,&m->phase_names);CHKERRQ(ierr);
  for (i=0; i<m->nphases; i++) {
    ierr = PetscMalloc1(PETSC_MAX_PATH_LEN,&m->phase_names[i]);CHKERRQ(ierr);
    PetscSNPrintf(m->phase_names[i],PETSC_MAX_PATH_LEN-1,"%s",names[i]);
  }
  
  if (!m->csl) {
    ierr = PetscMalloc1(m->nphases,&m->csl);CHKERRQ(ierr);
    for (i=0; i<m->nphases; i++) {
      m->csl[i] = NULL;
    }
  }
  m->setup_phases = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCCreate"
PetscErrorCode TPMCCreate(MPI_Comm comm,TPMC *m)
{
  TPMC m_;
  const char *phase_names[] = { "solid" , "liquid" };
  PetscInt i;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(comm,PETSC_ERR_SUP,"Currently this is only supported for comm.size = 1");
  
  ierr = _TPMCCreate(comm,&m_);CHKERRQ(ierr);
  ierr = _TPMCSetPhases(m_,PHASE_MAX,phase_names);CHKERRQ(ierr);
  
  for (i=0; i<m_->nphases; i++) {
    CSLPoly sl;
    
    CSLPolyCreate(&sl);
    CSLPolySetInterpolateType(sl,I_WENO3);
    CSLPolySetSubcellResolution(sl,1,1);
    
    m_->csl[i] = sl;
  }

  CSLPolySetCacheCharacteristics(m_->csl[PHASE_IDX_SOLID]);
  
  *m  = m_;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCDestroy"
PetscErrorCode TPMCDestroy(TPMC *_m)
{
  TPMC m;
  PetscInt i,k;
  PetscErrorCode ierr;
  
  if (!_m) PetscFunctionReturn(0);
  if (!(*_m)) PetscFunctionReturn(0);
  m = *_m;
  
  for (i=0; i<m->nphases; i++) {
    ierr = PetscFree(m->phase_names[i]);CHKERRQ(ierr);
  }
  for (k=0; k<m->ncomponents; k++) {
    ierr = ComponentFDestroy(&m->component[k]);CHKERRQ(ierr);
  }
  ierr = PetscFree(m->phase_names);CHKERRQ(ierr);
  free(m->component);
  
  ierr = PetscFree(m->active_set_s);CHKERRQ(ierr);
  ierr = PetscFree(m->active_set_l);CHKERRQ(ierr);
  ierr = PetscFree(m->map_set_s);CHKERRQ(ierr);
  ierr = PetscFree(m->map_set_l);CHKERRQ(ierr);

  ierr = PetscFree(m->cbar_c);CHKERRQ(ierr);
  ierr = PetscFree(m->cs_c);CHKERRQ(ierr);
  ierr = PetscFree(m->cl_c);CHKERRQ(ierr);
  ierr = PetscFree(m->phi_c);CHKERRQ(ierr);
  ierr = PetscFree(m->gamma_c);CHKERRQ(ierr);

  ierr = PetscFree(m->temperature_prev_v);CHKERRQ(ierr);
  ierr = PetscFree(m->temperature_v);CHKERRQ(ierr);
  ierr = PetscFree(m->pressure_v);CHKERRQ(ierr);
  ierr = PetscFree(m);CHKERRQ(ierr);

  *_m  = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ComponentFDestroy"
PetscErrorCode ComponentFDestroy(ComponentF *_c)
{
  PetscErrorCode ierr;
  ComponentF c;
  
  if (!_c) PetscFunctionReturn(0);
  if (!(*_c)) PetscFunctionReturn(0);
  c = *_c;
  
  ierr = PetscFree(c->phase_active);CHKERRQ(ierr);
  
  ierr = PetscFree(c);CHKERRQ(ierr);
  *_c = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ComponentFCreate"
PetscErrorCode ComponentFCreate(const char name[],PetscInt nphases,PetscBool active[],PetscBool solve,ComponentF *c)
{
  ComponentF c_;
  PetscInt i;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&c_);CHKERRQ(ierr);
  ierr = PetscMemzero(c_,sizeof(struct _p_ComponentF));CHKERRQ(ierr);
  
  c_->nphases = nphases;
  c_->solve = solve;
  ierr = PetscSNPrintf(c_->name,PETSC_MAX_PATH_LEN-1,"%s",name);CHKERRQ(ierr);
  ierr = PetscMalloc1(nphases,&c_->phase_active);CHKERRQ(ierr);
  for (i=0; i<nphases; i++) {
    c_->phase_active[i] = active[i];
  }
  
  *c  = c_;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "private_TPMCAddComponent"
PetscErrorCode private_TPMCAddComponent(TPMC m,const char name[],PetscBool active[],PetscBool solve)
{
  PetscErrorCode ierr;
  
  if (m->setup) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add new components after TPMCSetup() has been called");
  if (!m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add a component. Must call _TPMCSetPhases() first");
  if (!active) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must provide list of length %D indicating if this component is present (or not) in each phase",m->nphases);
  
  if (m->component == NULL) {
    m->component = malloc(sizeof(ComponentF));
  } else {
    ComponentF *tmp;
    
    tmp = realloc(m->component,(m->ncomponents+1)*sizeof(ComponentF));
    m->component = tmp;
  }
  
  ierr = ComponentFCreate(name,m->nphases,active,solve,&m->component[m->ncomponents]);CHKERRQ(ierr);
  
  m->ncomponents++;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCAddComponent"
PetscErrorCode TPMCAddComponent(TPMC m,const char name[])
{
  PetscErrorCode ierr;
  PetscBool active[] = { PETSC_TRUE, PETSC_TRUE };
  
  if (m->setup) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add new components after TPMCSetup() has been called");
  if (!m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add a component. Must call _TPMCSetPhases() first");
  ierr = private_TPMCAddComponent(m,name,active,PETSC_TRUE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCComponentSetPhaseActive"
PetscErrorCode TPMCComponentSetPhaseActive(TPMC m,PetscInt index,PetscBool active[])
{
  if (m->setup) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add new components after TPMCSetup() has been called");
  if (!m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add a component. Must call _TPMCSetPhases() first");
  if (!active) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must provide list of length %D indicating if this component is present (or not) in each phase",m->nphases);
  
  if (index < 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Component index must be >= 0");
  if (index >= m->ncomponents) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Component index must be <= %D",m->ncomponents);
  
  m->component[index]->phase_active[0] = active[0];
  m->component[index]->phase_active[1] = active[1];
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCComponentEliminate"
PetscErrorCode TPMCComponentEliminate(TPMC m,PetscInt index)
{
  if (m->setup) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add new components after TPMCSetup() has been called");
  if (!m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot add a component. Must call _TPMCSetPhases() first");
  
  if (index < 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Component index must be >= 0");
  if (index >= m->ncomponents) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Component index must be <= %D",m->ncomponents);
  
  m->component[index]->solve = PETSC_FALSE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetup_ActiveSets"
PetscErrorCode TPMCSetup_ActiveSets(TPMC m)
{
  PetscInt k;
  PetscErrorCode ierr;
  
  if (m->active_set_s) PetscFunctionReturn(0);;
  
  ierr = PetscMalloc1(m->ncomponents,&m->active_set_s);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->ncomponents,&m->active_set_l);CHKERRQ(ierr);
  m->nactive_s = 0;
  m->nactive_l = 0;
  for (k=0; k<m->ncomponents; k++) {
    m->active_set_s[k] = -1;
    m->active_set_l[k] = -1;
    
    if (m->component[k]->solve) {
      if (m->component[k]->phase_active[PHASE_IDX_SOLID]) {
        m->active_set_s[k] = m->nactive_s;
        m->nactive_s++;
      }
      if (m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
        m->active_set_l[k] = m->nactive_l;
        m->nactive_l++;
      }
    }
  }

  ierr = PetscMalloc1(m->nactive_s,&m->map_set_s);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->nactive_l,&m->map_set_l);CHKERRQ(ierr);

  m->nactive_s = 0;
  m->nactive_l = 0;
  for (k=0; k<m->ncomponents; k++) {
    
    if (m->component[k]->solve) {
      if (m->component[k]->phase_active[PHASE_IDX_SOLID]) {
        m->map_set_s[m->nactive_s] = k;
        m->nactive_s++;
      }
      
      if (m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
        m->map_set_l[m->nactive_l] = k;
        m->nactive_l++;
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetup"
PetscErrorCode TPMCSetup(TPMC m)
{
  PetscErrorCode ierr;
  PetscInt i,k;
  PetscInt cnt ;
  
  if (m->setup) PetscFunctionReturn(0);
  if (!m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must call _TPMCSetPhases() before TPMCSetup()");
  if (m->ncomponents == 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"No components have been defined. You must call MPMCAddComponent() at least once");
  
  //if (!m->thermodyn_calc) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must call TPMCSetThermoDynamicMethod() before TPMCSetup()");
  
  for (i=0; i<m->nphases; i++) {
    if (m->csl[i] == NULL) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"CSLPoly objects not created. Slot %D is NULL",i);
  }
  
  /* check consistency of input */
  for (k=0; k<m->ncomponents; k++) {
    cnt = 0;
    for (i=0; i<m->nphases; i++) {
      if (m->component[k]->phase_active[i]) {
        cnt++;
      }
    }
    if (cnt == 0) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Components %s is not present in any phase",m->component[k]->name);
    }
  }
  
  m->component_index_eliminated = -1;
  cnt = 0;
  for (k=0; k<m->ncomponents; k++) {
    if (m->component[k]->solve) {
      cnt++;
    } else {
      m->component_index_eliminated = k;
    }
  }
  if (cnt == m->ncomponents) {
    /* search for least dynamic component to eliminate */
    for (k=0; k<m->ncomponents; k++) {
      if (m->component[k]->phase_active[PHASE_IDX_SOLID] && !m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
        m->component_index_eliminated = k;
        break;
      }
    }
    /* otherwise take the last component */
    if (m->component_index_eliminated == -1) {
      m->component_index_eliminated = m->ncomponents - 1;
    }
  }
  
  if (cnt == m->ncomponents) {
    PetscPrintf(PETSC_COMM_WORLD,"[TPMCSetup] User did not specify a component to eliminate - TPMC is eliminating component %s[%D]\n",m->component[m->component_index_eliminated]->name,m->component_index_eliminated);
    PetscPrintf(PETSC_COMM_WORLD,"[TPMCSetup] You can explicitly call TPMCComponentEliminate() to control the component eliminated\n");
  }
  
  if (cnt < (m->ncomponents-1)) {
    SETERRQ3(PETSC_COMM_WORLD,PETSC_ERR_USER,"Non-optimal number of components requested to be solved. ncomponents = %D, optimal %D, found %D",m->ncomponents,m->ncomponents-1,cnt);
  }

  ierr = TPMCSetup_ActiveSets(m);CHKERRQ(ierr);
  
  for (i=0; i<m->nphases; i++) {
    if (m->csl[i]->poly_npoints == 0) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"CSLPoly objects has not been configured. Slot %D does not have a polygon defined. Must call TPMCConfigureCSLPoly() before TPMCSetup()",i);
  }
  CSLPolySetBlockSize(m->csl[PHASE_IDX_SOLID],m->nactive_s);

#if HAVE_GAMMA_SUPPORT
  CSLPolySetBlockSizeImplicit(m->csl[PHASE_IDX_SOLID],1);
  CSLPolySetImplicitFieldEvaluators(m->csl[PHASE_IDX_SOLID],porosity_ifield_evaluate_at_point,porosity_ifield_evaluate_at_boundarypoint,(void*)m);
#endif
  
  CSLPolySetUpFields(m->csl[PHASE_IDX_SOLID]);

  CSLPolySetBlockSize(m->csl[PHASE_IDX_LIQUID],m->nactive_l);
  CSLPolySetUpFields(m->csl[PHASE_IDX_LIQUID]);

  //CSLPolySetApplyGlobalBounds(m->csl[PHASE_IDX_LIQUID]);
  //for (k=0; k<m->ncomponents; k++) {
  //  CSLPolySetGlobalBounds(m->csl[PHASE_IDX_LIQUID],k,1.0e-20,0.2);
  //}
  
  /* allocate buffers */
  m->ncells = m->csl[PHASE_IDX_SOLID]->ncells;

  ierr = PetscMalloc1(m->ncomponents*m->ncells,&m->cbar_c);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->ncomponents*m->ncells,&m->cs_c);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->ncomponents*m->ncells,&m->cl_c);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->ncells,&m->phi_c);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->ncells,&m->gamma_c);CHKERRQ(ierr);

  for (i=0; i<m->ncells; i++) {
    m->phi_c[i] = 0.0;
    m->gamma_c[i] = 0.0;
  }
  for (i=0; i<m->ncomponents*m->ncells; i++) {
    m->cbar_c[i] = 0.0;
    m->cs_c[i] = 0.0;
    m->cl_c[i] = 0.0;
  }
  
  m->nvert = m->csl[PHASE_IDX_SOLID]->nvert;
  ierr = PetscMalloc1(m->nvert,&m->pressure_v);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->nvert,&m->temperature_v);CHKERRQ(ierr);
  ierr = PetscMalloc1(m->nvert,&m->temperature_prev_v);CHKERRQ(ierr);

  for (i=0; i<m->nvert; i++) {
    m->pressure_v[i] = 0.0;
    m->temperature_v[i] = 0.0;
    m->temperature_prev_v[i] = 0.0;
  }
  
  
  m->setup = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCView"
PetscErrorCode TPMCView(TPMC m)
{
  PetscInt k,i;
  
  if (!m->setup_phases) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Phases not defined - must call _TPMCSetPhases() first");
  
  PetscPrintf(PETSC_COMM_WORLD,"============== TwoPhase-MultiComponent ==============\n");
  printf("%25s","");
  for (i=0; i<m->nphases; i++) {
    printf("%10s",m->phase_names[i]);
  }
  printf("\n");
  for (k=0; k<m->ncomponents; k++) {
    printf("%20s [%.2d]",m->component[k]->name,k);
    for (i=0; i<m->nphases; i++) {
      printf("%9s%d","",(int)m->component[k]->phase_active[i]);
    }
    if (m->component[k]->solve) printf("%5ssolve","");
    else printf("%5seliminate","");
    printf("\n");
  }
  if (m->active_set_s) {
    printf("  [Solid components]\n");
    for (k=0; k<m->ncomponents; k++) {
      if (m->active_set_s[k] != -1) {
        printf("    natural %.2d -> active  %.2d\n",k,m->active_set_s[k]);
      }
    }
    for (k=0; k<m->nactive_s; k++) {
      printf("    active  %.2d -> natural %.2d\n",k,m->map_set_s[k]);
    }
  }
  if (m->active_set_l) {
    printf("  [Liquid components]\n");
    for (k=0; k<m->ncomponents; k++) {
      if (m->active_set_l[k] != -1) {
        printf("    natural %.2d -> active  %.2d\n",k,m->active_set_l[k]);
      }
    }
    for (k=0; k<m->nactive_l; k++) {
      printf("    active  %.2d -> natural %.2d\n",k,m->map_set_l[k]);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCConfigureCSLPoly"
PetscErrorCode TPMCConfigureCSLPoly(TPMC m,PetscInt npoly,PetscReal polycoor[],PetscInt edgelabel[],PetscReal ds,PetscInt mcell_s,PetscInt mcell_l)
{
  PetscInt i;
  
  for (i=0; i<m->nphases; i++) {
    CSLPoly dsl;
    
    dsl = m->csl[i];
    
    CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
    CSLPolySetDomainDefault(dsl,ds);

    CSLPolySetUpMesh(dsl);
    CSLPolyBoundaryPCreate(dsl);
    CSLPolyMarkCells(dsl);
    CSLPolyMarkVertices(dsl);
    CSLPolyLabelFacets(dsl);
    CSLPolyLabelCells(dsl);
    
  }

  CSLPolySetSubcellResolution(m->csl[PHASE_IDX_SOLID],mcell_s,mcell_s);
  CSLPolySetSubcellResolution(m->csl[PHASE_IDX_LIQUID],mcell_l,mcell_l);
  
  PetscFunctionReturn(0);
}


/*
 Store into (1-phi).c^s_k, phi.c^l_k in each CSLPoly using the active_set_{s,l} arrays
*/
#undef __FUNCT__
#define __FUNCT__ "TPMCSetWeightedConcentrations_Interior"
PetscErrorCode TPMCSetWeightedConcentrations_Interior(TPMC m)
{
  PetscInt k,c,ncells,nf,nk;
  CSLPoly csl;
  
  nk = m->ncomponents;
  
  /* solid */
  csl = m->csl[PHASE_IDX_SOLID];
  nf = csl->nfields;
  ncells = csl->ncells;
  for (k=0; k<nk; k++) {
    if (!m->component[k]->solve) { continue; }
    
    if (m->component[k]->phase_active[PHASE_IDX_SOLID]) {
      PetscInt field_index;
      
      field_index = m->active_set_s[k];
#ifdef MC_DEBUG
      printf("  [TPMCSetWeightedConcentrations][solid] inserting component %s[%d] into CSL_solid.field[%d] \n",m->component[k]->name,k,field_index);
#endif
      for (c=0; c<ncells; c++) {
        CellP cell;

        cell = csl->cell_list[c];
        if (cell->type != COOR_INTERIOR) continue;

        csl->value_c[nf*c+field_index] = m->cs_c[nk*c+k] * (1.0 - m->phi_c[c]);
      }
    }
  }
  
  /* liquid */
  csl = m->csl[PHASE_IDX_LIQUID];
  nf = csl->nfields;
  ncells = csl->ncells;
  for (k=0; k<nk; k++) {
    if (!m->component[k]->solve) { continue; }

    if (m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
      PetscInt field_index;
      
      field_index = m->active_set_l[k];
#ifdef MC_DEBUG
      printf("  [TPMCSetWeightedConcentrations][liquid] inserting component %s[%d] into CSL_liquid.field[%d] \n",m->component[k]->name,k,field_index);
#endif
      for (c=0; c<ncells; c++) {
        CellP cell;
        
        cell = csl->cell_list[c];
        if (cell->type != COOR_INTERIOR) continue;

        csl->value_c[nf*c+field_index] = m->cl_c[nk*c+k] * (m->phi_c[c]);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCFuseWeightedConcentrations"
PetscErrorCode TPMCFuseWeightedConcentrations(TPMC m)
{
  PetscInt f,c,ncells,nf,nk;
  CSLPoly csl;
  PetscErrorCode ierr;
  
  nk = m->ncomponents;
  
  ierr = PetscMemzero(m->cbar_c,sizeof(PetscReal)*m->ncomponents*m->ncells);CHKERRQ(ierr);
  
  /* solid */
  csl = m->csl[PHASE_IDX_SOLID];
  nf = m->nactive_s;
  ncells = csl->ncells;
  for (f=0; f<nf; f++) {
    PetscInt field_index;
    
    field_index = m->map_set_s[f];
#ifdef MC_DEBUG
    printf("  [TPMCFuseWeightedConcentrations][solid] summing CSL_solid.field[%d] into bulk component %s[%d] \n",f,m->component[field_index]->name,field_index);
#endif
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      m->cbar_c[nk*c + field_index] += csl->value_c[nf*c + f];
    }
  }
  
  /* liquid */
  csl = m->csl[PHASE_IDX_LIQUID];
  nf = m->nactive_l;
  ncells = csl->ncells;
  for (f=0; f<nf; f++) {
    PetscInt field_index;
    
    field_index = m->map_set_l[f];
#ifdef MC_DEBUG
    printf("  [TPMCFuseWeightedConcentrations][liquid] summing CSL_liquid.field[%d] into bulk component %s[%d] \n",f,m->component[field_index]->name,field_index);
#endif
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      m->cbar_c[nk*c + field_index] += csl->value_c[nf*c + f];
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCFuseWeightedConcentrationsArray"
PetscErrorCode TPMCFuseWeightedConcentrationsArray(TPMC m,const PetscReal cl[],const PetscReal cs[],PetscReal cbulk[])
{
  PetscInt f,c,ncells,nf,nk;
  CSLPoly csl;
  PetscErrorCode ierr;
  
  nk = m->ncomponents;
  
  ierr = PetscMemzero(cbulk,sizeof(PetscReal)*m->ncomponents*m->ncells);CHKERRQ(ierr);
  
  /* solid */
  csl = m->csl[PHASE_IDX_SOLID];
  nf = m->nactive_s;
  ncells = csl->ncells;
  for (f=0; f<nf; f++) {
    PetscInt field_index;
    
    field_index = m->map_set_s[f];
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      cbulk[nk*c + field_index] += cs[nf*c + f];
    }
  }
  
  /* liquid */
  csl = m->csl[PHASE_IDX_LIQUID];
  nf = m->nactive_l;
  ncells = csl->ncells;
  for (f=0; f<nf; f++) {
    PetscInt field_index;
    
    field_index = m->map_set_l[f];
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      cbulk[nk*c + field_index] += cl[nf*c + f];
    }
  }
  
  PetscFunctionReturn(0);
}

///
#undef __FUNCT__
#define __FUNCT__ "TPMCEnforceEquilibrium_Interior"
PetscErrorCode TPMCEnforceEquilibrium_Interior(TPMC m,PetscReal cbulk_c[])
{
  PetscInt k,c,ncells,nk;
  PetscReal cbulk[20],T,P,cs[20],cl[20],phi;
  CSLPoly csl;
  PetscBool valid;
  PetscErrorCode ierr;
  
  nk = m->ncomponents;
  ncells = m->ncells;
  csl = m->csl[0];
  
  ierr = PetscMemzero(m->cl_c,sizeof(PetscReal)*m->ncomponents*m->ncells);CHKERRQ(ierr);
  ierr = PetscMemzero(m->cs_c,sizeof(PetscReal)*m->ncomponents*m->ncells);CHKERRQ(ierr);
  ierr = PetscMemzero(m->phi_c,sizeof(PetscReal)*m->ncells);CHKERRQ(ierr);

  for (c=0; c<ncells; c++) {
    CellP cell;
    PetscReal *coor;
    
    cell = csl->cell_list[c];
    
    if (cell->type != COOR_INTERIOR) {
      for (k=0; k<nk; k++) {
        m->cs_c[nk*c + k] = 0.0;
        m->cl_c[nk*c + k] = 0.0;
      }
      m->phi_c[c] = 0.0;
      continue;
    }
  
    /* get coordinate, P, T */
    coor = csl->cell_list[c]->coor;
    
    /* compute P, T at coor */
    TPMCInterpolatePT(m,(PetscReal*)coor,&P,&T);

    for (k=0; k<nk; k++) {
      cbulk[k] = cbulk_c[nk*c + k];
      cl[k] = 0.0;
      cs[k] = 0.0;
    }
    phi = 0.0;
    
    /* enfore equilibrium */
    ierr = m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, m->ncomponents, cbulk, cl, cs, &phi, &valid, m->thermodyn_ctx );CHKERRQ(ierr);
    
    if (!valid) {
      
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"[TPMC] Thermodynamic evaluation failed");
    }
    for (k=0; k<nk; k++) {
      m->cs_c[nk*c + k] = cs[k];
      m->cl_c[nk*c + k] = cl[k];
    }
    m->phi_c[c] = phi;
  }
  PetscFunctionReturn(0);
}
///

#undef __FUNCT__
#define __FUNCT__ "TPMCApplyUnitySumPhaseComponents_Interior"
PetscErrorCode TPMCApplyUnitySumPhaseComponents_Interior(TPMC m)
{
  PetscInt k,c,ncells,nk;
  PetscInt index_eliminated;
  CSLPoly csl;
  
  nk = m->ncomponents;
  ncells = m->ncells;
  csl = m->csl[0];

  /* determine eliminated component - intialize value to 1.0 */
  index_eliminated = m->component_index_eliminated;
  k = index_eliminated;
#ifdef MC_DEBUG
  printf("  [TPMCApplyUnitySumPhaseComponents] initializing cs,cl to 1.0 for eliminated component %s[%d] \n",m->component[k]->name,k);
#endif
  for (c=0; c<ncells; c++) {
    CellP cell;
    
    cell = csl->cell_list[c];
    if (cell->type != COOR_INTERIOR) { continue; }
    
    m->cs_c[nk*c + k] = 1.0;
    m->cl_c[nk*c + k] = 1.0;
  }

  /* apply unity sum to the individual liquid and solid concentrations */
  for (k=0; k<nk; k++) {
    if (k == index_eliminated) continue;

#ifdef MC_DEBUG
    printf("  [TPMCApplyUnitySumPhaseComponents] subtracting cs[%d],cl[%d] from eliminated component %s[%d] \n",k,k,m->component[index_eliminated]->name,index_eliminated);
#endif

    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) { continue; }

      m->cs_c[nk*c + index_eliminated] -= m->cs_c[nk*c + k];
      m->cl_c[nk*c + index_eliminated] -= m->cl_c[nk*c + k];
    }
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCApplyUnitySumBulkComponents_Interior"
PetscErrorCode TPMCApplyUnitySumBulkComponents_Interior(TPMC m,PetscReal cbar_c[])
{
  PetscInt k,c,ncells,nk;
  PetscInt index_eliminated;
  CSLPoly csl;
  
  nk = m->ncomponents;
  ncells = m->ncells;
  csl = m->csl[0];
  
  /* determine eliminated component - intialize value to 1.0 */
  index_eliminated = m->component_index_eliminated;
  
  /* apply unity sum to the bulk concentrations */
  for (c=0; c<ncells; c++) {
    CellP cell;
    
    cell = csl->cell_list[c];
    if (cell->type != COOR_INTERIOR) { continue; }
    
    cbar_c[nk*c + index_eliminated] = 1.0;
    for (k=0; k<nk; k++) {
      if (k == index_eliminated) continue;
      
      cbar_c[nk*c + index_eliminated] -= cbar_c[nk*c + k];
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCUpdateVelocity"
PetscErrorCode TPMCUpdateVelocity(TPMC m)
{
  CSLPoly dsl;
  
  /* Prepare for BC specification */
  dsl = m->csl[PHASE_IDX_SOLID];
  CSLPolyComputeCellAverageDivVelocity(dsl);
  CSLPolyMarkInflowFacets(dsl);
  
  dsl = m->csl[PHASE_IDX_LIQUID];
  CSLPolyComputeCellAverageDivVelocity(dsl);
  CSLPolyMarkInflowFacets(dsl);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCGenericFacetBCIterator"
PetscErrorCode TPMCGenericFacetBCIterator(TPMC m,PetscInt edge,void (*F)(const PetscReal*,const PetscReal*,const PetscReal *,PetscReal*,void*),void *ctx)
{
  PetscInt s;
  PetscReal *vv;
  CSLPoly p;
  PetscReal cbulk[20],T,P,cs[20],cl[20],phi;
  PetscInt f;
  PetscBool valid;
  PetscErrorCode ierr;
  
  /* call user function for inflowing boundaries */
  p = m->csl[PHASE_IDX_SOLID];
  for (s=0; s<p->boundary_nfacets; s++) {
    const PetscReal *coor;
    const PetscReal *normal;
    const PetscReal *velocity;
    
    if (p->boundary_facet_list[s]->label != edge) continue;
    if (p->boundary_facet_list[s]->is_inflow == -1) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must call TPMCUpdateVelocity() to determine if BC is inflow/outflow");
    };
    if (p->boundary_facet_list[s]->is_inflow != 1) continue;
    
    coor     = p->boundary_facet_list[s]->coor;
    normal   = p->boundary_facet_list[s]->normal;
    velocity = p->boundary_facet_list[s]->velocity;
    vv       = p->boundary_facet_list[s]->value;
    
    PetscMemzero(cbulk,sizeof(PetscReal)*m->ncomponents);
    F((const PetscReal*)coor,(const PetscReal*)normal,(const PetscReal*)velocity,cbulk,ctx);
    ierr = TPMCEnforceUnitySumBulkComponentsAtPoint(m,cbulk);CHKERRQ(ierr);
    
    /* compute P, T at coor */
    TPMCInterpolatePT(m,(PetscReal*)coor,&P,&T);
    
    /* enfore equilibrium */
    ierr = m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, m->ncomponents, cbulk, cl, cs, &phi, &valid, m->thermodyn_ctx );CHKERRQ(ierr);
    
    /* fetch and set only the ones I require */
    for (f=0; f<m->nactive_s; f++) {
      vv[f] = cs[ m->map_set_s[f] ] * (1.0 - phi);
    }
#if HAVE_GAMMA_SUPPORT
    for (f=0; f<p->nimplicitfields; f++) {
      ivv[f] = 1.0 - phi;
    }
#endif
    
    p->boundary_facet_list[s]->bcset = 1;
  }

  p = m->csl[PHASE_IDX_LIQUID];
  for (s=0; s<p->boundary_nfacets; s++) {
    const PetscReal *coor;
    const PetscReal *normal;
    const PetscReal *velocity;
    
    if (p->boundary_facet_list[s]->label != edge) continue;
    if (p->boundary_facet_list[s]->is_inflow == -1) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Must call TPMCUpdateVelocity() to determine if BC is inflow/outflow");
    };
    if (p->boundary_facet_list[s]->is_inflow != 1) continue;
    
    coor     = p->boundary_facet_list[s]->coor;
    normal   = p->boundary_facet_list[s]->normal;
    velocity = p->boundary_facet_list[s]->velocity;
    vv       = p->boundary_facet_list[s]->value;
    
    PetscMemzero(cbulk,sizeof(PetscReal)*m->ncomponents);
    F(coor,normal,velocity,cbulk,ctx);
    
    /* compute P, T at coor */
    TPMCInterpolatePT(m,(PetscReal*)coor,&P,&T);
    
    /* enfore equilibrium */
    ierr = m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, m->ncomponents, cbulk, cl, cs, &phi, &valid, m->thermodyn_ctx );CHKERRQ(ierr);
    
    /* fetch and set only the ones I require */
    for (f=0; f<m->nactive_l; f++) {
      vv[f] = cl[ m->map_set_l[f] ] * (phi);
    }

    p->boundary_facet_list[s]->bcset = 1;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCInitializeBC"
PetscErrorCode TPMCInitializeBC(TPMC m)
{
  CSLPolyInitializeBC(m->csl[0]);
  CSLPolyInitializeBC(m->csl[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCVerifyBC"
PetscErrorCode TPMCVerifyBC(TPMC m)
{
  CSLPolyVerifyBC(m->csl[0]);
  CSLPolyVerifyBC(m->csl[1]);
  PetscFunctionReturn(0);
}

/*
 
Calling pattern
 
TPMCUpdateVelocity()
 
TPMCInitializeBC()
 // TPMCGenericFacetBCIterator()
 // TPMCGenericFacetBCIterator()
 // TPMCGenericFacetBCIterator()
TPMCVerifyBC()
*/


#undef __FUNCT__
#define __FUNCT__ "TPMCEquilibrateInitialCondition"
PetscErrorCode TPMCEquilibrateInitialCondition(TPMC m)
{
  PetscErrorCode ierr;
  ierr = TPMCApplyUnitySumBulkComponents_Interior(m,m->cbar_c);CHKERRQ(ierr);
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cbar_c);
  
  ierr = TPMCEnforceEquilibrium_Interior(m,m->cbar_c);CHKERRQ(ierr);
#if HAVE_GAMMA_SUPPORT
  /* set initial values for porosity */
  {
    CSLPoly csl;
    PetscInt c,ncells;
    
    csl = m->csl[PHASE_IDX_SOLID];
    ncells = csl->ncells;
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      csl->ivalue_c[c] = 1.0 - m->phi_c[c];
    }
  }
#endif
  ierr = TPMCApplyUnitySumPhaseComponents_Interior(m);CHKERRQ(ierr);
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cs_c);
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cl_c);
  
  ierr = TPMCSetWeightedConcentrations_Interior(m);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 Given \bar{c}_k, decompose into c^s_k, c^l_k, phi

 Store into (1-phi).c^s_k, phi.c^l_k in each CSLPoly using the active_set_{s,l} arrays
 
 Update weighted concentration (1-phi).c^s_k
 Update weighted concentration phi.c^l_k
 
 Combine concentrations in \bar{c}
*/
#undef __FUNCT__
#define __FUNCT__ "TPMCSolve"
PetscErrorCode TPMCSolve(TPMC m,PetscInt tk,PetscReal dt,PetscReal time_tk)
{
  CSLPoly dsl;
  PetscBool positivity_enforced;
  PetscErrorCode ierr;

  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve] Cbulk_input",m->ncells,m->ncomponents,m->cbar_c);CHKERRQ(ierr);

  /* BC reconstruction */
  dsl = m->csl[PHASE_IDX_SOLID];
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve] (1-phi).Cs_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve][reconstructed] (1-phi).Cs_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);

  dsl = m->csl[PHASE_IDX_LIQUID];
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve] (phi).Cl_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve][reconstructed] (phi).Cl_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);

  /* Update */
  dsl = m->csl[PHASE_IDX_SOLID];
  //CSLPolyUpdate(dsl,time_tk,dt);
  //CSLPolyUpdate_Conservative(dsl,time_tk,dt);
  //CSLPolyUpdateI_Conservative(dsl,time_tk,dt);
  CSLPolyUpdate_Strang2Multiplicative(dsl,time_tk,dt);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] (1-phi).Cs_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  CSLPolyReconstructExteriorFacetFields(dsl);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][reconstructed] (1-phi).Cs_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);

  dsl = m->csl[PHASE_IDX_LIQUID];
  //CSLPolyUpdate(dsl,time_tk,dt);
  //CSLPolyUpdate_Conservative(dsl,time_tk,dt);
  //CSLPolyUpdateI_Conservative(dsl,time_tk,dt);
  CSLPolyUpdate_Strang2Multiplicative(dsl,time_tk,dt);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] (phi).Cl_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  CSLPolyReconstructExteriorFacetFields(dsl);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][reconstructed] (phi).Cl_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);

  /* re-assemble */
  ierr = TPMCFuseWeightedConcentrations(m);CHKERRQ(ierr);
  ierr = TPMCEnforceFieldPositivity(m,m->ncomponents*m->ncells,m->cbar_c,&positivity_enforced);CHKERRQ(ierr);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][fuse] Cbulk",m->ncells,m->ncomponents,m->cbar_c);CHKERRQ(ierr);
  ierr = TPMCApplyUnitySumBulkComponents_Interior(m,m->cbar_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][unity-sum] Cbulk",m->ncells,m->ncomponents,m->cbar_c);CHKERRQ(ierr);
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cbar_c);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cbulk_extrapolate",m->ncells,m->ncomponents,m->cbar_c);CHKERRQ(ierr);

  ierr = TPMCEnforceEquilibrium_Interior(m,m->cbar_c);CHKERRQ(ierr);
  //ierr = TPMCApplyUnitySumPhaseComponents_Interior(m);CHKERRQ(ierr);

  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] porosity",m->ncells,1,m->phi_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cs",m->ncells,m->ncomponents,m->cs_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cl",m->ncells,m->ncomponents,m->cl_c);CHKERRQ(ierr);
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cs_c);
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cl_c);

  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cs_extrapolate",m->ncells,m->ncomponents,m->cs_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cl_extrapolate",m->ncells,m->ncomponents,m->cl_c);CHKERRQ(ierr);

  // This should be considered as a necessary requirement for non-analytic equilibrium models
  //ierr = TPMCSetWeightedConcentrations_Interior(m);CHKERRQ(ierr);
  
  /* compute melting rate */
  {
    CSLPoly csl;
    PetscInt c,ncells;
    
    csl = m->csl[PHASE_IDX_SOLID];
    ncells = csl->ncells;

    for (c=0; c<ncells; c++) {
      m->gamma_c[c] = 0.0;
    }
  }
#if HAVE_GAMMA_SUPPORT
  {
    CSLPoly csl;
    PetscInt c,ncells,nchar,nc;
    PetscReal dVa,dVd,int_phi_dVa,int_phi_dVd,avg_gamma;
    CharacteristicP *traj_cell;
    //double minr=1.0e32,maxr=-1.0e32;
    
    csl = m->csl[PHASE_IDX_SOLID];
    ncells = csl->ncells;
    dVa = csl->dx[0] * csl->dx[1];
    CSLPolyGetCachedCharacteristics(csl,&nchar,&traj_cell);
    nc = 0;
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      if (nc > nchar) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Melting rate computation failed - extracted characteristics are inconsistent with CSL interior");
      
      dVa = traj_cell[nc]->volume0;
      dVd = traj_cell[nc]->volume;

      int_phi_dVa = (1.0 - m->phi_c[c]) * dVa;
      int_phi_dVd = csl->ivalue_c[c] * dVa;
      avg_gamma = (int_phi_dVa - int_phi_dVd)/(dVa * dt);
      m->gamma_c[c] = -avg_gamma;
      
      //double r = dVa/dVd;
      //if (r < minr) { minr = r; }
      //if (r > maxr) { maxr = r; }
      //if (c%20==0) printf("phi_c %+1.4e : phi_c_k %+1.4e\n",m->phi_c[c],csl->ivalue_c[c]);
      //if (c%20==0) printf("  dVa %+1.4e : dVd %+1.4e\n",dVa,dVd);
      //if (c%20==0) printf("  delta (1-phi) %+1.4e\n",(1.0 - m->phi_c[c]) - csl->ivalue_c[c]);
      //if (c%20==0) printf("  ratio (1-phi)/ %+1.4e\n",(1.0 - m->phi_c[c])/csl->ivalue_c[c]);
      //if (c%20==0) printf("  delta (1-phi)/dt %+1.4e\n",((1.0 - m->phi_c[c]) - csl->ivalue_c[c])/dt);
      //if (c%20==0) printf("  actual gamma %+1.4e\n",-avg_gamma);
      nc++;
    }
    //printf("  min/max ratio %+1.4e / %+1.4e \n",minr,maxr);
  }
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] gamma",m->ncells,1,m->gamma_c);CHKERRQ(ierr);
#endif
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],1,m->phi_c);
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],1,m->gamma_c);

  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] porosity_extrapolate",m->ncells,1,m->phi_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] gamma_extrapolate",m->ncells,1,m->gamma_c);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCComputeStateUpdate"
PetscErrorCode TPMCComputeStateUpdate(TPMC m,PetscInt tk,PetscReal dt,PetscReal time_tk,PetscReal cbulk[])
{
  CSLPoly dsl;
  PetscReal *state_s,*state_l;
  PetscInt nf,ncells;
  PetscBool positivity_enforced;
  PetscErrorCode ierr;
  
  dsl = m->csl[PHASE_IDX_SOLID];
  nf = m->nactive_s;
  ncells = dsl->ncells;
  ierr = PetscMalloc1(nf*ncells,&state_s);CHKERRQ(ierr);
  ierr = PetscMemzero(state_s,nf*ncells*sizeof(PetscReal));CHKERRQ(ierr);
  
  dsl = m->csl[PHASE_IDX_LIQUID];
  nf = m->nactive_l;
  ncells = dsl->ncells;
  ierr = PetscMalloc1(nf*ncells,&state_l);CHKERRQ(ierr);
  ierr = PetscMemzero(state_l,nf*ncells*sizeof(PetscReal));CHKERRQ(ierr);

  /* BC reconstruction */
  dsl = m->csl[PHASE_IDX_SOLID];
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve] (1-phi).Cs_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve][reconstructed] (1-phi).Cs_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  
  dsl = m->csl[PHASE_IDX_LIQUID];
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve] (phi).Cl_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[presolve][reconstructed] (phi).Cl_input",m->ncells,dsl->nfields,dsl->value_c);CHKERRQ(ierr);
  
  /* Update */
  dsl = m->csl[PHASE_IDX_SOLID];
  nf = m->nactive_s;
  CSLPolyComputeStateUpdate_Strang2Multiplicative(dsl,time_tk,dt,state_s);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] (1-phi).Cs_input",m->ncells,dsl->nfields,state_s);CHKERRQ(ierr);
  //CSLPolyReconstructExteriorFacetFields(dsl); // not required for state update
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][reconstructed] (1-phi).Cs_input",m->ncells,dsl->nfields,state_s);CHKERRQ(ierr);
  
  dsl = m->csl[PHASE_IDX_LIQUID];
  nf = m->nactive_l;
  CSLPolyComputeStateUpdate_Strang2Multiplicative(dsl,time_tk,dt,state_l);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] (phi).Cl_input",m->ncells,dsl->nfields,state_l);CHKERRQ(ierr);
  //CSLPolyReconstructExteriorFacetFields(dsl); // not required for state update
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][reconstructed] (phi).Cl_input",m->ncells,dsl->nfields,state_l);CHKERRQ(ierr);
  
  /* re-assemble */
  ierr = TPMCFuseWeightedConcentrationsArray(m,state_l,state_s,cbulk);CHKERRQ(ierr);
  ierr = TPMCEnforceFieldPositivity(m,m->ncomponents*m->ncells,cbulk,&positivity_enforced);CHKERRQ(ierr);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][fuse] Cbulk",m->ncells,m->ncomponents,cbulk);CHKERRQ(ierr);
  ierr = TPMCApplyUnitySumBulkComponents_Interior(m,cbulk);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve][unity-sum] Cbulk",m->ncells,m->ncomponents,cbulk);CHKERRQ(ierr);
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,cbulk);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cbulk_extrapolate",m->ncells,m->ncomponents,cbulk);CHKERRQ(ierr);
  
  ierr = PetscFree(state_l);CHKERRQ(ierr);
  ierr = PetscFree(state_s);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMEquilibrateState"
PetscErrorCode TPMEquilibrateState(TPMC m,PetscReal cbulk[])
{
  //PetscBool positivity_enforced;
  PetscErrorCode ierr;
  
  //for (c=0; c<m->ncells*m->ncomponents; c++) {
    //m->cbar_c[c] = cbulk[c];
  //}

  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,cbulk);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cbulk_extrapolate",m->ncells,m->ncomponents,cbulk);CHKERRQ(ierr);

  //ierr = TPMCEnforceFieldPositivity(m,m->ncomponents*m->ncells,cbulk,&positivity_enforced);CHKERRQ(ierr);
  ierr = TPMCEnforceEquilibrium_Interior(m,cbulk);CHKERRQ(ierr);
  //ierr = TPMCApplyUnitySumPhaseComponents_Interior(m);CHKERRQ(ierr);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] porosity",m->ncells,1,m->phi_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cs",m->ncells,m->ncomponents,m->cs_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cl",m->ncells,m->ncomponents,m->cl_c);CHKERRQ(ierr);
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cs_c);
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],m->ncomponents,m->cl_c);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cs_extrapolate",m->ncells,m->ncomponents,m->cs_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] Cl_extrapolate",m->ncells,m->ncomponents,m->cl_c);CHKERRQ(ierr);
  
  /* compute melting rate */
  {
    CSLPoly csl;
    PetscInt c,ncells;
    
    csl = m->csl[PHASE_IDX_SOLID];
    ncells = csl->ncells;
    
    for (c=0; c<ncells; c++) {
      m->gamma_c[c] = 0.0;
    }
  }
#if HAVE_GAMMA_SUPPORT
  {
    CSLPoly csl;
    PetscInt c,ncells,nchar,nc;
    PetscReal dVa,dVd,int_phi_dVa,int_phi_dVd,avg_gamma;
    CharacteristicP *traj_cell;
    //double minr=1.0e32,maxr=-1.0e32;
    
    csl = m->csl[PHASE_IDX_SOLID];
    ncells = csl->ncells;
    dVa = csl->dx[0] * csl->dx[1];
    CSLPolyGetCachedCharacteristics(csl,&nchar,&traj_cell);
    nc = 0;
    for (c=0; c<ncells; c++) {
      CellP cell;
      
      cell = csl->cell_list[c];
      if (cell->type != COOR_INTERIOR) continue;
      
      if (nc > nchar) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Melting rate computation failed - extracted characteristics are inconsistent with CSL interior");
      
      dVa = traj_cell[nc]->volume0;
      dVd = traj_cell[nc]->volume;
      
      int_phi_dVa = (1.0 - m->phi_c[c]) * dVa;
      int_phi_dVd = csl->ivalue_c[c] * dVa;
      avg_gamma = (int_phi_dVa - int_phi_dVd)/(dVa * dt);
      m->gamma_c[c] = -avg_gamma;
      
      //double r = dVa/dVd;
      //if (r < minr) { minr = r; }
      //if (r > maxr) { maxr = r; }
      //if (c%20==0) printf("phi_c %+1.4e : phi_c_k %+1.4e\n",m->phi_c[c],csl->ivalue_c[c]);
      //if (c%20==0) printf("  dVa %+1.4e : dVd %+1.4e\n",dVa,dVd);
      //if (c%20==0) printf("  delta (1-phi) %+1.4e\n",(1.0 - m->phi_c[c]) - csl->ivalue_c[c]);
      //if (c%20==0) printf("  ratio (1-phi)/ %+1.4e\n",(1.0 - m->phi_c[c])/csl->ivalue_c[c]);
      //if (c%20==0) printf("  delta (1-phi)/dt %+1.4e\n",((1.0 - m->phi_c[c]) - csl->ivalue_c[c])/dt);
      //if (c%20==0) printf("  actual gamma %+1.4e\n",-avg_gamma);
      nc++;
    }
    //printf("  min/max ratio %+1.4e / %+1.4e \n",minr,maxr);
  }
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] gamma",m->ncells,1,m->gamma_c);CHKERRQ(ierr);
#endif
  
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],1,m->phi_c);
  CSLPolyExtrapolateExteriorCellFields(m->csl[0],1,m->gamma_c);
  
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] porosity_extrapolate",m->ncells,1,m->phi_c);CHKERRQ(ierr);
  ierr = TPMCCheckFieldForNAN(m,PETSC_TRUE,"[postsolve] gamma_extrapolate",m->ncells,1,m->gamma_c);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 Enhancement:
 
 Interpolate fields from csl[0] and csl[1] independently
 Combine and perform unity sum as required
 
*/
#undef __FUNCT__
#define __FUNCT__ "TPMCFuseWeightedConcentrationsPointwise"
PetscErrorCode TPMCFuseWeightedConcentrationsPointwise(TPMC m,PetscReal cs[],PetscReal cl[],PetscReal cbulk[])
{
  PetscInt f,nf,k,nk;
  PetscInt field_index,index_eliminated;
  PetscErrorCode ierr;
  
  nk = m->ncomponents;
  
  ierr = PetscMemzero(cbulk,sizeof(PetscReal)*m->ncomponents);CHKERRQ(ierr);
  
  /* solid */
  nf = m->nactive_s;
  for (f=0; f<nf; f++) {
    field_index = m->map_set_s[f];
    cbulk[field_index] += cs[f];
  }
  
  /* liquid */
  nf = m->nactive_l;
  for (f=0; f<nf; f++) {
    field_index = m->map_set_l[f];
    cbulk[field_index] += cl[f];
  }
  
  /* determine eliminated component - intialize value to 1.0 */
  index_eliminated = m->component_index_eliminated;
  
  /* apply unity sum to the bulk concentrations */
  cbulk[index_eliminated] = 1.0;
  for (k=0; k<nk; k++) {
    if (k == index_eliminated) continue;
    cbulk[index_eliminated] -= cbulk[k];
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCInterpolateBulkConcentrationAtPoint"
PetscErrorCode TPMCInterpolateBulkConcentrationAtPoint(TPMC m,const PetscReal coor[],PetscReal cbar_p[],PetscBool *inside_poly,PetscBool *inside_box)
{
  PetscInt k,inside_domain,inside_csl_domain;
  PetscReal buffer_cs[CSLPOLY_MAX_FIELDS],buffer_cl[CSLPOLY_MAX_FIELDS];
  PetscBool positivity_enforced;
  PetscErrorCode ierr;
  
  /* this method does not set values if the coor[] is ouside the poly or csl domain */
  CSLPolyInterpolateAtPointWithArray(m->csl[PHASE_IDX_SOLID],m->csl[PHASE_IDX_SOLID]->interp_type,
                                     coor,
                                     m->csl[PHASE_IDX_SOLID]->nfields,(const PetscReal*)m->csl[PHASE_IDX_SOLID]->value_c,
                                     NULL,NULL,
                                     buffer_cs,&inside_domain,&inside_csl_domain);

  CSLPolyInterpolateAtPointWithArray(m->csl[PHASE_IDX_LIQUID],m->csl[PHASE_IDX_LIQUID]->interp_type,
                                     coor,
                                     m->csl[PHASE_IDX_LIQUID]->nfields,(const PetscReal*)m->csl[PHASE_IDX_LIQUID]->value_c,
                                     NULL,NULL,
                                     buffer_cl,&inside_domain,&inside_csl_domain);
  
  TPMCFuseWeightedConcentrationsPointwise(m,buffer_cs,buffer_cl,cbar_p);
  ierr = TPMCEnforceFieldPositivity(m,m->ncomponents,cbar_p,&positivity_enforced);CHKERRQ(ierr);
  
  if (inside_domain == 1) {
    for (k=0; k<m->ncomponents; k++) {
      if (cbar_p[k] < 0.0) {
        PetscInt ci,cj,cellidx;
        
        printf("  [TPMCInterp]: coor (%+1.6e,%+1.6e) cbar[%d] = %+1.6e < 0.0\n",coor[0],coor[1],k,cbar_p[k]);
        
        ci = (PetscInt)( (coor[0] - m->csl[0]->gmin[0])/m->csl[0]->dx[0] );
        cj = (PetscInt)( (coor[1] - m->csl[0]->gmin[1])/m->csl[0]->dx[1] );
        if (ci == m->csl[0]->mx[0]) ci--;
        if (cj == m->csl[0]->mx[1]) cj--;
        cellidx = ci + cj * m->csl[0]->mx[0];
        cbar_p[k] = m->cbar_c[m->ncomponents*cellidx + k];
        printf("  [TPMCInterp]: falling back to using cell data %+1.6e\n",cbar_p[k]);
        if (cbar_p[k] < 0.0) {
          printf("  [TPMCInterp]: coor (%+1.6e,%+1.6e) cbar[%d] = %+1.6e < 0.0\n",coor[0],coor[1],k,cbar_p[k]);
          printf("  [TPMCInterp]: cell %d has a cell constant < 0.0 ... aborting\n",cellidx);
          exit(1);
        }
      }
    }
    
  }
  *inside_poly = PETSC_TRUE;
  if (inside_domain == 0) { *inside_poly = PETSC_FALSE; }
  
  *inside_box = PETSC_TRUE;
  if (inside_csl_domain == 0) { *inside_box = PETSC_FALSE; }
  
  /* If not inside csl domain set values to zero - not sure I even want to do this... */
  if (!(*inside_box)) {
    PetscInt k;
    
    for (k=0; k<m->ncomponents; k++) {
      cbar_p[k] = 0.0;
    }
  }
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "__TPMCInterpolateBulkConcentrationAtPoint"
PetscErrorCode __TPMCInterpolateBulkConcentrationAtPoint(TPMC m,const PetscReal coor[],PetscReal cbar_p[],PetscBool *inside_poly,PetscBool *inside_box)
{
  PetscInt k,inside_domain,inside_csl_domain;
  /*PetscReal gmin[] = {1.0e-20,1.0e-20,1.0e-20,1.0e-20,1.0e-20};*/
  /*PetscReal gmax[] = {0.6,0.6,0.6,0.6,0.6};*/
  
  /* this method does not set values if the coor[] is ouside the poly or csl domain */
  CSLPolyInterpolateAtPointWithArray(m->csl[0],m->csl[0]->interp_type,
                                          coor,
                                          m->ncomponents,(const PetscReal*)m->cbar_c,
                                          NULL,NULL,
                                          cbar_p,&inside_domain,&inside_csl_domain);
  if (inside_domain == 1) {
    for (k=0; k<m->ncomponents; k++) {
      if (cbar_p[k] < 0.0) {
        PetscInt ci,cj,cellidx;
        
        printf("  [TPMCInterp]: coor (%+1.6e,%+1.6e) cbar[%d] = %+1.6e < 0.0\n",coor[0],coor[1],k,cbar_p[k]);
   
        ci = (PetscInt)( (coor[0] - m->csl[0]->gmin[0])/m->csl[0]->dx[0] );
        cj = (PetscInt)( (coor[1] - m->csl[0]->gmin[1])/m->csl[0]->dx[1] );
        if (ci == m->csl[0]->mx[0]) ci--;
        if (cj == m->csl[0]->mx[1]) cj--;
        cellidx = ci + cj * m->csl[0]->mx[0];
        cbar_p[k] = m->cbar_c[m->ncomponents*cellidx + k];
        printf("  [TPMCInterp]: falling back to using cell data %+1.6e\n",cbar_p[k]);
        if (cbar_p[k] < 0.0) {
          printf("  [TPMCInterp]: coor (%+1.6e,%+1.6e) cbar[%d] = %+1.6e < 0.0\n",coor[0],coor[1],k,cbar_p[k]);
          printf("  [TPMCInterp]: cell %d has a cell constant < 0.0 ... aborting\n",cellidx);
          exit(1);
        }
      }
    }
    
  }
  *inside_poly = PETSC_TRUE;
  if (inside_domain == 0) { *inside_poly = PETSC_FALSE; }
  
  *inside_box = PETSC_TRUE;
  if (inside_csl_domain == 0) { *inside_box = PETSC_FALSE; }
  
  /* If not inside csl domain set values to zero - not sure I even want to do this... */
  if (!(*inside_box)) {
    PetscInt k;
    
    for (k=0; k<m->ncomponents; k++) {
      cbar_p[k] = 0.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetThermoDynamicMethod"
PetscErrorCode TPMCSetThermoDynamicMethod(TPMC m,
                                          PetscErrorCode (*f)(const PetscReal*,const PetscReal*,const PetscInt,const PetscReal*,PetscReal*,PetscReal*,PetscReal*,PetscBool*,void*),
                                          void *thermodyn_ctx)
{
  m->thermodyn_calc = f;
  m->thermodyn_ctx  = thermodyn_ctx;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCViewAllVTS"
PetscErrorCode TPMCViewAllVTS(TPMC m,const char fname[])
{
  PetscInt nx,ny,mx,my,i,j,f;
  FILE*	vtk_fp = NULL;
  char fieldname[PETSC_MAX_PATH_LEN];
  CSLPoly csl,csl_l,csl_s;
  
  vtk_fp = fopen(fname,"w");
  if (!vtk_fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file \"%s\"",fname);
  csl   = m->csl[PHASE_IDX_LIQUID]; /* for non-phase specific csl quantities */
  csl_l = m->csl[PHASE_IDX_LIQUID];
  csl_s = m->csl[PHASE_IDX_SOLID];
  
  nx = csl->nx[0];
  ny = csl->nx[1];
  mx = nx - 1;
  my = ny - 1;
  
  fprintf(vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
  fprintf(vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
  
  fprintf(vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", 0,nx-1, 0,ny-1, 0,0);
  fprintf(vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", 0,nx-1, 0,ny-1, 0,0);
  
  /* VTS COORD DATA */
  fprintf( vtk_fp, "    <Points>\n");
  fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp, "      ");
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", csl->coor_v[2*(i+j*nx)], csl->coor_v[2*(i+j*nx)+1], 0.0 );
    }
  }
  fprintf(vtk_fp, "\n      </DataArray>\n");
  fprintf(vtk_fp, "    </Points>\n");
  
  
  /* VTS NODAL DATA */
  fprintf(vtk_fp, "    <PointData>\n");
  
  /* velocity */
  if (m->view_vel) {
    fprintf(vtk_fp, "      <DataArray Name=\"_u_s\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", csl_s->vel_v[2*(i+j*nx)], csl_s->vel_v[2*(i+j*nx)+1], 0.0 );
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");

    fprintf(vtk_fp, "      <DataArray Name=\"_u_l\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", csl_l->vel_v[2*(i+j*nx)], csl_l->vel_v[2*(i+j*nx)+1], 0.0 );
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }
  
  if (m->view_interior_v) {
    fprintf(vtk_fp, "      <DataArray Name=\"_vert_is_interior\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf(vtk_fp,"%d ", (int)csl->is_interior_v[i+j*nx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }

  if (m->view_T) {
    fprintf(vtk_fp, "      <DataArray Name=\"T\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf(vtk_fp,"%1.6e ", m->temperature_v[i+j*nx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");

    fprintf(vtk_fp, "      <DataArray Name=\"T_prev\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        fprintf(vtk_fp,"%1.6e ", m->temperature_prev_v[i+j*nx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }

  fprintf(vtk_fp, "    </PointData>\n");
  
  /* VTS CELL DATA */
  fprintf(vtk_fp, "    <CellData>\n");
  
  if (m->view_bulk) {
    for (f=0; f<m->ncomponents; f++) {
      sprintf(fieldname,"c%s",m->component[f]->name);
      fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",fieldname);
      fprintf(vtk_fp,"      ");
      for (j=0; j<my; j++) {
        for (i=0; i<mx; i++) {
          fprintf(vtk_fp,"%1.6e ", m->cbar_c[m->ncomponents*(i+j*mx)+f]);
        }
      }
      fprintf(vtk_fp, "\n      </DataArray>\n");
    }
  }

  if (m->view_phase) {
    for (f=0; f<m->ncomponents; f++) {
      sprintf(fieldname,"c%s_s",m->component[f]->name);
      fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",fieldname);
      fprintf(vtk_fp,"      ");
      for (j=0; j<my; j++) {
        for (i=0; i<mx; i++) {
          fprintf(vtk_fp,"%1.6e ", m->cs_c[m->ncomponents*(i+j*mx)+f]);
        }
      }
      fprintf(vtk_fp, "\n      </DataArray>\n");
    }

    for (f=0; f<m->ncomponents; f++) {
      sprintf(fieldname,"c%s_l",m->component[f]->name);
      fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",fieldname);
      fprintf(vtk_fp,"      ");
      for (j=0; j<my; j++) {
        for (i=0; i<mx; i++) {
          fprintf(vtk_fp,"%1.6e ", m->cl_c[m->ncomponents*(i+j*mx)+f]);
        }
      }
      fprintf(vtk_fp, "\n      </DataArray>\n");
    }
  }
  
  if (m->view_vel) {
    fprintf(vtk_fp, "      <DataArray Name=\"_div[u_s]\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", csl_s->divu_c[i+j*mx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");

    fprintf(vtk_fp, "      <DataArray Name=\"_div[u_l]\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", csl_l->divu_c[i+j*mx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }
  
  if (m->view_gamma) {
    fprintf(vtk_fp, "      <DataArray Name=\"gamma\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", m->gamma_c[i+j*mx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }

  if (m->view_phi) {
    fprintf(vtk_fp, "      <DataArray Name=\"phi\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", m->phi_c[i+j*mx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");

#if HAVE_GAMMA_SUPPORT
    fprintf(vtk_fp, "      <DataArray Name=\"phi_k\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%1.6e ", m->csl[PHASE_IDX_SOLID]->ivalue_c[i+j*mx]);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
#endif
  }
  
  if (m->view_interior_c) {
    fprintf(vtk_fp, "      <DataArray Name=\"_cell_is_interior\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf(vtk_fp,"      ");
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        fprintf(vtk_fp,"%d ", (int)csl->cell_list[i+j*mx]->is_interior);
      }
    }
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }
  
  fprintf(vtk_fp, "    </CellData>\n");
  
  
  /* VTS HEADER - CLOSE */
  fprintf(vtk_fp, "    </Piece>\n");
  fprintf(vtk_fp, "  </StructuredGrid>\n");
  fprintf(vtk_fp, "</VTKFile>\n");
  
  fclose(vtk_fp);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCViewAllVTSBinary"
PetscErrorCode TPMCViewAllVTSBinary(TPMC m,const char fname[])
{
  PetscInt nx,ny,mx,my,i,j,f;
  FILE*	vtk_fp = NULL;
  char fieldname[PETSC_MAX_PATH_LEN];
  CSLPoly csl,csl_l,csl_s;
  long int bytes = 0;
  int bytes_item;
  float *buffer;
  PetscErrorCode ierr;
  
  vtk_fp = fopen(fname,"w");
  if (!vtk_fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file \"%s\"",fname);
  csl   = m->csl[PHASE_IDX_LIQUID]; /* for non-phase specific csl quantities */
  csl_l = m->csl[PHASE_IDX_LIQUID];
  csl_s = m->csl[PHASE_IDX_SOLID];
  
  nx = csl->nx[0];
  ny = csl->nx[1];
  mx = nx - 1;
  my = ny - 1;
  
  ierr = PetscMalloc1(nx*ny*3,&buffer);CHKERRQ(ierr);
  
  fprintf(vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
#ifdef WORDSIZE_BIGENDIAN
  fprintf(vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
  fprintf(vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
  
  fprintf(vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", 0,nx-1, 0,ny-1, 0,0);
  fprintf(vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", 0,nx-1, 0,ny-1, 0,0);
  
  /* VTS COORD DATA */
  fprintf( vtk_fp, "    <Points>\n");
  fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float32\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
  bytes += sizeof(int) + sizeof(float) * nx*ny*3;
  fprintf(vtk_fp, "    </Points>\n");
  
  
  /* VTS NODAL DATA */
  fprintf(vtk_fp, "    <PointData>\n");
  
  /* velocity */
  if (m->view_vel) {
    fprintf(vtk_fp, "      <DataArray Name=\"_u_s\" type=\"Float32\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * nx*ny*3;
    
    fprintf(vtk_fp, "      <DataArray Name=\"_u_l\" type=\"Float32\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * nx*ny*3;
  }
  
  if (m->view_interior_v) {
    fprintf(vtk_fp, "      <DataArray Name=\"_vert_is_interior\" type=\"Int32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(int) * nx*ny;
  }
  
  if (m->view_T) {
    fprintf(vtk_fp, "      <DataArray Name=\"T\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * nx*ny;
    
    fprintf(vtk_fp, "      <DataArray Name=\"T_prev\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * nx*ny;

  }
  fprintf(vtk_fp, "    </PointData>\n");
  
  /* VTS CELL DATA */
  fprintf(vtk_fp, "    <CellData>\n");
  
  if (m->view_bulk) {
    for (f=0; f<m->ncomponents; f++) {
      sprintf(fieldname,"c%s",m->component[f]->name);
      fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",fieldname,(int)bytes);
      bytes += sizeof(int) + sizeof(float) * mx*my;
    }
  }
  
  if (m->view_phase) {
    for (f=0; f<m->ncomponents; f++) {
      sprintf(fieldname,"c%s_s",m->component[f]->name);
      fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",fieldname,(int)bytes);
      bytes += sizeof(int) + sizeof(float) * mx*my;
    }
    
    for (f=0; f<m->ncomponents; f++) {
      sprintf(fieldname,"c%s_l",m->component[f]->name);
      fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",fieldname,(int)bytes);
      bytes += sizeof(int) + sizeof(float) * mx*my;
    }
  }
  
  if (m->view_vel) {
    fprintf(vtk_fp, "      <DataArray Name=\"_div[u_s]\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * mx*my;
    
    fprintf(vtk_fp, "      <DataArray Name=\"_div[u_l]\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * mx*my;
  }
  
  if (m->view_gamma) {
    fprintf(vtk_fp, "      <DataArray Name=\"gamma\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * mx*my;
  }
  
  if (m->view_phi) {
    fprintf(vtk_fp, "      <DataArray Name=\"phi\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * mx*my;
    
#if HAVE_GAMMA_SUPPORT
    fprintf(vtk_fp, "      <DataArray Name=\"phi_k\" type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(float) * mx*my;
#endif
  }
  
  if (m->view_interior_c) {
    fprintf(vtk_fp, "      <DataArray Name=\"_cell_is_interior\" type=\"Int32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",(int)bytes);
    bytes += sizeof(int) + sizeof(int) * mx*my;
  }
  
  fprintf(vtk_fp, "    </CellData>\n");
  
  
  /* VTS HEADER - CLOSE */
  fprintf(vtk_fp, "    </Piece>\n");
  fprintf(vtk_fp, "  </StructuredGrid>\n");
  fprintf(vtk_fp, "  <AppendedData encoding=\"raw\">\n");
 	fprintf(vtk_fp, "_");
 
  // appended data
  // coordinates
  bytes_item = 3 * nx * ny * 1 * sizeof(float);
  fwrite(&bytes_item,sizeof(int),1,vtk_fp);
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      buffer[3*(i+j*nx)+0] = (float)csl->coor_v[2*(i+j*nx)+0];
      buffer[3*(i+j*nx)+1] = (float)csl->coor_v[2*(i+j*nx)+1];
      buffer[3*(i+j*nx)+2] = 0.0;
    }
  }
  fwrite(buffer,sizeof(float),3*nx*ny,vtk_fp);
  
  // nodal data - velocity
  if (m->view_vel) {
    bytes_item = 3 * nx * ny * 1 * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        buffer[3*(i+j*nx)+0] = (float)csl_s->vel_v[2*(i+j*nx)+0];
        buffer[3*(i+j*nx)+1] = (float)csl_s->vel_v[2*(i+j*nx)+1];
        buffer[3*(i+j*nx)+2] = 0.0;
      }
    }
    fwrite(buffer,sizeof(float),3*nx*ny,vtk_fp);
    
    bytes_item = 3 * nx * ny * 1 * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        buffer[3*(i+j*nx)+0] = (float)csl_l->vel_v[2*(i+j*nx)+0];
        buffer[3*(i+j*nx)+1] = (float)csl_l->vel_v[2*(i+j*nx)+1];
        buffer[3*(i+j*nx)+2] = 0.0;
      }
    }
    fwrite(buffer,sizeof(float),3*nx*ny,vtk_fp);
  }
  
  // nodal data - interior
  if (m->view_interior_v) {
    bytes_item = nx * ny * sizeof(int);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    fwrite(csl->is_interior_v,sizeof(int),nx*ny,vtk_fp);
  }
  
  // nodal data - temperature
  if (m->view_T) {
    bytes_item = nx * ny * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        buffer[i+j*nx] = (float)m->temperature_v[i+j*nx];
      }
    }
    fwrite(buffer,sizeof(float),nx*ny,vtk_fp);
    
    bytes_item = nx * ny * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<ny; j++) {
      for (i=0; i<nx; i++) {
        buffer[i+j*nx] = (float)m->temperature_prev_v[i+j*nx];
      }
    }
    fwrite(buffer,sizeof(float),nx*ny,vtk_fp);
  }
  
  // cell data - bulk fields
  if (m->view_bulk) {
    for (f=0; f<m->ncomponents; f++) {
      
      bytes_item = mx * my * sizeof(float);
      fwrite(&bytes_item,sizeof(int),1,vtk_fp);
      for (j=0; j<my; j++) {
        for (i=0; i<mx; i++) {
          buffer[i+j*mx] = (float)m->cbar_c[m->ncomponents*(i+j*mx)+f];
        }
      }
      fwrite(buffer,sizeof(float),mx*my,vtk_fp);
    }
  }
  
  // cell data - phase fields
  if (m->view_phase) {
    for (f=0; f<m->ncomponents; f++) {
      
      bytes_item = mx * my * sizeof(float);
      fwrite(&bytes_item,sizeof(int),1,vtk_fp);
      for (j=0; j<my; j++) {
        for (i=0; i<mx; i++) {
          buffer[i+j*mx] = (float)m->cs_c[m->ncomponents*(i+j*mx)+f];
        }
      }
      fwrite(buffer,sizeof(float),mx*my,vtk_fp);
    }
    
    for (f=0; f<m->ncomponents; f++) {
      
      bytes_item = mx * my * sizeof(float);
      fwrite(&bytes_item,sizeof(int),1,vtk_fp);
      for (j=0; j<my; j++) {
        for (i=0; i<mx; i++) {
          buffer[i+j*mx] = (float)m->cl_c[m->ncomponents*(i+j*mx)+f];
        }
      }
      fwrite(buffer,sizeof(float),mx*my,vtk_fp);
    }
  }
  
  // cell data - div fields
  if (m->view_vel) {
    
    bytes_item = mx * my * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        buffer[i+j*mx] = (float)csl_s->divu_c[i+j*mx];
      }
    }
    fwrite(buffer,sizeof(float),mx*my,vtk_fp);

    bytes_item = mx * my * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        buffer[i+j*mx] = (float)csl_l->divu_c[i+j*mx];
      }
    }
    fwrite(buffer,sizeof(float),mx*my,vtk_fp);
  }

  // cell data - gamma/phi
  if (m->view_gamma) {
    
    bytes_item = mx * my * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        buffer[i+j*mx] = (float)m->gamma_c[i+j*mx];
      }
    }
    fwrite(buffer,sizeof(float),mx*my,vtk_fp);
  }
  
  if (m->view_phi) {

    bytes_item = mx * my * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        buffer[i+j*mx] = (float)m->phi_c[i+j*mx];
      }
    }
    fwrite(buffer,sizeof(float),mx*my,vtk_fp);
    
#if HAVE_GAMMA_SUPPORT
    bytes_item = mx * my * sizeof(float);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        buffer[i+j*mx] = (float)m->csl[PHASE_IDX_SOLID]->ivalue_c[i+j*mx];
      }
    }
    fwrite(buffer,sizeof(float),mx*my,vtk_fp);
#endif
  }
  
  // cell data - interior
  if (m->view_interior_c) {
    int is_interior;
    
    bytes_item = mx * my * sizeof(int);
    fwrite(&bytes_item,sizeof(int),1,vtk_fp);
    for (j=0; j<my; j++) {
      for (i=0; i<mx; i++) {
        is_interior = csl->cell_list[i+j*mx]->is_interior;
        fwrite(&is_interior,sizeof(int),1,vtk_fp);
      }
    }
  }
  
  fprintf(vtk_fp, "\n  </AppendedData>\n");
  fprintf(vtk_fp, "</VTKFile>\n");
  
  ierr = PetscFree(buffer);CHKERRQ(ierr);
  fclose(vtk_fp);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCFacetViewAllVTU"
PetscErrorCode TPMCFacetViewAllVTU(TPMC m,const char fname[])
{
  PetscInt i,k;
  FILE*	vtk_fp = NULL;
  CSLPoly sl,csl_l,csl_s;
  char fieldname[PETSC_MAX_PATH_LEN];
  
  vtk_fp = fopen(fname,"w");
  if (!vtk_fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file \"%s\"",fname);
  sl   = m->csl[PHASE_IDX_LIQUID]; /* for non-phase specific csl quantities */
  csl_l = m->csl[PHASE_IDX_LIQUID];
  csl_s = m->csl[PHASE_IDX_SOLID];

  fprintf(vtk_fp, "<?xml version=\"1.0\"?>\n");
  /* VTS HEADER - OPEN */
  fprintf(vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
  fprintf(vtk_fp, "  <UnstructuredGrid>\n");
  fprintf( vtk_fp,"  <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",sl->boundary_nfacets,sl->boundary_nfacets);
  
  fprintf(vtk_fp,"    <Cells>\n");
  fprintf(vtk_fp,"      <DataArray Name=\"connectivity\" type=\"Int32\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", i);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"      <DataArray Name=\"offsets\" type=\"Int32\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", i+1);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"      <DataArray Name=\"types\" type=\"UInt8\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", 1);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"    </Cells>\n");
  
  /* VTS COORD DATA */
  fprintf( vtk_fp,"    <Points>\n");
  fprintf( vtk_fp,"      <DataArray Name=\"Points\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf( vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->boundary_facet_list[i]->coor[0], sl->boundary_facet_list[i]->coor[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  fprintf(vtk_fp,"    </Points>\n");
  
  /* VTS NODAL DATA */
  fprintf(vtk_fp,"    <PointData>\n");
  
  fprintf(vtk_fp,"      <DataArray Name=\"u_l\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<csl_l->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", csl_l->boundary_facet_list[i]->velocity[0], csl_l->boundary_facet_list[i]->velocity[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"u_s\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<csl_s->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", csl_s->boundary_facet_list[i]->velocity[0], csl_s->boundary_facet_list[i]->velocity[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"normal\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%1.6e %1.6e %1.6e ", sl->boundary_facet_list[i]->normal[0], sl->boundary_facet_list[i]->normal[1], 0.0 );
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  
  fprintf(vtk_fp,"      <DataArray Name=\"is_inflow_l\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<csl_l->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", (int)csl_l->boundary_facet_list[i]->is_inflow);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"is_inflow_s\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<csl_s->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", (int)csl_s->boundary_facet_list[i]->is_inflow);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");

  fprintf(vtk_fp,"      <DataArray Name=\"label\" type=\"Int32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
  fprintf(vtk_fp,"      ");
  for (i=0; i<sl->boundary_nfacets; i++) {
    fprintf(vtk_fp,"%d ", (int)sl->boundary_facet_list[i]->label);
  }
  fprintf(vtk_fp,"\n      </DataArray>\n");
  
  for (k=0; k<m->ncomponents; k++) {
    if (!m->component[k]->solve) continue;
    
    sprintf(fieldname,"c%s",m->component[k]->name);
    fprintf(vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",fieldname);
    fprintf(vtk_fp,"      ");

    if (m->component[k]->phase_active[PHASE_IDX_SOLID] && m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
      PetscInt f_l,f_s;
      /* solid and liquid */
      f_s = m->active_set_s[k];
      f_l = m->active_set_l[k];
      for (i=0; i<sl->boundary_nfacets; i++) {
        fprintf(vtk_fp,"%1.6e ", csl_l->boundary_facet_list[i]->value[f_l] + csl_s->boundary_facet_list[i]->value[f_s]);
      }
    } else if (!m->component[k]->phase_active[PHASE_IDX_SOLID] && m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
      PetscInt f_l;
      /* liquid only */
      f_l = m->active_set_l[k];
      for (i=0; i<sl->boundary_nfacets; i++) {
        fprintf(vtk_fp,"%1.6e ", csl_l->boundary_facet_list[i]->value[f_l]);
      }
    } else if (m->component[k]->phase_active[PHASE_IDX_SOLID] && !m->component[k]->phase_active[PHASE_IDX_LIQUID]) {
      PetscInt f_s;
      /* solid only */
      f_s = m->active_set_s[k];
      for (i=0; i<sl->boundary_nfacets; i++) {
        fprintf(vtk_fp,"%1.6e ", csl_s->boundary_facet_list[i]->value[f_s]);
      }
    }
    
    fprintf(vtk_fp, "\n      </DataArray>\n");
  }

  fprintf(vtk_fp,"    </PointData>\n");
  
  fprintf(vtk_fp,"    <CellData>\n");
  fprintf(vtk_fp,"    </CellData>\n");
  
  /* VTS HEADER - CLOSE */
  fprintf(vtk_fp,"    </Piece>\n");
  fprintf(vtk_fp,"  </UnstructuredGrid>\n");
  fprintf(vtk_fp,"</VTKFile>\n");
  
  fclose(vtk_fp);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCEquilibrium"
PetscErrorCode TPMCEquilibrium(TPMC m,const PetscReal P,const PetscReal T,PetscReal cbulk[],
                               PetscReal cl[],PetscReal cs[],PetscReal *phi,PetscBool *valid)
{
  PetscInt k,nk = m->ncomponents;
  PetscReal sum[2];
  PetscInt eliminated = m->component_index_eliminated;
  PetscErrorCode ierr;
  
  /* apply unity sum on input bulk */
  cbulk[eliminated] = 0.0;
  sum[0] = 1.0;
  for (k=0; k<nk; k++) {
    sum[0] -= cbulk[k];
  }
  cbulk[eliminated] = sum[0];
  
  /* enfore equilibrium */
  ierr = m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, nk, (const PetscReal*)cbulk, cl, cs, phi, valid, m->thermodyn_ctx );CHKERRQ(ierr);

  /* apply unity sum on cl,cs output */
  cl[eliminated] = 0.0;
  cs[eliminated] = 0.0;
  sum[0] = sum[1] = 1.0;
  for (k=0; k<nk; k++) {
    sum[0] -= cl[k];
    sum[1] -= cs[k];
  }
  cl[eliminated] = sum[0];
  cs[eliminated] = sum[1];
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCEnforceUnitySumBulkComponentsAtPoint"
PetscErrorCode TPMCEnforceUnitySumBulkComponentsAtPoint(TPMC m,PetscReal cbulk[])
{
  PetscInt k,nk = m->ncomponents;
  PetscReal sum;
  PetscInt eliminated = m->component_index_eliminated;
  
  /* apply unity sum on input bulk */
  cbulk[eliminated] = 0.0;
  sum = 1.0;
  for (k=0; k<nk; k++) {
    sum -= cbulk[k];
  }
  cbulk[eliminated] = sum;
  
  PetscFunctionReturn(0);
}

/*
 Methods used to evaluate
   (1/|Omega(t_k-dt)|) \int phi dOmega(t_k-dt)
 These should use temperature from time t_k - dt (T_prev_c[])
*/
void porosity_ifield_evaluate_at_point(CSLPoly csl_liquid,PetscReal coor[],PetscReal ivalues[],void *data)
{
  TPMC m = (TPMC)data;
  PetscReal cs_weighted[20],cl_weighted[20],cbulk[20],T,P,cs[20],cl[20],phi;
  PetscInt f,index;
  PetscBool valid;
  
  CSLPolyInterpolateAtPoint(m->csl[PHASE_IDX_SOLID],coor,cs_weighted);
  CSLPolyInterpolateAtPoint(m->csl[PHASE_IDX_LIQUID],coor,cl_weighted);

  PetscMemzero(cbulk,sizeof(PetscReal)*m->ncomponents);
  for (f=0; f<m->nactive_s; f++) {
    index = m->map_set_s[f];
    cbulk[index] += cs_weighted[f];
  }
  for (f=0; f<m->nactive_l; f++) {
    index = m->map_set_l[f];
    cbulk[index] += cl_weighted[f];
  }
  TPMCEnforceUnitySumBulkComponentsAtPoint(m,cbulk);
  
  /* compute P, T at coor */
  TPMCInterpolatePTprevious(m,(PetscReal*)coor,&P,&T);
  
  /* enfore equilibrium */
  m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, m->ncomponents, cbulk, cl, cs, &phi, &valid, m->thermodyn_ctx );

  ivalues[0] = 1.0 - phi;
}

void porosity_ifield_evaluate_at_boundarypoint(CSLPoly csl_liquid,PetscInt facet_intersected,PetscReal coor[],PetscReal ivalues[],void *data)
{
  TPMC m = (TPMC)data;
  PetscReal cs_weighted[20],cl_weighted[20],cbulk[20],T,P,cs[20],cl[20],phi;
  PetscInt f,index;
  PetscBool valid;
  
  CSLPolyInterpolateAtBoundaryPoint(m->csl[PHASE_IDX_SOLID],facet_intersected,coor,cs_weighted);
  CSLPolyInterpolateAtBoundaryPoint(m->csl[PHASE_IDX_LIQUID],facet_intersected,coor,cl_weighted);
  
  PetscMemzero(cbulk,sizeof(PetscReal)*m->ncomponents);
  for (f=0; f<m->nactive_s; f++) {
    index = m->map_set_s[f];
    cbulk[index] += cs_weighted[f];
  }
  for (f=0; f<m->nactive_l; f++) {
    index = m->map_set_l[f];
    cbulk[index] += cl_weighted[f];
  }
  TPMCEnforceUnitySumBulkComponentsAtPoint(m,cbulk);
  
  /* compute P, T at coor */
  TPMCInterpolatePTprevious(m,(PetscReal*)coor,&P,&T);
  
  /* enfore equilibrium */
  m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, m->ncomponents, cbulk, cl, cs, &phi, &valid, m->thermodyn_ctx );
  
  ivalues[0] = 1.0 - phi;
}

#undef __FUNCT__
#define __FUNCT__ "TPMCUpdateTemperatureHistory"
PetscErrorCode TPMCUpdateTemperatureHistory(TPMC m)
{
  PetscErrorCode ierr;
  ierr = PetscMemcpy(m->temperature_prev_v,m->temperature_v,sizeof(PetscReal)*m->nvert);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetViewComponents"
PetscErrorCode TPMCSetViewComponents(TPMC m,PetscBool value_bulk,PetscBool value_phase)
{
  m->view_bulk = value_bulk;
  m->view_phase = value_phase;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetViewVelocities"
PetscErrorCode TPMCSetViewVelocities(TPMC m,PetscBool value)
{
  m->view_vel = value;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetViewInteriorPoints"
PetscErrorCode TPMCSetViewInteriorPoints(TPMC m,PetscBool value_c,PetscBool value_v)
{
  m->view_interior_c = value_c;
  m->view_interior_v = value_v;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetViewGammaPhi"
PetscErrorCode TPMCSetViewGammaPhi(TPMC m,PetscBool value_gamma,PetscBool value_phi)
{
  m->view_gamma = value_gamma;
  m->view_phi = value_phi;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSetViewTemperature"
PetscErrorCode TPMCSetViewTemperature(TPMC m,PetscBool value)
{
  m->view_T = value;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCCheckFieldForNAN"
PetscErrorCode TPMCCheckFieldForNAN(TPMC m,PetscBool iscelldata,const char stage[],PetscInt N,PetscInt nf,const PetscReal field[])
{
#ifdef MC_DEBUG_NAN_CHECKER
  PetscBool foundNAN = PETSC_FALSE;
  PetscInt k,f,indexI,indexF,nc;
  PetscReal value;
  PetscInt ii,jj;
  
  nc = m->ncomponents;
  for (k=0; k<N; k++) {
    PetscBool point_valid = PETSC_TRUE;
    
    for (f=0; f<nf; f++) {
      value = field[nf*k+f];
      
      if (fpclassify(value) == FP_NAN) {
        foundNAN = PETSC_TRUE;
        indexI = k;
        indexF = f;
        point_valid = PETSC_FALSE;
        
        printf("[error]: ====== TPMCCheckFieldForNAN Report ======\n");
        if (iscelldata) {
          jj = indexI / m->csl[0]->mx[0];
          ii = indexI - m->csl[0]->mx[0] * jj;
          printf("[error]: ======   CellField \"%s\" : index %d -> field %d : %+1.12e : Detected NAN\n",stage,indexI,indexF,field[nf*indexI+indexF]);
        } else {
          jj = indexI / m->csl[0]->nx[0];
          ii = indexI - m->csl[0]->nx[0] * jj;
          printf("[error]: ======   VertexField \"%s\" : index %d -> field %d : %+1.12e : Detected NAN\n",stage,indexI,indexF,field[nf*indexI+indexF]);
        }
      }
    }
    
    if (!point_valid) {
      if (iscelldata) {
        jj = indexI / m->csl[0]->mx[0];
        ii = indexI - m->csl[0]->mx[0] * jj;
        printf("[error]: ======   CellField \"%s\" : index %d :: Detected NAN\n",stage,indexI);
        printf("[error]: ======   Corresponds to cell [%d][%d] with coordinates (%+1.10e , %+1.10e)\n",ii,jj,m->csl[0]->cell_list[indexI]->coor[0],m->csl[0]->cell_list[indexI]->coor[1]);
        printf("[error]: ======   Cell->is_interior   : %d\n",m->csl[0]->cell_list[indexI]->is_interior);
        printf("[error]: ======   Cell->type          : %d\n",(int)m->csl[0]->cell_list[indexI]->type);
        printf("[error]: ======   Cell->tag           : %d\n",(int)m->csl[0]->cell_list[indexI]->tag);
        printf("[error]: ======   Cell->closest_facet : %d\n",m->csl[0]->cell_list[indexI]->closest_facet);
        
        printf("[state]: ======           cbulk       cl      cs\n");
        for (f=0; f<nc; f++) {
          printf("[state]: ======   [%d] %+1.4e    %+1.4e    %+1.4e\n",f,m->cbar_c[indexI*nc+f],m->cl_c[indexI*nc+f],m->cs_c[indexI*nc+f]);
        }
        printf("[state]: ======           phi   %+1.4e\n",m->phi_c[indexI]);
        
      } else {
        jj = indexI / m->csl[0]->nx[0];
        ii = indexI - m->csl[0]->nx[0] * jj;
        printf("[error]: ======   VertexField \"%s\" : index %d :: Detected NAN\n",stage,indexI);
        printf("[error]: ======   Corresponds to vertex [%d][%d] with coordinates (%+1.10e , %+1.10e)\n",ii,jj,m->csl[0]->coor_v[2*indexI],m->csl[0]->coor_v[2*indexI+1]);
        printf("[error]: ======   Vertex->is_interior : %d\n",m->csl[0]->is_interior_v[indexI]);
        printf("[error]: ======   Vertex->type        : %d\n",(int)m->csl[0]->type_v[indexI]);
      }
      
    }
    
  }
  if (foundNAN) {
//    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"NAN(s) detected within MultiComponent data field in stage \"%s\"",stage);
  }
#endif
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCSmoothLiquid"
PetscErrorCode TPMCSmoothLiquid(TPMC m,PetscReal kappa,PetscReal tf)
{
  CSLPoly csl;
  PetscInt step,Nt;
  PetscInt i,j,s_ij,stencil[4];
  PetscReal dx2,dtstar,L_ij[2],vx,vy,t1;
  PetscReal *vxnew;
  PetscReal smfk = 2.0;
  PetscReal int_vx,int_vy,scale_x,scale_y,dv,new_int_vx,new_int_vy;
  PetscErrorCode ierr;
  
  csl = m->csl[PHASE_IDX_LIQUID];
  
  // force diffusivity of 1.0
  kappa = 1.0;
  
  dx2 = csl->dx[0]*csl->dx[0];
  dtstar = 0.25 * dx2/ kappa;
  dtstar = 0.25 * dx2/ 1.0;
  
  /*
  Nt = (PetscInt)(tf/dtstar);
  printf("Nt %d smoothing steps being performed\n",Nt);
  if (Nt < 2) {
    Nt = 2;
    printf("Forcing Nt %d smoothing steps being performed\n",Nt);
  }
  */
  PetscOptionsGetReal(NULL,NULL,"-smfk",&smfk,NULL);
  // choose a length scale to diffuse over (2.dx)
  t1 = PetscPowReal(smfk*csl->dx[0],2.0)/1.0;
  // compute the number of time increments
  Nt = (PetscInt)(t1/dtstar) + 1;
  printf("Nt %d smoothing steps being performed\n",Nt);
  if (Nt <= 1) {
    Nt = 1;
    printf("Forcing Nt %d smoothing steps being performed\n",Nt);
  }
  dtstar = t1 / ((PetscReal)Nt);
  
  ierr = PetscMalloc1(csl->nx[0]*csl->nx[1]*2,&vxnew);CHKERRQ(ierr);
  ierr = PetscMemcpy(vxnew,csl->vel_v,sizeof(PetscReal)*csl->nx[0]*csl->nx[1]*2);CHKERRQ(ierr);
  
  dv = csl->dx[0] * csl->dx[1];
  int_vx = int_vy = 0.0;
  for (j=1; j<csl->nx[1]-1; j++) {
    for (i=1; i<csl->nx[0]-1; i++) {
      s_ij = i + j*csl->nx[0];
      if (csl->type_v[s_ij] != COOR_INTERIOR) continue;

      int_vx += csl->vel_v[2*s_ij+0] * dv;
      int_vy += csl->vel_v[2*s_ij+1] * dv;
    }
  }
  
  for (step=0; step<=Nt; step++) {
    double v2;
    double max = 0.0;
    //FILE *fp;
    //char fname[128];
    
    //sprintf(fname,"sm-%d.gp",step);
    //fp = fopen(fname,"w");
    for (j=1; j<csl->nx[1]-1; j++) {
      for (i=1; i<csl->nx[0]-1; i++) {
        s_ij = i + j*csl->nx[0];
        
        // only apply smoothing to interior vertices
        if (csl->type_v[s_ij] != COOR_INTERIOR) continue;
        
        stencil[0] = (i + 1) + (j + 0)*csl->nx[0];
        stencil[1] = (i - 1) + (j + 0)*csl->nx[0];
        stencil[2] = (i + 0) + (j - 1)*csl->nx[0];
        stencil[3] = (i + 0) + (j + 1)*csl->nx[0];

        vx = csl->vel_v[2*s_ij+0];
        vy = csl->vel_v[2*s_ij+1];
        
        L_ij[0] = kappa * ( -4.0 * csl->vel_v[2*s_ij+0] + csl->vel_v[2*stencil[0]+0] + csl->vel_v[2*stencil[1]+0] + csl->vel_v[2*stencil[2]+0] + csl->vel_v[2*stencil[3]+0]);
        L_ij[1] = kappa * ( -4.0 * csl->vel_v[2*s_ij+1] + csl->vel_v[2*stencil[0]+1] + csl->vel_v[2*stencil[1]+1] + csl->vel_v[2*stencil[2]+1] + csl->vel_v[2*stencil[3]+1]);

        vx = vx + dtstar * L_ij[0]/dx2;
        vy = vy + dtstar * L_ij[1]/dx2;
        
        v2 = sqrt(vx*vx + vy*vy);
        if (v2 > max) { max = v2; }
        vxnew[2*s_ij+0] = vx;
        vxnew[2*s_ij+1] = vy;
        
        //fprintf(fp,"%1.4e %1.4e    %1.4e %1.4e    %1.4e %1.4e\n",csl->coor_v[2*s_ij],csl->coor_v[2*s_ij+1],csl->vel_v[2*s_ij+0],csl->vel_v[2*s_ij+1], vxnew[2*s_ij+0],vxnew[2*s_ij+1]);
      }
    }
    //fclose(fp);
    printf("step %d : max %+1.4e\n",step,max);
    
    ierr = PetscMemcpy(csl->vel_v,vxnew,sizeof(PetscReal)*csl->nx[0]*csl->nx[1]*2);CHKERRQ(ierr);
  }
  
  /* rescale */
  new_int_vx = new_int_vy = 0.0;
  for (j=1; j<csl->nx[1]-1; j++) {
    for (i=1; i<csl->nx[0]-1; i++) {
      s_ij = i + j*csl->nx[0];
      if (csl->type_v[s_ij] != COOR_INTERIOR) continue;
      
      new_int_vx += csl->vel_v[2*s_ij+0] * dv;
      new_int_vy += csl->vel_v[2*s_ij+1] * dv;
    }
  }
  scale_x = int_vx / new_int_vx;
  scale_y = int_vy / new_int_vy;

  new_int_vx = new_int_vy = 0.0;
  for (j=1; j<csl->nx[1]-1; j++) {
    for (i=1; i<csl->nx[0]-1; i++) {
      s_ij = i + j*csl->nx[0];
      if (csl->type_v[s_ij] != COOR_INTERIOR) continue;
      
      csl->vel_v[2*s_ij+0] = csl->vel_v[2*s_ij+0] * scale_x;
      csl->vel_v[2*s_ij+1] = csl->vel_v[2*s_ij+1] * scale_y;
      new_int_vx += csl->vel_v[2*s_ij+0] * dv;
      new_int_vy += csl->vel_v[2*s_ij+1] * dv;
    }
  }
  printf("int_vx %+1.4e \n",int_vx);
  printf("int_vy %+1.4e \n",int_vy);
  printf("int_vx[rescaled] %+1.4e \n",new_int_vx);
  printf("int_vy[rescaled] %+1.4e \n",new_int_vy);
  
  {
    double v2;
    double max = 0.0;
    for (j=1; j<csl->nx[1]-1; j++) {
      for (i=1; i<csl->nx[0]-1; i++) {
        s_ij = i + j*csl->nx[0];
        
        // only apply smoothing to interior vertices
        if (csl->type_v[s_ij] != COOR_INTERIOR) continue;
        
        vx = csl->vel_v[2*s_ij+0];
        vy = csl->vel_v[2*s_ij+1];
        v2 = sqrt(vx*vx + vy*vy);
        if (v2 > max) { max = v2; }
      }
    }
    printf("<final> : max %+1.4e <rescaled>\n",max);
  }
  
  ierr = PetscFree(vxnew);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode TPMCInterpolateField(TPMC m,PetscInt ndof,PetscReal field_v[],PetscReal coor[],PetscReal field_p[])
{
  PetscInt nidx[4];
  PetscInt k,ci,cj,d;
  PetscReal xi[2],x0[2],Ni[4];
  CSLPoly p;
  
  p = m->csl[0];
  
  ci = (PetscInt)( (coor[0] - p->gmin[0])/p->dx[0] );
  cj = (PetscInt)( (coor[1] - p->gmin[1])/p->dx[1] );
  if (ci == p->mx[0]) ci--;
  if (cj == p->mx[1]) cj--;
  
  /* determine 4 nodes values to use for Q1 interpolation */
  nidx[0] = CSLNodeIdx(p,ci  ,cj);
  nidx[1] = CSLNodeIdx(p,ci+1,cj);
  nidx[2] = CSLNodeIdx(p,ci  ,cj+1);
  nidx[3] = CSLNodeIdx(p,ci+1,cj+1);
  
  x0[0] = p->gmin[0] + ci * p->dx[0];
  x0[1] = p->gmin[1] + cj * p->dx[1];
  
  xi[0] = 2.0*(coor[0]-x0[0])/p->dx[0] - 1.0;
  xi[1] = 2.0*(coor[1]-x0[1])/p->dx[1] - 1.0;
  
  Ni[0] = 0.25 * (1.0 - xi[0]) * (1.0 - xi[1]);
  Ni[1] = 0.25 * (1.0 + xi[0]) * (1.0 - xi[1]);
  Ni[2] = 0.25 * (1.0 - xi[0]) * (1.0 + xi[1]);
  Ni[3] = 0.25 * (1.0 + xi[0]) * (1.0 + xi[1]);
  
  for (d=0; d<ndof; d++) {
    field_p[d] = 0.0;
  }
  for (d=0; d<ndof; d++) {
    for (k=0; k<4; k++) {
      field_p[d] += Ni[k] * field_v[ ndof * nidx[k] + d ];
    }
  }
  PetscFunctionReturn(0);
}

/*
 This only measures differences associated with temperature and not vs,vl
*/
#undef __FUNCT__
#define __FUNCT__ "MCComputeResidual"
PetscErrorCode MCComputeResidual(TPMC m,PetscReal temperature_v[],PetscInt *_nk,PetscInt *_ncells,PetscReal *_norms[])
{
  PetscReal *norms;
  PetscInt nk,k,c,ncells;
  PetscReal dv;
  PetscReal cbulk[20],T,P,cs[20],cl[20],phi;
  CSLPoly csl;
  PetscBool valid;
  PetscErrorCode ierr;
  
  csl = m->csl[0];
  dv = csl->dx[0] * csl->dx[1];
  nk = m->ncomponents;
  ncells = m->ncells;
  ierr = PetscMalloc1(nk,&norms);CHKERRQ(ierr);
  for (k=0; k<nk; k++) {
    norms[k] = 0.0;
  }
  
  for (c=0; c<ncells; c++) {
    CellP cell;
    PetscReal *coor;
    
    cell = csl->cell_list[c];
    
    if (cell->type != COOR_INTERIOR) {
      continue;
    }
    
    /* get coordinate, P, T */
    coor = csl->cell_list[c]->coor;
    
    /* compute P, T at coor */
    //TPMCInterpolatePT(m,(PetscReal*)coor,&P,&T);
    ierr = TPMCInterpolateField(m,1,m->pressure_v,(PetscReal*)coor,&P);CHKERRQ(ierr);
    ierr = TPMCInterpolateField(m,1,temperature_v,(PetscReal*)coor,&T);CHKERRQ(ierr);
    
    for (k=0; k<nk; k++) {
      cbulk[k] = m->cbar_c[nk*c + k];
      cl[k] = 0.0;
      cs[k] = 0.0;
    }
    phi = 0.0;
    
    /* enfore equilibrium */
    ierr = m->thermodyn_calc( (const PetscReal*)&P, (const PetscReal*)&T, m->ncomponents, cbulk, cl, cs, &phi, &valid, m->thermodyn_ctx );CHKERRQ(ierr);
    //if (!valid) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"[TPMC] Thermodynamic evaluation failed");
    // ignore invalid values - let the nan propogate into the norms[] array
    
    for (k=0; k<nk; k++) {
      PetscReal cb_k;
      
      cb_k = (1.0 - phi) * cs[k] + phi * cl[k];
      norms[k] += (cb_k - cbulk[k])*(cb_k - cbulk[k]) * dv;
    }
  }
  for (k=0; k<nk; k++) {
    norms[k] = PetscSqrtReal(norms[k]);
  }
  
  *_ncells = m->csl[0]->ncells_interior;
  *_nk = m->ncomponents;
  *_norms = norms;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCEnforceFieldPositivity"
PetscErrorCode TPMCEnforceFieldPositivity(TPMC m,PetscInt N,PetscReal field[],PetscBool *enforcement_occurred)
{
  PetscInt i;
  const PetscReal lower_bound = 0.0;
  const PetscReal prescribed_min = 1.0e-20;
  
  *enforcement_occurred = PETSC_FALSE;
  for (i=0; i<N; i++) {
    if (field[i] < lower_bound) {
      field[i] = prescribed_min;
      *enforcement_occurred = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPMCComputeCFLTimestep"
PetscErrorCode TPMCComputeCFLTimestep(TPMC m,PetscReal *_dt_l,PetscReal *_dt_s)
{
  PetscInt v;
  PetscReal vx[2],vs_mag,vl_mag;
  PetscReal dx,dy,dt_min[2],ds;
  CSLPoly csl_l,csl_s;
  
  csl_l = m->csl[PHASE_IDX_LIQUID];
  csl_s = m->csl[PHASE_IDX_SOLID];
  dx = m->csl[0]->dx[0];
  dy = m->csl[0]->dx[1];
  ds = dx;
  if (dy < ds) { ds = dy; }
  
  dt_min[0] = dt_min[1] = 1.0e32;
  
  for (v=0; v<m->nvert; v++) {
    PetscReal dt_trial[2];
    if (m->csl[0]->type_v[v] != COOR_INTERIOR) continue;
    
    vx[0] = csl_l->vel_v[2*v+0];
    vx[1] = csl_l->vel_v[2*v+1];
    vl_mag = PetscSqrtReal(vx[0]*vx[0] + vx[1]*vx[1]);
    
    vx[0] = csl_s->vel_v[2*v+0];
    vx[1] = csl_s->vel_v[2*v+1];
    vs_mag = PetscSqrtReal(vx[0]*vx[0] + vx[1]*vx[1]);
    
    dt_trial[PHASE_IDX_LIQUID] = ds / (vl_mag + 1.0e-32);
    dt_trial[PHASE_IDX_SOLID]  = ds / (vs_mag + 1.0e-32);

    dt_min[PHASE_IDX_LIQUID] = PetscMin(dt_min[PHASE_IDX_LIQUID],dt_trial[PHASE_IDX_LIQUID]);
    dt_min[PHASE_IDX_SOLID] = PetscMin(dt_min[PHASE_IDX_SOLID],dt_trial[PHASE_IDX_SOLID]);
  }

  *_dt_s = dt_min[PHASE_IDX_SOLID];
  *_dt_l = dt_min[PHASE_IDX_LIQUID];
  
  PetscFunctionReturn(0);
}

