
#if !defined(__PDE_DARCY_KERNELS_H)
#define __PDE_DARCY_KERNELS_H

/* dot( \vec w , a \vec u) */
PetscErrorCode LFormMF__inner_w_a_u(
                                    const PetscInt nubasis,
                                    const PetscReal factor,const PetscReal a_qp,
                                    const PetscReal Ux[],const PetscReal Uy[],
                                    const PetscReal Nu[],
                                    PetscReal Y[]);

/* div( \vec w ) div ( \vec u) */
PetscErrorCode LFormMF__div_w_div_u(
                                    const PetscInt nubasis,
                                    const PetscReal factor,
                                    const PetscReal Ux[],const PetscReal Uy[],
                                    const PetscReal dNudx[],const PetscReal dNudy[],
                                    PetscReal Y[]);

/* div( \vec w ) f */
PetscErrorCode LFormMF__div_w_f(
                                const PetscInt nubasis,
                                const PetscReal factor,const PetscReal f_qp,
                                const PetscReal dNudx[],const PetscReal dNudy[],
                                PetscReal Y[]);


/* ========= Hdiv-L2 ========= */
/* div( \vec w ) p */
PetscErrorCode LFormMF__div_w_p(
                                const PetscInt nubasis,const PetscInt npbasis,
                                const PetscReal alpha,
                                const PetscReal P[],
                                const PetscReal dNudx[],const PetscReal dNudy[],
                                const PetscReal Np[],PetscReal Y[]);

/* q div( \vec u ) */
PetscErrorCode LFormMF__q_div_u(
                                const PetscInt nubasis,const PetscInt npbasis,
                                const PetscReal factor,
                                const PetscReal Ux[],const PetscReal Uy[],
                                const PetscReal dNudx[],const PetscReal dNudy[],const PetscReal Np[],
                                PetscReal Y[]);

/* ========= L2-H1 ========= */
/* dot( \vec w , grad(p) ) */
PetscErrorCode LFormMF__inner_w_grad_p(
                                       const PetscInt nubasis,const PetscInt npbasis,
                                       const PetscReal alpha,
                                       const PetscReal P[],
                                       const PetscReal Nw[],
                                       const PetscReal dNpdx[],const PetscReal dNpdy[],PetscReal Y[]);

/* dot( grad(q) , \vec u ) */
PetscErrorCode LFormMF__inner_grad_q_u(
                                       const PetscInt nubasis,const PetscInt npbasis,
                                       const PetscReal alpha,
                                       const PetscReal Ux[],const PetscReal Uy[],
                                       const PetscReal Nu[],
                                       const PetscReal dNpdx[],const PetscReal dNpdy[],PetscReal Y[]);

/* q p */
PetscErrorCode LFormMF__qp(
                           const PetscInt npbasis,
                           const PetscReal factor,
                           const PetscReal P[],
                           const PetscReal Np[],
                           PetscReal Y[]);

#endif
