
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <pdedarcyimpl.h>
#include <pdedarcy.h>
#include <coefficient.h>
#include <quadratureimpl.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <element_container.h>
#include <fe_geometry_utils.h>
#include <petscutils.h>
#include "../src/pde/darcy/kernels.h"

//#define LOG_MF_OP

#define PENALTY 0.0

#undef __FUNCT__
#define __FUNCT__ "FormBilinearA11_PDEDarcy"
PetscErrorCode FormBilinearA11_PDEDarcy(PDEDarcy *darcy,Mat A11,Mat B11)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj,kk;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *coords,*elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal *detJ,**Nu,**dNuxi,**dNueta,**dNux,**dNuy,*weight,*coeff_a,*coeff_a_e,diagD[3];
  IS ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM dm = NULL;
  EContainer c;
  Quadrature quadrature;
  PetscReal **B;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = darcy->dmu;
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&nbasis,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  
  bc = darcy->u_dirichlet;
  
  ierr = CoefficientGetQuadrature(darcy->a,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"a",NULL,NULL,NULL,&coeff_a);CHKERRQ(ierr);
  
  c = darcy->ec_u;
  Nu     = c->N;
  dNuxi  = c->dNr1;
  dNueta = c->dNr2;
  dNux   = c->dNx1;
  dNuy   = c->dNx2;
  
  detJ     = c->buf_q_scalar_a;
  eldofs   = c->buf_basis_2index_a;
  elcoords = c->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(2*2*nbasis*nbasis,&Ke);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(3,&B);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[0]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[1]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[2]);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B11),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B11);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    /* generate masked dof indices */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = elnidx[i];
      
      eldofs[2*i+0] = GINDICES[2*nidx];
      eldofs[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = elnidx[i];
      
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nbasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis*2*2);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_a,&coeff_a_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Nu_q;
      
      /* get access to element->quadrature points */
      Nu_q   = Nu[q];

      fac = weight[q] * detJ[q];
      
      for (i=0; i<nbasis; i++) {
        PetscReal Nu_i = Nu_q[i];
        
        B[0][2*i  ] = Nu_i; B[0][2*i+1] = 0.0;
        B[1][2*i  ] = 0.0;  B[1][2*i+1] = Nu_i;
        B[2][2*i  ] = 0.0;  B[2][2*i+1] = 0.0;
      }
      
      diagD[0] = fac * coeff_a_e[q];
      diagD[1] = fac * coeff_a_e[q];
      diagD[2] = fac * coeff_a_e[q];
      
      /* form Bt tildeD B */
      /*
       Ke_ij = Bt_ik . D_kl . B_lj
       = B_ki . D_kl . B_lj
       = B_ki . D_kk . B_kj
      */
      for (ii=0; ii<2*nbasis; ii++) {
        for (jj=ii; jj<2*nbasis; jj++) {
          for (kk=0; kk<3; kk++) {
            Ke[ii*(2*nbasis)+jj] += B[kk][ii]*diagD[kk]*B[kk][jj];
          }
        }
      }
      
      /* grad-div stab */
      /*
      for (ii=0; ii<nbasis; ii++) {
        for (jj=ii; jj<nbasis; jj++) {
          PetscReal bform[2][2];
          PetscReal penalty = PENALTY;
       
          bform[0][0] = penalty * dNux_q[ii] * dNux_q[jj];
          bform[0][1] = penalty * dNux_q[ii] * dNuy_q[jj];
          bform[1][0] = penalty * dNuy_q[ii] * dNux_q[jj];
          bform[1][1] = penalty * dNuy_q[ii] * dNuy_q[jj];
       
          Ke[(2*ii+0)*(2*nbasis)+(2*jj+0)] += fac * bform[0][0];
          Ke[(2*ii+0)*(2*nbasis)+(2*jj+1)] += fac * bform[0][1];
          Ke[(2*ii+1)*(2*nbasis)+(2*jj+0)] += fac * bform[1][0];
          Ke[(2*ii+1)*(2*nbasis)+(2*jj+1)] += fac * bform[1][1];
        }
      }
      */
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < 2*nbasis; ii++) {
      for (jj = ii; jj < 2*nbasis; jj++) {
        Ke[jj*(2*nbasis)+ii] = Ke[ii*(2*nbasis)+jj];
      }
    }
    
    ierr = MatSetValuesLocal(B11,2*nbasis,eldofs,2*nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
    ierr = BCListInsertScaling(B11,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A11 != B11) {
    ierr = MatAssemblyBegin(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobian(Auu): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(B[2]);CHKERRQ(ierr);
  ierr = PetscFree(B[1]);CHKERRQ(ierr);
  ierr = PetscFree(B[0]);CHKERRQ(ierr);
  ierr = PetscFree(B);CHKERRQ(ierr);
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormBilinearA11_PDEDarcyL2"
PetscErrorCode FormBilinearA11_PDEDarcyL2(PDEDarcy *darcy,Mat A11,Mat B11)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,k,ii,jj,kk;
  PetscInt  nbasis_u,nbasis_g;
  PetscReal   *coords_g,*elcoords_g;
  PetscInt    *element_u,*element_g,*eldofs;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal **Nu,**GNIxi,**GNIeta,**dNudx,**dNudy,*weight,*coeff_a,*coeff_a_e,diagD[3],detJ_affine;
  BCList bc = NULL;
  DM dm = NULL;
  EContainer cu;
  Quadrature quadrature;
  PetscReal **B;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = darcy->dmu;
  
  ierr = DMTetSpaceElement(dm,&nel,&nbasis_u,&element_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,&nbasis_g,&element_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  bc = darcy->u_dirichlet;
  if (bc) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"L2H1 bilinear form does not support Dirichlet velocity boundary conditions");
  
  ierr = CoefficientGetQuadrature(darcy->a,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"a",NULL,NULL,NULL,&coeff_a);CHKERRQ(ierr);
  
  cu = darcy->ec_u;
  Nu     = cu->N;
  GNIxi  = cu->dNr1;
  GNIeta = cu->dNr2;
  dNudx  = cu->dNx1;
  dNudy  = cu->dNx2;

  eldofs   = cu->buf_basis_2index_a;
  
  ierr = PetscMalloc1(2*nbasis_g,&elcoords_g);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*2*nbasis_u*nbasis_u,&Ke);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(3,&B);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis_u,&B[0]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis_u,&B[1]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis_u,&B[2]);CHKERRQ(ierr);
  
  ierr = MatZeroEntries(B11);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *u_el_lidx;
    
    /* get velocity dof indices */
    u_el_lidx = &element_u[nbasis_u*e];
    for (k=0; k<nbasis_u; k++) {
      PetscInt nidx = u_el_lidx[k];
      
      eldofs[2*k + 0] = 2 * nidx + 0;
      eldofs[2*k + 1] = 2 * nidx + 1;
    }

    /* get coordinates */
    for (k=0; k<nbasis_g; k++) {
      PetscInt nidx = element_g[nbasis_g*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }

    /* compute derivatives */
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_u,elcoords_g,GNIxi,GNIeta,dNudx,dNudy,&detJ_affine);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis_u*nbasis_u*2*2);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_a,&coeff_a_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Nu_q;
      
      /* get access to element->quadrature points */
      Nu_q   = Nu[q];
      
      fac = weight[q] * detJ_affine;
      
      for (i=0; i<nbasis_u; i++) {
        PetscReal Nu_i = Nu_q[i];
        
        B[0][2*i  ] = Nu_i; B[0][2*i+1] = 0.0;
        B[1][2*i  ] = 0.0;  B[1][2*i+1] = Nu_i;
        B[2][2*i  ] = 0.0;  B[2][2*i+1] = 0.0;
      }
      
      diagD[0] = fac * coeff_a_e[q];
      diagD[1] = fac * coeff_a_e[q];
      diagD[2] = fac * coeff_a_e[q];
      
      /* form Bt tildeD B */
      for (ii=0; ii<2*nbasis_u; ii++) {
        for (jj=ii; jj<2*nbasis_u; jj++) {
          for (kk=0; kk<3; kk++) {
            Ke[ii*(2*nbasis_u)+jj] += B[kk][ii]*diagD[kk]*B[kk][jj];
          }
        }
      }
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < 2*nbasis_u; ii++) {
      for (jj = ii; jj < 2*nbasis_u; jj++) {
        Ke[jj*(2*nbasis_u)+ii] = Ke[ii*(2*nbasis_u)+jj];
      }
    }
    
    ierr = MatSetValuesLocal(B11,2*nbasis_u,eldofs,2*nbasis_u,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A11 != B11) {
    ierr = MatAssemblyBegin(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobian(Auu): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(B[2]);CHKERRQ(ierr);
  ierr = PetscFree(B[1]);CHKERRQ(ierr);
  ierr = PetscFree(B[0]);CHKERRQ(ierr);
  ierr = PetscFree(B);CHKERRQ(ierr);
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = PetscFree(elcoords_g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* - \int div(w) p dV */
#undef __FUNCT__
#define __FUNCT__ "FormBilinearA12_PDEDarcyHdiv"
PetscErrorCode FormBilinearA12_PDEDarcyHdiv(PDEDarcy *darcy,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nubasis,npbasis;
  PetscInt  *element_u,*element_p;
  PetscReal *coords,*elcoords_u,*weight;
  PetscInt  *eldofs_u;
  PetscReal *Ae;
  PetscInt  q,nqp;
  PetscReal *detJ,**dNuxi,**dNueta,**dNux,**dNuy,**Np;
  IS        ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM     dmu = NULL;
  DM     dmp = NULL;
  EContainer cu,cp;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dmu = darcy->dmu;
  dmp = darcy->dmp;
  ierr = DMTetSpaceElement(dmu,&nel,&nubasis,&element_u,NULL,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&npbasis,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(darcy->ref,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  bc = darcy->u_dirichlet;
  
  cu = darcy->ec_u;
  dNuxi  = cu->dNr1;
  dNueta = cu->dNr2;
  dNux   = cu->dNx1;
  dNuy   = cu->dNx2;

  cp = darcy->ec_p;
  Np = cp->N;
  
  detJ       = cu->buf_q_scalar_a;
  eldofs_u   = cu->buf_basis_2index_a;
  elcoords_u = cu->buf_basis_2vector_a;

  ierr = PetscMalloc1(2*nubasis*npbasis,&Ae);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *elnidx_u,*elnidx_p;
    
    /* get element -> node map */
    elnidx_u = &element_u[nubasis*e];
    elnidx_p = &element_p[npbasis*e];
    
    /* generate masked dof indices */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      eldofs_u[2*i+0] = GINDICES[2*nidx];
      eldofs_u[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      elcoords_u[2*i  ] = coords[2*nidx  ];
      elcoords_u[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords_u,dNuxi,dNueta,dNux,dNuy,detJ);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nubasis*npbasis*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Np_q;
      PetscReal *dNux_q,*dNuy_q;
      
      /* get access to element->quadrature points */
      Np_q   = Np[q];
      dNux_q = dNux[q];
      dNuy_q = dNuy[q];
      
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<nubasis; ii++) {
        for (jj=0; jj<npbasis; jj++) {
          
          Ae[npbasis*(2*ii+0) + jj] -= dNux_q[ii] * Np_q[jj] * fac;
          Ae[npbasis*(2*ii+1) + jj] -= dNuy_q[ii] * Np_q[jj] * fac;
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(B,2*nubasis,eldofs_u,npbasis,elnidx_p,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobianHdiv(Aup): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* - \int q div(u) dV */
#undef __FUNCT__
#define __FUNCT__ "FormBilinearA21_PDEDarcyHdiv"
PetscErrorCode FormBilinearA21_PDEDarcyHdiv(PDEDarcy *darcy,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nubasis,npbasis;
  PetscInt  *element_u,*element_p;
  PetscReal *coords,*elcoords_u,*weight;
  PetscInt  *eldofs_u;
  PetscReal *Ae;
  PetscInt  q,nqp;
  PetscReal *detJ,**dNuxi,**dNueta,**dNux,**dNuy,**Np;
  IS        ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM     dmu = NULL;
  DM     dmp = NULL;
  EContainer cu,cp;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dmu = darcy->dmu;
  dmp = darcy->dmp;
  ierr = DMTetSpaceElement(dmu,&nel,&nubasis,&element_u,NULL,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&npbasis,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(darcy->ref,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  bc = darcy->u_dirichlet;
  
  cu = darcy->ec_u;
  dNuxi  = cu->dNr1;
  dNueta = cu->dNr2;
  dNux   = cu->dNx1;
  dNuy   = cu->dNx2;
  
  cp = darcy->ec_p;
  Np = cp->N;
  
  detJ       = cu->buf_q_scalar_a;
  eldofs_u   = cu->buf_basis_2index_a;
  elcoords_u = cu->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(2*nubasis*npbasis,&Ae);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *elnidx_u,*elnidx_p;
    
    /* get element -> node map */
    elnidx_u = &element_u[nubasis*e];
    elnidx_p = &element_p[npbasis*e];
    
    /* generate masked dof indices */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      eldofs_u[2*i+0] = GINDICES[2*nidx];
      eldofs_u[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      elcoords_u[2*i  ] = coords[2*nidx  ];
      elcoords_u[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords_u,dNuxi,dNueta,dNux,dNuy,detJ);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nubasis*npbasis*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Np_q;
      PetscReal *dNux_q,*dNuy_q;
      
      /* get access to element->quadrature points */
      Np_q   = Np[q];
      dNux_q = dNux[q];
      dNuy_q = dNuy[q];
      
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<npbasis; ii++) {
        for (jj=0; jj<nubasis; jj++) {
          Ae[2*nubasis*ii + (2*jj+0)] -= Np_q[ii] * dNux_q[jj] * fac;
          Ae[2*nubasis*ii + (2*jj+1)] -= Np_q[ii] * dNuy_q[jj] * fac;
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(B,npbasis,elnidx_p,2*nubasis,eldofs_u,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobianHdiv(Apu): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* \int dot(w , grad(p)) dV */
#undef __FUNCT__
#define __FUNCT__ "FormBilinearA12_PDEDarcyL2"
PetscErrorCode FormBilinearA12_PDEDarcyL2(PDEDarcy *darcy,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nubasis,npbasis;
  PetscInt  *element_u,*element_p;
  PetscReal *coords,*elcoords_p,*weight;
  PetscInt  *eldofs_u,*eldofs_p;
  PetscReal *Ae;
  PetscInt  q,nqp;
  PetscReal *detJ,**dNpxi,**dNpeta,**dNpx,**dNpy,**Nu;
  IS        ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM     dmu = NULL;
  DM     dmp = NULL;
  EContainer cu,cp;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dmu = darcy->dmu;
  dmp = darcy->dmp;
  ierr = DMTetSpaceElement(dmu,&nel,&nubasis,&element_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&npbasis,&element_p,NULL,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(darcy->ref,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  bc = darcy->p_dirichlet;
  
  cu = darcy->ec_u;
  Nu = cu->N;

  cp = darcy->ec_p;
  dNpxi  = cp->dNr1;
  dNpeta = cp->dNr2;
  dNpx   = cp->dNx1;
  dNpy   = cp->dNx2;
  
  detJ       = cu->buf_q_scalar_a;
  eldofs_u   = cu->buf_basis_2index_a;
  elcoords_p = cp->buf_basis_2vector_a;
  
  eldofs_p   = cp->buf_basis_index_a;
  
  ierr = PetscMalloc1(2*nubasis*npbasis,&Ae);CHKERRQ(ierr);
  
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *elnidx_u,*elnidx_p;
    
    /* get element -> node map */
    elnidx_u = &element_u[nubasis*e];
    elnidx_p = &element_p[npbasis*e];
    
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      eldofs_u[2*i+0] = 2*nidx;
      eldofs_u[2*i+1] = 2*nidx+1;
    }

    /* generate masked dof indices */
    for (i=0; i<npbasis; i++) {
      const PetscInt nidx = elnidx_p[i];
      
      eldofs_p[i] = GINDICES[nidx];
    }

    /* get element coordinates */
    for (i=0; i<npbasis; i++) {
      const PetscInt nidx = elnidx_p[i];
      
      elcoords_p[2*i  ] = coords[2*nidx  ];
      elcoords_p[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,npbasis,elcoords_p,dNpxi,dNpeta,dNpx,dNpy,detJ);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nubasis*npbasis*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Nu_q;
      PetscReal *dNpx_q,*dNpy_q;
      
      /* get access to element->quadrature points */
      Nu_q   = Nu[q];
      dNpx_q = dNpx[q];
      dNpy_q = dNpy[q];
      
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<nubasis; ii++) {
        for (jj=0; jj<npbasis; jj++) {
          
          Ae[npbasis*(2*ii+0) + jj] += Nu_q[ii] * dNpx_q[jj] * fac;
          Ae[npbasis*(2*ii+1) + jj] += Nu_q[ii] * dNpy_q[jj] * fac;
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(B,2*nubasis,eldofs_u,npbasis,eldofs_p,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobianL2(Aup): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* \int dot( grad(q) , u ) dV */
#undef __FUNCT__
#define __FUNCT__ "FormBilinearA21_PDEDarcyL2"
PetscErrorCode FormBilinearA21_PDEDarcyL2(PDEDarcy *darcy,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nubasis,npbasis;
  PetscInt  *element_u,*element_p;
  PetscReal *coords,*elcoords_p,*weight;
  PetscInt  *eldofs_u,*eldofs_p;
  PetscReal *Ae;
  PetscInt  q,nqp;
  PetscReal *detJ,**dNpxi,**dNpeta,**dNpx,**dNpy,**Nu;
  IS        ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM     dmu = NULL;
  DM     dmp = NULL;
  EContainer cu,cp;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dmu = darcy->dmu;
  dmp = darcy->dmp;
  ierr = DMTetSpaceElement(dmu,&nel,&nubasis,&element_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&npbasis,&element_p,NULL,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(darcy->ref,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  bc = darcy->p_dirichlet;
  
  cu = darcy->ec_u;
  Nu = cu->N;
  
  cp = darcy->ec_p;
  dNpxi  = cp->dNr1;
  dNpeta = cp->dNr2;
  dNpx   = cp->dNx1;
  dNpy   = cp->dNx2;
  
  detJ       = cu->buf_q_scalar_a;
  eldofs_u   = cu->buf_basis_2index_a;
  elcoords_p = cp->buf_basis_2vector_a;
  
  eldofs_p   = cp->buf_basis_index_a;
  
  ierr = PetscMalloc1(2*nubasis*npbasis,&Ae);CHKERRQ(ierr);
  
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *elnidx_u,*elnidx_p;
    
    /* get element -> node map */
    elnidx_u = &element_u[nubasis*e];
    elnidx_p = &element_p[npbasis*e];
    
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      eldofs_u[2*i+0] = 2*nidx;
      eldofs_u[2*i+1] = 2*nidx+1;
    }

    /* generate masked dof indices */
    for (i=0; i<npbasis; i++) {
      const PetscInt nidx = elnidx_p[i];
      
      eldofs_p[i] = GINDICES[nidx];
    }

    /* get element coordinates */
    for (i=0; i<npbasis; i++) {
      const PetscInt nidx = elnidx_p[i];
      
      elcoords_p[2*i  ] = coords[2*nidx  ];
      elcoords_p[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,npbasis,elcoords_p,dNpxi,dNpeta,dNpx,dNpy,detJ);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nubasis*npbasis*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Nu_q;
      PetscReal *dNpx_q,*dNpy_q;
      
      /* get access to element->quadrature points */
      Nu_q   = Nu[q];
      dNpx_q = dNpx[q];
      dNpy_q = dNpy[q];
      
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<npbasis; ii++) {
        for (jj=0; jj<nubasis; jj++) {
          Ae[2*nubasis*ii + (2*jj+0)] += dNpx_q[ii] * Nu_q[jj] * fac;
          Ae[2*nubasis*ii + (2*jj+1)] += dNpy_q[ii] * Nu_q[jj] * fac;
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(B,npbasis,eldofs_p,2*nubasis,eldofs_u,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobianL2(Apu): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormBilinearSpp_PDEDarcy"
PetscErrorCode FormBilinearSpp_PDEDarcy(PDEDarcy *darcy,Mat B22)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nbasis_p,nbasis_g;
  PetscInt  *element_g,*element_p;
  PetscReal   *coords_g,*elcoords_g;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal detJ_affine,**Np,**dNpxi,**dNpeta,**dNpx,**dNpy,*weight,*coeff_a,*coeff_a_e;
  DM dm = NULL;
  EContainer cp;
  Quadrature quadrature;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = darcy->dmp;
  ierr = DMTetSpaceElement(dm,&nel,&nbasis_p,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,&nbasis_g,&element_g,NULL,NULL,&coords_g);CHKERRQ(ierr);

  ierr = CoefficientGetQuadrature(darcy->a,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"a",NULL,NULL,NULL,&coeff_a);CHKERRQ(ierr);
  
  cp  = darcy->ec_p;
  Np     = cp->N;
  dNpxi  = cp->dNr1;
  dNpeta = cp->dNr2;
  dNpx   = cp->dNx1;
  dNpy   = cp->dNx2;
  
  ierr = PetscMalloc1(nbasis_p*nbasis_p,&Ke);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis_g,&elcoords_g);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt    *eldofs_p;
    PetscInt    *eldofs_g;
    
    /* get element -> node map */
    eldofs_p = &element_p[nbasis_p*e];
    eldofs_g = &element_g[nbasis_g*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis_g; i++) {
      const PetscInt nidx = eldofs_g[i];
      
      elcoords_g[2*i  ] = coords_g[2*nidx  ];
      elcoords_g[2*i+1] = coords_g[2*nidx+1];
    }
    
    /* compute derivatives */
    //EvaluateBasisGeometryDerivatives(nqp,nbasis_u,elcoords,cv->dNr1,cv->dNr2,cv->dNx1,cv->dNx2,detJ);
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_p,elcoords_g,dNpxi,dNpeta,dNpx,dNpy,&detJ_affine);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis_p*nbasis_p);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_a,&coeff_a_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      /* get access to element->quadrature points */
      fac = weight[q] * detJ_affine;
      
      for (ii=0; ii<nbasis_p; ii++) {
        for (jj=ii; jj<nbasis_p; jj++) {
          Ke[ii*nbasis_p+jj] += (1.0/coeff_a_e[q]) * Np[q][ii] * Np[q][jj] * fac;
        }
      }
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < nbasis_p; ii++) {
      for (jj = ii; jj < nbasis_p; jj++) {
        Ke[jj*nbasis_p+ii] = Ke[ii*nbasis_p+jj];
      }
    }
    ierr = MatSetValuesLocal(B22,nbasis_p,eldofs_p,nbasis_p,eldofs_p,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B22,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B22,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobian(Spp): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = PetscFree(elcoords_g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormJacobian_Darcy"
PetscErrorCode FormJacobian_Darcy(SNES snes,Vec X,Mat A,Mat B,void *ctx)
{
  PDE               pde = (PDE)ctx;
  PDEDarcy          *data;
  DM                pack,dau,dap;
  IS                *is;
  Vec               Uloc,Ploc;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscBool         is_mffd = PETSC_FALSE;
  PetscBool         is_nest = PETSC_FALSE;
  PetscBool         is_shell = PETSC_FALSE;
  PetscErrorCode    ierr;
  
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  pack = data->dms;
  
  ierr = DMCompositeGetEntries(pack,&dau,&dap);CHKERRQ(ierr);
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  
  ierr = DMCompositeScatter(pack,X,Uloc,Ploc);CHKERRQ(ierr);
  pde->local_vector_valid = PETSC_TRUE;
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetGlobalISs(pack,&is);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->a);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->f);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);

  /* preconditioner for Jacobian */
  ierr = PetscObjectTypeCompare((PetscObject)B,MATMFFD, &is_mffd);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST, &is_nest);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATSHELL,&is_shell);CHKERRQ(ierr);
  
  {
    Mat Buu;
    
    ierr = MatGetSubMatrix(B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) { ierr = FormBilinearA11_PDEDarcyL2(data,Buu,Buu);CHKERRQ(ierr); }
    ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    
    ierr = MatGetSubMatrix(B,is[0],is[1],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    if (Buu) {
      is_shell = PETSC_FALSE;
      ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
      if (!is_shell) {
        ierr = FormBilinearA12_PDEDarcyL2(data,Buu,Buu);CHKERRQ(ierr);
      }
      ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    }

    ierr = MatGetSubMatrix(B,is[1],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    if (Buu) {
      is_shell = PETSC_FALSE;
      ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
      if (!is_shell) {
        ierr = FormBilinearA21_PDEDarcyL2(data,Buu,Buu);CHKERRQ(ierr);
      }
      ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    }

    ierr = MatGetSubMatrix(B,is[1],is[1],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    if (Buu) {
      ierr = MatZeroEntries(Buu);CHKERRQ(ierr);
    }
    if (data->p_dirichlet && Buu) { /* insert boundary conditions */
      PetscInt NUM_GINDICES;
      const PetscInt *GINDICES;
      IS ltog;
      
      NUM_GINDICES = data->p_dirichlet->L_local;
      ierr = ISCreateStride(PetscObjectComm((PetscObject)Buu),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
      ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);

      ierr = BCListInsertScaling(Buu,-1,(PetscInt*)GINDICES,data->p_dirichlet);CHKERRQ(ierr);

      ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
      ierr = ISDestroy(&ltog);CHKERRQ(ierr);

      ierr = MatAssemblyBegin(Buu,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      ierr = MatAssemblyEnd  (Buu,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      //ierr = MatView(Buu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    }
    if (Buu) { ierr = MatDestroy(&Buu);CHKERRQ(ierr); }
    
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* preconditioner for Schur */
  {
    PetscBool is_fs;
    KSP ksp;
    PC pc;
    PCFieldSplitSchurPreType ptype;
    Mat Spp = NULL;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    
    ierr = PetscObjectTypeCompare((PetscObject)pc,PCFIELDSPLIT, &is_fs);CHKERRQ(ierr);
    if (is_fs) {
      ierr = PCFieldSplitGetSchurPre(pc,&ptype,&Spp);CHKERRQ(ierr);
      
      if (Spp) {
        ierr = MatZeroEntries(Spp);CHKERRQ(ierr);
        ierr = FormBilinearSpp_PDEDarcy(data,Spp);CHKERRQ(ierr);
      }
    }
  }
  
  /* clean up */
  ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
  ierr = PetscFree(is);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormJacobian_DarcyHdivL2"
PetscErrorCode FormJacobian_DarcyHdivL2(SNES snes,Vec X,Mat A,Mat B,void *ctx)
{
  PDE               pde = (PDE)ctx;
  PDEDarcy          *data;
  DM                pack,dau,dap;
  IS                *is;
  Vec               Uloc,Ploc;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscBool         is_mffd = PETSC_FALSE;
  PetscBool         is_nest = PETSC_FALSE;
  PetscBool         is_shell = PETSC_FALSE;
  PetscErrorCode    ierr;
  
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  pack = data->dms;
  
  ierr = DMCompositeGetEntries(pack,&dau,&dap);CHKERRQ(ierr);
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  
  ierr = DMCompositeScatter(pack,X,Uloc,Ploc);CHKERRQ(ierr);
  pde->local_vector_valid = PETSC_TRUE;
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetGlobalISs(pack,&is);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->a);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->f);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);
  
  /* preconditioner for Jacobian */
  ierr = PetscObjectTypeCompare((PetscObject)B,MATMFFD, &is_mffd);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST, &is_nest);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATSHELL,&is_shell);CHKERRQ(ierr);
  
  {
    Mat Buu;
    
    ierr = MatGetSubMatrix(B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) { ierr = FormBilinearA11_PDEDarcy(data,Buu,Buu);CHKERRQ(ierr); }
    ierr = MatDestroy(&Buu);CHKERRQ(ierr);

    ierr = MatGetSubMatrix(B,is[0],is[1],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    if (Buu) {
      is_shell = PETSC_FALSE;
      ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
      if (!is_shell) {
        ierr = FormBilinearA12_PDEDarcyHdiv(data,Buu,Buu);CHKERRQ(ierr);
      }
      ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    }

    ierr = MatGetSubMatrix(B,is[1],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    if (Buu) {
      is_shell = PETSC_FALSE;
      ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
      if (!is_shell) {
        ierr = FormBilinearA21_PDEDarcyHdiv(data,Buu,Buu);CHKERRQ(ierr);
      }
      ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    }

    ierr = MatGetSubMatrix(B,is[1],is[1],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    if (Buu) {
      ierr = MatZeroEntries(Buu);CHKERRQ(ierr);
    }
    if (data->p_dirichlet && Buu) { /* insert boundary conditions */
      PetscInt NUM_GINDICES;
      const PetscInt *GINDICES;
      IS ltog;
      
      NUM_GINDICES = data->p_dirichlet->L_local;
      ierr = ISCreateStride(PetscObjectComm((PetscObject)Buu),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
      ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
      
      ierr = BCListInsertScaling(Buu,-1,(PetscInt*)GINDICES,data->p_dirichlet);CHKERRQ(ierr);
      
      ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
      ierr = ISDestroy(&ltog);CHKERRQ(ierr);
      
      ierr = MatAssemblyBegin(Buu,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      ierr = MatAssemblyEnd  (Buu,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      //ierr = MatView(Buu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    }
    if (Buu) { ierr = MatDestroy(&Buu);CHKERRQ(ierr); }
    
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* preconditioner for Schur */
  {
    PetscBool is_fs;
    KSP ksp;
    PC pc;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    
    ierr = PetscObjectTypeCompare((PetscObject)pc,PCFIELDSPLIT, &is_fs);CHKERRQ(ierr);
    if (is_fs) {
      PCFieldSplitSchurPreType ptype;
      Mat Spp = NULL;
      
      ierr = PCFieldSplitGetSchurPre(pc,&ptype,&Spp);CHKERRQ(ierr);

      if (Spp) {
        Mat Buu,Bup,Bpu;
        Vec diag;
        Mat _P = NULL,_PtAP = NULL;

        ierr = MatGetSubMatrix(B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
        ierr = MatGetSubMatrix(B,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
        ierr = MatGetSubMatrix(B,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
        
        ierr = MatCreateVecs(Buu,&diag,NULL);CHKERRQ(ierr);
        ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
        ierr = VecReciprocal(diag);CHKERRQ(ierr);
        
        ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
        ierr = MatMatMult(Bpu,Bup,MAT_REUSE_MATRIX,1.0,&Spp);CHKERRQ(ierr);
        ierr = PetscObjectSetName((PetscObject)Spp,"Spp*_");CHKERRQ(ierr);
        
        ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
        ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
        
        ierr = MatScale(Spp,-1.0);CHKERRQ(ierr);
        
        ierr = PetscObjectQuery((PetscObject)Spp,"PCAuxSpace_P",(PetscObject*)&_P);CHKERRQ(ierr);
        ierr = PetscObjectQuery((PetscObject)Spp,"PCAuxSpace_PtAP",(PetscObject*)&_PtAP);CHKERRQ(ierr);
        if (_P && _PtAP) {
          /*Mat A12P,Pt,A21_d_A12P;*/
          Mat A12P,D;
          
          /*// Assemble X = (diag(M))(Bup.P); Y = Bpu.X; PtAP = Pt.Y //
          ierr = MatMatMult(Bup,_P,MAT_INITIAL_MATRIX,1.0,&A12P);CHKERRQ(ierr);
          
          ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
          ierr = VecReciprocal(diag);CHKERRQ(ierr);
          ierr = VecScale(diag,-1.0);CHKERRQ(ierr);
          ierr = MatDiagonalScale(A12P,diag,NULL);CHKERRQ(ierr);
          
          ierr = MatMatMult(Bpu,A12P,MAT_INITIAL_MATRIX,1.0,&A21_d_A12P);CHKERRQ(ierr);
          ierr = MatDestroy(&A12P);CHKERRQ(ierr);
          
          ierr = MatTranspose(_P,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
          ierr = MatMatMult(Pt,A21_d_A12P,MAT_REUSE_MATRIX,1.0,&_PtAP);CHKERRQ(ierr);
          ierr = MatDestroy(&A21_d_A12P);CHKERRQ(ierr);
          ierr = MatDestroy(&Pt);CHKERRQ(ierr);
          */
          // Faster alternative using MatPtAP() //
          ierr = MatMatMult(Bup,_P,MAT_INITIAL_MATRIX,1.0,&A12P);CHKERRQ(ierr);
          ierr = MatDuplicateDiagonalReciprocal(Buu,MATAIJ,&D);CHKERRQ(ierr);
          ierr = MatScale(D,-1.0);CHKERRQ(ierr);
          ierr = MatPtAP(D,A12P,MAT_REUSE_MATRIX,1.0,&_PtAP);CHKERRQ(ierr);
          
          ierr = MatDestroy(&A12P);CHKERRQ(ierr);
          ierr = MatDestroy(&D);CHKERRQ(ierr);
        }
        
        ierr = MatDestroy(&Buu);CHKERRQ(ierr);
        ierr = MatDestroy(&Bup);CHKERRQ(ierr);
        ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
        ierr = VecDestroy(&diag);CHKERRQ(ierr);
      }
      
    }
  }
  
  /* clean up */
  ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
  ierr = PetscFree(is);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_SurfaceI_U"
static PetscErrorCode FormFunctionLocal_SurfaceI_U(PDEDarcy *data,DM dm,PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis,nbasis_face;
  PetscReal *w,*xi;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,nen,npe;
  PetscInt e,i,q;
  PetscScalar *el_F;
  Coefficient qN;
  Quadrature quadrature;
  EContainer elcontainer;
  PetscReal *_el_coor_face,dJs;
  PetscInt bf,nfacets;
  PetscReal *normal,**N_face;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscBool issetup;
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* return early if no facets have been defined */
  if (nfacets == 0) {
    PetscFunctionReturn(0);
  } else {
    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetOrientations(f,&normal,NULL);CHKERRQ(ierr);
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  qN = data->p_neumann;
  ierr = CoefficientGetQuadrature(qN,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"pN",&nfacets,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&elcontainer);CHKERRQ(ierr);
  _el_coor_face = elcontainer->buf_basis_2vector_a;
  N_face        = elcontainer->N;
  
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&el_F);CHKERRQ(ierr);
  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt *eidx,face_id,*eidx_face;
    PetscReal *normal_f;
    
    e = facet_to_element[bf];
    face_id = facet_element_face_id[bf];
    
    /* get basis dofs */
    eidx = &element[npe*e];
    
    /* face dof */
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = eidx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    ierr = BFacetRestrictField(f,face_id,2,elcoords,_el_coor_face);CHKERRQ(ierr);
    
    EvaluateBasisSurfaceJacobian_Affine(nbasis_face,_el_coor_face,&dJs);
    normal_f = &normal[2*bf];
    
    ierr = PetscMemzero(el_F,sizeof(PetscScalar)*nbasis*2);CHKERRQ(ierr);
    /* 
      \int (\vec w . p \vec n) dS
    */
    for (q=0; q<nqp; q++) {
      PetscReal pressure_q;
      
      pressure_q = coeff[bf*nqp + q];
      
      for (i=0; i<nbasis_face; i++) {
        PetscScalar residual[2];
        
        residual[0] = N_face[q][i] * ( pressure_q ) * normal_f[0];
        residual[1] = N_face[q][i] * ( pressure_q ) * normal_f[1];
        
        /* note the positive sign */
        el_F[2*i+0] += w[q] * ( residual[0] ) * dJs;
        el_F[2*i+1] += w[q] * ( residual[1] ) * dJs;
      }
    }
    
    for (i=0; i<nbasis_face; i++) {
      PetscInt face_idx;
      
      face_idx = eidx_face[i];
      
      F[2*eidx[face_idx]+0] += el_F[2*i+0];
      F[2*eidx[face_idx]+1] += el_F[2*i+1];
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  ierr = PetscFree(el_F);CHKERRQ(ierr);
  ierr = EContainerDestroy(&elcontainer);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_SurfaceI_P"
static PetscErrorCode FormFunctionLocal_SurfaceI_P(PDEDarcy *data,DM dm,PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis,nbasis_face;
  PetscReal *w,*xi;
  PetscReal *coords,*elcoords,*coeff,*coeff_x;
  PetscInt *element,nen,npe;
  PetscInt e,i,q;
  PetscScalar *el_F;
  Coefficient qN;
  Quadrature quadrature;
  EContainer elcontainer;
  PetscReal *_el_coor_face,dJs;
  PetscInt bf,nfacets;
  PetscReal *normal,**N_face;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscBool issetup;
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* return early if no facets have been defined */
  if (nfacets == 0) {
    PetscFunctionReturn(0);
  } else {
    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetOrientations(f,&normal,NULL);CHKERRQ(ierr);
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  qN = data->u_neumann;
  ierr = CoefficientGetQuadrature(qN,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"uN",&nfacets,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,NULL,NULL,&coeff_x);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&elcontainer);CHKERRQ(ierr);
  _el_coor_face = elcontainer->buf_basis_2vector_a;
  N_face        = elcontainer->N;
  
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&el_F);CHKERRQ(ierr);
  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt *eidx,face_id,*eidx_face;
    PetscReal *normal_f;
    
    e = facet_to_element[bf];
    face_id = facet_element_face_id[bf];
    
    /* get basis dofs */
    eidx = &element[npe*e];
    
    /* face dof */
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = eidx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    ierr = BFacetRestrictField(f,face_id,2,elcoords,_el_coor_face);CHKERRQ(ierr);
    
    EvaluateBasisSurfaceJacobian_Affine(nbasis_face,_el_coor_face,&dJs);
    normal_f = &normal[2*bf];
    
    /*
      -\int w (\vec u .\vec n) dS
    */
    ierr = PetscMemzero(el_F,sizeof(PetscScalar)*nbasis);CHKERRQ(ierr);
    for (q=0; q<nqp; q++) {
      PetscReal *flux_q;
      
      flux_q = &coeff[2*bf*nqp + 2*q];
      
      for (i=0; i<nbasis_face; i++) {
        PetscScalar residual;
        
        residual = N_face[q][i] * (flux_q[0] * normal_f[0] +  flux_q[1] * normal_f[1]);
        
        /* note the negative sign */
        el_F[i] -= w[q] * ( residual ) * dJs;
      }
    }
    
    for (i=0; i<nbasis_face; i++) {
      PetscInt face_idx;
      
      face_idx = eidx_face[i];
      
      F[eidx[face_idx]] += el_F[i];
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  ierr = PetscFree(el_F);CHKERRQ(ierr);
  ierr = EContainerDestroy(&elcontainer);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_VolumeI_U_L2H1"
static PetscErrorCode FormFunctionLocal_VolumeI_U_L2H1(PDEDarcy *darcy,
                                          DM dau,PetscScalar ufield[],
                                          DM dap,PetscScalar pfield[],PetscScalar Ru[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**Nu,**dNpxi,**dNpeta;
  PetscReal *detJ,**dNpx,**dNpy;
  PetscReal fac;
  Coefficient a,f;
  Quadrature quadrature_a,quadrature_f;
  PetscReal *coeff_a,*coeff_a_e;
  PetscReal *coeff_f,*coeff_f_e;
  PetscInt ncomp_a,ncomp_f;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  a = darcy->a;
  f = darcy->f;
  
  ierr = CoefficientGetQuadrature(a,&quadrature_a);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_a,"a",NULL,NULL,&ncomp_a,&coeff_a);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(f,&quadrature_f);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_f,"f",NULL,NULL,&ncomp_f,&coeff_f);CHKERRQ(ierr);
  
  if (volQ->npoints != quadrature_a->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  if (volQ->npoints != quadrature_f->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  Nu     = darcy->ec_u->N;
  dNpxi  = darcy->ec_p->dNr1;
  dNpeta = darcy->ec_p->dNr2;
  dNpx   = darcy->ec_p->dNx1;
  dNpy   = darcy->ec_p->dNx2;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Be);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,NULL,NULL,&coords);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    /* get coordinates from pressure space - safe for H1 */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }
    
    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    //EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    EvaluateBasisGeometryDerivatives(nqp,npbasis,elcoords,dNpxi,dNpeta,dNpx,dNpy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Fe, sizeof(PetscScalar) * (nubasis*2) );
    PetscMemzero( Be, sizeof(PetscScalar) * (nubasis*2) );
    
    ierr = QuadratureGetElementValues(quadrature_a,e,1,coeff_a,&coeff_a_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_f,e,2,coeff_f,&coeff_f_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      LFormMF__inner_w_a_u(nubasis,fac,coeff_a_e[q],Uxe,Uye,Nu[q],Fe);
      /* grad-div stab */
      /*LFormMF__div_w_div_u(nubasis,PENALTY * fac,Uxe,Uye,dNux[q],dNuy[q],Fe);*/
       
      LFormMF__inner_w_grad_p(nubasis,npbasis,fac,Pe,Nu[q],dNpx[q],dNpy[q],Fe);
      
      /* compute RHS terms here */
      for (k=0; k<nubasis; k++) {
        Be[2*k  ] += fac * Nu[q][k] * coeff_f_e[2*q + 0];
        Be[2*k+1] += fac * Nu[q][k] * coeff_f_e[2*q + 1];
      }
      /* grad-div stab */
      /*LFormMF__div_w_f(nubasis,-PENALTY * fac,coeff_g_e[q],dNux[q],dNuy[q],Be);*/

    }
    
    /* combine RHS with A.x */
    for (k=0; k<nubasis; k++) {
      Fe[2*k  ] = Fe[2*k  ] - Be[2*k  ];
      Fe[2*k+1] = Fe[2*k+1] - Be[2*k+1];
    }
    
    /* sum into residual vector */
    for (k=0; k<nubasis; k++) {
      Ru[ u_el_lidx[2*k]   ] += Fe[2*k];
      Ru[ u_el_lidx[2*k+1] ] += Fe[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyResidual(u): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(u_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Uxe);
  PetscFree(Uye);
  PetscFree(Fe);
  PetscFree(Be);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_VolumeI_P_L2H1"
static PetscErrorCode FormFunctionLocal_VolumeI_P_L2H1(PDEDarcy *darcy,
                                          DM dau,PetscScalar ufield[],
                                          DM dap,PetscScalar pfield[],PetscScalar Rp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**dNpxi,**dNpeta,**Np,**Nu;
  PetscReal *detJ,**dNpx,**dNpy;
  PetscReal fac;
  Coefficient g;
  Quadrature quadrature_g;
  PetscReal *coeff_g,*coeff_g_e;
  PetscInt ncomp_g;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  g = darcy->g;
  
  ierr = CoefficientGetQuadrature(g,&quadrature_g);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_g,"g",NULL,NULL,&ncomp_g,&coeff_g);CHKERRQ(ierr);
  
  if (volQ->npoints != quadrature_g->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  Nu     = darcy->ec_u->N;
  Np     = darcy->ec_p->N;
  dNpxi  = darcy->ec_p->dNr1;
  dNpeta = darcy->ec_p->dNr2;
  dNpx   = darcy->ec_p->dNx1;
  dNpy   = darcy->ec_p->dNx2;

  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Be);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,NULL,NULL,&coords);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    /* get coordinates */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }
    
    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,npbasis,elcoords,dNpxi,dNpeta,dNpx,dNpy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Fe, sizeof(PetscScalar) * (npbasis) );
    PetscMemzero( Be, sizeof(PetscScalar) * (npbasis) );
    
    ierr = QuadratureGetElementValues(quadrature_g,e,1,coeff_g,&coeff_g_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      LFormMF__inner_grad_q_u(nubasis,npbasis,fac,Uxe,Uye,Nu[q],dNpx[q],dNpy[q],Fe);
      //LFormMF__qp(npbasis,fac,Pe,Np[q],Fe);
      
      /* compute body force terms here */
      for (k=0; k<npbasis; k++) {
        Be[k] += fac * Np[q][k] * coeff_g_e[q];
      }
    }
    
    /* combine body force with A.x */
    for (k=0; k<npbasis; k++) {
      Fe[k] = Fe[k] - Be[k];
    }
    
    /* sum into residual vector */
    for (k=0; k<npbasis; k++) {
      Rp[ p_el_lidx[k]   ] += Fe[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyResidual(p): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(u_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Uxe);
  PetscFree(Uye);
  PetscFree(Fe);
  PetscFree(Be);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_VolumeI_U_HdivL2"
static PetscErrorCode FormFunctionLocal_VolumeI_U_HdivL2(PDEDarcy *darcy,
                                                       DM dau,PetscScalar ufield[],
                                                       DM dap,PetscScalar pfield[],PetscScalar Ru[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**Nu,**dNuxi,**dNueta,**Np;
  PetscReal *detJ,**dNux,**dNuy;
  PetscReal fac;
  Coefficient a,f,g;
  Quadrature quadrature_a,quadrature_f,quadrature_g;
  PetscReal *coeff_a,*coeff_a_e;
  PetscReal *coeff_f,*coeff_f_e;
  PetscReal *coeff_g,*coeff_g_e;
  PetscInt ncomp_a,ncomp_f;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  a = darcy->a;
  f = darcy->f;
  g = darcy->g;
  
  ierr = CoefficientGetQuadrature(a,&quadrature_a);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_a,"a",NULL,NULL,&ncomp_a,&coeff_a);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(f,&quadrature_f);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_f,"f",NULL,NULL,&ncomp_f,&coeff_f);CHKERRQ(ierr);

  ierr = CoefficientGetQuadrature(g,&quadrature_g);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_g,"g",NULL,NULL,NULL,&coeff_g);CHKERRQ(ierr);

  if (volQ->npoints != quadrature_a->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  if (volQ->npoints != quadrature_f->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  Nu     = darcy->ec_u->N;
  dNuxi  = darcy->ec_u->dNr1;
  dNueta = darcy->ec_u->dNr2;
  dNux   = darcy->ec_u ->dNx1;
  dNuy   = darcy->ec_u->dNx2;
  Np     = darcy->ec_p->N;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Be);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,NULL,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    /* get coordinates from pressure space - safe for H1 */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }
    
    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    //EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Fe, sizeof(PetscScalar) * (nubasis*2) );
    PetscMemzero( Be, sizeof(PetscScalar) * (nubasis*2) );
    
    ierr = QuadratureGetElementValues(quadrature_a,e,1,coeff_a,&coeff_a_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_f,e,2,coeff_f,&coeff_f_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_g,e,1,coeff_g,&coeff_g_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      LFormMF__inner_w_a_u(nubasis,fac,coeff_a_e[q],Uxe,Uye,Nu[q],Fe);
      /* grad-div stab */
      //LFormMF__div_w_div_u(nubasis,PENALTY * fac,Uxe,Uye,dNux[q],dNuy[q],Fe);
      
      /* note the negative sign */
      LFormMF__div_w_p(nubasis,npbasis,-fac,Pe,dNux[q],dNuy[q],Np[q],Fe);
      
      /* compute RHS terms here */
      for (k=0; k<nubasis; k++) {
        Be[2*k  ] += fac * Nu[q][k] * coeff_f_e[2*q + 0];
        Be[2*k+1] += fac * Nu[q][k] * coeff_f_e[2*q + 1];
      }
      /* grad-div stab */
      //LFormMF__div_w_f(nubasis,-PENALTY * fac,coeff_g_e[q],dNux[q],dNuy[q],Be);
    }
    
    /* combine RHS with A.x */
    for (k=0; k<nubasis; k++) {
      Fe[2*k  ] = Fe[2*k  ] - Be[2*k  ];
      Fe[2*k+1] = Fe[2*k+1] - Be[2*k+1];
    }
    
    /* sum into residual vector */
    for (k=0; k<nubasis; k++) {
      Ru[ u_el_lidx[2*k]   ] += Fe[2*k];
      Ru[ u_el_lidx[2*k+1] ] += Fe[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyResidual(u): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(u_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Uxe);
  PetscFree(Uye);
  PetscFree(Fe);
  PetscFree(Be);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_VolumeI_P_HdivL2"
static PetscErrorCode FormFunctionLocal_VolumeI_P_HdivL2(PDEDarcy *darcy,
                                                       DM dau,PetscScalar ufield[],
                                                       DM dap,PetscScalar pfield[],PetscScalar Rp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**dNuxi,**dNueta,**Np;
  PetscReal *detJ,**dNux,**dNuy;
  PetscReal fac;
  Coefficient g;
  Quadrature quadrature_g;
  PetscReal *coeff_g,*coeff_g_e;
  PetscInt ncomp_g;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  g = darcy->g;
  
  ierr = CoefficientGetQuadrature(g,&quadrature_g);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_g,"g",NULL,NULL,&ncomp_g,&coeff_g);CHKERRQ(ierr);
  
  if (volQ->npoints != quadrature_g->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  dNuxi  = darcy->ec_u->dNr1;
  dNueta = darcy->ec_u->dNr2;
  dNux   = darcy->ec_u->dNx1;
  dNuy   = darcy->ec_u->dNx2;
  
  Np     = darcy->ec_p->N;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Be);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,NULL,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }
    
    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Fe, sizeof(PetscScalar) * (npbasis) );
    PetscMemzero( Be, sizeof(PetscScalar) * (npbasis) );
    
    ierr = QuadratureGetElementValues(quadrature_g,e,1,coeff_g,&coeff_g_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      /* note the negative sign */
      LFormMF__q_div_u(nubasis,npbasis,-fac,Uxe,Uye,dNux[q],dNuy[q],Np[q],Fe);
      
      /* compute body force terms here */
      for (k=0; k<npbasis; k++) {
        Be[k] += fac * Np[q][k] * coeff_g_e[q];
      }
    }
    
    /* combine body force with A.x */
    for (k=0; k<npbasis; k++) {
      Fe[k] = Fe[k] - Be[k];
    }
    
    /* sum into residual vector */
    for (k=0; k<npbasis; k++) {
      Rp[ p_el_lidx[k]   ] += Fe[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyResidual(p): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(u_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Uxe);
  PetscFree(Uye);
  PetscFree(Fe);
  PetscFree(Be);
  
  PetscFunctionReturn(0);
}

/*
 Computes r = Ax - b
 SNES will scale by -1, F = -r = b - Ax
 Thus, in OUR function, dirichlet slots become A_ii(x_i - phi)
 In SNES, these become A_ii(phi-x_i), and the updates on the dirichlet slots will be
 A_ii d_i = -F_i
 = A_ii(phi-x_i)
 Then the update will be
 x_i^new = x_i + d_i
 = x_i + inv(A_ii) A_ii(phi-x_i)
 = x_i + phi - x_i
 = phi
 */
#undef __FUNCT__
#define __FUNCT__ "FormFunction_Darcy"
PetscErrorCode FormFunction_Darcy(SNES snes,Vec X,Vec F,void *ctx)
{
  PetscErrorCode    ierr;
  PDE               pde = (PDE)ctx;
  PDEDarcy          *data;
  DM                pack,dau,dap;
  Vec               Uloc,Ploc,FUloc,FPloc;
  Vec               u,p,Fu,Fp;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscScalar       *LA_FUloc,*LA_FPloc;
  BCList            u_bclist,p_bclist;
  
  PetscFunctionBegin;
  
  data = (PDEDarcy*)pde->data;
  pack = data->dms;
  u_bclist = data->u_dirichlet;
  p_bclist = data->p_dirichlet;
  
  ierr = DMCompositeGetEntries(pack,&dau,&dap);CHKERRQ(ierr);
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  ierr = DMCompositeGetLocalVectors(pack,&FUloc,&FPloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMCompositeScatter(pack,X,Uloc,Ploc);CHKERRQ(ierr);
  /* insert boundary conditions into local vectors */
  if (u_bclist) {
    ierr = BCListInsertLocal(u_bclist,Uloc);CHKERRQ(ierr);
  }
  if (p_bclist) {
    ierr = BCListInsertLocal(p_bclist,Ploc);CHKERRQ(ierr);
  }
  
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(FUloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(FPloc);CHKERRQ(ierr);
  ierr = VecGetArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
  ierr = VecGetArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->a);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->f);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->g);CHKERRQ(ierr);
  if (data->u_neumann) {
    ierr = CoefficientEvaluate(data->u_neumann);CHKERRQ(ierr);
  }
  if (data->p_neumann) {
    ierr = CoefficientEvaluate(data->p_neumann);CHKERRQ(ierr);
  }
  
  switch (data->bform_type) {
    case DARCY_BFORM_L2H1: /* L2-H1 */
      /* momentum */
      ierr = FormFunctionLocal_VolumeI_U_L2H1(data,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
      /* continuity */
      ierr = FormFunctionLocal_VolumeI_P_L2H1(data,dau,LA_Uloc,dap,LA_Ploc,LA_FPloc);CHKERRQ(ierr);
      
      if (data->u_neumann) {
        ierr = FormFunctionLocal_SurfaceI_P(data,dap,LA_FPloc);CHKERRQ(ierr);
      }
      
      break;
    case DARCY_BFORM_HDIVL2: /* Hdiv-L2 */
      /* momentum */
      ierr = FormFunctionLocal_VolumeI_U_HdivL2(data,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
      /* continuity */
      ierr = FormFunctionLocal_VolumeI_P_HdivL2(data,dau,LA_Uloc,dap,LA_Ploc,LA_FPloc);CHKERRQ(ierr);

      if (data->p_neumann) {
        ierr = FormFunctionLocal_SurfaceI_U(data,dau,LA_FUloc);CHKERRQ(ierr);
      }
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown form type");
      break;
  }
  
  ierr = VecRestoreArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  ierr = DMCompositeGather(pack,F,ADD_VALUES,FUloc,FPloc);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreLocalVectors(pack,&FUloc,&FPloc);CHKERRQ(ierr);
  
  /* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
  ierr = DMCompositeGetAccess(pack,F,&Fu,&Fp);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(pack,X,&u,&p);CHKERRQ(ierr);
  
  if (u_bclist) { ierr = BCListResidualDirichlet(u_bclist,u,Fu);CHKERRQ(ierr); }
  if (p_bclist) { ierr = BCListResidualDirichlet(p_bclist,p,Fp);CHKERRQ(ierr); }
  
  ierr = DMCompositeRestoreAccess(pack,X,&u,&p);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(pack,F,&Fu,&Fp);CHKERRQ(ierr);
  
  {
    MatNullSpace nullsp = NULL;
    Mat mat;
    
    ierr = SNESGetJacobian(snes,&mat,0,0,0);CHKERRQ(ierr);
    ierr = MatGetNullSpace(mat,&nullsp);CHKERRQ(ierr);
    if (nullsp) {
      ierr = MatNullSpaceRemove(nullsp,F);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}
