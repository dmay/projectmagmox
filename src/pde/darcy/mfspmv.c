
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <fe_geometry_utils.h>
#include <pdedarcyimpl.h>
#include "../src/pde/darcy/kernels.h"


//#define LOG_MF_OP

/* grad(p) */
#undef __FUNCT__
#define __FUNCT__ "MFDarcyHdivL2_A12"
static PetscErrorCode MFDarcyHdivL2_A12(PDEDarcy *darcy,DM dau,DM dap,PetscScalar Xp[],PetscScalar Yu[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Pe,*Ye;
  PetscInt  *vel_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**GNIxi,**GNIeta,**NIp;
  PetscReal *detJ,**dNudx,**dNudy;
  PetscReal fac;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  
  GNIxi  = darcy->ec_u->dNr1;
  GNIeta = darcy->ec_u->dNr2;
  dNudx  = darcy->ec_u->dNx1;
  dNudy  = darcy->ec_u->dNx2;
  NIp    = darcy->ec_p->N;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&vel_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Ye);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      vel_el_lidx[2*k + 0] = 2 * nidx + 0;
      vel_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get pressure dof indices and dofs */
    p_el_lidx = &elnidx_p[npbasis*e];
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = Xp[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,GNIxi,GNIeta,dNudx,dNudy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Ye, sizeof(PetscScalar)* ( nubasis*2 ) );
    
    for (q=0; q<nqp; q++) {
      fac = -WEIGHT[q] * detJ[q];
      LFormMF__div_w_p(nubasis,npbasis,fac,Pe,dNudx[q],dNudy[q],NIp[q],Ye);
    }
    
    for (k=0; k<nubasis; k++) {
      Yu[ vel_el_lidx[2*k]   ] += Ye[2*k];
      Yu[ vel_el_lidx[2*k+1] ] += Ye[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"MatMultA12(MF): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(vel_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Ye);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MFDarcyL2H1_A12"
static PetscErrorCode MFDarcyL2H1_A12(PDEDarcy *darcy,DM dau,DM dap,PetscScalar Xp[],PetscScalar Yu[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Pe,*Ye;
  PetscInt  *vel_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**GNIxi,**GNIeta,**NIu;
  PetscReal *detJ,**dNpdx,**dNpdy;
  PetscReal fac;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  
  NIu    = darcy->ec_u->N;
  GNIxi  = darcy->ec_p->dNr1;
  GNIeta = darcy->ec_p->dNr2;
  dNpdx  = darcy->ec_p->dNx1;
  dNpdy  = darcy->ec_p->dNx2;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&vel_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Ye);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,&coords);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      vel_el_lidx[2*k + 0] = 2 * nidx + 0;
      vel_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices and dofs */
    p_el_lidx = &elnidx_p[npbasis*e];
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = Xp[nidx];
    }

    /* get coordinates */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,npbasis,elcoords,GNIxi,GNIeta,dNpdx,dNpdy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Ye, sizeof(PetscScalar)* ( nubasis*2 ) );
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      LFormMF__inner_w_grad_p(nubasis,npbasis,fac,Pe,NIu[q],dNpdx[q],dNpdy[q],Ye);
    }
    
    for (k=0; k<nubasis; k++) {
      Yu[ vel_el_lidx[2*k]   ] += Ye[2*k];
      Yu[ vel_el_lidx[2*k+1] ] += Ye[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"MatMultA12(MF): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(vel_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Ye);
  
  PetscFunctionReturn(0);
}

/*
 IN:  X - a pressure vector
 OUT: Y - a velocity vector
 */
#undef __FUNCT__
#define __FUNCT__ "MatMult_MFDarcy_A12"
PetscErrorCode MatMult_MFDarcy_A12(Mat A,Vec X,Vec Y)
{
  PDE               pde;
  PDEDarcy          *darcy;
  PetscErrorCode    ierr;
  DM                dau,dap;
  Vec               XPloc,YUloc;
  PetscScalar       *LA_XPloc;
  PetscScalar       *LA_YUloc;
  
  PetscFunctionBegin;
  
  ierr = MatShellGetContext(A,(void**)&pde);CHKERRQ(ierr);
  darcy = (PDEDarcy*)pde->data;
  
  dau  = darcy->dmu;
  dap  = darcy->dmp;
  
  ierr = DMGetLocalVector(dap,&XPloc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dau,&YUloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMGlobalToLocalBegin(dap,X,INSERT_VALUES,XPloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dap,X,INSERT_VALUES,XPloc);CHKERRQ(ierr);
  
  /* Zero entries in local vectors corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out columns when the mat-mult is performed */
  /* if we have pressure boundary conditions */
  if (darcy->p_dirichlet) {
    ierr = BCListInsertLocalZero(darcy->p_dirichlet,XPloc);CHKERRQ(ierr);
  }
  
  ierr = VecGetArray(XPloc,&LA_XPloc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(YUloc);CHKERRQ(ierr);
  ierr = VecGetArray(YUloc,&LA_YUloc);CHKERRQ(ierr);
  
  /* grad */
  switch (darcy->bform_type) {
    case DARCY_BFORM_L2H1:
      ierr = MFDarcyL2H1_A12(darcy,dau,dap,LA_XPloc,LA_YUloc);CHKERRQ(ierr);
      break;
    case DARCY_BFORM_HDIVL2:
      ierr = MFDarcyHdivL2_A12(darcy,dau,dap,LA_XPloc,LA_YUloc);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown form type");
      break;
  }
  
  ierr = VecRestoreArray(YUloc,&LA_YUloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(XPloc,&LA_XPloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dau,YUloc,ADD_VALUES,Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (dau,YUloc,ADD_VALUES,Y);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dap,&XPloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dau,&YUloc);CHKERRQ(ierr);
  
  /* modify Y for the boundary conditions, y_k = 0, 0 is inserted as this is an off-diagonal operator */
  /* Clobbering entries in global vector corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out rows when the mat-mult is performed */
  if (darcy->u_dirichlet) {
    ierr = BCListInsertZero(darcy->u_dirichlet,Y);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}


/* div(u) */
#undef __FUNCT__
#define __FUNCT__ "MFDarcyHdivL2_A21"
PetscErrorCode MFDarcyHdivL2_A21(PDEDarcy *darcy,DM dau,DM dap,PetscScalar Xu[],PetscScalar Yp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Xue,*Ype,*ux,*uy;
  PetscInt  *vel_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**GNIxi,**GNIeta,**NIp;
  PetscReal *detJ,**dNudx,**dNudy;
  PetscReal fac;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  
  GNIxi  = darcy->ec_u->dNr1;
  GNIeta = darcy->ec_u->dNr2;
  dNudx  = darcy->ec_u->dNx1;
  dNudy  = darcy->ec_u->dNx2;
  NIp    = darcy->ec_p->N;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&vel_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Ype);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Xue);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      vel_el_lidx[2*k + 0] = 2 * nidx + 0;
      vel_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = vel_el_lidx[2*k];
      PetscInt nidx_v = vel_el_lidx[2*k+1];
      
      Xue[k          ] = Xu[nidx_u];
      Xue[k + nubasis] = Xu[nidx_v];
    }
    ux = &Xue[0];
    uy = &Xue[nubasis];
    
    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get p dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,GNIxi,GNIeta,dNudx,dNudy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Ype, sizeof(PetscScalar)* ( npbasis ) );
    
    for (q=0; q<nqp; q++) {
      fac = -WEIGHT[q] * detJ[q];
      LFormMF__q_div_u(nubasis,npbasis,fac,ux,uy,dNudx[q],dNudy[q],NIp[q],Ype);
    }
    
    for (k=0; k<npbasis; k++) {
      Yp[ p_el_lidx[k] ] += Ype[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"MatMultA21(MF): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(vel_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Xue);
  PetscFree(Ype);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MFDarcyL2H1_A21"
PetscErrorCode MFDarcyL2H1_A21(PDEDarcy *darcy,DM dau,DM dap,PetscScalar Xu[],PetscScalar Yp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Xue,*Ype,*ux,*uy;
  PetscInt  *vel_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**GNIxi,**GNIeta,**NIu;
  PetscReal *detJ,**dNpdx,**dNpdy;
  PetscReal fac;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = darcy->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  
  NIu    = darcy->ec_u->N;
  GNIxi  = darcy->ec_p->dNr1;
  GNIeta = darcy->ec_p->dNr2;
  dNpdx  = darcy->ec_p->dNx1;
  dNpdy  = darcy->ec_p->dNx2;
  
  nubasis = darcy->nubasis;
  npbasis = darcy->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&vel_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Ype);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Xue);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,&coords);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      vel_el_lidx[2*k + 0] = 2 * nidx + 0;
      vel_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = vel_el_lidx[2*k];
      PetscInt nidx_v = vel_el_lidx[2*k+1];
      
      Xue[k          ] = Xu[nidx_u];
      Xue[k + nubasis] = Xu[nidx_v];
    }
    ux = &Xue[0];
    uy = &Xue[nubasis];
    
    /* get p dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];

    /* get coordinates */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,npbasis,elcoords,GNIxi,GNIeta,dNpdx,dNpdy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Ype, sizeof(PetscScalar)* ( npbasis ) );
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      LFormMF__inner_grad_q_u(nubasis,npbasis,fac,ux,uy,NIu[q],dNpdx[q],dNpdy[q],Ype);
    }
    
    for (k=0; k<npbasis; k++) {
      Yp[ p_el_lidx[k] ] += Ype[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"MatMultA21(MF): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(vel_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Xue);
  PetscFree(Ype);
  
  PetscFunctionReturn(0);
}

/*
 IN:  X - a velocity vector
 OUT: Y - a pressure vector
 */
#undef __FUNCT__
#define __FUNCT__ "MatMult_MFDarcy_A21"
PetscErrorCode MatMult_MFDarcy_A21(Mat A,Vec X,Vec Y)
{
  PDE               pde;
  PDEDarcy          *darcy;
  PetscErrorCode    ierr;
  DM                dau,dap;
  Vec               XUloc,YPloc;
  PetscScalar       *LA_XUloc;
  PetscScalar       *LA_YPloc;
  
  PetscFunctionBegin;
  
  ierr = MatShellGetContext(A,(void**)&pde);CHKERRQ(ierr);
  darcy = (PDEDarcy*)pde->data;
  
  dau  = darcy->dmu;
  dap  = darcy->dmp;
  
  ierr = DMGetLocalVector(dau,&XUloc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dap,&YPloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMGlobalToLocalBegin(dau,X,INSERT_VALUES,XUloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dau,X,INSERT_VALUES,XUloc);CHKERRQ(ierr);
  
  /* Zero entries in local vectors corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out columns when the mat-mult is performed */
  if (darcy->u_dirichlet) {
    ierr = BCListInsertLocalZero(darcy->u_dirichlet,XUloc);CHKERRQ(ierr);
  }
  ierr = VecGetArray(XUloc,&LA_XUloc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(YPloc);CHKERRQ(ierr);
  ierr = VecGetArray(YPloc,&LA_YPloc);CHKERRQ(ierr);
  
  /* div */
  switch (darcy->bform_type) {
    case DARCY_BFORM_L2H1:
      ierr = MFDarcyL2H1_A21(darcy,dau,dap,LA_XUloc,LA_YPloc);CHKERRQ(ierr);
      break;
    case DARCY_BFORM_HDIVL2:
      ierr = MFDarcyHdivL2_A21(darcy,dau,dap,LA_XUloc,LA_YPloc);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown form type");
      break;
  }
  
  ierr = VecRestoreArray(YPloc,&LA_YPloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(XUloc,&LA_XUloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dap,YPloc,ADD_VALUES,Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (dap,YPloc,ADD_VALUES,Y);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dap,&YPloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dau,&XUloc);CHKERRQ(ierr);
  
  /* modify Y for the boundary conditions, y_k = 0, 0 is inserted as this is an off-diagonal operator */
  /* Clobbering entries in global vector corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out rows when the mat-mult is performed */
  /* if we have pressure boundary conditions */
  if (darcy->p_dirichlet) {
    ierr = BCListInsertZero(darcy->p_dirichlet,Y);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode MatMultAdd_basic(Mat A,Vec v1,Vec v2,Vec v3);

#undef __FUNCT__
#define __FUNCT__ "DarcyOperatorCreate_MFA12"
PetscErrorCode DarcyOperatorCreate_MFA12(PDE pde,Mat *A12)
{
  PDEDarcy *darcy = (PDEDarcy*)pde->data;
  Mat B;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatCreateShell(PETSC_COMM_WORLD,darcy->mu,darcy->mp,darcy->Mu,darcy->Mp,(void*)pde,&B);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_MFDarcy_A12);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT_ADD,(void(*)(void))MatMultAdd_basic);CHKERRQ(ierr);
  
  *A12 = B;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyOperatorCreate_MFA21"
PetscErrorCode DarcyOperatorCreate_MFA21(PDE pde,Mat *A21)
{
  PDEDarcy *darcy = (PDEDarcy*)pde->data;
  Mat B;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatCreateShell(PETSC_COMM_WORLD,darcy->mp,darcy->mu,darcy->Mp,darcy->Mu,(void*)pde,&B);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_MFDarcy_A21);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT_ADD,(void(*)(void))MatMultAdd_basic);CHKERRQ(ierr);
  
  *A21 = B;
  
  PetscFunctionReturn(0);
}


