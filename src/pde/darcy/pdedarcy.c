
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <petscutils.h>

#include <pdedarcyimpl.h>
#include <pdedarcy.h>

const char darcy_description[] =
"\"PDE Darcy\" solves the PDE \n"
"    a(x) u + grad(p) + gamma grad(div(u)) = f(x) \n"
"                                  -div(u) = g(x) \n"
"  [Assumptions] \n"
"  * Only DMTET is supported. \n"
"  [User notes] \n"
"  * The functions a(x), f(x),g(x) are defined via a Coefficient using point-wise defined \n"
"    quadrature point values. The quadrature fields are named \"a\", \"f\", \"g\" respectively. \n";


/* forms.c */
PetscErrorCode FormFunction_Darcy(SNES snes,Vec X,Vec F,void *ctx);
PetscErrorCode FormJacobian_Darcy(SNES snes,Vec X,Mat A,Mat B,void *ctx);
PetscErrorCode FormJacobian_DarcyHdivL2(SNES snes,Vec X,Mat A,Mat B,void *ctx);

/* mfspmv */
PetscErrorCode DarcyOperatorCreate_MFA12(PDE pde,Mat *A12);
PetscErrorCode DarcyOperatorCreate_MFA21(PDE pde,Mat *A21);
PetscErrorCode DMCreateMatNest_Darcy(DM dms,Mat *_A);


#undef __FUNCT__
#define __FUNCT__ "PDEConfigureSNES_Darcy"
PetscErrorCode PDEConfigureSNES_Darcy(PDE pde,SNES snes,Vec F,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PDEDarcy *data;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  
  ierr = SNESSetApplicationContext(snes,(void*)pde);CHKERRQ(ierr);
  
  ierr = SNESSetFunction(snes,F,pde->ops->form_function,(void*)pde);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes,A,B,pde->ops->form_jacobian,(void*)pde);CHKERRQ(ierr);

  if (!pde->x) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must call PDESetSolution() first");
  
  ierr = SNESSetSolution(snes,pde->x);CHKERRQ(ierr);
  
  if (!data->dmu || !data->dmp) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must provide a valid DM - Call PDEDarcySetData()");
  } else { /* check its a DMTET */
    PetscBool istet;
    
    istet = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)data->dmu,DMTET,&istet);CHKERRQ(ierr);
    if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for a velocity space defined via DMTET");
    
    istet = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)data->dmp,DMTET,&istet);CHKERRQ(ierr);
    if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for a pressure space defined via DMTET");
  }
  ierr = SNESSetDM(snes,data->dms);CHKERRQ(ierr);
  
  /* solver configuration */
  {
    KSP ksp;
    PC  pc;
    IS  *is_field;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    ierr = PCSetType(pc,PCFIELDSPLIT);CHKERRQ(ierr);
    ierr = PCFieldSplitSetType(pc,PC_COMPOSITE_SCHUR);CHKERRQ(ierr);

    ierr = DMCompositeGetGlobalISs(data->dms,&is_field);CHKERRQ(ierr);
    
    /*
    ierr = PCFieldSplitSetIS(pc,"u",is_field[0]);CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(pc,"p",is_field[1]);CHKERRQ(ierr);
    */
    ierr = PetscObjectSetName((PetscObject)data->dmu,"u");CHKERRQ(ierr); /* do not require this */
    ierr = DMSetOptionsPrefix(data->dmu,"u");CHKERRQ(ierr); /* require this for -stk_pc_fieldsplit_dm_splits */
    ierr = PetscObjectSetName((PetscObject)data->dmp,"p");CHKERRQ(ierr);
    ierr = DMSetOptionsPrefix(data->dmp,"p");CHKERRQ(ierr);
     
    if (data->bform_type == DARCY_BFORM_L2H1) {
      Mat Spp = NULL;
      
      ierr = DMCreateMatrix(data->dmp,&Spp);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject)Spp,"Spp");CHKERRQ(ierr);
      ierr = PCFieldSplitSetSchurPre(pc,PC_FIELDSPLIT_SCHUR_PRE_USER,Spp);CHKERRQ(ierr);
      ierr = MatDestroy(&Spp);CHKERRQ(ierr);
    }
    if (data->bform_type == DARCY_BFORM_HDIVL2) {
      Mat      Spp = NULL;
      Mat      Buu,Bup,Bpu;
      //Vec diag;
      PetscInt pk;
      DM       dm_p_dg,dm_p_cg;
      Mat      P;
      Mat      A12P,D,PtSP;
      PetscInt MS,MPtSP;
      
      
      /* Setup an empty matrix defined by A21.A12 */
      ierr = MatGetSubMatrix(A,is_field[0],is_field[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
      ierr = MatGetSubMatrix(A,is_field[0],is_field[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
      ierr = MatGetSubMatrix(A,is_field[1],is_field[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
      
      ierr = MatMatMult(Bpu,Bup,MAT_INITIAL_MATRIX,1.0,&Spp);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject)Spp,"Spp*_");CHKERRQ(ierr);
      
      ierr = MatScale(Spp,0.0);CHKERRQ(ierr);
      
      ierr = PCFieldSplitSetSchurPre(pc,PC_FIELDSPLIT_SCHUR_PRE_USER,Spp);CHKERRQ(ierr);
      
      // attach DMs and P
      dm_p_dg = data->dmp;
      
      pk = 1;
      ierr = PetscOptionsGetInt(NULL,NULL,"-aux_pk",&pk,NULL);CHKERRQ(ierr);
      ierr = DMTetCloneGeometry(dm_p_dg,1,DMTET_CWARP_AND_BLEND,pk,&dm_p_cg);CHKERRQ(ierr);
      
      ierr = PetscObjectCompose((PetscObject)Spp,"PCAuxSpace_DMNatural",(PetscObject)dm_p_dg);CHKERRQ(ierr);
      ierr = PetscObjectCompose((PetscObject)Spp,"PCAuxSpace_DMAux",(PetscObject)dm_p_cg);CHKERRQ(ierr);
      
      ierr = DMTetAssembleProjection(dm_p_cg,dm_p_dg,&P);CHKERRQ(ierr);
      ierr = PetscObjectCompose((PetscObject)Spp,"PCAuxSpace_P",(PetscObject)P);CHKERRQ(ierr);
      ierr = DMDestroy(&dm_p_cg);CHKERRQ(ierr);

      ierr = MatMatMult(Bup,P,MAT_INITIAL_MATRIX,1.0,&A12P);CHKERRQ(ierr);
      ierr = MatCreateDiagonal(Buu,MATAIJ,&D);CHKERRQ(ierr);
      ierr = MatPtAP(D,A12P,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);
      ierr = MatDestroy(&A12P);CHKERRQ(ierr);
      ierr = MatDestroy(&D);CHKERRQ(ierr);

      ierr = PetscObjectCompose((PetscObject)Spp,"PCAuxSpace_PtAP",(PetscObject)PtSP);CHKERRQ(ierr);

      ierr = MatGetSize(Bpu,&MS,NULL);CHKERRQ(ierr);
      ierr = MatGetSize(PtSP,&MPtSP,NULL);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"[fine] S %d x %d | [coarse] Pt.S.P %d x %d\n",MS,MS,MPtSP,MPtSP);
      
      ierr = MatDestroy(&Buu);CHKERRQ(ierr);
      ierr = MatDestroy(&Bup);CHKERRQ(ierr);
      ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
      
      ierr = MatDestroy(&PtSP);CHKERRQ(ierr);
      ierr = MatDestroy(&P);CHKERRQ(ierr);
      ierr = MatDestroy(&Spp);CHKERRQ(ierr);
    }
    
    ierr = ISDestroy(&is_field[0]);CHKERRQ(ierr);
    ierr = ISDestroy(&is_field[1]);CHKERRQ(ierr);
    ierr = PetscFree(is_field);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDestroy_Darcy"
PetscErrorCode PDEDestroy_Darcy(PDE pde)
{
  PetscErrorCode ierr;
  PDEDarcy *data;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;

  ierr = DMDestroy(&data->dms);CHKERRQ(ierr);
  ierr = DMDestroy(&data->dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&data->dmp);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->a);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->f);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->g);CHKERRQ(ierr);

  if (data->p_neumann) { ierr = CoefficientDestroy(&data->p_neumann);CHKERRQ(ierr); }
  if (data->u_neumann) { ierr = CoefficientDestroy(&data->u_neumann);CHKERRQ(ierr); }

  ierr = QuadratureDestroy(&data->ref);CHKERRQ(ierr);
  
  ierr = PetscFree(data);CHKERRQ(ierr);
  pde->data = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEUpdateLocal_Darcy"
PetscErrorCode PDEUpdateLocal_Darcy(PDE pde)
{
  PetscErrorCode ierr;
  PDEDarcy *data;
  Vec Uloc,Ploc;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMCompositeScatter(data->dms,pde->x,Uloc,Ploc);CHKERRQ(ierr);
  
  /* note - this shouldn't be necessary */
  /* insert boundary conditions into local vectors */
  //ierr = BCListInsertLocal(data->u_dirichlet,Uloc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreate_Darcy"
PetscErrorCode PDECreate_Darcy(PDE pde)
{
  PetscErrorCode ierr;
  PDEDarcy *data;
  MPI_Comm comm;
  
  PetscFunctionBegin;
  ierr = PetscObjectChangeTypeName((PetscObject)pde,PDETypeNames[(int)PDEDARCY]);CHKERRQ(ierr);
  ierr = PetscStrallocpy(darcy_description,&pde->description);CHKERRQ(ierr);
  
  ierr = PetscNewLog(pde,&data);CHKERRQ(ierr);
  data->dmu = NULL;
  data->dmp = NULL;
  data->dms = NULL;
  data->u_dirichlet = NULL;
  data->a = NULL;
  data->f = NULL;
  data->g = NULL;
  data->p_neumann = NULL;
  data->u_neumann = NULL;
  pde->data = data;
  
  pde->ops->configure_snes = PDEConfigureSNES_Darcy;
  pde->ops->form_function = FormFunction_Darcy;
  pde->ops->form_jacobian = FormJacobian_Darcy;
  pde->ops->destroy = PDEDestroy_Darcy;
  pde->ops->update_local_solution = PDEUpdateLocal_Darcy;
  
  /* create coefficients */
  ierr = PetscObjectGetComm((PetscObject)pde,&comm);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->a);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->f);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->g);CHKERRQ(ierr);
  
  ierr = CoefficientSetPDE(data->a,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->f,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->g,pde);CHKERRQ(ierr);
  
  ierr = CoefficientSetType(data->a,COEFF_QUADRATURE);CHKERRQ(ierr);
  ierr = CoefficientSetType(data->f,COEFF_QUADRATURE);CHKERRQ(ierr);
  ierr = CoefficientSetType(data->g,COEFF_QUADRATURE);CHKERRQ(ierr);
  
  ierr = CoefficientCreate(comm,&data->p_neumann);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->u_neumann);CHKERRQ(ierr);

  ierr = CoefficientSetPDE(data->p_neumann,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->u_neumann,pde);CHKERRQ(ierr);

  ierr = CoefficientSetType(data->p_neumann,COEFF_QUADRATURE);CHKERRQ(ierr);
  ierr = CoefficientSetType(data->u_neumann,COEFF_QUADRATURE);CHKERRQ(ierr);

  /* set default method to jam zeros into the quadrature values */
  ierr = CoefficientSetComputeQuadratureEmpty(data->p_neumann);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadratureEmpty(data->u_neumann);CHKERRQ(ierr);

  ierr = QuadratureCreate(&data->ref);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateMatNest_Darcy"
PetscErrorCode DMCreateMatNest_Darcy(DM dms,Mat *_A)
{
  Mat A,Auu,Aup=NULL,Apu=NULL,App,bA[2][2];
  IS *is;
  DM dmu,dmp;
  PetscInt i,j;
  PDE pde;
  PDEDarcy *data;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMGetApplicationContext(dms,(void**)&pde);CHKERRQ(ierr);
  data = (PDEDarcy*)pde->data;
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);

  ierr = DMCreateMatrix(dmu,&Auu);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmp,&App);CHKERRQ(ierr);
  ierr = DarcyOperatorCreate_MFA12(pde,&Aup);CHKERRQ(ierr);
  ierr = DarcyOperatorCreate_MFA21(pde,&Apu);CHKERRQ(ierr);

  ierr = PetscObjectSetName((PetscObject)Auu,"Auu");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)App,"App");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)Aup,"Aup-mf");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)Apu,"Apu-mf");CHKERRQ(ierr);

  if (data->bform_type == DARCY_BFORM_L2H1) { // L2 //
    Mat AA,BB;
    
    ierr = DMTetCreateMixedSpaceMatrix(dmu,dmp,&AA);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)AA,"Aup");CHKERRQ(ierr);
    ierr = MatDestroy(&Aup);CHKERRQ(ierr);
    Aup = AA;
    
    ierr = DMTetCreateMixedSpaceMatrix(dmp,dmu,&BB);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)BB,"Apu");CHKERRQ(ierr);
    ierr = MatDestroy(&Apu);CHKERRQ(ierr);
    Apu = BB;
  }

  if (data->bform_type == DARCY_BFORM_HDIVL2) { // Hdiv //
    Mat AA,BB;
    
    ierr = DMTetCreateMixedSpaceMatrix(dmu,dmp,&AA);CHKERRQ(ierr);
    ierr = DMTetCreateMixedSpaceMatrix(dmp,dmu,&BB);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)AA,"Aup");CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)BB,"Apu");CHKERRQ(ierr);
    
    ierr = MatDestroy(&Aup);CHKERRQ(ierr);
    Aup = AA;

    ierr = MatDestroy(&Apu);CHKERRQ(ierr);
    Apu = BB;
  }
  
  bA[0][0] = Auu; bA[0][1] = Aup;
  bA[1][0] = Apu; bA[1][1] = App;
  
  ierr = DMCompositeGetGlobalISs(dms,&is);CHKERRQ(ierr);
  
  ierr = MatCreateNest(PetscObjectComm((PetscObject)dms),2,is,2,is,&bA[0][0],&A);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* tidy up */
  for (i=0; i<2; i++) {
    for (j=0; j<2; j++) {
      if (bA[i][j]) { ierr = MatDestroy(&bA[i][j]);CHKERRQ(ierr); }
    }
  }
  ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
  ierr = PetscFree(is);CHKERRQ(ierr);

  *_A = A;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetUp_Darcy"
PetscErrorCode PDESetUp_Darcy(PDE pde)
{
  PDEDarcy *data;
  Vec X;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  
  ierr = DMCompositeCreate(PETSC_COMM_WORLD,&data->dms);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(data->dms,(void*)pde);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(data->dms,data->dmu);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(data->dms,data->dmp);CHKERRQ(ierr);
  ierr = DMSetUp(data->dms);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(data->dms,&X);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  
  {
    PetscInt mu,mp,Mu,Mp;
    Vec u,p;
    
    ierr = DMGetGlobalVector(data->dms,&X);CHKERRQ(ierr);
    ierr = DMCompositeGetAccess(data->dms,X,&u,&p);CHKERRQ(ierr);
    ierr = VecGetSize(u,&Mu);CHKERRQ(ierr);
    ierr = VecGetLocalSize(u,&mu);CHKERRQ(ierr);
    ierr = VecGetSize(p,&Mp);CHKERRQ(ierr);
    ierr = VecGetLocalSize(p,&mp);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(data->dms,X,&u,&p);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(data->dms,&X);CHKERRQ(ierr);
    
    data->mu = mu;
    data->mp = mp;
    data->Mu = Mu;
    data->Mp = Mp;
  }
  
  ierr = EContainerCreate(data->dmu,data->ref,&data->ec_u);CHKERRQ(ierr);
  ierr = EContainerCreate(data->dmp,data->ref,&data->ec_p);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(data->dmu,data->ref,&data->ec_g);CHKERRQ(ierr);
  data->nubasis = data->ec_u->nbasis;
  data->npbasis = data->ec_p->nbasis;
  
  data->dms->ops->creatematrix = DMCreateMatNest_Darcy;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcySetData"
PetscErrorCode PDEDarcySetData(PDE pde,DM dmu,DM dmp,BCList ubc,BCList pbc,DarcyBformType form_type)
{
  PDEDarcy *data;
  PetscBool istet;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;

  istet = PETSC_FALSE;
  ierr = PetscObjectTypeCompare((PetscObject)dmu,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for a velocity space defined via DMTET");
  
  istet = PETSC_FALSE;
  ierr = PetscObjectTypeCompare((PetscObject)dmp,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for a pressure space defined via DMTET");

  switch (form_type) {
    case DARCY_BFORM_L2H1:
      data->bform_type = DARCY_BFORM_L2H1;
    {
      DMTetBasisType basis_type;
      
      ierr = DMTetGetBasisType(dmu,&basis_type);CHKERRQ(ierr);
      if ((basis_type != DMTET_DG) && (basis_type != DMTET_CWARP_AND_BLEND)) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[FormType L2H1] Basis for velocity must be DMTET_DG or DMTET_CWARP_AND_BLEND");
      }
      
      ierr = DMTetGetBasisType(dmp,&basis_type);CHKERRQ(ierr);
      if (basis_type != DMTET_CWARP_AND_BLEND) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[FormType L2H1] Basis for velocity must be DMTET_CWARP_AND_BLEND");
      }
      
      if (ubc) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[FormType L2H1] Form does not support strong Dirichlet BCs for velocity");
    }
      break;
    case DARCY_BFORM_HDIVL2:
      data->bform_type = DARCY_BFORM_HDIVL2;
      pde->ops->form_jacobian = FormJacobian_DarcyHdivL2;
      
      if (pbc) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[FormType HdivL2] Form does not support strong Dirichlet BCs for pressure");
      break;
      
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Must provide a form_type value L2H1 = 0, HdivL2 = 1");
      break;
  }
  
  if (dmu) {
    ierr = PetscObjectReference((PetscObject)dmu);CHKERRQ(ierr);
    if (data->dmu) { ierr = DMDestroy(&data->dmu);CHKERRQ(ierr); }
    data->dmu = dmu;
  }
  if (dmp) {
    ierr = PetscObjectReference((PetscObject)dmp);CHKERRQ(ierr);
    if (data->dmp) { ierr = DMDestroy(&data->dmp);CHKERRQ(ierr); }
    data->dmp = dmp;
  }
  if (ubc) { data->u_dirichlet = ubc; }
  if (pbc) { data->p_dirichlet = pbc; }
  
  {
    Quadrature quadrature;
    PetscInt nelements;
    
    ierr = CoefficientGetQuadrature(data->a,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"a",1);CHKERRQ(ierr);
    
    ierr = CoefficientGetQuadrature(data->f,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"f",2);CHKERRQ(ierr);

    ierr = CoefficientGetQuadrature(data->g,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"g",1);CHKERRQ(ierr);
    
    {
      Quadrature quadrature;
      BFacetList bfacet_list;
      PetscInt   nfacets;
      
      /* Need to double check how QuadratureSetUpFromDM_Facet() use the dm passed in - it must support DG spaces */
      if (data->bform_type == DARCY_BFORM_HDIVL2) {
        ierr = CoefficientGetQuadrature(data->p_neumann,&quadrature);CHKERRQ(ierr);
        ierr = QuadratureSetUpFromDM_Facet(quadrature,dmu);CHKERRQ(ierr);
        ierr = DMTetGetGeometryBFacetList(dmu,&bfacet_list);CHKERRQ(ierr);
        ierr = BFacetListGetSizes(bfacet_list,&nfacets,NULL);CHKERRQ(ierr);
        ierr = QuadratureSetProperty(quadrature,nfacets,"pN",1);CHKERRQ(ierr);
      }
      if (data->bform_type == DARCY_BFORM_L2H1) {
        ierr = CoefficientGetQuadrature(data->u_neumann,&quadrature);CHKERRQ(ierr);
        ierr = QuadratureSetUpFromDM_Facet(quadrature,dmp);CHKERRQ(ierr);
        ierr = DMTetGetGeometryBFacetList(dmp,&bfacet_list);CHKERRQ(ierr);
        ierr = BFacetListGetSizes(bfacet_list,&nfacets,NULL);CHKERRQ(ierr);
        ierr = QuadratureSetProperty(quadrature,nfacets,"uN",2);CHKERRQ(ierr);
      }
    }

    quadrature = data->ref;
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
  }
  ierr = PDESetUp_Darcy(pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyL2H1SetData"
PetscErrorCode PDEDarcyL2H1SetData(PDE pde,DM dmu,DM dmp,BCList pbc)
{
  PetscErrorCode ierr;
  ierr = PDEDarcySetData(pde,dmu,dmp,NULL,pbc,(PetscInt)DARCY_BFORM_L2H1);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyHdivL2SetData"
PetscErrorCode PDEDarcyHdivL2SetData(PDE pde,DM dmu,DM dmp,BCList ubc)
{
  PetscErrorCode ierr;
  ierr = PDEDarcySetData(pde,dmu,dmp,ubc,NULL,(PetscInt)DARCY_BFORM_HDIVL2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreateDarcy"
PetscErrorCode PDECreateDarcy(MPI_Comm comm,DM dmu,DM dmp,BCList udirichlet,BCList pdirichlet,PetscInt form_type,PDE *pde)
{
  PDE            _pde;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PDECreate(comm,&_pde);CHKERRQ(ierr);
  ierr = PDESetType(_pde,PDEDARCY);CHKERRQ(ierr);
  ierr = PDEDarcySetData(_pde,dmu,dmp,udirichlet,pdirichlet,form_type);CHKERRQ(ierr);
  *pde = _pde;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyGetVCoefficients"
PetscErrorCode PDEDarcyGetVCoefficients(PDE pde,Coefficient *a,Coefficient *f,Coefficient *g)
{
  PDEDarcy *data;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  if (a) { *a = data->a; }
  if (f) { *f = data->f; }
  if (g) { *g = data->g; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyGetSCoefficients"
PetscErrorCode PDEDarcyGetSCoefficients(PDE pde,Coefficient *uN,Coefficient *pN)
{
  PDEDarcy *data;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  if (uN) { *uN = data->u_neumann; }
  if (pN) { *pN = data->p_neumann; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyGetDM"
PetscErrorCode PDEDarcyGetDM(PDE pde,DM *dms)
{
  PDEDarcy *data;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  if (dms) { *dms = data->dms; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyCreateOperator_MatNest"
PetscErrorCode PDEDarcyCreateOperator_MatNest(PDE pde,Mat *A)
{
  PDEDarcy       *data;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEDarcy*)pde->data;
  ierr = DMCreateMatNest_Darcy(data->dms,A);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode StokesUP_CG_AsciiVTU_g(DM dm,Vec field,const char filename[]);
PetscErrorCode StokesUP_DG_AsciiVTU_g(DM dm,Vec field,const char filename[]);
PetscErrorCode StokesP_CG_AsciiVTU(DM dm,Vec field,const char filename[]);

#undef __FUNCT__
#define __FUNCT__ "PDEDarcyFieldView_AsciiVTU"
PetscErrorCode PDEDarcyFieldView_AsciiVTU(PDE pde,const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  const char *option;
  PetscMPIInt rank;
  Vec X,velocity,pressure,Uloc,Ploc;
  DM dms,dmu,dmp;
  char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN];
  
  PetscFunctionBegin;
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)pde),&rank);CHKERRQ(ierr);
  ierr = PetscObjectGetOptionsPrefix((PetscObject)pde,&option);CHKERRQ(ierr);
  header[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (option) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",option);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  ierr = PDEDarcyGetDM(pde,&dms);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(dms,&Uloc,&Ploc);CHKERRQ(ierr);
  
  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
  
  ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"darcy_u-r%.4d.vtu",(int)rank);CHKERRQ(ierr);
  ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(filename,str);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmu,velocity,INSERT_VALUES,Uloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmu,velocity,INSERT_VALUES,Uloc);CHKERRQ(ierr);
  
  {
    DMTetBasisType btype;

    ierr = DMTetGetBasisType(dmu,&btype);CHKERRQ(ierr);
    if (btype == DMTET_DG) {
      ierr = StokesUP_DG_AsciiVTU_g(dmu,Uloc,filename);CHKERRQ(ierr);
    } else {
      ierr = StokesUP_CG_AsciiVTU_g(dmu,Uloc,filename);CHKERRQ(ierr);
    }
  }
  
  ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"darcy_p-r%.4d.vtu",(int)rank);CHKERRQ(ierr);
  ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(filename,str);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmp,pressure,INSERT_VALUES,Ploc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmp,pressure,INSERT_VALUES,Ploc);CHKERRQ(ierr);
  {
    PetscInt order;
    DMTetBasisType btype;
    
    ierr = DMTetGetBasisOrder(dmp,&order);CHKERRQ(ierr);
    ierr = DMTetGetBasisType(dmp,&btype);CHKERRQ(ierr);
    if (btype == DMTET_DG) {
      ierr = StokesUP_DG_AsciiVTU_g(dmp,Ploc,filename);CHKERRQ(ierr);
    } else {
      if (order == 1) {
        ierr = StokesP_CG_AsciiVTU(dmp,Ploc,filename);CHKERRQ(ierr);
      } else {
        ierr = StokesUP_CG_AsciiVTU_g(dmp,Ploc,filename);CHKERRQ(ierr);
      }
    }
  }
  
  ierr = DMCompositeRestoreAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(dms,&Uloc,&Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
