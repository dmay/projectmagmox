
#include <petsc.h>


PetscErrorCode LFormMF__inner_w_a_u(
    const PetscInt nubasis,
    const PetscReal factor,const PetscReal a_qp,
    const PetscReal Ux[],const PetscReal Uy[],
    const PetscReal Nu[],
    PetscReal Y[])
{
  PetscInt  iu;
  PetscReal u_qp[] = {0.0,0.0};
  
  // velocity at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    u_qp[0] += Ux[iu] * Nu[iu];
    u_qp[1] += Uy[iu] * Nu[iu];
  }
  
  u_qp[0] = factor * u_qp[0] * a_qp;
  u_qp[1] = factor * u_qp[1] * a_qp;
  
  // y = A11.u at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[idx]   += Nu[iu] * u_qp[0];
    Y[++idx] += Nu[iu] * u_qp[1];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode LFormMF__div_w_div_u(
    const PetscInt nubasis,
    const PetscReal factor,
    const PetscReal Ux[],const PetscReal Uy[],
    const PetscReal dNudx[],const PetscReal dNudy[],
    PetscReal Y[])
{
  PetscInt  iu;
  PetscReal divu_qp = 0.0;
  
  // divergence at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    divu_qp += Ux[iu] * dNudx[iu];
    divu_qp += Uy[iu] * dNudy[iu];
  }
  
  divu_qp = factor * divu_qp;
  divu_qp = factor * divu_qp;
  
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[idx]   += dNudx[iu] * divu_qp;
    Y[++idx] += dNudy[iu] * divu_qp;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode LFormMF__div_w_f(
    const PetscInt nubasis,
    const PetscReal factor,const PetscReal f_qp,
    const PetscReal dNudx[],const PetscReal dNudy[],
    PetscReal Y[])
{
  PetscInt  iu;
  
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[idx]   += dNudx[iu] * f_qp;
    Y[++idx] += dNudy[iu] * f_qp;
  }
  PetscFunctionReturn(0);
}

/* Hdiv-L2 */
PetscErrorCode LFormMF__div_w_p(
    const PetscInt nubasis,const PetscInt npbasis,
    const PetscReal alpha,
    const PetscReal P[],
    const PetscReal dNudx[],const PetscReal dNudy[],
    const PetscReal Np[],PetscReal Y[])
{
  PetscInt  iu,ip;
  PetscReal p_gp = 0.0,tmp;
  
  // pressure at quadrature point //
  for (ip=0; ip<npbasis; ip++) {
    p_gp += Np[ip] * P[ip];
  }
  
  // y = A12.p at quadrature point //
  tmp = alpha * p_gp;
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[  idx] += tmp * dNudx[iu];
    Y[++idx] += tmp * dNudy[iu];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode LFormMF__q_div_u(
    const PetscInt nubasis,const PetscInt npbasis,
    const PetscReal factor,
    const PetscReal Ux[],const PetscReal Uy[],
    const PetscReal dNudx[],const PetscReal dNudy[],const PetscReal Np[],
    PetscReal Y[])
{
  PetscInt  iu,ip;
  PetscReal strain_rate_gp[] = {0.0,0.0,0.0};
  PetscReal div_gp,tmp;
  
  // strain-rate (e_xx,e_yy,2.e_xy) at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    strain_rate_gp[0] += Ux[iu] * dNudx[iu];
    strain_rate_gp[1] += Uy[iu] * dNudy[iu];
  }
  
  // divergence at quadrature point //
  div_gp = strain_rate_gp[0] + strain_rate_gp[1];
  
  // y = A21.u at quadrature point //
  tmp = factor * div_gp;
  for (ip=0; ip<npbasis; ip++) {
    Y[ip] += tmp * Np[ip];
  }
  PetscFunctionReturn(0);
}

/* L2-H1 */
PetscErrorCode LFormMF__inner_w_grad_p(
    const PetscInt nubasis,const PetscInt npbasis,
    const PetscReal alpha,
    const PetscReal P[],
    const PetscReal Nw[],
    const PetscReal dNpdx[],const PetscReal dNpdy[],PetscReal Y[])
{
  PetscInt  iu,ip;
  PetscReal gradp_qp[] = {0.0,0.0};
  
  // pressure at quadrature point //
  for (ip=0; ip<npbasis; ip++) {
    gradp_qp[0] += dNpdx[ip] * P[ip];
    gradp_qp[1] += dNpdy[ip] * P[ip];
  }
  gradp_qp[0] = gradp_qp[0] * alpha;
  gradp_qp[1] = gradp_qp[1] * alpha;
  
  // y = A12.p at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[  idx] += Nw[iu] * gradp_qp[0];
    Y[++idx] += Nw[iu] * gradp_qp[1];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode LFormMF__inner_grad_q_u(
    const PetscInt nubasis,const PetscInt npbasis,
    const PetscReal alpha,
    const PetscReal Ux[],const PetscReal Uy[],
    const PetscReal Nu[],
    const PetscReal dNpdx[],const PetscReal dNpdy[],PetscReal Y[])
{
  PetscInt  iu,ip;
  PetscReal u_qp[] = {0.0,0.0};
  
  // velocity at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    u_qp[0] += Ux[iu] * Nu[iu];
    u_qp[1] += Uy[iu] * Nu[iu];
  }
  
  u_qp[0] = alpha * u_qp[0];
  u_qp[1] = alpha * u_qp[1];
  
  // y = A12.p at quadrature point //
  for (ip=0; ip<npbasis; ip++) {
    Y[ip] += dNpdx[ip] * u_qp[0] + dNpdy[ip] * u_qp[1];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode LFormMF__qp(
    const PetscInt npbasis,
    const PetscReal factor,
    const PetscReal P[],
    const PetscReal Np[],
    PetscReal Y[])
{
  PetscInt  ip;
  PetscReal p_qp = 0.0;
  
  // pressure at quadrature point //
  for (ip=0; ip<npbasis; ip++) {
    p_qp += P[ip] * Np[ip];
  }
  
  p_qp = factor * p_qp;
  
  // y = A11.u at quadrature point //
  for (ip=0; ip<npbasis; ip++) {
    Y[ip]   += Np[ip] * p_qp;
  }
  PetscFunctionReturn(0);
}
