
#include <petsc.h>
#include <fe_geometry_utils.h>

/*
 
 Note:
 - fe_geometry_utils.h contains the definitions of the functions.
 - All are decalared as inline
 - In this c file, force the compiler to create an external symbol for each inlined function
 
 See
  http://stackoverflow.com/questions/5229343/how-to-declare-an-inline-function-in-c99-multi-file-project
 for more information
 
*/

extern inline void EvaluateBasis_P1(PetscReal xi[],PetscReal N[]);
extern inline void EvaluateBasisDerivative_P1(PetscReal xi[],PetscReal GNxi[],PetscReal GNeta[]);
extern inline void EvaluateElementJacobian(PetscInt nbasis,
                                    PetscReal *__restrict el_coords,
                                    PetscReal *__restrict GNIxi,PetscReal *__restrict GNIeta,PetscReal J[2][2]);
extern inline void EvaluateElementGeometry(PetscInt nqp,PetscInt nbasis,
                                    PetscReal *__restrict el_coords,
                                    PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                    PetscReal *__restrict detJ);
extern inline void EvaluateBasisDerivatives(PetscInt nqp,PetscInt nbasis,
                                     PetscReal *__restrict el_coords,
                                     PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                     PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy);
extern inline void EvaluateBasisGeometryDerivatives(PetscInt nqp,PetscInt nbasis,
                                             PetscReal *__restrict el_coords,
                                             PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                             PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy,
                                             PetscReal *__restrict detJ);

extern inline void EvaluateBasisGeometry_Affine(PetscReal *__restrict el_vert,
                                         PetscReal *__restrict detJ);
extern inline void EvaluateBasisDerivatives_Affine(PetscInt nqp,PetscInt nbasis,
                                            PetscReal *__restrict el_vert,
                                            PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                            PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy);
extern inline void EvaluateBasisGeometryDerivatives_Affine(PetscInt nqp,PetscInt nbasis,
                                                    PetscReal *__restrict el_vert,
                                                    PetscReal *__restrict *__restrict GNIxi,PetscReal *__restrict *__restrict GNIeta,
                                                    PetscReal *__restrict *__restrict dNudx,PetscReal *__restrict *__restrict dNudy,
                                                    PetscReal *__restrict detJ);

extern inline void EvaluateGeometrySurfaceJacobian_Affine(PetscReal *__restrict el_vert_face,
                                                PetscReal *__restrict detJ);
extern inline void EvaluateBasisSurfaceJacobian_Affine(PetscInt nbasis_face,PetscReal *__restrict el_vert_face,
                                                       PetscReal *__restrict detJ);
