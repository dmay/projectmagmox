

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <quadrature.h>
#include <quadratureimpl.h>
#include <fe_geometry_utils.h>
#include <element_container.h>
#include <pde.h>

PetscLogEvent PDE_ProjectF;
PetscLogEvent PDE_ProjectQ;
PetscLogEvent PDE_ProjectRHS;
PetscLogEvent PDE_ProjectAssemble;
extern PetscLogStage stageLog_PDE_Project;


#undef __FUNCT__
#define __FUNCT__ "ProjectorCheckDMTetVecBlockSizeCompat"
PetscErrorCode ProjectorCheckDMTetVecBlockSizeCompat(DM dm,Vec x)
{
  PetscErrorCode ierr;
  PetscInt bs_dm,bs_vec;

  ierr = DMTetGetDof(dm,&bs_dm);CHKERRQ(ierr);
  ierr = VecGetBlockSize(x,&bs_vec);CHKERRQ(ierr);
  if (bs_dm != bs_vec) SETERRQ2(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Block size on the DM (%D) does not match block size on the Vec (%D)",bs_dm,bs_vec);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "ProjectorRHS_Native"
PetscErrorCode ProjectorRHS_Native(DM dmF,Vec xF,DM dmT,Vec xT,PetscInt rT)
{
  PetscErrorCode ierr;
  PetscInt order,orderF,orderT;
  PetscInt    e,nel,npe_F,npe_T,nbasis_F,nbasis_T,i,j,k,d;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *elnidx_F,*elnidx_T,*elbasismap_F,*elbasismap_T,*elbasismap_g;
  PetscInt    *idx_F,*idx_T;
  PetscReal   *coords_g,*lforme,*xFe;
  PetscInt    q,nqp;
  Quadrature  quadrature;
  EContainer  F_space,T_space;
  PetscReal   **NF,**NT;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         xTloc,xFloc;
  const PetscReal *LA_xF;
  PetscLogDouble t0,t1;
  
  ierr = PetscLogEventBegin(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);
  PetscTime(&t0);
  ierr = VecZeroEntries(xT);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dmF,&xFloc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmF,xF,INSERT_VALUES,xFloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmF,xF,INSERT_VALUES,xFloc);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dmT,&xTloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(xTloc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nel,&nbasis_F,&elbasismap_F,&npe_F,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nel,&nbasis_T,&elbasismap_T,&npe_T,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMTetGetBasisOrder(dmF,&orderF);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dmT,&orderT);CHKERRQ(ierr);

  /*
   Left hand side is 2 orderF
   Rhs is orderF + orderT
  */
  order = orderF + orderT;
  
  // quadrature //
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,order);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmF,quadrature,&F_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dmT,quadrature,&T_space);CHKERRQ(ierr);
  
  NF     = F_space->N;
  NT     = T_space->N;
  
  ierr = PetscMalloc1(rT*nbasis_F,&xFe);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&lforme);CHKERRQ(ierr);

  ierr = PetscMalloc1(rT*nbasis_F,&idx_F);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&idx_T);CHKERRQ(ierr);

  ierr = VecGetArrayRead(xFloc,&LA_xF);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_F = &elbasismap_F[nbasis_F*e];
    elnidx_T = &elbasismap_T[nbasis_T*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get from indices */
    for (k=0; k<nbasis_F; k++) {
      PetscInt nidx = elnidx_F[k];
      
      for (d=0; d<rT; d++) {
        idx_F[rT*k + d] = rT * nidx + d;
      }
    }

    /* get to indices */
    for (k=0; k<nbasis_T; k++) {
      PetscInt nidx = elnidx_T[k];
      
      for (d=0; d<rT; d++) {
        idx_T[rT*k + d] = rT * nidx + d;
      }
    }
    
    /* get from dofs */
    for (k=0; k<nbasis_F; k++) {
      PetscInt nidx = elnidx_F[k];
      
      for (d=0; d<rT; d++) {
        xFe[rT*k+d] = LA_xF[rT*nidx+d];
      }
    }
    
    /* compute derivatives */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);
    
    ierr = PetscMemzero(lforme,sizeof(PetscReal)*nbasis_T*rT);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      fac = detJ_affine * weight[q];
      
      /* compute xF at point */
      for (d=0; d<rT; d++) {
        PetscReal field,lform;
        
        field = 0.0;
        for (j=0; j<nbasis_F; j++) {
          field += NF[q][j] * xFe[rT*j + d];
        }
        
        lform = field;
        for (i=0; i<nbasis_T; i++) {
          lforme[rT*i+d] += fac * NT[q][i] * ( lform );
        }
      }
    }
    
    ierr = VecSetValues(xTloc,rT*nbasis_T,idx_T,lforme,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArrayRead(xFloc,&LA_xF);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(xTloc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(xTloc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  
  ierr = VecDestroy(&xTloc);CHKERRQ(ierr);
  ierr = VecDestroy(&xFloc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&F_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&T_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  
  ierr = PetscFree(xFe);CHKERRQ(ierr);
  ierr = PetscFree(lforme);CHKERRQ(ierr);
  ierr = PetscFree(idx_F);CHKERRQ(ierr);
  ierr = PetscFree(idx_T);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dmF,"AssembleLForm: CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 Computes
 grad(xF) = \partial xF_i / \partial x_j
*/
#undef __FUNCT__
#define __FUNCT__ "ProjectorRHS_Gradient"
PetscErrorCode ProjectorRHS_Gradient(DM dmF,Vec xF,PetscInt rF,DM dmT,Vec xT,PetscInt rT)
{
  PetscErrorCode ierr;
  PetscInt order,orderF,orderT;
  PetscInt    e,nel,npe_F,npe_T,nbasis_F,nbasis_T,i,j,k,d;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *elnidx_F,*elnidx_T,*elbasismap_F,*elbasismap_T,*elbasismap_g;
  PetscInt    *idx_F,*idx_T;
  PetscReal   *coords_g,*lforme,*xFe;
  PetscInt    q,nqp;
  Quadrature  quadrature;
  EContainer  F_space,T_space;
  PetscReal   **dNFxi,**dNFeta,**dNFx,**dNFy,**NT;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         xTloc,xFloc;
  const PetscReal *LA_xF;
  PetscLogDouble t0,t1;
  
  ierr = PetscLogEventBegin(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);
  PetscTime(&t0);
  ierr = VecZeroEntries(xT);CHKERRQ(ierr);
  
  ierr = DMTetSetDof(dmF,rF);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmF,&xFloc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmF,xF,INSERT_VALUES,xFloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmF,xF,INSERT_VALUES,xFloc);CHKERRQ(ierr);
  
  ierr = DMTetSetDof(dmT,rT);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmT,&xTloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(xTloc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nel,&nbasis_F,&elbasismap_F,&npe_F,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nel,&nbasis_T,&elbasismap_T,&npe_T,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMTetGetBasisOrder(dmF,&orderF);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dmT,&orderT);CHKERRQ(ierr);
  
  /*
   Left hand side is 2 orderF
   Rhs is (orderF-1) + orderT
   */
  order = (orderF-1) + orderT;
  if (order < 1) { order = 1; }
  
  // quadrature //
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,order);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmF,quadrature,&F_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dmT,quadrature,&T_space);CHKERRQ(ierr);
  
  dNFxi  = F_space->dNr1;
  dNFeta = F_space->dNr2;
  dNFx   = F_space->dNx1;
  dNFy   = F_space->dNx2;
  NT     = T_space->N;
  
  ierr = PetscMalloc1(rF*nbasis_F,&xFe);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&lforme);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(rF*nbasis_F,&idx_F);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&idx_T);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(xFloc,&LA_xF);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_F = &elbasismap_F[nbasis_F*e];
    elnidx_T = &elbasismap_T[nbasis_T*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get from indices */
    for (k=0; k<nbasis_F; k++) {
      PetscInt nidx = elnidx_F[k];
      
      for (d=0; d<rF; d++) {
        idx_F[rF*k + d] = rF * nidx + d;
      }
    }
    
    /* get to indices */
    for (k=0; k<nbasis_T; k++) {
      PetscInt nidx = elnidx_T[k];
      
      for (d=0; d<rT; d++) {
        idx_T[rT*k + d] = rT * nidx + d;
      }
    }
    
    /* get from dofs */
    for (k=0; k<nbasis_F; k++) {
      PetscInt nidx = elnidx_F[k];
      
      for (d=0; d<rF; d++) {
        xFe[rF*k+d] = LA_xF[rF*nidx+d];
      }
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_F,elcoords_g,dNFxi,dNFeta,dNFx,dNFy,&detJ_affine);
    
    ierr = PetscMemzero(lforme,sizeof(PetscReal)*nbasis_T*rT);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      fac = detJ_affine * weight[q];
      
      /* compute d(xF_i)/d(x_j) at point */
      for (d=0; d<rF; d++) {
        PetscReal grad_field[2];
        
        grad_field[0] = grad_field[1] = 0.0;
        for (j=0; j<nbasis_F; j++) {
          grad_field[0] += dNFx[q][j] * xFe[rF*j + d];
          grad_field[1] += dNFy[q][j] * xFe[rF*j + d];
        }
        
        for (i=0; i<nbasis_T; i++) {
          lforme[rT*i+2*d+0] += fac * NT[q][i] * ( grad_field[0] );
          lforme[rT*i+2*d+1] += fac * NT[q][i] * ( grad_field[1] );
        }
      }
    }
    
    ierr = VecSetValues(xTloc,rT*nbasis_T,idx_T,lforme,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArrayRead(xFloc,&LA_xF);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(xTloc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(xTloc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  
  ierr = VecDestroy(&xTloc);CHKERRQ(ierr);
  ierr = VecDestroy(&xFloc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&F_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&T_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  
  ierr = PetscFree(xFe);CHKERRQ(ierr);
  ierr = PetscFree(lforme);CHKERRQ(ierr);
  ierr = PetscFree(idx_F);CHKERRQ(ierr);
  ierr = PetscFree(idx_T);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dmF,"AssembleLForm: CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 Computes
 div(xF) = \sum_i \partial xF_i / \partial x_i
*/
#undef __FUNCT__
#define __FUNCT__ "ProjectorRHS_Divergence"
PetscErrorCode ProjectorRHS_Divergence(DM dmF,Vec xF,PetscInt rF,DM dmT,Vec xT,PetscInt rT)
{
  PetscErrorCode ierr;
  PetscInt order,orderF,orderT;
  PetscInt    e,nel,npe_F,npe_T,nbasis_F,nbasis_T,i,j,k,d;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *elnidx_F,*elnidx_T,*elbasismap_F,*elbasismap_T,*elbasismap_g;
  PetscInt    *idx_F,*idx_T;
  PetscReal   *coords_g,*lforme,*xFe;
  PetscInt    q,nqp;
  Quadrature  quadrature;
  EContainer  F_space,T_space;
  PetscReal   **dNFxi,**dNFeta,**dNFx,**dNFy,**NT;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         xTloc,xFloc;
  const PetscReal *LA_xF;
  PetscLogDouble t0,t1;
  
  ierr = PetscLogEventBegin(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);
  PetscTime(&t0);
  ierr = VecZeroEntries(xT);CHKERRQ(ierr);
  
  ierr = DMTetSetDof(dmF,rF);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmF,&xFloc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmF,xF,INSERT_VALUES,xFloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmF,xF,INSERT_VALUES,xFloc);CHKERRQ(ierr);
  
  ierr = DMTetSetDof(dmT,rT);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmT,&xTloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(xTloc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nel,&nbasis_F,&elbasismap_F,&npe_F,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nel,&nbasis_T,&elbasismap_T,&npe_T,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMTetGetBasisOrder(dmF,&orderF);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dmT,&orderT);CHKERRQ(ierr);
  
  /*
   Left hand side is 2 orderF
   Rhs is (orderF-1) + orderT
   */
  order = (orderF-1) + orderT;
  if (order < 1) { order = 1; }
  
  // quadrature //
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,order);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmF,quadrature,&F_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dmT,quadrature,&T_space);CHKERRQ(ierr);
  
  dNFxi  = F_space->dNr1;
  dNFeta = F_space->dNr2;
  dNFx   = F_space->dNx1;
  dNFy   = F_space->dNx2;
  NT     = T_space->N;
  
  ierr = PetscMalloc1(rF*nbasis_F,&xFe);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&lforme);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(rF*nbasis_F,&idx_F);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&idx_T);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(xFloc,&LA_xF);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_F = &elbasismap_F[nbasis_F*e];
    elnidx_T = &elbasismap_T[nbasis_T*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get from indices */
    for (k=0; k<nbasis_F; k++) {
      PetscInt nidx = elnidx_F[k];
      
      for (d=0; d<rF; d++) {
        idx_F[rF*k + d] = rF * nidx + d;
      }
    }
    
    /* get to indices */
    for (k=0; k<nbasis_T; k++) {
      PetscInt nidx = elnidx_T[k];
      
      for (d=0; d<rT; d++) {
        idx_T[rT*k + d] = rT * nidx + d;
      }
    }
    
    /* get from dofs */
    for (k=0; k<nbasis_F; k++) {
      PetscInt nidx = elnidx_F[k];
      
      for (d=0; d<rF; d++) {
        xFe[rF*k+d] = LA_xF[rF*nidx+d];
      }
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_F,elcoords_g,dNFxi,dNFeta,dNFx,dNFy,&detJ_affine);
    
    ierr = PetscMemzero(lforme,sizeof(PetscReal)*nbasis_T*rT);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal div_field;
      
      fac = detJ_affine * weight[q];
      
      /* compute sum_i d(xF_i)/d(x_i) at point */
      div_field = 0.0;
      for (j=0; j<nbasis_F; j++) {
        div_field += dNFx[q][j] * xFe[rF*j + 0];
        div_field += dNFy[q][j] * xFe[rF*j + 1];
      }
      
      for (i=0; i<nbasis_T; i++) {
        lforme[rT*i+0] += fac * NT[q][i] * ( div_field );
      }
    }
    
    ierr = VecSetValues(xTloc,rT*nbasis_T,idx_T,lforme,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArrayRead(xFloc,&LA_xF);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(xTloc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(xTloc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  
  ierr = VecDestroy(&xTloc);CHKERRQ(ierr);
  ierr = VecDestroy(&xFloc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&F_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&T_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  
  ierr = PetscFree(xFe);CHKERRQ(ierr);
  ierr = PetscFree(lforme);CHKERRQ(ierr);
  ierr = PetscFree(idx_F);CHKERRQ(ierr);
  ierr = PetscFree(idx_T);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dmF,"AssembleLForm: CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetProjectField"
PetscErrorCode DMTetProjectField(DM dmF,Vec vF,ProjectorType type,DM dmT,Vec *_vT)
{
  PetscErrorCode ierr;
  PetscInt bsF,bsF0,bsT,bsT0,nelF,nelT;
  Mat M;
  Vec vT,Ng;
  KSP ksp;
  PetscLogDouble t0,t1;
  PetscBool use_classic_method = PETSC_FALSE;
  
  ierr = PetscLogStagePush(stageLog_PDE_Project);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(PDE_ProjectF,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-project_field_use_classic",&use_classic_method,NULL);CHKERRQ(ierr);
  ierr = ProjectorCheckDMTetVecBlockSizeCompat(dmF,vF);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmF,&bsF);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmF,&bsF0);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmT,&bsT0);CHKERRQ(ierr);
  if (type == PRJTYPE_NATIVE) {
    bsT = bsF;
  } else if (type == PRJTYPE_GRADIENT) {
    if (bsF == 1) {
      bsT = 2;
    } else if (bsF == 2) {
      bsT = 4;
    } else SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Can only compute gradient of a scalar, or a vector field");
  } else if (type == PRJTYPE_DIVERGENCE) {
    if (bsF == 1) {
      SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Can only compute divergence of a vector field");
    } else if (bsF == 2) {
      bsT = 1;
    } else SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Can only compute divergence of a vector field");
  } else {
    SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Unknown projection type specified");
  }
  
  ierr = DMTetSpaceElement(dmF,&nelF,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nelT,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  if (nelF != nelT) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for compatible DMs with matching numbers of elements. DM_from #ele %D : DM_to #ele %D",nelF,nelT);
  
  //ierr = DMTetSetDof(dmT,bsT);CHKERRQ(ierr);
  if (bsT != bsT0) {
    SETERRQ2(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"DM(\"to\") DOF mismatch. Expected dmT has DOF %D - found %D",bsT,bsT0);
  }
  
  if (*_vT == NULL) {
    ierr = DMCreateGlobalVector(dmT,&vT);CHKERRQ(ierr);
  } else {
    PetscInt bs;
    
    vT = *_vT;
    /* if user created this vector - check the block size matches */
    ierr = VecGetBlockSize(vT,&bs);CHKERRQ(ierr);
    if (bs != bsT) SETERRQ(PetscObjectComm((PetscObject)vT),PETSC_ERR_SUP,"User created vector has incorrect block size");
  }
  
  ierr = VecDuplicate(vT,&Ng);CHKERRQ(ierr);

  /* form rhs */
  if (type == PRJTYPE_NATIVE) {
    ierr = ProjectorRHS_Native(dmF,vF,dmT,Ng,bsT);CHKERRQ(ierr);
  } else if (type == PRJTYPE_GRADIENT) {
    ierr = ProjectorRHS_Gradient(dmF,vF,bsF,dmT,Ng,bsT);CHKERRQ(ierr);
  } else if (type == PRJTYPE_DIVERGENCE) {
    ierr = ProjectorRHS_Divergence(dmF,vF,bsF,dmT,Ng,bsT);CHKERRQ(ierr);
  }
  
  //ierr = DMTetSetDof(dmT,bsT);CHKERRQ(ierr);
  if (use_classic_method) {
    ierr = AssembleBForm_VectorMassMatrix(dmT,bsT,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  } else {
    ierr = AssembleBForm_VectorMassMatrixCellWise(dmT,bsT,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  }
  
  ierr = KSPCreate(PetscObjectComm((PetscObject)dmF),&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"proj_f_");CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,M,M);CHKERRQ(ierr);
  PetscTime(&t0);
  ierr = KSPSolve(ksp,Ng,vT);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dmF,"KSPSolve(proj_f_): CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);

  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = VecDestroy(&Ng);CHKERRQ(ierr);
  ierr = MatDestroy(&M);CHKERRQ(ierr);
  
  *_vT = vT;
  
  /* reset the block size on the "to" DM */
  //ierr = DMTetSetDof(dmF,bsF0);CHKERRQ(ierr);
  //ierr = DMTetSetDof(dmT,bsT0);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(PDE_ProjectF,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ProjectorRHS_Quadrature"
PetscErrorCode ProjectorRHS_Quadrature(Quadrature quadrature,const char field[],DM dmT,Vec xT,PetscInt rT)
{
  PetscErrorCode ierr;
  PetscInt    e,nel,npe_T,nbasis_T,i,k,d;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *elnidx_T,*elbasismap_T,*elbasismap_g;
  PetscInt    *idx_T;
  PetscReal   *coords_g,*lforme;
  PetscInt    q,nqp;
  EContainer  T_space;
  PetscReal   **NT;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         xTloc;
  PetscReal   *q_data;
  PetscLogDouble t0,t1;
  
  ierr = PetscLogEventBegin(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);
  PetscTime(&t0);
  ierr = VecZeroEntries(xT);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dmT,&xTloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(xTloc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmT,&nel,&nbasis_T,&elbasismap_T,&npe_T,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  // quadrature //
  ierr = QuadratureGetProperty(quadrature,field,NULL,NULL,NULL,&q_data);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmT,quadrature,&T_space);CHKERRQ(ierr);
  
  NT     = T_space->N;
  
  ierr = PetscMalloc1(rT*nbasis_T,&lforme);CHKERRQ(ierr);
  ierr = PetscMalloc1(rT*nbasis_T,&idx_T);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_T = &elbasismap_T[nbasis_T*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get to indices */
    for (k=0; k<nbasis_T; k++) {
      PetscInt nidx = elnidx_T[k];
      
      for (d=0; d<rT; d++) {
        idx_T[rT*k + d] = rT * nidx + d;
      }
    }
    
    /* compute derivatives */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);
    
    ierr = PetscMemzero(lforme,sizeof(PetscReal)*nbasis_T*rT);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      fac = detJ_affine * weight[q];
      
      /* get xF at point */
      for (d=0; d<rT; d++) {
        PetscReal field_q;
        
        field_q = q_data[rT*(nqp*e + q) + d];
        for (i=0; i<nbasis_T; i++) {
          lforme[rT*i+d] += fac * NT[q][i] * ( field_q );
        }
      }
    }
    
    ierr = VecSetValues(xTloc,rT*nbasis_T,idx_T,lforme,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecAssemblyBegin(xTloc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(xTloc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmT,xTloc,ADD_VALUES,xT);CHKERRQ(ierr);
  
  ierr = VecDestroy(&xTloc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&T_space);CHKERRQ(ierr);
  
  ierr = PetscFree(lforme);CHKERRQ(ierr);
  ierr = PetscFree(idx_T);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dmT,"AssembleLForm: CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);
  
  {
    PetscReal *minmax,field_q;
    ierr = PetscMalloc1(rT*2,&minmax);CHKERRQ(ierr);
    
    for (d=0; d<rT; d++) {
      minmax[2*d  ] = PETSC_MAX_REAL; /* min / max */
      minmax[2*d+1] = PETSC_MIN_REAL;
    }
    for (q=0; q<nel*nqp; q++) {
      for (d=0; d<rT; d++) {
        field_q = q_data[rT*q + d];

        minmax[2*d  ] = PetscMin(minmax[2*d  ],field_q);
        minmax[2*d+1] = PetscMax(minmax[2*d+1],field_q);
      }
    }
    for (d=0; d<rT; d++) {
      PetscPrintf(PetscObjectComm((PetscObject)dmT),"quadratureField[\"%s\"][%.2D]: min %+1.8e : max %+1.8e\n",field,d,minmax[2*d],minmax[2*d+1]);
    }
    ierr = PetscFree(minmax);CHKERRQ(ierr);
  }
  ierr = PetscLogEventEnd(PDE_ProjectRHS,0,0,0,0);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetProjectQuadratureField"
PetscErrorCode DMTetProjectQuadratureField(Quadrature quadrature,const char field[],DM dmT,Vec *_vT)
{
  PetscErrorCode ierr;
  PetscInt bsT,bsT0;
  Mat M;
  Vec vT,Ng;
  KSP ksp;
  PetscReal *data;
  PetscLogDouble t0,t1;
  PetscBool use_classic_method = PETSC_FALSE;
  
  ierr = PetscLogStagePush(stageLog_PDE_Project);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(PDE_ProjectQ,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-project_field_use_classic",&use_classic_method,NULL);CHKERRQ(ierr);
  
  ierr = DMTetGetDof(dmT,&bsT0);CHKERRQ(ierr);

  // determine blocksizeTo from quadrature field num. components //
  ierr = QuadratureGetProperty(quadrature,field,NULL,NULL,&bsT,&data);CHKERRQ(ierr);
  
  if (bsT != bsT0) SETERRQ2(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"DM(\"to\") DOF mismatch. Expected dmT has DOF %D - found %D",bsT,bsT0);
  
  if (*_vT == NULL) {
    ierr = DMCreateGlobalVector(dmT,&vT);CHKERRQ(ierr);
  } else {
    PetscInt bs;
    
    vT = *_vT;
    /* if user created this vector - check the block size matches */
    ierr = VecGetBlockSize(vT,&bs);CHKERRQ(ierr);
    if (bs != bsT) SETERRQ(PetscObjectComm((PetscObject)vT),PETSC_ERR_SUP,"User created vector has incorrect block size");
  }
  
  ierr = VecDuplicate(vT,&Ng);CHKERRQ(ierr);
  
  /* form rhs */
  ierr = ProjectorRHS_Quadrature(quadrature,field,dmT,Ng,bsT);CHKERRQ(ierr);
  
  //ierr = DMTetSetDof(dmT,bsT);CHKERRQ(ierr);
  if (use_classic_method) {
    ierr = AssembleBForm_VectorMassMatrix(dmT,bsT,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  } else {
    ierr = AssembleBForm_VectorMassMatrixCellWise(dmT,bsT,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  }
  
  ierr = KSPCreate(PetscObjectComm((PetscObject)dmT),&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"proj_q_");CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,M,M);CHKERRQ(ierr);
  PetscTime(&t0);
  ierr = KSPSolve(ksp,Ng,vT);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dmT,"KSPSolve(proj_q_): CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);

  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = VecDestroy(&Ng);CHKERRQ(ierr);
  ierr = MatDestroy(&M);CHKERRQ(ierr);

  {
    PetscReal minmax[2];
    PetscInt b;
    
    for (b=0; b<bsT; b++) {
      ierr = VecStrideMin(vT,b,NULL,&minmax[0]);CHKERRQ(ierr);
      ierr = VecStrideMax(vT,b,NULL,&minmax[1]);CHKERRQ(ierr);
      
      PetscPrintf(PetscObjectComm((PetscObject)dmT),"projQuadratureField[\"%s\"][%.2D]: min %+1.8e : max %+1.8e\n",field,b,minmax[0],minmax[1]);
    }
  }
  *_vT = vT;
  ierr = PetscLogEventEnd(PDE_ProjectQ,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleBForm_VectorMixedMassMatrix"
PetscErrorCode AssembleBForm_VectorMixedMassMatrix(DM dm_w,DM dm_u,PetscInt ndof,Mat A)
{
  PetscErrorCode ierr;
  PetscInt   e,nel,nbasis_w,nbasis_u,i,j,k,d;
  PetscReal  elcoords_g[2 * 3];
  PetscInt   *elnidx_w,*elnidx_u,*elbasismap_w,*elbasismap_u,*elbasismap_g,*eldofs_w,*eldofs_u;
  PetscReal  *coords_g,*Ae;
  PetscInt   q,nqp;
  Quadrature quadrature;
  EContainer w_space,u_space;
  PetscReal  **N_w,**N_u,*xi,*weight;
  PetscReal  detJ_affine;
  
  ierr = PetscLogEventBegin(PDE_ProjectAssemble,0,0,0,0);CHKERRQ(ierr);
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm_w,&nel,&nbasis_w,&elbasismap_w,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_u,&nel,&nbasis_u,&elbasismap_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm_w,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(quadrature,dm_w);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm_w,quadrature,&w_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dm_u,quadrature,&u_space);CHKERRQ(ierr);
  
  N_w = w_space->N;
  N_u = u_space->N;
  
  ierr = PetscMalloc1(nbasis_w*nbasis_u*ndof*ndof,&Ae);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis_w*ndof,&eldofs_w);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis_u*ndof,&eldofs_u);CHKERRQ(ierr);
  
  
  for (e=0; e<nel; e++) {
    /* get element -> basis map */
    elnidx_w = &elbasismap_w[nbasis_w*e];
    elnidx_u = &elbasismap_u[nbasis_u*e];
    
    /* get indices */
    for (k=0; k<nbasis_w; k++) {
      PetscInt nidx = elnidx_w[k];
      for (d=0; d<ndof; d++) {
        eldofs_w[ndof*k + d] = ndof*nidx + d;
      }
    }
    for (k=0; k<nbasis_u; k++) {
      PetscInt nidx = elnidx_u[k];
      for (d=0; d<ndof; d++) {
        eldofs_u[ndof*k + d] = ndof*nidx + d;
      }
    }
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nbasis_w*nbasis_u*ndof*ndof);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,*Ni,*Nj;
      
      Ni = N_w[q];
      Nj = N_u[q];
      fac = detJ_affine * weight[q];
      for (i=0; i<nbasis_w; i++) {
        for (j=0; j<nbasis_u; j++) {
          PetscReal Ae_ij;
          
          Ae_ij = fac * Ni[i] * Nj[j];
          
          for (d=0; d<ndof; d++) {
            PetscInt index;
            
            index = (ndof*i + d)*nbasis_u*ndof + (ndof*j + d);
            Ae[index] += Ae_ij;
          }
          
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(A,nbasis_w*ndof,eldofs_w,nbasis_u*ndof,eldofs_u,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = PetscFree(eldofs_w);CHKERRQ(ierr);
  ierr = PetscFree(eldofs_u);CHKERRQ(ierr);
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = EContainerDestroy(&w_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&u_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(PDE_ProjectAssemble,0,0,0,0);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleBForm_InverseVectorMassMatrix_DG"
PetscErrorCode AssembleBForm_InverseVectorMassMatrix_DG(DM dm,PetscInt ndof,Mat A)
{
  PetscErrorCode ierr;
  PetscInt   e,nel,npe,nbasis,i,j,k,d;
  PetscReal  elcoords_g[2 * 3];
  PetscInt   *elnidx,*elbasismap,*elbasismap_g,*eldofs;
  PetscReal  *coords_g,*Ae,*iMe;
  PetscInt   q,nqp;
  Quadrature quadrature;
  EContainer v_space;
  PetscReal  **Nu,*xi,*weight;
  PetscReal  detJ_affine;
  DMTetBasisType btype;
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_DG) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only support for assembling inverse mass matrix when using a DG space");
  
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&elbasismap,&npe,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(quadrature,dm);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&v_space);CHKERRQ(ierr);
  
  Nu = v_space->N;
  
  ierr = PetscMalloc1(nbasis*nbasis*ndof*ndof,&iMe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*nbasis*ndof*ndof,&Ae);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*ndof,&eldofs);CHKERRQ(ierr);
  
  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &elbasismap[nbasis*e];
    
    /* get indices */
    for (k=0; k<nbasis; k++) {
      PetscInt nidx = elnidx[k];
      
      for (d=0; d<ndof; d++) {
        eldofs[ndof*k + d] = ndof*nidx + d;
      }
    }
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);
    
    
    /* On the first element - compute the local inverse of the element wise mass matrix */
    if (e == 0) {
      Mat M,iM;
      PC pc;
      PetscScalar *LA_dense;
      
      ierr = PetscMemzero(iMe,sizeof(PetscReal)*nbasis*nbasis*ndof*ndof);CHKERRQ(ierr);
      for (q=0; q<nqp; q++) {
        PetscReal *Ni;
        
        Ni = Nu[q];
        for (i=0; i<nbasis; i++) {
          for (j=0; j<nbasis; j++) {
            PetscReal Ae_ij;
            
            Ae_ij = Ni[i] * Ni[j] * weight[q];
            
            for (d=0; d<ndof; d++) {
              PetscInt index;
              
              index = (ndof*i + d)*nbasis*ndof + (ndof*j + d);
              iMe[index] += Ae_ij;
            }
          }
        }
      } // quadrature
      
      /* compute explicit inverse */
      ierr = MatCreate(PETSC_COMM_SELF,&M);CHKERRQ(ierr);
      ierr = MatSetSizes(M,nbasis*ndof,nbasis*ndof,nbasis*ndof,nbasis*ndof);CHKERRQ(ierr);
      ierr = MatSetType(M,MATSEQDENSE);CHKERRQ(ierr);
      ierr = MatSeqDenseSetPreallocation(M,NULL);CHKERRQ(ierr);
      ierr = MatDenseGetArray(M,&LA_dense);CHKERRQ(ierr);
      for (i=0; i<nbasis*ndof * nbasis*ndof; i++) {
        LA_dense[i] = iMe[i];
      }
      ierr = MatDenseRestoreArray(M,&LA_dense);
      ierr = MatAssemblyBegin(M,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      ierr = MatAssemblyEnd(M,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      
      ierr = PCCreate(PETSC_COMM_SELF,&pc);CHKERRQ(ierr);
      ierr = PCSetOperators(pc,M,M);CHKERRQ(ierr);
      ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
      ierr = PCSetUp(pc);CHKERRQ(ierr);
      ierr = PCComputeExplicitOperator(pc,&iM);CHKERRQ(ierr);
      
      /* save */
      ierr = MatDenseGetArray(iM,&LA_dense);CHKERRQ(ierr);
      for (i=0; i<nbasis*ndof * nbasis*ndof; i++) {
        iMe[i] = LA_dense[i];
      }
      ierr = MatDenseRestoreArray(iM,&LA_dense);CHKERRQ(ierr);

      ierr = PCDestroy(&pc);CHKERRQ(ierr);
      ierr = MatDestroy(&M);CHKERRQ(ierr);
      ierr = MatDestroy(&iM);CHKERRQ(ierr);
    }
    
    for (i=0; i<nbasis*ndof * nbasis*ndof; i++) {
      Ae[i] = iMe[i] * (1.0/detJ_affine);
    }
    
    ierr = MatSetValuesLocal(A,nbasis*ndof,eldofs,nbasis*ndof,eldofs,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = PetscFree(eldofs);CHKERRQ(ierr);
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = PetscFree(iMe);CHKERRQ(ierr);
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 P = (\int u_to v_to dV)(\int u_to v_from dV)
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetAssembleProjection"
PetscErrorCode DMTetAssembleProjection(DM dmF,DM dmT,Mat *_P)
{
  PetscErrorCode ierr;
  PetscInt       bsF0,bsT0,nelF,nelT;
  DMTetBasisType btypeF,btypeT;
  Mat            mixed_M,natural_invM,P;
  
  ierr = PetscLogStagePush(stageLog_PDE_Project);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmF,&bsF0);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmT,&bsT0);CHKERRQ(ierr);
  if (bsT0 != bsF0) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Can only assemble between DMTETs defined with the space DOF");

  /* check the "to" DM is a DG space */
  ierr = DMTetGetBasisType(dmF,&btypeF);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(dmT,&btypeT);CHKERRQ(ierr);
  if (btypeT != DMTET_DG) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Only support for assembling projector when the \"to\" space is DG");
  
  ierr = DMTetSpaceElement(dmF,&nelF,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nelT,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  if (nelF != nelT) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for compatible DMs with matching numbers of elements. DM_from #ele %D : DM_to #ele %D",nelF,nelT);
  
  /* For DG spaces, one optimization is to fuse these operations into single method - this would avoid using MatMatMult() */
  /* assemble the mixed mass matrix */
  ierr = DMTetCreateMixedSpaceMatrix(dmT,dmF,&mixed_M);CHKERRQ(ierr);
  ierr = AssembleBForm_VectorMixedMassMatrix(dmT,dmF,bsF0,mixed_M);CHKERRQ(ierr);
  
  /* assemble the inverse of the natural mass matrix */
  ierr = DMCreateMatrix(dmT,&natural_invM);CHKERRQ(ierr);
  ierr = AssembleBForm_InverseVectorMassMatrix_DG(dmT,bsF0,natural_invM);CHKERRQ(ierr);
  
  ierr = MatMatMult(natural_invM,mixed_M,MAT_INITIAL_MATRIX,1.0,&P);CHKERRQ(ierr);
  *_P = P;

  ierr = MatDestroy(&mixed_M);CHKERRQ(ierr);
  ierr = MatDestroy(&natural_invM);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

