
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <fe_geometry_utils.h>
#include <pdestokesimpl.h>

PetscErrorCode MatMult_MFStokes_A12(Mat A,Vec X,Vec Y);
PetscErrorCode MatMult_MFStokes_A21(Mat A,Vec X,Vec Y);

#undef __FUNCT__
#define __FUNCT__ "MatMultAdd_basic"
PetscErrorCode MatMultAdd_basic(Mat A,Vec v1,Vec v2,Vec v3)
{
  PetscErrorCode ierr;
  PetscScalar *LA_v2,*LA_v3,*LA_tmp;
  PetscInt i,n;
  Vec tmp;
  
  PetscFunctionBegin;
  ierr = VecDuplicate(v2,&tmp);CHKERRQ(ierr);
  ierr = MatMult(A,v1,tmp);CHKERRQ(ierr);
  
  ierr = VecGetLocalSize(v2,&n);CHKERRQ(ierr);
  ierr = VecGetArray(v2,&LA_v2);CHKERRQ(ierr);
  ierr = VecGetArray(v3,&LA_v3);CHKERRQ(ierr);
  ierr = VecGetArray(tmp,&LA_tmp);CHKERRQ(ierr);
  for (i=0; i<n; i++) {
    LA_v3[i] = LA_tmp[i] + LA_v2[i];
  }
  ierr = VecRestoreArray(v3,&LA_v3);CHKERRQ(ierr);
  ierr = VecRestoreArray(v2,&LA_v2);CHKERRQ(ierr);
  ierr = VecRestoreArray(tmp,&LA_tmp);CHKERRQ(ierr);
  ierr = VecDestroy(&tmp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesOperatorCreate_MFA12"
PetscErrorCode StokesOperatorCreate_MFA12(PDE pde,Mat *A12)
{
  PDEStokes *Stk = (PDEStokes*)pde->data;
  Mat B;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatCreateShell(PETSC_COMM_WORLD,Stk->mu,Stk->mp,Stk->Mu,Stk->Mp,(void*)pde,&B);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_MFStokes_A12);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT_ADD,(void(*)(void))MatMultAdd_basic);CHKERRQ(ierr);
  //ierr = MatShellSetOperation(B,MATOP_DESTROY,(void(*)(void))MatDestroy_MatStokesMF);CHKERRQ(ierr);
  
  *A12 = B;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesOperatorCreate_MFA21"
PetscErrorCode StokesOperatorCreate_MFA21(PDE pde,Mat *A21)
{
  PDEStokes *Stk = (PDEStokes*)pde->data;
  Mat B;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatCreateShell(PETSC_COMM_WORLD,Stk->mp,Stk->mu,Stk->Mp,Stk->Mu,(void*)pde,&B);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_MFStokes_A21);CHKERRQ(ierr);
  ierr = MatShellSetOperation(B,MATOP_MULT_ADD,(void(*)(void))MatMultAdd_basic);CHKERRQ(ierr);
  //ierr = MatShellSetOperation(B,MATOP_DESTROY,(void(*)(void))MatDestroy_MatStokesMF);CHKERRQ(ierr);
  
  *A21 = B;
  
  PetscFunctionReturn(0);
}
