
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <fe_geometry_utils.h>
#include <pdestokesimpl.h>


//#define LOG_MF_OP

/* div(u) */

PetscErrorCode MatMultMF_Stokes2d_B21(const PetscInt nubasis,const PetscInt npbasis,
                                      const PetscReal fac,
                                      const PetscReal Ux[],const PetscReal Uy[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],const PetscReal Np[],
                                      PetscReal Y[]);


#undef __FUNCT__
#define __FUNCT__ "MFStokesWrapper_A21"
PetscErrorCode MFStokesWrapper_A21(PDEStokes *stokes,DM dau,DM dap,PetscScalar Xu[],PetscScalar Yp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Xue,*Ype,*ux,*uy;
  PetscInt  *vel_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**GNIxi,**NGIeta,**NIp;
  PetscReal *detJ,**dNudx,**dNudy;
  PetscReal fac;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = stokes->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  
  GNIxi  = stokes->dNudxi;
  NGIeta = stokes->dNudeta;
  dNudx  = stokes->dNudx;
  dNudy  = stokes->dNudy;
  NIp    = stokes->Np;
  
  nubasis = stokes->nubasis;
  npbasis = stokes->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&vel_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Ype);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Xue);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nen_u*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      vel_el_lidx[2*k + 0] = 2 * nidx + 0;
      vel_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = vel_el_lidx[2*k];
      PetscInt nidx_v = vel_el_lidx[2*k+1];
      
      Xue[k          ] = Xu[nidx_u];
      Xue[k + nubasis] = Xu[nidx_v];
    }
    ux = &Xue[0];
    uy = &Xue[nubasis];
    
    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get p dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,GNIxi,NGIeta,dNudx,dNudy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Ype, sizeof(PetscScalar)* ( npbasis ) );
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      MatMultMF_Stokes2d_B21(nubasis,npbasis,fac,ux,uy,dNudx[q],dNudy[q],NIp[q],Ype);
    }
    
    for (k=0; k<npbasis; k++) {
      Yp[ p_el_lidx[k] ] += Ype[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"MatMultA21(MF): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(vel_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Xue);
  PetscFree(Ype);
  
  PetscFunctionReturn(0);
}

/*
 IN:  X - a velocity vector
 OUT: Y - a pressure vector
 */
#undef __FUNCT__
#define __FUNCT__ "MatMult_MFStokes_A21"
PetscErrorCode MatMult_MFStokes_A21(Mat A,Vec X,Vec Y)
{
  PDE               pde;
  PDEStokes         *stokes;
  PetscErrorCode    ierr;
  DM                dau,dap;
  Vec               XUloc,YPloc;
  PetscScalar       *LA_XUloc;
  PetscScalar       *LA_YPloc;
  
  PetscFunctionBegin;
  
  ierr = MatShellGetContext(A,(void**)&pde);CHKERRQ(ierr);
  stokes = (PDEStokes*)pde->data;
  
  dau  = stokes->dmu;
  dap  = stokes->dmp;
  
  ierr = DMGetLocalVector(dau,&XUloc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dap,&YPloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMGlobalToLocalBegin(dau,X,INSERT_VALUES,XUloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dau,X,INSERT_VALUES,XUloc);CHKERRQ(ierr);
  
  /* Zero entries in local vectors corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out columns when the mat-mult is performed */
  ierr = BCListInsertLocalZero(stokes->u_dirichlet,XUloc);CHKERRQ(ierr);
  
  ierr = VecGetArray(XUloc,&LA_XUloc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(YPloc);CHKERRQ(ierr);
  ierr = VecGetArray(YPloc,&LA_YPloc);CHKERRQ(ierr);
  
  /* div */
  ierr = MFStokesWrapper_A21(stokes,dau,dap,LA_XUloc,LA_YPloc);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(YPloc,&LA_YPloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(XUloc,&LA_XUloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dap,YPloc,ADD_VALUES,Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (dap,YPloc,ADD_VALUES,Y);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dap,&YPloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dau,&XUloc);CHKERRQ(ierr);
  
  /* modify Y for the boundary conditions, y_k = 0, 0 is inserted as this is an off-diagonal operator */
  /* Clobbering entries in global vector corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out rows when the mat-mult is performed */
  /* if we have pressure boundary conditions */
  /*
   ierr = BCListInsertZero(stokes->u_dirichlet,Y);CHKERRQ(ierr);
   */
  
  PetscFunctionReturn(0);
}
