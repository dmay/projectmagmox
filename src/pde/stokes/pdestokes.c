
/*
 
 
*/

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <dmbcs.h>

#include <pdestokesimpl.h>

const char stokes_description[] =
"\"PDE Stokes\" solves the PDE \n"
"    div ( 2 eta symgrad(u) ) - grad(p) = f(x) \n"
"                              - div(u) = g(x) \n"
"  [Assumptions] \n"
"  * Only DMTET is supported. \n"
"  [User notes] \n"
"  * The function f(x),g(x) are defined via a Coefficient using point-wise defined \n"
"    quadrature point values. The quadrature fields are named \"fu\", \"fp\" respectively. \n"
"  * The coefficient eta is defined point-wise.";


/* residual.c */
PetscErrorCode FormFunction_Stokes(SNES snes,Vec X,Vec F,void *ctx);
/* bilinear_forms.c */
PetscErrorCode FormJacobian_Stokes(SNES snes,Vec X,Mat A,Mat B,void *ctx);

/* operators.c */
PetscErrorCode StokesOperatorCreate_MFA12(PDE pde,Mat *A12);
PetscErrorCode StokesOperatorCreate_MFA21(PDE pde,Mat *A21);

#undef __FUNCT__
#define __FUNCT__ "PDEConfigureSNES_Stokes"
PetscErrorCode PDEConfigureSNES_Stokes(PDE pde,SNES snes,Vec F,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PDEStokes *data;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  
  ierr = SNESSetApplicationContext(snes,(void*)pde);CHKERRQ(ierr);
  //ierr = PetscObjectReference((PetscObject)pde);CHKERRQ(ierr);
  
  ierr = SNESSetFunction(snes,F,pde->ops->form_function,(void*)pde);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes,A,B,pde->ops->form_jacobian,(void*)pde);CHKERRQ(ierr);

  if (!pde->x) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must call PDESetSolution() first");
  }
  ierr = SNESSetSolution(snes,pde->x);CHKERRQ(ierr);
  
  if (!data->dmu || !data->dmp) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Must provide a valid DM - Call PDEStokesSetData()");
  } else { /* check its a DMTET */
    PetscBool istet;
    
    istet = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)data->dmu,DMTET,&istet);CHKERRQ(ierr);
    if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for a velocity space defined via DMTET");
    
    istet = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)data->dmp,DMTET,&istet);CHKERRQ(ierr);
    if (!istet) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Only valid for a pressure space defined via DMTET");
  }
  ierr = SNESSetDM(snes,data->dms);CHKERRQ(ierr);
  
  /* solver configuration */
  {
    KSP ksp;
    PC pc;
    Mat Spp;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    ierr = PCSetType(pc,PCFIELDSPLIT);CHKERRQ(ierr);
    ierr = PCFieldSplitSetType(pc,PC_COMPOSITE_SCHUR);CHKERRQ(ierr);

    ierr = DMCreateMatrix(data->dmp,&Spp);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)Spp,"Spp");CHKERRQ(ierr);
    ierr = PCFieldSplitSetSchurPre(pc,PC_FIELDSPLIT_SCHUR_PRE_USER,Spp);CHKERRQ(ierr);

    ierr = PetscObjectSetName((PetscObject)data->dmu,"u");CHKERRQ(ierr); /* do not require this */
    ierr = DMSetOptionsPrefix(data->dmu,"u");CHKERRQ(ierr); /* require this for -stk_pc_fieldsplit_dm_splits */
    ierr = PetscObjectSetName((PetscObject)data->dmp,"p");CHKERRQ(ierr);
    ierr = DMSetOptionsPrefix(data->dmp,"p");CHKERRQ(ierr);
    
    /*
    {
      IS *is_stokes_field;
      
      ierr = DMCompositeGetGlobalISs(data->dms,&is_stokes_field);CHKERRQ(ierr);
      ierr = PCFieldSplitSetIS(pc,"u",is_stokes_field[0]);CHKERRQ(ierr);
      ierr = PCFieldSplitSetIS(pc,"p",is_stokes_field[1]);CHKERRQ(ierr);
      ierr = ISDestroy(&is_stokes_field[0]);CHKERRQ(ierr);
      ierr = ISDestroy(&is_stokes_field[1]);CHKERRQ(ierr);
      ierr = PetscFree(is_stokes_field);CHKERRQ(ierr);
    }
    */
    
    ierr = MatDestroy(&Spp);CHKERRQ(ierr);
  }
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDestroy_Stokes"
PetscErrorCode PDEDestroy_Stokes(PDE pde)
{
  PetscErrorCode ierr;
  PDEStokes *data;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;

  ierr = DMDestroy(&data->dms);CHKERRQ(ierr);
  ierr = DMDestroy(&data->dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&data->dmp);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->eta);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->fu);CHKERRQ(ierr);
  ierr = CoefficientDestroy(&data->fp);CHKERRQ(ierr);
  if (data->q_N) {
    ierr = CoefficientDestroy(&data->q_N);CHKERRQ(ierr);
  }
  ierr = QuadratureDestroy(&data->ref);CHKERRQ(ierr);
  
  ierr = PetscFree(data);CHKERRQ(ierr);
  pde->data = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEUpdateLocal_Stokes"
PetscErrorCode PDEUpdateLocal_Stokes(PDE pde)
{
  PetscErrorCode ierr;
  PDEStokes *data;
  Vec Uloc,Ploc;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMCompositeScatter(data->dms,pde->x,Uloc,Ploc);CHKERRQ(ierr);
  
  /* note - this shouldn't be necessary */
  /* insert boundary conditions into local vectors */
  //ierr = BCListInsertLocal(data->u_dirichlet,Uloc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreate_Stokes"
PetscErrorCode PDECreate_Stokes(PDE pde)
{
  PetscErrorCode ierr;
  PDEStokes *data;
  MPI_Comm comm;
  
  PetscFunctionBegin;
  ierr = PetscObjectChangeTypeName((PetscObject)pde,PDETypeNames[(int)PDESTOKES]);CHKERRQ(ierr);
  ierr = PetscStrallocpy(stokes_description,&pde->description);CHKERRQ(ierr);
  
  ierr = PetscNewLog(pde,&data);CHKERRQ(ierr);
  data->dmu = NULL;
  data->dmp = NULL;
  data->u_dirichlet = NULL;
  pde->data = data;
  
  pde->ops->configure_snes = PDEConfigureSNES_Stokes;
  pde->ops->form_function = FormFunction_Stokes;
  pde->ops->form_jacobian = FormJacobian_Stokes;
  pde->ops->destroy = PDEDestroy_Stokes;
  pde->ops->update_local_solution = PDEUpdateLocal_Stokes;
  
  /* create coefficients */
  ierr = PetscObjectGetComm((PetscObject)pde,&comm);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->eta);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->fu);CHKERRQ(ierr);
  ierr = CoefficientCreate(comm,&data->fp);CHKERRQ(ierr);
  data->q_N = NULL; /* explictly set this to NULL as it makes no sense to even declare it for Stokes */
  
  ierr = CoefficientSetPDE(data->eta,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->fu,pde);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->fp,pde);CHKERRQ(ierr);
  
  ierr = CoefficientSetType(data->eta,COEFF_QUADRATURE);CHKERRQ(ierr);
  ierr = CoefficientSetType(data->fu,COEFF_QUADRATURE);CHKERRQ(ierr);
  ierr = CoefficientSetType(data->fp,COEFF_QUADRATURE);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&data->ref);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateMatNest_Stokes"
PetscErrorCode DMCreateMatNest_Stokes(DM dms,Mat *_A)
{
  Mat A,Auu,Aup=NULL,Apu=NULL,App,bA[2][2];
  IS *is;
  DM dmu,dmp;
  PetscInt i,j;
  PDE pde;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMGetApplicationContext(dms,(void**)&pde);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);

  ierr = DMCreateMatrix(dmu,&Auu);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmp,&App);CHKERRQ(ierr);
  ierr = StokesOperatorCreate_MFA12(pde,&Aup);CHKERRQ(ierr);
  ierr = StokesOperatorCreate_MFA21(pde,&Apu);CHKERRQ(ierr);
  
  ierr = PetscObjectSetName((PetscObject)Auu,"Auu");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)App,"App");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)Aup,"Aup-mf");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)Apu,"Apu-mf");CHKERRQ(ierr);
  
  {
    Mat AA;
    
    ierr = DMTetCreateMixedSpaceMatrix(dmu,dmp,&AA);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)AA,"Aup");CHKERRQ(ierr);
    ierr = MatDestroy(&Aup);CHKERRQ(ierr);
    Aup = AA;
    
    ierr = DMTetCreateMixedSpaceMatrix(dmp,dmu,&AA);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject)AA,"Apu");CHKERRQ(ierr);
    ierr = MatDestroy(&Apu);CHKERRQ(ierr);
    Apu = AA;
  }
  
  bA[0][0] = Auu; bA[0][1] = Aup;
  bA[1][0] = Apu; bA[1][1] = App;
  
  ierr = DMCompositeGetGlobalISs(dms,&is);CHKERRQ(ierr);
  
  ierr = MatCreateNest(PetscObjectComm((PetscObject)dms),2,is,2,is,&bA[0][0],&A);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* tidy up */
  for (i=0; i<2; i++) {
    for (j=0; j<2; j++) {
      if (bA[i][j]) { ierr = MatDestroy(&bA[i][j]);CHKERRQ(ierr); }
    }
  }
  ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
  ierr = PetscFree(is);CHKERRQ(ierr);

  *_A = A;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetUp_Stokes"
PetscErrorCode PDESetUp_Stokes(PDE pde)
{
  PDEStokes *data;
  Vec X;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  
  ierr = DMCompositeCreate(PETSC_COMM_WORLD,&data->dms);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(data->dms,(void*)pde);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(data->dms,data->dmu);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(data->dms,data->dmp);CHKERRQ(ierr);
  ierr = DMSetUp(data->dms);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(data->dms,&X);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  
  {
    PetscInt mu,mp,Mu,Mp;
    Vec u,p;
    
    ierr = DMGetGlobalVector(data->dms,&X);CHKERRQ(ierr);
    ierr = DMCompositeGetAccess(data->dms,X,&u,&p);CHKERRQ(ierr);
    ierr = VecGetSize(u,&Mu);CHKERRQ(ierr);
    ierr = VecGetLocalSize(u,&mu);CHKERRQ(ierr);
    ierr = VecGetSize(p,&Mp);CHKERRQ(ierr);
    ierr = VecGetLocalSize(p,&mp);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(data->dms,X,&u,&p);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(data->dms,&X);CHKERRQ(ierr);
    
    data->mu = mu;
    data->mp = mp;
    data->Mu = Mu;
    data->Mp = Mp;
  }
  
  data->dms->ops->creatematrix = DMCreateMatNest_Stokes;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesSetData"
PetscErrorCode PDEStokesSetData(PDE pde,DM dmu,DM dmp,BCList ubc)
{
  PDEStokes *data;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;

  if (dmu) {
    ierr = PetscObjectReference((PetscObject)dmu);CHKERRQ(ierr);
    if (data->dmu) {
      ierr = DMDestroy(&data->dmu);CHKERRQ(ierr);
    }
    data->dmu = dmu;
  }
  if (dmp) {
    ierr = PetscObjectReference((PetscObject)dmp);CHKERRQ(ierr);
    if (data->dmp) {
      ierr = DMDestroy(&data->dmp);CHKERRQ(ierr);
    }
    data->dmp = dmp;
  }
  if (ubc) {
    data->u_dirichlet = ubc;
  }
  
  {
    Quadrature quadrature;
    PetscInt nelements,nqp;
    PetscReal *xi;
    
    ierr = CoefficientGetQuadrature(data->eta,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"eta",1);CHKERRQ(ierr);
    
    ierr = CoefficientGetQuadrature(data->fu,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"fu",2);CHKERRQ(ierr);

    ierr = CoefficientGetQuadrature(data->fp,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"fp",1);CHKERRQ(ierr);
    
    quadrature = data->ref;
    ierr = QuadratureSetUpFromDM(quadrature,data->dmu);CHKERRQ(ierr);
    
    ierr = QuadratureGetRule(quadrature,&nqp,&xi,NULL);CHKERRQ(ierr);
    ierr = DMTetTabulateBasis(data->dmu,nqp,xi,&data->nubasis,&data->Nu);CHKERRQ(ierr);
    ierr = DMTetTabulateBasisDerivatives(data->dmu,nqp,xi,&data->nubasis,&data->dNudxi,&data->dNudeta,NULL);CHKERRQ(ierr);
    ierr = DMTetTabulateBasisDerivatives(data->dmu,nqp,xi,&data->nubasis,&data->dNudx,&data->dNudy,NULL);CHKERRQ(ierr);
    ierr = DMTetTabulateBasis(data->dmp,nqp,xi,&data->npbasis,&data->Np);CHKERRQ(ierr);
    
  }
  
  ierr = PDESetUp_Stokes(pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreateStokes"
PetscErrorCode PDECreateStokes(MPI_Comm comm,DM dmu,DM dmp,BCList udirichlet,PDE *pde)
{
  PDE _pde;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PDECreate(comm,&_pde);CHKERRQ(ierr);
  ierr = PDESetType(_pde,PDESTOKES);CHKERRQ(ierr);
  ierr = PDEStokesSetData(_pde,dmu,dmp,udirichlet);CHKERRQ(ierr);
  *pde = _pde;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesGetCoefficients"
PetscErrorCode PDEStokesGetCoefficients(PDE pde,Coefficient *eta,Coefficient *fu,Coefficient *fp)
{
  PDEStokes *data;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  if (eta) { *eta = data->eta; }
  if (fu) {   *fu = data->fu;  }
  if (fp) {   *fp = data->fp;  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesGetDM"
PetscErrorCode PDEStokesGetDM(PDE pde,DM *dms)
{
  PDEStokes *data;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  if (dms) { *dms = data->dms; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesGetBCLists"
PetscErrorCode PDEStokesGetBCLists(PDE pde,BCList *u,BCList *p)
{
  PDEStokes *data;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  if (u) { *u = data->u_dirichlet; }
  if (p) {
    PetscPrintf(PetscObjectComm((PetscObject)pde),"[warning] pressure dirichlet boundary returned will always be NULL\n");
    *p = NULL;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesCreateOperator_MatNest"
PetscErrorCode PDEStokesCreateOperator_MatNest(PDE pde,Mat *A)
{
  PDEStokes *data;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  ierr = DMCreateMatNest_Stokes(data->dms,A);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
