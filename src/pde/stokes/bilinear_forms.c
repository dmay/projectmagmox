
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <pdestokes.h>
#include <pdestokesimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <element_container.h>
#include <fe_geometry_utils.h>

//#define LOG_MF_OP

PetscErrorCode FormBilinearA12_PDEStokes(PDEStokes *darcy,Mat B);
PetscErrorCode FormBilinearA21_PDEStokes(PDEStokes *darcy,Mat B);

#undef __FUNCT__
#define __FUNCT__ "FormBilinearA11_PDEStokes"
PetscErrorCode FormBilinearA11_PDEStokes(PDEStokes *stokes,Mat A11,Mat B11)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj,kk;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *coords,*elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal *detJ,**dNuxi,**dNueta,**dNux,**dNuy,*weight,*coeff_eta,*coeff_eta_e,diagD[3];
  IS ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM dm = NULL;
  EContainer c;
  Quadrature quadrature;
  PetscReal **B;
  PetscLogDouble t0,t1;

  
  PetscFunctionBegin;
  dm = stokes->dmu;
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&nbasis,&NUM_GINDICES,&coords);CHKERRQ(ierr);

  bc = stokes->u_dirichlet;
  
  ierr = CoefficientGetQuadrature(stokes->eta,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&coeff_eta);CHKERRQ(ierr);

  ierr = EContainerCreate(dm,quadrature,&c);CHKERRQ(ierr);
  dNuxi  = c->dNr1;
  dNueta = c->dNr2;
  dNux   = c->dNx1;
  dNuy   = c->dNx2;
  
  detJ     = c->buf_q_scalar_a;
  eldofs   = c->buf_basis_2index_a;
  elcoords = c->buf_basis_2vector_a;

  ierr = PetscMalloc1(2*2*nbasis*nbasis,&Ke);CHKERRQ(ierr);

  ierr = PetscMalloc1(3,&B);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[0]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[1]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[2]);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B11),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  
  ierr = MatZeroEntries(B11);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    
    /* get element -> node map */
    elnidx = &element[nbasis*e];

    /* generate masked dof indices */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = elnidx[i];
      
      eldofs[2*i+0] = GINDICES[2*nidx];
      eldofs[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = elnidx[i];
      
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nbasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);

    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis*2*2);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_eta,&coeff_eta_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *dNdx_q,*dNdy_q;
      
      /* get access to element->quadrature points */
      dNdx_q = dNux[q];
      dNdy_q = dNuy[q];

      fac = weight[q] * detJ[q];

      for (i=0; i<nbasis; i++) {
        PetscReal d_dx_i = dNdx_q[i];
        PetscReal d_dy_i = dNdy_q[i];

        B[0][2*i  ] = d_dx_i; B[0][2*i+1] = 0.0;
        B[1][2*i  ] = 0.0;    B[1][2*i+1] = d_dy_i;
        B[2][2*i  ] = d_dy_i; B[2][2*i+1] = d_dx_i;
      }

#if 1
      diagD[0] = 2.0 * fac * coeff_eta_e[q];
      diagD[1] = 2.0 * fac * coeff_eta_e[q];
      diagD[2] = fac * coeff_eta_e[q];
      
      /* form Bt tildeD B */
      /*
       Ke_ij = Bt_ik . D_kl . B_lj
             = B_ki . D_kl . B_lj
             = B_ki . D_kk . B_kj
       */
      for (ii=0; ii<2*nbasis; ii++) {
        for (jj=ii; jj<2*nbasis; jj++) {
          for (kk=0; kk<3; kk++) {
            Ke[ii*(2*nbasis)+jj] += B[kk][ii]*diagD[kk]*B[kk][jj];
          }
        }
      }
#endif

#if 0
      PetscReal D[2][2];
      PetscInt ll;
      PetscReal dev = fac * 2.0 * coeff_eta_e[q];
      
      for (kk=0; kk<3; kk++) {
        for (ll=0; ll<3; ll++) {
          D[kk][ll] = 0.0;
        }
      }
      
      D[0][0] = dev;
      D[1][1] = dev;
      D[2][2] = 0.5 * dev;
      
      PetscReal bulk = 0.0*fac * ( (1.0/3.0) * coeff_eta_e[q] );
      D[0][0] += bulk;   D[0][1]  = bulk;   D[0][2]  = 0.0;
      D[1][0]  = bulk;   D[1][1] += bulk;   D[1][2]  = 0.0;
      D[2][0]  = 0.0;    D[2][1]  = 0.0;
      
      for (ii=0; ii<2*nbasis; ii++) {
        for (jj=ii; jj<2*nbasis; jj++) {
          for (kk=0; kk<3; kk++) {
            for (ll=0; ll<3; ll++) {
              Ke[ii*(2*nbasis)+jj] += B[kk][ii] * D[kk][ll] * B[ll][jj];
            }
          }
        }
      }      /*
       a div(v) div(u)
      
       a (dv1_i/dx + dv2_i/dy) (du1_j/dx + du2_j/dy)
       
      */
      
      for (ii=0; ii<nbasis; ii++) {
        for (jj=ii; jj<nbasis; jj++) {
          Ke[(2*ii+0)*(2*nbasis)+(2*jj+0)] += fac * (-2.0/3.0) * dNdx_q[ii] * dNdx_q[jj];
          Ke[(2*ii+0)*(2*nbasis)+(2*jj+1)] += fac * (-2.0/3.0) * dNdx_q[ii] * dNdy_q[jj];
          
          Ke[(2*ii+1)*(2*nbasis)+(2*jj+0)] += fac * (-2.0/3.0) * dNdy_q[ii] * dNdx_q[jj];
          Ke[(2*ii+1)*(2*nbasis)+(2*jj+1)] += fac * (-2.0/3.0) * dNdy_q[ii] * dNdy_q[jj];
        }
      }

      
#endif
      
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < 2*nbasis; ii++) {
      for (jj = ii; jj < 2*nbasis; jj++) {
        Ke[jj*(2*nbasis)+ii] = Ke[ii*(2*nbasis)+jj];
      }
    }

    ierr = MatSetValuesLocal(B11,2*nbasis,eldofs,2*nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = BCListInsertScaling(B11,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A11 != B11) {
    ierr = MatAssemblyBegin(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }

  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"StokesJacobian(Auu): %1.4e (sec)\n",t1-t0);
#endif

  ierr = PetscFree(B[2]);CHKERRQ(ierr);
  ierr = PetscFree(B[1]);CHKERRQ(ierr);
  ierr = PetscFree(B[0]);CHKERRQ(ierr);
  ierr = PetscFree(B);CHKERRQ(ierr);
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  ierr = EContainerDestroy(&c);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormBilinearSpp_PDEStokes"
PetscErrorCode FormBilinearSpp_PDEStokes(PDEStokes *stokes,Mat B22)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nbasis_p,nbasis_u,npe_u;
  PetscInt  *element_u,*element_p;
  PetscReal   *coords,*elcoords;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal *detJ,**Np,*weight,*coeff_eta,*coeff_eta_e;
  DM dm = NULL;
  EContainer c,cv;
  Quadrature quadrature;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = stokes->dmp;
  ierr = DMTetSpaceElement(stokes->dmu,&nel,&nbasis_u,&element_u,&npe_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nel,&nbasis_p,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(stokes->eta,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&coeff_eta);CHKERRQ(ierr);
  
  ierr = EContainerCreate(stokes->dmu,quadrature,&cv);CHKERRQ(ierr);
  ierr = EContainerCreate(stokes->dmp,quadrature,&c);CHKERRQ(ierr);
  Np     = c->N;
  
  detJ     = cv->buf_q_scalar_a;
  elcoords = cv->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(nbasis_p*nbasis_p,&Ke);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt    *eldofs_u;
    PetscInt    *eldofs_p;
    
    /* get element -> node map */
    eldofs_u = &element_u[nbasis_u*e];
    eldofs_p = &element_p[nbasis_p*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis_u; i++) {
      const PetscInt nidx = eldofs_u[i];
      
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nbasis_u,elcoords,cv->dNr1,cv->dNr2,cv->dNx1,cv->dNx2,detJ);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis_p*nbasis_p);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_eta,&coeff_eta_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      /* get access to element->quadrature points */
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<nbasis_p; ii++) {
        for (jj=ii; jj<nbasis_p; jj++) {
          Ke[ii*nbasis_p+jj] += (1.0/coeff_eta_e[q]) * Np[q][ii] * Np[q][jj] * fac;
        }
      }
    } // quadrature
    
    /*
    for (ii=0; ii<nbasis*nbasis; ii++) {
      if (PetscAbsReal(Ke[ii]) < 1.0e-12) {
        Ke[ii] = 0.0;
      }
    }
    */
    /* fill lower triangular part */
    for (ii = 0; ii < nbasis_p; ii++) {
      for (jj = ii; jj < nbasis_p; jj++) {
        Ke[jj*nbasis_p+ii] = Ke[ii*nbasis_p+jj];
      }
    }

    /*
    printf("e=%d \n",e);
    for (ii=0; ii<nbasis; ii++) {
      for (jj=0; jj<nbasis; jj++) {
        printf("%1.4e ",Ke[ii*nbasis+jj]);
      }printf("\n");
    }
    */
    ierr = MatSetValuesLocal(B22,nbasis_p,eldofs_p,nbasis_p,eldofs_p,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B22,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B22,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"StokesJacobian(Spp): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = EContainerDestroy(&c);CHKERRQ(ierr);
  ierr = EContainerDestroy(&cv);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormJacobian_Stokes"
PetscErrorCode FormJacobian_Stokes(SNES snes,Vec X,Mat A,Mat B,void *ctx)
{
  PDE               pde = (PDE)ctx;
  PDEStokes         *data;
  DM                stokes_pack,dau,dap;
  IS                *is;
  Vec               Uloc,Ploc;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscBool         is_mffd = PETSC_FALSE;
  PetscBool         is_nest = PETSC_FALSE;
  PetscBool         is_shell = PETSC_FALSE;
  PetscErrorCode    ierr;
  

  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  ierr = PDEStokesGetDM(pde,&stokes_pack);CHKERRQ(ierr);
  
  ierr = DMCompositeGetEntries(stokes_pack,&dau,&dap);CHKERRQ(ierr);

  //ierr = DMCompositeGetLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];

  ierr = DMCompositeScatter(stokes_pack,X,Uloc,Ploc);CHKERRQ(ierr);
  pde->local_vector_valid = PETSC_TRUE;
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetGlobalISs(stokes_pack,&is);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->eta);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fu);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fp);CHKERRQ(ierr);

#if 0
  ierr = PetscObjectTypeCompare((PetscObject)A,MATMFFD, &is_mffd);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)A,MATNEST, &is_nest);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)A,MATSHELL,&is_shell);CHKERRQ(ierr);
  
  if (is_nest) {
    Mat Auu;
    
    ierr = MatGetSubMatrix(A,is[0],is[0],MAT_INITIAL_MATRIX,&Auu);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Auu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = FormBilinearA11_PDEStokes(data,Auu,Auu);CHKERRQ(ierr);
    }
    
    ierr = MatDestroy(&Auu);CHKERRQ(ierr);
  }
  /* If shell, do nothing */
  /* If mffd,  do nothing */
  
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
#endif
  
  /* preconditioner for Jacobian */
  ierr = PetscObjectTypeCompare((PetscObject)B,MATMFFD, &is_mffd);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST, &is_nest);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATSHELL,&is_shell);CHKERRQ(ierr);

  {
    Mat Buu;//,Bpp;
    Mat Bup,Bpu;

    ierr = MatGetSubMatrix(B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = FormBilinearA11_PDEStokes(data,Buu,Buu);CHKERRQ(ierr);
    }
    ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    
    ierr = MatGetSubMatrix(B,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Bup,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = MatZeroEntries(Bup);CHKERRQ(ierr);
      ierr = FormBilinearA12_PDEStokes(data,Bup);CHKERRQ(ierr);
    }
    ierr = MatDestroy(&Bup);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Bpu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = MatZeroEntries(Bpu);CHKERRQ(ierr);
      ierr = FormBilinearA21_PDEStokes(data,Bpu);CHKERRQ(ierr);
    }
    ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
    
    /*
    Bpp = NULL;
    ierr = MatGetSubMatrix(B,is[1],is[1],MAT_INITIAL_MATRIX,&Bpp);CHKERRQ(ierr);
    if (Bpp) {
      is_shell = PETSC_FALSE;
      ierr = PetscObjectTypeCompare((PetscObject)Bpp,MATSHELL,&is_shell);CHKERRQ(ierr);
      if (!is_shell) {
        ierr = MatZeroEntries(Bpp);CHKERRQ(ierr);
        ierr = FormBilinearSpp_PDEStokes(data,Bpp);CHKERRQ(ierr);
      }
     ierr = MatDestroy(&Bpp);CHKERRQ(ierr);
    }
    */
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* preconditioner for Schur */
  {
    PetscBool is_fs;
    KSP ksp;
    PC pc;
    PCFieldSplitSchurPreType ptype;
    Mat Spp;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    
    ierr = PetscObjectTypeCompare((PetscObject)pc,PCFIELDSPLIT, &is_fs);CHKERRQ(ierr);
    if (is_fs) {
      ierr = PCFieldSplitGetSchurPre(pc,&ptype,&Spp);CHKERRQ(ierr);
      
      if (Spp) {
        ierr = MatZeroEntries(Spp);CHKERRQ(ierr);
        ierr = FormBilinearSpp_PDEStokes(data,Spp);CHKERRQ(ierr);
      }
    }
  }
  
  /* clean up */
  ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
  ierr = PetscFree(is);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  //ierr = DMCompositeRestoreLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
