
#include <petsc.h>


#undef __FUNCT__
#define __FUNCT__ "MatMultMF_StkDcy2Field2d_B11dev"
PetscErrorCode MatMultMF_StkDcy2Field2d_B11dev(
  const PetscInt nubasis,
  const PetscReal fac,const PetscReal eta,const PetscReal alpha,
  const PetscReal ux[],const PetscReal uy[],
  const PetscReal dNudx[],const PetscReal dNudy[],
  PetscReal y[])
{
  const PetscReal one_third = 1.0/3.0;
  PetscInt     iu;
  PetscReal    strain_rate[] = { 0.0, 0.0, 0.0 };
  PetscReal    tau[] = { 0.0, 0.0, 0.0 };
  PetscReal    two_eta,div,dev,bulk;
  
  
  // strain-rate (e_xx,e_yy,2.e_xy) at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    strain_rate[0] += dNudx[iu]*ux[iu];
    strain_rate[1] += dNudy[iu]*uy[iu];
    strain_rate[2] += dNudy[iu]*ux[iu] + dNudx[iu]*uy[iu];
  }
  // div(u) //
  div = strain_rate[0] + strain_rate[1];
  dev = one_third * ( div );
  // enforce strain-rate is deviatoric //
  strain_rate[0] -= dev;
  strain_rate[1] -= dev;
  
  // deviatoric stress (t_xx,t_yy,t_xy) at quadrature point //
  two_eta = 2.0 * eta * fac;
  tau[0] = two_eta * strain_rate[0];
  tau[1] = two_eta * strain_rate[1];
  tau[2] = eta * strain_rate[2] * fac;
  bulk = alpha * div * fac;
  
  // y = \int grad(u) : tau at quadrature point //
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    y[idx]   += dNudx[iu]*(tau[0] + bulk) + dNudy[iu]*tau[2];
    y[++idx] += dNudx[iu]*tau[2]          + dNudy[iu]*(tau[1] + bulk);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatMultMF_StkDcy2Field2d_B22"
PetscErrorCode MatMultMF_StkDcy2Field2d_B22(
  const PetscInt npbasis,
  const PetscReal fac,const PetscReal beta,
  const PetscReal p[],
  const PetscReal dNpdx[],const PetscReal dNpdy[],
  PetscReal y[])
{
  PetscInt     ip;
  PetscReal    tmp,gradp_qp[] = { 0.0, 0.0 };
  
  
  // grad(p) at quadrature point //
  for (ip=0; ip<npbasis; ip++) {
    gradp_qp[0] += dNpdx[ip] * p[ip];
    gradp_qp[1] += dNpdy[ip] * p[ip];
  }
  
  // y = \int beta grad(q) \dot grad(p) at quadrature point //
  tmp = -fac * beta;
  for (ip=0; ip<npbasis; ip++) {
    y[ip] += tmp * (dNpdx[ip] * gradp_qp[0] + dNpdy[ip] * gradp_qp[1]);
  }
  PetscFunctionReturn(0);
}
