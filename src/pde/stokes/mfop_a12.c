
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <fe_geometry_utils.h>
#include <pdestokesimpl.h>


//#define LOG_MF_OP

/* grad(p) */

PetscErrorCode MatMultMF_Stokes2d_B12(const PetscInt nubasis,const PetscInt npbasis,
                                      const PetscReal fac,
                                      const PetscReal P[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],
                                      const PetscReal Np[],PetscReal Y[]);

#undef __FUNCT__
#define __FUNCT__ "MFStokesWrapper_A12"
PetscErrorCode MFStokesWrapper_A12(PDEStokes *stokes,DM dau,DM dap,PetscScalar Xp[],PetscScalar Yu[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Pe,*Ye;
  PetscInt  *vel_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**GNIxi,**NGIeta,**NIp;
  PetscReal *detJ,**dNudx,**dNudy;
  PetscReal fac;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = stokes->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  
  GNIxi  = stokes->dNudxi;
  NGIeta = stokes->dNudeta;
  dNudx  = stokes->dNudx;
  dNudy  = stokes->dNudy;
  NIp    = stokes->Np;
  
  nubasis = stokes->nubasis;
  npbasis = stokes->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&vel_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Ye);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nen_u*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      vel_el_lidx[2*k + 0] = 2 * nidx + 0;
      vel_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get pressure dof indices and dofs */
    p_el_lidx = &elnidx_p[npbasis*e];
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = Xp[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,GNIxi,NGIeta,dNudx,dNudy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Ye, sizeof(PetscScalar)* ( nubasis*2 ) );
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      MatMultMF_Stokes2d_B12(nubasis,npbasis,fac,Pe,dNudx[q],dNudy[q],NIp[q],Ye);
    }
    
    for (k=0; k<nubasis; k++) {
      Yu[ vel_el_lidx[2*k]   ] += Ye[2*k];
      Yu[ vel_el_lidx[2*k+1] ] += Ye[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"MatMultA12(MF): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(vel_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Ye);
  
  PetscFunctionReturn(0);
}

/*
 IN:  X - a pressure vector
 OUT: Y - a velocity vector
 */
#undef __FUNCT__
#define __FUNCT__ "MatMult_MFStokes_A12"
PetscErrorCode MatMult_MFStokes_A12(Mat A,Vec X,Vec Y)
{
  PDE               pde;
  PDEStokes         *stokes;
  PetscErrorCode    ierr;
  DM                dau,dap;
  Vec               XPloc,YUloc;
  PetscScalar       *LA_XPloc;
  PetscScalar       *LA_YUloc;
  
  PetscFunctionBegin;
  
  ierr = MatShellGetContext(A,(void**)&pde);CHKERRQ(ierr);
  stokes = (PDEStokes*)pde->data;
  
  dau  = stokes->dmu;
  dap  = stokes->dmp;
  
  ierr = DMGetLocalVector(dap,&XPloc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dau,&YUloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMGlobalToLocalBegin(dap,X,INSERT_VALUES,XPloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dap,X,INSERT_VALUES,XPloc);CHKERRQ(ierr);
  
  /* Zero entries in local vectors corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out columns when the mat-mult is performed */
  /* if we have pressure boundary conditions */
  /*
   ierr = BCListInsertLocalZero(ctx->p_bclist,XPloc);CHKERRQ(ierr);
   */
  
  ierr = VecGetArray(XPloc,&LA_XPloc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(YUloc);CHKERRQ(ierr);
  ierr = VecGetArray(YUloc,&LA_YUloc);CHKERRQ(ierr);
  
  /* grad */
  ierr = MFStokesWrapper_A12(stokes,dau,dap,LA_XPloc,LA_YUloc);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(YUloc,&LA_YUloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(XPloc,&LA_XPloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dau,YUloc,ADD_VALUES,Y);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (dau,YUloc,ADD_VALUES,Y);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dap,&XPloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dau,&YUloc);CHKERRQ(ierr);
  
  /* modify Y for the boundary conditions, y_k = 0, 0 is inserted as this is an off-diagonal operator */
  /* Clobbering entries in global vector corresponding to dirichlet boundary conditions */
  /* This has the affect of zeroing out rows when the mat-mult is performed */
  ierr = BCListInsertZero(stokes->u_dirichlet,Y);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

