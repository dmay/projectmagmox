
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <pdestokes.h>
#include <pdestokesimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <element_container.h>
#include <fe_geometry_utils.h>

//#define LOG_BFORM

#undef __FUNCT__
#define __FUNCT__ "FormBilinear_A11"
static PetscErrorCode FormBilinear_A11(PDEStokes *stokes,Mat A11,Mat B11)
{
  PetscErrorCode ierr;
  const PetscReal two_thirds = 2.0/3.0;
  PetscInt  e,nel,i,ii,jj,kk,ll;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *coords,*elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal *detJ,**dNuxi,**dNueta,**dNux,**dNuy,*weight,D[3][3];
  PetscReal *coeff_eta,*coeff_zeta,*eta_e,*zeta_e;
  IS ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM dm = NULL;
  EContainer c;
  Quadrature quadrature;
  PetscReal **B;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = stokes->dmu;
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&nbasis,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  
  bc = stokes->u_dirichlet;
  
  ierr = CoefficientGetQuadrature(stokes->eta,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&coeff_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"zeta*",NULL,NULL,NULL,&coeff_zeta);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&c);CHKERRQ(ierr);
  dNuxi  = c->dNr1;
  dNueta = c->dNr2;
  dNux   = c->dNx1;
  dNuy   = c->dNx2;
  
  detJ     = c->buf_q_scalar_a;
  eldofs   = c->buf_basis_2index_a;
  elcoords = c->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(2*2*nbasis*nbasis,&Ke);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(3,&B);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[0]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[1]);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis,&B[2]);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B11),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    /* generate masked dof indices */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = elnidx[i];
      
      eldofs[2*i+0] = GINDICES[2*nidx];
      eldofs[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = elnidx[i];
      
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nbasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis*2*2);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_eta,&eta_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_zeta,&zeta_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,dev,alpha;
      PetscReal *dNdx_q,*dNdy_q;
      
      /* get access to element->quadrature points */
      dNdx_q = dNux[q];
      dNdy_q = dNuy[q];
      
      fac = weight[q] * detJ[q];
      
      for (i=0; i<nbasis; i++) {
        PetscReal d_dx_i = dNdx_q[i];
        PetscReal d_dy_i = dNdy_q[i];
        
        B[0][2*i  ] = d_dx_i; B[0][2*i+1] = 0.0;
        B[1][2*i  ] = 0.0;    B[1][2*i+1] = d_dy_i;
        B[2][2*i  ] = d_dy_i; B[2][2*i+1] = d_dx_i;
      }
      
      for (kk=0; kk<3; kk++) {
        for (ll=0; ll<3; ll++) {
          D[kk][ll] = 0.0;
        }
      }

      dev = fac * 2.0 * eta_e[q];
      D[0][0] = dev;
      D[1][1] = dev;
      D[2][2] = 0.5 * dev;
      
      /*
      bulk = 0.0*fac * ( - two_thirds * eta_e[q] + zeta_e[q] );
      D[0][0] += bulk;   D[0][1]  = bulk;   D[0][2]  = 0.0;
      D[1][0]  = bulk;   D[1][1] += bulk;   D[1][2]  = 0.0;
      D[2][0]  = 0.0;    D[2][1]  = 0.0;
      */
      
      /* form Bt D B */
      /* these loops can be flattened latter in an optimzation pass */
      for (ii=0; ii<2*nbasis; ii++) {
        for (jj=ii; jj<2*nbasis; jj++) {
          for (kk=0; kk<3; kk++) {
            for (ll=0; ll<3; ll++) {
              Ke[ii*(2*nbasis)+jj] += B[kk][ii] * D[kk][ll] * B[ll][jj];
            }
          }
        }
      }
      
      /*
       a div(v) div(u)
       
       a (dv1_i/dx + dv2_i/dy) (du1_j/dx + du2_j/dy)
       
       */
      
      alpha = - two_thirds * eta_e[q] + zeta_e[q];
      for (ii=0; ii<nbasis; ii++) {
        for (jj=ii; jj<nbasis; jj++) {
          
          Ke[(2*ii+0)*(2*nbasis)+(2*jj+0)] += fac * (alpha) * dNdx_q[ii] * dNdx_q[jj];
          Ke[(2*ii+0)*(2*nbasis)+(2*jj+1)] += fac * (alpha) * dNdx_q[ii] * dNdy_q[jj];
          
          Ke[(2*ii+1)*(2*nbasis)+(2*jj+0)] += fac * (alpha) * dNdy_q[ii] * dNdx_q[jj];
          Ke[(2*ii+1)*(2*nbasis)+(2*jj+1)] += fac * (alpha) * dNdy_q[ii] * dNdy_q[jj];
        }
      }
      
      
      
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < 2*nbasis; ii++) {
      for (jj = ii; jj < 2*nbasis; jj++) {
        Ke[jj*(2*nbasis)+ii] = Ke[ii*(2*nbasis)+jj];
      }
    }
    
    ierr = MatSetValuesLocal(B11,2*nbasis,eldofs,2*nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = BCListInsertScaling(B11,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  if (A11 != B11) {
    ierr = MatAssemblyBegin(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A11,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  
  PetscTime(&t1);
#ifdef LOG_BFORM
  PetscPrintf(PETSC_COMM_WORLD,"StokesDarcy2FieldJacobian(Auu): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(B[2]);CHKERRQ(ierr);
  ierr = PetscFree(B[1]);CHKERRQ(ierr);
  ierr = PetscFree(B[0]);CHKERRQ(ierr);
  ierr = PetscFree(B);CHKERRQ(ierr);
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  ierr = EContainerDestroy(&c);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormBilinear_A22"
static PetscErrorCode FormBilinear_A22(PDEStokes *stokes,Mat A,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *coords,*elcoords;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal *detJ,**dNpxi,**dNpeta,**dNpx,**dNpy,*weight;
  PetscReal *coeff_k,*coeff_k_e;
  DM dm = NULL;
  EContainer c;
  Quadrature quadrature;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = stokes->dmp;
  ierr = DMTetSpaceElement(dm,&nel,NULL,&element,&nbasis,NULL,&coords);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(stokes->eta,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&coeff_k);CHKERRQ(ierr);
  
  ierr = EContainerCreate(stokes->dmp,quadrature,&c);CHKERRQ(ierr);
  dNpxi  = c->dNr1;
  dNpeta = c->dNr2;
  dNpx   = c->dNx1;
  dNpy   = c->dNx2;
  
  detJ     = c->buf_q_scalar_a;
  elcoords = c->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(nbasis*nbasis,&Ke);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt    *eldofs;
    
    /* get element -> node map */
    eldofs = &element[nbasis*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = eldofs[i];
      
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nbasis,elcoords,dNpxi,dNpeta,dNpx,dNpy,detJ);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_k,&coeff_k_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      /* get access to element->quadrature points */
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<nbasis; ii++) {
        for (jj=ii; jj<nbasis; jj++) {
          PetscReal bform;
          
          bform = -coeff_k_e[q] * ( dNpx[q][ii]*dNpx[q][jj] + dNpy[q][ii]*dNpy[q][jj] );
          Ke[ii*nbasis+jj] += bform * fac;
        }
      }
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < nbasis; ii++) {
      for (jj = ii; jj < nbasis; jj++) {
        Ke[jj*nbasis+ii] = Ke[ii*nbasis+jj];
      }
    }
    
    ierr = MatSetValuesLocal(B,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (A != B) {
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscTime(&t1);
#ifdef LOG_BFORM
  PetscPrintf(PETSC_COMM_WORLD,"StokesDaracy2FieldJacobian(App): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = EContainerDestroy(&c);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormBilinear_B22"
static PetscErrorCode FormBilinear_B22(PDEStokes *stokes,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *coords,*elcoords;
  PetscReal   *Ke;
  PetscInt q,nqp;
  PetscReal *detJ,**Np,**dNpxi,**dNpeta,**dNpx,**dNpy,*weight;
  PetscReal *coeff_eta,*coeff_eta_e,*coeff_k,*coeff_k_e;
  DM dm = NULL;
  EContainer c;
  Quadrature quadrature;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dm = stokes->dmp;
  ierr = DMTetSpaceElement(dm,&nel,NULL,&element,&nbasis,NULL,&coords);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(stokes->eta,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&coeff_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&coeff_k);CHKERRQ(ierr);
  
  ierr = EContainerCreate(stokes->dmp,quadrature,&c);CHKERRQ(ierr);
  Np     = c->N;
  dNpxi  = c->dNr1;
  dNpeta = c->dNr2;
  dNpx   = c->dNx1;
  dNpy   = c->dNx2;
  
  detJ     = c->buf_q_scalar_a;
  elcoords = c->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(nbasis*nbasis,&Ke);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt    *eldofs;
    
    /* get element -> node map */
    eldofs = &element[nbasis*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      const PetscInt nidx = eldofs[i];
      
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nbasis,elcoords,dNpxi,dNpeta,dNpx,dNpy,detJ);
    
    ierr = PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_eta,&coeff_eta_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature,e,1,coeff_k,&coeff_k_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      
      /* get access to element->quadrature points */
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<nbasis; ii++) {
        for (jj=ii; jj<nbasis; jj++) {
          PetscReal bform;
          
          Ke[ii*nbasis+jj] += (1.0/coeff_eta_e[q]) * Np[q][ii] * Np[q][jj] * fac;
          
          bform = coeff_k_e[q] * ( dNpx[q][ii]*dNpx[q][jj] + dNpy[q][ii]*dNpy[q][jj] );
          Ke[ii*nbasis+jj] += bform * fac;
        }
      }
    } // quadrature
    
    /* fill lower triangular part */
    for (ii = 0; ii < nbasis; ii++) {
      for (jj = ii; jj < nbasis; jj++) {
        Ke[jj*nbasis+ii] = Ke[ii*nbasis+jj];
      }
    }
    
    ierr = MatSetValuesLocal(B,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscTime(&t1);
#ifdef LOG_BFORM
  PetscPrintf(PETSC_COMM_WORLD,"StokesDaracy2FieldJacobian(Spp): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ke);CHKERRQ(ierr);
  ierr = EContainerDestroy(&c);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* - \int div(w) p dV */
#undef __FUNCT__
#define __FUNCT__ "FormBilinearA12_PDEStokes"
PetscErrorCode FormBilinearA12_PDEStokes(PDEStokes *darcy,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nubasis,npbasis;
  PetscInt  *element_u,*element_p;
  PetscReal *coords,*elcoords_u,*weight;
  PetscInt  *eldofs_u;
  PetscReal *Ae;
  PetscInt  q,nqp;
  PetscReal *detJ,**dNuxi,**dNueta,**dNux,**dNuy,**Np;
  IS        ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM     dmu = NULL;
  DM     dmp = NULL;
  EContainer cu,cp;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dmu = darcy->dmu;
  dmp = darcy->dmp;
  ierr = DMTetSpaceElement(dmu,&nel,&nubasis,&element_u,NULL,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&npbasis,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(darcy->ref,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  bc = darcy->u_dirichlet;
  
  ierr = EContainerCreate(dmu,darcy->ref,&cu);CHKERRQ(ierr);
  dNuxi  = cu->dNr1;
  dNueta = cu->dNr2;
  dNux   = cu->dNx1;
  dNuy   = cu->dNx2;
  
  ierr = EContainerCreate(dmp,darcy->ref,&cp);CHKERRQ(ierr);
  Np = cp->N;
  
  detJ       = cu->buf_q_scalar_a;
  eldofs_u   = cu->buf_basis_2index_a;
  elcoords_u = cu->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(2*nubasis*npbasis,&Ae);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *elnidx_u,*elnidx_p;
    
    /* get element -> node map */
    elnidx_u = &element_u[nubasis*e];
    elnidx_p = &element_p[npbasis*e];
    
    /* generate masked dof indices */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      eldofs_u[2*i+0] = GINDICES[2*nidx];
      eldofs_u[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      elcoords_u[2*i  ] = coords[2*nidx  ];
      elcoords_u[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords_u,dNuxi,dNueta,dNux,dNuy,detJ);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nubasis*npbasis*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Np_q;
      PetscReal *dNux_q,*dNuy_q;
      
      /* get access to element->quadrature points */
      Np_q   = Np[q];
      dNux_q = dNux[q];
      dNuy_q = dNuy[q];
      
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<nubasis; ii++) {
        for (jj=0; jj<npbasis; jj++) {
          
          Ae[npbasis*(2*ii+0) + jj] -= dNux_q[ii] * Np_q[jj] * fac;
          Ae[npbasis*(2*ii+1) + jj] -= dNuy_q[ii] * Np_q[jj] * fac;
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(B,2*nubasis,eldofs_u,npbasis,elnidx_p,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobianHdiv(Aup): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  ierr = EContainerDestroy(&cu);CHKERRQ(ierr);
  ierr = EContainerDestroy(&cp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* - \int q div(u) dV */
#undef __FUNCT__
#define __FUNCT__ "FormBilinearA21_PDEStokes"
PetscErrorCode FormBilinearA21_PDEStokes(PDEStokes *darcy,Mat B)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,i,ii,jj;
  PetscInt  nubasis,npbasis;
  PetscInt  *element_u,*element_p;
  PetscReal *coords,*elcoords_u,*weight;
  PetscInt  *eldofs_u;
  PetscReal *Ae;
  PetscInt  q,nqp;
  PetscReal *detJ,**dNuxi,**dNueta,**dNux,**dNuy,**Np;
  IS        ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM     dmu = NULL;
  DM     dmp = NULL;
  EContainer cu,cp;
  PetscLogDouble t0,t1;
  
  
  PetscFunctionBegin;
  dmu = darcy->dmu;
  dmp = darcy->dmp;
  ierr = DMTetSpaceElement(dmu,&nel,&nubasis,&element_u,NULL,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&npbasis,&element_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(darcy->ref,&nqp,NULL,&weight);CHKERRQ(ierr);
  
  bc = darcy->u_dirichlet;
  
  ierr = EContainerCreate(dmu,darcy->ref,&cu);CHKERRQ(ierr);
  dNuxi  = cu->dNr1;
  dNueta = cu->dNr2;
  dNux   = cu->dNx1;
  dNuy   = cu->dNx2;
  
  ierr = EContainerCreate(dmp,darcy->ref,&cp);CHKERRQ(ierr);
  Np = cp->N;
  
  detJ       = cu->buf_q_scalar_a;
  eldofs_u   = cu->buf_basis_2index_a;
  elcoords_u = cu->buf_basis_2vector_a;
  
  ierr = PetscMalloc1(2*nubasis*npbasis,&Ae);CHKERRQ(ierr);
  
  NUM_GINDICES = 2 * NUM_GINDICES; /* vector */
  ierr = ISCreateStride(PetscObjectComm((PetscObject)B),NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *elnidx_u,*elnidx_p;
    
    /* get element -> node map */
    elnidx_u = &element_u[nubasis*e];
    elnidx_p = &element_p[npbasis*e];
    
    /* generate masked dof indices */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      eldofs_u[2*i+0] = GINDICES[2*nidx];
      eldofs_u[2*i+1] = GINDICES[2*nidx+1];
    }
    
    /* get element coordinates */
    for (i=0; i<nubasis; i++) {
      const PetscInt nidx = elnidx_u[i];
      
      elcoords_u[2*i  ] = coords[2*nidx  ];
      elcoords_u[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords_u,dNuxi,dNueta,dNux,dNuy,detJ);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nubasis*npbasis*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Np_q;
      PetscReal *dNux_q,*dNuy_q;
      
      /* get access to element->quadrature points */
      Np_q   = Np[q];
      dNux_q = dNux[q];
      dNuy_q = dNuy[q];
      
      fac = weight[q] * detJ[q];
      
      for (ii=0; ii<npbasis; ii++) {
        for (jj=0; jj<nubasis; jj++) {
          Ae[2*nubasis*ii + (2*jj+0)] -= Np_q[ii] * dNux_q[jj] * fac;
          Ae[2*nubasis*ii + (2*jj+1)] -= Np_q[ii] * dNuy_q[jj] * fac;
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(B,npbasis,elnidx_p,2*nubasis,eldofs_u,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"DarcyJacobianHdiv(Apu): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  ierr = EContainerDestroy(&cu);CHKERRQ(ierr);
  ierr = EContainerDestroy(&cp);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormJacobian_StkDcy2Field"
PetscErrorCode FormJacobian_StkDcy2Field(SNES snes,Vec X,Mat A,Mat B,void *ctx)
{
  PDE               pde = (PDE)ctx;
  PDEStokes         *data;
  DM                stokes_pack,dau,dap;
  IS                *is;
  Vec               Uloc,Ploc;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscBool         is_mffd = PETSC_FALSE;
  PetscBool         is_nest = PETSC_FALSE;
  PetscBool         is_shell = PETSC_FALSE;
  PetscErrorCode    ierr;
  
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  ierr = PDEStokesGetDM(pde,&stokes_pack);CHKERRQ(ierr);
  
  ierr = DMCompositeGetEntries(stokes_pack,&dau,&dap);CHKERRQ(ierr);
  
  //ierr = DMCompositeGetLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  
  ierr = DMCompositeScatter(stokes_pack,X,Uloc,Ploc);CHKERRQ(ierr);
  pde->local_vector_valid = PETSC_TRUE;
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetGlobalISs(stokes_pack,&is);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->eta);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fu);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fp);CHKERRQ(ierr);
  
  /* preconditioner for Jacobian */
  ierr = PetscObjectTypeCompare((PetscObject)B,MATMFFD, &is_mffd);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST, &is_nest);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)B,MATSHELL,&is_shell);CHKERRQ(ierr);
  
  {
    Mat Buu,Bpp;
    Mat Bup,Bpu;
    
    ierr = MatGetSubMatrix(B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is[1],is[1],MAT_INITIAL_MATRIX,&Bpp);CHKERRQ(ierr);
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Buu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = MatZeroEntries(Buu);CHKERRQ(ierr);
      ierr = FormBilinear_A11(data,Buu,Buu);CHKERRQ(ierr);
    }

    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Bpp,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = MatZeroEntries(Bpp);CHKERRQ(ierr);
      ierr = FormBilinear_A22(data,Bpp,Bpp);CHKERRQ(ierr);
    }

    
    ierr = MatGetSubMatrix(B,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);

    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Bup,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = MatZeroEntries(Bup);CHKERRQ(ierr);
      ierr = FormBilinearA12_PDEStokes(data,Bup);CHKERRQ(ierr);
    }
    
    is_shell = PETSC_FALSE;
    ierr = PetscObjectTypeCompare((PetscObject)Bpu,MATSHELL,&is_shell);CHKERRQ(ierr);
    if (!is_shell) {
      ierr = MatZeroEntries(Bpu);CHKERRQ(ierr);
      ierr = FormBilinearA21_PDEStokes(data,Bpu);CHKERRQ(ierr);
    }
    
    ierr = MatDestroy(&Bup);CHKERRQ(ierr);
    ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
    ierr = MatDestroy(&Buu);CHKERRQ(ierr);
    ierr = MatDestroy(&Bpp);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* preconditioner for Schur */
  {
    PetscBool is_fs;
    KSP ksp;
    PC pc;
    PCFieldSplitSchurPreType ptype;
    Mat Spp;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    
    ierr = PetscObjectTypeCompare((PetscObject)pc,PCFIELDSPLIT,&is_fs);CHKERRQ(ierr);
    if (is_fs) {
      ierr = PCFieldSplitGetSchurPre(pc,&ptype,&Spp);CHKERRQ(ierr);
      
      if (Spp) {
        ierr = MatZeroEntries(Spp);CHKERRQ(ierr);
        ierr = FormBilinear_B22(data,Spp);CHKERRQ(ierr);
      }
    }
  }
  
  /* clean up */
  ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
  ierr = PetscFree(is);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  //ierr = DMCompositeRestoreLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
