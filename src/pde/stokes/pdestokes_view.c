
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>
#include <pde.h>
#include <pdestokes.h>

#undef __FUNCT__
#define __FUNCT__ "sumdec_WAB"
static PetscInt sumdec_WAB(PetscInt a)
{
  PetscInt res,ac;
  
  res = a;
  ac = a;
  while (ac != 0) {
    ac--;
    res = res + ac;
  }
  return(res);
}

#undef __FUNCT__
#define __FUNCT__ "StokesUP_CG_AsciiVTU_g"
PetscErrorCode StokesUP_CG_AsciiVTU_g(DM dm,Vec field,const char filename[])
{
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt i,j,e,dof;
  const PetscScalar *LA_field;
  PetscInt nelements,npe,nnodes,*element,basis_order;
  PetscReal *coords;
  DMTetBasisType btype;
  PetscInt nspilt,nfull,nsubel,nel_l;
  
  PetscFunctionBegin;
  fp = fopen(filename,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",filename);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render continuous basis");
  }
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  
  //ierr = DMTetGeometryElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements * nsubel;
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes,nel_l);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &element[npe*e];
    PetscInt map[30][30],np,cnt;
    
    /* init map */
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        map[i][j] = 0;
      }
    }

    np = basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        map[i][j] = idx[cnt];
        cnt++;
      }
    }

    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        
        if (i == np-j-2) {
          /* last triangle - located on side */
          //printf("tri = { %d %d %d }\n",map[i][j],map[i+1][j],map[i][j+1]);
          //tetl->geometry->element[ 3 * triangle_cnt + 0 ] = map[i  ][j  ];
          //tetl->geometry->element[ 3 * triangle_cnt + 1 ] = map[i+1][j  ];
          //tetl->geometry->element[ 3 * triangle_cnt + 2 ] = map[i  ][j+1];

          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);

        } else {
          /* two triangles to define */
          //printf("tri = { %d %d %d }\n",map[i][j],map[i+1][j],map[i][j+1]);
          //printf("tri = { %d %d %d }\n",map[i+1][j],map[i+1][j+1],map[i][j+1]);
          
          //tetl->geometry->element[ 3 * triangle_cnt + 0 ] = map[i  ][j  ];
          //tetl->geometry->element[ 3 * triangle_cnt + 1 ] = map[i+1][j  ];
          //tetl->geometry->element[ 3 * triangle_cnt + 2 ] = map[i  ][j+1];
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
          //tetl->geometry->element[ 3 * triangle_cnt + 0 ] = map[i+1][j  ];
          //tetl->geometry->element[ 3 * triangle_cnt + 1 ] = map[i+1][j+1];
          //tetl->geometry->element[ 3 * triangle_cnt + 2 ] = map[i  ][j+1];
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
        }
      }
    }
    
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",coords[2*i],coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  ierr = VecGetArrayRead(field,&LA_field);CHKERRQ(ierr);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  if (dof == 2) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"velocity\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
    for (i=0; i<nnodes; i++) {
      PetscReal vx,vy;
      
      vx = LA_field[2*i];    if (PetscAbsReal(vx) < 1.0e-10) { vx = 0.0; }
      vy = LA_field[2*i+1];  if (PetscAbsReal(vy) < 1.0e-10) { vy = 0.0; }
      PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e 0.0 ",vx,vy);
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  } else {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"pressure\" NumberOfComponents=\"1\" format=\"ascii\"> \n");
    for (i=0; i<nnodes; i++) {
      PetscReal pressure;

      pressure = LA_field[i]; if (PetscAbsReal(pressure) < 1.0e-10) { pressure = 0.0; }
      PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",pressure);
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  ierr = VecRestoreArrayRead(field,&LA_field);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "StokesP_CG_AsciiVTU"
PetscErrorCode StokesP_CG_AsciiVTU(DM dm,Vec field,const char filename[])
{
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt i,e;
  const PetscScalar *LA_field;
  PetscInt nelements,npe,nnodes,*element,basis_order;
  PetscReal *coords;
  DMTetBasisType btype;
  
  PetscFunctionBegin;
  fp = fopen(filename,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",filename);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render continuous basis");
  }

  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  if (basis_order != 1) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render k=1 basis");
  }
  
  //ierr = DMTetGeometryElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes,nelements);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                 element[3*e],element[3*e+1],element[3*e+2]);
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",coords[2*i],coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  ierr = VecGetArrayRead(field,&LA_field);CHKERRQ(ierr);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"pressure\" NumberOfComponents=\"1\" format=\"ascii\"> \n");
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",LA_field[i]);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  ierr = VecRestoreArrayRead(field,&LA_field);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

PetscErrorCode DMTetMeshGenerateBasisCoords2d_DG(PetscInt order,PetscInt *_npe,PetscReal **_xi);

#undef __FUNCT__
#define __FUNCT__ "StokesUP_DG_AsciiVTU_g"
PetscErrorCode StokesUP_DG_AsciiVTU_g(DM dm,Vec field,const char filename[])
{
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt i,j,e,dof;
  const PetscScalar *LA_field;
  PetscInt nelements,nbasis,npe_g,nnodes,*element,*element_g,basis_order;
  PetscReal *coords_g;
  DMTetBasisType btype;
  PetscInt nspilt,nfull,nsubel,nel_l,nnodes_l;
  PetscInt npoints;
  PetscReal *xi,**N;
  
  PetscFunctionBegin;
  fp = fopen(filename,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",filename);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_DG) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render discontinuous basis");
  }
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetMeshGenerateBasisCoords2d_DG(basis_order,&npoints,&xi);
  {
    DM_TET *tet = (DM_TET*)dm->data;
    PetscInt basis_order_ref,nb;
    DMTetBasisType basis_type_ref;

    /* temporarily over-ride basis definition */
    basis_type_ref = tet->basis_type;
    basis_order_ref = tet->basis_order;
    tet->basis_type = DMTET_CWARP_AND_BLEND;
    tet->basis_order = 1;
    
    ierr = DMTetTabulateBasis(dm,npoints,xi,&nb,&N);CHKERRQ(ierr);
    
    /* reset basis definition */
    tet->basis_type = basis_type_ref;
    tet->basis_order = basis_order_ref;
  }
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe_g,NULL,&coords_g);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,&nbasis,&element,NULL,&nnodes,NULL);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements * nsubel;
  nnodes_l = nelements * nbasis;
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes_l,nel_l);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &element[nbasis*e];
    PetscInt map[30][30],np,cnt;
    
    /* init map */
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        map[i][j] = 0;
      }
    }
    
    np = basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        
        if (i == np-j-2) {
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
        } else {
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
        }
      }
    }
    
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscInt *idx_g = &element_g[npe_g*e];

    /* interpolate coordinates */
    for (i=0; i<nbasis; i++) {
      PetscReal coor[2];
      
      coor[0] = coor[1] = 0.0;
      for (j=0; j<npe_g; j++) {
        coor[0] += N[i][j] * coords_g[ 2*idx_g[j] + 0 ];
        coor[1] += N[i][j] * coords_g[ 2*idx_g[j] + 1 ];
      }
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",coor[0],coor[1]);
    }
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  ierr = VecGetArrayRead(field,&LA_field);CHKERRQ(ierr);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  if (dof == 2) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"velocity\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
    for (e=0; e<nelements; e++) {
      PetscInt *idx = &element[nbasis*e];
      
      for (i=0; i<nbasis; i++) {
        PetscReal vx,vy;
        
        vx = LA_field[2*idx[i]];    if (PetscAbsReal(vx) < 1.0e-10) { vx = 0.0; }
        vy = LA_field[2*idx[i]+1];  if (PetscAbsReal(vy) < 1.0e-10) { vy = 0.0; }
        PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e 0.0 ",vx,vy);
      }
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  } else {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"pressure\" NumberOfComponents=\"1\" format=\"ascii\"> \n");
    for (e=0; e<nelements; e++) {
      PetscInt *idx = &element[nbasis*e];
      
      for (i=0; i<nbasis; i++) {
        PetscReal pressure;
      
        pressure = LA_field[idx[i]]; if (PetscAbsReal(pressure) < 1.0e-10) { pressure = 0.0; }
        PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",pressure);
      }
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  ierr = VecRestoreArrayRead(field,&LA_field);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  ierr = PetscFree(xi);CHKERRQ(ierr);
  for (i=0; i<npoints; i++) {
    ierr = PetscFree(N[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree(N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesFieldView_AsciiVTU"
PetscErrorCode PDEStokesFieldView_AsciiVTU(PDE pde,const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  const char *option;
  PetscMPIInt rank;
  Vec X,velocity,pressure,Uloc,Ploc;
  DM dms,dmu,dmp;
  char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN];
  
  PetscFunctionBegin;
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)pde),&rank);CHKERRQ(ierr);
  ierr = PetscObjectGetOptionsPrefix((PetscObject)pde,&option);CHKERRQ(ierr);
  header[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (option) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",option);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  ierr = PDEStokesGetDM(pde,&dms);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(dms,&Uloc,&Ploc);CHKERRQ(ierr);

  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);

  ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"stokes_u-r%.4d.vtu",(int)rank);CHKERRQ(ierr);
  ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(filename,str);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmu,velocity,INSERT_VALUES,Uloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmu,velocity,INSERT_VALUES,Uloc);CHKERRQ(ierr);
  ierr = StokesUP_CG_AsciiVTU_g(dmu,Uloc,filename);CHKERRQ(ierr);
  
  ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"stokes_p-r%.4d.vtu",(int)rank);CHKERRQ(ierr);
  ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(filename,str);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmp,pressure,INSERT_VALUES,Ploc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmp,pressure,INSERT_VALUES,Ploc);CHKERRQ(ierr);
  {
    PetscInt order;
    DMTetBasisType btype;
    
    ierr = DMTetGetBasisOrder(dmp,&order);CHKERRQ(ierr);
    ierr = DMTetGetBasisType(dmp,&btype);CHKERRQ(ierr);
    if (btype == DMTET_DG) {
      ierr = StokesUP_DG_AsciiVTU_g(dmp,Ploc,filename);CHKERRQ(ierr);
    } else {
      if (order == 1) {
        ierr = StokesP_CG_AsciiVTU(dmp,Ploc,filename);CHKERRQ(ierr);
      } else {
        ierr = StokesUP_CG_AsciiVTU_g(dmp,Ploc,filename);CHKERRQ(ierr);
      }
    }
  }

  ierr = DMCompositeRestoreAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(dms,&Uloc,&Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesFieldView_BinaryVTU"
PetscErrorCode PDEStokesFieldView_BinaryVTU(PDE pde,const char prefix[])
{
  PetscFunctionBegin;
  SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"H5 viewer not implemented");
  PetscFunctionReturn(0);
}

/*
 Should call generic H5 viewers defined for DMTET for geometry, basis (these don't exist yet)
 This function should be able to jam content into an existing H5 file, or create a new one
 
 <Stokes>
  <velocity>
    <geometry>
    <space>
      <field>
 <pressure>
  <geometry>
  <space>
    <field>
 
*/
#undef __FUNCT__
#define __FUNCT__ "PDEStokesFieldView_H5"
PetscErrorCode PDEStokesFieldView_H5(PDE pde,const char prefix[])
{
  PetscFunctionBegin;
  SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"H5 viewer not implemented");
  PetscFunctionReturn(0);
}
