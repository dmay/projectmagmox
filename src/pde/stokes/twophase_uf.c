
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <fe_geometry_utils.h>
#include <element_container.h>

#include <pdestokesimpl.h>
#include <pdestokes.h>


#undef __FUNCT__
#define __FUNCT__ "StkDarcyFluidVelocityProjectU_RHS"
PetscErrorCode StkDarcyFluidVelocityProjectU_RHS(PDE pde,DM dms,DM dmu,DM dmp,Vec Xp,Vec F)
{
  PetscErrorCode ierr;
  PDEStokes   *stokes;
  PetscInt    e,nel,npe_u,npe_p,nbasis_u,nbasis_p,i,j,k;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *u_el_lidx,*elnidx_u,*elnidx_p,*elbasismap_u,*elbasismap_p,*elbasismap_g;
  PetscReal   *coords_g,*Fe,*pe;
  PetscInt    q,nqp;
  Quadrature  quadrature,quadrature_eta;
  EContainer  v_space,p_space;
  PetscReal   **Nu,**dNpxi,**dNpeta,**dNpx,**dNpy;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         Xuloc,Xploc,Floc;
  PetscReal   *LA_Xp;
  Coefficient eta;
  PetscReal   *coeff_k_phi,*coeff_k_phi_e;
  PetscReal   *coeff_e3,*coeff_e3_e;
  PetscInt    ncomp_k,ncomp_e3;
  
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  
  ierr = DMCompositeGetLocalVectors(dms,&Xuloc,&Xploc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmp,Xp,INSERT_VALUES,Xploc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmp,Xp,INSERT_VALUES,Xploc);CHKERRQ(ierr);
  
  ierr = DMGetLocalVector(dmu,&Floc);CHKERRQ(ierr);
  ierr = VecZeroEntries(Floc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmu,&nel,&nbasis_u,&elbasismap_u,&npe_u,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmu,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&nbasis_p,&elbasismap_p,&npe_p,NULL,NULL);CHKERRQ(ierr);

  stokes = (PDEStokes*)pde->data;

  // quadrature //
  quadrature = stokes->ref;
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);

  // GetCoefficients //
  eta = stokes->eta;
  ierr = CoefficientGetQuadrature(eta,&quadrature_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"k*",NULL,NULL,&ncomp_k,&coeff_k_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"e_3",NULL,NULL,&ncomp_e3,&coeff_e3);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmu,quadrature,&v_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dmp,quadrature,&p_space);CHKERRQ(ierr);
  
  Nu     = v_space->N;
  dNpxi  = p_space->dNr1;
  dNpeta = p_space->dNr2;
  dNpx   = p_space->dNx1;
  dNpy   = p_space->dNx2;
  
  u_el_lidx = v_space->buf_basis_2index_a;
  Fe        = v_space->buf_basis_2vector_a;
  pe        = p_space->buf_basis_scalar_a;
  
  ierr = VecGetArray(Xploc,&LA_Xp);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_u = &elbasismap_u[nbasis_u*e];
    elnidx_p = &elbasismap_p[nbasis_p*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get velocity dof indices */
    for (k=0; k<nbasis_u; k++) {
      PetscInt nidx = elnidx_u[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dofs */
    for (k=0; k<nbasis_p; k++) {
      PetscInt nidx = elnidx_p[k];
      
      pe[k] = LA_Xp[nidx];
    }

    ierr = QuadratureGetElementValues(quadrature_eta,e,1,        coeff_k_phi,&coeff_k_phi_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_eta,e, ncomp_e3,coeff_e3,   &coeff_e3_e);CHKERRQ(ierr);
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_p,elcoords_g,dNpxi,dNpeta,dNpx,dNpy,&detJ_affine);
    
    ierr = PetscMemzero(Fe,sizeof(PetscReal)*nbasis_u*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,lform[2],gradp[2];
      
      fac = detJ_affine * weight[q];

      /* compute grad(p) */
      gradp[0] = gradp[1] = 0.0;
      for (j=0; j<nbasis_p; j++) {
        gradp[0] += dNpx[q][j] * pe[j];
        gradp[1] += dNpy[q][j] * pe[j];
      }
      
      lform[0] = coeff_k_phi_e[q] * (gradp[0] - coeff_e3_e[2*q + 0]);
      lform[1] = coeff_k_phi_e[q] * (gradp[1] - coeff_e3_e[2*q + 1]);

      for (i=0; i<nbasis_u; i++) {
        Fe[2*i + 0] += fac * Nu[q][i] * ( lform[0] );
        Fe[2*i + 1] += fac * Nu[q][i] * ( lform[1] );
      }
    }
    
    ierr = VecSetValues(Floc,2*nbasis_u,u_el_lidx,Fe,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArray(Xploc,&LA_Xp);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(Floc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(Floc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmu,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmu,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dmu,&Floc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(dms,&Xuloc,&Xploc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&p_space);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StkDarcyFluidVelocityProjectP_RHS"
PetscErrorCode StkDarcyFluidVelocityProjectP_RHS(PDE pde,DM dms,DM dmu,DM dmp,Vec Xu,Vec Xp,Vec F)
{
  PetscErrorCode ierr;
  PDEStokes   *stokes;
  PetscInt    e,nel,npe_u,npe_p,nbasis_u,nbasis_p,i,j,k,npe_u_proj,nbasis_u_proj;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *u_el_lidx,*elnidx_u,*elnidx_u_proj,*elnidx_p,*elbasismap_u,*elbasismap_u_proj,*elbasismap_p,*elbasismap_g;
  PetscReal   *coords_g,*Fe,*ue,*pe;
  PetscInt    q,nqp;
  Quadrature  quadrature,quadrature_eta;
  EContainer  u_space,p_space;
  PetscReal   **Nu,**Np,**dNpxi,**dNpeta,**dNpx,**dNpy;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         Xuloc,Xploc,Floc;
  PetscReal   *LA_Xu,*LA_Xp;
  Coefficient eta;
  PetscReal   *coeff_k_phi,*coeff_k_phi_e;
  PetscReal   *coeff_e3,*coeff_e3_e;
  PetscInt    ncomp_k,ncomp_e3;
  DM          dm_vector;
  
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  
  ierr = DMCompositeGetLocalVectors(dms,&Xuloc,&Xploc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmu,Xu,INSERT_VALUES,Xuloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmu,Xu,INSERT_VALUES,Xuloc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmp,Xp,INSERT_VALUES,Xploc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmp,Xp,INSERT_VALUES,Xploc);CHKERRQ(ierr);
  
  ierr = DMTetCreateSharedSpace(dmp,2,&dm_vector);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm_vector,&Floc);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(Floc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmu,&nel,&nbasis_u,&elbasismap_u,&npe_u,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_vector,&nel,&nbasis_u_proj,&elbasismap_u_proj,&npe_u_proj,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&nbasis_p,&elbasismap_p,&npe_p,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmp,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  if (nbasis_u_proj != nbasis_p) SETERRQ(PetscObjectComm((PetscObject)dmu),PETSC_ERR_USER,"Expected basis of projected velocity to match the pressure space");
  
  stokes = (PDEStokes*)pde->data;
  
  // quadrature //
  quadrature = stokes->ref;
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  // GetCoefficients //
  eta = stokes->eta;
  ierr = CoefficientGetQuadrature(eta,&quadrature_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"k*",NULL,NULL,&ncomp_k,&coeff_k_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"e_3",NULL,NULL,&ncomp_e3,&coeff_e3);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmu,quadrature,&u_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dmp,quadrature,&p_space);CHKERRQ(ierr);
  
  Nu     = u_space->N;
  Np     = p_space->N;
  dNpxi  = p_space->dNr1;
  dNpeta = p_space->dNr2;
  dNpx   = p_space->dNx1;
  dNpy   = p_space->dNx2;
  
  ue        = u_space->buf_basis_2vector_a;
  u_el_lidx = p_space->buf_basis_2index_a;
  Fe        = p_space->buf_basis_2vector_a;
  pe        = p_space->buf_basis_scalar_a;
  
  ierr = VecGetArray(Xuloc,&LA_Xu);CHKERRQ(ierr);
  ierr = VecGetArray(Xploc,&LA_Xp);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_u      = &elbasismap_u[nbasis_u*e];
    elnidx_u_proj = &elbasismap_u_proj[nbasis_u_proj*e];
    elnidx_p      = &elbasismap_p[nbasis_p*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get velocity dof indices for the projection */
    for (k=0; k<nbasis_u_proj; k++) {
      PetscInt nidx = elnidx_u_proj[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }

    /* get velocity dofs */
    for (k=0; k<nbasis_u; k++) {
      PetscInt nidx = elnidx_u[k];
      
      ue[2*k+0] = LA_Xu[2*nidx+0];
      ue[2*k+1] = LA_Xu[2*nidx+1];
    }

    /* get pressure dofs */
    for (k=0; k<nbasis_p; k++) {
      PetscInt nidx = elnidx_p[k];
      
      pe[k] = LA_Xp[nidx];
    }
    
    ierr = QuadratureGetElementValues(quadrature_eta,e,1,        coeff_k_phi,&coeff_k_phi_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_eta,e, ncomp_e3,coeff_e3,   &coeff_e3_e);CHKERRQ(ierr);
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_p,elcoords_g,dNpxi,dNpeta,dNpx,dNpy,&detJ_affine);
    
    ierr = PetscMemzero(Fe,sizeof(PetscReal)*nbasis_u_proj*2);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,lform[2],us[2],gradp[2];
      
      fac = detJ_affine * weight[q];

      /* compute u */
      us[0] = us[1] = 0.0;
      for (j=0; j<nbasis_u; j++) {
        us[0] += Nu[q][j] * ue[2*j+0];
        us[1] += Nu[q][j] * ue[2*j+1];
      }

      /* compute grad(p) */
      gradp[0] = gradp[1] = 0.0;
      for (j=0; j<nbasis_p; j++) {
        gradp[0] += dNpx[q][j] * pe[j];
        gradp[1] += dNpy[q][j] * pe[j];
      }
      
      lform[0] = us[0] - coeff_k_phi_e[q] * (gradp[0] - coeff_e3_e[2*q + 0]);
      lform[1] = us[1] - coeff_k_phi_e[q] * (gradp[1] - coeff_e3_e[2*q + 1]);
      
      for (i=0; i<nbasis_u_proj; i++) {
        Fe[2*i + 0] += fac * Np[q][i] * ( lform[0] );
        Fe[2*i + 1] += fac * Np[q][i] * ( lform[1] );
      }
    }
    
    ierr = VecSetValues(Floc,2*nbasis_u_proj,u_el_lidx,Fe,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArray(Xploc,&LA_Xp);CHKERRQ(ierr);
  ierr = VecRestoreArray(Xuloc,&LA_Xu);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(Floc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(Floc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dm_vector,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm_vector,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  
  ierr = VecDestroy(&Floc);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_vector);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(dms,&Xuloc,&Xploc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&u_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&p_space);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StkDarcyDivVelocityProjectP_RHS"
PetscErrorCode StkDarcyDivVelocityProjectP_RHS(PDE pde,DM dms,DM dmu,DM dmp,Vec Xu,Vec Xp,Vec F)
{
  PetscErrorCode ierr;
  PDEStokes   *stokes;
  PetscInt    e,nel,npe_u,npe_p,nbasis_u,nbasis_p,i,j,k;
  PetscReal   elcoords_g[2 * 3];
  PetscInt    *u_el_lidx,*elnidx_u,*elnidx_p,*elbasismap_u,*elbasismap_p,*elbasismap_g;
  PetscReal   *coords_g,*Fe,*ue;
  PetscInt    q,nqp;
  Quadrature  quadrature;
  EContainer  u_space,p_space;
  PetscReal   **Np,**dNuxi,**dNueta,**dNux,**dNuy;
  PetscReal   *xi,*weight;
  PetscReal   detJ_affine;
  Vec         Xuloc,Xploc,Floc;
  PetscReal   *LA_Xu,*LA_Xp;
  
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  
  ierr = DMCompositeGetLocalVectors(dms,&Xuloc,&Xploc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmu,Xu,INSERT_VALUES,Xuloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmu,Xu,INSERT_VALUES,Xuloc);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(dmp,Xp,INSERT_VALUES,Xploc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmp,Xp,INSERT_VALUES,Xploc);CHKERRQ(ierr);
  
  ierr = DMGetLocalVector(dmp,&Floc);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(Floc);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmu,&nel,&nbasis_u,&elbasismap_u,&npe_u,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&nbasis_p,&elbasismap_p,&npe_p,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmp,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  stokes = (PDEStokes*)pde->data;
  
  // quadrature //
  quadrature = stokes->ref;
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  
  ierr = EContainerCreate(dmu,quadrature,&u_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dmp,quadrature,&p_space);CHKERRQ(ierr);
  
  dNuxi  = u_space->dNr1;
  dNueta = u_space->dNr2;
  dNux   = u_space->dNx1;
  dNuy   = u_space->dNx2;
  Np     = p_space->N;
  
  ue        = u_space->buf_basis_2vector_a;
  u_el_lidx = p_space->buf_basis_2index_a;
  Fe        = p_space->buf_basis_scalar_a;
  
  ierr = VecGetArray(Xuloc,&LA_Xu);CHKERRQ(ierr);
  ierr = VecGetArray(Xploc,&LA_Xp);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    /* [u,p] get element -> node map */
    elnidx_u = &elbasismap_u[nbasis_u*e];
    elnidx_p = &elbasismap_p[nbasis_p*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* get velocity dof indices */
    for (k=0; k<nbasis_p; k++) {
      PetscInt nidx = elnidx_p[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get velocity dofs */
    for (k=0; k<nbasis_u; k++) {
      PetscInt nidx = elnidx_u[k];
      
      ue[2*k+0] = LA_Xu[2*nidx+0];
      ue[2*k+1] = LA_Xu[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis_u,elcoords_g,dNuxi,dNueta,dNux,dNuy,&detJ_affine);
    
    ierr = PetscMemzero(Fe,sizeof(PetscReal)*nbasis_p);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,lform,divu;
      
      fac = detJ_affine * weight[q];
      
      /* compute div(u) */
      divu = 0.0;
      for (j=0; j<nbasis_p; j++) {
        divu += dNux[q][j] * ue[2*j];
        divu += dNuy[q][j] * ue[2*j+1];
      }
      
      lform = divu;
      
      for (i=0; i<nbasis_p; i++) {
        Fe[i] += fac * Np[q][i] * ( lform );
      }
    }
    
    ierr = VecSetValues(Floc,nbasis_p,elnidx_p,Fe,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecRestoreArray(Xploc,&LA_Xp);CHKERRQ(ierr);
  ierr = VecRestoreArray(Xuloc,&LA_Xu);CHKERRQ(ierr);
  
  ierr = VecAssemblyBegin(Floc);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(Floc);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmp,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmp,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(dmp,&Floc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(dms,&Xuloc,&Xploc);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&u_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&p_space);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
