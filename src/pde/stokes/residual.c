

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <pdestokes.h>
#include <pdestokesimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <dmbcs.h>
#include <fe_geometry_utils.h>

//#define LOG_MF_OP

PetscErrorCode MatMultMF_Stokes2d_B11(
                                      const PetscInt nubasis,
                                      const PetscReal FAC,const PetscReal eta_gp,
                                      const PetscReal Ux[],const PetscReal Uy[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],
                                      PetscReal Y[]);

PetscErrorCode MatMultMF_Stokes2d_B12(
                                      const PetscInt nubasis,const PetscInt npbasis,
                                      const PetscReal FAC,
                                      const PetscReal P[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],
                                      const PetscReal Np[],PetscReal Y[]);

PetscErrorCode MatMultMF_Stokes2d_B21(
                                      const PetscInt nubasis,const PetscInt npbasis,
                                      const PetscReal FAC,
                                      const PetscReal Ux[],const PetscReal Uy[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],const PetscReal Np[],
                                      PetscReal Y[]);


#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_U"
static PetscErrorCode FormFunctionLocal_U(PDEStokes *stokes,
                                          DM dau,PetscScalar ufield[],
                                          DM dap,PetscScalar pfield[],PetscScalar Ru[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**Nu,**dNuxi,**dNueta,**Np;
  PetscReal *detJ,**dNux,**dNuy;
  PetscReal fac;
  Coefficient eta,fu;
  Quadrature quadrature_eta,quadrature_fu;
  PetscReal *coeff_eta,*coeff_eta_e;
  PetscReal *coeff_fu,*coeff_fu_e;
  PetscInt ncomp_eta,ncomp_fu;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = stokes->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  eta = stokes->eta;
  fu = stokes->fu;

  ierr = CoefficientGetQuadrature(eta,&quadrature_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"eta",NULL,NULL,&ncomp_eta,&coeff_eta);CHKERRQ(ierr);

  ierr = CoefficientGetQuadrature(fu,&quadrature_fu);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_fu,"fu",NULL,NULL,&ncomp_fu,&coeff_fu);CHKERRQ(ierr);

  if (volQ->npoints != quadrature_eta->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  if (volQ->npoints != quadrature_fu->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  Nu     = stokes->Nu;
  dNuxi  = stokes->dNudxi;
  dNueta = stokes->dNudeta;
  dNux   = stokes->dNudx;
  dNuy   = stokes->dNudy;
  Np     = stokes->Np;
  
  nubasis = stokes->nubasis;
  npbasis = stokes->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Be);CHKERRQ(ierr);

  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);

  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,NULL,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }

    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];

    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }

    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }

    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Fe, sizeof(PetscScalar) * (nubasis*2) );
    PetscMemzero( Be, sizeof(PetscScalar) * (nubasis*2) );
    
    ierr = QuadratureGetElementValues(quadrature_eta,e,1,coeff_eta,&coeff_eta_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_fu,e,2,coeff_fu,&coeff_fu_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      MatMultMF_Stokes2d_B11(nubasis,fac,coeff_eta_e[q],Uxe,Uye,dNux[q],dNuy[q],Fe);
      MatMultMF_Stokes2d_B12(nubasis,npbasis,fac,Pe,dNux[q],dNuy[q],Np[q],Fe);
      
      /* compute body force terms here */
      for (k=0; k<nubasis; k++) {
        Be[2*k  ] += fac * Nu[q][k] * coeff_fu_e[2*q + 0];
        Be[2*k+1] += fac * Nu[q][k] * coeff_fu_e[2*q + 1];
      }
    }
    
    /* combine body force with A.x */
    for (k=0; k<nubasis; k++) {
      Fe[2*k  ] = Fe[2*k  ] - Be[2*k  ];
      Fe[2*k+1] = Fe[2*k+1] - Be[2*k+1];
    }
    
    /* sum into residual vector */
    for (k=0; k<nubasis; k++) {
      Ru[ u_el_lidx[2*k]   ] += Fe[2*k];
      Ru[ u_el_lidx[2*k+1] ] += Fe[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"StokesResidual(u): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(u_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Uxe);
  PetscFree(Uye);
  PetscFree(Fe);
  PetscFree(Be);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_P"
static PetscErrorCode FormFunctionLocal_P(PDEStokes *stokes,
                                          DM dau,PetscScalar ufield[],
                                          DM dap,PetscScalar pfield[],PetscScalar Rp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,**dNuxi,**dNueta,**Np;
  PetscReal *detJ,**dNux,**dNuy;
  PetscReal fac;
  Coefficient fp;
  Quadrature quadrature_fp;
  PetscReal *coeff_fp,*coeff_fp_e;
  PetscInt ncomp_fp;
  PetscLogDouble t0,t1;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = stokes->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  fp = stokes->fp;
  
  ierr = CoefficientGetQuadrature(fp,&quadrature_fp);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_fp,"fp",NULL,NULL,&ncomp_fp,&coeff_fp);CHKERRQ(ierr);
  
  if (volQ->npoints != quadrature_fp->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  dNuxi  = stokes->dNudxi;
  dNueta = stokes->dNudeta;
  dNux   = stokes->dNudx;
  dNuy   = stokes->dNudy;
  Np     = stokes->Np;
  
  nubasis = stokes->nubasis;
  npbasis = stokes->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Be);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,NULL,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,NULL,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nubasis*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[npbasis*e];
    
    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }
    
    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    /* initialise element stiffness matrix */
    PetscMemzero( Fe, sizeof(PetscScalar) * (npbasis) );
    PetscMemzero( Be, sizeof(PetscScalar) * (npbasis) );
    
    ierr = QuadratureGetElementValues(quadrature_fp,e,1,coeff_fp,&coeff_fp_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      //MatMultMF_Stokes2d_B11(nubasis,fac,coeff_eta_e[q],Uxe,Uye,dNux[q],dNuy[q],Fe);
      //MatMultMF_Stokes2d_B12(nubasis,npbasis,fac,Pe,dNux[q],dNuy[q],Np[q],Fe);
      MatMultMF_Stokes2d_B21(nubasis,npbasis,fac,Uxe,Uye,dNux[q],dNuy[q],Np[q],Fe);
      
      /* compute body force terms here */
      for (k=0; k<npbasis; k++) {
        Be[k] += fac * Np[q][k] * coeff_fp_e[q];
      }
    }
    
    /* combine body force with A.x */
    for (k=0; k<npbasis; k++) {
      Fe[k] = Fe[k] - Be[k];
    }
    
    /* sum into residual vector */
    for (k=0; k<npbasis; k++) {
      Rp[ p_el_lidx[k]   ] += Fe[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"StokesResidual(p): %1.4e (sec)\n",t1-t0);
#endif
  
  PetscFree(u_el_lidx);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(Pe);
  PetscFree(Uxe);
  PetscFree(Uye);
  PetscFree(Fe);
  PetscFree(Be);
  
  PetscFunctionReturn(0);
}

/*
 Computes r = Ax - b
 SNES will scale by -1, F = -r = b - Ax
 Thus, in OUR function, dirichlet slots become A_ii(x_i - phi)
 In SNES, these become A_ii(phi-x_i), and the updates on the dirichlet slots will be
 A_ii d_i = -F_i
 = A_ii(phi-x_i)
 Then the update will be
 x_i^new = x_i + d_i
 = x_i + inv(A_ii) A_ii(phi-x_i)
 = x_i + phi - x_i
 = phi
 */
#undef __FUNCT__
#define __FUNCT__ "FormFunction_Stokes"
PetscErrorCode FormFunction_Stokes(SNES snes,Vec X,Vec F,void *ctx)
{
  PetscErrorCode    ierr;
  PDE               pde = (PDE)ctx;
  PDEStokes         *data;
  DM                stokes_pack,dau,dap;
  Vec               Uloc,Ploc,FUloc,FPloc;
  Vec               u,p,Fu,Fp;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscScalar       *LA_FUloc,*LA_FPloc;
  BCList            u_bclist;
  
  PetscFunctionBegin;
  
  data = (PDEStokes*)pde->data;
  ierr = PDEStokesGetDM(pde,&stokes_pack);CHKERRQ(ierr);
  ierr = PDEStokesGetBCLists(pde,&u_bclist,NULL);CHKERRQ(ierr); /* need getter for this */
  
  ierr = DMCompositeGetEntries(stokes_pack,&dau,&dap);CHKERRQ(ierr);
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  //ierr = DMCompositeGetLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(stokes_pack,&FUloc,&FPloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMCompositeScatter(stokes_pack,X,Uloc,Ploc);CHKERRQ(ierr);
  /* insert boundary conditions into local vectors */
  ierr = BCListInsertLocal(u_bclist,Uloc);CHKERRQ(ierr);
  
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(FUloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(FPloc);CHKERRQ(ierr);
  ierr = VecGetArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
  ierr = VecGetArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->eta);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fu);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fp);CHKERRQ(ierr);
  
  /* momentum */
  ierr = FormFunctionLocal_U(data,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
  
  /* continuity */
  ierr = FormFunctionLocal_P(data,dau,LA_Uloc,dap,LA_Ploc,LA_FPloc);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  ierr = DMCompositeGather(stokes_pack,F,ADD_VALUES,FUloc,FPloc);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreLocalVectors(stokes_pack,&FUloc,&FPloc);CHKERRQ(ierr);
  //ierr = DMCompositeRestoreLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  
  /* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
  ierr = DMCompositeGetAccess(stokes_pack,F,&Fu,&Fp);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(stokes_pack,X,&u,&p);CHKERRQ(ierr);
  
  ierr = BCListResidualDirichlet(u_bclist,u,Fu);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreAccess(stokes_pack,X,&u,&p);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(stokes_pack,F,&Fu,&Fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
