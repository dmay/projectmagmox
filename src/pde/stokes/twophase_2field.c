
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <dmbcs.h>

#include <pdestokesimpl.h>
#include <pdestokes.h>
#include <pdestokesdarcy2field.h>

const char twofield_description[] =
"\"PDE Stokes\" solves the PDE \n"
"    div ( 2 eta symgrad(u) ) - grad(p) + x = f(x) \n"
"                              - div(u) + x = g(x) \n"
"THIS NEEDS COMPLETING - JUST A STUB";


/* stokes methods we need to use */
PetscErrorCode PDECreate_Stokes(PDE pde);
PetscErrorCode PDEConfigureSNES_Stokes(PDE pde,SNES snes,Vec F,Mat A,Mat B);
PetscErrorCode PDEUpdateLocal_Stokes(PDE pde);
PetscErrorCode PDEDestroy_Stokes(PDE pde);
PetscErrorCode PDEStokesGetDM(PDE pde,DM *dms);

PetscErrorCode FormFunction_StkDcy2Field(SNES snes,Vec X,Vec F,void *ctx);
PetscErrorCode FormJacobian_StkDcy2Field(SNES snes,Vec X,Mat A,Mat B,void *ctx);

#undef __FUNCT__
#define __FUNCT__ "PDECreate_TwoPhase2Field"
PetscErrorCode PDECreate_TwoPhase2Field(PDE pde)
{
  PetscErrorCode ierr;
  MPI_Comm comm;
  PDEStokes *data;
  
  PetscFunctionBegin;
  ierr = PDECreate_Stokes(pde);CHKERRQ(ierr);
  ierr = PetscObjectChangeTypeName((PetscObject)pde,PDETypeNames[(int)PDESTKDCY2F]);CHKERRQ(ierr);
  ierr = PetscStrallocpy(twofield_description,&pde->description);CHKERRQ(ierr);
  
  pde->ops->configure_snes = PDEConfigureSNES_Stokes;
  pde->ops->form_function = FormFunction_StkDcy2Field;
  pde->ops->form_jacobian = FormJacobian_StkDcy2Field;
  pde->ops->destroy = PDEDestroy_Stokes;
  pde->ops->update_local_solution = PDEUpdateLocal_Stokes;

  /* Peform setup for Darcy velocity here - this is required as it was not performed by PDECreate_Stokes() */
  /* create coefficient for Darcy velocity neumann BC */
  ierr = PetscObjectGetComm((PetscObject)pde,&comm);CHKERRQ(ierr);
  data = (PDEStokes*)pde->data;
  ierr = CoefficientCreate(comm,&data->q_N);CHKERRQ(ierr);
  ierr = CoefficientSetPDE(data->q_N,pde);CHKERRQ(ierr);
  
  {
    Quadrature quadrature;
    
    ierr = CoefficientSetType(data->q_N,COEFF_QUADRATURE);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(data->q_N,&quadrature);CHKERRQ(ierr);
    /* set default method to jam zeros into the quadrature values */
    ierr = CoefficientSetComputeQuadratureEmpty(data->q_N);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesDarcy2FieldSetData"
PetscErrorCode PDEStokesDarcy2FieldSetData(PDE pde,DM dmu,DM dmp,BCList ubc,BCList pbc)
{
  PetscErrorCode ierr;
  PDEStokes *data;

  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;

  ierr = PDEStokesSetData(pde,dmu,dmp,ubc);CHKERRQ(ierr);
  //data->p_dirichlet = pbc;

  {
    Quadrature quadrature;
    PetscInt nelements;
    
    ierr = CoefficientGetQuadrature(data->eta,&quadrature);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(data->dmu,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"zeta",1);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"zeta*",1);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"k*",1);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"e_3",2);CHKERRQ(ierr);
  }
  {
    Quadrature quadrature;
    BFacetList bfacet_list;
    PetscInt nfacets;
    
    ierr = CoefficientGetQuadrature(data->q_N,&quadrature);CHKERRQ(ierr);
    
    ierr = QuadratureSetUpFromDM_Facet(quadrature,dmp);CHKERRQ(ierr);
    
    ierr = DMTetGetGeometryBFacetList(dmp,&bfacet_list);CHKERRQ(ierr);
    ierr = BFacetListGetSizes(bfacet_list,&nfacets,NULL);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nfacets,"q_N",2);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesDarcy2FieldGetCoefficients"
PetscErrorCode PDEStokesDarcy2FieldGetCoefficients(PDE pde,Coefficient *eta,Coefficient *fu,Coefficient *fp,Coefficient *qn)
{
  PetscErrorCode ierr;
  PDEStokes *data;

  data = (PDEStokes*)pde->data;
  ierr = PDEStokesGetCoefficients(pde,eta,fu,fp);CHKERRQ(ierr);
  if (qn) { *qn = data->q_N; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEStokesDarcy2FieldGetDM"
PetscErrorCode PDEStokesDarcy2FieldGetDM(PDE pde,DM *dms)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = PDEStokesGetDM(pde,dms);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 Project the term 
   k/phi (grad(p) - e_3)
 onto the velocity function space
*/
#undef __FUNCT__
#define __FUNCT__ "PDEStokesDarcy2FieldComputeFluidVelocityU"
PetscErrorCode PDEStokesDarcy2FieldComputeFluidVelocityU(PDE pde,Vec *_uf)
{
  PetscErrorCode ierr;
  DM dms,dmu,dmp;
  Vec X,Xu,Xp,uf;
  Vec g_hat,N_g;
  Mat M;
  KSP ksp;
  KSPConvergedReason reason;
  
  PetscFunctionBegin;
  ierr = PDEStokesDarcy2FieldGetDM(pde,&dms);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);

  /* create vector to store the fluid velocity if one doesn't exist */
  if (*_uf == NULL) {
    ierr = DMCreateGlobalVector(dmu,&uf);CHKERRQ(ierr);
  } else {
    uf = *_uf;
  }
  
  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dms,X,&Xu,&Xp);CHKERRQ(ierr);
  ierr = VecDuplicate(Xu,&N_g);CHKERRQ(ierr);
  ierr = VecDuplicate(Xu,&g_hat);CHKERRQ(ierr);

  ierr = AssembleBForm_VectorMassMatrix(dmu,2,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  ierr = StkDarcyFluidVelocityProjectU_RHS(pde,dms,dmu,dmp,Xp,N_g);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"proj_");CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,M,M);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,N_g,g_hat);CHKERRQ(ierr);
  ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
  if (reason == KSP_DIVERGED_NANORINF) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Projection used to compute the liquid velocity failed due to NaN in the RHS - likely k*/phi is infinity");

  ierr = VecCopy(Xu,uf);CHKERRQ(ierr);
  ierr = VecAXPY(uf,-1.0,g_hat);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreAccess(dms,X,&Xu,&Xp);CHKERRQ(ierr);
  
  ierr = VecDestroy(&g_hat);CHKERRQ(ierr);
  ierr = VecDestroy(&N_g);CHKERRQ(ierr);
  ierr = MatDestroy(&M);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  
  *_uf = uf;
  
  PetscFunctionReturn(0);
}

/*
 Project the term
   u_s - k/phi (grad(p) - e_3)
 onto the pressure function space
*/
#undef __FUNCT__
#define __FUNCT__ "PDEStokesDarcy2FieldComputeFluidVelocityP"
PetscErrorCode PDEStokesDarcy2FieldComputeFluidVelocityP(PDE pde,Vec *_uf)
{
  PetscErrorCode ierr;
  DM dms,dmu,dmp;
  Vec X,Xu,Xp,uf;
  Vec N_g;
  Mat M;
  KSP ksp_p;
  KSPConvergedReason reason;
  
  PetscFunctionBegin;
  ierr = PDEStokesDarcy2FieldGetDM(pde,&dms);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);
  
  /* create vector to store the fluid velocity if one doesn't exist */
  if (*_uf == NULL) {
    DM dm_vector;
    
    ierr = DMTetCreateSharedSpace(dmp,2,&dm_vector);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(dm_vector,&uf);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_vector);CHKERRQ(ierr);
  } else {
    uf = *_uf;
  }
  
  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dms,X,&Xu,&Xp);CHKERRQ(ierr);
  ierr = VecDuplicate(uf,&N_g);CHKERRQ(ierr);
  
  ierr = AssembleBForm_VectorMassMatrix(dmp,2,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  ierr = StkDarcyFluidVelocityProjectP_RHS(pde,dms,dmu,dmp,Xu,Xp,N_g);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp_p);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp_p,"proj_");CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp_p);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp_p,M,M);CHKERRQ(ierr);
  ierr = KSPSolve(ksp_p,N_g,uf);CHKERRQ(ierr);
  ierr = KSPGetConvergedReason(ksp_p,&reason);CHKERRQ(ierr);
  
  if (reason == KSP_DIVERGED_NANORINF) {
    Coefficient coeff_eta;
    PetscViewer viewer;
    
    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"PDEStokesDarcy2FieldComputeFluidVelocityP-proj.matvec",&viewer);CHKERRQ(ierr);
    ierr = MatView(M,viewer);CHKERRQ(ierr);
    ierr = VecView(N_g,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    
    ierr = PDEStokesDarcy2FieldGetCoefficients(pde,&coeff_eta,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientView_GP(coeff_eta,dmp,"PDEStokesDarcy2FieldComputeFluidVelocityP-error");CHKERRQ(ierr);
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Projection used to compute the liquid velocity failed due to NaN in the RHS - likely k*/phi is infinity");
  }
  
  ierr = DMCompositeRestoreAccess(dms,X,&Xu,&Xp);CHKERRQ(ierr);
  
  ierr = VecDestroy(&N_g);CHKERRQ(ierr);
  ierr = MatDestroy(&M);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp_p);CHKERRQ(ierr);
  
  *_uf = uf;
  
  PetscFunctionReturn(0);
}

/*
 Project the term
 div(u_s)
 onto the pressure function space
 */
#undef __FUNCT__
#define __FUNCT__ "PDEStokesDarcy2FieldComputeDivSolidVelocityP"
PetscErrorCode PDEStokesDarcy2FieldComputeDivSolidVelocityP(PDE pde,Vec *_divu)
{
  PetscErrorCode ierr;
  DM dms,dmu,dmp;
  Vec X,Xu,Xp,divu;
  Vec N_g;
  Mat M;
  KSP ksp_p;
  KSPConvergedReason reason;

  PetscFunctionBegin;
  ierr = PDEStokesDarcy2FieldGetDM(pde,&dms);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dms,&dmu,&dmp);CHKERRQ(ierr);
  
  /* create vector to store the fluid velocity if one doesn't exist */
  if (*_divu == NULL) {
    ierr = DMCreateGlobalVector(dmp,&divu);CHKERRQ(ierr);
  } else {
    divu = *_divu;
  }
  
  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dms,X,&Xu,&Xp);CHKERRQ(ierr);
  ierr = VecDuplicate(divu,&N_g);CHKERRQ(ierr);
  
  ierr = AssembleBForm_VectorMassMatrix(dmp,1,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  ierr = StkDarcyDivVelocityProjectP_RHS(pde,dms,dmu,dmp,Xu,Xp,N_g);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp_p);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp_p,"proj_");CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp_p);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp_p,M,M);CHKERRQ(ierr);
  ierr = KSPSolve(ksp_p,N_g,divu);CHKERRQ(ierr);
  ierr = KSPGetConvergedReason(ksp_p,&reason);CHKERRQ(ierr);
  if (reason == KSP_DIVERGED_NANORINF) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Projection used to compute div(v_s) failed due to NaN in the RHS");
  
  ierr = DMCompositeRestoreAccess(dms,X,&Xu,&Xp);CHKERRQ(ierr);
  
  ierr = VecDestroy(&N_g);CHKERRQ(ierr);
  ierr = MatDestroy(&M);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp_p);CHKERRQ(ierr);
  
  *_divu = divu;
  
  PetscFunctionReturn(0);
}
