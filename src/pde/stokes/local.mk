libproj-y.c += $(call thisdir, \
    pdestokes.c \
    pdestokes_view.c \
    forms.c \
    mfop_a12.c mfop_a21.c \
    operators.c \
    residual.c \
    bilinear_forms.c \
    twophase_2field.c forms_2field.c residual_2field.c bilinear_forms_2field.c twophase_uf.c \
)

