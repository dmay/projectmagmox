
#include <petsc.h>

PetscErrorCode MatMultMF_Stokes2d_B(
  const PetscInt nubasis,const PetscInt npbasis,
  const PetscReal FAC,const PetscReal eta_gp,
  const PetscReal Ux[],const PetscReal Uy[],const PetscReal P[],
  const PetscReal dNudx[],const PetscReal dNudy[],
  const PetscReal Np[],PetscReal Y[])
{
  const PetscInt nsd = 2;
  PetscInt       iu,ip,k;
  PetscReal    p_gp = 0.0;
  PetscReal    strain_rate_gp[] = {0.0,0.0,0.0};
  PetscReal    tau_gp[] = {0.0,0.0,0.0};
  PetscReal    div_gp = 0.0;
  PetscReal    te,tmp;
  
  
  // pressure at gauss point //
  for (ip=0; ip<npbasis; ip++) {
    p_gp += Np[ip]*P[ip];
  }
  
  // strain-rate (e_xx,e_yy,2.e_xy,) at gauss point //
  for (iu=0; iu<nubasis; iu++) {
    strain_rate_gp[0] += Ux[iu]*dNudx[iu];
    strain_rate_gp[1] += Uy[iu]*dNudy[iu];
    strain_rate_gp[2] += Ux[iu]*dNudy[iu] + Uy[iu]*dNudx[iu];
  }
  
  // deviatoric stress (t_xx,t_yy,t_xy) at gauss point //
  te = 2.0 * eta_gp;
  tau_gp[0] = te*strain_rate_gp[0];
  tau_gp[1] = te*strain_rate_gp[1];
  tau_gp[2] = eta_gp*strain_rate_gp[2];
  
  // divergence at gauss point //
  for (k=0; k<2; k++) {
    div_gp += strain_rate_gp[k];
  }
  
  // y = A11.u + A12.p at gauss point //
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[idx]   += FAC*(dNudx[iu]*tau_gp[0] + dNudy[iu]*tau_gp[2] - dNudx[iu]*p_gp);
    Y[++idx] += FAC*(dNudx[iu]*tau_gp[2] + dNudy[iu]*tau_gp[1] - dNudy[iu]*p_gp);
  }
  
  // y = A21.u at gauss point //
  tmp = -FAC * div_gp;
  for (ip=0; ip<npbasis; ip++) {
    PetscInt idx = nsd*nubasis+ip;
    Y[idx] += tmp*Np[ip];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode MatMultMF_Stokes2d_B11(
  const PetscInt nubasis,
  const PetscReal FAC,const PetscReal eta_gp,
  const PetscReal Ux[],const PetscReal Uy[],
  const PetscReal dNudx[],const PetscReal dNudy[],
  PetscReal Y[])
{
  PetscInt     iu;
  PetscReal    strain_rate_gp[] = {0.0,0.0,0.0};
  PetscReal    tau_gp[] = {0.0,0.0,0.0};
  PetscReal    te;
  
  
  // strain-rate (e_xx,e_yy,2.e_xy) at gauss point //
  for (iu=0; iu<nubasis; iu++) {
    strain_rate_gp[0] += Ux[iu]*dNudx[iu];
    strain_rate_gp[1] += Uy[iu]*dNudy[iu];
    strain_rate_gp[2] += Ux[iu]*dNudy[iu] + Uy[iu]*dNudx[iu];
  }

  //PetscReal div = strain_rate_gp[0] + strain_rate_gp[1];
  //PetscReal dev = (1.0/3.0) * ( div );
  // enforce strain-rate is deviatoric //
  //strain_rate_gp[0] -= dev;
  //strain_rate_gp[1] -= dev;

  // deviatoric stress (t_xx,t_yy,t_xy) at gauss point //
  te = 2.0 * eta_gp;
  tau_gp[0] = te*strain_rate_gp[0] * FAC;
  tau_gp[1] = te*strain_rate_gp[1] * FAC;
  tau_gp[2] = eta_gp*strain_rate_gp[2] * FAC;
  
  // y = A11.u at gauss point //
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[idx]   += (dNudx[iu]*tau_gp[0] + dNudy[iu]*tau_gp[2]);
    Y[++idx] += (dNudx[iu]*tau_gp[2] + dNudy[iu]*tau_gp[1]);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode MatMultMF_Stokes2d_B12(
  const PetscInt nubasis,const PetscInt npbasis,
  const PetscReal FAC,
  const PetscReal P[],
  const PetscReal dNudx[],const PetscReal dNudy[],
  const PetscReal Np[],PetscReal Y[])
{
  PetscInt     iu,ip;
  PetscReal    p_gp = 0.0,tmp;
  
  
  // pressure at gauss point //
  for (ip=0; ip<npbasis; ip++) {
    p_gp += Np[ip]*P[ip];
  }
  
  // y = A12.p at gauss point //
  tmp = -FAC * p_gp;
  for (iu=0; iu<nubasis; iu++) {
    PetscInt idx = 2*iu;
    Y[  idx] += tmp*dNudx[iu];
    Y[++idx] += tmp*dNudy[iu];
  }
  PetscFunctionReturn(0);
}

PetscErrorCode MatMultMF_Stokes2d_B21(
  const PetscInt nubasis,const PetscInt npbasis,
  const PetscReal FAC,
  const PetscReal Ux[],const PetscReal Uy[],
  const PetscReal dNudx[],const PetscReal dNudy[],const PetscReal Np[],
  PetscReal Y[])
{
  PetscInt     iu,ip;
  PetscReal    strain_rate_gp[] = {0.0,0.0,0.0};
  PetscReal    div_gp,tmp;
  
  
  // strain-rate (e_xx,e_yy,2.e_xy) at gauss point //
  for (iu=0; iu<nubasis; iu++) {
    strain_rate_gp[0] += Ux[iu]*dNudx[iu];
    strain_rate_gp[1] += Uy[iu]*dNudy[iu];
  }
  
  // divergence at gauss point //
  div_gp = strain_rate_gp[0] + strain_rate_gp[1];
  
  // y = A21.u at gauss point //
  tmp = -FAC*div_gp;
  for (ip=0; ip<npbasis; ip++) {
    Y[ip] += tmp*Np[ip];
  }
  PetscFunctionReturn(0);
}


/* Generic forms */
inline void lform_mf__pt_p(
  const PetscReal fac,
  const PetscInt ntbasis,
    const PetscReal Nt[],const PetscReal dNt_x[],const PetscReal dNt_y[],const PetscReal dNt_z[],
    PetscReal y[],
  const PetscInt nbasis,
    const PetscReal N[],const PetscReal dN_x[],const PetscReal dN_y[],const PetscReal dN_z[],
    const PetscReal x[])
{
  PetscInt i,j;
  PetscReal sum = 0.0;
  
  for (j=0; j<nbasis; j++) {
    sum += N[j] * x[j];
  }
  for (i=0; i<ntbasis; i++) {
    y[i] +=  fac * Nt[i] * sum;
  }
}

inline void lform_mf__grad_pt_a_grad_p(
                          const PetscReal fac,const PetscReal a[],
                          const PetscInt ntbasis,
                          const PetscReal Nt[],const PetscReal dNt_x[],const PetscReal dNt_y[],const PetscReal dNt_z[],
                          PetscReal y[],
                          const PetscInt nbasis,
                          const PetscReal N[],const PetscReal dN_x[],const PetscReal dN_y[],const PetscReal dN_z[],
                          const PetscReal x[])
{
  PetscInt i,j;
  PetscReal grad[] = {0.0,0.0};
  
  for (j=0; j<nbasis; j++) {
    grad[0] += dN_x[j] * x[2*j  ];
    grad[1] += dN_y[j] * x[2*j+1];
  }
  grad[0] = a[0] * grad[0];
  grad[1] = a[0] * grad[1];
  for (i=0; i<ntbasis; i++) {
    y[i] +=  fac * ( dNt_x[i] * grad[0] + dNt_y[i] * grad[1] );
  }
}

inline void lform_mf__grad_pt_aij_grad_p(
                                       const PetscReal fac,const PetscReal a[],
                                       const PetscInt ntbasis,
                                       const PetscReal Nt[],const PetscReal dNt_x[],const PetscReal dNt_y[],const PetscReal dNt_z[],
                                       PetscReal y[],
                                       const PetscInt nbasis,
                                       const PetscReal N[],const PetscReal dN_x[],const PetscReal dN_y[],const PetscReal dN_z[],
                                       const PetscReal x[])
{
  PetscInt i,j;
  PetscReal grad[] = {0.0,0.0};
  PetscReal f[2];
  
  for (j=0; j<nbasis; j++) {
    grad[0] += dN_x[j] * x[2*j  ];
    grad[1] += dN_y[j] * x[2*j+1];
  }
  f[0] = a[0] * grad[0] + a[1] * grad[1];
  f[1] = a[2] * grad[0] + a[3] * grad[1];
  for (i=0; i<ntbasis; i++) {
    y[i] +=  fac * ( dNt_x[i] * f[0] + dNt_y[i] * f[1] );
  }
}

inline void lform_mf__div_Vt_p(
                                         const PetscReal fac,const PetscReal a[],
                                         const PetscInt ntbasis,
                                         const PetscReal Nt[],const PetscReal dNt_x[],const PetscReal dNt_y[],const PetscReal dNt_z[],
                                         PetscReal y[],
                                         const PetscInt nbasis,
                                         const PetscReal N[],const PetscReal dN_x[],const PetscReal dN_y[],const PetscReal dN_z[],
                                         const PetscReal x[])
{
  PetscInt i,j;
  PetscReal sum = 0.0;
  
  for (j=0; j<nbasis; j++) {
    sum += N[j] * x[j];
  }
  for (i=0; i<ntbasis; i++) {
    y[2*i  ] +=  fac * ( dNt_x[i] * sum );
    y[2*i+1] +=  fac * ( dNt_y[i] * sum );
  }
}

inline void lform_mf__pt_div_V(
                               const PetscReal fac,const PetscReal a[],
                               const PetscInt ntbasis,
                               const PetscReal Nt[],const PetscReal dNt_x[],const PetscReal dNt_y[],const PetscReal dNt_z[],
                               PetscReal y[],
                               const PetscInt nbasis,
                               const PetscReal N[],const PetscReal dN_x[],const PetscReal dN_y[],const PetscReal dN_z[],
                               const PetscReal x[])
{
  PetscInt i,j;
  PetscReal sum = 0.0;
  
  for (j=0; j<nbasis; j++) {
    sum += dN_x[j] * x[2*j  ];
    sum += dN_y[j] * x[2*j+1];
  }
  for (i=0; i<ntbasis; i++) {
    y[i] +=  fac * ( Nt[i] * sum );
  }
}

inline void lform_mf__grad_Vt_a_symgrad_V(
                               const PetscReal fac,const PetscReal a[],
                               const PetscInt ntbasis,
                               const PetscReal Nt[],const PetscReal dNt_x[],const PetscReal dNt_y[],const PetscReal dNt_z[],
                               PetscReal y[],
                               const PetscInt nbasis,
                               const PetscReal N[],const PetscReal dN_x[],const PetscReal dN_y[],const PetscReal dN_z[],
                               const PetscReal x[])
{
  PetscInt i,j;
  PetscReal sum = 0.0;
  
  for (j=0; j<nbasis; j++) {
    sum += dN_x[j] * x[2*j  ];
    sum += dN_y[j] * x[2*j+1];
  }
  for (i=0; i<ntbasis; i++) {
    y[i] +=  fac * ( Nt[i] * sum );
  }
}
