

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdeimpl.h>
#include <pdestokes.h>
#include <pdestokesimpl.h>
#include <coefficient.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <dmbcs.h>
#include <fe_geometry_utils.h>
#include <element_container.h>

//#define LOG_MF_OP

PetscErrorCode MatMultMF_StkDcy2Field2d_B11dev(const PetscInt nubasis,
                                               const PetscReal fac,const PetscReal eta,const PetscReal alpha,
                                               const PetscReal ux[],const PetscReal uy[],
                                               const PetscReal dNudx[],const PetscReal dNudy[],
                                               PetscReal y[]);

PetscErrorCode MatMultMF_Stokes2d_B12(const PetscInt nubasis,const PetscInt npbasis,
                                      const PetscReal FAC,
                                      const PetscReal p[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],
                                      const PetscReal Np[],PetscReal Y[]);

PetscErrorCode MatMultMF_Stokes2d_B21(const PetscInt nubasis,const PetscInt npbasis,
                                      const PetscReal FAC,
                                      const PetscReal Ux[],const PetscReal Uy[],
                                      const PetscReal dNudx[],const PetscReal dNudy[],const PetscReal Np[],
                                      PetscReal Y[]);

PetscErrorCode MatMultMF_StkDcy2Field2d_B22(const PetscInt npbasis,
                                            const PetscReal fac,const PetscReal beta,
                                            const PetscReal p[],
                                            const PetscReal dNpdx[],const PetscReal dNpdy[],
                                            PetscReal y[]);

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_Us"
static PetscErrorCode FormFunctionLocal_Us(PDEStokes *stokes,
                                          DM dau,PetscScalar ufield[],
                                          DM dap,PetscScalar pfield[],PetscScalar Ru[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p;
  PetscReal *coords,*elcoords;
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,*detJ;
  PetscReal **Nu,**dNuxi,**dNueta,**dNux,**dNuy,**Np;
  PetscReal fac;
  Coefficient eta,fu;
  Quadrature quadrature_eta,quadrature_fu;
  PetscReal *coeff_eta,*coeff_eta_e,*coeff_zeta_star,*coeff_zeta_star_e;
  PetscReal *coeff_fu,*coeff_fu_e;
  PetscInt ncomp_eta,ncomp_zeta_star,ncomp_fu;
  PetscLogDouble t0,t1;
  EContainer v_space,q_space;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = stokes->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  eta = stokes->eta;
  fu = stokes->fu;

  ierr = CoefficientGetQuadrature(eta,&quadrature_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"eta",NULL,NULL,&ncomp_eta,&coeff_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"zeta*",NULL,NULL,&ncomp_zeta_star,&coeff_zeta_star);CHKERRQ(ierr);

  ierr = CoefficientGetQuadrature(fu,&quadrature_fu);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_fu,"fu",NULL,NULL,&ncomp_fu,&coeff_fu);CHKERRQ(ierr);

  if (volQ->npoints != quadrature_eta->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  if (volQ->npoints != quadrature_fu->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  ierr = EContainerCreate(dau,volQ,&v_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dap,volQ,&q_space);CHKERRQ(ierr);
  Nu     = v_space->N;
  dNuxi  = v_space->dNr1;
  dNueta = v_space->dNr2;
  dNux   = v_space->dNx1;
  dNuy   = v_space->dNx2;
  Np     = q_space->N;
  
  nubasis = stokes->nubasis;
  npbasis = stokes->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis*2,&Be);CHKERRQ(ierr);

  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);

  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,NULL);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nen_u*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }

    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[nen_p*e];

    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }

    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }

    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    
    /* initialise element stiffness matrix */
    ierr = PetscMemzero( Fe, sizeof(PetscScalar) * (nubasis*2) );CHKERRQ(ierr);
    ierr = PetscMemzero( Be, sizeof(PetscScalar) * (nubasis*2) );CHKERRQ(ierr);
    
    ierr = QuadratureGetElementValues(quadrature_eta,e,1,coeff_eta,&coeff_eta_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_eta,e,1,coeff_zeta_star,&coeff_zeta_star_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_fu,e,2,coeff_fu,&coeff_fu_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      MatMultMF_StkDcy2Field2d_B11dev(nubasis,fac,coeff_eta_e[q],coeff_zeta_star_e[q],Uxe,Uye,dNux[q],dNuy[q],Fe);
      MatMultMF_Stokes2d_B12(nubasis,npbasis,fac,Pe,dNux[q],dNuy[q],Np[q],Fe);
      
      /* compute body force terms here */
      for (k=0; k<nubasis; k++) {
        Be[2*k  ] += fac * Nu[q][k] * coeff_fu_e[2*q + 0];
        Be[2*k+1] += fac * Nu[q][k] * coeff_fu_e[2*q + 1];
      }
    }
    
    /* combine body force with A.x */
    for (k=0; k<nubasis; k++) {
      Fe[2*k  ] = Fe[2*k  ] + Be[2*k  ];
      Fe[2*k+1] = Fe[2*k+1] + Be[2*k+1];
    }
    
    /* sum into residual vector */
    for (k=0; k<nubasis; k++) {
      Ru[ u_el_lidx[2*k]   ] += Fe[2*k];
      Ru[ u_el_lidx[2*k+1] ] += Fe[2*k+1];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"[StkDcy2Field] Residual(u): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = EContainerDestroy(&q_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = PetscFree(u_el_lidx);CHKERRQ(ierr);
  ierr = PetscFree(detJ);CHKERRQ(ierr);
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  ierr = PetscFree(Pe);CHKERRQ(ierr);
  ierr = PetscFree(Uxe);CHKERRQ(ierr);
  ierr = PetscFree(Uye);CHKERRQ(ierr);
  ierr = PetscFree(Fe);CHKERRQ(ierr);
  ierr = PetscFree(Be);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_Ps"
static PetscErrorCode FormFunctionLocal_Ps(PDEStokes *stokes,
                                          DM dau,PetscScalar ufield[],
                                          DM dap,PetscScalar pfield[],PetscScalar Rp[])
{
  Quadrature volQ;
  PetscErrorCode ierr;
  PetscInt q,nqp,k;
  PetscInt nel,nen_u,nen_p,e,nubasis,npbasis;
  PetscInt *elnidx_u,*elnidx_p,*elnidx_g;
  PetscReal *coords,*elcoords,*coords_g,elcoords_g[3*2];
  PetscReal *Uxe,*Uye,*Pe,*Fe,*Be;
  PetscInt  *u_el_lidx,*p_el_lidx;
  PetscReal *WEIGHT,*XI,*detJ;
  PetscReal **dNuxi,**dNueta,**dNux,**dNuy;
  PetscReal **dNpxi,**dNpeta,**dNpx,**dNpy,**Np;
  PetscReal fac,detJ_affine;
  Coefficient eta,fp;
  Quadrature quadrature_eta,quadrature_fp;
  PetscReal *coeff_k_phi,*coeff_k_phi_e;
  PetscReal *coeff_fp,*coeff_fp_e;
  PetscReal *coeff_e3,*coeff_e3_e;
  PetscInt ncomp_k,ncomp_fp,ncomp_e3;
  PetscLogDouble t0,t1;
  EContainer v_space,q_space;
  
  PetscFunctionBegin;
  /* quadrature */
  volQ = stokes->ref;
  ierr = QuadratureGetRule(volQ,&nqp,&XI,&WEIGHT);CHKERRQ(ierr);
  // GetCoefficients //
  eta = stokes->eta;
  fp  = stokes->fp;
  
  ierr = CoefficientGetQuadrature(eta,&quadrature_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"k*",NULL,NULL,&ncomp_k,&coeff_k_phi);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(fp,&quadrature_fp);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_fp,"fp",NULL,NULL,&ncomp_fp,&coeff_fp);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(eta,&quadrature_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_eta,"e_3",NULL,NULL,&ncomp_e3,&coeff_e3);CHKERRQ(ierr);
  
  if (volQ->npoints != quadrature_eta->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  if (volQ->npoints != quadrature_fp->npoints) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Method doesn't support residual evaluation with mixed integration rules");
  
  ierr = EContainerCreate(dau,volQ,&v_space);CHKERRQ(ierr);
  ierr = EContainerCreate(dap,volQ,&q_space);CHKERRQ(ierr);
  dNuxi  = v_space->dNr1;
  dNueta = v_space->dNr2;
  dNux   = v_space->dNx1;
  dNuy   = v_space->dNx2;
  Np     = q_space->N;
  dNpxi  = q_space->dNr1;
  dNpeta = q_space->dNr2;
  dNpx   = q_space->dNx1;
  dNpy   = q_space->dNx2;
  
  
  nubasis = stokes->nubasis;
  npbasis = stokes->npbasis;
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&u_el_lidx);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uxe);CHKERRQ(ierr);
  ierr = PetscMalloc1(nubasis,&Uye);CHKERRQ(ierr);

  ierr = PetscMalloc1(npbasis,&Fe);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Be);CHKERRQ(ierr);
  ierr = PetscMalloc1(npbasis,&Pe);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nubasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dau,&nel,NULL,&elnidx_u,&nen_u,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dap,&nel,NULL,&elnidx_p,&nen_p,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dau,&nel,NULL,&elnidx_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  PetscTime(&t0);
  for (e=0; e<nel; e++) {
    PetscInt *idx;
    
    /* get velocity dof indices */
    idx = &elnidx_u[nen_u*e];
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      u_el_lidx[2*k + 0] = 2 * nidx + 0;
      u_el_lidx[2*k + 1] = 2 * nidx + 1;
    }
    
    /* get pressure dof indices */
    p_el_lidx = &elnidx_p[nen_p*e];
    
    /* get coordinates */
    for (k=0; k<3; k++) {
      PetscInt nidx = elnidx_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }

    /* get coordinates */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx = idx[k];
      
      elcoords[2*k + 0] = coords[2*nidx+0];
      elcoords[2*k + 1] = coords[2*nidx+1];
    }
    
    /* get velocity dofs */
    for (k=0; k<nubasis; k++) {
      PetscInt nidx_u = u_el_lidx[2*k];
      PetscInt nidx_v = u_el_lidx[2*k+1];
      
      Uxe[k] = ufield[nidx_u];
      Uye[k] = ufield[nidx_v];
    }
    
    /* get pressure dofs */
    for (k=0; k<npbasis; k++) {
      PetscInt nidx = p_el_lidx[k];
      
      Pe[k] = pfield[nidx];
    }
    
    //EvaluateBasisGeometryDerivatives(nqp,nubasis,elcoords,dNuxi,dNueta,dNux,dNuy,detJ);
    EvaluateBasisGeometryDerivatives_Affine(nqp,nubasis,elcoords_g,dNuxi,dNueta,dNux,dNuy,&detJ_affine);
    EvaluateBasisDerivatives_Affine(nqp,npbasis,elcoords_g,dNpxi,dNpeta,dNpx,dNpy);
    for (q=0; q<nqp; q++) {
     detJ[q] = detJ_affine;
    }
    
    /* initialise element stiffness matrix */
    ierr = PetscMemzero( Fe, sizeof(PetscScalar) * (npbasis) );CHKERRQ(ierr);
    ierr = PetscMemzero( Be, sizeof(PetscScalar) * (npbasis) );CHKERRQ(ierr);
    
    ierr = QuadratureGetElementValues(quadrature_eta,e,1,coeff_k_phi,&coeff_k_phi_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_fp,e,1,coeff_fp,&coeff_fp_e);CHKERRQ(ierr);
    ierr = QuadratureGetElementValues(quadrature_eta,e,ncomp_e3,coeff_e3,&coeff_e3_e);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      fac = WEIGHT[q] * detJ[q];
      
      MatMultMF_Stokes2d_B21(nubasis,npbasis,fac,Uxe,Uye,dNux[q],dNuy[q],Np[q],Fe);
      MatMultMF_StkDcy2Field2d_B22(npbasis,fac,coeff_k_phi_e[q],Pe,dNpx[q],dNpy[q],Fe);

      /* compute gravity contribution */
      for (k=0; k<npbasis; k++) {
        Fe[k] += fac * coeff_k_phi_e[q] * (dNpx[q][k] * coeff_e3[2*q + 0] + dNpy[q][k] * coeff_e3[2*q + 1]);
      }

      /* compute body force terms here */
      for (k=0; k<npbasis; k++) {
        Be[k] += fac * Np[q][k] * coeff_fp_e[q];
      }
    }
    
    /* combine body force with A.x */
    for (k=0; k<npbasis; k++) {
      Fe[k] = Fe[k] - Be[k];
    }
    
    /* sum into residual vector */
    for (k=0; k<npbasis; k++) {
      Rp[ p_el_lidx[k] ] += Fe[k];
    }
    
  }
  
  PetscTime(&t1);
#ifdef LOG_MF_OP
  PetscPrintf(PETSC_COMM_WORLD,"[StkDcy2Field] Residual(p): %1.4e (sec)\n",t1-t0);
#endif
  
  ierr = EContainerDestroy(&q_space);CHKERRQ(ierr);
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = PetscFree(u_el_lidx);CHKERRQ(ierr);
  ierr = PetscFree(detJ);CHKERRQ(ierr);
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  ierr = PetscFree(Pe);CHKERRQ(ierr);
  ierr = PetscFree(Uxe);CHKERRQ(ierr);
  ierr = PetscFree(Uye);CHKERRQ(ierr);
  ierr = PetscFree(Fe);CHKERRQ(ierr);
  ierr = PetscFree(Be);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_Ps_Neumann"
PetscErrorCode FormFunctionLocal_Ps_Neumann(PDEStokes *data,DM dm,PetscScalar Rp[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis,nbasis_face;
  PetscReal *w,*xi;
  PetscReal *coords,*elcoords,*coeff;
  PetscInt *element,nen,npe;
  PetscInt e,i,q;
  PetscScalar *el_F;
  Coefficient qN;
  Quadrature quadrature;
  EContainer elcontainer;
  PetscReal *_el_coor_face,dJs;
  PetscInt bf,nfacets;
  PetscReal *normal,**N_face;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscBool issetup;
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* return early if no facets have been defined */
  if (nfacets == 0) {
    PetscFunctionReturn(0);
  } else {
    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetOrientations(f,&normal,NULL);CHKERRQ(ierr);
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  
  qN = data->q_N;
  ierr = CoefficientGetQuadrature(qN,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N",&nfacets,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&elcontainer);CHKERRQ(ierr);
  _el_coor_face = elcontainer->buf_basis_2vector_a;
  N_face        = elcontainer->N;
  
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&el_F);CHKERRQ(ierr);
  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt *eidx,face_id,*eidx_face;
    PetscReal *normal_f;
    
    e = facet_to_element[bf];
    face_id = facet_element_face_id[bf];
    
    /* get basis dofs */
    eidx = &element[npe*e];
    
    /* face dof */
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = eidx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    ierr = BFacetRestrictField(f,face_id,2,elcoords,_el_coor_face);CHKERRQ(ierr);
    
    EvaluateBasisSurfaceJacobian_Affine(nbasis_face,_el_coor_face,&dJs);
    normal_f = &normal[2*bf];
    
    ierr = PetscMemzero(el_F,sizeof(PetscScalar)*nbasis);CHKERRQ(ierr);
    for (q=0; q<nqp; q++) {
      PetscReal *flux_q;
      PetscReal *N_face_q;
      
      N_face_q = N_face[q];
      flux_q = &coeff[2*bf*nqp + 2*q];
      
      for (i=0; i<nbasis_face; i++) {
        PetscScalar residual;
        
        residual = N_face_q[i] * (flux_q[0] * normal_f[0] +  flux_q[1] * normal_f[1]);
        
        el_F[i] += w[q] * ( residual ) * dJs;
      }
    }
    
    for (i=0; i<nbasis_face; i++) {
      PetscInt face_idx;
      
      face_idx = eidx_face[i];
      
      Rp[eidx[face_idx]] += el_F[i];
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  ierr = PetscFree(el_F);CHKERRQ(ierr);
  ierr = EContainerDestroy(&elcontainer);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 Computes r = Ax - b
 SNES will scale by -1, F = -r = b - Ax
 Thus, in OUR function, dirichlet slots become A_ii(x_i - phi)
 In SNES, these become A_ii(phi-x_i), and the updates on the dirichlet slots will be
 A_ii d_i = -F_i
 = A_ii(phi-x_i)
 Then the update will be
 x_i^new = x_i + d_i
 = x_i + inv(A_ii) A_ii(phi-x_i)
 = x_i + phi - x_i
 = phi
 */
#undef __FUNCT__
#define __FUNCT__ "FormFunction_StkDcy2Field"
PetscErrorCode FormFunction_StkDcy2Field(SNES snes,Vec X,Vec F,void *ctx)
{
  PetscErrorCode    ierr;
  PDE               pde = (PDE)ctx;
  PDEStokes         *data;
  DM                stokes_pack,dau,dap;
  Vec               Uloc,Ploc,FUloc,FPloc;
  Vec               u,p,Fu,Fp;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscScalar       *LA_FUloc,*LA_FPloc;
  BCList            u_bclist,p_bclist = NULL;
  
  PetscFunctionBegin;
  data = (PDEStokes*)pde->data;
  ierr = PDEStokesGetDM(pde,&stokes_pack);CHKERRQ(ierr);
  ierr = PDEStokesGetBCLists(pde,&u_bclist,&p_bclist);CHKERRQ(ierr); /* need getter for this */
  
  ierr = DMCompositeGetEntries(stokes_pack,&dau,&dap);CHKERRQ(ierr);
  
  Uloc = pde->xlocal[0];
  Ploc = pde->xlocal[1];
  //ierr = DMCompositeGetLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(stokes_pack,&FUloc,&FPloc);CHKERRQ(ierr);
  
  /* get the local (ghosted) entries for each physics */
  ierr = DMCompositeScatter(stokes_pack,X,Uloc,Ploc);CHKERRQ(ierr);

  /* insert boundary conditions into local vectors */
  ierr = BCListInsertLocal(u_bclist,Uloc);CHKERRQ(ierr);
  if (p_bclist) {
    ierr = BCListInsertLocal(p_bclist,Ploc);CHKERRQ(ierr);
  }
  
  ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  /* compute Ax - b */
  ierr = VecZeroEntries(FUloc);CHKERRQ(ierr);
  ierr = VecZeroEntries(FPloc);CHKERRQ(ierr);
  ierr = VecGetArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
  ierr = VecGetArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
  
  /* ======================================== */
  /*         UPDATE NON-LINEARITIES           */
  /* ======================================== */
  ierr = CoefficientEvaluate(data->eta);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fu);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->fp);CHKERRQ(ierr);
  ierr = CoefficientEvaluate(data->q_N);CHKERRQ(ierr);
  
  /* momentum */
  ierr = FormFunctionLocal_Us(data,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
  
  /* continuity */
  /*   + boundary terms */
  ierr = FormFunctionLocal_Ps_Neumann(data,dap,LA_FPloc);CHKERRQ(ierr);
  /*   + volume terms */
  ierr = FormFunctionLocal_Ps(data,dau,LA_Uloc,dap,LA_Ploc,LA_FPloc);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  
  /* do global fem summation */
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  ierr = DMCompositeGather(stokes_pack,F,ADD_VALUES,FUloc,FPloc);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreLocalVectors(stokes_pack,&FUloc,&FPloc);CHKERRQ(ierr);
  //ierr = DMCompositeRestoreLocalVectors(stokes_pack,&Uloc,&Ploc);CHKERRQ(ierr);
  
  /* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
  ierr = DMCompositeGetAccess(stokes_pack,F,&Fu,&Fp);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(stokes_pack,X,&u,&p);CHKERRQ(ierr);
  
  ierr = BCListResidualDirichlet(u_bclist,u,Fu);CHKERRQ(ierr);
  if (p_bclist) {
    ierr = BCListResidualDirichlet(p_bclist,p,Fp);CHKERRQ(ierr);
  }
  
  ierr = DMCompositeRestoreAccess(stokes_pack,X,&u,&p);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(stokes_pack,F,&Fu,&Fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
