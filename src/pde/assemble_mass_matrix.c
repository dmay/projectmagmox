
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>
#include <dmtet_common.h> /* defined DMTetCreateMatrix_ScalarCoupling() */

#include <quadrature.h>
#include <quadratureimpl.h>
#include <fe_geometry_utils.h>
#include <element_container.h>

//PetscErrorCode DMTetCreateMatrix_ScalarCoupling(DM dm_w,Mat *_A); /* dmtet_common.h */

#undef __FUNCT__
#define __FUNCT__ "AssembleBForm_VectorMassMatrixMAIJ"
PetscErrorCode AssembleBForm_VectorMassMatrixMAIJ(DM dm,PetscInt bs,MatReuse reuse,Mat *_A)
{
  PetscErrorCode ierr;
  Mat        A,As;
  PetscInt   e,nel,npe,nbasis,i,j,k;
  PetscReal  elcoords_g[2 * 3];
  PetscInt   *elnidx,*elbasismap,*elbasismap_g;
  PetscReal  *coords_g,*Ae;
  PetscInt   q,nqp;
  Quadrature quadrature;
  EContainer v_space;
  PetscReal  **Nu,*xi,*weight;
  PetscReal  detJ_affine;
  
  if (reuse == MAT_INITIAL_MATRIX) {
    PetscInt bs0;
    
    ierr = DMTetGetDof(dm,&bs0);CHKERRQ(ierr);
    if (bs0 != 1) {
      DM dm_scalar;
      
      ierr = DMTetCreateSharedSpace(dm,1,&dm_scalar);CHKERRQ(ierr);
      ierr = DMCreateMatrix(dm_scalar,&As);CHKERRQ(ierr);
      ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
    } else {
      ierr = DMCreateMatrix(dm,&As);CHKERRQ(ierr);
    }
    ierr = MatCreateMAIJ(As,bs,&A);CHKERRQ(ierr);
 } else {
    PetscBool ismaij = PETSC_FALSE;
    A = *_A;
    
    ierr = PetscObjectTypeCompare((PetscObject)A,MATMAIJ,&ismaij);CHKERRQ(ierr);
    if (ismaij) {
      ierr = MatMAIJGetAIJ(A,&As);CHKERRQ(ierr);
    } else {
      As = A;
      ierr = PetscObjectReference((PetscObject)As);CHKERRQ(ierr);
    }
  }
  
  ierr = MatZeroEntries(As);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&elbasismap,&npe,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(quadrature,dm);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);

  ierr = EContainerCreate(dm,quadrature,&v_space);CHKERRQ(ierr);
  
  Nu  = v_space->N;

  ierr = PetscMalloc1(nbasis*nbasis,&Ae);CHKERRQ(ierr);

  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &elbasismap[nbasis*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nbasis*nbasis);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,*Ni;
      
      Ni = Nu[q];
      fac = detJ_affine * weight[q];
      for (i=0; i<nbasis; i++) {
        for (j=0; j<nbasis; j++) {
          Ae[ i*nbasis + j ] += fac * Ni[i] * Ni[j];
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(As,nbasis,elnidx,nbasis,elnidx,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(As,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(As,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = MatDestroy(&As);CHKERRQ(ierr);
  *_A = A;

  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleBForm_VectorMassMatrix"
PetscErrorCode AssembleBForm_VectorMassMatrix(DM dm,PetscInt ndof,MatReuse reuse,Mat *_A)
{
  PetscErrorCode ierr;
  Mat        A;
  PetscInt   e,nel,npe,nbasis,i,j,k,d;
  PetscReal  elcoords_g[2 * 3];
  PetscInt   *elnidx,*elbasismap,*elbasismap_g,*eldofs;
  PetscReal  *coords_g,*Ae;
  PetscInt   q,nqp;
  Quadrature quadrature;
  EContainer v_space;
  PetscReal  **Nu,*xi,*weight;
  PetscReal  detJ_affine;
  PetscLogDouble t0,t1;
  
  PetscTime(&t0);
  if (reuse == MAT_INITIAL_MATRIX) {
    PetscInt bs0;

    ierr = DMTetGetDof(dm,&bs0);CHKERRQ(ierr);
    if (bs0 != ndof) {
      DM dm_ndof;
    
      ierr = DMTetCreateSharedSpace(dm,ndof,&dm_ndof);CHKERRQ(ierr);
      ierr = DMCreateMatrix(dm_ndof,&A);CHKERRQ(ierr);
      ierr = DMDestroy(&dm_ndof);CHKERRQ(ierr);
    } else {
      ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
    }
  } else {
    A = *_A;
  }
  
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&elbasismap,&npe,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(quadrature,dm);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&v_space);CHKERRQ(ierr);
  
  Nu       = v_space->N;
  
  ierr = PetscMalloc1(nbasis*nbasis*ndof*ndof,&Ae);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*ndof,&eldofs);CHKERRQ(ierr);
  
  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &elbasismap[nbasis*e];
    
    /* get indices */
    for (k=0; k<nbasis; k++) {
      PetscInt nidx = elnidx[k];

      for (d=0; d<ndof; d++) {
        eldofs[ndof*k + d] = ndof*nidx + d;
      }
    }

    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* compute derivatives */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);
    
    ierr = PetscMemzero(Ae,sizeof(PetscReal)*nbasis*nbasis*ndof*ndof);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,*Ni;
      
      Ni = Nu[q];
      fac = detJ_affine * weight[q];
      for (i=0; i<nbasis; i++) {
        for (j=0; j<nbasis; j++) {
          PetscReal Ae_ij;
          
          Ae_ij = fac * Ni[i] * Ni[j];
          
          for (d=0; d<ndof; d++) {
            PetscInt index;
            
            index = (ndof*i + d)*nbasis*ndof + (ndof*j + d);
            Ae[index] += Ae_ij;
          }
          
        }
      }
    } // quadrature
    
    ierr = MatSetValuesLocal(A,nbasis*ndof,eldofs,nbasis*ndof,eldofs,Ae,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *_A = A;
  
  ierr = PetscFree(eldofs);CHKERRQ(ierr);
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dm,"CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleBForm_VectorMassMatrixCellWise"
PetscErrorCode AssembleBForm_VectorMassMatrixCellWise(DM dm,PetscInt ndof,MatReuse reuse,Mat *_A)
{
  PetscErrorCode ierr;
  Mat        A;
  PetscInt   e,nel,npe,nbasis,i,j,k,d;
  PetscReal  elcoords_g[2 * 3];
  PetscInt   *elnidx,*elbasismap,*elbasismap_g,*eldofs;
  PetscReal  *coords_g,*Ae,*Me;
  PetscInt   q,nqp;
  Quadrature quadrature;
  EContainer v_space;
  PetscReal  **Nu,*xi,*weight;
  PetscReal  detJ_affine;
  PetscLogDouble t0,t1;
  
  PetscTime(&t0);
  if (reuse == MAT_INITIAL_MATRIX) {
    PetscInt bs0;
    
    ierr = DMTetGetDof(dm,&bs0);CHKERRQ(ierr);
    if (bs0 != ndof) {
      DM dm_ndof;
      
      ierr = DMTetCreateSharedSpace(dm,ndof,&dm_ndof);CHKERRQ(ierr);
      //ierr = DMCreateMatrix(dm_ndof,&A);CHKERRQ(ierr);
      ierr = DMTetCreateMatrix_ScalarCoupling(dm_ndof,&A);CHKERRQ(ierr);
      ierr = DMDestroy(&dm_ndof);CHKERRQ(ierr);
    } else {
      //ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
      ierr = DMTetCreateMatrix_ScalarCoupling(dm,&A);CHKERRQ(ierr);
    }
  } else {
    A = *_A;
  }
  
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&elbasismap,&npe,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&elbasismap_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(quadrature,dm);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&weight);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&v_space);CHKERRQ(ierr);
  
  Nu       = v_space->N;
  
  ierr = PetscMalloc1(nbasis*nbasis,&Me);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*nbasis,&Ae);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&eldofs);CHKERRQ(ierr);
  
  ierr = PetscMemzero(Me,sizeof(PetscReal)*nbasis*nbasis);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    PetscReal fac,*Ni;
    
    Ni = Nu[q];
    fac = weight[q];
    for (i=0; i<nbasis; i++) {
      for (j=0; j<nbasis; j++) {
        PetscReal Me_ij;
        PetscInt index;
        
        Me_ij = fac * Ni[i] * Ni[j];
        index = i * nbasis + j;
        Me[index] += Me_ij;
      }
    }
  } // quadrature

  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &elbasismap[nbasis*e];
    
    /* get element vertices */
    for (k=0; k<3; k++) {
      PetscInt nidx = elbasismap_g[3*e+k];
      
      elcoords_g[2*k + 0] = coords_g[2*nidx+0];
      elcoords_g[2*k + 1] = coords_g[2*nidx+1];
    }
    
    /* compute transformation */
    EvaluateBasisGeometry_Affine(elcoords_g,&detJ_affine);

    /* scale reference cell mass matrix */
    for (i=0; i<nbasis*nbasis; i++) {
      Ae[i] = detJ_affine * Me[i];
    }
    
    /* get indices for dof */
    for (d=0; d<ndof; d++) {
      for (k=0; k<nbasis; k++) {
        PetscInt nidx = elnidx[k];
        eldofs[k] = ndof*nidx + d;
      }
      ierr = MatSetValuesLocal(A,nbasis,eldofs,nbasis,eldofs,Ae,ADD_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *_A = A;
  
  ierr = PetscFree(eldofs);CHKERRQ(ierr);
  ierr = PetscFree(Ae);CHKERRQ(ierr);
  ierr = PetscFree(Me);CHKERRQ(ierr);
  ierr = EContainerDestroy(&v_space);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  PetscTime(&t1);
  ierr = PetscInfo1((void*)dm,"CPU time <rank 0> %1.4e (sec)\n",t1-t0);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
