
#include <petsc.h>
#include <pde.h>
#include <pdeimpl.h>

PetscClassId PDE_CLASSID;

const char *PDETypeNames[] = {
  "uninit",
  "stokes",
  "helmholtz",
  "advdiff",
  "stokes_darcy_2field",
  "darcy"
};


#undef __FUNCT__
#define __FUNCT__ "PDEInitializePackage"
PetscErrorCode PDEInitializePackage(void)
{
  PetscErrorCode   ierr;
  static PetscBool registered = PETSC_FALSE;
  
  PetscFunctionBegin;
  if (!registered) {
    ierr = PetscClassIdRegister("PDE Mangement",&PDE_CLASSID);CHKERRQ(ierr);
    registered = PETSC_TRUE;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEDestroy"
PetscErrorCode PDEDestroy(PDE *pde)
{
  PDE p;
  PetscInt k;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!pde) PetscFunctionReturn(0);
  if (!*pde) PetscFunctionReturn(0);
  PetscValidHeaderSpecific(*pde,PDE_CLASSID,1);
  if (--((PetscObject)(*pde))->refct > 0) { *pde = NULL; PetscFunctionReturn(0); }
  p = *pde;

  if (p->description) {
    ierr = PetscFree(p->description);CHKERRQ(ierr);
  }
  if (p->ops->destroy && p->data) {
    ierr = p->ops->destroy(p);CHKERRQ(ierr);
    p->data = NULL;
  }
  ierr = VecDestroy(&p->x);CHKERRQ(ierr);
  for (k=0; k<p->n; k++) {
    ierr = VecDestroy(&p->xlocal[k]);CHKERRQ(ierr);
  }
  for (k=0; k<p->npde; k++) {
    ierr = PDEDestroy(&p->commom_pde[k]);CHKERRQ(ierr);
  }
  ierr = PetscHeaderDestroy(pde);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEView"
PetscErrorCode PDEView(PDE pde,PetscViewer viewer)
{
  PetscErrorCode ierr;
  PetscBool iascii;
  
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  if (iascii) {
    ierr = PetscObjectPrintClassNamePrefixType((PetscObject)pde,viewer);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"PDEType: %s\n",PDETypeNames[(int)pde->type]);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"Description:\n");CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"%s\n",pde->description);CHKERRQ(ierr);
    
    if (pde->ops->view) {
      ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
      ierr = pde->ops->view(pde,viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
    }
    ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDECreate"
PetscErrorCode PDECreate(MPI_Comm comm,PDE *_pde)
{
  PDE pde;
  PetscInt k;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  ierr = PDEInitializePackage();CHKERRQ(ierr);
  
  ierr = PetscHeaderCreate(pde, PDE_CLASSID, "PDE", "PDE Manager", "PDE", comm, PDEDestroy, PDEView);CHKERRQ(ierr);
  ierr = PetscMemzero(pde->ops, sizeof(struct _PDEOps));CHKERRQ(ierr);
  //ierr = PetscMalloc1(1,&pde);CHKERRQ(ierr);
  //ierr = PetscMemzero(pde,sizeof(struct _p_PDE));CHKERRQ(ierr);
  
  pde->npde = 0;
  pde->x = NULL;
  for (k=0; k<MAX_VECS; k++) {
    pde->xlocal[k] = NULL;
  }
  pde->local_vector_valid = PETSC_FALSE;
  pde->type = PDEUNINIT;
  *_pde = pde;
  PetscFunctionReturn(0);
}

/* prototypes for constructors */
PetscErrorCode PDECreate_Helmholtz(PDE pde);
PetscErrorCode PDECreate_Stokes(PDE pde);
PetscErrorCode PDECreate_ADFD(PDE pde);
PetscErrorCode PDECreate_TwoPhase2Field(PDE pde);
PetscErrorCode PDECreate_Darcy(PDE pde);

#undef __FUNCT__
#define __FUNCT__ "PDESetType"
PetscErrorCode PDESetType(PDE pde,PDEType type)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  pde->type = type;
  switch (type) {
    case PDEUNINIT:
      SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Un-initialized type for PDE");
      break;
    case PDESTOKES:
      pde->ops->create = PDECreate_Stokes;
      break;
    case PDEHELMHOLTZ:
      pde->ops->create = PDECreate_Helmholtz;
      break;
    case PDEADVDIFF:
      pde->ops->create = PDECreate_ADFD;
      break;
    case PDESTKDCY2F:
      pde->ops->create = PDECreate_TwoPhase2Field;
      break;
    case PDEDARCY:
      pde->ops->create = PDECreate_Darcy;
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Unknown type of PDE specified");
  }
  ierr = pde->ops->create(pde);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetSolution"
PetscErrorCode PDESetSolution(PDE pde,Vec x)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");
  
  ierr = PetscObjectReference((PetscObject)x);CHKERRQ(ierr);
  if (pde->x) {
    ierr = VecDestroy(&pde->x);CHKERRQ(ierr);
  }
  pde->x = x;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetLocalSolution"
PetscErrorCode PDESetLocalSolution(PDE pde,Vec x)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");
  
  ierr = PetscObjectReference((PetscObject)x);CHKERRQ(ierr);
  if (pde->xlocal[0]) {
    ierr = VecDestroy(&pde->xlocal[0]);CHKERRQ(ierr);
  }
  pde->n = 1;
  pde->xlocal[0] = x;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetLocalSolutions"
PetscErrorCode PDESetLocalSolutions(PDE pde,PetscInt n,Vec x[])
{
  PetscInt k;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");

  if (n > MAX_VECS) SETERRQ1(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Cannot set more than %D local vectors",MAX_VECS);
  
  for (k=0; k<n; k++) {
    ierr = PetscObjectReference((PetscObject)x[k]);CHKERRQ(ierr);
  }
  for (k=0; k<n; k++) {
    if (pde->xlocal[k]) {
      ierr = VecDestroy(&pde->xlocal[k]);CHKERRQ(ierr);
    }
    pde->xlocal[k] = x[k];
  }
  pde->n = n;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEGetLocalSolutions"
PetscErrorCode PDEGetLocalSolutions(PDE pde,PetscInt *n,Vec x[])
{
  PetscInt k;
  
  PetscFunctionBegin;
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");
  
  if (!pde->local_vector_valid) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Local vector is not up-to-date, you must call PDEUpdateLocalSolution() first");
  
  for (k=0; k<pde->n; k++) {
    x[k] = pde->xlocal[k];
  }
  if (n) { *n = pde->n; }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEGetSolution"
PetscErrorCode PDEGetSolution(PDE pde,Vec *x)
{
  PetscFunctionBegin;
  if (pde->x == NULL) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Solution of PDE not provided - Call PDESetSolution()");
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");
  if (x) {
    *x = pde->x;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEUpdateLocalSolution"
PetscErrorCode PDEUpdateLocalSolution(PDE pde)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");
  if (pde->x == NULL) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Solution of PDE not provided - Call PDESetSolution()");
  if (pde->xlocal == NULL) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Local solution of PDE not provided - Call PDESetLocalSolution()");
  if (!pde->ops->update_local_solution) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"No method to update local solution was provided for PDE");
  } else {
    ierr = pde->ops->update_local_solution(pde);CHKERRQ(ierr);
  }
  pde->local_vector_valid = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetSharedPDE"
PetscErrorCode PDESetSharedPDE(PDE pde,PDE p)
{
  PetscFunctionBegin;
  if (pde->npde < PDE_MAX_COMMON) {
    pde->commom_pde[pde->npde] = p;
    pde->npde++;
  } else SETERRQ1(PetscObjectComm((PetscObject)pde),PETSC_ERR_SUP,"Can only share %D PDEs",PDE_MAX_COMMON);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDEConfigureSNES"
PetscErrorCode PDEConfigureSNES(PDE pde,SNES snes,Vec F,Mat A,Mat B)
{
  const char *prefix;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (pde->x == NULL) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Solution of PDE not provided");
  if (pde->xlocal[0] == NULL) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Local solution of PDE not provided");
  if (pde->type == PDEUNINIT) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"Type of PDE has not been set");
  if (!pde->ops->configure_snes) {
    SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"No SNES configuration was provided for PDE");
  } else {
    ierr = PetscObjectGetOptionsPrefix((PetscObject)pde,&prefix);CHKERRQ(ierr);
    if (prefix) {
      ierr = SNESSetOptionsPrefix(snes,prefix);CHKERRQ(ierr);
    }
    ierr = pde->ops->configure_snes(pde,snes,F,A,B);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESetOptionsPrefix"
PetscErrorCode PDESetOptionsPrefix(PDE pde,const char prefix[])
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = PetscObjectSetOptionsPrefix((PetscObject)pde,prefix);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PDESNESSolve"
PetscErrorCode PDESNESSolve(PDE pde,SNES snes)
{
  PetscErrorCode ierr;
  Vec X;
  PetscInt k;
  
  PetscFunctionBegin;

  for (k=0; k<pde->n; k++) {
    ierr = VecZeroEntries(pde->xlocal[k]);CHKERRQ(ierr);
  }
  pde->local_vector_valid = PETSC_FALSE;

  /* Let the residual evaluation do this if its required */
  //ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);

  ierr = PDEGetSolution(pde,&X);CHKERRQ(ierr);
  
  ierr = SNESSolve(snes,NULL,X);CHKERRQ(ierr);

  pde->local_vector_valid = PETSC_FALSE;
  ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

