libproj-y.c += $(call thisdir, \
   pde.c \
   fe_geometry_utils.c \
   element_container.c \
   assemble_mass_matrix.c \
   projector.c \
)

PROJ_INC += -I$(abspath $(call thisdir,.))

include $(call incsubdirs,helmholtz stokes advdiff darcy)
