
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <quadrature.h>
#include <element_container.h>
#include <dmtetimpl.h>    /*I   "petscdmtet.h"   I*/

#undef __FUNCT__
#define __FUNCT__ "EContainerCreate"
PetscErrorCode EContainerCreate(DM dm,Quadrature q,EContainer *_e)
{
  PetscErrorCode ierr;
  ierr = EContainerCreate_Generic(dm,q,_e);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 Old style - only supports volume integrations.
 Keeping for posterity
*/
#undef __FUNCT__
#define __FUNCT__ "_EContainerCreate"
PetscErrorCode _EContainerCreate(DM dm,Quadrature q,EContainer *_e)
{
  EContainer e;
  PetscInt nb;
  PetscReal *xi;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&e);CHKERRQ(ierr);
  ierr = PetscMemzero(e,sizeof(struct _p_EContainer));CHKERRQ(ierr);

  e->q = q;
  ierr = DMTetGetBasisType(dm,&e->basistype);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,NULL,&e->nbasis,NULL,NULL,NULL,NULL);CHKERRQ(ierr);

  ierr = QuadratureGetRule(q,&e->nqp,&xi,NULL);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,e->nqp,xi,&nb,&e->N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,e->nqp,xi,&nb,&e->dNr1,&e->dNr2,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,e->nqp,xi,&nb,&e->dNx1,&e->dNx2,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_index_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_index_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_index_c);CHKERRQ(ierr);

  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2index_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2index_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2index_c);CHKERRQ(ierr);

  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_scalar_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_scalar_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_scalar_c);CHKERRQ(ierr);

  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2vector_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2vector_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2vector_c);CHKERRQ(ierr);

  ierr = PetscMalloc1(e->nqp,&e->buf_q_scalar_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nqp,&e->buf_q_scalar_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nqp,&e->buf_q_scalar_c);CHKERRQ(ierr);

  *_e = e;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EContainerSetup_AllocateBuffers"
PetscErrorCode _EContainerSetup_AllocateBuffers(EContainer e)
{
  PetscErrorCode ierr;
  
  /* allocate buffers  */
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_index_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_index_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_index_c);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2index_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2index_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2index_c);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_scalar_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_scalar_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nbasis,&e->buf_basis_scalar_c);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2vector_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2vector_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*e->nbasis,&e->buf_basis_2vector_c);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(e->nqp,&e->buf_q_scalar_a);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nqp,&e->buf_q_scalar_b);CHKERRQ(ierr);
  ierr = PetscMalloc1(e->nqp,&e->buf_q_scalar_c);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EContainerSetup_VolumeQuadrature"
PetscErrorCode _EContainerSetup_VolumeQuadrature(EContainer e,DM dm)
{
  PetscInt nb;
  PetscReal *xi;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  
  ierr = QuadratureGetRule(e->q,&e->nqp,&xi,NULL);CHKERRQ(ierr);
  
  /* allocate and evaluate basis */
  ierr = DMTetTabulateBasis(dm,e->nqp,xi,&nb,&e->N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,e->nqp,xi,&nb,&e->dNr1,&e->dNr2,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,e->nqp,xi,&nb,&e->dNx1,&e->dNx2,NULL);CHKERRQ(ierr);
  
  /* setup buffers */
  ierr = _EContainerSetup_AllocateBuffers(e);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EContainerSetup_SurfaceQuadrature"
PetscErrorCode _EContainerSetup_SurfaceQuadrature(EContainer e,DM dm)
{
  PetscInt order,nb,nbasis_face,q;
  PetscReal *xi_1d,*xi,*face_scalar;
  PetscErrorCode ierr;
  BFacetList bfacet;
  
  PetscFunctionBegin;
  
  
  ierr = QuadratureGetRule(e->q,&e->nqp,&xi_1d,NULL);CHKERRQ(ierr);
  /* pad 1d coordinates with an eta coordinate which mimics the location of quad points living on face 0 */
  ierr = PetscMalloc1(e->nqp*2,&xi);CHKERRQ(ierr);
  for (q=0; q<e->nqp; q++) {
    xi[2*q+0] = xi_1d[q];
    xi[2*q+1] = -1.0;
  }
  
  /* allocate and evaluate basis */
  ierr = DMTetTabulateBasis(dm,e->nqp,xi,&nb,&e->N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,e->nqp,xi,&nb,&e->dNr1,&e->dNr2,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,e->nqp,xi,&nb,&e->dNx1,&e->dNx2,NULL);CHKERRQ(ierr);
  
  /* restrict and copy values */
  ierr = DMTetGetSpaceBFacetList(dm,&bfacet);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(bfacet,NULL,&nbasis_face);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis_face,&face_scalar);CHKERRQ(ierr);

  for (q=0; q<e->nqp; q++) {
    PetscInt face_id = 0; /* 
                           I'm assuming basis evaluated and restricted on all faces will be identical!
                           - it better be otherwise the basis cannot be C0 continuous 
                           */
    /* basis */
    ierr = BFacetRestrictField(bfacet,face_id,1,e->N[q],face_scalar);CHKERRQ(ierr);
    ierr = PetscMemzero(e->N[q],sizeof(PetscReal)*e->nbasis);CHKERRQ(ierr);
    ierr = PetscMemcpy(e->N[q],face_scalar,sizeof(PetscReal)*nbasis_face);CHKERRQ(ierr);
    
    /* basis derivative - xi */
    ierr = BFacetRestrictField(bfacet,face_id,1,e->dNr1[q],face_scalar);CHKERRQ(ierr);
    ierr = PetscMemzero(e->dNr1[q],sizeof(PetscReal)*e->nbasis);CHKERRQ(ierr);
    ierr = PetscMemcpy(e->dNr1[q],face_scalar,sizeof(PetscReal)*nbasis_face);CHKERRQ(ierr);

    /* basis derivative - eta --> just set this to 0 as we don't need this for line geometries */
    ierr = PetscMemzero(e->dNr2[q],sizeof(PetscReal)*e->nbasis);CHKERRQ(ierr);
  }
  
  /* setup buffers */
  ierr = _EContainerSetup_AllocateBuffers(e);CHKERRQ(ierr);
  
  /* reset e->nbasis to match nbasis on the facet (yes I over allocate) */
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  e->nbasis = order + 1;
  
  ierr = PetscFree(xi);CHKERRQ(ierr);
  ierr = PetscFree(face_scalar);CHKERRQ(ierr);
                        
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EContainerCreate_Generic"
PetscErrorCode EContainerCreate_Generic(DM dm,Quadrature q,EContainer *_e)
{
  EContainer e;
  QuadratureGeometryType gtype;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&e);CHKERRQ(ierr);
  ierr = PetscMemzero(e,sizeof(struct _p_EContainer));CHKERRQ(ierr);

  /* set sizes and types */
  e->q = q;
  ierr = DMTetGetBasisType(dm,&e->basistype);CHKERRQ(ierr);
  ierr = QuadratureGetRule(q,&e->nqp,NULL,NULL);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,NULL,&e->nbasis,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  
  /* check if vol or surf quadrature - setup accordingly */
  ierr = QuadratureGetGeometryType(q,&gtype);CHKERRQ(ierr);
  switch (gtype) {
    case QUADRATURE_SURFACE:
      ierr = _EContainerSetup_SurfaceQuadrature(e,dm);CHKERRQ(ierr);
      break;

    case QUADRATURE_VOLUME:
      ierr = _EContainerSetup_VolumeQuadrature(e,dm);CHKERRQ(ierr);
      break;
      
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Unrecognized quadrature rule - cannot setup ElementContainer");
      break;
  }
  
  *_e = e;
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "EContainerDestroy"
PetscErrorCode EContainerDestroy(EContainer *_e)
{
  PetscErrorCode ierr;
  EContainer e;
  PetscInt q;
  
  PetscFunctionBegin;
  if (!_e) PetscFunctionReturn(0);
  e = *_e;
  
  /* basis tables */
  for (q=0; q<e->nqp; q++) {
    ierr = PetscFree(e->N[q]);CHKERRQ(ierr);

    ierr = PetscFree(e->dNr1[q]);CHKERRQ(ierr);
    ierr = PetscFree(e->dNr2[q]);CHKERRQ(ierr);

    ierr = PetscFree(e->dNx1[q]);CHKERRQ(ierr);
    ierr = PetscFree(e->dNx2[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(e->N);CHKERRQ(ierr);
  
  ierr = PetscFree(e->dNr1);CHKERRQ(ierr);
  ierr = PetscFree(e->dNr2);CHKERRQ(ierr);

  ierr = PetscFree(e->dNx1);CHKERRQ(ierr);
  ierr = PetscFree(e->dNx2);CHKERRQ(ierr);

  /* buffers */
  /* basis */
  /*   <int> */
  ierr = PetscFree(e->buf_basis_index_a);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_index_b);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_index_c);CHKERRQ(ierr);

  /*   <2xint> */
  ierr = PetscFree(e->buf_basis_2index_a);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_2index_b);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_2index_c);CHKERRQ(ierr);

  /*   <float> */
  ierr = PetscFree(e->buf_basis_scalar_a);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_scalar_b);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_scalar_c);CHKERRQ(ierr);

  /*   <2xfloat> */
  ierr = PetscFree(e->buf_basis_2vector_a);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_2vector_b);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_basis_2vector_c);CHKERRQ(ierr);
  
  /* quadrature */
  /*   <float> */
  ierr = PetscFree(e->buf_q_scalar_a);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_q_scalar_b);CHKERRQ(ierr);
  ierr = PetscFree(e->buf_q_scalar_c);CHKERRQ(ierr);
  
  /* struct */
  ierr = PetscFree(e);CHKERRQ(ierr);
  
  *_e = NULL;
  PetscFunctionReturn(0);
}





#undef __FUNCT__
#define __FUNCT__ "EContainerGeometryCreate_Generic"
PetscErrorCode EContainerGeometryCreate_Generic(DM dm,Quadrature q,EContainer *_e)
{
  EContainer e;
  QuadratureGeometryType gtype;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&e);CHKERRQ(ierr);
  ierr = PetscMemzero(e,sizeof(struct _p_EContainer));CHKERRQ(ierr);
  
  /* set sizes and types */
  e->q = q;
  e->basistype = DMTET_CWARP_AND_BLEND;
  ierr = QuadratureGetRule(q,&e->nqp,NULL,NULL);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,NULL,&e->nbasis,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  
  /* check if vol or surf quadrature - setup accordingly */
  ierr = QuadratureGetGeometryType(q,&gtype);CHKERRQ(ierr);
  switch (gtype) {
    case QUADRATURE_SURFACE:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Cannot setup ElementGeometryContainer for surface quadrature");
      break;
      
    case QUADRATURE_VOLUME:
    {
      DM_TET *tet = (DM_TET*)dm->data;
      PetscInt basis_order_ref;
      DMTetBasisType basis_type_ref;
      
      /* temporarily over-ride basis definition */
      basis_type_ref = tet->basis_type;
      basis_order_ref = tet->basis_order;
      tet->basis_type = DMTET_CWARP_AND_BLEND;
      tet->basis_order = 1;

      ierr = _EContainerSetup_VolumeQuadrature(e,dm);CHKERRQ(ierr);

      /* reset basis definition */
      tet->basis_type = basis_type_ref;
      tet->basis_order = basis_order_ref;
    }
      break;
      
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Unrecognized quadrature rule - cannot setup ElementContainer");
      break;
  }
  
  *_e = e;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EContainerGeometryCreate"
PetscErrorCode EContainerGeometryCreate(DM dm,Quadrature q,EContainer *_e)
{
  PetscErrorCode ierr;
  ierr = EContainerGeometryCreate_Generic(dm,q,_e);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
