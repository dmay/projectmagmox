
#include <stdio.h>
#include <string.h>
#include <petsc.h>

/* PVD helpers */
#undef __FUNCT__
#define __FUNCT__ "ParaviewPVDOpen"
PetscErrorCode ParaviewPVDOpen(const char pvdfilename[])
{
  PetscMPIInt rank;
  FILE        *fp = NULL;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  /* only master generates this file */
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  if(rank != 0) { PetscFunctionReturn(0); }
  
  fp = fopen(pvdfilename,"w");
  if (!fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file \"%s\"",pvdfilename);
  fprintf(fp,"<?xml version=\"1.0\"?>\n");
#ifdef WORDSIZE_BIGENDIAN
  fprintf(fp,"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
  fprintf(fp,"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
  
  fprintf(fp,"<Collection>\n");
  
  fprintf(fp,"</Collection>\n");
  fprintf(fp,"</VTKFile>");
  fclose(fp);
  PetscFunctionReturn(0);
}

/* scan PVD file forwards - this is obviously inefficient as we want to write data at the end of the file */
#undef __FUNCT__
#define __FUNCT__ "_ParaviewPVDAppend"
PetscErrorCode _ParaviewPVDAppend(const char pvdfilename[],PetscReal time,const char datafile[], const char DirectoryName[])
{
  PetscMPIInt rank;
  FILE        *fp = NULL;
  char        line[10000];
  int         key_L,position;
  char        key[] = "</Collection>";
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  /* only master generates this file */
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  if (rank != 0) { PetscFunctionReturn(0); }
  
  fp = fopen(pvdfilename,"r+");
  if (!fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file for appending data \"%s\"",pvdfilename);
  /* reset to start of file */
  rewind(fp);
  
  key_L = strlen(key);
  position = -1;
  while (!feof(fp)) {
    position = ftell(fp);
    fgets(line,10000-1,fp);
    if (strncmp(key,line,key_L) == 0) {
      break;
    }
  }
  if (position == -1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot locate keyword in pvd file");
  
  fseek(fp,position,SEEK_SET);
  
  /* write new data */
  if (DirectoryName == NULL) {
    fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"./%s\"/>\n",time, datafile );
  } else if ( (strlen(DirectoryName) == 0) ) {
    fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"./%s\"/>\n",time, datafile );
  } else {
    fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"%s/%s\"/>\n",time, DirectoryName, datafile );
  }
  
  /* close tag */
  fprintf(fp,"</Collection>\n");
  fprintf(fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

/* scan PVD file backwards */
#undef __FUNCT__
#define __FUNCT__ "ParaviewPVDAppend"
PetscErrorCode ParaviewPVDAppend(const char pvdfilename[],PetscReal time,const char datafile[], const char DirectoryName[])
{
  PetscMPIInt rank;
  FILE        *fp = NULL;
  int         nlines,bytes;
  int         position;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  /* only master generates this file */
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  if (rank != 0) { PetscFunctionReturn(0); }
  
  fp = fopen(pvdfilename,"r+");
  if (!fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file for appending data \"%s\"",pvdfilename);

  position = -1;
  nlines = 0;
  bytes = 0;
  while(fseek(fp, --bytes, SEEK_END) == 0) {
    if ((fgetc(fp) == '\n') && (++nlines > 1)) {
      position = ftell(fp);
      break;
    }
  }
  if (position == -1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Failed to locate tail end of the PVD file");
  
  fseek(fp,position,SEEK_SET);
  
  /* write new data */
  if (DirectoryName == NULL) {
    fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"./%s\"/>\n",time, datafile );
  } else if ( (strlen(DirectoryName) == 0) ) {
    fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"./%s\"/>\n",time, datafile );
  } else {
    fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"%s/%s\"/>\n",time, DirectoryName, datafile );
  }
  
  /* close tag */
  fprintf(fp,"</Collection>\n");
  fprintf(fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}
