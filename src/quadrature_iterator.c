
#include <petscdmtet.h>
#include <dmtetdt.h>
#include <dmtetimpl.h>
#include <quadratureimpl.h>
#include <quadrature.h>

#undef __FUNCT__
#define __FUNCT__ "SQuadraturePointCreate"
PetscErrorCode SQuadraturePointCreate(SQuadraturePoint *_p)
{
  SQuadraturePoint p;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&p);CHKERRQ(ierr);
  p->element_id = -1;
  p->face_id = -1;
  p->coor[0] = p->coor[1] = 0.0;
  p->normal[0] = p->normal[1] = 0.0;
  p->tangent[0] = p->tangent[1] = 0.0;
  p->nexpected_values = -1;
  *_p = p;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SQuadraturePointDestroy"
PetscErrorCode SQuadraturePointDestroy(SQuadraturePoint *_p)
{
  SQuadraturePoint p;
  PetscErrorCode ierr;
  
  if (!_p) PetscFunctionReturn(0);
  if (!*_p) PetscFunctionReturn(0);
  p = *_p;
  ierr = PetscFree(p);CHKERRQ(ierr);
  *_p = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluationPointCreate"
PetscErrorCode EvaluationPointCreate(EvaluationPoint *_p)
{
  EvaluationPoint p;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&p);CHKERRQ(ierr);
  p->element_id = -1;
  p->face_id = -1;
  p->region_tag = -1;
  
  p->point_type = UNINITIALIZED_POINT;

  p->has_coords = PETSC_FALSE;
  p->coor[0] = p->coor[1] = 0.0;
  
  p->has_orientation = PETSC_FALSE;
  p->normal[0] = p->normal[1] = 0.0;
  p->tangent[0] = p->tangent[1] = 0.0;
  
  p->nexpected_values = -1;
  *_p = p;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluationPointSetup"
PetscErrorCode EvaluationPointSetup(EvaluationPoint p,EvaluationPointType type,PetscBool has_coords,PetscBool has_orientation,PetscInt values)
{
  p->point_type = type;
  p->has_coords = has_coords;
  p->has_orientation = has_orientation;
  p->nexpected_values = values;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluationPointReset"
PetscErrorCode EvaluationPointReset(EvaluationPoint p)
{
  p->element_id = -1;
  p->face_id = -1;
  p->region_tag = -1;
  
  p->coor[0] = p->coor[1] = 0.0;
  p->normal[0] = p->normal[1] = 0.0;
  p->tangent[0] = p->tangent[1] = 0.0;

  p->values_set  = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluationPointDestroy"
PetscErrorCode EvaluationPointDestroy(EvaluationPoint *_p)
{
  EvaluationPoint p;
  PetscErrorCode ierr;
  
  if (!_p) PetscFunctionReturn(0);
  if (!*_p) PetscFunctionReturn(0);
  p = *_p;
  ierr = PetscFree(p);CHKERRQ(ierr);
  *_p = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_SurfaceQuadratureIterator"
PetscErrorCode _SurfaceQuadratureIterator(Quadrature quadrature,DM dm,const char fieldname[],PetscReal time,PetscErrorCode (*fp_evaluator)(SQuadraturePoint,PetscReal,PetscReal*,void*),void *ctx)
{
  PetscErrorCode ierr;
  PetscInt nqp;
  PetscReal *q_coor,*q_field;
  PetscInt q,c;
  PetscInt bf,nfacets,nbasis_face,ncomponents_coor,ncomponents_field;
  PetscReal *normal,*tangent;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscReal *flux_pointwise;
  SQuadraturePoint surface_point;

  
  if (quadrature->gtype != QUADRATURE_SURFACE) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only valid for surface quadrature rules");
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* return early if no facets have been defined */
  if (nfacets == 0) {
    PetscFunctionReturn(0);
  } else {
    PetscBool issetup;

    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetOrientations(f,&normal,&tangent);CHKERRQ(ierr);
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomponents_coor,&q_coor);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,fieldname,&nfacets,&nqp,&ncomponents_field,&q_field);CHKERRQ(ierr);
  
  ierr = SQuadraturePointCreate(&surface_point);CHKERRQ(ierr);
  surface_point->nexpected_values = ncomponents_field;
  ierr = PetscMalloc1(ncomponents_field,&flux_pointwise);CHKERRQ(ierr);
  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt e_id,face_id;
    PetscReal *normal_pointwise,*tangent_pointwise;
    
    e_id    = facet_to_element[bf];
    face_id = facet_element_face_id[bf];
    
    normal_pointwise  = &normal[2*bf];
    tangent_pointwise = &tangent[2*bf];
    
    for (q=0; q<nqp; q++) {

      surface_point->element_id  = e_id;
      surface_point->face_id     = face_id;
      surface_point->normal[0]   = normal_pointwise[0];
      surface_point->normal[1]   = normal_pointwise[1];
      surface_point->tangent[0]  = tangent_pointwise[0];
      surface_point->tangent[1]  = tangent_pointwise[1];
      surface_point->coor[0]     = q_coor[2*(bf*nqp + q)+0];
      surface_point->coor[1]     = q_coor[2*(bf*nqp + q)+1];
      surface_point->values_set  = PETSC_TRUE;
      
      ierr = PetscMemzero(flux_pointwise,sizeof(PetscReal)*ncomponents_field);CHKERRQ(ierr);
      ierr = fp_evaluator(surface_point,time,flux_pointwise,ctx);CHKERRQ(ierr);
      
      if (surface_point->values_set) {
        for (c=0; c<ncomponents_field; c++) {
          q_field[ncomponents_field*(bf*nqp + q)+c] = flux_pointwise[c];
        }
      }
    }
  }
  ierr = PetscFree(flux_pointwise);CHKERRQ(ierr);
  ierr = SQuadraturePointDestroy(&surface_point);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureIterator"
PetscErrorCode SurfaceQuadratureIterator(Quadrature quadrature,DM dm,const char fieldname[],PetscReal time,PetscErrorCode (*fp_evaluator)(EvaluationPoint,PetscReal,PetscReal*,void*),void *ctx)
{
  PetscErrorCode ierr;
  PetscInt nqp;
  PetscReal *q_coor,*q_field;
  PetscInt q,c;
  PetscInt bf,nfacets,nbasis_face,ncomponents_coor,ncomponents_field;
  PetscReal *normal,*tangent;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscReal *flux_pointwise;
  EvaluationPoint surface_point;
  
  
  if (quadrature->gtype != QUADRATURE_SURFACE) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only valid for surface quadrature rules");
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* return early if no facets have been defined */
  if (nfacets == 0) {
    PetscFunctionReturn(0);
  } else {
    PetscBool issetup;
    
    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetOrientations(f,&normal,&tangent);CHKERRQ(ierr);
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomponents_coor,&q_coor);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,fieldname,&nfacets,&nqp,&ncomponents_field,&q_field);CHKERRQ(ierr);
  
  ierr = EvaluationPointCreate(&surface_point);CHKERRQ(ierr);
  ierr = EvaluationPointSetup(surface_point,S_QUADRATURE_POINT,PETSC_TRUE,PETSC_TRUE,ncomponents_field);CHKERRQ(ierr);
  ierr = PetscMalloc1(ncomponents_field,&flux_pointwise);CHKERRQ(ierr);
  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt e_id,face_id;
    PetscReal *normal_pointwise,*tangent_pointwise;
    
    e_id    = facet_to_element[bf];
    face_id = facet_element_face_id[bf];
    
    normal_pointwise  = &normal[2*bf];
    tangent_pointwise = &tangent[2*bf];
    
    for (q=0; q<nqp; q++) {
      
      ierr = EvaluationPointReset(surface_point);CHKERRQ(ierr);

      surface_point->element_id  = e_id;
      surface_point->face_id     = face_id;
      surface_point->normal[0]   = normal_pointwise[0];
      surface_point->normal[1]   = normal_pointwise[1];
      surface_point->tangent[0]  = tangent_pointwise[0];
      surface_point->tangent[1]  = tangent_pointwise[1];
      surface_point->coor[0]     = q_coor[2*(bf*nqp + q)+0];
      surface_point->coor[1]     = q_coor[2*(bf*nqp + q)+1];
      surface_point->values_set  = PETSC_TRUE;
      
      ierr = PetscMemzero(flux_pointwise,sizeof(PetscReal)*ncomponents_field);CHKERRQ(ierr);
      ierr = fp_evaluator(surface_point,time,flux_pointwise,ctx);CHKERRQ(ierr);
      
      if (surface_point->values_set) {
        for (c=0; c<ncomponents_field; c++) {
          q_field[ncomponents_field*(bf*nqp + q)+c] = flux_pointwise[c];
        }
      }
    }
  }
  ierr = PetscFree(flux_pointwise);CHKERRQ(ierr);
  ierr = EvaluationPointDestroy(&surface_point);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#if 0
#undef __FUNCT__
#define __FUNCT__ "PDEHelmholtzQuadratureIterator_Neumann"
PetscErrorCode PDEHelmholtzQuadratureIterator_Neumann(PDEHelmholtz *data,PetscErrorCode (*fp_evaluator)(SQuadraturePoint,PetscReal*,void *ctx),void *ctx)
{
  PetscErrorCode ierr;
  DM dm;
  Coefficient qN;
  Quadrature quadrature;
  
  dm = data->dm;
  qN = data->q_N;
  ierr = CoefficientGetQuadrature(qN,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",fp_evaluator,ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
#endif
