
/*
 
 Solves 
   div( grad(u) ) + u = g
 using P_k basis defined over triangles.
 
 The chosen solution is
   u(x,y) =  cos(\pi y) sin(\pi x)
 The function g(x,y) is manufactured.
 
 We define a residual
   F = Ku + Mu - G
 and solve the following discrete problem
   A dX = -F
   X = X + dX
 where A is the Jacobian of F.
 Here F is simply the discrete form of div(grad()) as the
 problem is linear.

 The assembly and imposition of the Dirichlet boundary within
 A preserves symmetry
 
*/

#include <petsc.h>
#include <petscdmtet.h>
#include <dmbcs.h>
#include <quadrature.h>
#include <proj_init_finalize.h>

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution"
PetscReal mms_evaluate_solution(PetscReal x,PetscReal y)
{
  PetscReal u;
  u =  cos(M_PI*y)*sin(M_PI*x);
  return u;
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution_gradients"
void mms_evaluate_solution_gradients(PetscReal x,PetscReal y,PetscReal *dudx,PetscReal *dudy)
{
  *dudx =  M_PI*cos(M_PI*x)*cos(M_PI*y);
  *dudy =  -M_PI*sin(M_PI*x)*sin(M_PI*y);
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_rhs"
PetscReal mms_evaluate_rhs(PetscReal x,PetscReal y)
{
  PetscReal f;
  f =  cos(M_PI*y)*sin(M_PI*x) + 2*pow(M_PI,2)*cos(M_PI*y)*sin(M_PI*x);
  return f;
}

void ElementEvaluateGeometry_2d(PetscInt nqp,PetscInt nbasis,PetscReal el_coords[],
                                PetscReal **GNIxi,PetscReal **GNIeta,
                                PetscReal detJ[])
{
  PetscInt k,q;
  PetscReal J[2][2];
  
  for (q=0; q<nqp; q++) {
    //
    J[0][0] = J[0][1] = 0.0;
    J[1][0] = J[1][1] = 0.0;
    
    for (k=0; k<nbasis; k++) {
      PetscReal xc = el_coords[2*k+0];
      PetscReal yc = el_coords[2*k+1];
      
      J[0][0] += GNIxi[q][k] * xc ;
      J[0][1] += GNIxi[q][k] * yc ;
      
      J[1][0] += GNIeta[q][k] * xc ;
      J[1][1] += GNIeta[q][k] * yc ;
    }
    
    detJ[q] = J[0][0]*J[1][1] - J[0][1]*J[1][0];
  }
}

void ElementEvaluateDerivatives_2d(PetscInt nqp,PetscInt nbasis,PetscReal el_coords[],
                                   PetscReal **GNIxi,PetscReal **GNIeta,
                                   PetscReal **dNudx,PetscReal **dNudy)
{
  PetscInt k,q;
  PetscReal J[2][2],iJ[2][2],od,detJ;
  
  for (q=0; q<nqp; q++) {
    //
    J[0][0] = J[0][1] = 0.0;
    J[1][0] = J[1][1] = 0.0;
    
    for (k=0; k<nbasis; k++) {
      PetscReal xc = el_coords[2*k+0];
      PetscReal yc = el_coords[2*k+1];
      
      J[0][0] += GNIxi[q][k] * xc ;
      J[0][1] += GNIxi[q][k] * yc ;
      
      J[1][0] += GNIeta[q][k] * xc ;
      J[1][1] += GNIeta[q][k] * yc ;
    }
    
    detJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
    od = 1.0/detJ;
    
    iJ[0][0] =  J[1][1] * od;
    iJ[0][1] = -J[0][1] * od;
    iJ[1][0] = -J[1][0] * od;
    iJ[1][1] =  J[0][0] * od;
    
    /* shape function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "Helmholtz_AssembleBilinearSym"
PetscErrorCode Helmholtz_AssembleBilinearSym(Mat A,DM dm,BCList bc)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,npe,i,j;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *coords,*Ke;
  PetscInt q,nqp,nb;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  IS ltog;
	const PetscInt *GINDICES;
	PetscInt       NUM_GINDICES;
  
  
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&npe,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  nbasis = npe;
  
  PetscMalloc1(2*nbasis,&elcoords);
  PetscMalloc1(nbasis,&eldofs);
  PetscMalloc1(nbasis*nbasis,&Ke);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc1(nqp,&detJ);
  
  ierr = ISCreateStride(PETSC_COMM_WORLD,NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog, &GINDICES);CHKERRQ(ierr);
	ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  
  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    /* generate dofs */
    for (i=0; i<nbasis; i++) {
			const int NID = elnidx[i];

      eldofs[i] = GINDICES[NID];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = elnidx[i];
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Ni,*dNidx,*dNidy;
      
      /* get access to element->quadrature points */
      Ni    = tab_N[q];
      dNidx = tab_dNdx[q];
      dNidy = tab_dNdy[q];
      
      fac = PetscAbsReal(detJ[q]) * w[q];
      
      for (i=0; i<nbasis; i++) {
        for (j=0; j<nbasis; j++) {
          Ke[ i*nbasis + j ] += fac * (dNidx[i]*dNidx[j] + dNidy[i]*dNidy[j] + Ni[i]*Ni[j]);
        }
      }
      
    } // quadrature
    
    for (i=0; i<nbasis*nbasis; i++) {
      if (PetscAbsReal(Ke[i]) < 1.0e-12) {
        Ke[i] = 0.0;
      }
    }
    ierr = MatSetValues(A,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(A,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
	ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
	ierr = BCListInsertScaling(A,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  ierr = ISRestoreIndices(ltog, &GINDICES);CHKERRQ(ierr);

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  PetscFree(w);
  PetscFree(xi);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Ke);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Helmholtz_AssembleResidual_Local"
PetscErrorCode Helmholtz_AssembleResidual_Local(DM dm,PetscScalar x[],PetscScalar F[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  PetscReal *coords,*elcoords;
  PetscInt *element,nen,npe,dof;
  PetscInt e,i,q;
  PetscScalar *el_x,*el_F,*function;
  
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_x);
  PetscMalloc(sizeof(PetscReal)*nbasis,&el_F);
  PetscMalloc(sizeof(PetscReal)*nqp,&function);
  
  /* zero pass through */
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
      
      el_x[i] = x[nidx];
    }
    
    
    PetscMemzero(el_F,sizeof(PetscScalar)*nbasis*dof);
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      function[q] = mms_evaluate_rhs(xq,yq);
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp,gradx_qp[2];
      
      /* get x at each quadrature point */
      x_qp = gradx_qp[0] = gradx_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp        += tab_N[q][i] * el_x[i];
        gradx_qp[0] += tab_dNdx[q][i] * el_x[i];
        gradx_qp[1] += tab_dNdy[q][i] * el_x[i];
      }
      
      for (i=0; i<nbasis; i++) {
        PetscScalar residual;
        
        residual = tab_dNdx[q][i] * gradx_qp[0] + tab_dNdy[q][i] * gradx_qp[1]
                 + tab_N[q][i] * x_qp
                 - tab_N[q][i] * function[q];
        
        el_F[i] += w[q] * ( residual ) * PetscAbsReal(detJ[q]);
      }
    }
    
    //ierr = VecSetValues(b,nbasis,idx,el_F,ADD_VALUES);CHKERRQ(ierr);
    for (i=0; i<nbasis; i++) {
      F[idx[i]] += el_F[i];
    }
  }
  
  PetscFree(detJ);
  PetscFree(xi);
  PetscFree(w);
  PetscFree(function);
  PetscFree(elcoords);
  PetscFree(el_x);
  PetscFree(el_F);

  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);

  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Helmholtz_AssembleResidual"
PetscErrorCode Helmholtz_AssembleResidual(DM dm,BCList bc,Vec X,Vec F)
{
  PetscErrorCode ierr;
  Vec Xloc,Floc;
  PetscScalar *LA_Xloc,*LA_Floc;
  
  //ierr = DMGetLocalVector(dm,&Xloc);CHKERRQ(ierr);
  //ierr = DMGetLocalVector(dm,&Floc);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&Xloc);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&Floc);CHKERRQ(ierr);

  ierr = DMGlobalToLocalBegin(dm,X,INSERT_VALUES,Xloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,X,INSERT_VALUES,Xloc);CHKERRQ(ierr);
  
	ierr = BCListInsertLocal(bc,Xloc);CHKERRQ(ierr);
  
	ierr = VecGetArray(Xloc,&LA_Xloc);CHKERRQ(ierr);
	
	/* compute Ax - b */
	ierr = VecZeroEntries(Floc);CHKERRQ(ierr);
	ierr = VecGetArray(Floc,&LA_Floc);CHKERRQ(ierr);

  /* local residual evaluation */
  ierr = Helmholtz_AssembleResidual_Local(dm,LA_Xloc,LA_Floc);CHKERRQ(ierr);

	ierr = VecRestoreArray(Floc,&LA_Floc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Xloc,&LA_Xloc);CHKERRQ(ierr);
  
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	ierr = DMLocalToGlobalBegin(dm,Floc,ADD_VALUES,F);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(dm,Floc,ADD_VALUES,F);CHKERRQ(ierr);
  
  //ierr = DMRestoreLocalVector(dm,&Floc);CHKERRQ(ierr);
  //ierr = DMRestoreLocalVector(dm,&Xloc);CHKERRQ(ierr);
  ierr = VecDestroy(&Floc);CHKERRQ(ierr);
  ierr = VecDestroy(&Xloc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = BCListResidualDirichlet(bc,X,F);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FindBoundary"
PetscBool FindBoundary(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[0] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[0] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary"
PetscErrorCode EvalDirichletBoundary(PetscScalar coor[],PetscScalar *val,void *ctx)
{
  *val = mms_evaluate_solution(coor[0],coor[1]);
  PetscFunctionReturn(0);
}

#include <dmtetimpl.h>

#undef __FUNCT__
#define __FUNCT__ "_DMTetBasisView2d_VTK"
PetscErrorCode _DMTetBasisView2d_VTK(DM dm,Vec field,const char name[])
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  FILE *fp;
  PetscInt i,e;
  const PetscScalar *LA_field;
  
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",tet->space->nnodes,tet->space->nnodes);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",e);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",e+1);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"1 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<tet->space->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",tet->space->coords[2*i],tet->space->coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  ierr = VecGetArrayRead(field,&LA_field);CHKERRQ(ierr);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"one\" format=\"ascii\"> \n");
  for (i=0; i<tet->space->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.4e ",LA_field[i]);
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  ierr = VecRestoreArrayRead(field,&LA_field);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

typedef enum { Q_MIN_BALL_H, Q_MAX_BALL_H, Q_VOL, Q_L2, Q_H1, Q_NUM_ENTRIES } IntegralQuantities;

#undef __FUNCT__
#define __FUNCT__ "IntegrateQuantities"
PetscErrorCode IntegrateQuantities(DM dm,Vec x,PetscReal E[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni,**GNixi,**GNieta,**dNdx,**dNdy;
  PetscReal *coords,*elcoords,*detJ,*elfield;
  PetscInt *element,nen,npe,dof;
  PetscInt e,i,q;
  const PetscScalar *LA_x;
  PetscReal vol_cell_range[2];
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&Ni);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&GNixi,&GNieta,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nbasis,&elfield);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal*)*nqp,&dNdx);
  PetscMalloc(sizeof(PetscReal*)*nqp,&dNdy);
  for (q=0; q<nqp; q++) {
    PetscMalloc(sizeof(PetscReal)*nbasis,&dNdx[q]);
    PetscMalloc(sizeof(PetscReal)*nbasis,&dNdy[q]);
  }
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  E[Q_MIN_BALL_H] = 0.0; /* min h ball */
  E[Q_MAX_BALL_H] = 0.0; /* max h ball */
  E[Q_VOL] = 0.0; /* vol */
  E[Q_L2] = 0.0; /* L2 */
  E[Q_H1] = 0.0; /* H1 */
  
  
  vol_cell_range[0] = PETSC_MAX_REAL;
  vol_cell_range[1] = PETSC_MIN_REAL;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    PetscReal vol_cell;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elfield[i] = LA_x[nidx];
    }
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,GNixi,GNieta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,GNixi,GNieta,dNdx,dNdy);
    
    vol_cell = 0.0;
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      PetscReal field_exact,field_approx;
      PetscReal grad_field_exact[2],grad_field_approx[2];
      PetscReal integrand;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + Ni[q][i] * elcoords[2*i+0];
        yq = yq + Ni[q][i] * elcoords[2*i+1];
      }
      
      /* evaluate solution */
      field_exact = mms_evaluate_solution(xq,yq);
      /* interpolate field to each quadrature point */
      field_approx = 0.0;
      for (i=0; i<nbasis; i++) {
        field_approx = field_approx + Ni[q][i] * elfield[i];
      }
      
      /* evaluate derivatives of solution */
      mms_evaluate_solution_gradients(xq,yq,&grad_field_exact[0],&grad_field_exact[1]);
      /* interpolate gradient of field to each quadrature point */
      grad_field_approx[0] = grad_field_approx[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        grad_field_approx[0] += dNdx[q][i] * elfield[i];
        grad_field_approx[1] += dNdy[q][i] * elfield[i];
      }
      
      vol_cell += w[q] * 1.0 * PetscAbsReal(detJ[q]);
      
      E[Q_VOL] += w[q] * 1.0 * PetscAbsReal(detJ[q]);
      
      integrand = (field_exact - field_approx)*(field_exact - field_approx);
      E[Q_L2] += w[q] * integrand * PetscAbsReal(detJ[q]);
      
      integrand  = (field_exact - field_approx)*(field_exact - field_approx);
      //integrand  = 0.0; //(field_exact - field_approx)*(field_exact - field_approx);
      integrand += (grad_field_exact[0] - grad_field_approx[0])*(grad_field_exact[0] - grad_field_approx[0]);
      integrand += (grad_field_exact[1] - grad_field_approx[1])*(grad_field_exact[1] - grad_field_approx[1]);
      E[Q_H1] += w[q] * integrand * PetscAbsReal(detJ[q]);
    }
    
    if (vol_cell < vol_cell_range[0]) { vol_cell_range[0] = vol_cell; }
    if (vol_cell > vol_cell_range[1]) { vol_cell_range[1] = vol_cell; }
  }
  
  E[Q_MIN_BALL_H] = PetscSqrtReal(vol_cell_range[0]/M_PI);
  E[Q_MAX_BALL_H] = PetscSqrtReal(vol_cell_range[1]/M_PI);
  E[Q_L2] = PetscSqrtReal(E[Q_L2]);
  E[Q_H1] = PetscSqrtReal(E[Q_H1]);
  
  /* tidy up */
  ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  PetscFree(elfield);
  PetscFree(detJ);
  PetscFree(elcoords);
  for (q=0; q<nqp; q++) {
    PetscFree(Ni[q]);
    PetscFree(GNixi[q]);
    PetscFree(GNieta[q]);
    PetscFree(dNdx[q]);
    PetscFree(dNdy[q]);
  }
  PetscFree(Ni);
  PetscFree(GNixi);
  PetscFree(GNieta);
  PetscFree(dNdx);
  PetscFree(dNdy);
  PetscFree(xi);
  PetscFree(w);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(PetscInt mx)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  BCList bc;
  Vec dX,X,F;
  Mat A;
  KSP ksp;
  
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-1.0,1.0,-1.0,1.0, mx,mx, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm,&dX);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundary,NULL,&bc);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);
  
  ierr = Helmholtz_AssembleBilinearSym(A,dm,bc);CHKERRQ(ierr);
  
  ierr = Helmholtz_AssembleResidual(dm,bc,X,F);CHKERRQ(ierr);
  ierr = VecScale(F,-1.0);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,F,dX);CHKERRQ(ierr);
  
  ierr = VecAXPY(X,1.0,dX);CHKERRQ(ierr);
  
  //ierr = _DMTetBasisView2d_VTK(dm,X,"mmssol.vtu");CHKERRQ(ierr);
  {
    PetscInt nf = 1;
    const char *fname[] = {"field"};
    
    ierr = DMTetViewFields_VTU(dm,nf,&X,fname,"mmssol_");CHKERRQ(ierr);
  }
  
  {
    PetscReal E[Q_NUM_ENTRIES];
    PetscInt order,nel,nbasis;
    
    ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(dm,&nel,0,0,0,&nbasis,0);CHKERRQ(ierr);
    
    ierr = IntegrateQuantities(dm,X,E);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"%.2D %1.4e  %1.12e %1.12e\n",order,1.0/((PetscReal)mx),E[Q_L2],E[Q_H1]);
  }
  
  
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscInt n,mxlist[] = { 8, 16, 32, 64, 128 };
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD," P          h                  L2            H1_semi\n");
  PetscPrintf(PETSC_COMM_WORLD,"----------------------------------------------------\n");
  for (n=0; n<sizeof(mxlist)/sizeof(PetscInt); n++) {
    ierr = example3(mxlist[n]);CHKERRQ(ierr);
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

