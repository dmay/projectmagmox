proj-tests-y.c += $(call thisdir, \
    test_dsde.c ptable.c \
    test_proj_init_finalize.c \
    test_dmtet_partition.c \
    test_dmtet.c \
    test_helmholtz_mms.c \
    test_helmholtz_sym_mms.c \
    test_dmtet_p2p1.c \
    test_pde.c \
    test_stokes.c \
    test_stokes_ann.c \
    test_dmtet_rt.c \
    test_dmtet_h5.c \
    test_dm_4field.c test_dm_3field.c \
    test_dmdasl.c \
    test_adfd.c test_adhelm.c \
    test_dmtetsubmesh.c \
    test_dmtetinterp.c \
    plot_Spiegelman87.c \
    test_dmtet_bfacets.c \
    test_pde_helmholtz_neumann.c \
    test_pde_stokes_mms.c \
    test_dmda_extrude.c \
    test_dmda_csl_multicomponent.c  \
    test_polycsl.c test_polycsl_ex2.c \
    test_multicomponent.c \
    test_pde_darcy_mms.c \
    test_pde_darcy.c \
    prealloc.c \
    test_dmtetproject.c \
)

PROJ_INC += -I$(abspath $(call thisdir,.))

include $(call incsubdirs,vankeken-benchmarks rhebergen-siam-2014 vankeken-twophase)

