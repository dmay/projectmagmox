
#include <stdio.h>
#include <math.h>
#include <petsc.h>
#include <Spiegelman87.h>

#undef __FUNCT__
#define __FUNCT__ "PlotRidge"
PetscErrorCode PlotRidge(void)
{
  PetscErrorCode ierr;
  PetscInt i,j,nx,ny;
  PetscReal W,D;
  PetscReal alpha,w0,u0;
  PetscReal coor[2],vel[2];
  //FILE *fp;
  DM da;
  Vec coordvec,V,v;
  const PetscScalar *LA_c;
  PetscScalar *LA_V;
  PetscViewer viewer;
  
  nx = 201;
  ny = 201;
  
  W = 10.0;
  D = 5.0;
  //dx = W / ((PetscReal)(nx-1));
  //dy = D / ((PetscReal)(ny-1));
  
  u0 = 1.0;
  w0 = u0/1.56;
  alpha = 40.0 * M_PI/180.0;
  
  ierr = DMDACreate2d(PETSC_COMM_SELF, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,
                      nx,ny,PETSC_DECIDE,PETSC_DECIDE,2,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMDASetFieldName(da,0,"u");CHKERRQ(ierr);
  ierr = DMDASetFieldName(da,1,"v");CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(da,-0.5*W,0.5*W,-D,0.0,0.0,0.0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da,&V);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da,&v);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da,&coordvec);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coordvec,&LA_c);CHKERRQ(ierr);
  
  /* solid velocity */
  ierr = VecGetArray(V,&LA_V);CHKERRQ(ierr);
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i+j*nx;
      
      coor[0] = LA_c[2*idx];
      coor[1] = LA_c[2*idx+1];
      
      ierr = Spiegelman87Ridge_EvaluateSolidVelocity(coor,alpha,w0,u0,vel);CHKERRQ(ierr);
      LA_V[2*idx]   = vel[0];
      LA_V[2*idx+1] = vel[1];
    }
  }
  ierr = VecRestoreArray(V,&LA_V);CHKERRQ(ierr);

  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"Spiegelman87Ridge_vs.vts");CHKERRQ(ierr);
  ierr = VecView(V,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  /* fluid velocity */
  ierr = VecGetArray(v,&LA_V);CHKERRQ(ierr);
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i+j*nx;
      
      coor[0] = LA_c[2*idx];
      coor[1] = LA_c[2*idx+1];
      
      ierr = Spiegelman87Ridge_EvaluateFluidVelocity(coor,alpha,w0,u0,vel);CHKERRQ(ierr);
      LA_V[2*idx]   = vel[0];
      LA_V[2*idx+1] = vel[1];
    }
  }
  ierr = VecRestoreArray(v,&LA_V);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"Spiegelman87Ridge_vf.vts");CHKERRQ(ierr);
  ierr = VecView(v,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  

  /* pressure */
  //fp = fopen("Spiegelman87Ridge_p.gp","w");
  //fclose(fp);
  
  ierr = VecRestoreArrayRead(coordvec,&LA_c);CHKERRQ(ierr);
  
  ierr = VecDestroy(&v);CHKERRQ(ierr);
  ierr = VecDestroy(&V);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PlotTrench"
PetscErrorCode PlotTrench(void)
{
  PetscErrorCode ierr;
  PetscInt i,j,nx,ny;
  PetscReal W,D;
  PetscReal beta,w0,u0;
  PetscReal coor[2],vel[2];
  //FILE *fp;
  DM da;
  Vec coordvec,V,v;
  const PetscScalar *LA_c;
  PetscScalar *LA_V;
  PetscViewer viewer;
  
  nx = 31;
  ny = 31;
  
  W = 10.0;
  D = 5.0;
  //dx = W / ((PetscReal)(nx-1));
  //dy = D / ((PetscReal)(ny-1));
  
  u0 = 1.0;
  w0 = u0/1.56;
  beta = 30.0 * M_PI/180.0;
  
  ierr = DMDACreate2d(PETSC_COMM_SELF, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,
                      nx,ny,PETSC_DECIDE,PETSC_DECIDE,2,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMDASetFieldName(da,0,"u");CHKERRQ(ierr);
  ierr = DMDASetFieldName(da,1,"v");CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(da,-W,0.0,-D,0.0,0.0,0.0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da,&V);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da,&v);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da,&coordvec);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coordvec,&LA_c);CHKERRQ(ierr);
  
  /* solid velocity */
  ierr = VecGetArray(V,&LA_V);CHKERRQ(ierr);
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i+j*nx;
      
      coor[0] = LA_c[2*idx];
      coor[1] = LA_c[2*idx+1];
      
      ierr = Spiegelman87Trench_EvaluateSolidVelocity(coor,beta,w0,u0,vel);CHKERRQ(ierr);
      LA_V[2*idx]   = vel[0];
      LA_V[2*idx+1] = vel[1];
    }
  }
  ierr = VecRestoreArray(V,&LA_V);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"Spiegelman87Trench_vs.vts");CHKERRQ(ierr);
  ierr = VecView(V,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  /* fluid velocity */
  ierr = VecGetArray(v,&LA_V);CHKERRQ(ierr);
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscInt idx = i+j*nx;
      
      coor[0] = LA_c[2*idx];
      coor[1] = LA_c[2*idx+1];
      
      ierr = Spiegelman87Trench_EvaluateFluidVelocity(coor,beta,w0,u0,vel);CHKERRQ(ierr);
      LA_V[2*idx]   = vel[0];
      LA_V[2*idx+1] = vel[1];
    }
  }
  ierr = VecRestoreArray(v,&LA_V);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"Spiegelman87Trench_vf.vts");CHKERRQ(ierr);
  ierr = VecView(v,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  
  /* pressure */
  //fp = fopen("Spiegelman87Trench_p.gp","w");
  //fclose(fp);
  
  ierr = VecRestoreArrayRead(coordvec,&LA_c);CHKERRQ(ierr);
  
  ierr = VecDestroy(&v);CHKERRQ(ierr);
  ierr = VecDestroy(&V);CHKERRQ(ierr);
  ierr = DMDestroy(&da);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int nargs,char *args[])
{
  PetscErrorCode ierr;
  
  ierr = PetscInitialize(&nargs,&args,NULL,NULL);CHKERRQ(ierr);
  
  ierr = PlotRidge();CHKERRQ(ierr);
  ierr = PlotTrench();CHKERRQ(ierr);
  
  ierr = PetscFinalize();CHKERRQ(ierr);
  return(0);
}
