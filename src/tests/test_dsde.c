
#include <petsc.h>
#include <dsde.h>
#include <proj_init_finalize.h>

// [example_1]
// Use the DSDE once then destroy it.
// Demonstrates basic usage
//
PetscErrorCode example_1(void)
{
  PetscErrorCode ierr;
  DSDataEx dsde;
  double value;
  PetscMPIInt commrank,commsize;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  ierr = DSDataExCreate(PETSC_COMM_WORLD,&dsde);CHKERRQ(ierr);
  ierr = DSDataExPackInitialize(dsde,sizeof(double));CHKERRQ(ierr);
  
  if (commrank == 1) {
    value = 16.6;
    ierr = DSDataExPackData(dsde,0,1,(void*)&value);CHKERRQ(ierr);
  }

  if (commrank == 10) {
    value = 36.6;
    ierr = DSDataExPackData(dsde,0,1,(void*)&value);CHKERRQ(ierr);
  }

  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);

  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  ierr = DSDataExEnd(dsde);CHKERRQ(ierr);
  
  {
    double *data = NULL;
    int i,len;
    
    ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] %+1.2e\n",commrank,i,data[i]);
    }

    ierr = DSDataExGetRecvDataByRank(dsde,0,&len,(void**)&data);CHKERRQ(ierr);
    printf("rank[%d]: recv %d items from source r = 0\n",commrank,len);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] from source r = 0 %+1.2e\n",commrank,i,data[i]);
    }
    ierr = DSDataExGetRecvDataByRank(dsde,1,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] from source r = 1 %+1.2e\n",commrank,i,data[i]);
    }

  }
  
  ierr = DSDataExView(dsde);CHKERRQ(ierr);
  ierr = DSDataExDestroy(&dsde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

// [example_2]
// Use the DSDE twice with the same topology but different data types then destroy it.
// Demonstrates the usage of
//   DSDataExTopologySetMode(dsde,DSDE_TOPOLOGY_LOCKED)
// to ensure the send send/recv phase does not introduce any new rank -> dest pairs.
//
PetscErrorCode example_2(void)
{
  PetscErrorCode ierr;
  DSDataEx dsde;
  double value;
  int value_i;
  PetscMPIInt commrank,commsize;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  ierr = DSDataExCreate(PETSC_COMM_WORLD,&dsde);CHKERRQ(ierr);
  
  ierr = DSDataExPackInitialize(dsde,sizeof(double));CHKERRQ(ierr);
  
  if (commrank == 0) {
    value = 16.6;
    ierr = DSDataExPackData(dsde,1,1,(void*)&value);CHKERRQ(ierr);
  }
  
  if (commrank == 10) {
    value = 36.6;
    ierr = DSDataExPackData(dsde,0,1,(void*)&value);CHKERRQ(ierr);
  }
  
  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  ierr = DSDataExEnd(dsde);CHKERRQ(ierr);
  
  {
    double *data = NULL;
    int i,len;
    
    ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] %+1.2e\n",commrank,i,data[i]);
    }
    
    ierr = DSDataExGetRecvDataByRank(dsde,0,&len,(void**)&data);CHKERRQ(ierr);
    printf("rank[%d]: recv %d items from source r = 0\n",commrank,len);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] from source r = 0 %+1.2e\n",commrank,i,data[i]);
    }
    ierr = DSDataExGetRecvDataByRank(dsde,1,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] from source r = 1 %+1.2e\n",commrank,i,data[i]);
    }
    
  }
  
  ierr = DSDataExView(dsde);CHKERRQ(ierr);
  
  ierr = MPI_Barrier(PETSC_COMM_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"================================\n");

  ierr = DSDataExTopologySetMode(dsde,DSDE_TOPOLOGY_LOCKED);CHKERRQ(ierr);
  
  ierr = DSDataExPackInitialize(dsde,sizeof(int));CHKERRQ(ierr);
  if (commrank == 0) {
    value_i = 33;
    ierr = DSDataExPackData(dsde,1,1,(void*)&value_i);CHKERRQ(ierr);
  }
  
  if (commrank == 10) {
    value_i = 99;
    ierr = DSDataExPackData(dsde,0,1,(void*)&value_i);CHKERRQ(ierr);
  }

  // When calling
  //   ierr = DSDataExTopologySetMode(dsde,DSDE_TOPOLOGY_LOCKED);CHKERRQ(ierr);
  // the code below will produce an error
  //if (commrank == 3) {
  //  value_i = 9;
  //  ierr = DSDataExPackData(dsde,2,1,(void*)&value_i);CHKERRQ(ierr);
  //}
  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);

  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  ierr = DSDataExEnd(dsde);CHKERRQ(ierr);
  
  {
    int *data = NULL;
    int i,len;
    
    ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] %d\n",commrank,i,data[i]);
    }
    
    ierr = DSDataExGetRecvDataByRank(dsde,0,&len,(void**)&data);CHKERRQ(ierr);
    printf("rank[%d]: recv %d items from source r = 0\n",commrank,len);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] from source r = 0 %d\n",commrank,i,data[i]);
    }
    ierr = DSDataExGetRecvDataByRank(dsde,1,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("rank[%d]: recv[%d] from source r = 1 %d\n",commrank,i,data[i]);
    }
    
  }

  ierr = DSDataExView(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExDestroy(&dsde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

// [example_3]
// Performs two send/recv phases with a flush in between and then destroys the object.
// Demonstrates the usage of
//   DSDataExTopologySetUseOnlyActiveNeighbours(dsde,PETSC_TRUE)
// to reduce the volume of empty messages being send during multiple send/recv phases.
//
PetscErrorCode example_3(void)
{
  PetscErrorCode ierr;
  DSDataEx dsde;
  double value;
  PetscMPIInt commrank,commsize;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  ierr = DSDataExCreate(PETSC_COMM_WORLD,&dsde);CHKERRQ(ierr);
  
  ierr = DSDataExPackInitialize(dsde,sizeof(double));CHKERRQ(ierr);
  
  if (commrank == 0) {
    value = 16.6;
    ierr = DSDataExPackData(dsde,1,1,(void*)&value);CHKERRQ(ierr);
  }
  if (commrank == 1) {
    value = 18.6;
    ierr = DSDataExPackData(dsde,2,1,(void*)&value);CHKERRQ(ierr);
  }
  if (commrank == 3) {
    value = 20.6;
    ierr = DSDataExPackData(dsde,0,1,(void*)&value);CHKERRQ(ierr);
  }
  
  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  ierr = DSDataExEnd(dsde);CHKERRQ(ierr);

  {
    double *data = NULL;
    int i,len;
    
    ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("[i] rank[%d]: recv[%d] %lf\n",commrank,i,data[i]);
    }
  }

  // flush
  ierr = DSDataExPackFlush(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExTopologySetUseOnlyActiveNeighbours(dsde,PETSC_TRUE);CHKERRQ(ierr);
  
  if (commrank == 1) {
    value = 36.6;
    ierr = DSDataExPackData(dsde,4,1,(void*)&value);CHKERRQ(ierr);
  }
  if (commrank == 0) {
    value = 106.6;
    ierr = DSDataExPackData(dsde,2,1,(void*)&value);CHKERRQ(ierr);
  }
  
  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);

  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  ierr = DSDataExEnd(dsde);CHKERRQ(ierr);

  {
    double *data = NULL;
    int i,len;
    
    ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("[ii] rank[%d]: recv[%d] %lf\n",commrank,i,data[i]);
    }
  }
  
  ierr = DSDataExView(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExDestroy(&dsde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

// [example_4]
// All-to-zero
// This is not memory scalable as it will fetch all the data from all ranks on root
// DSDE should be modified to support a memory scalable partial gather operation
//
PetscErrorCode example_4(void)
{
  PetscErrorCode ierr;
  DSDataEx dsde;
  double value;
  PetscMPIInt commrank,commsize;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  ierr = DSDataExCreate(PETSC_COMM_WORLD,&dsde);CHKERRQ(ierr);
  
  ierr = DSDataExPackInitialize(dsde,sizeof(double));CHKERRQ(ierr);

  value = (double)commrank + 0.01;
  ierr = DSDataExPackData(dsde,0,1,(void*)&value);CHKERRQ(ierr);

  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  ierr = DSDataExEnd(dsde);CHKERRQ(ierr);
  
  {
    double *data = NULL;
    int i,len;
    
    ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
    for (i=0; i<len; i++) {
      printf("[ii] rank[%d]: recv[%d] %lf\n",commrank,i,data[i]);
    }
  }
  
  ierr = DSDataExView(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExDestroy(&dsde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#ifdef DSDE_PARTIAL_GATHER
// [example_4a]
// All-to-zero but avoids collecting ALL data at once (memory scalable)
//
PetscErrorCode example_4a(void)
{
  PetscErrorCode ierr;
  DSDataEx dsde;
  double value;
  PetscMPIInt k,commrank,commsize;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  ierr = DSDataExCreate(PETSC_COMM_WORLD,&dsde);CHKERRQ(ierr);
  
  ierr = DSDataExPackInitialize(dsde,sizeof(double));CHKERRQ(ierr);
  
  value = (double)commrank + 0.01;
  ierr = DSDataExPackData(dsde,0,1,(void*)&value);CHKERRQ(ierr);
  
  ierr = DSDataExPackFinalize(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExBegin(dsde);CHKERRQ(ierr);
  //ierr = DSDataExEnd(dsde);CHKERRQ(ierr);
  for (k=commsize-1; k>=1; k--) {
    if (k == (commsize-1) ) {
      ierr = DSDataExEndByRank(dsde,PETSC_TRUE,PETSC_FALSE,k);CHKERRQ(ierr);
    } else if (k == 1) {
      ierr = DSDataExEndByRank(dsde,PETSC_FALSE,PETSC_TRUE,k);CHKERRQ(ierr);
    } else {
      ierr = DSDataExEndByRank(dsde,PETSC_FALSE,PETSC_FALSE,k);CHKERRQ(ierr);
    }
    
    //if (commrank == 0) {
      double *data = NULL;
      int i,len;
      
      ierr = DSDataExGetRecvData(dsde,&len,(void**)&data);CHKERRQ(ierr);
      for (i=0; i<len; i++) {
        printf("[ii] rank[%d]: recv[%d] %lf\n",commrank,i,data[i]);
      }
    //}
    dsde->communication_status = DSDEOBJECT_INITIALIZED;
  }
  //ierr = DSDataExEndByRank(dsde,PETSC_FALSE,PETSC_FALSE,commsize-1);CHKERRQ(ierr);// test check for repeated recvs (currently not caught correctly)
  
  //ierr = DSDataExView(dsde);CHKERRQ(ierr);
  
  ierr = DSDataExDestroy(&dsde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
#endif

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  //ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  ierr = PetscInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  for (int i=0; i<1; i++) {
    //ierr = example_1();CHKERRQ(ierr);
    //ierr = example_2();CHKERRQ(ierr);
    ierr = example_3();CHKERRQ(ierr);
    //ierr = example_4();CHKERRQ(ierr);
#ifdef DSDE_PARTIAL_GATHER
    /*ierr = example_4a();CHKERRQ(ierr);*/
#endif
  }
  
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}

