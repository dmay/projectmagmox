
#include <petsc.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdestokes.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>


#undef __FUNCT__
#define __FUNCT__ "fp_quadrature"
PetscErrorCode fp_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp;
  PetscReal *coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature"
PetscErrorCode eta_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp;
  PetscReal *coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 1.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature"
PetscErrorCode fu_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  dm = (DM)data;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq,r2;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      r2 = PetscSqrtReal( (xq-0.5)*(xq-0.5) + (yq-0.5)*(yq-0.5) );
      
      coeff[2*(e*nqp + q)+0] = 0.0;
      coeff[2*(e*nqp + q)+1] = 0.0;
      if (r2 < 0.3) {
        coeff[2*(e*nqp + q)+1] = -1.0;
      }
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_u"
PetscErrorCode EvalDirichletBoundary_u(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_FALSE;
  
  if (coor[0] < 1.0e-10) { /* left */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (coor[0] > 1.0 - 1.0e-10) { /* right */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_v"
PetscErrorCode EvalDirichletBoundary_v(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_FALSE;
  
  if (coor[1] < 1.0e-10) { /* bottom */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (coor[1] > 1.0 - 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "stokes_test1"
PetscErrorCode stokes_test1(void)
{
  PetscErrorCode ierr;
  DM dmu,dmp,dms;
  PDE pde;
  SNES snes;
  Mat A;
  Vec X,F;
  BCList bc;
  PetscInt k;

  k = 1;
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, 8,8, PETSC_TRUE,
                                     2,DMTET_CWARP_AND_BLEND,k+1,&dmu);CHKERRQ(ierr);

  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, 8,8, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,k,&dmp);CHKERRQ(ierr);

  ierr = DMBCListCreate(dmu,&bc);CHKERRQ(ierr);
  //ierr = DMTetCreateDirichletList_Default(dmu,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary_u,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,1,EvalDirichletBoundary_v,NULL);CHKERRQ(ierr);

  ierr = PDECreateStokes(PETSC_COMM_WORLD,dmu,dmp,bc,&pde);CHKERRQ(ierr);
  ierr = PDEStokesGetDM(pde,&dms);CHKERRQ(ierr);
  
//  ierr = DMCreateMatrix(dms,&A);CHKERRQ(ierr);
//  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
//  ierr = DMCreateGlobalVector(dms,&F);CHKERRQ(ierr);
  
  ierr = PDEStokesCreateOperator_MatNest(pde,&A);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&X,&F);CHKERRQ(ierr);

  //MatMult(A,X,F);

  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmu,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmp,&xp[1]);CHKERRQ(ierr);

    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);

    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }

  /* boundary condition */
  {
    Vec velocity,pressure;
    
    ierr = DMCompositeGetAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
    ierr = BCListInsert(bc,velocity);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
  }

  {
    Coefficient e,fu,fp;
    
    ierr = PDEStokesGetCoefficients(pde,&e,&fu,&fp);CHKERRQ(ierr);

    ierr = CoefficientSetComputeQuadrature(e,eta_quadrature,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fp,fp_quadrature,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)dmp);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,A,A);CHKERRQ(ierr);
  
  //SNESComputeFunction(snes,X,F);
  //SNESComputeJacobian(snes,X,A,A);
  
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  ierr = PDEStokesFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
  
  //ierr = PDEView(pde,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 -ksp_monitor_true_residual -pc_type fieldsplit  -pc_fieldsplit_type schur -ksp_converged_reason -fieldsplit_u_pc_type ilu -fieldsplit_p_pc_type none -ksp_type fgmres -k 1 -fieldsplit_p_ksp_max_it 1 -fieldsplit_u_ksp_rtol 1.0e-1 -dmtet_square_mx 32 -dmtet_square_my 32
*/
int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt tidx = 1;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(NULL,NULL,"-tidx",&tidx,NULL);CHKERRQ(ierr);
  switch (tidx) {
    case 1:
      ierr = stokes_test1();CHKERRQ(ierr);
      break;
    case 2:
      break;
    case 3:
      break;
      
    default:
      break;
  }
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

