
/*
 
 Solves
   div( grad(u) ) + u = g
 using P_k basis defined over triangles.
 
 The chosen solution is
   u(x,y) =  cos(\pi y) sin(\pi x)
 The function g(x,y) is manufactured.

 We solve the following discrete problem
   A x = y
 
 The assembly and imposition of the Dirichlet boundary within
 A does not preserve symmetry
 
*/

#include <petsc.h>
#include <petscdmtet.h>
#include <dmbcs.h>
#include <quadrature.h>
#include <proj_init_finalize.h>
#include <dmtetimpl.h>

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution"
PetscReal mms_evaluate_solution(PetscReal x,PetscReal y)
{
  PetscReal u;
  
  //u = tanh( 6.0 * ( x*x + y*y - 0.5*0.5 ) );
  u =  cos(M_PI*y)*sin(M_PI*x);
  
  return u;
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution_gradients"
void mms_evaluate_solution_gradients(PetscReal x,PetscReal y,PetscReal *dudx,PetscReal *dudy)
{
  
  //*dudx = 12.0*x*(1.0 - pow(tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)),2.0));
  //*dudy = 12.0*y*(1.0 - pow(tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)),2.0));
  *dudx =  M_PI*cos(M_PI*x)*cos(M_PI*y);
  *dudy =  -M_PI*sin(M_PI*x)*sin(M_PI*y);
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_rhs"
PetscReal mms_evaluate_rhs(PetscReal x,PetscReal y)
{
  PetscReal f;
  
  //f = -24.0 - tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)) - 288.0*pow(x,2.0)*(1.0 - pow(tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)),2.0))*tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)) - 288.0*pow(y,2.0)*(1.0 - pow(tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)),2.0))*tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)) + 24.0*pow(tanh(1.5 - 6.0*pow(x,2.0) - 6.0*pow(y,2.0)),2.0);
  
  f =  cos(M_PI*y)*sin(M_PI*x) + 2*pow(M_PI,2)*cos(M_PI*y)*sin(M_PI*x);
  
  return f;
}

void ElementEvaluateGeometry_2d(PetscInt nqp,PetscInt nbasis,PetscReal el_coords[],
                                PetscReal **GNIxi,PetscReal **GNIeta,
                                PetscReal detJ[])
{
  PetscInt k,q;
  PetscReal J[2][2];
  
  for (q=0; q<nqp; q++) {
    //
    J[0][0] = J[0][1] = 0.0;
    J[1][0] = J[1][1] = 0.0;
    
    for (k=0; k<nbasis; k++) {
      PetscReal xc = el_coords[2*k+0];
      PetscReal yc = el_coords[2*k+1];
      
      J[0][0] += GNIxi[q][k] * xc ;
      J[0][1] += GNIxi[q][k] * yc ;
      
      J[1][0] += GNIeta[q][k] * xc ;
      J[1][1] += GNIeta[q][k] * yc ;
    }
    
    detJ[q] = J[0][0]*J[1][1] - J[0][1]*J[1][0];
  }
}

void ElementEvaluateDerivatives_2d(PetscInt nqp,PetscInt nbasis,PetscReal el_coords[],
                                   PetscReal **GNIxi,PetscReal **GNIeta,
                                   PetscReal **dNudx,PetscReal **dNudy)
{
  PetscInt k,q;
  PetscReal J[2][2],iJ[2][2],od,detJ;
  
  for (q=0; q<nqp; q++) {
    //
    J[0][0] = J[0][1] = 0.0;
    J[1][0] = J[1][1] = 0.0;
    
    for (k=0; k<nbasis; k++) {
      PetscReal xc = el_coords[2*k+0];
      PetscReal yc = el_coords[2*k+1];
      
      J[0][0] += GNIxi[q][k] * xc ;
      J[0][1] += GNIxi[q][k] * yc ;
      
      J[1][0] += GNIeta[q][k] * xc ;
      J[1][1] += GNIeta[q][k] * yc ;
    }
    
    detJ = J[0][0]*J[1][1] - J[0][1]*J[1][0];
    od = 1.0/detJ;
    
    iJ[0][0] =  J[1][1] * od;
    iJ[0][1] = -J[0][1] * od;
    iJ[1][0] = -J[1][0] * od;
    iJ[1][1] =  J[0][0] * od;
    
    /* shape function derivatives */
    for (k=0; k<nbasis; k++) {
      dNudx[q][k] = iJ[0][0]*GNIxi[q][k] + iJ[0][1]*GNIeta[q][k];
      dNudy[q][k] = iJ[1][0]*GNIxi[q][k] + iJ[1][1]*GNIeta[q][k];
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "HelmholtzAssembleBilinear"
PetscErrorCode HelmholtzAssembleBilinear(Mat A,DM dm,BCList bc)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,npe,i,j;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *coords,*Ke;
  PetscInt q,nqp,nb;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  /*int crank;*/
  /*DM_TET *tet = (DM_TET*)dm->data;*/
  
  /*MPI_Comm_rank(PETSC_COMM_WORLD,&crank);*/
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  nbasis = npe;
  
  PetscMalloc1(2*nbasis,&elcoords);
  PetscMalloc1(nbasis,&eldofs);
  PetscMalloc1(nbasis*nbasis,&Ke);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  PetscMalloc1(nqp,&detJ);
  
  for (e=0; e<nel; e++) {
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    /* generate dofs */
    for (i=0; i<nbasis; i++) {
      eldofs[i] = elnidx[i];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = elnidx[i];
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
      
      //PetscPrintf(PETSC_COMM_SELF,"[r%d] el %d eg (%d): i %d : xy %+1.8e %+1.8e\n",
      //            crank,e,tet->partition->element_gid_local[e],i,elcoords[2*i  ],elcoords[2*i+1]);
    }
    
    /* compute derivatives */
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Ni,*dNidx,*dNidy;
      
      /* get access to element->quadrature points */
      Ni    = tab_N[q];
      dNidx = tab_dNdx[q];
      dNidy = tab_dNdy[q];
      
      fac = PetscAbsReal(detJ[q]) * w[q];
      
      for (i=0; i<nbasis; i++) {
        for (j=0; j<nbasis; j++) {
          Ke[ i*nbasis + j ] += fac * (dNidx[i]*dNidx[j] + dNidy[i]*dNidy[j] + Ni[i]*Ni[j]);
        }
      }
      
    } // quadrature
    
    for (i=0; i<nbasis*nbasis; i++) {
      if (PetscAbsReal(Ke[i]) < 1.0e-12) {
        Ke[i] = 0.0;
      }
    }
    //for (i=0; i<nbasis*nbasis; i++) {
    //  PetscPrintf(PETSC_COMM_SELF,"[r%d] el %d eg (%d): i %d : %+1.8e\n",crank,e,tet->partition->element_gid_local[e],i,Ke[i]);
    //}
    
    ierr = MatSetValuesLocal(A,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  ierr = MatSetOption(A,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  ierr = BCListZeroRows(bc,A);CHKERRQ(ierr);

  PetscFree(w);
  PetscFree(xi);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Ke);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleLinearForm"
PetscErrorCode AssembleLinearForm(DM dm,Vec b,BCList bc)
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni,**GNix,**GNiy;
  PetscReal *coords,*elcoords,*detJ,*function;
  PetscInt *element,nen,npe,dof;
  PetscInt e,i,q;
  PetscScalar *bj;
  
  
  ierr = VecZeroEntries(b);CHKERRQ(ierr);
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&Ni);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&GNix,&GNiy,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscScalar)*nbasis*dof,&bj);
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal)*nqp,&function);
  
  /* zero pass through */
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    PetscMemzero(bj,sizeof(PetscScalar)*nbasis*dof);
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,GNix,GNiy,detJ);
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + Ni[q][i] * elcoords[2*i+0];
        yq = yq + Ni[q][i] * elcoords[2*i+1];
      }
      
      function[q] = mms_evaluate_rhs(xq,yq);
    }
    
    for (q=0; q<nqp; q++) {
      for (i=0; i<nbasis; i++) {
        bj[i] += w[q] * Ni[q][i] * function[q] * PetscAbsReal(detJ[q]);
      }
    }
    ierr = VecSetValuesLocal(b,nbasis,idx,bj,ADD_VALUES);CHKERRQ(ierr);
  }
  
  ierr = VecAssemblyBegin(b);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(b);CHKERRQ(ierr);
  
  PetscFree(function);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(bj);
  for (q=0; q<nqp; q++) {
    PetscFree(Ni[q]);
    PetscFree(GNix[q]);
    PetscFree(GNiy[q]);
  }
  PetscFree(Ni);
  PetscFree(GNix);
  PetscFree(GNiy);
  PetscFree(xi);
  PetscFree(w);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FindBoundary"
PetscBool FindBoundary(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[0] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[0] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary"
PetscErrorCode EvalDirichletBoundary(PetscScalar coor[],PetscScalar *val,void *ctx)
{
  *val = mms_evaluate_solution(coor[0],coor[1]);
  PetscFunctionReturn(0);
}

#include <dmtetimpl.h>

#undef __FUNCT__
#define __FUNCT__ "_DMTetBasisView2d_VTK"
PetscErrorCode _DMTetBasisView2d_VTK(DM dm,Vec field,const char name[])
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  FILE *fp;
  PetscInt i,e;
  const PetscScalar *LA_field;
  
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",tet->space->nnodes,tet->space->nnodes);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",e);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",e+1);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"1 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<tet->space->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",tet->space->coords[2*i],tet->space->coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  ierr = VecGetArrayRead(field,&LA_field);CHKERRQ(ierr);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"one\" format=\"ascii\"> \n");
  for (i=0; i<tet->space->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.4e ",LA_field[i]);
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  ierr = VecRestoreArrayRead(field,&LA_field);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

typedef enum { Q_MIN_BALL_H=0, Q_MAX_BALL_H, Q_VOL, Q_L2, Q_H1, Q_NUM_ENTRIES } IntegralQuantities;

#undef __FUNCT__
#define __FUNCT__ "IntegrateQuantities"
PetscErrorCode IntegrateQuantities(DM dm,Vec x,PetscReal E[])
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni,**GNixi,**GNieta,**dNdx,**dNdy;
  PetscReal *coords,*elcoords,*detJ,*elfield;
  PetscInt *element,nen,npe,dof;
  PetscInt e,i,q;
  Vec xl;
  const PetscScalar *LA_x;
  PetscReal vol_cell_range[2];
  PetscReal E_g[Q_NUM_ENTRIES];
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&Ni);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nbasis,&GNixi,&GNieta,NULL);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);
  PetscMalloc(sizeof(PetscReal)*nbasis,&elfield);
  PetscMalloc(sizeof(PetscReal)*nqp,&detJ);
  PetscMalloc(sizeof(PetscReal*)*nqp,&dNdx);
  PetscMalloc(sizeof(PetscReal*)*nqp,&dNdy);
  for (q=0; q<nqp; q++) {
    PetscMalloc(sizeof(PetscReal)*nbasis,&dNdx[q]);
    PetscMalloc(sizeof(PetscReal)*nbasis,&dNdy[q]);
  }
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(xl,&LA_x);CHKERRQ(ierr);
  
  E[Q_MIN_BALL_H] = 0.0; /* min h ball */
  E[Q_MAX_BALL_H] = 0.0; /* max h ball */
  E[Q_VOL] = 0.0; /* vol */
  E[Q_L2] = 0.0; /* L2 */
  E[Q_H1] = 0.0; /* H1 */
  
  
  vol_cell_range[0] = PETSC_MAX_REAL;
  vol_cell_range[1] = PETSC_MIN_REAL;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    PetscReal vol_cell;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elfield[i] = LA_x[nidx];
    }
    
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,GNixi,GNieta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,GNixi,GNieta,dNdx,dNdy);
    
    vol_cell = 0.0;
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      PetscReal field_exact,field_approx;
      PetscReal grad_field_exact[2],grad_field_approx[2];
      PetscReal integrand;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + Ni[q][i] * elcoords[2*i+0];
        yq = yq + Ni[q][i] * elcoords[2*i+1];
      }
      
      /* evaluate solution */
      field_exact = mms_evaluate_solution(xq,yq);
      /* interpolate field to each quadrature point */
      field_approx = 0.0;
      for (i=0; i<nbasis; i++) {
        field_approx = field_approx + Ni[q][i] * elfield[i];
      }
      
      /* evaluate derivatives of solution */
      mms_evaluate_solution_gradients(xq,yq,&grad_field_exact[0],&grad_field_exact[1]);
      /* interpolate gradient of field to each quadrature point */
      grad_field_approx[0] = grad_field_approx[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        grad_field_approx[0] += dNdx[q][i] * elfield[i];
        grad_field_approx[1] += dNdy[q][i] * elfield[i];
      }
      
      vol_cell += w[q] * 1.0 * PetscAbsReal(detJ[q]);
      
      E[Q_VOL] += w[q] * 1.0 * PetscAbsReal(detJ[q]);
      
      integrand = (field_exact - field_approx)*(field_exact - field_approx);
      E[Q_L2] += w[q] * integrand * PetscAbsReal(detJ[q]);
      
      integrand  = (field_exact - field_approx)*(field_exact - field_approx);
      //integrand  = 0.0; //(field_exact - field_approx)*(field_exact - field_approx);
      integrand += (grad_field_exact[0] - grad_field_approx[0])*(grad_field_exact[0] - grad_field_approx[0]);
      integrand += (grad_field_exact[1] - grad_field_approx[1])*(grad_field_exact[1] - grad_field_approx[1]);
      E[Q_H1] += w[q] * integrand * PetscAbsReal(detJ[q]);
    }
    
    if (vol_cell < vol_cell_range[0]) { vol_cell_range[0] = vol_cell; }
    if (vol_cell > vol_cell_range[1]) { vol_cell_range[1] = vol_cell; }
  }

  /* tidy up */
  ierr = VecRestoreArrayRead(xl,&LA_x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);

  //typedef enum { Q_MIN_BALL_H, Q_MAX_BALL_H, Q_VOL, Q_L2, Q_H1, Q_NUM_ENTRIES } IntegralQuantities;
  ierr = MPI_Allreduce(&E[Q_MIN_BALL_H],&E_g[Q_MIN_BALL_H],1,MPIU_REAL,MPIU_MIN,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&E[Q_MAX_BALL_H],&E_g[Q_MAX_BALL_H],1,MPIU_REAL,MPIU_MAX,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&E[Q_VOL],&E_g[Q_VOL],1,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&E[Q_L2],&E_g[Q_L2],1,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&E[Q_H1],&E_g[Q_H1],1,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  
  ierr = PetscMemcpy(E,E_g,sizeof(PetscReal)*Q_NUM_ENTRIES);CHKERRQ(ierr);
  
  E[Q_MIN_BALL_H] = PetscSqrtReal(vol_cell_range[0]/M_PI);
  E[Q_MAX_BALL_H] = PetscSqrtReal(vol_cell_range[1]/M_PI);
  E[Q_L2] = PetscSqrtReal(E[Q_L2]);
  E[Q_H1] = PetscSqrtReal(E[Q_H1]);
  
  
  PetscFree(elfield);
  PetscFree(detJ);
  PetscFree(elcoords);
  for (q=0; q<nqp; q++) {
    PetscFree(Ni[q]);
    PetscFree(GNixi[q]);
    PetscFree(GNieta[q]);
    PetscFree(dNdx[q]);
    PetscFree(dNdy[q]);
  }
  PetscFree(Ni);
  PetscFree(GNixi);
  PetscFree(GNieta);
  PetscFree(dNdx);
  PetscFree(dNdy);
  PetscFree(xi);
  PetscFree(w);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(PetscInt mx)
{
  PetscErrorCode ierr;
  DM dms=NULL,dm;
  PetscReal gmin[3],gmax[3];
  BCList bc;
  Vec x,y;
  Mat A;
  KSP ksp;
  PetscMPIInt commrank;
  PetscInt degree;

  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  if (commrank == 0) {
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-1.0,1.0,-1.0,1.0, mx,mx, PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,1,&dms);CHKERRQ(ierr);
    ierr = DMSetUp(dms);CHKERRQ(ierr);
  }

  degree = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-tet_basis_order",&degree,NULL);CHKERRQ(ierr);
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dms,1,DMTET_CWARP_AND_BLEND,degree,&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dms);CHKERRQ(ierr);
  ierr = DMSetUp(dm);CHKERRQ(ierr);

  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundary,NULL,&bc);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  
  
  ierr = DMCreateGlobalVector(dm,&x);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&y);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  
  ierr = HelmholtzAssembleBilinear(A,dm,bc);CHKERRQ(ierr);
  
  ierr = AssembleLinearForm(dm,y,bc);CHKERRQ(ierr);
  //ierr = _DMTetBasisView2d_VTK(dm,y,"mmsrhs.vtu");CHKERRQ(ierr);

  ierr = BCListInsert(bc,y);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,y,x);CHKERRQ(ierr);
  
  //ierr = _DMTetBasisView2d_VTK(dm,x,"mmssol.vtu");CHKERRQ(ierr);
  {
    PetscInt nf = 1;
    const char *fname[] = {"field"};
    
    ierr = DMTetViewFields_VTU(dm,nf,&x,fname,"mmssol_");CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dm,nf,&x,fname,"mmssol_");CHKERRQ(ierr);
  }
  
  {
    PetscReal E[Q_NUM_ENTRIES];
    PetscInt order,nel,nbasis;
    
    ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(dm,&nel,0,0,0,&nbasis,0);CHKERRQ(ierr);
    
    ierr = IntegrateQuantities(dm,x,E);CHKERRQ(ierr);
    /*
    PetscPrintf(PETSC_COMM_WORLD,"Error summary:\n");
    PetscPrintf(PETSC_COMM_WORLD,"  %.8D           [elements in domain] \n",nel);
    PetscPrintf(PETSC_COMM_WORLD,"  %.2D                 [basis order] \n",order);
    PetscPrintf(PETSC_COMM_WORLD,"  %.8D           [basis used in domain] \n",nbasis);
    
    PetscPrintf(PETSC_COMM_WORLD,"  %1.12e [h: min ball diameter] \n",E[Q_MIN_BALL_H]);
    PetscPrintf(PETSC_COMM_WORLD,"  %1.12e [h: max ball diameter] \n",E[Q_MAX_BALL_H]);
    PetscPrintf(PETSC_COMM_WORLD,"  %1.12e [vol: domain] \n",E[Q_VOL]);
    PetscPrintf(PETSC_COMM_WORLD,"  %1.12e [L2] \n", E[Q_L2]);
    PetscPrintf(PETSC_COMM_WORLD,"  %1.12e [L2: domain volume normalized] \n", PetscSqrtReal(E[Q_L2]*E[Q_L2]/E[Q_VOL]));
    PetscPrintf(PETSC_COMM_WORLD,"  %1.12e [H1 semi] \n", E[Q_H1]);
    */
    PetscPrintf(PETSC_COMM_WORLD,"%.2D %1.4e  %1.12e %1.12e\n",order,1.0/((PetscReal)mx),E[Q_L2],E[Q_H1]);
  }
  
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscInt n,mxlist[] = { 8, 16, 32, 64, 128 };
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD," P          h                  L2            H1_semi\n");
  PetscPrintf(PETSC_COMM_WORLD,"----------------------------------------------------\n");
  for (n=0; n<sizeof(mxlist)/sizeof(PetscInt); n++) {
    ierr = example3(mxlist[n]);CHKERRQ(ierr);
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

