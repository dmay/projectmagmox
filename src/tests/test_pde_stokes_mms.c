
/*
 
 Variable viscosity Stokes example using the method of manufactured solutions.
 
 The test includes the following features:
 (1) computation of L2, H1 errors for velocity
 (2) computation of L2 error for pressure
 (3) ability to perform simulations over a sequence of meshes
    + use options -mxy 6 -meshes 3 to solve on the following sequence [ 6^2, 12^2, 24^2 ]
 (4) ability to change polynomial order of Stokes element P_{k+1}-P_{k}
    + use option -pk N
 (5) ablity to change the representation of the coefficient for viscosity (eta) and the right hand side (fu)
    + use option pk_eta to define the viscosity via a P_{k} C0 function space in H_1
    + use option pk_fu to define the right hand side via a P_{k} C0 function space in H_1
 (6) ability to use DG-{P0,P1} function spaces for viscosity and right hand side
    + use option -pk_eta_dg -pk_eta 1 and / or -pk_fu_dg -pk_fu 1
 
 The results are as follows:
   Let u be in P_{k+1}; p be in P_{k}
   Optimality of the u,p solution is obtained under the following conditions
 
   [1]  eta \in P_{k} \in H_1 [CG spaces]
         fu \in P_{k} \in H_1 [CG spaces]
 
   [2]  eta \in P_{k} \in L_2 or H_1 [CG or DG spaces]
         fu \in P_{k-1} \in L_2      [DG spaces]
 
*/

#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <pdestokes.h>
#include <dmbcs.h>
#include <mms_stokes_exp_eta.h>
#include <fe_geometry_utils.h>
#include <inorms.h>

typedef struct {
  DM dmP;
  PetscInt pk_eta,pk_fu;
  Vec eta,fu;
  DM dmE,dmF;
} MMSStokes;

/* This function will get moved once I've merged branch dmay/KLWQP1 */
#undef __FUNCT__
#define __FUNCT__ "DMTetVecIterator"
PetscErrorCode DMTetVecIterator(DM dm,Vec x,
                                          PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),
                                          void *ctx)
{
  PetscInt e,k;
  PetscBool istet = PETSC_FALSE;
  PetscInt blocksize;
  PetscScalar *LA_x;
  PetscInt nen,*regionlist,npe,*element;
  PetscReal *coords;
  Vec xl;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"A valid evaluation function must be provided");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for DMTET");
  
  ierr = VecGetBlockSize(x,&blocksize);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = VecGetArray(xl,&LA_x);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,NULL,NULL,&element,&npe,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dm,&nen,&regionlist,NULL);CHKERRQ(ierr);
  
  {
    /* process all dofs at once */
    for (e=0; e<nen; e++) {
      PetscInt *eidx;
      
      eidx = &element[e*npe];
      for (k=0; k<npe; k++) {
        PetscInt d,loc;
        PetscScalar coor_k[3],value[10];
        
        
        coor_k[0] = coords[2*eidx[k]+0];
        coor_k[1] = coords[2*eidx[k]+1];
        coor_k[2] = 0.0;
        
        ierr = eval(coor_k,value,ctx);CHKERRQ(ierr);
        
        for (d=0; d<blocksize; d++) {
          loc = blocksize * eidx[k] + d;
          LA_x[loc] = value[d];
        }
        
      }
    }
  }
  
  ierr = VecRestoreArray(xl,&LA_x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dm,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature"
PetscErrorCode eta_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff,eta;
  PetscReal *coords,*elcoords,**tab_N,*xi;
  PetscInt *element,nen,npe,nbasis;
  DM dm;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  ctx = (MMSStokes*)data;
  dm = ctx->dmP;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * elcoords[2*i+0];
        x_qp[1] += tab_N[q][i] * elcoords[2*i+1];
      }
      ierr = StokesSolution_EvaluateViscosity(x_qp,&eta,NULL);CHKERRQ(ierr);
      coeff[e*nqp + q] = eta;
    }
  }
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature_i"
PetscErrorCode eta_quadrature_i(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp,degree;
  PetscReal *coeff,eta_qp;
  PetscReal *coords,*el_coords,*el_eta,**tab_N,*xi;
  PetscInt *element,nen,npe,nbasis;
  DM dm;
  Vec eta;
  const PetscScalar *LA_eta;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  ctx = (MMSStokes*)data;
  dm = ctx->dmE;

  ierr = DMTetGetBasisOrder(dm,&degree);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: eta [CG-P%D]\n",degree);

  eta = ctx->eta;
  ierr = VecGetArrayRead(eta,&LA_eta);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&el_coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&el_eta);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and eta */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      el_coords[2*i+0] = coords[2*nidx+0];
      el_coords[2*i+1] = coords[2*nidx+1];

      el_eta[i] = LA_eta[nidx];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * el_coords[2*i+0];
        x_qp[1] += tab_N[q][i] * el_coords[2*i+1];
      }

      /* get interpolated viscosity at point */
      eta_qp = 0.0;
      for (i=0; i<nbasis; i++) {
        eta_qp += tab_N[q][i] * el_eta[i];
      }
      
      coeff[e*nqp + q] = eta_qp;
    }
  }
  ierr = PetscFree(el_eta);CHKERRQ(ierr);
  ierr = PetscFree(el_coords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(eta,&LA_eta);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature_p"
PetscErrorCode eta_quadrature_p(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt degree;
  DM dm;
  Vec eta;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  ctx = (MMSStokes*)data;
  dm = ctx->dmE;
  
  ierr = DMTetGetBasisOrder(dm,&degree);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: eta [project CG-P%D]\n",degree);
  
  /* evaluate fu on the quadrature points */
  ierr = eta_quadrature(c,quadrature,eregion,(void*)ctx);CHKERRQ(ierr);
  
  /* project quadrature values onto DMTet */
  eta = ctx->eta;
  ierr = DMTetProjectQuadratureField(quadrature,"eta",dm,&eta);CHKERRQ(ierr);
  
  /* interpolate projected field */
  ierr = eta_quadrature_i(c,quadrature,eregion,(void*)ctx);CHKERRQ(ierr);

  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature_dg0"
PetscErrorCode eta_quadrature_dg0(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff,eta;
  PetscInt nen_g,*element_g;
  PetscReal *coords_g,elcoords_vert[2*3];
  PetscReal *coords,*elcoords,**tab_N,*xi,*w,dJ;
  PetscInt *element,nen,npe,nbasis;
  DM dm;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: eta [DG-P0]\n");
  ctx = (MMSStokes*)data;
  dm = ctx->dmP;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,&nen_g,0,&element_g,0,0,&coords_g);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    PetscReal eta_avg,el_vol;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    {
      PetscInt *gidx = &element_g[3*e];
      
      for (i=0; i<3; i++) {
        PetscInt nidx = gidx[i];
        
        elcoords_vert[2*i+0] = coords_g[2*nidx+0];
        elcoords_vert[2*i+1] = coords_g[2*nidx+1];
      }
    }
    
    eta_avg = 0.0;
    el_vol = 0.0;
    EvaluateBasisGeometry_Affine(elcoords_vert,&dJ);
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * elcoords[2*i+0];
        x_qp[1] += tab_N[q][i] * elcoords[2*i+1];
      }
      ierr = StokesSolution_EvaluateViscosity(x_qp,&eta,NULL);CHKERRQ(ierr);
      
      el_vol += w[q] * 1.0 * dJ;
      eta_avg += w[q] * eta * dJ;
    }
    
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = eta_avg/el_vol;
    }
  }
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature_dg"
PetscErrorCode eta_quadrature_dg(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,j,q,e;
  PetscReal *coords,*el_coords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,eta_qp;
  PetscInt nqp;
  PetscReal *xi,*w;
  MMSStokes *ctx;
  PetscReal Me[3][3],iMe[3][3],P[3];
  
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: eta [DG]\n");
  ctx = (MMSStokes*)data;
  dm = ctx->dmE;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&el_coords);CHKERRQ(ierr);
  
  /* assemble element mass matrix */
  for (i=0; i<nbasis; i++) {
    for (j=0; j<nbasis; j++) {
      Me[i][j] = 0.0;
      for (q=0; q<nqp; q++) {
        Me[i][j] += w[q] * tab_N[q][i] * tab_N[q][j];
      }
    }
  }
  
  {
    PetscReal t4, t6, t8, t10, t12, t14, t17;
    
    t4  = Me[2][0] * Me[0][1];
    t6  = Me[2][0] * Me[0][2];
    t8  = Me[1][0] * Me[0][1];
    t10 = Me[1][0] * Me[0][2];
    t12 = Me[0][0] * Me[1][1];
    t14 = Me[0][0] * Me[1][2];
    t17 = 0.1e1 / (t4 * Me[1][2] - t6 * Me[1][1] - t8 * Me[2][2] + t10 * Me[2][1] + t12 * Me[2][2] - t14 * Me[2][1]);
    
    iMe[0][0] = (Me[1][1] * Me[2][2] - Me[1][2] * Me[2][1]) * t17;
    iMe[0][1] = -(Me[0][1] * Me[2][2] - Me[0][2] * Me[2][1]) * t17;
    iMe[0][2] = (Me[0][1] * Me[1][2] - Me[0][2] * Me[1][1]) * t17;
    iMe[1][0] = -(-Me[2][0] * Me[1][2] + Me[1][0] * Me[2][2]) * t17;
    iMe[1][1] = (-t6 + Me[0][0] * Me[2][2]) * t17;
    iMe[1][2] = -(-t10 + t14) * t17;
    iMe[2][0] = (-Me[2][0] * Me[1][1] + Me[1][0] * Me[2][1]) * t17;
    iMe[2][1] = -(-t4 + Me[0][0] * Me[2][1]) * t17;
    iMe[2][2] = (-t8 + t12) * t17;
  }
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    PetscReal eta_proj[3];
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and fu */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      el_coords[2*i+0] = coords[2*nidx+0];
      el_coords[2*i+1] = coords[2*nidx+1];
    }
    
    ierr = PetscMemzero(P,sizeof(PetscReal)*3);CHKERRQ(ierr);
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * el_coords[2*i+0];
        x_qp[1] += tab_N[q][i] * el_coords[2*i+1];
      }
      
      /* get viscosity at point */
      ierr = StokesSolution_EvaluateViscosity(x_qp,&eta_qp,NULL);CHKERRQ(ierr);
      
      for (i=0; i<nbasis; i++) {
        P[i] += w[q] * tab_N[q][i] * eta_qp;
      }
    }
    
    ierr = PetscMemzero(eta_proj,sizeof(PetscReal)*3);CHKERRQ(ierr);
    for (i=0; i<nbasis; i++) {
      for (j=0; j<nbasis; j++) {
        eta_proj[i] += iMe[i][j] * P[j];
      }
    }
    
    for (q=0; q<nqp; q++) {
      eta_qp = 0.0;
      for (j=0; j<nbasis; j++) {
        eta_qp += tab_N[q][j] * eta_proj[j];
      }
      coeff[e*nqp + q] = eta_qp;
    }
  }
  
  ierr = PetscFree(el_coords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fp_quadrature"
PetscErrorCode fp_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp;
  PetscReal *coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature"
PetscErrorCode fu_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,Fu[2];
  PetscInt nqp;
  PetscReal *xi,*w;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  ctx = (MMSStokes*)data;
  dm = ctx->dmP;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * elcoords[2*i+0];
        x_qp[1] += tab_N[q][i] * elcoords[2*i+1];
      }
      ierr = StokesSolution_EvaluateFu(x_qp,Fu,NULL);CHKERRQ(ierr);
      
      coeff[2*(e*nqp + q)+0] = Fu[0];
      coeff[2*(e*nqp + q)+1] = Fu[1];
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature_i"
PetscErrorCode fu_quadrature_i(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*el_coords,*el_fu,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,Fu_qp[2];
  PetscInt nqp,degree;
  PetscReal *xi;
  Vec fu;
  const PetscScalar *LA_fu;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  ctx = (MMSStokes*)data;
  dm = ctx->dmF;
  
  ierr = DMTetGetBasisOrder(dm,&degree);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: Fu [CG-P%D]\n",degree);
  
  fu = ctx->fu;
  ierr = VecGetArrayRead(fu,&LA_fu);CHKERRQ(ierr);

  ierr = QuadratureGetRule(quadrature,&nqp,&xi,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&el_coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&el_fu);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and fu */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      el_coords[2*i+0] = coords[2*nidx+0];
      el_coords[2*i+1] = coords[2*nidx+1];

      el_fu[2*i+0] = LA_fu[2*nidx+0];
      el_fu[2*i+1] = LA_fu[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * el_coords[2*i+0];
        x_qp[1] += tab_N[q][i] * el_coords[2*i+1];
      }

      /* get interpolated Fu at point */
      Fu_qp[0] = Fu_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        Fu_qp[0] += tab_N[q][i] * el_fu[2*i+0];
        Fu_qp[1] += tab_N[q][i] * el_fu[2*i+1];
      }

      coeff[2*(e*nqp + q)+0] = Fu_qp[0];
      coeff[2*(e*nqp + q)+1] = Fu_qp[1];
    }
  }
  
  ierr = PetscFree(el_fu);CHKERRQ(ierr);
  ierr = PetscFree(el_coords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(fu,&LA_fu);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature_p"
PetscErrorCode fu_quadrature_p(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt degree;
  MMSStokes *ctx;
  Vec fu;
  
  PetscFunctionBegin;
  ctx = (MMSStokes*)data;
  dm = ctx->dmF;
  
  ierr = DMTetGetBasisOrder(dm,&degree);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: Fu [project CG-P%D]\n",degree);
  
  /* evaluate fu on the quadrature points */
  ierr = fu_quadrature(c,quadrature,eregion,(void*)ctx);CHKERRQ(ierr);

  /* project quadrature values onto DMTet */
  fu = ctx->fu;
  ierr = DMTetProjectQuadratureField(quadrature,"fu",dm,&fu);CHKERRQ(ierr);
  
  /* interpolate projected field */
  ierr = fu_quadrature_i(c,quadrature,eregion,(void*)ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature_dg0"
PetscErrorCode fu_quadrature_dg0(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords_g,elcoords_vert[2*3];
  PetscInt nen_g,*element_g;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,Fu[2],dJ;
  PetscInt nqp;
  PetscReal *xi,*w;
  MMSStokes *ctx;
  
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: Fu [DG-P0]\n");
  ctx = (MMSStokes*)data;
  dm = ctx->dmP;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,&nen_g,0,&element_g,0,0,&coords_g);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    PetscReal el_vol,Fu_avg[2];
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    {
      PetscInt *gidx = &element_g[3*e];
      
      for (i=0; i<3; i++) {
        PetscInt nidx = gidx[i];
        
        elcoords_vert[2*i+0] = coords_g[2*nidx+0];
        elcoords_vert[2*i+1] = coords_g[2*nidx+1];
      }
    }
    
    Fu_avg[0] = Fu_avg[1] = 0.0;
    el_vol = 0.0;
    EvaluateBasisGeometry_Affine(elcoords_vert,&dJ);
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * elcoords[2*i+0];
        x_qp[1] += tab_N[q][i] * elcoords[2*i+1];
      }
      ierr = StokesSolution_EvaluateFu(x_qp,Fu,NULL);CHKERRQ(ierr);
      
      el_vol += w[q] * 1.0 * PetscAbsReal(dJ);
      Fu_avg[0] += w[q] * Fu[0] * PetscAbsReal(dJ);
      Fu_avg[1] += w[q] * Fu[1] * PetscAbsReal(dJ);
    }
    
    for (q=0; q<nqp; q++) {
      coeff[2*(e*nqp + q)+0] = Fu_avg[0]/el_vol;
      coeff[2*(e*nqp + q)+1] = Fu_avg[1]/el_vol;
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature_dg"
PetscErrorCode fu_quadrature_dg(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,j,q,e;
  PetscReal *coords,*el_coords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,Fu_qp[2];
  PetscInt nqp;
  PetscReal *xi,*w;
  MMSStokes *ctx;
  PetscReal Me[3][3],iMe[3][3],Pu[3],Pv[3];
  
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD,"#  Coefficient: Fu [DG]\n");
  ctx = (MMSStokes*)data;
  dm = ctx->dmF;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&el_coords);CHKERRQ(ierr);

  /* assemble element mass matrix */
  for (i=0; i<nbasis; i++) {
    for (j=0; j<nbasis; j++) {
      Me[i][j] = 0.0;
      for (q=0; q<nqp; q++) {
        Me[i][j] += w[q] * tab_N[q][i] * tab_N[q][j];
      }
    }
  }
  
  {
    PetscReal t4, t6, t8, t10, t12, t14, t17;

    t4  = Me[2][0] * Me[0][1];
    t6  = Me[2][0] * Me[0][2];
    t8  = Me[1][0] * Me[0][1];
    t10 = Me[1][0] * Me[0][2];
    t12 = Me[0][0] * Me[1][1];
    t14 = Me[0][0] * Me[1][2];
    t17 = 0.1e1 / (t4 * Me[1][2] - t6 * Me[1][1] - t8 * Me[2][2] + t10 * Me[2][1] + t12 * Me[2][2] - t14 * Me[2][1]);
    
    iMe[0][0] = (Me[1][1] * Me[2][2] - Me[1][2] * Me[2][1]) * t17;
    iMe[0][1] = -(Me[0][1] * Me[2][2] - Me[0][2] * Me[2][1]) * t17;
    iMe[0][2] = (Me[0][1] * Me[1][2] - Me[0][2] * Me[1][1]) * t17;
    iMe[1][0] = -(-Me[2][0] * Me[1][2] + Me[1][0] * Me[2][2]) * t17;
    iMe[1][1] = (-t6 + Me[0][0] * Me[2][2]) * t17;
    iMe[1][2] = -(-t10 + t14) * t17;
    iMe[2][0] = (-Me[2][0] * Me[1][1] + Me[1][0] * Me[2][1]) * t17;
    iMe[2][1] = -(-t4 + Me[0][0] * Me[2][1]) * t17;
    iMe[2][2] = (-t8 + t12) * t17;
  }
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    PetscReal fu_proj[3],fv_proj[3];
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and fu */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      el_coords[2*i+0] = coords[2*nidx+0];
      el_coords[2*i+1] = coords[2*nidx+1];
    }
    
    ierr = PetscMemzero(Pu,sizeof(PetscReal)*3);CHKERRQ(ierr);
    ierr = PetscMemzero(Pv,sizeof(PetscReal)*3);CHKERRQ(ierr);
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * el_coords[2*i+0];
        x_qp[1] += tab_N[q][i] * el_coords[2*i+1];
      }
      
      /* get Fu at point */
      ierr = StokesSolution_EvaluateFu(x_qp,Fu_qp,NULL);CHKERRQ(ierr);
      
      for (i=0; i<nbasis; i++) {
        Pu[i] += w[q] * tab_N[q][i] * Fu_qp[0];
        Pv[i] += w[q] * tab_N[q][i] * Fu_qp[1];
      }
    }
    
    ierr = PetscMemzero(fu_proj,sizeof(PetscReal)*3);CHKERRQ(ierr);
    ierr = PetscMemzero(fv_proj,sizeof(PetscReal)*3);CHKERRQ(ierr);
    for (i=0; i<nbasis; i++) {
      for (j=0; j<nbasis; j++) {
        fu_proj[i] += iMe[i][j] * Pu[j];
        fv_proj[i] += iMe[i][j] * Pv[j];
      }
    }

    for (q=0; q<nqp; q++) {
      Fu_qp[0] = Fu_qp[1] = 0.0;
      for (j=0; j<nbasis; j++) {
        Fu_qp[0] += tab_N[q][j] * fu_proj[j];
        Fu_qp[1] += tab_N[q][j] * fv_proj[j];
      }
      coeff[2*(e*nqp + q)+0] = Fu_qp[0];
      coeff[2*(e*nqp + q)+1] = Fu_qp[1];
    }
  }
  
  ierr = PetscFree(el_coords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_UseMMS"
PetscErrorCode EvalDirichletBoundary_UseMMS(PetscScalar coor[],PetscInt dof,
                                            PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscErrorCode ierr;
  PetscReal velocity[2];
  const PetscReal coor_eps = 1.0e-10;
  
  *constrain = PETSC_FALSE;
  *val = 0.0;
  
  ierr = StokesSolution_EvaluateV(coor,velocity,NULL);CHKERRQ(ierr);
  
  if ( PetscAbsReal(coor[0]) < coor_eps) { // left
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[0] - 1.0) < coor_eps) { // right
    *constrain = PETSC_TRUE;
  }

  if ( PetscAbsReal(coor[1]) < coor_eps) { // bottom
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[1] - 1.0) < coor_eps) { // top
    *constrain = PETSC_TRUE;
  }

  if (*constrain) {
    //printf("%1.4e %1.4e %1.4e %1.4e\n",coor[0],coor[1],velocity[0],velocity[1]);
    *val = velocity[dof];
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pde_stokes_mms"
PetscErrorCode pde_stokes_mms(PetscBool use_SV,PetscInt mxy,PetscReal *h,PetscReal *uL2,PetscReal *uH1,PetscReal *pL2)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcV;
  SNES snes;
  PetscBool view = PETSC_FALSE;
  PetscInt pk,pk_eta,pk_fu;
  PetscBool pk_eta_dg,pk_fu_dg,eta_project,fu_project;
  MMSStokes ctx;
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Stokes mesh %.3D x %.3D : element: P%D-P%D\n",mxy,mxy,pk+1,pk);
  if (!use_SV) {
    ierr = DMTetCreatePartitionedSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pk+1,&dmV);CHKERRQ(ierr);

    ierr = DMTetCreateSharedGeometry(dmV,1,DMTET_CWARP_AND_BLEND,pk,&dmP);CHKERRQ(ierr);
  } else {
    DM dm;
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pk+1,&dm);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dm,1,&dmV);CHKERRQ(ierr);
    ierr = DMDestroy(&dm);CHKERRQ(ierr);
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_DG,pk,&dm);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dm,1,&dmP);CHKERRQ(ierr);
    ierr = DMDestroy(&dm);CHKERRQ(ierr);
  }
  ctx.dmP = dmV;
  
  eta_project = PETSC_FALSE;
  pk_eta_dg = PETSC_FALSE;
  pk_eta = -1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk_eta",&pk_eta,NULL);CHKERRQ(ierr);
  ctx.pk_eta = pk_eta;
  ctx.dmE = NULL;
  ctx.eta = NULL;
  if (pk_eta >= 0) {
    const char *field[] = { "eta" };

    ierr = PetscOptionsGetBool(NULL,NULL,"-eta_project",&eta_project,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetBool(NULL,NULL,"-pk_eta_dg",&pk_eta_dg,NULL);CHKERRQ(ierr);
    if (pk_eta_dg) {
      PetscPrintf(PETSC_COMM_WORLD,"# Stokes: Using interpolated eta: P%D [DG]\n",pk_eta);
      if (pk_eta > 1) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only DG-P1 supported");
    } else {
      PetscPrintf(PETSC_COMM_WORLD,"# Stokes: Using interpolated eta: P%D [CG]\n",pk_eta);
      if (pk_eta == 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"CG space cannot be P0");
    }
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,PetscMax(1,pk_eta),&ctx.dmE);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(ctx.dmE,&ctx.eta);CHKERRQ(ierr);
    
    ierr = DMTetVecIterator(ctx.dmE,ctx.eta,StokesSolution_EvaluateViscosity,NULL);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx.dmE,1,&ctx.eta,field,"eta_");CHKERRQ(ierr);
  }
  
  fu_project = PETSC_FALSE;
  pk_fu_dg = PETSC_FALSE;
  pk_fu = -1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk_fu",&pk_fu,NULL);CHKERRQ(ierr);
  ctx.pk_fu = pk_fu;
  ctx.dmF = NULL;
  ctx.fu = NULL;
  if (pk_fu >= 0) {
    const char *field[] = { "fu" };

    ierr = PetscOptionsGetBool(NULL,NULL,"-fu_project",&fu_project,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetBool(NULL,NULL,"-pk_fu_dg",&pk_fu_dg,NULL);CHKERRQ(ierr);
    if (pk_fu_dg) {
      PetscPrintf(PETSC_COMM_WORLD,"# Stokes: Using interpolated fu: P%D [DG]\n",pk_fu);
      if (pk_fu > 1) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only DG-P1 supported");
    } else {
      PetscPrintf(PETSC_COMM_WORLD,"# Stokes: Using interpolated fu: P%D [CG]\n",pk_fu);
      if (pk_fu == 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"CG space cannot be P0");
    }
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,PetscMax(1,pk_fu),&ctx.dmF);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(ctx.dmF,&ctx.fu);CHKERRQ(ierr);

    ierr = DMTetVecIterator(ctx.dmF,ctx.fu,StokesSolution_EvaluateFu,NULL);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx.dmF,1,&ctx.fu,field,"fu_");CHKERRQ(ierr);
  }

  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);

  ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_UseMMS,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_UseMMS,NULL);CHKERRQ(ierr);

  /* pde for stokes */
  ierr = PDECreateStokes(PETSC_COMM_WORLD,dmV,dmP,bcV,&pde);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde,"stk_");CHKERRQ(ierr);

  ierr = PDEStokesGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);

  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  {
    Coefficient e,fu,fp;
    
    ierr = PDEStokesGetCoefficients(pde,&e,&fu,&fp);CHKERRQ(ierr);
    
    /* viscosity */
    if (pk_eta >= 0) {
      if (!pk_eta_dg) {
        if (eta_project) {
          ierr = CoefficientSetComputeQuadrature(e,eta_quadrature_p,NULL,(void*)&ctx);CHKERRQ(ierr);
        } else {
          ierr = CoefficientSetComputeQuadrature(e,eta_quadrature_i,NULL,(void*)&ctx);CHKERRQ(ierr);
        }
      } else {
        if (ctx.pk_eta == 0) {
          ierr = CoefficientSetComputeQuadrature(e,eta_quadrature_dg0,NULL,(void*)&ctx);CHKERRQ(ierr);
        } else {
          ierr = CoefficientSetComputeQuadrature(e,eta_quadrature_dg,NULL,(void*)&ctx);CHKERRQ(ierr);
        }
      }
    } else {
      ierr = CoefficientSetComputeQuadrature(e,eta_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    }

    if (view) {
      ierr = CoefficientEvaluate(e);CHKERRQ(ierr);
      ierr = CoefficientView_GP(e,dmP,"eta-qp");CHKERRQ(ierr);
    }

    /* uv-momentum right hand side */
    if (pk_fu >= 0) {
      if (!pk_fu_dg) {
        if (fu_project) {
          ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature_p,NULL,(void*)&ctx);CHKERRQ(ierr);
        } else {
          ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature_i,NULL,(void*)&ctx);CHKERRQ(ierr);
        }
      } else {
        if (ctx.pk_fu == 0) {
          ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature_dg0,NULL,(void*)&ctx);CHKERRQ(ierr);
        } else {
          ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature_dg,NULL,(void*)&ctx);CHKERRQ(ierr);
        }
      }
    } else {
      ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    }
    
    if (view) {
      ierr = CoefficientEvaluate(fu);CHKERRQ(ierr);
      ierr = CoefficientView_GP(fu,dmP,"fu-qp");CHKERRQ(ierr);
    }

    // force something - hack //
    /*
    {
      Quadrature qq;
      ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
      ierr = CoefficientEvaluate(fu);CHKERRQ(ierr);
      
      ierr = CoefficientGetQuadrature(fu,&qq);CHKERRQ(ierr);
      ierr = DMTetProjectQuadratureField(qq,"fu",ctx.dmF,&ctx.fu);CHKERRQ(ierr);

      ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature_i,NULL,(void*)&ctx);CHKERRQ(ierr);
      ierr = CoefficientEvaluate(fu);CHKERRQ(ierr);
      ierr = CoefficientView_GP(fu,dmP,"fu-qp");CHKERRQ(ierr);
    }
    */
    
    /* p-continuity right hand side */
    ierr = CoefficientSetComputeQuadrature(fp,fp_quadrature,NULL,NULL);CHKERRQ(ierr);
  }
  
  /* stokes solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);

  {
    Vec Xv,Xp;
    PetscReal Eu[Q_NUM_ENTRIES];
    PetscReal Ep[Q_NUM_ENTRIES],p_avg;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Eu,dmV,Xv,
                         StokesSolution_EvaluateV,
                         StokesSolution_EvaluateGradientV,NULL);CHKERRQ(ierr);

    ierr = IntegrateField(&p_avg,dmP,Xp);CHKERRQ(ierr);
    //printf("p_avg = %1.4e\n",p_avg);
    
    ierr = VecShift(Xp,-p_avg);CHKERRQ(ierr);
    //ierr = IntegrateField(&p_avg,dmP,Xp);CHKERRQ(ierr);
    //printf("p_avg* = %1.4e\n",p_avg);
    
    ierr = ComputeINorms(Ep,dmP,Xp,
                         StokesSolution_EvaluateP,
                         NULL,NULL);CHKERRQ(ierr);

    {
      PetscReal Enorms[5];
      
      ierr = IntegrateFieldHdiv(Enorms,dmV,Xv);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"# degree  %.2D %1.4e | hdiv %1.12e | hdiv_Linf %1.12e\n",pk,1.0/((PetscReal)mxy),Enorms[3],Enorms[4]);
      
    }
    
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    //PetscPrintf(PETSC_COMM_WORLD,"degree  %.2D %1.4e  %1.12e %1.12e %1.12e\n",pk,1.0/((PetscReal)mxy),Eu[Q_L2],Eu[Q_H1],Ep[Q_L2]);

    *h = 1.0/((PetscReal)mxy);
    *uL2 = Eu[Q_L2];
    *uH1 = Eu[Q_H1];
    *pL2 = Ep[Q_L2];
  }
  
  if (view) {
    ierr = PDEStokesFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
  }
  
  if (ctx.dmF) { ierr = DMDestroy(&ctx.dmF);CHKERRQ(ierr); }
  if (ctx.dmE) { ierr = DMDestroy(&ctx.dmE);CHKERRQ(ierr); }
  if (ctx.eta) { ierr = VecDestroy(&ctx.eta);CHKERRQ(ierr); }
  if (ctx.fu) { ierr = VecDestroy(&ctx.fu);CHKERRQ(ierr); }
  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 
 ./${PETSC_ARCH}/test_pde_stokes_mms.app -ignore_preamble -stk_fieldsplit_u_pc_type lu -stk_fieldsplit_u_pc_factor_mat_solver_package umfpack -stk_ksp_type fgmres -stk_pc_fieldsplit_schur_fact_type UPPER -stk_fieldsplit_p_ksp_type preonly -stk_fieldsplit_p_pc_type lu -stk_fieldsplit_p_pc_factor_mat_solver_package umfpack -stk_ksp_rtol 1.0e-10 -mxy 16
*/
int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscInt mxy,L,k,m;
  PetscReal *res_h,*u_L2,*u_H1,*p_L2;
  PetscBool use_SV = PETSC_FALSE;
  PetscBool harness_viewer = PETSC_FALSE;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,NULL,"-use_sv",&use_SV,NULL);CHKERRQ(ierr);

  mxy = 6;
  ierr = PetscOptionsGetInt(NULL,NULL,"-mxy",&mxy,NULL);CHKERRQ(ierr);

  L = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-meshes",&L,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&res_h);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&u_L2);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&u_H1);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&p_L2);CHKERRQ(ierr);
  
  m = mxy;
  for (k=0; k<L; k++) {
    ierr = pde_stokes_mms(use_SV,m,&res_h[k],&u_L2[k],&u_H1[k],&p_L2[k]);CHKERRQ(ierr);
    m = m * 2;
  }

  ierr = PetscOptionsGetBool(NULL,NULL,"-harness_viewer",&harness_viewer,NULL);CHKERRQ(ierr);
  if (!harness_viewer) {
    PetscPrintf(PETSC_COMM_WORLD,"      level          h              u(L2)              u(H1)              p(L2)            rates\n");
    PetscPrintf(PETSC_COMM_WORLD,"------------------------------------------------------------------------------- | -----------------------------------------------------------\n");
    for (k=0; k<L; k++) {
      PetscPrintf(PETSC_COMM_WORLD,"[mesh %.4D] %1.4e %1.12e %1.12e %1.12e |",k,res_h[k],u_L2[k],u_H1[k],p_L2[k]);
      if (k > 0) {
        PetscReal ruL2,ruH1,rpL2;
        
        ruL2 = log10(u_L2[k]/u_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
        ruH1 = log10(u_H1[k]/u_H1[k-1]) / log10(res_h[k]/res_h[k-1]);
        rpL2 = log10(p_L2[k]/p_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
        
        PetscPrintf(PETSC_COMM_WORLD," %+1.12e %+1.12e %+1.12e",ruL2,ruH1,rpL2);
      }
      PetscPrintf(PETSC_COMM_WORLD,"\n");
    }
  } else {
    for (k=0; k<L; k++) {
      PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-uL2 %1.12e\n",k,u_L2[k]);
      PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-uH1 %1.12e\n",k,u_H1[k]);
      PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-pL2 %1.12e\n",k,p_L2[k]);
      if (k > 0) {
        PetscReal ruL2,ruH1,rpL2;
        
        ruL2 = log10(u_L2[k]/u_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
        ruH1 = log10(u_H1[k]/u_H1[k-1]) / log10(res_h[k]/res_h[k-1]);
        rpL2 = log10(p_L2[k]/p_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
        PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-rate-uL2 %1.12e\n",k,ruL2);
        PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-rate-uH1 %1.12e\n",k,ruH1);
        PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-rate-pL2 %1.12e\n",k,rpL2);
      }
    }
  }

  ierr = PetscFree(res_h);CHKERRQ(ierr);
  ierr = PetscFree(u_L2);CHKERRQ(ierr);
  ierr = PetscFree(u_H1);CHKERRQ(ierr);
  ierr = PetscFree(p_L2);CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return(0);
}


