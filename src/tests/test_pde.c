
#include <petsc.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>

PetscErrorCode DMTetViewBasisFunctions2d_CWARPANDBLEND(DM dm);

#undef __FUNCT__
#define __FUNCT__ "test1_constructdestroy"
PetscErrorCode test1_constructdestroy(void)
{
  PetscErrorCode ierr;
  DM dm;
  PDE pde;
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);

  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDEView(pde,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test2_snes"
PetscErrorCode test2_snes(void)
{
  PetscErrorCode ierr;
  DM dm;
  PDE pde;
  SNES snes;
  Mat A;
  Vec X,Xlocal,F;
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dm,NULL);CHKERRQ(ierr);
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,A,A);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FindBoundary"
PetscBool FindBoundary(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[0] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[0] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution"
PetscReal mms_evaluate_solution(PetscReal x,PetscReal y)
{
  PetscReal u;
  u =  cos(M_PI*y)*sin(M_PI*x);
  return u;
}

#undef __FUNCT__
#define __FUNCT__ "__mms_evaluate_rhs"
PetscReal __mms_evaluate_rhs(PetscReal x,PetscReal y)
{
  PetscReal f;
  f =  cos(M_PI*y)*sin(M_PI*x) + 2*pow(M_PI,2)*cos(M_PI*y)*sin(M_PI*x);
  return f;
}

#undef __FUNCT__
#define __FUNCT__ "mms_rhs_quadrature"
PetscErrorCode mms_rhs_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  dm = (DM)data;
 
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);

  ierr = PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      //function[q] = mms_evaluate_rhs(xq,yq);
      coeff[e*nqp + q] = __mms_evaluate_rhs(xq,yq);
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary"
PetscErrorCode EvalDirichletBoundary(PetscScalar coor[],PetscScalar *val,void *ctx)
{
  *val = mms_evaluate_solution(coor[0],coor[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test3_snessolve"
PetscErrorCode test3_snessolve(void)
{
  PetscErrorCode ierr;
  DM dm;
  PDE pde;
  SNES snes;
  Mat A;
  Vec X,Xlocal,F;
  BCList bc;
  
  //ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-1.0,1.0,-1.0,1.0, 8,8, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundary,NULL,&bc);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dm,bc);CHKERRQ(ierr);
  
  
  {
    Coefficient g;
    
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(g,mms_rhs_quadrature,NULL,(void*)dm);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,A,A);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);

  //ierr = PDEView(pde,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt tidx = 3;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(NULL,NULL,"-tidx",&tidx,NULL);CHKERRQ(ierr);
  switch (tidx) {
    case 1:
      ierr = test1_constructdestroy();CHKERRQ(ierr);
      break;
    case 2:
      ierr = test2_snes();CHKERRQ(ierr);
      break;
    case 3:
      ierr = test3_snessolve();CHKERRQ(ierr);
      break;
      
    default:
      break;
  }
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

