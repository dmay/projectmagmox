
#include <petsc.h>
#include <petscdmtet.h>
#include <dmtet_topology.h>
#include <dmtet_bfacet.h>
#include <dmtetimpl.h>
#include <proj_init_finalize.h>
#include <quadrature.h>
#include <element_container.h>
#include <fe_geometry_utils.h>



PetscErrorCode DMTetViewBasisFunctions2d_CWARPANDBLEND(DM);
PetscErrorCode DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(PetscInt,PetscInt*,PetscReal**);

/*
 Testing of ordering, topology
*/
#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  PetscErrorCode ierr;
  DM dm;
  
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/triangle-domain/tri.1",1,DMTET_CWARP_AND_BLEND,3,&dm);CHKERRQ(ierr);

  //ierr = DMTetGeometryView2d_VTK(dm,"tri-domain-g.vtu");
  //ierr = DMTetBasisView2d_Continuous_VTK(dm,"tri-domain-b.vtu");CHKERRQ(ierr);
  
  ierr = DMTetTopologySetup(dm,1);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dm);CHKERRQ(ierr);
  
  /* output faces */
#if 0
  {
    DM_TET *tet;
    DMTetMesh *geometry;
    BFacetList f;
    PetscInt bf;
    
    tet = (DM_TET*)dm->data;
    geometry = tet->geometry;
    f = geometry->boundary_facets;
    
    for (bf=0; bf<f->nfacets; bf++) {
      PetscInt *eln;
      PetscInt eidx,fidx;
      
      eidx = f->facet_to_element[bf];
      fidx = f->facet_element_face_id[bf];
      eln = &geometry->element[3*eidx];
      
      if (fidx == 0) {
        printf("facet %d : type 0 : verts %d %d  local[0,1]\n",fidx,eln[0],eln[1]);
      }
      if (fidx == 1) {
        printf("facet %d : type 1 : verts %d %d  local[1,2] el[%d %d %d]\n",fidx,eln[1],eln[2],eln[0],eln[1],eln[2]);
      }
    }
  }
#endif
  
  PetscInt i,j,order,nij,npe;
  PetscReal *xilocal;
  order = 7;

  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&npe,&xilocal);CHKERRQ(ierr);
  

  PetscInt fccnt[]={0,0,0},fcid0[8];
  PetscInt fcid1[8];
  PetscInt fcid2[8];
  nij = 0;
  for (j=0; j<=order; j++) {
    for (i=0; i<=order; i++) {
      if (i+j > order) { continue; }
      //printf("i %d j %d -> nij [%+1.6e %+1.6e]\n",i,j,xilocal[2*nij+0],xilocal[2*nij+1]);
      printf("%+1.6e %+1.6e %lf\n",xilocal[2*nij+0],xilocal[2*nij+1],(double)nij);
      
      if (j==0) {
        fcid0[fccnt[0]] = nij;
        fccnt[0]++;
      }

      if (i==0) {
        fcid2[fccnt[2]] = nij;
        fccnt[2]++;
      }

      if (i==(order-j)) {
        fcid1[fccnt[1]] = nij;
        fccnt[1]++;
      }

      nij++;
    }
  }

  // reverse order of fcid2
  for (i=0; i<=order/2; i++) {
    PetscInt tmp = fcid2[i];
    fcid2[i] = fcid2[order-i];
    fcid2[order-i] = tmp;
  }
  
  nij = 0;
  j = 0;
  {
    for (i=0; i<=order; i++) {
      //printf("i %d j %d -> nij [%+1.6e %+1.6e]\n",i,j,xilocal[2*nij+0],xilocal[2*nij+1]);
      printf("Face0: %+1.6e %+1.6e %lf\n",xilocal[2*nij+0],xilocal[2*nij+1],(double)nij);
      nij++;
    }
  }
  printf("\n");
  for (i=0; i<=order; i++) {
    printf("Face0: %+1.6e %+1.6e %lf\n",xilocal[2*fcid0[i]+0],xilocal[2*fcid0[i]+1],(double)fcid0[i]);
  }
  printf("\n");
  for (i=0; i<=order; i++) {
    printf("Face1: %+1.6e %+1.6e %lf\n",xilocal[2*fcid1[i]+0],xilocal[2*fcid1[i]+1],(double)fcid1[i]);
  }
  printf("\n");
  for (i=0; i<=order; i++) {
    printf("Face2: %+1.6e %+1.6e %lf\n",xilocal[2*fcid2[i]+0],xilocal[2*fcid2[i]+1],(double)fcid2[i]);
  }

  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/*
 Testing surface integration
 Uses divergence theorem
 
 \int_\Omega div(F) dV = \int_{\partial \Omega} F . n dS
 
 where :
 
 [test 0]
 F = 0.5 (x, y)
 divF = 1.0

 With this choice of F, the integral on the LHS, is \int_\Omega 1 dV = |\Omega|

 [test 1]
 F = (0.25*x*y, 0.1*y*y)
 div(F) = 0.25*y + 0.2*y
 
*/

void evaluate_analytic_function(PetscInt fid,PetscReal x[],PetscReal divF[],PetscReal F[])
{
  switch (fid) {
    case 0:
      if (F) {
        F[0] = 0.5*x[0];
        F[1] = 0.5*x[1];
      }
      if (divF) { divF[0] = 1.0; }
      break;

    case 1:
      if (F) {
        F[0] = 0.25*x[0]*x[1];
        F[1] = 0.1*x[1]*x[1];
      }
      if (divF) {
        divF[0] = 0.25*x[1] + 0.2*x[1];
      }
      break;
      
    default:
      break;
  }
}

#undef __FUNCT__
#define __FUNCT__ "example2"
PetscErrorCode example2(void)
{
  PetscErrorCode ierr;
  DM dm;
  Quadrature q_vol,q_surf;
  PetscInt nqp_vol,nqp_s;
  PetscReal *xi_vol,*xi_s,*w_vol,*w_s;
  PetscReal int_vol,int_vol_g,int_vol_e,dJv,*_dJv;
  PetscReal int_s,int_s_g,int_s_e,dJs;
  PetscInt e,q,i;
  EContainer ec_v,ec_s;
  PetscInt nelements,*element,*element_g,nbasis,npe;
  PetscReal *coords,*coords_g,*_el_coor;
  PetscInt fid = 0;
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-fid",&fid,NULL);CHKERRQ(ierr);
  
  /*
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/triangle-domain/tri.1",1,DMTET_CWARP_AND_BLEND,3,&dms);CHKERRQ(ierr);
    ierr = DMSetUp(dms);CHKERRQ(ierr);
    ierr = DMTetGeometryView2d_VTK(dms,"tri-domain-g.vtu");
  }
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dms,&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dms);CHKERRQ(ierr);
  ierr = DMSetUp(dm);CHKERRQ(ierr);
  */
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/triangle-domain/tri.1",1,DMTET_CWARP_AND_BLEND,3,&dm);CHKERRQ(ierr);
  
  ierr = DMTetTopologySetup(dm,1);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dm);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nelements,&nbasis,&element,&npe,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&q_vol);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(q_vol,dm);CHKERRQ(ierr);
  ierr = QuadratureGetRule(q_vol,&nqp_vol,&xi_vol,&w_vol);CHKERRQ(ierr);
  ierr = EContainerCreate_Generic(dm,q_vol,&ec_v);CHKERRQ(ierr);
  
  _dJv     = ec_v->buf_q_scalar_a;
  _el_coor = ec_v->buf_basis_2vector_a;
  
  /* 
   \int div(F) dV, F = 0.5 (x, y)
  */
  int_vol = 0.0;
  for (e=0; e<nelements; e++) {
    PetscInt *eidx;
    
    eidx = &element[nbasis*e];
    for (i=0; i<nbasis; i++) {
      _el_coor[2*i+0] = coords[2*eidx[i]+0];
      _el_coor[2*i+1] = coords[2*eidx[i]+1];
    }
    
    EvaluateElementGeometry(nqp_vol,nbasis,_el_coor,
                                   ec_v->dNr1,ec_v->dNr2,_dJv);
    
    int_vol_e = 0.0;
    for (q=0; q<nqp_vol; q++) {
      PetscReal x_qp[] = {0.0,0.0};
      PetscReal lform = 0.0;
      
      // get x at quadrature point
      for (i=0; i<nbasis; i++) {
        x_qp[0] += ec_v->N[q][i] * _el_coor[2*i+0];
        x_qp[1] += ec_v->N[q][i] * _el_coor[2*i+1];
      }
      
      // eval div(F)
      evaluate_analytic_function(fid,x_qp,&lform,NULL);
      
      dJv = _dJv[q];
      int_vol_e += w_vol[q] * lform * dJv;
    }
    int_vol += int_vol_e;
  }
  ierr = MPI_Allreduce(&int_vol,&int_vol_g,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"I_v %+1.10e\n",int_vol_g);
  
  ierr = QuadratureCreate(&q_surf);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM_Facet(q_surf,dm);CHKERRQ(ierr);
  ierr = QuadratureGetRule(q_surf,&nqp_s,&xi_s,&w_s);CHKERRQ(ierr);
  ierr = QuadratureComputeCoordinates(q_surf,dm);CHKERRQ(ierr);
  ierr = EContainerCreate_Generic(dm,q_surf,&ec_s);CHKERRQ(ierr);

  
  /*
   \int F.n dS, F = 0.5 (x, y)
  */

#if 0 // see below for better usage
  DM_TET     *tet;
  DMTetMesh  *geometry,*space;
  BFacetList f_g,f_s;
  PetscInt   bf;
  PetscReal *xiface;
  
  tet = (DM_TET*)dm->data;
  geometry = tet->geometry;
  space    = tet->space;
  f_g = geometry->boundary_facets;
  f_s = space->boundary_facets;

  ierr = PetscMalloc1(2*nqp_s,&xiface);CHKERRQ(ierr);

  int_s = 0.0;
  for (bf=0; bf<f_g->nfacets; bf++) {
    PetscInt *eidx,nb;
    PetscReal dl;
    PetscReal **N_face;
    
    e = f_s->facet_to_element[bf];
    
    eidx = &element[nbasis*e];
    for (i=0; i<nbasis; i++) {
      _el_coor[2*i+0] = coords[2*eidx[i]+0];
      _el_coor[2*i+1] = coords[2*eidx[i]+1];
    }
    
    //EvaluateElementGeometry(nqp_vol,nbasis,_el_coor,
    //                        ec_v->dNr1,ec_v->dNr2,_dJv);
    
    {
      PetscReal dx,dy;
      PetscInt *eidx_g;
      
      eidx_g = &element_g[3*e];
      
      switch (f_s->facet_element_face_id[bf]) {
        case 0:
          dx = coords_g[2*eidx_g[1]] - coords_g[2*eidx_g[0]];
          dy = coords_g[2*eidx_g[1]+1] - coords_g[2*eidx_g[0]+1];
          
          for (q=0; q<nqp_s; q++) {
            xiface[2*q+0] = xi_s[q];
            xiface[2*q+1] = -1.0;
          }
          break;
        
        case 1:
          dx = coords_g[2*eidx_g[2]] - coords_g[2*eidx_g[1]];
          dy = coords_g[2*eidx_g[2]+1] - coords_g[2*eidx_g[1]+1];

          for (q=0; q<nqp_s; q++) {
            xiface[2*q+1] = xi_s[q];
            xiface[2*q+0] = -xi_s[q];
          }
          break;
        
        case 2:
          dx = coords_g[2*eidx_g[0]] - coords_g[2*eidx_g[2]];
          dy = coords_g[2*eidx_g[0]+1] - coords_g[2*eidx_g[2]+1];
          for (q=0; q<nqp_s; q++) {
            xiface[2*q+0] = -1.0;
            xiface[2*q+1] = xi_s[q];
          }
          break;
      }
      
      dl = PetscSqrtReal(dx*dx + dy*dy);
    }
    
    ierr = DMTetTabulateBasis(dm,nqp_s,xiface,&nb,&N_face);CHKERRQ(ierr);

    
    int_s_e = 0.0;
    for (q=0; q<nqp_s; q++) {
      PetscReal x_qp[] = {0.0,0.0};
      PetscReal F_qp[] = {0.0,0.0};
      PetscReal lform,*normal;
      
      // get x at quadrature point
      for (i=0; i<nbasis; i++) {
        x_qp[0] += N_face[q][i] * _el_coor[2*i+0];
        x_qp[1] += N_face[q][i] * _el_coor[2*i+1];
      }
      // PetscPrintf(PETSC_COMM_SELF,"%+1.4e %+1.4e\n",x_qp[0],x_qp[1]);
      
      // eval F
      evaluate_analytic_function(fid,x_qp,NULL,F_qp);
      
      normal = &f_s->face_normal[2*bf];
      lform = F_qp[0] * normal[0] + F_qp[1] * normal[1];
      
      dJs = dl;
      int_s_e += w_s[q] * lform * 0.5 * dJs;
    }
    int_s += int_s_e;
    
    
    ierr = PetscFree(xiface);CHKERRQ(ierr);
    for (q=0; q<nqp_s; q++) {
      ierr = PetscFree(N_face[q]);CHKERRQ(ierr);
    }
    ierr = PetscFree(N_face);CHKERRQ(ierr);
    
  }
   PetscPrintf(PETSC_COMM_SELF,"I_s %+1.10e\n",int_s);
#endif

  

  
  /* 
   Better way to define surface integration
  */
  DM_TET     *tet;
  DMTetMesh  *geometry,*space;
  BFacetList f_g,f_s;
  PetscInt   bf;
  PetscReal  _el_basis_face_coor[2*30];
  PetscReal *face_quadrature_coor;
  
  tet = (DM_TET*)dm->data;
  geometry = tet->geometry;
  space    = tet->space;
  f_g = geometry->boundary_facets;
  f_s = space->boundary_facets;
  
  ierr = QuadratureGetProperty(q_surf,"coor",NULL,NULL,NULL,&face_quadrature_coor);CHKERRQ(ierr);

  
  int_s = 0.0;
  for (bf=0; bf<f_g->nfacets; bf++) {
    PetscInt *eidx,face_id;
    
    e = f_s->facet_to_element[bf];
    face_id = f_s->facet_element_face_id[bf];
    
    eidx = &element[nbasis*e];
    for (i=0; i<nbasis; i++) {
      _el_coor[2*i+0] = coords[2*eidx[i]+0];
      _el_coor[2*i+1] = coords[2*eidx[i]+1];
    }
    
    ierr = BFacetRestrictField(f_s,face_id,2,_el_coor,_el_basis_face_coor);CHKERRQ(ierr);
    EvaluateBasisSurfaceJacobian_Affine(f_s->nbasis_facet,_el_basis_face_coor,&dJs);

    int_s_e = 0.0;
    for (q=0; q<nqp_s; q++) {
      PetscReal *x_qp;
      PetscReal F_qp[] = {0.0,0.0};
      PetscReal lform,*normal,*face_quadrature_coor_e;
      
      ierr = QuadratureGetElementValues(q_surf,bf,2,face_quadrature_coor,&face_quadrature_coor_e);
      x_qp = &face_quadrature_coor_e[2*q];
      
      // PetscPrintf(PETSC_COMM_SELF,"%+1.4e %+1.4e\n",x_qp[0],x_qp[1]);
      
      // eval F
      evaluate_analytic_function(fid,x_qp,NULL,F_qp);
      
      normal = &f_s->face_normal[2*bf];
      lform = F_qp[0] * normal[0] + F_qp[1] * normal[1];
      
      int_s_e += w_s[q] * lform * dJs;
    }
    int_s += int_s_e;
  }
  ierr = MPI_Allreduce(&int_s,&int_s_g,1,MPIU_REAL,MPIU_SUM,PETSC_COMM_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"I_s %+1.10e\n",int_s_g);
  
  ierr = EContainerDestroy(&ec_v);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ec_s);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&q_vol);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&q_surf);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = example1();CHKERRQ(ierr);
  ierr = example2();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

