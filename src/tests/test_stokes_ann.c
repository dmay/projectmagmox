
#include <petsc.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdestokes.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>


#undef __FUNCT__
#define __FUNCT__ "fp_quadrature"
PetscErrorCode fp_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp;
  PetscReal *coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature"
PetscErrorCode eta_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp;
  PetscReal *coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 1.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature"
PetscErrorCode fu_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  dm = (DM)data;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq,r2,r,n[2],gravity[2],rho;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
     
      r = PetscSqrtReal(xq*xq + yq*yq);
      n[0] = -xq/r;
      n[1] = -yq/r;
      gravity[0] = 9.8 * n[0];
      gravity[1] = 9.8 * n[1];
      
      coeff[2*(e*nqp + q)+0] = 0.0;
      coeff[2*(e*nqp + q)+1] = 0.0;

      rho = 1.0;
      r2 = PetscSqrtReal( (xq-0.5)*(xq-0.5) + (yq-0.5)*(yq-0.5) );
      if (r2 < 0.1) {
        rho = 3.3;
      }

      r2 = PetscSqrtReal( (xq+0.65)*(xq+0.65) + (yq)*(yq) );
      if (r2 < 0.1) {
        rho = 3.3;
      }

      coeff[2*(e*nqp + q)+0] = rho * gravity[0];
      coeff[2*(e*nqp + q)+1] = rho * gravity[1];
      
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_uv"
PetscErrorCode EvalDirichletBoundary_uv(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal r;
  *constrain = PETSC_FALSE;
  
  r = PetscSqrtReal(coor[0]*coor[0] + coor[1]*coor[1]);
  if (r < 0.33 + 1.0e-6) { /* inner */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  } else if (r > 1.0 - 1.0e-6) { /* outer */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  //if (*constrain) printf("%1.4e %1.4e\n",coor[0],coor[1]);
  PetscFunctionReturn(0);
}

struct Point {
  PetscReal distance;
  PetscInt index;
};

int compare_point_min2max(const void *a,const void *b)
{
  int val;
  struct Point *point_a = (struct Point*)a;
  struct Point *point_b = (struct Point*)b;
  
  val = 0;
  if (point_a->distance < point_b->distance) {
    val = -1;
  } else if (point_a->distance > point_b->distance) {
    val = 1;
  }
  return(val);
}

int compare_point_max2min(const void *a,const void *b)
{
  int val;
  struct Point *point_a = (struct Point*)a;
  struct Point *point_b = (struct Point*)b;
  
  val = 0;
  if (point_a->distance < point_b->distance) {
    val = 1;
  } else if (point_a->distance > point_b->distance) {
    val = -1;
  }
  return(val);
}

#undef __FUNCT__
#define __FUNCT__ "InnerRing"
PetscErrorCode InnerRing(DM dm,PetscInt nnodes,PetscScalar coor[],PetscInt doflist[],PetscScalar vallist[],void *ctx)
{
  PetscErrorCode ierr;
  PetscInt nmin = *((PetscInt*)ctx);
  struct Point *point;
  PetscInt k;
  
  PetscFunctionBegin;
  ierr = PetscMalloc1(nnodes,&point);CHKERRQ(ierr);
  for (k=0; k<nnodes; k++) {
    point[k].distance = PetscSqrtReal(coor[2*k]*coor[2*k] + coor[2*k+1]*coor[2*k+1]);
    point[k].index = k;
  }
  
  qsort(point,nnodes,sizeof(struct Point),compare_point_min2max);
  
  for (k=0; k<nmin; k++) {
    doflist[ point[k].index ] = BCList_DIRICHLET;
    vallist[ point[k].index ] = 0.0;
  }
  
  ierr = PetscFree(point);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "OuterRing"
PetscErrorCode OuterRing(DM dm,PetscInt nnodes,PetscScalar coor[],PetscInt doflist[],PetscScalar vallist[],void *ctx)
{
  PetscErrorCode ierr;
  PetscInt nmin = *((PetscInt*)ctx);
  struct Point *point;
  PetscInt k;
 
  PetscFunctionBegin;
  ierr = PetscMalloc1(nnodes,&point);CHKERRQ(ierr);
  for (k=0; k<nnodes; k++) {
    point[k].distance = PetscSqrtReal(coor[2*k]*coor[2*k] + coor[2*k+1]*coor[2*k+1]);
    point[k].index = k;
  }
  
  qsort(point,nnodes,sizeof(struct Point),compare_point_max2min);
  
  for (k=0; k<nmin; k++) {
    doflist[ point[k].index ] = BCList_DIRICHLET;
    //printf("%d %1.4e %1.4e\n",point[k].index,coor[2*point[k].index],coor[2*point[k].index+1]);
    vallist[ point[k].index ] = 0.0;
  }
  
  ierr = PetscFree(point);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 Defines a model with a curved boundary.
 A hacky implementation is required to impose Dirichlet BCs on this boundary.
 See stokes_test2() for a better implementation which iterates over the boundary facets.
*/
#undef __FUNCT__
#define __FUNCT__ "stokes_test1"
PetscErrorCode stokes_test1(void)
{
  PetscErrorCode ierr;
  DM dmu,dmp,dms;
  PDE pde;
  SNES snes;
  Mat A;
  Vec X,F;
  BCList bc;
  PetscInt k,ninner,nouter;

  k = 1;
  PetscOptionsGetInt(NULL,NULL,"-k",&k,NULL);

  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/annulus/ann.1",2,DMTET_CWARP_AND_BLEND,k+1,&dmu);CHKERRQ(ierr);

  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/annulus/ann.1",1,DMTET_CWARP_AND_BLEND,k,&dmp);CHKERRQ(ierr);

  ierr = DMBCListCreate(dmu,&bc);CHKERRQ(ierr);

  {
    PetscInt j,nn,nseg;
    PetscReal *coor;
    
    ninner = 0;
    nouter = 0;
    
    DMTetGeometryElement(dmu,0,0,0,0,&nn,&coor);
    for (j=0; j<nn; j++) {
      PetscReal r = PetscSqrtReal(coor[2*j+0]*coor[2*j+0] + coor[2*j+1]*coor[2*j+1]);
      if (r < 0.33 + 1.0e-3) { /* inner */
        ninner++;
      }
    }
    nseg = ninner;
    ninner = (k+1)*nseg;
    PetscPrintf(PETSC_COMM_WORLD,"n_inner points %d \n",ninner);

    for (j=0; j<nn; j++) {
      PetscReal r = PetscSqrtReal(coor[2*j+0]*coor[2*j+0] + coor[2*j+1]*coor[2*j+1]);
      if (r > 1.0 - 1.0e-3) { /* outer */
        nouter++;
      }
    }
    nseg = nouter;
    nouter = (k+1)*nseg;
    PetscPrintf(PETSC_COMM_WORLD,"n_outer points %d \n",nouter);
  }
  
  ierr = DMTetBCListUserTraverse(bc,0,InnerRing,(void*)&ninner);CHKERRQ(ierr);
  ierr = DMTetBCListUserTraverse(bc,0,OuterRing,(void*)&nouter);CHKERRQ(ierr);
  
  ierr = DMTetBCListUserTraverse(bc,1,InnerRing,(void*)&ninner);CHKERRQ(ierr);
  ierr = DMTetBCListUserTraverse(bc,1,OuterRing,(void*)&nouter);CHKERRQ(ierr);

  
  ierr = PDECreateStokes(PETSC_COMM_WORLD,dmu,dmp,bc,&pde);CHKERRQ(ierr);
  ierr = PDEStokesGetDM(pde,&dms);CHKERRQ(ierr);
  
  ierr = PDEStokesCreateOperator_MatNest(pde,&A);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&X,&F);CHKERRQ(ierr);

  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmu,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmp,&xp[1]);CHKERRQ(ierr);

    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
  
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }

  /* boundary condition */
  {
    Vec velocity,pressure;
    
    ierr = DMCompositeGetAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
    ierr = BCListInsert(bc,velocity);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
  }

  {
    Coefficient e,fu,fp;
    
    ierr = PDEStokesGetCoefficients(pde,&e,&fu,&fp);CHKERRQ(ierr);

    ierr = CoefficientSetComputeQuadrature(e,eta_quadrature,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fp,fp_quadrature,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)dmp);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,A,A);CHKERRQ(ierr);
  
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  ierr = PDEStokesFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
  
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
 
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AnnulusNoSlip"
PetscErrorCode AnnulusNoSlip(PetscScalar coor[],PetscScalar n[],PetscScalar t[],PetscInt dofidx,PetscScalar *val,PetscBool *is_dirichlet,void *ctx)
{
  //PetscPrintf(PETSC_COMM_SELF,"%1.4e %1.4e   %1.4e %1.4e  %1.4e %1.4e\n",coor[0],coor[1],n[0],n[1],t[0],t[1]);
  *is_dirichlet = PETSC_TRUE;
  *val = 0.0;
  
  PetscFunctionReturn(0);
}

/*
 Uses the DMTet facet iterator to set the boundary conditions
*/
#undef __FUNCT__
#define __FUNCT__ "stokes_test2"
PetscErrorCode stokes_test2(void)
{
  PetscErrorCode ierr;
  DM dmu,dmp,dms;
  PDE pde;
  SNES snes;
  Mat A;
  Vec X,F;
  BCList bc;
  PetscInt k;
  
  k = 1;
  PetscOptionsGetInt(NULL,NULL,"-k",&k,NULL);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/annulus/ann.1",2,DMTET_CWARP_AND_BLEND,k+1,&dmu);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dmu);CHKERRQ(ierr);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/annulus/ann.1",1,DMTET_CWARP_AND_BLEND,k,&dmp);CHKERRQ(ierr);
  
  ierr = DMBCListCreate(dmu,&bc);CHKERRQ(ierr);
  
  
  ierr = DMTetFacetBCListTraverse(bc,0,AnnulusNoSlip,NULL);CHKERRQ(ierr);
  ierr = DMTetFacetBCListTraverse(bc,1,AnnulusNoSlip,NULL);CHKERRQ(ierr);
  
  
  ierr = PDECreateStokes(PETSC_COMM_WORLD,dmu,dmp,bc,&pde);CHKERRQ(ierr);
  ierr = PDEStokesGetDM(pde,&dms);CHKERRQ(ierr);
  
  ierr = PDEStokesCreateOperator_MatNest(pde,&A);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&X,&F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmu,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmp,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  
  /* boundary condition */
  {
    Vec velocity,pressure;
    
    ierr = DMCompositeGetAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
    ierr = BCListInsert(bc,velocity);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dms,X,&velocity,&pressure);CHKERRQ(ierr);
  }
  
  {
    Coefficient e,fu,fp;
    
    ierr = PDEStokesGetCoefficients(pde,&e,&fu,&fp);CHKERRQ(ierr);
    
    ierr = CoefficientSetComputeQuadrature(e,eta_quadrature,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fp,fp_quadrature,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)dmp);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,A,A);CHKERRQ(ierr);
  
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  ierr = PDEStokesFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
  
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
./test_stokes_ann.app -ksp_monitor_true_residual  -fieldsplit_u_ksp_converged_reason -fieldsplit_u_pc_type lu  -ksp_type fgmres -k 1 -fieldsplit_u_ksp_rtol 1.0e-1 -fieldsplit_u_ksp_monitor -pc_fieldsplit_schur_fact_type UPPER  -fieldsplit_p_ksp_type preonly -fieldsplit_p_pc_type lu
*/
int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt tidx = 2;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(NULL,NULL,"-tidx",&tidx,NULL);CHKERRQ(ierr);
  switch (tidx) {
    case 1:
      ierr = stokes_test1();CHKERRQ(ierr);
      break;
    case 2:
      ierr = stokes_test2();CHKERRQ(ierr);
      break;
    case 3:
      break;
      
    default:
      break;
  }
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

