
#include <petsc.h>
#include <petscmat.h>
#include <petscdmtet.h>
#include <proj_init_finalize.h>

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  Mat A;
  PetscInt row[]={-1,-1,-1},col[] = {-1,-1,-1};
  PetscMPIInt commsize,commrank;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);

  
  ierr = MatCreate(PETSC_COMM_WORLD,&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,30,40);CHKERRQ(ierr);
  ierr = MatSetType(A,MATAIJ);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);

  ierr = FARCMatPreallocationInit(A);CHKERRQ(ierr);

  row[0] = 2;
  col[0] = 3;
  ierr = FARCMatPreallocationSetValues(A,3,row,3,col);CHKERRQ(ierr);

  ierr = FARCMatPreallocationFlush(A);CHKERRQ(ierr);
  
  row[0] = 2;
  col[0] = 4;
  ierr = FARCMatPreallocationSetValues(A,1,row,1,col);CHKERRQ(ierr);

  row[0] = 2;
  col[0] = 10;
  ierr = FARCMatPreallocationSetValues(A,1,row,1,col);CHKERRQ(ierr);

  row[0] = 2;
  col[0] = 12;
  ierr = FARCMatPreallocationSetValues(A,1,row,1,col);CHKERRQ(ierr);

  row[0] = 2;
  col[0] = 13;
  ierr = FARCMatPreallocationSetValues(A,1,row,1,col);CHKERRQ(ierr);

  if (commrank == 0) {
    row[0] = 2;
    col[0] = 14;
    ierr = FARCMatPreallocationSetValues(A,1,row,1,col);CHKERRQ(ierr);
  }

  
  
  
  ierr = FARCMatPreallocationFinalize(A);CHKERRQ(ierr);

  ierr = MatSetValue(A,2,3,3.3,INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatSetValue(A,2,4,3.3,INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatSetValue(A,2,10,3.3,INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatSetValue(A,2,12,3.3,INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatSetValue(A,2,13,3.3,INSERT_VALUES);CHKERRQ(ierr);
  ierr = MatSetValue(A,2,14,3.3,INSERT_VALUES);CHKERRQ(ierr);

  
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return(0);
}


