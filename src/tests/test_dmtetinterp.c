
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <proj_init_finalize.h>


#undef __FUNCT__
#define __FUNCT__ "func1"
PetscErrorCode func1(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = PetscSinReal(PETSC_PI * x[0]) * x[1] * PetscCosReal(3.0 * PETSC_PI * x[0] * x[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func2"
PetscErrorCode func2(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = PetscSinReal(4.4 * PETSC_PI * x[0]) + 3.3;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func3"
PetscErrorCode func3(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = x[0] * x[1] * x[1] + 2.0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "poly_p3"
PetscErrorCode poly_p3(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = 2.0 + x[0] + x[0]*x[1] + x[0]*x[0]*x[0] + 0.33 * x[1]*x[1]*x[1] + 0.11 * x[0]*x[1]*x[1];
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "test_interp"
PetscErrorCode test_interp(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT,dmTsub;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xsub;
  Mat interp_sub2g;
  
  /* thermal problem */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmTsub);CHKERRQ(ierr);

  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p+2,&dmT);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmTsub,&Xsub);CHKERRQ(ierr);

  /* interpolate from sub to parent */
  ierr = DMTetVecTraverse(dmTsub,Xsub,poly_p3,NULL);CHKERRQ(ierr);
  ierr = VecSet(X,-20.0);CHKERRQ(ierr);

  ierr = DMCreateInterpolation(dmTsub,dmT,&interp_sub2g,NULL);CHKERRQ(ierr);
  ierr = MatMult(interp_sub2g,Xsub,X);CHKERRQ(ierr);

  PetscSNPrintf(prefix,127,"%s_sub2p_ref_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);

  PetscSNPrintf(prefix,127,"%s_sub2p_interp_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);

  /* clean up */
  ierr = MatDestroy(&interp_sub2g);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsub);CHKERRQ(ierr);
  ierr = DMDestroy(&dmTsub);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_interp_mpi"
PetscErrorCode test_interp_mpi(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT_s=NULL,dmT=NULL,dmTsub;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xsub;
  Mat interp_sub2g;
  PetscMPIInt commrank;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  /* thermal problem */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r3/wedge.2",
                                     1,DMTET_CWARP_AND_BLEND,T_p,&dmT_s);CHKERRQ(ierr);
    ierr = DMSetUp(dmT_s);CHKERRQ(ierr);
  }
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dmT_s,1,DMTET_CWARP_AND_BLEND,T_p,&dmTsub);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT_s);CHKERRQ(ierr);
  ierr = DMSetUp(dmTsub);CHKERRQ(ierr);
  
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r3/wedge.2",
                                     1,DMTET_CWARP_AND_BLEND,T_p,&dmT_s);CHKERRQ(ierr);
    ierr = DMSetUp(dmT_s);CHKERRQ(ierr);
  }
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dmT_s,1,DMTET_CWARP_AND_BLEND,T_p+2,&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT_s);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);
  
  
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmTsub,&Xsub);CHKERRQ(ierr);
  
  /* interpolate from sub to parent */
  ierr = DMTetVecTraverse(dmTsub,Xsub,poly_p3,NULL);CHKERRQ(ierr);
  ierr = VecSet(X,-20.0);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dmTsub,dmT,&interp_sub2g,NULL);CHKERRQ(ierr);
  ierr = MatMult(interp_sub2g,Xsub,X);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_sub2p_ref_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_sub2p_interp_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  /* clean up */
  ierr = MatDestroy(&interp_sub2g);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsub);CHKERRQ(ierr);
  ierr = DMDestroy(&dmTsub);CHKERRQ(ierr);
  
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = test_interp();CHKERRQ(ierr);
  } else {
    ierr = test_interp_mpi();CHKERRQ(ierr);
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

