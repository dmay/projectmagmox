
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <pdestokes.h>
#include <pdestokesdarcy2field.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <coefficient.h>
#include <Batchelor_corner_flow_soln.h>
#include <index.h>


typedef struct {
  PetscReal eta;         // shear viscosity
  PetscReal phi,phi0,n;         // porosity
  PetscReal rho,rho_l;   // solid, liquid density [\rho_s , \rho_f]
  PetscReal zeta,k,k0;      // bulk viscosity, effective permability
  PetscReal g[2];        // gravity term
  DM        dm;          // mesh
  PetscReal fu_fac;      // scaling factor
  PetscReal Vdarcy;
} MaterialProperties;


#undef __FUNCT__
#define __FUNCT__ "EvalVelocity_vanKeken"
PetscErrorCode EvalVelocity_vanKeken(PetscReal coor[],PetscReal vel[])
{
  double plate_thickness,alpha,origin_x,origin_y,origin[2],vmag,veld[2],coord[2];
  
  plate_thickness = 50.0 / 600.0; // normalized
  origin_x = plate_thickness;
  origin_y = -plate_thickness;
  alpha = atan2(-origin_y, origin_x); // slab dip angle
  origin[0] = origin_x;
  origin[1] = origin_y;
  vmag = sqrt(1.0*1.0 + 1.0*1.0);
  coord[0] = (PetscReal)coor[0];
  coord[1] = (PetscReal)coor[1];
  
  evaluate_BatchelorCornerFlowSoln(alpha,origin,coord,veld);
  vel[0] = (PetscReal)veld[0];
  vel[1] = (PetscReal)veld[1];
  
  if (coor[1] >= -50.0/600.0) { /* crust */
    vel[0] = 0.0;
    vel[1] = 0.0;
  }
  if (coor[1] <= -coor[0]) { /* slab */
    vel[0] =  1.0 / vmag;
    vel[1] = -1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalTemperature_Case1A"
PetscErrorCode EvalTemperature_Case1A(PetscScalar coor[],PetscInt dof,
                                     PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_s = 273.0;
  PetscReal T_0 = 1573.0;
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  
  *constrain = PETSC_FALSE;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_s;
    
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_s + (T_0 - T_s) * erf( depth * 600.0*1.0e3 / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;

    PetscFunctionReturn(0);
  }

  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */

      T = T_s + (T_0 - T_s) * depth / plate_thickness;
      *constrain = PETSC_TRUE;
      *val = T;
      
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      PetscReal origin[2],alpha;
      double coor_d[2],vel_d[2];

      origin[0] = plate_thickness;
      origin[1] = -plate_thickness;
      alpha = atan2(-origin[1], origin[0]); // slab dip angle

      coor_d[0] = (double)coor[0];
      coor_d[1] = (double)coor[1];
      evaluate_BatchelorCornerFlowSoln(alpha,origin,coor_d,vel_d);
      
      if (vel_d[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val = T_0;
        PetscFunctionReturn(0);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_uv"
PetscErrorCode plate_uv(PetscScalar coor[],PetscInt dof,
                                               PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_TRUE;
  if (dof == 0) {
    *val = 0.0;
  } else if (dof == 1) {
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_boundary_base_uv"
PetscErrorCode plate_boundary_base_uv(PetscScalar coor[],PetscInt dof,
                                 PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal depth,plate_thickness;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  *constrain = PETSC_FALSE;
  
  if (depth < plate_thickness+1.0e-10) { /* bottom of plate boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_uv"
PetscErrorCode slab_uv(PetscScalar coor[],PetscInt dof,
                        PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vmag;
  
  *constrain = PETSC_TRUE;
  vmag = PetscSqrtReal(1.0*1.0 + 1.0*1.0);
  if (dof == 0) {
    *val = 1.0 / vmag;
  } else if (dof == 1) {
    *val = -1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_uv2"
PetscErrorCode slab_boundary_uv2(PetscScalar coor[],PetscInt dof,
                                PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2];
  PetscErrorCode ierr;
  
  *constrain = PETSC_FALSE;
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] > 1.0) { /* bottom boundary */
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  if (coor[1] < -coor[0] + 1.0e-10) { /* slab */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "wedge_inflow"
PetscErrorCode wedge_inflow(PetscScalar coor[],PetscInt dof,
                                               PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2],depth,plate_thickness;
  PetscErrorCode ierr;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  *constrain = PETSC_FALSE;
  
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth < plate_thickness) {
      if (dof == 0) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      } else if (dof == 1) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      }
    }
    if (depth > plate_thickness) { /* inflow from the mantle */
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val  = vel[dof];
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "rhs_fp_QuadratureEvaluator"
PetscErrorCode rhs_fp_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscReal val,*coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  val = 0.0;
  ierr = QuadratureSetValues(quadrature,1,&val,coeff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "rhs_fv_QuadratureEvaluator"
PetscErrorCode rhs_fv_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  MaterialProperties *mprop;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,delta_rho;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  dm = mprop->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nbasis,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  delta_rho = mprop->rho - mprop->rho_l;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      coeff[2*(e*nqp + q)+0] = mprop->phi * delta_rho * mprop->g[0] * mprop->fu_fac;
      coeff[2*(e*nqp + q)+1] = mprop->phi * delta_rho * mprop->g[1] * mprop->fu_fac;
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "coeff_QuadratureEvaluator"
PetscErrorCode coeff_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  MaterialProperties *mprop;
  PetscInt q,e,nen,nqp,*ridx;
  PetscReal *coeff,delta_rho;
  DM dm;
  PetscReal e3[2];
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  dm = mprop->dm;

  ierr = DMTetGeometryGetAttributes(dm,NULL,&ridx,NULL);CHKERRQ(ierr);

  /* shear viscosity */
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = mprop->eta;
    }
  }

  /* bulk viscosity */
  ierr = QuadratureGetProperty(quadrature,"zeta*",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureSetValues(quadrature,1,&mprop->zeta,coeff);CHKERRQ(ierr);
  
  /* effective permability */
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureSetValues(quadrature,1,&mprop->k,coeff);CHKERRQ(ierr);
  
  /* Darcy buouyancy */
  ierr = QuadratureGetProperty(quadrature,"e_3",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  delta_rho = mprop->rho - mprop->rho_l;
  e3[0] = mprop->fu_fac * ( delta_rho ) * mprop->g[0];
  e3[1] = mprop->fu_fac * ( delta_rho ) * mprop->g[1];
  ierr = QuadratureSetValues(quadrature,2,e3,coeff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "coeff_QuadratureEvaluatorVariable"
PetscErrorCode coeff_QuadratureEvaluatorVariable(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  MaterialProperties *mprop;
  PetscInt i,q,e,nen,nqp,*ridx,*element,npe,nbasis;
  PetscReal *coeff,delta_rho;
  PetscReal *coords,*elcoords,**tab_N;
  DM dm;
  PetscReal e3[2];
  PetscReal *xi;
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  dm = mprop->dm;
  
  ierr = DMTetGeometryGetAttributes(dm,NULL,&ridx,NULL);CHKERRQ(ierr);
  
  /* shear viscosity */
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = mprop->eta;
    }
  }
  
  /* bulk viscosity */
  ierr = QuadratureGetProperty(quadrature,"zeta*",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureSetValues(quadrature,1,&mprop->zeta,coeff);CHKERRQ(ierr);
  
  /* effective permability */
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,NULL);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nbasis,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      coeff[e*nqp + q] = mprop->k; // * (1.0 + 10.0 * exp(-(xq-0.5)*(xq-0.5)/0.001-(yq-(-0.3))*(yq-(-0.3))/0.02));
    }
  }
  
  /* Darcy buouyancy */
  ierr = QuadratureGetProperty(quadrature,"e_3",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  delta_rho = mprop->rho - mprop->rho_l;
  e3[0] = mprop->fu_fac * ( delta_rho ) * mprop->g[0];
  e3[1] = mprop->fu_fac * ( delta_rho ) * mprop->g[1];
  ierr = QuadratureSetValues(quadrature,2,e3,coeff);CHKERRQ(ierr);
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyVel_SurfaceQuadratureEvaluator"
PetscErrorCode DarcyVel_SurfaceQuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp;
  MaterialProperties *mprop;
  PetscReal depth;

  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  for (e=0; e<nfacets; e++) {
    
    for (q=0; q<nqp; q++) {
      PetscReal *x_qp,flux_qp[2],k_star,delta_rho;
      
      /* get x,y coords at each quadrature point */
      x_qp = &q_coor[(e*nqp+q)*2];
      //FIELD_GET_OFFSET(q_coor,e,nqp,2,x_qp); // this is incorrect

      depth = PetscAbsReal(x_qp[1]);

      flux_qp[0] = 0.0;
      flux_qp[1] = 0.0;
      if (x_qp[1] < -x_qp[0] + 1.0e-10) { /* slab - wedge */
        flux_qp[0] = -1.0 * (mprop->Vdarcy) / PetscSqrtReal(2.0);
        flux_qp[1] = -1.0 * (mprop->Vdarcy) / PetscSqrtReal(2.0);
      }
      if (depth < (0.08333333333333 + 1.0e-10)) { /* plate - wedge */
        k_star = mprop->k;
        delta_rho = mprop->rho - mprop->rho_l;
        flux_qp[0] = -1.0*k_star * mprop->fu_fac * ( delta_rho ) * mprop->g[0];
        flux_qp[1] = -1.0*k_star * mprop->fu_fac * ( delta_rho ) * mprop->g[1];
      }
      q_coeff[e*nqp*2 + 2*q+0] = flux_qp[0];
      q_coeff[e*nqp*2 + 2*q+1] = flux_qp[1];
      
      //q_coeff_e[FIELD_ID(q,0,2)] = flux[0];
      //q_coeff_e[FIELD_ID(q,1,2)] = flux[1];
      //FIELD_SET_VALS2(flux_qp,q_coeff_e,q);
    }
  }

/* <debug>
  for (e=0; e<nfacets*nqp; e++) {
    printf("<old> flux %+1.4e %+1.4e : %d\n",q_coeff[2*e+0],q_coeff[2*e+1],e);
  }
*/
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_null"
PetscErrorCode DarcyFluxEvaluator_null(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  flux[0] = 0.0;
  flux[1] = 0.0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_plate_wedge"
PetscErrorCode DarcyFluxEvaluator_plate_wedge(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  MaterialProperties *mprop = (MaterialProperties*)data;
  PetscReal depth;
  
  depth = PetscAbsReal(point->coor[1]);
  if (depth < (0.08333333333333 + 1.0e-10)) { /* plate - wedge */
    PetscReal k_star = mprop->k;
    PetscReal delta_rho = mprop->rho - mprop->rho_l;
    
    flux[0] = -1.0*k_star * mprop->fu_fac * ( delta_rho ) * mprop->g[0];
    flux[1] = -1.0*k_star * mprop->fu_fac * ( delta_rho ) * mprop->g[1];
  } else {
    point->values_set = PETSC_FALSE;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_slab_wedge"
PetscErrorCode DarcyFluxEvaluator_slab_wedge(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  MaterialProperties *mprop = (MaterialProperties*)data;

  if (point->coor[1] < -point->coor[0] + 1.0e-10) { /* slab - wedge */
    
    /* the negative sign next to the normal - this is required as normal[] refers to the outward pointing normal to the boundary */
    /* Using the normal enables the slab dip to be altered */
    /* I noticed that using 1.0/PetscSqrtReal(2.0) versis normal[] results in very tiny differences in the residuals */
    flux[0] = -1.0 * (mprop->Vdarcy) * (-point->normal[0]);
    flux[1] = -1.0 * (mprop->Vdarcy) * (-point->normal[1]);
  } else {
    point->values_set = PETSC_FALSE;
  }
  PetscFunctionReturn(0);
}

/*
 Identical to DarcyVel_SurfaceQuadratureEvaluator(), just uses the iterator
*/
#undef __FUNCT__
#define __FUNCT__ "DarcyVel_SurfaceQuadratureEvaluator_v2"
PetscErrorCode DarcyVel_SurfaceQuadratureEvaluator_v2(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  MaterialProperties *mprop;
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  dm = mprop->dm;
  
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_null,(void*)mprop);CHKERRQ(ierr);
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_slab_wedge,(void*)mprop);CHKERRQ(ierr);
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_plate_wedge,(void*)mprop);CHKERRQ(ierr);
  
/* <debug>
  {
    PetscReal *q_coeff = NULL, *q_coor = NULL;
    PetscInt e,nfacets,nqp;

    ierr = QuadratureGetProperty(quadrature,"coor",&nen,  &nqp,  NULL,&q_coor);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature,"q_N",&nfacets,&nqp,NULL,&q_coeff);CHKERRQ(ierr);
    for (e=0; e<nfacets*nqp; e++) {
      printf("<new> coor %+1.4e %+1.4e : flux %+1.4e %+1.4e : %d\n",q_coor[2*e],q_coor[2*e+1],q_coeff[2*e+0],q_coeff[2*e+1],e);
    }
  }
*/
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FluxFree_SurfaceQuadratureEvaluator"
PetscErrorCode FluxFree_SurfaceQuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp;
  MaterialProperties *mprop;
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  for (e=0; e<nfacets; e++) {
    PetscReal *q_coeff_e;
    
    FIELD_GET_OFFSET(q_coeff,e,nqp,2,q_coeff_e); /* replaces q_coeff_e = &q_coeff[e*nqp*2] */
    for (q=0; q<nqp; q++) {
      PetscReal *x_qp,flux_qp[2],delta_rho,k_star,e3[2];
      
      /* get x,y coords at each quadrature point */
      x_qp = &q_coor[(e*nqp+q)*2];
      //FIELD_GET_OFFSET(q_coor,e,nqp,2,x_qp); // this is incorrect
      
      flux_qp[0] = 0.0;
      flux_qp[1] = 0.0;

      k_star = mprop->k;
      delta_rho = mprop->rho - mprop->rho_l;
      e3[0] = mprop->fu_fac * ( delta_rho ) * mprop->g[0];
      e3[1] = mprop->fu_fac * ( delta_rho ) * mprop->g[1];
      
      if (x_qp[1] < -x_qp[0] + 1.0e-10) { /* slab */
        flux_qp[0] = -k_star * e3[0];
        flux_qp[1] = -k_star * e3[1];
      }
      //q_coeff[e*nqp*2 + 2*q+0] = flux_qp[0];
      //q_coeff[e*nqp*2 + 2*q+1] = flux_qp[1];
      
      //q_coeff_e[FIELD_ID(q,0,2)] = flux[0];
      //q_coeff_e[FIELD_ID(q,1,2)] = flux[1];
      FIELD_SET_VALS2(flux_qp,q_coeff_e,q);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "vanKeken_Case1B_TwoPhase"
PetscErrorCode vanKeken_Case1B_TwoPhase(void)
{
  PetscErrorCode ierr;
  PetscInt   p_T,p_V;
  PetscReal  gmin[3],gmax[3],xr[2],yr[2],kappa_nd,kappa;
  DM         dm_T,dm_V,dm_P,dm_fulldomain_V,dm_fulldomain_P,dm_VP;
  char       prefix[128];
  Vec        X_T,Xlocal_T,F_T,Xsample,velocity,X_VP,F_VP;
  Mat        Jac_T,Jac_VP;
  PDE        pde_T,pde_VP;
  BCList     bc_T,bc_V;
  Mat        interp_tet2dasample;
  SNES       snes_T,snes_VP;
  PetscReal  L,V,T,E;
  DM         dmdasample;
  PetscInt   region_plate,region_slab,region_wedge;
  const char meshfilename[] = "examples/reference_meshes/subduction-1a/mesh-r1/wedge.1";
  //const char meshfilename[] = "examples/reference_meshes/subduction-1a/mesh-r2/wedge.2"; /* This mesh file needs to be generated - please refer to readme and scripts in the directory examples/reference_meshes/subduction-1a/mesh-r2 */
  MaterialProperties properties;
  PetscBool impose_slab_flux = PETSC_FALSE;
  
  /* set input options here */
  p_T = 2; // polynomial for velocity and temperature. Pressure is taken as p_T - 1
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&p_T,NULL);CHKERRQ(ierr);
  
  properties.n      = 3.0;
  properties.eta    = 1.0e21;
  properties.phi    = 1.0e-6;
  properties.phi0    = 1.0e-2;
  properties.rho    = 3300.0;
  properties.rho_l  = 2800.0;
  properties.zeta   = 1.0e22;
  properties.k      = 1.0e-12; // m^2
  properties.k0     = 1.0e-12; // m^2
  properties.g[0]   = 0.0;
  properties.g[1]   = 9.81;
  properties.dm     = NULL;
  properties.Vdarcy = 0.0;
  
  ierr = PetscOptionsGetReal(NULL,NULL,"-eta",&properties.eta,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-phi0",&properties.phi0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-zeta",&properties.zeta,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-k0",&properties.k0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-n",&properties.n,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetReal(NULL,NULL,"-phi",&properties.phi,NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,NULL,"-impose_slab_flux",&impose_slab_flux,NULL);CHKERRQ(ierr);
  if (impose_slab_flux) {
    /* set a non-zero default */
    properties.Vdarcy = 5.0 / (1.0e3 * 365.0 * 24.0 * 60.0 * 60.0); // 5.0 m/kyr
    ierr = PetscOptionsGetReal(NULL,NULL,"-Vdarcy",&properties.Vdarcy,NULL);CHKERRQ(ierr);
  }

  // compute phi
  if (impose_slab_flux) {
    PetscPrintf(PETSC_COMM_WORLD,"[note] Over-ridding any specified value of phi with a value consistent with the imposed Darcy velocity\n");
    properties.phi = properties.phi0 * PetscPowReal(properties.Vdarcy / (properties.k0 * (properties.rho - properties.rho_l) * properties.g[1]), 1.0/properties.n) ;
  }
  properties.k = properties.k0 * PetscPowReal(properties.phi/properties.phi0,properties.n);
  PetscPrintf(PETSC_COMM_WORLD,"eta  = %1.4e\n",properties.eta);
  PetscPrintf(PETSC_COMM_WORLD,"zeta = %1.4e\n",properties.zeta);
  PetscPrintf(PETSC_COMM_WORLD,"k0   = %1.4e\n",properties.k0);
  PetscPrintf(PETSC_COMM_WORLD,"phi0 = %1.4e\n",properties.phi0);
  PetscPrintf(PETSC_COMM_WORLD,"phi  = %1.4e\n",properties.phi);
  PetscPrintf(PETSC_COMM_WORLD,"n    = %1.4e\n",properties.n);
  PetscPrintf(PETSC_COMM_WORLD,"k    = %1.4e\n",properties.k);
  if (impose_slab_flux) {
    PetscPrintf(PETSC_COMM_WORLD,"Imposing non-zero flux through the slab\n");
  }
  PetscPrintf(PETSC_COMM_WORLD,"Vd   = %1.4e\n",properties.Vdarcy);
  
  
  /* Length, velocity and time scales used: x = L x' */
  E = 1.0e21; /* viscosity scale */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  T = L/V; /* time scale */
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  kappa_nd = kappa * T / (L*L);
  
  properties.eta  = properties.eta / E;
  properties.zeta = properties.zeta / E;
  properties.k    = properties.k * E / (L * L);
  properties.k0    = properties.k0 * E / (L * L);
  properties.fu_fac = (L*L)/(V*E);
  properties.Vdarcy = properties.Vdarcy / V;
  
  /* Create temperature mesh */
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,p_T,&dm_T);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(dm_T,gmin,gmax);CHKERRQ(ierr);
  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  
  ierr = DMCreateGlobalVector(dm_T,&X_T);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm_T,&Xlocal_T);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_T,&F_T);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dm_T,&Jac_T);CHKERRQ(ierr);
  ierr = MatSetOption(Jac_T,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  /* Define initial condition for T */
  ierr = VecSet(X_T,273.0);CHKERRQ(ierr);
  
  /* Define Dirichlet boundary condition for temperature */
  ierr = DMBCListCreate(dm_T,&bc_T);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc_T,0,EvalTemperature_Case1A,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc_T,X_T);CHKERRQ(ierr);
  
  /* View temperature initial condition */
  {
    const char *fieldsT[] = { "T" };
    
    ierr = PetscSNPrintf(prefix,127,"energy_%sic_",fieldsT[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_T,1,&X_T,fieldsT,prefix);CHKERRQ(ierr);
  }
  
  /* Create mesh for velocity and pressure (needed by two-phase formulation) */
  p_V = p_T;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   2,DMTET_CWARP_AND_BLEND,p_V,&dm_fulldomain_V);CHKERRQ(ierr);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,p_V-1,&dm_fulldomain_P);CHKERRQ(ierr);
  
  /* Extract a sub velocity-pressure meshes from the wedge domain */
  {
    PetscInt nregions = 1;
    PetscInt regionlist[] = {2};
    
    ierr = DMTetCreateSubmeshByRegions(dm_fulldomain_V,nregions,regionlist,&dm_V);CHKERRQ(ierr);
    ierr = DMTetCreateSubmeshByRegions(dm_fulldomain_P,nregions,regionlist,&dm_P);CHKERRQ(ierr);

    ierr = DMTetBFacetSetup(dm_P);CHKERRQ(ierr);

    properties.dm = dm_P; /* Attatch dm_P for potentially interpolating coords */
  }
  
  /* Define velocity Dirichlet boundary conditions on the sub mesh */
  ierr = DMBCListCreate(dm_V,&bc_V);CHKERRQ(ierr);
  
  region_plate = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_plate,0,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_plate,1,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  
  region_slab = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,0,slab_boundary_uv2,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,1,slab_boundary_uv2,NULL);CHKERRQ(ierr);
  
  region_wedge = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_wedge,0,wedge_inflow,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_wedge,1,wedge_inflow,NULL);CHKERRQ(ierr);
  
  /* Define velocity field */
  ierr = DMCreateGlobalVector(dm_fulldomain_V,&velocity);CHKERRQ(ierr);
  
  /* Create a profiling DMDA */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,111,101,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dmdasample);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dmdasample,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmdasample,&Xsample);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dm_T,dmdasample,&interp_tet2dasample,NULL);CHKERRQ(ierr);
  
  /* Create the PDE for the thermal problem */
  ierr = PDECreate(PETSC_COMM_WORLD,&pde_T);CHKERRQ(ierr);
  ierr = PDESetType(pde_T,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde_T,X_T);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde_T,Xlocal_T);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde_T,dm_T,bc_T);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetVelocity(pde_T,dm_fulldomain_V,velocity);CHKERRQ(ierr);
  
  /* Define coefficients for the thermal problem */
  {
    Coefficient k,alpha,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde_T,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,kappa_nd);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_alpha(pde_T,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_g(pde_T,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadratureNULL(g);CHKERRQ(ierr);
  }
  
  /* Create the PDE for the two-phase flow problem */
  //ierr = PDECreateStokes(PETSC_COMM_WORLD,dm_V,dm_P,bc_V,&pde_VP);CHKERRQ(ierr);
  ierr = PDECreate(PETSC_COMM_WORLD,&pde_VP);CHKERRQ(ierr);
  ierr = PDESetType(pde_VP,PDESTKDCY2F);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde_VP,"stkdcy_");CHKERRQ(ierr);
  ierr = PDEStokesDarcy2FieldSetData(pde_VP,dm_V,dm_P,bc_V,NULL);CHKERRQ(ierr);
  
  ierr = PDEStokesDarcy2FieldGetDM(pde_VP,&dm_VP);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dm_VP,&Jac_VP);CHKERRQ(ierr);
  ierr = MatCreateVecs(Jac_VP,&X_VP,&F_VP);CHKERRQ(ierr);
  ierr = PDESetSolution(pde_VP,X_VP);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dm_V,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dm_P,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde_VP,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* Insert velocity boundary condition into the solution vector */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bc_V,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  /* Define coefficients for the two-phase flow problem 
   - note that kappa*, zeta*, e3 are contained "inside" the variable coeff
  */
  {
    Coefficient coeff,rhs_fu,rhs_fp,qN;
    
    ierr = PDEStokesDarcy2FieldGetCoefficients(pde_VP,&coeff,&rhs_fu,&rhs_fp,&qN);CHKERRQ(ierr);
    //ierr = CoefficientSetComputeQuadrature(coeff,   coeff_QuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(coeff,   coeff_QuadratureEvaluatorVariable,NULL,(void*)&properties);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(rhs_fu, rhs_fv_QuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(rhs_fp, rhs_fp_QuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);

    ierr = CoefficientEvaluate(coeff);CHKERRQ(ierr);
    ierr = CoefficientView_GP(coeff,dm_P,"bilinear");CHKERRQ(ierr);

    if (impose_slab_flux) {
      //ierr = CoefficientSetComputeQuadrature(qN,DarcyVel_SurfaceQuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);
      ierr = CoefficientSetComputeQuadrature(qN,DarcyVel_SurfaceQuadratureEvaluator_v2,NULL,(void*)&properties);CHKERRQ(ierr);
    }
    //ierr = CoefficientSetComputeQuadrature(qN,FluxFree_SurfaceQuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);

    ierr = CoefficientEvaluate(qN);CHKERRQ(ierr);
    ierr = CoefficientView_GP(qN,dm_P,"flux");CHKERRQ(ierr);
  }
  
  /* Create and configure the two-phase flow solver */
  ierr = PDESetOptionsPrefix(pde_VP,"vp_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes_VP);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_VP,snes_VP,F_VP,Jac_VP,Jac_VP);CHKERRQ(ierr);
  ierr = SNESSetType(snes_VP,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes_VP);CHKERRQ(ierr);
  
  /* Create and configure the thermal solver */
  ierr = PDESetOptionsPrefix(pde_T,"t_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes_T);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_T,snes_T,F_T,Jac_T,Jac_T);CHKERRQ(ierr);
  ierr = SNESSetType(snes_T,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes_T);CHKERRQ(ierr);
  
  /* Solve two-phase flow problem for v, p */
  ierr = PDESNESSolve(pde_VP,snes_VP);CHKERRQ(ierr);
  
  /* View v, p */
  ierr = PDEStokesFieldView_AsciiVTU(pde_VP,NULL);CHKERRQ(ierr);
  
  /* Compute and view the fluid velocity */
  {
    const char *field[] = { "uf" };
    Vec uf = NULL;
    Coefficient coeff,rhs_fu,rhs_fp;
    PetscReal *coeff_data;
    PetscInt nen,nqp,e,q;
    Quadrature quadrature;
    
    ierr = PDEStokesGetCoefficients(pde_VP,&coeff,&rhs_fu,&rhs_fp);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(coeff,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature,"k*",&nen,&nqp,NULL,&coeff_data);CHKERRQ(ierr);
    for (e=0; e<nen; e++) {
      for (q=0; q<nqp; q++) {
        coeff_data[e*nqp + q] = coeff_data[e*nqp + q] / properties.phi;
      }
    }
    
    ierr = PDEStokesDarcy2FieldComputeFluidVelocityP(pde_VP,&uf);CHKERRQ(ierr);
    ierr = DMTetSetDof(dm_P,2);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_P,1,&uf,field,"stkdcy_uf_");CHKERRQ(ierr);
    ierr = DMTetSetDof(dm_P,1);CHKERRQ(ierr);
    
    ierr = VecDestroy(&uf);CHKERRQ(ierr);

    for (e=0; e<nen; e++) {
      for (q=0; q<nqp; q++) {
        coeff_data[e*nqp + q] = coeff_data[e*nqp + q] * properties.phi;
      }
    }
  
  }
  
  {
    BCList      bc_fulldomain_V;
    PetscInt    regionindex;
    const char  *fieldsV[] = { "V" };
    Mat         inject_sub2g;
    VecScatter  scat;
    Vec         Xv,Xp;
    
    ierr = DMBCListCreate(dm_fulldomain_V,&bc_fulldomain_V);CHKERRQ(ierr);

    regionindex = 3;
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,0,plate_uv,NULL);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,1,plate_uv,NULL);CHKERRQ(ierr);

    regionindex = 1;
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,0,slab_uv,NULL);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,1,slab_uv,NULL);CHKERRQ(ierr);

    ierr = BCListInsert(bc_fulldomain_V,velocity);CHKERRQ(ierr);

    /* Inject wedge velocity into full domain */
    ierr = DMCreateInjection(dm_fulldomain_V,dm_V,&inject_sub2g);CHKERRQ(ierr);
    ierr = MatScatterGetVecScatter(inject_sub2g,&scat);CHKERRQ(ierr);

    ierr = DMCompositeGetAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,Xv,velocity,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,Xv,velocity,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    
    /* View the velocity field on the full domain */
    ierr = PetscSNPrintf(prefix,127,"stkdcy_v_fulldomain_");CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_fulldomain_V,1,&velocity,fieldsV,prefix);CHKERRQ(ierr);

    ierr = BCListDestroy(&bc_fulldomain_V);CHKERRQ(ierr);
    ierr = MatDestroy(&inject_sub2g);CHKERRQ(ierr);
  }
  
  /* Solve thermal problem for T */
  ierr = PDESNESSolve(pde_T,snes_T);CHKERRQ(ierr);
  {
    const char *fieldsT[] = { "T" };
    
    ierr = PetscSNPrintf(prefix,127,"energy_%s_",fieldsT[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_T,1,&X_T,fieldsT,prefix);CHKERRQ(ierr);
  }
  
  /* clean up */
  ierr = PDEDestroy(&pde_T);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde_VP);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes_T);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes_VP);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_T);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_V);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal_T);CHKERRQ(ierr);
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = VecDestroy(&X_T);CHKERRQ(ierr);
  ierr = VecDestroy(&F_T);CHKERRQ(ierr);
  ierr = VecDestroy(&X_VP);CHKERRQ(ierr);
  ierr = VecDestroy(&F_VP);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsample);CHKERRQ(ierr);
  ierr = MatDestroy(&Jac_VP);CHKERRQ(ierr);
  ierr = MatDestroy(&Jac_T);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2dasample);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_fulldomain_V);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_fulldomain_P);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_V);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_P);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_T);CHKERRQ(ierr);
  ierr = DMDestroy(&dmdasample);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = vanKeken_Case1B_TwoPhase();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

