
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <pdestokes.h>
#include <pdestokesdarcy2field.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <quadrature.h>
#include <coefficient.h>
#include <element_container.h>
#include <Batchelor_corner_flow_soln.h>


typedef struct {
  PetscReal eta;         // shear viscosity
  PetscReal phi;         // porosity
  PetscReal rho,rho_l;   // solid, liquid density [\rho_s , \rho_f]
  PetscReal zeta,k;      // bulk viscosity, effective permability
  PetscReal g[2];        // gravity term
  DM        dm;          // mesh
  PetscReal fu_fac;      // scaling factor
} MaterialProperties;


#undef __FUNCT__
#define __FUNCT__ "EvalVelocity_vanKeken"
PetscErrorCode EvalVelocity_vanKeken(PetscReal coor[],PetscReal vel[])
{
  double plate_thickness,alpha,origin_x,origin_y,origin[2],vmag,veld[2],coord[2];
  
  plate_thickness = 50.0 / 600.0; // normalized
  origin_x = plate_thickness;
  origin_y = -plate_thickness;
  alpha = atan2(-origin_y, origin_x); // slab dip angle
  origin[0] = origin_x;
  origin[1] = origin_y;
  vmag = sqrt(1.0*1.0 + 1.0*1.0);
  coord[0] = (PetscReal)coor[0];
  coord[1] = (PetscReal)coor[1];
  
  evaluate_BatchelorCornerFlowSoln(alpha,origin,coord,veld);
  vel[0] = (PetscReal)veld[0];
  vel[1] = (PetscReal)veld[1];
  
  if (coor[1] >= -50.0/600.0) { /* crust */
    vel[0] = 0.0;
    vel[1] = 0.0;
  }
  if (coor[1] <= -coor[0]) { /* slab */
    vel[0] =  1.0 / vmag;
    vel[1] = -1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalTemperature_Case1A"
PetscErrorCode EvalTemperature_Case1A(PetscScalar coor[],PetscInt dof,
                                     PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_s = 273.0;
  PetscReal T_0 = 1573.0;
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  
  *constrain = PETSC_FALSE;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_s;
    
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_s + (T_0 - T_s) * erf( depth * 600.0*1.0e3 / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;

    PetscFunctionReturn(0);
  }

  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */

      T = T_s + (T_0 - T_s) * depth / plate_thickness;
      *constrain = PETSC_TRUE;
      *val = T;
      
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      PetscReal origin[2],alpha;
      double coor_d[2],vel_d[2];

      origin[0] = plate_thickness;
      origin[1] = -plate_thickness;
      alpha = atan2(-origin[1], origin[0]); // slab dip angle

      coor_d[0] = (double)coor[0];
      coor_d[1] = (double)coor[1];
      evaluate_BatchelorCornerFlowSoln(alpha,origin,coor_d,vel_d);
      
      if (vel_d[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val = T_0;
        PetscFunctionReturn(0);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_uv"
PetscErrorCode plate_uv(PetscScalar coor[],PetscInt dof,
                                               PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_TRUE;
  if (dof == 0) {
    *val = 0.0;
  } else if (dof == 1) {
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_boundary_base_uv"
PetscErrorCode plate_boundary_base_uv(PetscScalar coor[],PetscInt dof,
                                 PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal depth,plate_thickness;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  *constrain = PETSC_FALSE;
  
  if (depth < plate_thickness+1.0e-10) { /* bottom of plate boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_uv"
PetscErrorCode slab_uv(PetscScalar coor[],PetscInt dof,
                        PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vmag,mollifer;
  
  *constrain = PETSC_TRUE;
  vmag = PetscSqrtReal(1.0*1.0 + 1.0*1.0);

  mollifer = 1.0;
  if (coor[1] >= -8.3333333333e-02) { mollifer = 0.0; };
  
  if (dof == 0) {
    *val = mollifer * 1.0 / vmag;
  } else if (dof == 1) {
    *val = -mollifer * 1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_uv2"
PetscErrorCode slab_boundary_uv2(PetscScalar coor[],PetscInt dof,
                                PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2];
  PetscErrorCode ierr;
  
  PetscReal trench_coor[] = {8.3333333333e-02, -8.3333333333e-02};
  PetscReal trench_dist,dist_cutoff = 0.3;
  
  *constrain = PETSC_FALSE;
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] > 1.0) { /* bottom boundary */
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  if (coor[1] < -coor[0] + 1.0e-10) { /* slab */
    PetscReal vel_raw,vel_mollified,mollifer;
    
    trench_dist = (coor[0]-trench_coor[0])*(coor[0]-trench_coor[0]);
    trench_dist += (coor[1]-trench_coor[1])*(coor[1]-trench_coor[1]);
    trench_dist = PetscSqrtReal(trench_dist);
    
    vel_raw = vel[dof];
    mollifer = 0.5*(1.0-PetscCosReal(trench_dist*PETSC_PI/dist_cutoff));
    if (trench_dist >= dist_cutoff) {
      mollifer = 1.0;
    }
    vel_mollified = vel_raw * mollifer;
    //printf("slab %+1.10e %+1.10e [dof %d] vel %+1.4e | vel_moll %+1.4e \n",coor[0],coor[1],dof,vel_raw,vel_mollified);
    
    *constrain = PETSC_TRUE;
    *val = vel_mollified;
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "wedge_inflow"
PetscErrorCode wedge_inflow(PetscScalar coor[],PetscInt dof,
                                               PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2],depth,plate_thickness;
  PetscErrorCode ierr;
  PetscReal plate_coor[] = {1.0, -8.3333333333e-02};
  PetscReal plate_dist,dist_cutoff = 0.3;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  *constrain = PETSC_FALSE;
  
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth < plate_thickness) {
      if (dof == 0) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      } else if (dof == 1) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      }
    }
    if (depth > plate_thickness) { /* inflow from the mantle */
      PetscReal vel_raw,vel_mollified,mollifer;
      
      plate_dist = (coor[1]-plate_coor[1])*(coor[1]-plate_coor[1]);
      plate_dist = PetscSqrtReal(plate_dist);

      vel_raw = vel[dof];
      mollifer = 0.5*(1.0-PetscCosReal(plate_dist*PETSC_PI/dist_cutoff));
      if (plate_dist >= dist_cutoff) {
        mollifer = 1.0;
      }
      vel_mollified = vel_raw * mollifer;
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val  = vel_mollified;
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "rhs_fp_QuadratureEvaluator"
PetscErrorCode rhs_fp_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscReal val,*coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  val = 0.0;
  ierr = QuadratureSetValues(quadrature,1,&val,coeff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "rhs_fv_QuadratureEvaluator"
PetscErrorCode rhs_fv_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  MaterialProperties *mprop;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,delta_rho;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  dm = mprop->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nbasis,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  delta_rho = mprop->rho - mprop->rho_l;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      coeff[2*(e*nqp + q)+0] = mprop->phi * delta_rho * mprop->g[0] * mprop->fu_fac;
      coeff[2*(e*nqp + q)+1] = mprop->phi * delta_rho * mprop->g[1] * mprop->fu_fac;
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "coeff_QuadratureEvaluator"
PetscErrorCode coeff_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  MaterialProperties *mprop;
  PetscInt q,e,nen,nqp,*ridx;
  PetscReal *coeff,delta_rho;
  DM dm;
  PetscReal e3[2];
  
  PetscFunctionBegin;
  mprop = (MaterialProperties*)data;
  dm = mprop->dm;

  ierr = DMTetGeometryGetAttributes(dm,NULL,&ridx,NULL);CHKERRQ(ierr);

  /* shear viscosity */
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = mprop->eta;
    }
  }

  /* bulk viscosity */
  ierr = QuadratureGetProperty(quadrature,"zeta*",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureSetValues(quadrature,1,&mprop->zeta,coeff);CHKERRQ(ierr);
  
  /* effective permability */
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureSetValues(quadrature,1,&mprop->k,coeff);CHKERRQ(ierr);
  
  /* Darcy buouyancy */
  ierr = QuadratureGetProperty(quadrature,"e_3",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  delta_rho = mprop->rho - mprop->rho_l;
  e3[0] = mprop->fu_fac * ( delta_rho ) * mprop->g[0];
  e3[1] = mprop->fu_fac * ( delta_rho ) * mprop->g[1];
  ierr = QuadratureSetValues(quadrature,2,e3,coeff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

typedef struct {
  DM dm;
  Vec Xold;
} RHSContainer;

#undef __FUNCT__
#define __FUNCT__ "phi_RHS_Xold"
PetscErrorCode phi_RHS_Xold(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elX,**basis;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  Vec Xold;
  RHSContainer *data;
  const PetscScalar *LA_Xold;
  EContainer  space;
  
  PetscFunctionBegin;
  
  data = (RHSContainer*)user_context;
  dm   = data->dm;
  Xold = data->Xold;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&space);CHKERRQ(ierr);
  nbasis = space->nbasis;
  basis  = space->N;
  elX    = space->buf_basis_scalar_a;
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Xold,&LA_Xold);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elX[i] = LA_Xold[nidx];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal Xold_q;
      
      /* get x,y coords at each quadrature point */
      Xold_q = 0.0;
      for (i=0; i<nbasis; i++) {
        Xold_q += basis[q][i] * elX[i];
      }
      
      coeff[e*nqp + q] = Xold_q;
    }
  }
  ierr = VecRestoreArrayRead(Xold,&LA_Xold);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&space);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "phi_initial_condition"
PetscErrorCode phi_initial_condition(MaterialProperties properties,DM dm,Vec X,Vec divV)
{
  PetscErrorCode ierr;
  Vec coor;
  const PetscScalar *LA_coor;
  PetscInt i,bs,nnodes,nx,ny,mx,my;
  PetscScalar *LA_X,*LA_divV;
  PetscReal dx,dy,gmax[3],gmin[3],xrange[2],yrange[2];
  
  PetscFunctionBegin;

  ierr = DMDAGetInfo(dm,NULL,&nx,&ny,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMDAGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  xrange[0] = gmin[0];
  yrange[0] = gmin[1];
  xrange[1] = gmax[0];
  yrange[1] = gmax[1];
  mx = nx - 1;
  my = ny - 1;
  dx = (xrange[1] - xrange[0])/((PetscReal)mx);
  dy = (yrange[1] - yrange[0])/((PetscReal)my);

  
  ierr = DMGetCoordinates(dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetSize(X,&nnodes);CHKERRQ(ierr);
  ierr = VecGetBlockSize(X,&bs);CHKERRQ(ierr);
  nnodes = nnodes / bs;
  ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
  if (divV) {
    ierr = VecGetArray(divV,&LA_divV);CHKERRQ(ierr);
  }
  for (i=0; i<nnodes; i++) {
    PetscReal *xn,phi,rr,r_xy,xn0[2],G_xy;
    PetscInt b,nblobs = 7;
    PetscReal blob_coor[] = { 0.4,-0.2 , 0.6,-0.48 , 0.8,-0.2 , 0.9,-0.4 , 0.83,-0.6 , 0.6,-0.0833, 0.0833,-0.0833 };
    PetscReal phi_max = 0.01;
    //PetscInt b,nblobs = 1;
    //PetscReal blob_coor[] = { 0.8,-0.2 };
    
    xn = (PetscReal*)&LA_coor[2*i];

    phi = 0.0;

    /* gaussian */
    //xn0[0] =  0.5;
    //xn0[1] = -0.25;
    //rr = (xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]);
    //phi += 0.01 * exp(-200.0 * rr);
    
    
    /* conical body */
    //G_xy = 0.0;
    //xn0[0] =  0.4;
    //xn0[1] = -0.2;
    //rr = (xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]);
    //r_xy = (1.0/0.05) * PetscSqrtReal(rr);
    //if (r_xy < 1.0) {
    //  G_xy = 0.01 * (1.0 - r_xy);
    //}
    //phi += G_xy;
    
    //
    for (b=0; b<nblobs; b++) {
      G_xy = 0.0;
      
      xn0[0] = blob_coor[2*b+0];
      xn0[1] = blob_coor[2*b+1];
      
      rr = (xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]);
      r_xy = (1.0/0.05) * PetscSqrtReal(rr);
      if (r_xy < 1.0) {
        G_xy = phi_max * (1.0 - r_xy);
      }
      phi += G_xy;
    }
    //
    
    LA_X[i] = properties.phi + phi;
    LA_X[i] = 1.0 - (properties.phi + phi);
    
    /* if in the plate, or wedge set divV = 0, phi = 0 */
    if (xn[1] >= -8.3333333333e-02 - 10.0*dy) { // plate
      //LA_X[i] = 0.0;
      if (divV) { LA_divV[i] = 0.0; }
    }
    if (xn[1] <= -xn[0] + 10.0*dx) { // slab
      //LA_X[i] = 0.0;
      if (divV) { LA_divV[i] = 0.0; }
    }
  }
  if (divV) {
    ierr = VecRestoreArray(divV,&LA_divV);CHKERRQ(ierr);
  }
  ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "vanKeken_Case1B_TwoPhase"
PetscErrorCode vanKeken_Case1B_TwoPhase(void)
{
  PetscErrorCode ierr;
  PetscInt   p_T,p_V;
  PetscReal  gmin[3],gmax[3],xr[2],yr[2],kappa_nd,kappa;
  DM         dm_T,dm_V,dm_P,dm_fulldomain_V,dm_fulldomain_P,dm_VP;
  char       prefix[128];
  Vec        X_T,Xlocal_T,F_T,Xsample,velocity,X_VP,F_VP;
  Mat        Jac_T,Jac_VP;
  PDE        pde_T,pde_VP;
  BCList     bc_T,bc_V;
  Mat        interp_tet2dasample;
  SNES       snes_T,snes_VP;
  PetscReal  L,V,T,E;
  DM         dmdasample;
  PetscInt   region_plate,region_slab,region_wedge;
  const char meshfilename[] = "examples/reference_meshes/subduction-1a/mesh-r1/wedge.1";
  //const char meshfilename[] = "examples/reference_meshes/subduction-1a/mesh-r2/wedge.2"; /* This mesh file needs to be generated - please refer to readme and scripts in the directory examples/reference_meshes/subduction-1a/mesh-r2 */
  MaterialProperties properties;
  
  /* set input options here */
  p_T = 2; // polynomial for velocity and temperature. Pressure is taken as p_T - 1
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&p_T,NULL);CHKERRQ(ierr);
  
  properties.eta    = 1.0e21;
  properties.phi    = 1.0e-6;
  properties.rho    = 3300.0;
  properties.rho_l  = 2800.0;
  properties.zeta   = 1.0e22;
  properties.k      = 1.0e-12; // m^2
  properties.g[0]   = 0.0;
  properties.g[1]   = 9.81;
  properties.dm     = NULL;
  
  ierr = PetscOptionsGetReal(NULL,NULL,"-eta",&properties.eta,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-phi",&properties.phi,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-zeta",&properties.zeta,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-k",&properties.k,NULL);CHKERRQ(ierr);

  
  /* Length, velocity and time scales used: x = L x' */
  E = 1.0e21; /* viscosity scale */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  T = L/V; /* time scale */
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  kappa_nd = kappa * T / (L*L);
  
  properties.eta  = properties.eta / E;
  properties.zeta = properties.zeta / E;
  properties.k    = properties.k * E / (L * L);
  properties.fu_fac = (L*L)/(V*E);

  
  /* Create temperature mesh */
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,p_T,&dm_T);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(dm_T,gmin,gmax);CHKERRQ(ierr);
  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  
  ierr = DMCreateGlobalVector(dm_T,&X_T);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm_T,&Xlocal_T);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_T,&F_T);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dm_T,&Jac_T);CHKERRQ(ierr);
  ierr = MatSetOption(Jac_T,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  /* Define initial condition for T */
  ierr = VecSet(X_T,273.0);CHKERRQ(ierr);
  
  /* Define Dirichlet boundary condition for temperature */
  ierr = DMBCListCreate(dm_T,&bc_T);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc_T,0,EvalTemperature_Case1A,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc_T,X_T);CHKERRQ(ierr);
  
  /* View temperature initial condition */
  {
    const char *fieldsT[] = { "T" };
    
    ierr = PetscSNPrintf(prefix,127,"energy_%sic_",fieldsT[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_T,1,&X_T,fieldsT,prefix);CHKERRQ(ierr);
  }
  
  /* Create mesh for velocity and pressure (needed by two-phase formulation) */
  p_V = p_T;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   2,DMTET_CWARP_AND_BLEND,p_V,&dm_fulldomain_V);CHKERRQ(ierr);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,p_V-1,&dm_fulldomain_P);CHKERRQ(ierr);
  
  /* Extract a sub velocity-pressure meshes from the wedge domain */
  {
    PetscInt nregions = 1;
    PetscInt regionlist[] = {2};
    
    ierr = DMTetCreateSubmeshByRegions(dm_fulldomain_V,nregions,regionlist,&dm_V);CHKERRQ(ierr);
    ierr = DMTetCreateSubmeshByRegions(dm_fulldomain_P,nregions,regionlist,&dm_P);CHKERRQ(ierr);

    properties.dm = dm_P; /* Attatch dm_P for potentially interpolating coords */
  }
  
  /* Define velocity Dirichlet boundary conditions on the sub mesh */
  ierr = DMBCListCreate(dm_V,&bc_V);CHKERRQ(ierr);
  
  region_plate = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_plate,0,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_plate,1,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  
  region_slab = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,0,slab_boundary_uv2,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,1,slab_boundary_uv2,NULL);CHKERRQ(ierr);
  
  region_wedge = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_wedge,0,wedge_inflow,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_wedge,1,wedge_inflow,NULL);CHKERRQ(ierr);
  
  /* Define velocity field */
  ierr = DMCreateGlobalVector(dm_fulldomain_V,&velocity);CHKERRQ(ierr);
  
  /* Create a profiling DMDA */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,111,101,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dmdasample);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dmdasample,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmdasample,&Xsample);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dm_T,dmdasample,&interp_tet2dasample,NULL);CHKERRQ(ierr);
  
  /* Create the PDE for the thermal problem */
  ierr = PDECreate(PETSC_COMM_WORLD,&pde_T);CHKERRQ(ierr);
  ierr = PDESetType(pde_T,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde_T,X_T);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde_T,Xlocal_T);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde_T,dm_T,bc_T);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetVelocity(pde_T,dm_fulldomain_V,velocity);CHKERRQ(ierr);
  
  /* Define coefficients for the thermal problem */
  {
    Coefficient k,alpha,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde_T,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,kappa_nd);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_alpha(pde_T,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_g(pde_T,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadratureNULL(g);CHKERRQ(ierr);
  }
  
  /* Create the PDE for the two-phase flow problem */
  //ierr = PDECreateStokes(PETSC_COMM_WORLD,dm_V,dm_P,bc_V,&pde_VP);CHKERRQ(ierr);
  ierr = PDECreate(PETSC_COMM_WORLD,&pde_VP);CHKERRQ(ierr);
  ierr = PDESetType(pde_VP,PDESTKDCY2F);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde_VP,"stkdcy_");CHKERRQ(ierr);
  ierr = PDEStokesDarcy2FieldSetData(pde_VP,dm_V,dm_P,bc_V,NULL);CHKERRQ(ierr);
  
  ierr = PDEStokesDarcy2FieldGetDM(pde_VP,&dm_VP);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dm_VP,&Jac_VP);CHKERRQ(ierr);
  ierr = MatCreateVecs(Jac_VP,&X_VP,&F_VP);CHKERRQ(ierr);
  ierr = PDESetSolution(pde_VP,X_VP);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dm_V,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dm_P,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde_VP,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* Insert velocity boundary condition into the solution vector */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bc_V,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  /* Define coefficients for the two-phase flow problem 
   - note that kappa*, zeta*, e3 are contained "inside" the variable coeff
  */
  {
    Coefficient coeff,rhs_fu,rhs_fp;
    
    ierr = PDEStokesGetCoefficients(pde_VP,&coeff,&rhs_fu,&rhs_fp);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(coeff,   coeff_QuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(rhs_fu, rhs_fv_QuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(rhs_fp, rhs_fp_QuadratureEvaluator,NULL,(void*)&properties);CHKERRQ(ierr);
    
    ierr = CoefficientEvaluate(coeff);CHKERRQ(ierr);
    ierr = CoefficientView_GP(coeff,dm_P,"coeff");CHKERRQ(ierr);
    
  }
  
  /* Create and configure the two-phase flow solver */
  ierr = PDESetOptionsPrefix(pde_VP,"vp_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes_VP);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_VP,snes_VP,F_VP,Jac_VP,Jac_VP);CHKERRQ(ierr);
  ierr = SNESSetType(snes_VP,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes_VP);CHKERRQ(ierr);
  
  /* Create and configure the thermal solver */
  ierr = PDESetOptionsPrefix(pde_T,"t_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes_T);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_T,snes_T,F_T,Jac_T,Jac_T);CHKERRQ(ierr);
  ierr = SNESSetType(snes_T,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes_T);CHKERRQ(ierr);
  
  /* Solve two-phase flow problem for v, p */
  ierr = PDESNESSolve(pde_VP,snes_VP);CHKERRQ(ierr);
  
  /* View v, p */
  ierr = PDEStokesFieldView_AsciiVTU(pde_VP,NULL);CHKERRQ(ierr);
  
  /* Compute and view the fluid velocity */
  {
    const char *field[] = { "uf" };
    Vec uf = NULL;
    Coefficient coeff,rhs_fu,rhs_fp;
    PetscReal *coeff_data;
    PetscInt nen,nqp,e,q;
    Quadrature quadrature;
    
    ierr = PDEStokesGetCoefficients(pde_VP,&coeff,&rhs_fu,&rhs_fp);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(coeff,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature,"k*",&nen,&nqp,NULL,&coeff_data);CHKERRQ(ierr);
    for (e=0; e<nen; e++) {
      for (q=0; q<nqp; q++) {
        coeff_data[e*nqp + q] = coeff_data[e*nqp + q] / properties.phi;
      }
    }
    
    ierr = PDEStokesDarcy2FieldComputeFluidVelocityP(pde_VP,&uf);CHKERRQ(ierr);
    ierr = DMTetSetDof(dm_P,2);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_P,1,&uf,field,"stkdcy_uf_");CHKERRQ(ierr);
    ierr = DMTetSetDof(dm_P,1);CHKERRQ(ierr);
    
    ierr = VecDestroy(&uf);CHKERRQ(ierr);

    for (e=0; e<nen; e++) {
      for (q=0; q<nqp; q++) {
        coeff_data[e*nqp + q] = coeff_data[e*nqp + q] * properties.phi;
      }
    }
  
  }
  
  {
    BCList      bc_fulldomain_V;
    PetscInt    regionindex;
    const char  *fieldsV[] = { "V" };
    Mat         inject_sub2g;
    VecScatter  scat;
    Vec         Xv,Xp;
    
    ierr = DMBCListCreate(dm_fulldomain_V,&bc_fulldomain_V);CHKERRQ(ierr);

    regionindex = 3;
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,0,plate_uv,NULL);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,1,plate_uv,NULL);CHKERRQ(ierr);

    regionindex = 1;
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,0,slab_uv,NULL);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bc_fulldomain_V,regionindex,1,slab_uv,NULL);CHKERRQ(ierr);

    ierr = BCListInsert(bc_fulldomain_V,velocity);CHKERRQ(ierr);

    /* Inject wedge velocity into full domain */
    ierr = DMCreateInjection(dm_fulldomain_V,dm_V,&inject_sub2g);CHKERRQ(ierr);
    ierr = MatScatterGetVecScatter(inject_sub2g,&scat);CHKERRQ(ierr);

    ierr = DMCompositeGetAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,Xv,velocity,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,Xv,velocity,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    
    /* View the velocity field on the full domain */
    ierr = PetscSNPrintf(prefix,127,"stkdcy_v_fulldomain_");CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_fulldomain_V,1,&velocity,fieldsV,prefix);CHKERRQ(ierr);

    ierr = BCListDestroy(&bc_fulldomain_V);CHKERRQ(ierr);
    ierr = MatDestroy(&inject_sub2g);CHKERRQ(ierr);
  }
  
  /* Solve thermal problem for T */
  ierr = PDESNESSolve(pde_T,snes_T);CHKERRQ(ierr);
  {
    const char *fieldsT[] = { "T" };
    
    ierr = PetscSNPrintf(prefix,127,"energy_%s_",fieldsT[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_T,1,&X_T,fieldsT,prefix);CHKERRQ(ierr);
  }
  
#if 0
  /* check projection */
  {
    Vec proj_V = NULL;
    Vec proj_gradP = NULL;
    Vec proj_divV = NULL;
    Vec proj_gradV = NULL;
    char *fieldlist[10];
    Vec Xv,Xp;
    
    // v //
    ierr = DMTetProjectField(dm_fulldomain_V,velocity,PRJTYPE_NATIVE,dm_fulldomain_P,&proj_V);CHKERRQ(ierr);

    char fieldA[] = "proj_v";
    fieldlist[0] = fieldA;
    ierr = PetscSNPrintf(prefix,127,"%s_",fieldlist[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_fulldomain_P,1,&proj_V,(const char**)fieldlist,prefix);CHKERRQ(ierr);
    ierr = VecDestroy(&proj_V);CHKERRQ(ierr);
    
    ierr = DMCompositeGetAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
    
    // grad(p) //
    ierr = DMTetProjectField(dm_P,Xp,PRJTYPE_GRADIENT,dm_P,&proj_gradP);CHKERRQ(ierr);

    char fieldB[] = "proj_gradp";
    fieldlist[0] = fieldB;
    ierr = PetscSNPrintf(prefix,127,"%s_",fieldlist[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_P,1,&proj_gradP,(const char**)fieldlist,prefix);CHKERRQ(ierr);
    ierr = VecDestroy(&proj_gradP);CHKERRQ(ierr);

    // div(u) //
    ierr = DMTetProjectField(dm_V,Xv,PRJTYPE_DIVERGENCE,dm_P,&proj_divV);CHKERRQ(ierr);

    char fieldC[] = "proj_divv";
    fieldlist[0] = fieldC;
    ierr = PetscSNPrintf(prefix,127,"%s_",fieldlist[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_P,1,&proj_divV,(const char**)fieldlist,prefix);CHKERRQ(ierr);
    ierr = VecDestroy(&proj_divV);CHKERRQ(ierr);
    
    // grad(u) //
    ierr = DMTetProjectField(dm_V,Xv,PRJTYPE_GRADIENT,dm_P,&proj_gradV);CHKERRQ(ierr);
    
    char fieldD[] = "proj_gradv";
    fieldlist[0] = fieldD;
    ierr = PetscSNPrintf(prefix,127,"%s_",fieldlist[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_P,1,&proj_gradV,(const char**)fieldlist,prefix);CHKERRQ(ierr);
    ierr = VecDestroy(&proj_gradV);CHKERRQ(ierr);
    
    ierr = DMCompositeRestoreAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);
  }
#endif
  
  /* advect the porosity */
  PetscInt M,N;
  PetscReal dt,time;
  PetscInt k,nsteps=1,ionsteps=1;
  DMSemiLagrangian dsl;
  Vec velocity_sl,Xsl_phi,Fsl_phi,proj_divV=NULL,div_velocity_sl,Xv,Xp;
  Mat interp_tet2da,interp_tet2da_vel,interp_tetp12da;
  DM dmSL,dmvSL;
  
  PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nsteps",&nsteps,NULL);
  PetscOptionsGetInt(NULL,NULL,"-ionsteps",&ionsteps,NULL);
  
  /* Semi-Lagrangian mesh */
  M = 332; ierr = PetscOptionsGetInt(NULL,NULL,"-M",&M,NULL);CHKERRQ(ierr);
  N = 302; ierr = PetscOptionsGetInt(NULL,NULL,"-N",&N,NULL);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dm_T,gmin,gmax);CHKERRQ(ierr);
  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  dsl->debug = 0;
  //dsl->dactx->interpolation_type = DASLInterp_Q1;

  ierr = DMDASemiLagrangianSetType(dsl,SLTYPE_FVCONSERVATIVE);CHKERRQ(ierr);

  ierr = DMDASemiLagrangianGetVertexDM(dsl,&dmvSL);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianGetSLDM(dsl,&dmSL);CHKERRQ(ierr);
  ierr = DMDASetFieldName(dmSL,0,"phi");CHKERRQ(ierr);
  
  /* Define velocity field directly on the DMDA used by SL */
  /* interpolate from dm_fulldomain */
  ierr = DMCreateInterpolation(dm_T,dmvSL,&interp_tet2da,NULL);CHKERRQ(ierr);
  ierr = MatCreateMAIJ(interp_tet2da,2,&interp_tet2da_vel);CHKERRQ(ierr);
  velocity_sl = dsl->velocity;
  ierr = MatMult(interp_tet2da_vel,velocity,velocity_sl);CHKERRQ(ierr);

  //ierr = DMCreateGlobalVector(dmSL,&Xsl_phi);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl,&Xsl_phi);CHKERRQ(ierr);
  
  /* RHS for evolution equation */
  //ierr = DMCreateGlobalVector(dmSL,&Fsl_phi);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl,&Fsl_phi);CHKERRQ(ierr);

  // div(u) //
  /*
  ierr = VecDuplicate(Xsl_phi,&div_velocity_sl);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm_fulldomain_V,velocity,PRJTYPE_DIVERGENCE,dm_fulldomain_P,&proj_divV);CHKERRQ(ierr);
  ierr = DMCreateInterpolation(dm_fulldomain_P,dmSL,&interp_tetp12da,NULL);CHKERRQ(ierr);
  ierr = MatMult(interp_tetp12da,proj_divV,div_velocity_sl);CHKERRQ(ierr);
  */

  /* interpolate in sub-domain */
  ierr = DMCompositeGetAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);

  ierr = DMTetProjectField(dm_V,Xv,PRJTYPE_DIVERGENCE,dm_P,&proj_divV);CHKERRQ(ierr);
  interp_tetp12da = NULL;
  div_velocity_sl = NULL;
  ierr = VecDuplicate(Xsl_phi,&div_velocity_sl);CHKERRQ(ierr);
  //ierr = DMCreateInterpolation(dm_P,dmSL,&interp_tetp12da,NULL);CHKERRQ(ierr);
  //ierr = MatMult(interp_tetp12da,proj_divV,div_velocity_sl);CHKERRQ(ierr);

  ierr = DMCompositeRestoreAccess(dm_VP,X_VP,&Xv,&Xp);CHKERRQ(ierr);

  ierr = phi_initial_condition(properties,dmSL,Xsl_phi,div_velocity_sl);CHKERRQ(ierr);

  ierr = DMSLSetup(dsl);CHKERRQ(ierr);

  {
    PetscViewer viewer;
    DM cdm;
    
    ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(viewer,"trans_phi_step0.vtr");CHKERRQ(ierr);
    ierr = VecView(Xsl_phi,viewer);CHKERRQ(ierr);
    if (div_velocity_sl) {
      //ierr = VecView(div_velocity_sl,viewer);CHKERRQ(ierr); /* this leaks memory - comment it out */
    }
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    
    ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(viewer,"trans_vic.vtr");CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(dmSL,&cdm);CHKERRQ(ierr);
    ierr = VecView(velocity_sl,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  
  time = dt;

  for (k=1; k<=nsteps; k++) {
    
    PetscPrintf(PETSC_COMM_WORLD,"=== Step %D ===\n",k);
    
    //SL-CLASSIC ierr = VecSet(Fsl_phi,1.0);CHKERRQ(ierr);
    //SL-CLASSIC ierr = VecAXPY(Fsl_phi,-1.0,Xsl_phi);CHKERRQ(ierr); /* 1.0-phi */
    //SL-CLASSIC ierr = VecPointwiseMult(Fsl_phi,Fsl_phi,div_velocity_sl);CHKERRQ(ierr); /* F = (1-phi^k) div(v) **/
    
    //SL-CLASSIC//ierr = DMSemiLagrangianUpdate_DMDA(dsl,dt,Xsl_phi);CHKERRQ(ierr);
    //ierr = DMFVConservativeSemiLagrangianUpdate_DMDA(dsl,dt,Xsl_phi);CHKERRQ(ierr); // <depreciated> //
    //ierr = DMFVConservativeSemiLagrangianUpdate_v2(dsl,dt,Xsl_phi);CHKERRQ(ierr);
    ierr = DMDASemiLagrangianUpdate(dsl,(k-1)*dt,dt,Xsl_phi);CHKERRQ(ierr);
    
    //SL-CLASSIC ierr = VecAXPY(Xsl_phi,dt,Fsl_phi);CHKERRQ(ierr); /* phi^{k+1} += dt (1-phi^k) div(vs) */

    /*
    {
      PetscScalar *LA_field;
      PetscScalar min=0.0,max=2.0e-2;
      PetscInt nn,i;
      ierr = VecGetSize(Xsl_phi,&nn);CHKERRQ(ierr);
      ierr = VecGetArray(Xsl_phi,&LA_field);CHKERRQ(ierr);
      for (i=0; i<nn; i++) {
        if (LA_field[i] < min) { LA_field[i] = min; }
        if (LA_field[i] > max) { LA_field[i] = max; }
      }
      ierr = VecRestoreArray(Xsl_phi,&LA_field);CHKERRQ(ierr);
    }
    */
     
    if (k%ionsteps == 0) {
      PetscViewer viewer;
      
      PetscSNPrintf(prefix,127,"trans_phi_step%D.vtr",k);
      
      ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,prefix);CHKERRQ(ierr);
      ierr = VecView(Xsl_phi,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    }
    time = time + dt;
  }

  ierr = MatDestroy(&interp_tetp12da);CHKERRQ(ierr);
  ierr = VecDestroy(&div_velocity_sl);CHKERRQ(ierr);
  ierr = VecDestroy(&proj_divV);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsl_phi);CHKERRQ(ierr);
  ierr = VecDestroy(&Fsl_phi);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2da_vel);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2da);CHKERRQ(ierr);
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  
  
  /* clean up */
  ierr = PDEDestroy(&pde_T);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde_VP);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes_T);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes_VP);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_T);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_V);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal_T);CHKERRQ(ierr);
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = VecDestroy(&X_T);CHKERRQ(ierr);
  ierr = VecDestroy(&F_T);CHKERRQ(ierr);
  ierr = VecDestroy(&X_VP);CHKERRQ(ierr);
  ierr = VecDestroy(&F_VP);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsample);CHKERRQ(ierr);
  ierr = MatDestroy(&Jac_VP);CHKERRQ(ierr);
  ierr = MatDestroy(&Jac_T);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2dasample);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_fulldomain_V);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_fulldomain_P);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_V);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_P);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_T);CHKERRQ(ierr);
  ierr = DMDestroy(&dmdasample);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 ./${PETSC_ARCH}/bin/vk-case1b-advsl-twophase.app -options_file src/tests/vankeken-twophase/case1b.opts  -t_ksp_monitor -proj_ksp_monitor -proj_ksp_type fgmres -t_ksp_type fgmres -vp_ksp_rtol 1.0e-12  -dt 0.01 -nsteps 500 -ionsteps 50 -M 256 -N 256
*/
int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = vanKeken_Case1B_TwoPhase();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

