
#include <petsc.h>
#include <petscdm.h>
#include <cslpoly.h>
#include <proj_init_finalize.h>

PetscErrorCode box_polygon(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 4;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);

  coor[2*0+0] = 0.0;       coor[2*0+1] = 0.0;    edge[0] = 0;
  coor[2*1+0] = 1.0;       coor[2*1+1] = 0.0;    edge[1] = 1;
  coor[2*2+0] = 1.0;       coor[2*2+1] = 1.0;    edge[2] = 2;
  coor[2*3+0] = 0.0;       coor[2*3+1] = 1.0;    edge[3] = 3;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}

PetscErrorCode triangle_polygon(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 3;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);
  
  coor[2*0+0] = 0.1;        coor[2*0+1] = 0.23;    edge[0] = 0;
  coor[2*1+0] = 0.85;       coor[2*1+1] = 0.23;    edge[1] = 1;
  coor[2*2+0] = 0.25;       coor[2*2+1] = 0.8;     edge[2] = 2;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}

PetscErrorCode wedge_polygon(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 4;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);
  
  coor[2*0+0] = 0.3;       coor[2*0+1] = 0.33;    edge[0] = 0;
  coor[2*1+0] = 1.0;       coor[2*1+1] = 0.33;    edge[1] = 1;
  coor[2*2+0] = 1.0;       coor[2*2+1] = 0.8;     edge[2] = 2;
  coor[2*3+0] = 0.1;       coor[2*3+1] = 0.8;     edge[3] = 3;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}

void dirichlet_evaluator_cylinder(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.5;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  value[0] = G_xy;
}

void dirichlet_evaluator_cylinderSmall(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.4;
  xn0[1] = 0.4;
  G_xy = 0.0;
  r_xy = (1.0/0.07)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  value[0] = G_xy;
}

void dirichlet_evaluator_cylinder_tri_left(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.2;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.07)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 2.0) {
    G_xy = 1.0;
  }
  value[0] = G_xy;
}

void dirichlet_evaluator_cylinder_tri_right(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.6;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.07)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 2.0) {
    G_xy = 1.0;
  }
  value[0] = G_xy;
}

void dirichlet_evaluator_cylinderLeft(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.0;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  value[0] = G_xy;
}

void dirichlet_evaluator_3blobs(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  G_xy = 0.0;
  
  /* slotted cylinder */
  xn0[0] = 0.5;
  xn0[1] = 0.5;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    //if ((fabs(xn[0] - xn0[0]) >= 0.025) || (xn[1] >= 0.85)) {
      G_xy = 1.0;
    //}
  }
  
  /* conical body */
  xn0[0] = 0.5;
  xn0[1] = 0.25;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  /* smooth peak */
  xn0[0] = 0.25;
  xn0[1] = 0.5;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 0.25*(1.0 + cos(M_PI*r_xy));
  }

  value[0] = G_xy;
}

void dirichlet_evaluator_4blobs(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  G_xy = 0.0;
  
  /* conical body */
  xn0[0] = 0.25;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  xn0[0] = 0.4;
  xn0[1] = 0.38;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }

  xn0[0] = 0.15;
  xn0[1] = 0.65;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }

  xn0[0] = 0.7;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }

  value[0] = G_xy;
}

void dirichlet_evaluator_4blobs_t(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal time;
  PetscReal G_xy,r_xy,xn0[2],v[2];
  
  time  = *((PetscReal*)ctx);
  G_xy = 0.0;
  
  /* conical body */
  xn0[0] = 0.25;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  xn0[0] = 0.4;
  xn0[1] = 0.38;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  /* make this one time dependent - it will move along the line y = 1-2.2x */
  v[0] = 1.0/2.2;
  v[1] = -1.0;
  xn0[0] = 0.15 + (v[0] * 0.3) * time;
  xn0[1] = 0.65 + (v[1] * 0.3) * time;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  xn0[0] = 0.7;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  value[0] = G_xy;
}

PetscErrorCode vel_evaluator_down(PetscReal pos[],PetscReal value[])
{
  value[0] = -0.15;
  value[1] = -0.3;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_right(PetscReal pos[],PetscReal value[])
{
  value[0] = 1.0;
  value[1] = 0.0;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_left(PetscReal pos[],PetscReal value[])
{
  value[0] = -1.0;
  value[1] = 0.0;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_rigidRotation(PetscReal pos[],PetscReal value[])
{
  // solid body rotation about origin
  value[0] = -(pos[1]-0.5);
  value[1] =  (pos[0]-0.5);
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_45Deg(PetscReal pos[],PetscReal value[])
{
  // 45 degree flow
  value[0] = 0.5;
  value[1] = 0.5;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_cornerFlow(PetscReal pos[],PetscReal value[])
{
  // corner flow
  value[0] =  pos[0];
  value[1] = -pos[1];
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_source(PetscReal pos[],PetscReal value[])
{
  // source
  value[0] = 0.3*(pos[0] - 0.5);
  value[1] = 0.3*(pos[1] - 0.5);
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_liquidFlow(PetscReal pos[],PetscReal value[])
{
  PetscReal pp[2],vxr[2],cc[2],sink[2];
  
  pp[0] = 10.0*(pos[0]-0.5);
  pp[1] = 10.0*(pos[1]-0.5);

  /*
  value[0] = 1.0e-2*(1.0 - 11.0*pp[1] - 7.0*pp[0]);
  value[1] = 1.0e-2*(14.0 + 2.0*pp[0]*pp[0]);
  */
  vxr[0] = 1.0e-2*(1.0 - 11.0*pp[1] - 7.0*pp[0]);
  vxr[1] = 1.0e-2*(14.0 + 2.0*pp[0]*pp[0]);
  
  cc[0] = (pos[0] - 0.6);
  cc[1] = (pos[1] - 0.7);
  sink[0] = -(pos[0] - 0.6);
  sink[1] = -(pos[1] - 0.7);
  if (PetscSqrtReal(sink[0]*sink[0] + sink[1]*sink[1]) > 0.3) {
    sink[0] = 0.3;
    sink[1] = 0.3;
  }
  
  value[0] = 1.0*vxr[0] + 8.0*sink[0] * (exp(-100.0*(cc[0]*cc[0]+cc[1]*cc[1])));
  value[1] = 1.0*vxr[1] + 8.0*sink[1] * (exp(-100.0*(cc[0]*cc[0]+cc[1]*cc[1])));
  PetscFunctionReturn(0);
}

void bc_dirichlet(CSLPoly p,PetscReal time,void *ctx)
{
  PetscInt pe,edge;
  void (*my_eval)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  
  my_eval = (void (*)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*))ctx;
  
  for (pe=0; pe<p->poly_npoints; pe++) {
    edge = p->poly_edge_label[pe];
    CSLPolyGenericFacetBCIterator(p,edge,my_eval,(void*)&time);
  }
}


#undef __FUNCT__
#define __FUNCT__ "example2"
PetscErrorCode example2(void)
{
  CSLPoly dsl;
  PetscInt i,k,c;
  PetscErrorCode (*create_polygon)(PetscInt*,PetscReal**,PetscInt**);
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt model;
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time,dt;
  PetscReal ds = 1.0/128.0;
  PetscInt nt = 120;
  PetscInt nout = 2;
  PetscErrorCode ierr;

  model = 0;
  PetscOptionsGetInt(NULL,NULL,"-model",&model,NULL);
  PetscOptionsGetReal(NULL,NULL,"-ds",&ds,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  
  switch (model) {
    case 0:
      create_polygon = box_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 1:
      create_polygon = box_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinderLeft;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 12:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unknown model [12]");
      break;

    case 2:
      create_polygon = box_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_cornerFlow;
      break;
    case 3:
      create_polygon = box_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinderLeft;
      vel_evaluator = vel_evaluator_cornerFlow;
      break;
      
    case 4:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 5:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 6:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_rigidRotation;
      break;

    case 7:
      create_polygon = wedge_polygon;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 11:
      create_polygon = wedge_polygon;
      dirichlet_evaluator = dirichlet_evaluator_4blobs;
      vel_evaluator = vel_evaluator_liquidFlow;
      break;
    case 13:
      create_polygon = wedge_polygon;
      dirichlet_evaluator = dirichlet_evaluator_4blobs_t;
      vel_evaluator = vel_evaluator_liquidFlow;
      break;

    case 8:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_source;
      break;
    case 9:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinderSmall;
      vel_evaluator = vel_evaluator_source;
      break;
    case 10:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinderSmall;
      vel_evaluator = vel_evaluator_down;
      break;

    case 14:
      create_polygon = triangle_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinder_tri_left;
      vel_evaluator = vel_evaluator_right;
      
      dirichlet_evaluator = dirichlet_evaluator_cylinder_tri_right;
      vel_evaluator = vel_evaluator_left;
      break;
      

    default:
      create_polygon = box_polygon;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
  }

  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  //CSLPolySetInterpolateType(sl,I_CSPLINE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(dsl,I_WENO3);
  CSLPolySetSubcellResolution(dsl,2,2);
  CSLPolySetDefaultDivergenceMethod(dsl);
  
  ierr = create_polygon(&npoly,&polycoor,&edgelabel);
  CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
  CSLPolySetDomainDefault(dsl,ds);

  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  time = 0.0;
  /* set initial velocity field */
  for (i=0; i<dsl->nvert; i++) {
    PetscReal *pos = &dsl->coor_v[2*i];
    ierr = vel_evaluator(pos,&dsl->vel_v[2*i]);CHKERRQ(ierr);
  }
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;

    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;
  }

  /* setup - time dependent */
  
  CSLPolyComputeCellAverageDivVelocity(dsl);
  
  CSLPolyMarkInflowFacets(dsl);
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");

  dt = 0.2*(2.0*M_PI/99.0);
  time = dt;
  for (k=1; k<nt; k++) {
    char filename[128];
    
    // change velocity
    //CSLPolyMarkInflowFacets(dsl);
    
    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    
    CSLPolyReconstructExteriorCellFields(dsl);
    CSLPolyReconstructExteriorFacetFields(dsl);
    
    //CSLPolyUpdate(dsl,time,dt);
    //CSLPolyUpdate_Conservative(dsl,time,dt);
    CSLPolyUpdate_Strang2Multiplicative(dsl,time,dt);
    
    CSLPolyReconstructExteriorFacetFields(dsl);

    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    if (k%10 == 0) {
      PetscPrintf(PETSC_COMM_WORLD,"[step %4D] time = %1.2e\n",k,time);
    }

    time += dt;
  }
  
  PetscFunctionReturn(0);
}

typedef struct {
  const PetscReal *default_fields;
} ImplicitFieldCtx;

/*
 Define G(c,x)
   G_0(c,x) = c[0] * x^2 + y^1
   G_1(c,x) = c[0] * x^3 + y^2
*/
void ifields_evaluate_at_point(CSLPoly p,PetscReal coor[],PetscReal ivalues[],void *data)
{
  /*ImplicitFieldCtx *ctx = (ImplicitFieldCtx*)data;*/
  /*const PetscReal *value_c = ctx->default_fields;*/
  PetscReal c[CSLPOLY_MAX_FIELDS];
  
  CSLPolyInterpolateAtPoint(p,coor,c);
  ivalues[0] = c[0] * coor[0] * coor[0] + coor[1];
  ivalues[1] = c[0] * coor[0] * coor[0] * coor[0] + coor[1] * coor[1];
}

void ifields_evaluate_at_boundarypoint(CSLPoly p,PetscInt facet_intersected,PetscReal coor[],PetscReal ivalues[],void *data)
{
  /*ImplicitFieldCtx *ctx = (ImplicitFieldCtx*)data;*/
  /*const PetscReal *value_c = ctx->default_fields;*/
  PetscReal c[CSLPOLY_MAX_FIELDS];
  
  CSLPolyInterpolateAtBoundaryPoint(p,facet_intersected,coor,c);
  ivalues[0] = c[0] * coor[0] * coor[0] + coor[1];
  ivalues[1] = c[0] * coor[0] * coor[0] * coor[0] + coor[1] * coor[1];
}


void dirichlet_evaluator_cylinder_i(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal ivalues[],void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.5;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  ivalues[0] = G_xy * xn[0] * xn[0] + xn[1];
  ivalues[1] = G_xy * xn[0] * xn[0] * xn[0] + xn[1] * xn[1];
}

void bc_dirichlet_i(CSLPoly p,PetscReal time,void *ctx)
{
  PetscInt pe,edge;
  void (*my_eval)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  
  my_eval = (void (*)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*))ctx;
  
  for (pe=0; pe<p->poly_npoints; pe++) {
    edge = p->poly_edge_label[pe];
    CSLPolyGenericFacetBCIteratorI(p,edge,my_eval,(void*)&time);
  }
}


#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(void)
{
  CSLPoly dsl;
  PetscInt i,k,c;
  PetscErrorCode (*create_polygon)(PetscInt*,PetscReal**,PetscInt**);
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  void (*dirichlet_evaluator_i)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time,dt;
  PetscReal ds = 1.0/128.0;
  PetscInt nt = 120;
  PetscInt nout = 2;
  ImplicitFieldCtx *ctx;
  PetscErrorCode ierr;
  
  PetscOptionsGetReal(NULL,NULL,"-ds",&ds,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  
    create_polygon = box_polygon;
    dirichlet_evaluator = dirichlet_evaluator_cylinder;
    dirichlet_evaluator_i = dirichlet_evaluator_cylinder_i;
    vel_evaluator = vel_evaluator_45Deg;

  
  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  
  CSLPolySetBlockSizeImplicit(dsl,2);
  PetscMalloc1(1,&ctx);
  CSLPolySetImplicitFieldEvaluators(dsl,ifields_evaluate_at_point,ifields_evaluate_at_boundarypoint,ctx);
  
  //CSLPolySetInterpolateType(sl,I_CSPLINE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(sl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(dsl,I_WENO4);
  CSLPolySetSubcellResolution(dsl,2,2);
  CSLPolySetDefaultDivergenceMethod(dsl);
  
  ierr = create_polygon(&npoly,&polycoor,&edgelabel);
  CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
  CSLPolySetDomainDefault(dsl,ds);
  
  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  ctx->default_fields = (const PetscReal*)dsl->value_c;
  
  time = 0.0;
  /* set initial velocity field */
  for (i=0; i<dsl->nvert; i++) {
    PetscReal *pos = &dsl->coor_v[2*i];
    ierr = vel_evaluator(pos,&dsl->vel_v[2*i]);CHKERRQ(ierr);
  }
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;
    PetscReal iG_xy[] = { 0.0, 0.0 };
    
    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
      dirichlet_evaluator_i((const PetscReal*)pos,NULL,NULL,iG_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;

    dsl->ivalue_c[2*c+0] = iG_xy[0];
    dsl->ivalue_c[2*c+1] = iG_xy[1];
}
  
  /* setup - time dependent */
  
  CSLPolyComputeCellAverageDivVelocity(dsl);
  
  CSLPolyMarkInflowFacets(dsl);
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet_i,(void*)dirichlet_evaluator_i);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");
  
  dt = 0.2*(2.0*M_PI/99.0);
  time = dt;
  for (k=1; k<nt; k++) {
    char filename[128];
    
    // change velocity
    //CSLPolyMarkInflowFacets(dsl);
    
    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    CSLPolyApplyBC(dsl,time,bc_dirichlet_i,(void*)dirichlet_evaluator_i);
    
    CSLPolyReconstructExteriorCellFields(dsl);
    CSLPolyReconstructExteriorFacetFields(dsl);
    
    //CSLPolyUpdateI(dsl,time,dt);
    CSLPolyUpdateI_Conservative(dsl,time,dt);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    
    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    if (k%10 == 0) {
      PetscPrintf(PETSC_COMM_WORLD,"[step %4D] time = %1.2e\n",k,time);
    }
    
    time += dt;
  }
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt testid = 0;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  PetscOptionsGetInt(NULL,NULL,"-test",&testid,NULL);
  ierr = example2();CHKERRQ(ierr);
  //ierr = example3();CHKERRQ(ierr); // implicit functions
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

