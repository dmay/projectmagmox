
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <pdestokes.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <Batchelor_corner_flow_soln.h>


#undef __FUNCT__
#define __FUNCT__ "EvalVelocity_vanKekenCase1A"
PetscErrorCode EvalVelocity_vanKekenCase1A(PetscReal coor[],PetscReal vel[])
{
  double plate_thickness,alpha,origin_x,origin_y,origin[2],vmag,veld[2],coord[2];
  
  plate_thickness = 50.0 / 600.0; // normalized
  origin_x = plate_thickness;
  origin_y = -plate_thickness;
  alpha = atan2(-origin_y, origin_x); // slab dip angle
  origin[0] = origin_x;
  origin[1] = origin_y;
  vmag = sqrt(1.0*1.0 + 1.0*1.0);
  coord[0] = (PetscReal)coor[0];
  coord[1] = (PetscReal)coor[1];
  
  evaluate_BatchelorCornerFlowSoln(alpha,origin,coord,veld);
  vel[0] = (PetscReal)veld[0];
  vel[1] = (PetscReal)veld[1];
  
  if (coor[1] >= -50.0/600.0) { /* crust */
    vel[0] = 0.0;
    vel[1] = 0.0;
  }
  if (coor[1] <= -coor[0]) { /* slab */
    vel[0] =  1.0 / vmag;
    vel[1] = -1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_Case1A"
PetscErrorCode EvalDirichletBoundary_Case1A(PetscScalar coor[],PetscInt dof,
                                     PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_s = 273.0;
  PetscReal T_0 = 1573.0;
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  
  *constrain = PETSC_FALSE;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_s;
    
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_s + (T_0 - T_s) * erf( depth * 600.0*1.0e3 / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;

    PetscFunctionReturn(0);
  }

  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */

      T = T_s + (T_0 - T_s) * depth / plate_thickness;
      *constrain = PETSC_TRUE;
      *val = T;
      
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      PetscReal origin[2],alpha;
      double coor_d[2],vel_d[2];

      origin[0] = plate_thickness;
      origin[1] = -plate_thickness;
      alpha = atan2(-origin[1], origin[0]); // slab dip angle

      coor_d[0] = (double)coor[0];
      coor_d[1] = (double)coor[1];
      evaluate_BatchelorCornerFlowSoln(alpha,origin,coor_d,vel_d);
      
      if (vel_d[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val = T_0;
        PetscFunctionReturn(0);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_uv"
PetscErrorCode plate_uv(PetscScalar coor[],PetscInt dof,
                                               PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_TRUE;
  if (dof == 0) {
    *val = 0.0;
  } else if (dof == 1) {
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_boundary_uv"
PetscErrorCode plate_boundary_uv(PetscScalar coor[],PetscInt dof,
                        PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal depth,plate_thickness;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  *constrain = PETSC_FALSE;

  if (coor[0] < (0.0 + 1.0e-10)) { /* left boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (coor[1] > -1.0e-10) { /* top boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (depth > plate_thickness-1.0e-10) { /* bottom of plate boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_boundary_base_uv"
PetscErrorCode plate_boundary_base_uv(PetscScalar coor[],PetscInt dof,
                                 PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal depth,plate_thickness;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  *constrain = PETSC_FALSE;
  
  if (depth < plate_thickness+1.0e-10) { /* bottom of plate boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_uv"
PetscErrorCode slab_uv(PetscScalar coor[],PetscInt dof,
                        PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vmag;
  
  *constrain = PETSC_TRUE;
  vmag = PetscSqrtReal(1.0*1.0 + 1.0*1.0);
  if (dof == 0) {
    *val = 1.0 / vmag;
  } else if (dof == 1) {
    *val = -1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_uv"
PetscErrorCode slab_boundary_uv(PetscScalar coor[],PetscInt dof,
                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2];
  PetscErrorCode ierr;

  *constrain = PETSC_FALSE;
  ierr = EvalVelocity_vanKekenCase1A(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  if (coor[1] < -1.0 + 1.0e-10) { /* bottom boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  if (coor[0] > -coor[1]-1.0e-10) { /* slab */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_uv2"
PetscErrorCode slab_boundary_uv2(PetscScalar coor[],PetscInt dof,
                                PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2];
  PetscErrorCode ierr;
  
  *constrain = PETSC_FALSE;
  ierr = EvalVelocity_vanKekenCase1A(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] > 1.0) { /* bottom boundary */
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  if (coor[1] < -coor[0] + 1.0e-10) { /* slab */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "wedge_inflow"
PetscErrorCode wedge_inflow(PetscScalar coor[],PetscInt dof,
                                               PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2],depth,plate_thickness;
  PetscErrorCode ierr;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  *constrain = PETSC_FALSE;
  
  ierr = EvalVelocity_vanKekenCase1A(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth < plate_thickness) {
      if (dof == 0) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      } else if (dof == 1) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      }
    }
    if (depth > plate_thickness) { /* inflow from the mantle */
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val  = vel[dof];
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_Case1A_uv"
PetscErrorCode EvalDirichletBoundary_Case1A_uv(PetscScalar coor[],PetscInt dof,
                                            PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal vel[2],depth,plate_thickness;
  PetscErrorCode ierr;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized

  *constrain = PETSC_FALSE;

  ierr = EvalVelocity_vanKekenCase1A(coor,vel);CHKERRQ(ierr);
  
  if (coor[0] <= -coor[1]) { /* slab */

    *constrain = PETSC_TRUE;
    if (dof == 0) {
      *val  = vel[0];
    } else if (dof == 1) {
      *val  = vel[1];
    }
    
    PetscFunctionReturn(0);
  } else {
    if (depth <= plate_thickness) { /* within the plate */
      *constrain = PETSC_TRUE;
      *val = 0.0; /* u and v = 0 */
      PetscFunctionReturn(0);
    }
  }
  
  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */
      *constrain = PETSC_TRUE;
      *val = 0.0; /* u and v = 0 */
      
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        if (dof == 0) {
          *val  = vel[0];
        } else if (dof == 1) {
          *val  = vel[1];
        }

        PetscFunctionReturn(0);
      }
    }
  }

  PetscFunctionReturn(0);
}

/* profiling */
#undef __FUNCT__
#define __FUNCT__ "profile"
PetscErrorCode profile(Vec X,Mat interp_tet2dasample,DM dmdasample,Vec Xsample,PetscReal timeMyr)
{
  PetscErrorCode ierr;
  const DMDACoor2d **LA_coor;
  PetscScalar *LA_field;
  PetscInt i,j,xm,ym,xs,ys;
  DM dmc;
  Vec coor;
  PetscReal T_11,T_slab,T_wedge;
  static int beenhere = 0;
  FILE *fp;
  
  if (beenhere == 0) {
    fp = fopen("profile.dat","w");
    beenhere = 1;
  } else {
    fp = fopen("profile.dat","a");
  }
  
  PetscPrintf(PETSC_COMM_WORLD,"  time %+1.6e (Myr)\n",timeMyr);
  ierr = DMDAGetCorners(dmdasample,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
  ierr = DMGetCoordinateDM(dmdasample,&dmc);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dmdasample,&coor);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = MatMult(interp_tet2dasample,X,Xsample);CHKERRQ(ierr);
  ierr = VecGetArray(Xsample,&LA_field);CHKERRQ(ierr);
  
  // T_{11,11}
  {
    PetscInt idx;
    PetscInt jj_natural,ii_natural;
    PetscReal T_ij;
    
    i = 10;
    j = 10;
    jj_natural = ym - 1 - j;
    ii_natural = i;
    
    idx = ii_natural + jj_natural * xm;
    
    T_ij = LA_field[idx] - 273.0;
    
    T_11 = T_ij;
    //printf("T_{11,11}: %1.4e %1.4e %1.4e\n",xp*600.0e3,yp*600.0e3,T_ij);
  }
  PetscPrintf(PETSC_COMM_WORLD,"  T_{11,11} %+1.6e (C)\n",T_11);
  
  // T_slab
  T_slab = 0.0;
  for (i=0; i<36; i++) {
    PetscInt idx;
    PetscInt jj_natural,ii_natural;
    PetscReal T_ij;
    
    j = i;
    jj_natural = ym - 1 - j;
    ii_natural = i;
    idx = ii_natural + jj_natural * xm;
    
    T_ij = LA_field[idx] - 273.0;
    
    //printf("T_slab: %1.4e %1.4e %1.4e\n",xp*600.0e3,yp*600.0e3,T_ij);
    T_slab += T_ij * T_ij;
  }
  T_slab = PetscSqrtReal(T_slab / 36.0);
  PetscPrintf(PETSC_COMM_WORLD,"  T_slab    %+1.6e (C)\n",T_slab);
  
  // T_wedge
  T_wedge = 0.0;
  for (i=9; i<21; i++) {
    for (j=9; j<=i; j++) {
      PetscInt idx;
      PetscInt jj_natural,ii_natural;
      PetscReal T_ij;
      
      jj_natural = ym - 1 - j;
      ii_natural = i;
      idx = ii_natural + jj_natural * xm;
      
      T_ij = LA_field[idx] - 273.0;
      
      //printf("%1.4e %1.4e %1.4e %d %d\n",xp*600.0e3,yp*600.0e3,T_ij,ii_natural+1,j+1);
      T_wedge += T_ij * T_ij;
    }
  }
  T_wedge = PetscSqrtReal(T_wedge / 78.0);
  PetscPrintf(PETSC_COMM_WORLD,"  T_wedge   %+1.6e (C)\n",T_wedge);
  
  /*
   for (i=0; i<xm; i++) {
   for (j=0; j<ym; j++) {
   PetscInt idx = i + j * xm;
   PetscInt jj_natural,ii_natural;
   const PetscReal xp = LA_coor[j][i].x;
   const PetscReal yp = LA_coor[j][i].y;
   const PetscReal T_ij = LA_field[idx];
   
   jj_natural = ym - 1 - j;
   ii_natural = i;
   
   //printf("%1.4e %1.4e %1.4e\n",xp*600.0e3,yp*600.0e3,T_ij);
   }
   }
  */
  
  ierr = VecRestoreArray(Xsample,&LA_field);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  
  fprintf(fp,"%1.2e %1.6e %1.6e %1.6e\n",timeMyr,T_11,T_slab,T_wedge);
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fp_quadrature"
PetscErrorCode fp_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp;
  PetscReal *coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature"
PetscErrorCode fu_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  dm = (DM)data;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq,r2;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      r2 = PetscSqrtReal( (xq-0.5)*(xq-0.5) + (yq+0.5)*(yq+0.5) );
      
      coeff[2*(e*nqp + q)+0] = 0.0;
      coeff[2*(e*nqp + q)+1] = 0.0;
      if (r2 < 0.3) {
        coeff[2*(e*nqp + q)+1] = -1.0*0.0;
      }
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "eta_quadrature"
PetscErrorCode eta_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,nen,nqp,*ridx;
  PetscReal *coeff;
  DM dm;
  
  PetscFunctionBegin;
  dm = (DM)data;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetGeometryGetAttributes(dm,NULL,&ridx,NULL);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 1.0;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_u_top"
PetscErrorCode EvalDirichletBoundary_u_top(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_FALSE;
  
  if (coor[1] > 0.0 - 1.0e-10) { /* top */
    if (dof == 0) {
      *constrain = PETSC_TRUE;
      *val = 0.0*sin(M_PI*coor[0]/1.1);
    } else if (dof == 1) {
      *constrain = PETSC_TRUE;
      *val = 0.0;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_u"
PetscErrorCode EvalDirichletBoundary_u(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_FALSE;
  
  if (coor[0] < 1.0e-10) { /* left */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (coor[0] > 1.1 - 1.0e-10) { /* right */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_v"
PetscErrorCode EvalDirichletBoundary_v(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_FALSE;
  
  if (coor[1] < -1.0 + 1.0e-10) { /* bottom */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  if (coor[1] > 0.0 - 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "vanKeken_Case1BSteady"
PetscErrorCode vanKeken_Case1BSteady(void)
{
  PetscErrorCode ierr;
  PetscInt T_p,V_p;
  PetscReal gmin[3],gmax[3],xr[2],yr[2],kappa_nd,kappa;
  DM dmT,dmV,dmP,dmS;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xlocal,F,Xsample,velocity,XS,FS;
  Mat JA,JAS;
  PDE pde,pdeS;
  BCList bc,bcV;
  Mat interp_tet2dasample;
  SNES snes,snesS;
  PetscReal L,V,T;
  DM dmdasample;
  PetscInt region_plate,region_slab,region_wedge;
  
  /* x = L x' */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  T = L/V; /* time scale */
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  kappa_nd = kappa * T / (L*L);
  
  /* thermal problem */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(dmT,gmin,gmax);CHKERRQ(ierr);
  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmT,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmT,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  /* Define initial condition for T */
  ierr = VecSet(X,273.0);CHKERRQ(ierr);
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmT,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary_Case1A,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);
  
  /* View mesh */
  PetscSNPrintf(prefix,127,"energy_%sic_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);

  
  /* stokes problem */
  V_p = T_p;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   2,DMTET_CWARP_AND_BLEND,V_p,&dmV);CHKERRQ(ierr);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,V_p-1,&dmP);CHKERRQ(ierr);

  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);
  
  
  //
  //region_wedge = 2;
  //ierr = DMTetBCListTraverseByRegionId(bcV,region_wedge,0,wedge_inflow,NULL);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverseByRegionId(bcV,region_wedge,1,wedge_inflow,NULL);CHKERRQ(ierr);
  //
  //
  //region_slab = 1;
  //ierr = DMTetBCListTraverseByRegionId(bcV,region_slab,0,slab_uv,NULL);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverseByRegionId(bcV,region_slab,1,slab_uv,NULL);CHKERRQ(ierr);
  //
  
  
  /*
  region_plate = 3;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,0,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,1,plate_uv,NULL);CHKERRQ(ierr);
  */
  
  //ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_Case1A_uv,NULL);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_Case1A_uv,NULL);CHKERRQ(ierr);

  
  //ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_u_top,NULL);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_u_top,NULL);CHKERRQ(ierr);
  ////ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_u,NULL);CHKERRQ(ierr);
  ////ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_v,NULL);CHKERRQ(ierr);

  
  region_slab = 1;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_slab,0,slab_boundary_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_slab,1,slab_boundary_uv,NULL);CHKERRQ(ierr);

  //region_plate = 3;
  //ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,0,plate_boundary_uv,NULL);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,1,plate_boundary_uv,NULL);CHKERRQ(ierr);
  region_plate = 3;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,0,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,1,plate_uv,NULL);CHKERRQ(ierr);

  region_wedge = 2;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_wedge,0,wedge_inflow,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_wedge,1,wedge_inflow,NULL);CHKERRQ(ierr);
  
  /* Define velocity field */
  ierr = DMCreateGlobalVector(dmV,&velocity);CHKERRQ(ierr);

  
  /* profiling DMDA */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,111,101,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dmdasample);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dmdasample,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmdasample,&Xsample);CHKERRQ(ierr);

  ierr = DMCreateInterpolation(dmT,dmdasample,&interp_tet2dasample,NULL);CHKERRQ(ierr);

  /* pde for energy */
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmT,bc);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetVelocity(pde,dmV,velocity);CHKERRQ(ierr);

  {
    Coefficient k,alpha,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,kappa_nd);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadratureNULL(g);CHKERRQ(ierr);
  }

  /* pde for stokes */
  ierr = PDECreateStokes(PETSC_COMM_WORLD,dmV,dmP,bcV,&pdeS);CHKERRQ(ierr);
  ierr = PDEStokesGetDM(pdeS,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&JAS);CHKERRQ(ierr);
  ierr = MatCreateVecs(JAS,&XS,&FS);CHKERRQ(ierr);
  ierr = PDESetSolution(pdeS,XS);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pdeS,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  {
    Coefficient e,fu,fp;
    
    ierr = PDEStokesGetCoefficients(pdeS,&e,&fu,&fp);CHKERRQ(ierr);
    
    ierr = CoefficientSetComputeQuadrature(e,eta_quadrature,NULL,(void*)dmP);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)dmP);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fp,fp_quadrature,NULL,NULL);CHKERRQ(ierr);
    //ierr = CoefficientSetComputeQuadratureNULL(fu);CHKERRQ(ierr);
    //ierr = CoefficientSetComputeQuadratureNULL(fp);CHKERRQ(ierr);
  }

  /* stokes solver */
  ierr = PDESetOptionsPrefix(pdeS,"stk_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snesS);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pdeS,snesS,FS,JAS,JAS);CHKERRQ(ierr);
  ierr = SNESSetType(snesS,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snesS);CHKERRQ(ierr);
  
  /* energy solver */
  ierr = PDESetOptionsPrefix(pde,"thm_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);

  ierr = PDESNESSolve(pdeS,snesS);CHKERRQ(ierr);
  ierr = PDEStokesFieldView_AsciiVTU(pdeS,NULL);CHKERRQ(ierr);
  /* copy velocity solution into vector used by the energy solver */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
    ierr = VecCopy(Xv,velocity);CHKERRQ(ierr); /* Xv --> velocity */
    ierr = DMCompositeRestoreAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
  }

  
  //ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  {
    PetscSNPrintf(prefix,127,"energy_%s_",fieldsT[0]);
    ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  }
  
  ierr = profile(X,interp_tet2dasample,dmdasample,Xsample,0.0);CHKERRQ(ierr);
  
  
  
  
  /* clean up */
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = PDEDestroy(&pdeS);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = SNESDestroy(&snesS);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&XS);CHKERRQ(ierr);
  ierr = VecDestroy(&FS);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsample);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = MatDestroy(&JAS);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2dasample);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmdasample);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "vanKeken_Case1BSteady_subdomain"
PetscErrorCode vanKeken_Case1BSteady_subdomain(void)
{
  PetscErrorCode ierr;
  PetscInt T_p,V_p;
  PetscReal gmin[3],gmax[3],xr[2],yr[2],kappa_nd,kappa;
  DM dmT,dmV,dmP,dmV_f,dmP_f,dmS;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xlocal,F,Xsample,velocity,XS,FS;
  Mat JA,JAS;
  PDE pde,pdeS;
  BCList bc,bcV;
  Mat interp_tet2dasample;
  SNES snes,snesS;
  PetscReal L,V,T;
  DM dmdasample;
  PetscInt region_plate,region_slab,region_wedge;
  
  /* x = L x' */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  T = L/V; /* time scale */
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  kappa_nd = kappa * T / (L*L);
  
  /* thermal problem */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r2/wedge.2",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(dmT,gmin,gmax);CHKERRQ(ierr);
  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmT,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmT,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  /* Define initial condition for T */
  ierr = VecSet(X,273.0);CHKERRQ(ierr);
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmT,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary_Case1A,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);
  
  /* View mesh */
  PetscSNPrintf(prefix,127,"energy_%sic_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  
  /* stokes problem */
  V_p = T_p;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r2/wedge.2",
                                   2,DMTET_CWARP_AND_BLEND,V_p,&dmV_f);CHKERRQ(ierr);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r2/wedge.2",
                                   1,DMTET_CWARP_AND_BLEND,V_p-1,&dmP_f);CHKERRQ(ierr);
  
  {
    PetscInt nregions = 1;
    PetscInt regionlist[] = {2};
    
    ierr = DMTetCreateSubmeshByRegions(dmV_f,nregions,regionlist,&dmV);CHKERRQ(ierr);
    ierr = DMTetCreateSubmeshByRegions(dmP_f,nregions,regionlist,&dmP);CHKERRQ(ierr);
  }
  
  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);
  
  
  region_plate = 2;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,0,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_plate,1,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  
  region_slab = 2;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_slab,0,slab_boundary_uv2,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_slab,1,slab_boundary_uv2,NULL);CHKERRQ(ierr);
  
  region_wedge = 2;
  ierr = DMTetBCListTraverseByRegionId(bcV,region_wedge,0,wedge_inflow,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bcV,region_wedge,1,wedge_inflow,NULL);CHKERRQ(ierr);
  
  /* Define velocity field */
  ierr = DMCreateGlobalVector(dmV_f,&velocity);CHKERRQ(ierr);
  
  
  /* profiling DMDA */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,111,101,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dmdasample);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dmdasample,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmdasample,&Xsample);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dmT,dmdasample,&interp_tet2dasample,NULL);CHKERRQ(ierr);
  
  /* pde for energy */
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmT,bc);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetVelocity(pde,dmV_f,velocity);CHKERRQ(ierr);
  
  {
    Coefficient k,alpha,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,kappa_nd);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadratureNULL(g);CHKERRQ(ierr);
  }
  
  /* pde for stokes */
  ierr = PDECreateStokes(PETSC_COMM_WORLD,dmV,dmP,bcV,&pdeS);CHKERRQ(ierr);
  ierr = PDEStokesGetDM(pdeS,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&JAS);CHKERRQ(ierr);
  ierr = MatCreateVecs(JAS,&XS,&FS);CHKERRQ(ierr);
  ierr = PDESetSolution(pdeS,XS);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pdeS,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  {
    Coefficient e,fu,fp;
    
    ierr = PDEStokesGetCoefficients(pdeS,&e,&fu,&fp);CHKERRQ(ierr);
    
    ierr = CoefficientSetComputeQuadrature(e,eta_quadrature,NULL,(void*)dmP);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fu,fu_quadrature,NULL,(void*)dmP);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(fp,fp_quadrature,NULL,NULL);CHKERRQ(ierr);
    //ierr = CoefficientSetComputeQuadratureNULL(fu);CHKERRQ(ierr);
    //ierr = CoefficientSetComputeQuadratureNULL(fp);CHKERRQ(ierr);
  }
  
  /* stokes solver */
  ierr = PDESetOptionsPrefix(pdeS,"stk_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snesS);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pdeS,snesS,FS,JAS,JAS);CHKERRQ(ierr);
  ierr = SNESSetType(snesS,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snesS);CHKERRQ(ierr);
  
  /* energy solver */
  ierr = PDESetOptionsPrefix(pde,"thm_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pdeS,snesS);CHKERRQ(ierr);
  ierr = PDEStokesFieldView_AsciiVTU(pdeS,NULL);CHKERRQ(ierr);
  
  {
    BCList bcV_f;
    PetscInt regionindex;
    const char *fieldsV[] = { "V" };
    Mat inject_sub2g;
    VecScatter scat;
    Vec Xv,Xp;
    
    ierr = DMBCListCreate(dmV_f,&bcV_f);CHKERRQ(ierr);

    regionindex = 3;
    ierr = DMTetBCListTraverseByRegionId(bcV_f,regionindex,0,plate_uv,NULL);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bcV_f,regionindex,1,plate_uv,NULL);CHKERRQ(ierr);

    regionindex = 1;
    ierr = DMTetBCListTraverseByRegionId(bcV_f,regionindex,0,slab_uv,NULL);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bcV_f,regionindex,1,slab_uv,NULL);CHKERRQ(ierr);

    ierr = BCListInsert(bcV_f,velocity);CHKERRQ(ierr);

    /* inject wedge velocity into full domain */
    ierr = DMCreateInjection(dmV_f,dmV,&inject_sub2g);CHKERRQ(ierr);
    ierr = MatScatterGetVecScatter(inject_sub2g,&scat);CHKERRQ(ierr);

    ierr = DMCompositeGetAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,Xv,velocity,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,Xv,velocity,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,XS,&Xv,&Xp);CHKERRQ(ierr);
    
    /* view velocity field (debug) */
    PetscSNPrintf(prefix,127,"stk_vbc_");
    ierr = DMTetViewFields_VTU(dmV_f,1,&velocity,fieldsV,prefix);CHKERRQ(ierr);

    ierr = BCListDestroy(&bcV_f);CHKERRQ(ierr);
    ierr = MatDestroy(&inject_sub2g);CHKERRQ(ierr);
  }
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"energy_%s_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  ierr = profile(X,interp_tet2dasample,dmdasample,Xsample,0.0);CHKERRQ(ierr);
  
  
  
  /* clean up */
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = PDEDestroy(&pdeS);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = SNESDestroy(&snesS);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&XS);CHKERRQ(ierr);
  ierr = VecDestroy(&FS);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsample);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = MatDestroy(&JAS);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2dasample);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV_f);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP_f);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmdasample);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = vanKeken_Case1BSteady();CHKERRQ(ierr);
  ierr = vanKeken_Case1BSteady_subdomain();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

