
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <Batchelor_corner_flow_soln.h>


#undef __FUNCT__
#define __FUNCT__ "EvalVelocity_vanKekenCase1A"
PetscErrorCode EvalVelocity_vanKekenCase1A(PetscReal coor[],PetscReal vel[])
{
  double plate_thickness,alpha,origin_x,origin_y,origin[2],vmag,veld[2],coord[2];
  
  plate_thickness = 50.0 / 600.0; // normalized
  origin_x = plate_thickness;
  origin_y = -plate_thickness;
  alpha = atan2(-origin_y, origin_x); // slab dip angle
  origin[0] = origin_x;
  origin[1] = origin_y;
  vmag = sqrt(1.0*1.0 + 1.0*1.0);
  coord[0] = (PetscReal)coor[0];
  coord[1] = (PetscReal)coor[1];
  
  evaluate_BatchelorCornerFlowSoln(alpha,origin,coord,veld);
  vel[0] = (PetscReal)veld[0];
  vel[1] = (PetscReal)veld[1];
  
  if (coor[1] >= -50.0/600.0) { /* crust */
    vel[0] = 0.0;
    vel[1] = 0.0;
  }
  if (coor[1] <= -coor[0]) { /* slab */
    vel[0] =  1.0 / vmag;
    vel[1] = -1.0 / vmag;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_Case1A"
PetscErrorCode EvalDirichletBoundary_Case1A(PetscScalar coor[],PetscInt dof,
                                     PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_s = 273.0;
  PetscReal T_0 = 1573.0;
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  
  
  *constrain = PETSC_FALSE;
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  50.0 / 600.0; // normalized
  
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_s;
    
    PetscFunctionReturn(0);
  }

  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_s + (T_0 - T_s) * erf( depth * 600.0*1.0e3 / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;

    PetscFunctionReturn(0);
  }

  if (coor[0] > (1.1 - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */

      T = T_s + (T_0 - T_s) * depth / plate_thickness;
      *constrain = PETSC_TRUE;
      *val = T;
      
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      PetscReal origin[2],alpha;
      double coor_d[2],vel_d[2];

      origin[0] = plate_thickness;
      origin[1] = -plate_thickness;
      alpha = atan2(-origin[1], origin[0]); // slab dip angle

      coor_d[0] = (double)coor[0];
      coor_d[1] = (double)coor[1];
      evaluate_BatchelorCornerFlowSoln(alpha,origin,coor_d,vel_d);
      
      if (vel_d[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val = T_0;
        PetscFunctionReturn(0);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

typedef struct {
  DM dm;
  Vec Xold;
} RHSContainer;

#undef __FUNCT__
#define __FUNCT__ "RHS_Xold"
PetscErrorCode RHS_Xold(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elX,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  Vec Xold;
  RHSContainer *data;
  const PetscScalar *LA_Xold;
  
  PetscFunctionBegin;
  
  data = (RHSContainer*)user_context;
  dm   = data->dm;
  Xold = data->Xold;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&elX);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Xold,&LA_Xold);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elX[i] = LA_Xold[nidx];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal Xold_q;
      
      /* get x,y coords at each quadrature point */
      Xold_q = 0.0;
      for (i=0; i<nbasis; i++) {
        Xold_q += tab_N[q][i] * elX[i];
      }
      
      coeff[e*nqp + q] = Xold_q;
    }
  }
  ierr = VecRestoreArrayRead(Xold,&LA_Xold);CHKERRQ(ierr);
  
  ierr = PetscFree(elX);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* profiling */
#undef __FUNCT__
#define __FUNCT__ "profile"
PetscErrorCode profile(Vec X,Mat interp_tet2dasample,DM dmdasample,Vec Xsample,PetscReal timeMyr)
{
  PetscErrorCode ierr;
  const DMDACoor2d **LA_coor;
  PetscScalar *LA_field;
  PetscInt i,j,xm,ym,xs,ys;
  DM dmc;
  Vec coor;
  PetscReal T_11,T_slab,T_wedge;
  static int beenhere = 0;
  FILE *fp;
  
  if (beenhere == 0) {
    fp = fopen("profile.dat","w");
    beenhere = 1;
  } else {
    fp = fopen("profile.dat","a");
  }
  
  PetscPrintf(PETSC_COMM_WORLD,"  time %+1.6e (Myr)\n",timeMyr);
  ierr = DMDAGetCorners(dmdasample,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
  ierr = DMGetCoordinateDM(dmdasample,&dmc);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dmdasample,&coor);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = MatMult(interp_tet2dasample,X,Xsample);CHKERRQ(ierr);
  ierr = VecGetArray(Xsample,&LA_field);CHKERRQ(ierr);
  
  // T_{11,11}
  {
    PetscInt idx;
    PetscInt jj_natural,ii_natural;
    PetscReal T_ij;
    
    i = 10;
    j = 10;
    jj_natural = ym - 1 - j;
    ii_natural = i;
    
    idx = ii_natural + jj_natural * xm;
    
    //xp = LA_coor[jj_natural][ii_natural].x;
    //yp = LA_coor[jj_natural][ii_natural].y;
    T_ij = LA_field[idx] - 273.0;
    
    T_11 = T_ij;
    //printf("T_{11,11}: %1.4e %1.4e %1.4e\n",xp*600.0e3,yp*600.0e3,T_ij);
  }
  PetscPrintf(PETSC_COMM_WORLD,"  T_{11,11} %+1.6e (C)\n",T_11);
  
  // T_slab
  T_slab = 0.0;
  for (i=0; i<36; i++) {
    PetscInt idx;
    PetscInt jj_natural,ii_natural;
    PetscReal T_ij;
    
    j = i;
    jj_natural = ym - 1 - j;
    ii_natural = i;
    idx = ii_natural + jj_natural * xm;
    
    T_ij = LA_field[idx] - 273.0;
    
    //printf("T_slab: %1.4e %1.4e %1.4e\n",xp*600.0e3,yp*600.0e3,T_ij);
    T_slab += T_ij * T_ij;
  }
  T_slab = PetscSqrtReal(T_slab / 36.0);
  PetscPrintf(PETSC_COMM_WORLD,"  T_slab    %+1.6e (C)\n",T_slab);
  
  // T_wedge
  T_wedge = 0.0;
  for (i=9; i<21; i++) {
    for (j=9; j<=i; j++) {
      PetscInt idx;
      PetscInt jj_natural,ii_natural;
      PetscReal T_ij;
      
      jj_natural = ym - 1 - j;
      ii_natural = i;
      idx = ii_natural + jj_natural * xm;
      
      T_ij = LA_field[idx] - 273.0;
      
      //printf("%1.4e %1.4e %1.4e %d %d\n",xp*600.0e3,yp*600.0e3,T_ij,ii_natural+1,j+1);
      T_wedge += T_ij * T_ij;
    }
  }
  T_wedge = PetscSqrtReal(T_wedge / 78.0);
  PetscPrintf(PETSC_COMM_WORLD,"  T_wedge   %+1.6e (C)\n",T_wedge);
  
  /*
   for (i=0; i<xm; i++) {
   for (j=0; j<ym; j++) {
   PetscInt idx = i + j * xm;
   PetscInt jj_natural,ii_natural;
   const PetscReal xp = LA_coor[j][i].x;
   const PetscReal yp = LA_coor[j][i].y;
   const PetscReal T_ij = LA_field[idx];
   
   jj_natural = ym - 1 - j;
   ii_natural = i;
   
   //printf("%1.4e %1.4e %1.4e\n",xp*600.0e3,yp*600.0e3,T_ij);
   }
   }
  */
  
  ierr = VecRestoreArray(Xsample,&LA_field);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  
  fprintf(fp,"%1.2e %1.6e %1.6e %1.6e\n",timeMyr,T_11,T_slab,T_wedge);
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "vanKeken_Case1A"
PetscErrorCode vanKeken_Case1A(void)
{
  PetscErrorCode ierr;
  PetscInt k,T_p,M,N;
  PetscReal gmin[3],gmax[3],xr[2],yr[2],kappa_nd,kappa;
  DM dmT,dmSL;
  DMSemiLagrangian dsl;
  const char *fields[] = { "T" };
  char prefix[128];
  Vec X,Xold,Xlocal,F,Xsl,Xsample,deltaX;
  Mat JA;
  PDE pde;
  BCList bc;
  RHSContainer *container;
  PetscInt nsteps = 10;
  PetscInt ionsteps = 1000;
  PetscReal timestep = 0.01;
  Mat interp_tet2da,interp_tet2dasample;
  Mat interp_da2tet;
  SNES snes;
  PetscReal L,V,T,Tmyr,time_nd;
  DM dmdasample;
  
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-ionsteps",&ionsteps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-nsteps",&nsteps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&timestep,NULL);CHKERRQ(ierr);
  
  /* x = L x' */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  T = L/V; /* time scale */
  Tmyr = T/(1.0e6*365.0 * 24.0 * 60.0 * 60.0);
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  kappa_nd = kappa * T / (L*L);
  
  /* thermal mesh */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r2/wedge.2",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(dmT,gmin,gmax);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dmT,&deltaX);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&Xold);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmT,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmT,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);

  /* Define initial condition for T */
  ierr = VecSet(X,273.0);CHKERRQ(ierr);
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmT,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary_Case1A,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);

  /* View mesh */
  PetscSNPrintf(prefix,127,"energy_%sic_",fields[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fields,prefix);CHKERRQ(ierr);
  
  /* Semi-Lagrangian mesh */
  M = 331; ierr = PetscOptionsGetInt(NULL,NULL,"-M",&M,NULL);CHKERRQ(ierr);
  N = 301; ierr = PetscOptionsGetInt(NULL,NULL,"-N",&N,NULL);CHKERRQ(ierr);

  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  /* reach in and jank the dm from the semi-lagrangian context (yuck - don't do this) */
  dmSL = dsl->dm;
  ierr = DMDASetFieldName(dmSL,0,"phi");CHKERRQ(ierr);

  /* Define velocity field directly on the DMDA used by SL */
  {
    const DMDACoor2d **LA_coor;
    PetscScalar *LA_field;
    PetscInt i,j,xm,ym,xs,ys;
    DM dmc;
    Vec coor;
    double plate_thickness,alpha,origin_x,origin_y,origin[2],vmag;
    PetscViewer viewer;
    
    
    plate_thickness = 50.0 / 600.0; // normalized
    origin_x = plate_thickness;
    origin_y = -plate_thickness;
    alpha = atan2(-origin_y, origin_x); // slab dip angle
    origin[0] = origin_x;
    origin[1] = origin_y;
    vmag = sqrt(1.0*1.0 + 1.0*1.0);
    
    ierr = DMDAGetCorners(dmSL,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(dmSL,&dmc);CHKERRQ(ierr);
    ierr = DMGetCoordinates(dmSL,&coor);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
    
    ierr = VecGetArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    for (i=0; i<xm; i++) {
      for (j=0; j<ym; j++) {
        PetscInt idx = i + j * xm;
        const PetscReal xp = LA_coor[j][i].x;
        const PetscReal yp = LA_coor[j][i].y;
        double coor[2],vel[2];
        
        coor[0] = (double)xp;
        coor[1] = (double)yp;

        evaluate_BatchelorCornerFlowSoln(alpha,origin,coor,vel);

        if (yp >= -50.0/600.0) { /* crust */
          vel[0] = 0.0;
          vel[1] = 0.0;
        }
        if (yp <= -xp) { /* slab */
          vel[0] =  1.0 / vmag;
          vel[1] = -1.0 / vmag;
        }
        
        LA_field[2*idx+0] = (PetscScalar)vel[0];
        LA_field[2*idx+1] = (PetscScalar)vel[1];
      }
    }
    ierr = VecRestoreArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
    
    ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(viewer,"vel.vtr");CHKERRQ(ierr);
    ierr = VecView(dsl->velocity,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    
  }
  ierr = DMCreateGlobalVector(dmSL,&Xsl);CHKERRQ(ierr);

  
  /* profiling DMDA */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,111,101,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dmdasample);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dmdasample,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmdasample,&Xsample);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dmT,dmSL,&interp_tet2da,NULL);CHKERRQ(ierr);
  ierr = DMCreateInterpolation(dmSL,dmT,&interp_da2tet,NULL);CHKERRQ(ierr);
  ierr = DMCreateInterpolation(dmT,dmdasample,&interp_tet2dasample,NULL);CHKERRQ(ierr);

  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmT,bc);CHKERRQ(ierr);
  
  {
    Coefficient k,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,timestep * kappa_nd);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(1,&container);CHKERRQ(ierr);
    container->dm   = dmT;
    container->Xold = Xold;
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(g,RHS_Xold,NULL,(void*)container);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  time_nd = timestep;
  for (k=1; k<=nsteps; k++) {
    PetscReal max_delta;
    
    PetscPrintf(PETSC_COMM_WORLD,"=== Step %D ===\n",k);
    ierr = MatMult(interp_tet2da,X,Xsl);CHKERRQ(ierr);
    ierr = DMSemiLagrangianUpdate_DMDA(dsl,timestep,Xsl);CHKERRQ(ierr);
    ierr = MatMult(interp_da2tet,Xsl,X);CHKERRQ(ierr);
    
    ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);
    ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
    ierr = BCListInsert(bc,Xold);CHKERRQ(ierr);
    
    ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);

    {
      ierr = VecAXPY(deltaX,-1.0,X);CHKERRQ(ierr);
      ierr = VecMax(deltaX,NULL,&max_delta);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"  steady-state metric: max(|X-Xk|) = %+1.6e\n",PetscAbsReal(max_delta));
    }

    ierr = profile(X,interp_tet2dasample,dmdasample,Xsample,time_nd*Tmyr);CHKERRQ(ierr);
    
    if (k%ionsteps == 0) {
      PetscSNPrintf(prefix,127,"energy_%s_step%D_",fields[0],k);
      ierr = DMTetViewFields_VTU(dmT,1,&X,fields,prefix);CHKERRQ(ierr);
    }
    
    ierr = VecCopy(X,deltaX);CHKERRQ(ierr);
    
    if (PetscAbsReal(max_delta) < 1.0e-8) {
      PetscPrintf(PETSC_COMM_WORLD,"System is deemed to be at steady state\n");
    }
    time_nd = time_nd + timestep;
  }

  
  /* clean up */
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = VecDestroy(&deltaX);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsl);CHKERRQ(ierr);
  ierr = VecDestroy(&Xold);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsample);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2da);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_da2tet);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2dasample);CHKERRQ(ierr);
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmdasample);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
  ./vk-case1a.app -pc_type lu -T_p 7  -ksp_type fgmres  -ksp_monitor -snes_monitor -pc_type lu  -pc_factor_mat_solver_package umfpack
*/
#undef __FUNCT__
#define __FUNCT__ "vanKeken_Case1ASteady"
PetscErrorCode vanKeken_Case1ASteady(void)
{
  PetscErrorCode ierr;
  PetscInt T_p,V_p;
  PetscReal gmin[3],gmax[3],xr[2],yr[2],kappa_nd,kappa;
  DM dmT,dmV;
  const char *fields[] = { "T" };
  char prefix[128];
  Vec X,Xlocal,F,Xsample,velocity;
  Mat JA;
  PDE pde;
  BCList bc;
  Mat interp_tet2dasample;
  SNES snes;
  PetscReal L,V,T;
  DM dmdasample;
  
  
  /* x = L x' */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  T = L/V; /* time scale */
  kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  kappa_nd = kappa * T / (L*L);
  
  /* thermal mesh */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r2/wedge.2",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(dmT,gmin,gmax);CHKERRQ(ierr);
  xr[0] = gmin[0];
  xr[1] = gmax[0];
  yr[0] = gmin[1];
  yr[1] = gmax[1];
  
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmT,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmT,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  V_p = T_p;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r2/wedge.2",
                                   2,DMTET_CWARP_AND_BLEND,V_p,&dmV);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmV,&velocity);CHKERRQ(ierr);

  /* Define velocity field */
  {
    PetscReal *LA_velocity;
    PetscInt k,nnodes;
    PetscReal *coors;
    
    ierr = DMTetSpaceElement(dmV,NULL,NULL,NULL,NULL,&nnodes,&coors);CHKERRQ(ierr);
    ierr = VecGetArray(velocity,&LA_velocity);CHKERRQ(ierr);
    for (k=0; k<nnodes; k++) {
      PetscReal *coor_k,*vel_k;
      
      coor_k = &coors[2*k];
      vel_k = &LA_velocity[2*k];
      ierr = EvalVelocity_vanKekenCase1A(coor_k,vel_k);CHKERRQ(ierr);
    }
    ierr = VecRestoreArray(velocity,&LA_velocity);CHKERRQ(ierr);
  }
  
  /* Define initial condition for T */
  ierr = VecSet(X,273.0);CHKERRQ(ierr);
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmT,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary_Case1A,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);
  
  /* View mesh */
  PetscSNPrintf(prefix,127,"energy_%sic_",fields[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fields,prefix);CHKERRQ(ierr);
  
  /* profiling DMDA */
  ierr = DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,111,101,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dmdasample);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dmdasample,xr[0],xr[1],yr[0],yr[1],0,0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmdasample,&Xsample);CHKERRQ(ierr);
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmT,bc);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetVelocity(pde,dmV,velocity);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dmT,dmdasample,&interp_tet2dasample,NULL);CHKERRQ(ierr);

  {
    Coefficient k,alpha,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,kappa_nd);CHKERRQ(ierr);

    ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);

    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    //ierr = CoefficientSetComputeQuadrature(g,g_null,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadratureNULL(g);CHKERRQ(ierr);
}
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  ierr = profile(X,interp_tet2dasample,dmdasample,Xsample,0.0);CHKERRQ(ierr);
  
  {
    PetscSNPrintf(prefix,127,"energy_%s_step_inf_",fields[0]);
    ierr = DMTetViewFields_VTU(dmT,1,&X,fields,prefix);CHKERRQ(ierr);
  }
    
  
  
  /* clean up */
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsample);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2dasample);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmdasample);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = vanKeken_Case1A();CHKERRQ(ierr);
  ierr = vanKeken_Case1ASteady();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

