
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <matnest_utils.h>
#include <proj_init_finalize.h>


#undef __FUNCT__
#define __FUNCT__ "example_singlecomposite_3splits"
PetscErrorCode example_singlecomposite_3splits(void)
{
  PetscErrorCode ierr;
  DM dmus,dmps,dmpd,dms;
  Vec X,F;
  IS *is_pack;
  Mat bA[3][3],B;
  PetscInt i,j;
  PC pc;
  KSP ksp;
  
  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,4,2,1,NULL,&dmus);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmus,"us_");CHKERRQ(ierr);
  
  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,3,1,1,NULL,&dmps);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmps,"ps_");CHKERRQ(ierr);

  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,2,1,1,NULL,&dmpd);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmpd,"pd_");CHKERRQ(ierr);

  ierr = DMCompositeCreate(PETSC_COMM_WORLD,&dms);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmus);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmps);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmpd);CHKERRQ(ierr);
  ierr = DMSetUp(dms);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);

  ierr = DMGetGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dms,&X);CHKERRQ(ierr);

  ierr = DMCompositeGetGlobalISs(dms,&is_pack);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"us-field\n");
  ierr = ISView(is_pack[0],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"ps-field\n");
  ierr = ISView(is_pack[1],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"pd-field\n");
  ierr = ISView(is_pack[2],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  // operator
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      bA[i][j] = NULL;
    }
  }

  ierr = DMCreateMatrix(dmus,&bA[0][0]);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmps,&bA[1][1]);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmpd,&bA[2][2]);CHKERRQ(ierr);
  
  for (i=0; i<4*2; i++) MatSetValue(bA[0][0],i,i,0.1,INSERT_VALUES);
  for (i=0; i<3; i++)   MatSetValue(bA[1][1],i,i,0.01,INSERT_VALUES);
  for (i=0; i<2; i++)   MatSetValue(bA[2][2],i,i,0.0001,INSERT_VALUES);
  
  MatAssemblyBegin(bA[0][0],MAT_FINAL_ASSEMBLY); MatAssemblyEnd(bA[0][0],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(bA[1][1],MAT_FINAL_ASSEMBLY); MatAssemblyEnd(bA[1][1],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(bA[2][2],MAT_FINAL_ASSEMBLY); MatAssemblyEnd(bA[2][2],MAT_FINAL_ASSEMBLY);
  
  ierr = PetscObjectSetName((PetscObject)bA[0][0],"Auus");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)bA[1][1],"Apps");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)bA[2][2],"Appd");CHKERRQ(ierr);
  
  ierr = MatCreateNest(PetscObjectComm((PetscObject)dms),3,is_pack,3,is_pack,&bA[0][0],&B);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  /* tidy up */
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      if (bA[i][j]) { ierr = MatDestroy(&bA[i][j]);CHKERRQ(ierr); }
    }
  }
  
  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dms,&F);CHKERRQ(ierr);

  ierr = VecSet(F,1.0);CHKERRQ(ierr);
  for (i=0; i<4*2+3+2; i++) { ierr = VecSetValue(F,i,(PetscScalar)(i+1),INSERT_VALUES);CHKERRQ(ierr); }
  ierr = VecAssemblyBegin(F);CHKERRQ(ierr); ierr = VecAssemblyEnd(F);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,B,B);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCFIELDSPLIT);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  ierr = PCFieldSplitSetIS(pc,"us",is_pack[0]);CHKERRQ(ierr);
  ierr = PCFieldSplitSetIS(pc,"ps",is_pack[1]);CHKERRQ(ierr);
  ierr = PCFieldSplitSetIS(pc,"ud",is_pack[2]);CHKERRQ(ierr);
  
  ierr = KSPSolve(ksp,F,X);CHKERRQ(ierr);
  
  ierr = VecView(X,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = MatDestroy(&B);CHKERRQ(ierr);
  ierr = ISDestroy(&is_pack[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is_pack[1]);CHKERRQ(ierr);
  ierr = ISDestroy(&is_pack[2]);CHKERRQ(ierr);
  ierr = PetscFree(is_pack);CHKERRQ(ierr);
  ierr = DMDestroy(&dmus);CHKERRQ(ierr);
  ierr = DMDestroy(&dmps);CHKERRQ(ierr);
  ierr = DMDestroy(&dmpd);CHKERRQ(ierr);
  ierr = DMDestroy(&dms);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_singlecomposite_2splits"
PetscErrorCode example_singlecomposite_2splits(void)
{
  PetscErrorCode ierr;
  DM dmus,dmps,dmpd,dms;
  Vec X,F;
  IS *is_pack,is_s,is_d;
  Mat bA[3][3],B;
  PetscInt i,j;
  PC pc;
  KSP ksp;
  PetscInt nus,nps,npd;
  
  nus = 4 * 2;
  nps = 3;
  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,nus/2,2,1,NULL,&dmus);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmus,"us_");CHKERRQ(ierr);
  
  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,nps,1,1,NULL,&dmps);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmps,"ps_");CHKERRQ(ierr);

  npd = 2;
  ierr = DMDACreate1d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,npd,1,1,NULL,&dmpd);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmpd,"pd_");CHKERRQ(ierr);
  
  ierr = DMCompositeCreate(PETSC_COMM_WORLD,&dms);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmus);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmps);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmpd);CHKERRQ(ierr);
  ierr = DMSetUp(dms);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  
  ierr = DMGetGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dms,&X);CHKERRQ(ierr);
  
  ierr = DMCompositeGetGlobalISs(dms,&is_pack);CHKERRQ(ierr);
  
  /*
  PetscPrintf(PETSC_COMM_WORLD,"us-field\n");
  ierr = ISView(is_pack[0],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"ps-field\n");
  ierr = ISView(is_pack[1],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"ud-field\n");
  ierr = ISView(is_pack[2],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"pd-field\n");
  ierr = ISView(is_pack[3],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  */
  
  // operator
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      bA[i][j] = NULL;
    }
  }
  
  ierr = DMCreateMatrix(dmus,&bA[0][0]);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmps,&bA[1][1]);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmpd,&bA[2][2]);CHKERRQ(ierr);
  
  for (i=0; i<nus; i++) MatSetValue(bA[0][0],i,i,0.1,INSERT_VALUES);
  for (i=0; i<nps; i++) MatSetValue(bA[1][1],i,i,0.01,INSERT_VALUES);
  for (i=0; i<npd; i++) MatSetValue(bA[2][2],i,i,0.0001,INSERT_VALUES);
  
  MatAssemblyBegin(bA[0][0],MAT_FINAL_ASSEMBLY); MatAssemblyEnd(bA[0][0],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(bA[1][1],MAT_FINAL_ASSEMBLY); MatAssemblyEnd(bA[1][1],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(bA[2][2],MAT_FINAL_ASSEMBLY); MatAssemblyEnd(bA[2][2],MAT_FINAL_ASSEMBLY);
  
  ierr = PetscObjectSetName((PetscObject)bA[0][0],"Auus");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)bA[1][1],"Apps");CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)bA[2][2],"Appd");CHKERRQ(ierr);
  
  ierr = MatCreateNest(PetscObjectComm((PetscObject)dms),3,is_pack,3,is_pack,&bA[0][0],&B);CHKERRQ(ierr);
  ierr = MatSetDM(B,dms);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = DMCompositeFillMatNest(B);CHKERRQ(ierr);
  
  /* tidy up */
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      if (bA[i][j]) { ierr = MatDestroy(&bA[i][j]);CHKERRQ(ierr); }
    }
  }
  
  ierr = MatNestConfigureFusedISGetSubMatrix(B);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dms,&F);CHKERRQ(ierr);

  ierr = VecSet(F,1.0);CHKERRQ(ierr);
  for (i=0; i<nus+nps+npd; i++) { ierr = VecSetValue(F,i,(PetscScalar)(i+1),INSERT_VALUES);CHKERRQ(ierr); }
  ierr = VecAssemblyBegin(F);CHKERRQ(ierr); ierr = VecAssemblyEnd(F);CHKERRQ(ierr);
  
  ierr = VecSet(X,0.0);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,B,B);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  //ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCFIELDSPLIT);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  ierr = ISConcatenate(PETSC_COMM_WORLD, 2, &is_pack[0], &is_s);CHKERRQ(ierr);
  is_d = is_pack[2];
  
  /*
  // Standard 3 "field" split
  ierr = PCFieldSplitSetIS(pc,"us",is_pack[0]);CHKERRQ(ierr);
  ierr = PCFieldSplitSetIS(pc,"ps",is_pack[1]);CHKERRQ(ierr);
  ierr = PCFieldSplitSetIS(pc,"pd",is_pack[2]);CHKERRQ(ierr);
  */
  // Test we can pull out fused blocks from different locations
  
  /*
  {
    Mat submat;
    
    ierr = MatGetSubMatrix(B,is_s,is_s,MAT_INITIAL_MATRIX,&submat);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is_s,is_d,MAT_INITIAL_MATRIX,&submat);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is_d,is_s,MAT_INITIAL_MATRIX,&submat);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is_d,is_d,MAT_INITIAL_MATRIX,&submat);CHKERRQ(ierr);
    ierr = MatGetSubMatrix(B,is_pack[0],is_s,MAT_INITIAL_MATRIX,&submat);CHKERRQ(ierr);
    MatView(submat,PETSC_VIEWER_STDOUT_WORLD);
  }
  */

  //
  // Non-standard 2 "field" split
  ierr = PCFieldSplitSetIS(pc,"usps",is_s);CHKERRQ(ierr);
  ierr = PCFieldSplitSetIS(pc,"pd",is_d);CHKERRQ(ierr);
  //
  //
  // Nested ( (us) | (ps) ) | pd ) split
  {
    KSP *subksp;
    Mat Ad,Bd,X00,X11;
    PetscInt nsplits;
    IS isstokes[2];
    PC subpc;
    
    ierr = KSPSetUp(ksp);CHKERRQ(ierr);
    
    ierr = PCFieldSplitGetSubKSP(pc,&nsplits,&subksp);CHKERRQ(ierr);
    ierr = KSPGetOperators(subksp[0],&Ad,&Bd);CHKERRQ(ierr);
    
    ierr = MatNestGetISs(Bd,isstokes,NULL);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"[stokes block] uD\n");
    ierr = ISView(isstokes[0],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"[stokes block] pf\n");
    ierr = ISView(isstokes[1],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = MatNestGetSubMat(Bd,0,0,&X00);CHKERRQ(ierr);
    ierr = MatNestGetSubMat(Bd,1,1,&X11);CHKERRQ(ierr);
    ierr = MatView(X00,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = MatView(X11,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

    ierr = KSPGetPC(subksp[0],&subpc);CHKERRQ(ierr);
    ierr = PCSetType(subpc,PCFIELDSPLIT);CHKERRQ(ierr);
    ierr = KSPSetFromOptions(subksp[0]);CHKERRQ(ierr);
    ierr = KSPGetPC(subksp[0],&subpc);CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(subpc,"us",isstokes[0]);CHKERRQ(ierr);
    ierr = PCFieldSplitSetIS(subpc,"ps",isstokes[1]);CHKERRQ(ierr);
    ierr = KSPSetUp(subksp[0]);CHKERRQ(ierr);
    
  }
  //
  //ierr = KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = KSPSolve(ksp,F,X);CHKERRQ(ierr);
  ierr = VecView(X,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  //
  {
    Mat submat;

    ierr = MatGetSubMatrix(B,is_pack[0],is_pack[0],MAT_INITIAL_MATRIX,&submat);CHKERRQ(ierr);
    
    for (i=0; i<nus/2; i++) MatSetValue(submat,i,i,0.00001,INSERT_VALUES);
    MatAssemblyBegin(submat,MAT_FINAL_ASSEMBLY); MatAssemblyEnd(submat,MAT_FINAL_ASSEMBLY);

    ierr = MatDestroy(&submat);CHKERRQ(ierr);

    ierr = MatNestAssembly(B);CHKERRQ(ierr);
    
    ierr = KSPSetOperators(ksp,B,B);CHKERRQ(ierr);
    
  }
  ierr = KSPSolve(ksp,F,X);CHKERRQ(ierr);
  ierr = VecView(X,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //
  
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = MatDestroy(&B);CHKERRQ(ierr);
  ierr = ISDestroy(&is_s);CHKERRQ(ierr);
  ierr = ISDestroy(&is_pack[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is_pack[1]);CHKERRQ(ierr);
  ierr = ISDestroy(&is_pack[2]);CHKERRQ(ierr);
  ierr = PetscFree(is_pack);CHKERRQ(ierr);
  ierr = DMDestroy(&dmus);CHKERRQ(ierr);
  ierr = DMDestroy(&dmps);CHKERRQ(ierr);
  ierr = DMDestroy(&dmpd);CHKERRQ(ierr);
  ierr = DMDestroy(&dms);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = example_singlecomposite_3splits();CHKERRQ(ierr);
  ierr = example_singlecomposite_2splits();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

