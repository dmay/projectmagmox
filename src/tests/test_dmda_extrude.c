
#include <petsc.h>
#include <petscdm.h>
#include <proj_init_finalize.h>


PetscErrorCode DMDACreateExtruded(DM dm,DM *dm_extrduded)
{
  PetscErrorCode ierr;
  PetscInt d,nx[3],nx0,ny0;
  DM dme;
  PetscReal gmin[3],gmax[3],dx[3];
  
  
  nx0 = 12;
  ny0 = 10;
  
  nx[0] = nx0+2;
  nx[1] = ny0+2;
  
  ierr = DMDAGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  for (d=0; d<2; d++) {
    dx[d] = (gmax[d] - gmin[d])/((PetscReal)(nx[d]-1));
    dx[d] = 0.3;
  }
  
  ierr = DMDACreate2d(PETSC_COMM_SELF, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,
                      nx[0],nx[1],PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dme);CHKERRQ(ierr);

  ierr = DMDASetUniformCoordinates(dme,gmin[0]-dx[0],gmax[0]+dx[0],gmin[1]-dx[1],gmax[1]+dx[1],0.0,0.0);CHKERRQ(ierr);
  
  *dm_extrduded = dme;
  PetscFunctionReturn(0);
}


PetscErrorCode DMDAExtrudedInjectField(DM dm,Vec x,DM dme,Vec xe)
{
  PetscErrorCode ierr;
  PetscInt b,bs,lij,lij_e;
  PetscInt i,si,ni,li,li_e,ni_e;
  PetscInt j,sj,nj,lj,lj_e,nj_e;
  PetscScalar *LA_xe;
  const PetscScalar *LA_x;
  PetscInt M,N,shift[3];
  
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  ierr = VecGetArrayRead(x,&LA_x);CHKERRQ(ierr);

  //ierr = VecZeroEntries(xe);CHKERRQ(ierr);
  ierr = VecGetArray(xe,&LA_xe);CHKERRQ(ierr);
  
  /* inject overlapping part only */
  DMDAGetInfo(dm,0,&M,&N,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dm,&si,&sj,0,&ni,&nj,0);

  DMDAGetCorners(dme,0,0,0,&ni_e,&nj_e,0);

  shift[0] = 0;
  shift[1] = 0;
  shift[2] = 0;
  
  if (si == 0) { shift[0]++; }
  if (sj == 0) { shift[1]++; }
  //if (sk == 0) { shift[2]++; }
  
  for (j=sj; j<sj+nj; j++) {
    lj = j - sj;
    lj_e = lj + shift[1];
  
    for (i=si; i<si+ni; i++) {
      li = i - si;
      li_e = li + shift[0];
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("%d -> %d [%d -> %d] \n",li,li_e,lij,lij_e);
      
      for (b=0; b<bs; b++) {
        LA_xe[bs*lij_e + b] = LA_x[bs*lij + b];
      }
      
    }
  }
  ierr = VecRestoreArray(xe,&LA_xe);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* loop over extruded points, copy values from interior */
PetscErrorCode DMDAExtrudeField(DM dm,Vec x,DM dme,Vec xe)
{
  PetscErrorCode ierr;
  PetscInt b,bs,lij,lij_e;
  PetscInt i,si,ni,li,li_e,si_e,ni_e;
  PetscInt j,sj,nj,lj,lj_e,sj_e,nj_e;
  PetscScalar *LA_xe;
  const PetscScalar *LA_x;
  PetscInt M,N,M_e,N_e;
  
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  ierr = VecGetArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  ierr = VecGetArray(xe,&LA_xe);CHKERRQ(ierr);
  
  /* inject overlapping part only */
  DMDAGetInfo(dm,0,&M,&N,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dm,&si,&sj,0,&ni,&nj,0);
  
  DMDAGetInfo(dme,0,&M_e,&N_e,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dme,&si_e,&sj_e,0,&ni_e,&nj_e,0);
  
  // left
  if (si_e == 0) {
    PetscBool end_point_correction = PETSC_FALSE;

    li = 0;
    li_e = 0;
    
    /* sub-domain contains end points - do something special */
    if (sj_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((sj_e+nj_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (j=sj_e; j<sj_e+nj_e; j++) {
      lj_e = j - sj_e;
      
      lj = lj_e;
      if (end_point_correction) {
        lj = lj_e - 1;
        if (lj < 0) { lj = 0; }
        if (lj > nj-1) { lj = nj-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[left] (%d,%d) <== (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      for (b=0; b<bs; b++) {
        LA_xe[bs*lij_e + b] = LA_x[bs*lij + b];
      }
      
    }
  }
  
  // right
  if ((si_e+ni_e) == M_e) {
    PetscBool end_point_correction = PETSC_FALSE;

    li = ni - 1;
    li_e = ni_e - 1;
    
    /* sub-domain contains end points - do something special */
    if (sj_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((sj_e+nj_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (j=sj_e; j<sj_e+nj_e; j++) {
      lj_e = j - sj_e;
      
      lj = lj_e;
      if (end_point_correction) {
        lj = lj_e - 1;
        if (lj < 0) { lj = 0; }
        if (lj > nj-1) { lj = nj-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      //printf("[right] (%d,%d) <== (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      for (b=0; b<bs; b++) {
        LA_xe[bs*lij_e + b] = LA_x[bs*lij + b];
      }
    }
  }
  

  // bottom
  if (sj_e == 0) {
    PetscBool end_point_correction = PETSC_FALSE;

    lj = 0;
    lj_e = 0;
    
    /* sub-domain contains end points - do something special */
    if (si_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((si_e+ni_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (i=si_e; i<si_e+ni_e; i++) {
      li_e = i - si_e;
      
      li = li_e;
      if (end_point_correction) {
        li = li_e - 1;
        if (li < 0) { li = 0; }
        if (li > ni-1) { li = ni-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[bottom] (%d,%d) -> (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      for (b=0; b<bs; b++) {
        LA_xe[bs*lij_e + b] = LA_x[bs*lij + b];
      }
      
    }
  }

  // top
  if ((sj_e+nj_e) == N_e) {
    PetscBool end_point_correction = PETSC_FALSE;

    lj = nj - 1;
    lj_e = nj_e - 1;

    
    /* sub-domain contains end points - do something special */
    if (si_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((si_e+ni_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (i=si_e; i<si_e+ni_e; i++) {
      li_e = i - si_e;
      
      li = li_e;
      if (end_point_correction) {
        li = li_e - 1;
        if (li < 0) { li = 0; }
        if (li > ni-1) { li = ni-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[top] (%d,%d) -> (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      for (b=0; b<bs; b++) {
        LA_xe[bs*lij_e + b] = LA_x[bs*lij + b];
      }
      
    }
  }

  
  ierr = VecRestoreArray(xe,&LA_xe);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode DMDAExtrudedInjectCoordinates(DM dm,DM dme)
{
  PetscErrorCode ierr;
  Vec c,ce;
  
  ierr = DMGetCoordinates(dm,&c);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dme,&ce);CHKERRQ(ierr);
  
  ierr = DMDAExtrudedInjectField(dm,c,dme,ce);CHKERRQ(ierr);

  ierr = DMGetCoordinatesLocal(dm,&c);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(dme,&ce);CHKERRQ(ierr);
  
  ierr = DMDAExtrudedInjectField(dm,c,dme,ce);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* loop over extruded points, copy values from interior */
PetscErrorCode _DMDAExtrudeCoords(DM dm,Vec x,DM dme,Vec xe)
{
  PetscErrorCode ierr;
  PetscInt bs,lij,lij_e;
  PetscInt i,si,ni,li,li_e,si_e,ni_e;
  PetscInt j,sj,nj,lj,lj_e,sj_e,nj_e;
  PetscScalar *LA_xe;
  const PetscScalar *LA_x;
  PetscInt M,N,M_e,N_e;
  
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  ierr = VecGetArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  ierr = VecGetArray(xe,&LA_xe);CHKERRQ(ierr);
  
  /* inject overlapping part only */
  DMDAGetInfo(dm,0,&M,&N,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dm,&si,&sj,0,&ni,&nj,0);
  
  DMDAGetInfo(dme,0,&M_e,&N_e,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dme,&si_e,&sj_e,0,&ni_e,&nj_e,0);
  
  // left
  if (si_e == 0) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    li = 0;
    li_e = 0;
    
    /* sub-domain contains end points - do something special */
    if (sj_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((sj_e+nj_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (j=sj_e; j<sj_e+nj_e; j++) {
      lj_e = j - sj_e;
      
      lj = lj_e;
      if (end_point_correction) {
        lj = lj_e - 1;
        if (lj < 0) { continue; }
        if (lj > nj-1) { continue; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[left] (%d,%d) <== (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      // move y coordinate only //
      LA_xe[bs*lij_e + 1] = LA_x[bs*lij + 1];
      
    }
  }

  // right
  if ((si_e+ni_e) == M_e) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    li = ni - 1;
    li_e = ni_e - 1;
    
    /* sub-domain contains end points - do something special */
    if (sj_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((sj_e+nj_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (j=sj_e; j<sj_e+nj_e; j++) {
      lj_e = j - sj_e;
      
      lj = lj_e;
      if (end_point_correction) {
        lj = lj_e - 1;
        if (lj < 0) { continue; }
        if (lj > nj-1) { continue; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      //printf("[right] (%d,%d) <== (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      LA_xe[bs*lij_e + 1] = LA_x[bs*lij + 1];
    }
  }
  
  
  // bottom
  if (sj_e == 0) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    lj = 0;
    lj_e = 0;
    
    /* sub-domain contains end points - do something special */
    if (si_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((si_e+ni_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (i=si_e; i<si_e+ni_e; i++) {
      li_e = i - si_e;
      
      li = li_e;
      if (end_point_correction) {
        li = li_e - 1;
        if (li < 0) { continue; }
        if (li > ni-1) { continue; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[bottom] (%d,%d) -> (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      // move x coor only //
      LA_xe[bs*lij_e + 0] = LA_x[bs*lij + 0];
      
    }
  }
  
  // top
  if ((sj_e+nj_e) == N_e) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    lj = nj - 1;
    lj_e = nj_e - 1;
    
    
    /* sub-domain contains end points - do something special */
    if (si_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((si_e+ni_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (i=si_e; i<si_e+ni_e; i++) {
      li_e = i - si_e;
      
      li = li_e;
      if (end_point_correction) {
        li = li_e - 1;
        if (li < 0) { continue; }
        if (li > ni-1) { continue; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[top] (%d,%d) -> (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      LA_xe[bs*lij_e + 0] = LA_x[bs*lij + 0];
      
    }
  }
  
  ierr = VecRestoreArray(xe,&LA_xe);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* loop over extruded points, copy values from interior */
PetscErrorCode _DMDAExtrudeCoords_normal(DM dm,Vec x,PetscReal dx,PetscReal dy,DM dme,Vec xe)
{
  PetscErrorCode ierr;
  PetscInt bs,lij,lij_e;
  PetscInt i,si,ni,li,li_e,si_e,ni_e;
  PetscInt j,sj,nj,lj,lj_e,sj_e,nj_e;
  PetscScalar *LA_xe;
  const PetscScalar *LA_x;
  PetscInt M,N,M_e,N_e;
  
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  ierr = VecGetArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  ierr = VecGetArray(xe,&LA_xe);CHKERRQ(ierr);
  
  /* inject overlapping part only */
  DMDAGetInfo(dm,0,&M,&N,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dm,&si,&sj,0,&ni,&nj,0);
  
  DMDAGetInfo(dme,0,&M_e,&N_e,0,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(dme,&si_e,&sj_e,0,&ni_e,&nj_e,0);
  
  // left
  if (si_e == 0) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    li = 0;
    li_e = 0;
    
    /* sub-domain contains end points - do something special */
    if (sj_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((sj_e+nj_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (j=sj_e; j<sj_e+nj_e; j++) {
      lj_e = j - sj_e;
      
      lj = lj_e;
      if (end_point_correction) {
        lj = lj_e - 1;
        if (lj < 0) { lj = 0; }
        if (lj > nj-1) { lj = nj-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[left] (%d,%d) <== (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      // move x coordinate only //
      LA_xe[bs*lij_e + 0] = LA_x[bs*lij + 0] - dx;
      
    }
  }

  
  // right
  if ((si_e+ni_e) == M_e) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    li = ni - 1;
    li_e = ni_e - 1;
    
    /* sub-domain contains end points - do something special */
    if (sj_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((sj_e+nj_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (j=sj_e; j<sj_e+nj_e; j++) {
      lj_e = j - sj_e;
      
      lj = lj_e;
      if (end_point_correction) {
        lj = lj_e - 1;
        if (lj < 0) { lj = 0; }
        if (lj > nj-1) { lj = nj-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      //printf("[right] (%d,%d) <== (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      LA_xe[bs*lij_e + 0] = LA_x[bs*lij + 0] + dx;
    }
  }
  
  // bottom
  if (sj_e == 0) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    lj = 0;
    lj_e = 0;
    
    /* sub-domain contains end points - do something special */
    if (si_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((si_e+ni_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (i=si_e; i<si_e+ni_e; i++) {
      li_e = i - si_e;
      
      li = li_e;
      if (end_point_correction) {
        li = li_e - 1;
        if (li < 0) { li = 0; }
        if (li > ni-1) { li = ni-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[bottom] (%d,%d) -> (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      // move y coor only //
      LA_xe[bs*lij_e + 1] = LA_x[bs*lij + 1] - dy;
      
    }
  }


  // top
  if ((sj_e+nj_e) == N_e) {
    PetscBool end_point_correction = PETSC_FALSE;
    
    lj = nj - 1;
    lj_e = nj_e - 1;
    
    
    /* sub-domain contains end points - do something special */
    if (si_e == 0) {
      end_point_correction = PETSC_TRUE;
    }
    if ((si_e+ni_e) == N) {
      end_point_correction = PETSC_TRUE;
    }
    
    for (i=si_e; i<si_e+ni_e; i++) {
      li_e = i - si_e;
      
      li = li_e;
      if (end_point_correction) {
        li = li_e - 1;
        if (li < 0) { li = 0; }
        if (li > ni-1) { li = ni-1; }
      }
      
      lij   = li   + lj   * ni;
      lij_e = li_e + lj_e * ni_e;
      
      //printf("[top] (%d,%d) -> (%d,%d) [%d -> %d] \n",li_e,lj_e,li,lj,lij,lij_e);
      
      LA_xe[bs*lij_e + 1] = LA_x[bs*lij + 1] + dy;
      
    }
  }

  
  ierr = VecRestoreArray(xe,&LA_xe);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


PetscErrorCode DMDAExtrudeCoordinates(DM dm,PetscReal dx,PetscReal dy,DM dme)
{
  PetscErrorCode ierr;
  Vec c,ce;
  
  ierr = DMGetCoordinates(dm,&c);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dme,&ce);CHKERRQ(ierr);
  
  ierr = _DMDAExtrudeCoords(dm,c,dme,ce);CHKERRQ(ierr);
  ierr = _DMDAExtrudeCoords_normal(dm,c,dx,dy,dme,ce);CHKERRQ(ierr);
  
  ierr = DMGetCoordinatesLocal(dm,&c);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(dme,&ce);CHKERRQ(ierr);
  
  ierr = _DMDAExtrudeCoords(dm,c,dme,ce);CHKERRQ(ierr);
  ierr = _DMDAExtrudeCoords_normal(dm,c,dx,dy,dme,ce);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "extude"
PetscErrorCode extude(void)
{
  PetscErrorCode ierr;
  DM dm,dm_e;
  Vec X,X_e;
  PetscInt nx,ny;
  PetscViewer viewer;
  
  nx = 12;
  ny = 10;
  ierr = DMDACreate2d(PETSC_COMM_SELF, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,
                      nx,ny,PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&dm);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(dm,-1.1,1.1,0.0,0.88,0.0,0.0);CHKERRQ(ierr);
  {
    Vec c;
    PetscScalar *LA_c;
    PetscInt i,j,ni,nj;

    DMGetCoordinates(dm,&c);
    DMDAGetCorners(dm,0,0,0,&ni,&nj,0);
    VecGetArray(c,&LA_c);
    j = 0;
    for (i=0; i<ni; i++) {
      LA_c[2*(i+j*ni)+1] = cos(M_PI * LA_c[2*(i+j*ni)+0]) * 0.05;
    }

    i = 0;
    for (j=0; j<nj; j++) {
      LA_c[2*(i+j*ni)+0] = -1.1 + cos(M_PI * LA_c[2*(i+j*ni)+1]) * 0.05;// + 0.1*LA_c[2*(i+j*ni)+1];
      LA_c[2*(i+j*ni)+1] = LA_c[2*(i+j*ni)+1] + 0.05;
    }

    VecRestoreArray(c,&LA_c);
  }
  
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = VecSetRandom(X,NULL);CHKERRQ(ierr);

  ierr = DMDACreateExtruded(dm,&dm_e);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_e,&X_e);CHKERRQ(ierr);

  ierr = DMDAExtrudedInjectCoordinates(dm,dm_e);CHKERRQ(ierr);
  ierr = DMDAExtrudeCoordinates(dm,0.5,0.5,dm_e);CHKERRQ(ierr);

  ierr = VecSet(X_e,0.0);CHKERRQ(ierr);
  ierr = DMDAExtrudedInjectField(dm,X,dm_e,X_e);CHKERRQ(ierr);
  ierr = DMDAExtrudeField(dm,X,dm_e,X_e);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"dm.vts");CHKERRQ(ierr);
  ierr = VecView(X,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"dm_e.vts");CHKERRQ(ierr);
  ierr = VecView(X_e,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_e);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = extude();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

