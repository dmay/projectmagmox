
#include <petsc.h>
#include <petscdm.h>

#include <pde.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <proj_init_finalize.h>

PetscErrorCode PDEADFDSetData(PDE pde,DM dm,BCList list);
PetscErrorCode PDEADFDSetTimestep(PDE pde,PetscReal dt);

#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  const DMDACoor2d **LA_coor;
  PetscScalar **LA_X;
  PetscScalar *LA_field;
  PetscInt i,j,k,M,N;
  PDE pde;
  DM dm,dmc;
  Vec X,Xlocal,F,coor;
  Mat JA;
  SNES snes;
  PetscInt xm,ym,xs,ys;
  PetscInt nsteps = 40;
  PetscReal timestep = 0.05;
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-nsteps",&nsteps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&timestep,NULL);CHKERRQ(ierr);
  
  M = 129;
  N = 129;
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  
  /* reach in and jank the dm from the semi-lagrangian context (yuck - don't do this) */
  dm = dsl->dm;
  ierr = DMDASetFieldName(dm,0,"phi");CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dm,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);

  
  /* define initial boundary condition */
  ierr = DMGetCoordinateDM(dm,&dmc);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dm,&coor);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm,X,&LA_X);CHKERRQ(ierr);

  ierr = VecZeroEntries(X);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      PetscReal xp = LA_coor[j][i].x;
      PetscReal yp = LA_coor[j][i].y;
      
      
      //LA_X[j][i] = xp*xp*yp + xp*4.0 + yp;
      //if ((i == 0) || (j == 0) || (i == M-1) || (j == N-1)) {
      //  LA_X[j][i] = xp*xp*yp + xp*4.0 + yp;
      //}
      LA_X[j][i] = (1.0 - yp) + 0.1*sin(M_PI*yp)*cos(M_PI*xp);
      if (j==0) LA_X[j][i] = 1.0;
      if (j==N-1) LA_X[j][i] = 0.0;
    }
  }
  ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm,X,&LA_X);CHKERRQ(ierr);
  
  ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  ierr = VecGetArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
  for (i=0; i<xm; i++) {
    for (j=0; j<ym; j++) {
      PetscInt idx = i + j * xm;
      const PetscReal xp = LA_coor[j][i].x;
      const PetscReal yp = LA_coor[j][i].y;
      
      LA_field[2*idx+0] =  (yp-0.5) * sin(M_PI*xp) * 500.0;
      LA_field[2*idx+1] = -(xp-0.5) * sin(M_PI*yp) * 500.0;
    }
  }
  ierr = VecRestoreArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEADVDIFF);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEADFDSetData(pde,dm,NULL);CHKERRQ(ierr);
  ierr = PDEADFDSetTimestep(pde,timestep);CHKERRQ(ierr);
  
  //
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"vel.vtr");CHKERRQ(ierr);
  ierr = VecView(dsl->velocity,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"T_0.vtr");CHKERRQ(ierr);
  ierr = VecView(X,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  //
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  for (k=1; k<nsteps; k++) {
    char filename[128];
  
    PetscPrintf(PETSC_COMM_WORLD,"=== Step %D ===\n",k);
    ierr = DMSemiLagrangianUpdate_DMDA(dsl,timestep,X);CHKERRQ(ierr);
    ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);
    
    ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
    //ierr = VecView(X,viewer);CHKERRQ(ierr);
    
    //
    if (k%1 == 0) {
      PetscSNPrintf(filename,127,"T_%D.vtr",k);
      ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,filename);CHKERRQ(ierr);
      ierr = VecView(X,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    }
    //
  }
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = example1();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

