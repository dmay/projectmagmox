
#include <petsc.h>
#include <proj_init_finalize.h>


int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

