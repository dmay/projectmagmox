
#include <petsc.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>


#include <petscdmtet.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>

#undef __FUNCT__
#define __FUNCT__ "DMCreateGlobalVector_Demo"
PetscErrorCode DMCreateGlobalVector_Demo(DM dm,Vec *_x)
{
  Vec      x;
  PetscErrorCode ierr;
  
  ierr = VecCreate(PetscObjectComm((PetscObject)dm),&x);CHKERRQ(ierr);
  ierr = VecSetDM(x,dm);CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,10);CHKERRQ(ierr);
  ierr = VecSetBlockSize(x,1);CHKERRQ(ierr);
  ierr = VecSetType(x,VECSEQ);CHKERRQ(ierr);
  
  *_x = x;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example1_shell"
PetscErrorCode example1_shell(void)
{
  PetscErrorCode ierr;
  DM dmu,dmp,dms;
  Vec X;
  
  ierr = DMShellCreate(PETSC_COMM_WORLD,&dmu);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmu,"u_");CHKERRQ(ierr);

  ierr = DMShellCreate(PETSC_COMM_WORLD,&dmp);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmp,"p_");CHKERRQ(ierr);
  
  ierr = DMShellSetCreateLocalVector(dmu,DMCreateGlobalVector_Demo);CHKERRQ(ierr);
  ierr = DMShellSetCreateGlobalVector(dmu,DMCreateGlobalVector_Demo);CHKERRQ(ierr);
  
  ierr = DMShellSetCreateLocalVector(dmp,DMCreateGlobalVector_Demo);CHKERRQ(ierr);
  ierr = DMShellSetCreateGlobalVector(dmp,DMCreateGlobalVector_Demo);CHKERRQ(ierr);
  
  ierr = DMCompositeCreate(PETSC_COMM_WORLD,&dms);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmu);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmp);CHKERRQ(ierr);
  ierr = DMSetUp(dms);CHKERRQ(ierr);
  
  ierr = DMGetGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dms,&X);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  ierr = DMDestroy(&dms);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  PetscErrorCode ierr;
  DM dmu,dmp,dms;
  Vec X;
  PetscInt order_k;
  IS *is_stokes;
  PetscBool view = PETSC_FALSE;
  
  order_k = 1;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r1/solcx.1",2,DMTET_CWARP_AND_BLEND,order_k+1,&dmu);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmu,"u_");CHKERRQ(ierr);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,order_k,&dmp);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmp,"p_");CHKERRQ(ierr);

  PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  if (view) {
    ierr = DMTetGeometryView2d_VTK(dmu,"dmg.vtu");CHKERRQ(ierr);
    ierr = DMTetBasisView2d_VTK(dmu,"dmu.vtu");CHKERRQ(ierr);
    ierr = DMTetBasisView2d_VTK(dmp,"dmp.vtu");CHKERRQ(ierr);
  }
  
  ierr = DMCompositeCreate(PETSC_COMM_WORLD,&dms);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmu);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(dms,dmp);CHKERRQ(ierr);
  ierr = DMSetUp(dms);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);

  ierr = DMGetGlobalVector(dms,&X);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dms,&X);CHKERRQ(ierr);

  ierr = DMCompositeGetGlobalISs(dms,&is_stokes);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"u-field\n");
  ierr = ISView(is_stokes[0],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"p-field\n");
  ierr = ISView(is_stokes[1],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = ISDestroy(&is_stokes[0]);CHKERRQ(ierr);
  ierr = ISDestroy(&is_stokes[1]);CHKERRQ(ierr);
  ierr = PetscFree(is_stokes);CHKERRQ(ierr);
  ierr = DMDestroy(&dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  ierr = DMDestroy(&dms);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = example1_shell();CHKERRQ(ierr);
  ierr = example1();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

