#
#  Manufacture solution taken from
#
#  SIAM J. SCI. COMPUT. 2014 Society for Industrial and Applied Mathematics
#  Vol. 36, No. 4, pp. A1960–A1977
#  "ANALYSIS OF BLOCK PRECONDITIONERS FOR MODELS OF COUPLED MAGMA/MANTLE DYNAMICS"
#  SANDER RHEBERGEN†, GARTH N. WELLS, RICHARD F. KATZ, AND ANDREW J. WATHEN
#
#  See Sec 6.1 for problem definition
#
from sympy import *

x = Symbol('x')
y = Symbol('y')
a = Symbol('alpha')
eta = Symbol('eta')


kh = Symbol('k_max')
kl = Symbol('k_min')

c1 = (kh - kl) / (4.0 * tanh(5.0))
c2 = 2.0*(kh - kl) - 2.0*tanh(5.0)*(kl + kh)

k = c1 * ( tanh(10.0*x-5.0) + tanh(10.0*y-5.0) + c2/(kl - kh) + 2.0   )
            


p = -cos(4.0 * pi * x) * cos(2.0 * pi * y)

u = k * p.diff(x) + sin(pi * x) * sin(2.0 * pi * y) + 2.0

v = k * p.diff(y) + 0.5 * cos(pi * x) * cos(2.0 * pi * y) + 2.0

print('-----')
print('k = ',ccode(k))

print('-----')
print('u = ',ccode(u))

print('-----')
print('v = ',ccode(v))

print('-----')
print('p = ',ccode(p))

print('-----')
print('[ccode] grad[0] =', ccode(u.diff(x)) + ';')
print('[ccode] grad[1] =', ccode(u.diff(y)) + ';')
print('[ccode] grad[2] =', ccode(v.diff(x)) + ';')
print('[ccode] grad[3] =', ccode(v.diff(y)) + ';')


divu = u.diff(x) + v.diff(y)

edev_xx = u.diff(x) - (1.0/3.0) * divu
edev_yy = v.diff(y) - (1.0/3.0) * divu
edev_xy = 0.5 * (u.diff(y) + v.diff(x))

bulk = a * divu
s_xx = 2.0 * eta * edev_xx + bulk
s_yy = 2.0 * eta * edev_yy + bulk
s_xy = 2.0 * eta * edev_xy


fu = s_xx.diff(x) + s_xy.diff(y) - p.diff(x)
fv = s_xy.diff(x) + s_yy.diff(y) - p.diff(y)

print('-----')
print('fu = ',ccode(fu))
print('-----')
print('fv = ',ccode(fv))

kdpdx = k*p.diff(x)
kdpdy = k*p.diff(y)

fp = -divu + kdpdx.diff(x) + kdpdy.diff(y)
print('-----')
print('fp = ',ccode(fp))



