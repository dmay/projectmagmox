
#include <petsc.h>
#include <petscdmtet.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>

/*
 [Test]
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmtet_partition.app -dmtet.partition_type.ptscotch  -test_id 1 -dm_view -tet_basis_order 3
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmtet_partition.app -dmtet.partition_type.metis  -test_id 1 -dm_view -tet_basis_order 3
*/
#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  PetscErrorCode ierr;
  DM dm = NULL;
  DM dmp = NULL;
  PetscMPIInt commrank;
  PetscInt degree;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);

  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
    ierr = DMSetUp(dm);CHKERRQ(ierr);
  }
  degree = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-tet_basis_order",&degree,NULL);CHKERRQ(ierr);
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dm,1,DMTET_CWARP_AND_BLEND,degree,&dmp);CHKERRQ(ierr);
  ierr = DMSetUp(dmp);CHKERRQ(ierr);
  ierr = DMView(dmp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
[Test]
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmtet_partition.app -dmtet.partition_type.ptscotch  -test_id 2 -dm_view -tet_basis_order 6
*/
#undef __FUNCT__
#define __FUNCT__ "example2"
PetscErrorCode example2(void)
{
  PetscErrorCode ierr;
  DM dm = NULL;
  DM dmp = NULL;
  PetscMPIInt commrank;
  PetscInt degree;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  if (commrank == 0) {
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-0.1,1.1,0.0,1.0,32,32,PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
    ierr = DMSetUp(dm);CHKERRQ(ierr);
  }
  degree = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-tet_basis_order",&degree,NULL);CHKERRQ(ierr);
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dm,1,DMTET_CWARP_AND_BLEND,degree,&dmp);CHKERRQ(ierr);
  ierr = DMSetUp(dmp);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 [Test]
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 2 ./arch-gnu-c-debug/bin/test_dmtet_partition.app -dmtet.partition_type.ptscotch  -test_id 3 -dm_view -tet_basis_order 5
*/
#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(void)
{
  PetscErrorCode ierr;
  DM dm = NULL;
  DM dmp = NULL;
  PetscMPIInt commrank;
  Vec x,xl;
  PetscScalar *LA_xl;
  PetscInt k,M,degree;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
    ierr = DMSetUp(dm);CHKERRQ(ierr);
  }

  degree = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-tet_basis_order",&degree,NULL);CHKERRQ(ierr);
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dm,1,DMTET_CWARP_AND_BLEND,degree,&dmp);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMSetUp(dmp);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dmp,&x);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmp,&xl);CHKERRQ(ierr);
  
  ierr = VecGetLocalSize(xl,&M);CHKERRQ(ierr);

  /* insert */
  ierr = VecGetArray(xl,&LA_xl);CHKERRQ(ierr);
  for (k=0; k<M; k++) {
    LA_xl[k] = (PetscScalar)(commrank + 1);
  }
  ierr = VecRestoreArray(xl,&LA_xl);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmp,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmp,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  {
    PetscInt nf = 1;
    const char *fname[] = {"field1"};
    
    ierr = DMTetViewFields_VTU(dmp,nf,&x,fname,"insert_");CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dmp,nf,&x,fname,"insert_");CHKERRQ(ierr);
  }

  /* sum */
  ierr = VecZeroEntries(x);CHKERRQ(ierr);
  ierr = VecZeroEntries(xl);CHKERRQ(ierr);
  
  ierr = VecGetArray(xl,&LA_xl);CHKERRQ(ierr);
  for (k=0; k<M; k++) {
    LA_xl[k] = 1.0;
  }
  ierr = VecRestoreArray(xl,&LA_xl);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dmp,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dmp,xl,ADD_VALUES,x);CHKERRQ(ierr);
  {
    PetscInt nf = 1;
    const char *fname[] = {"field1"};
    
    ierr = DMTetViewFields_VTU(dmp,nf,&x,fname,"add_");CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dmp,nf,&x,fname,"add_");CHKERRQ(ierr);
  }
  
  /* insert L-2-G */
  {
    PetscReal *coords;
    
    ierr = DMTetSpaceElement(dmp,0,0,0,0,0,&coords);CHKERRQ(ierr);

    ierr = VecZeroEntries(x);CHKERRQ(ierr);
    ierr = VecZeroEntries(xl);CHKERRQ(ierr);
    ierr = VecGetArray(xl,&LA_xl);CHKERRQ(ierr);
    for (k=0; k<M; k++) {
      PetscReal xx = coords[2*k+0];
      PetscReal yy = coords[2*k+1];
      PetscReal value = PetscSinReal(3.3*M_PI*xx) * PetscCosReal(1.3*M_PI*yy);
      
      LA_xl[k] = value;
    }
    ierr = VecRestoreArray(xl,&LA_xl);CHKERRQ(ierr);
    
    ierr = DMLocalToGlobalBegin(dmp,xl,INSERT_VALUES,x);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(dmp,xl,INSERT_VALUES,x);CHKERRQ(ierr);
    {
      PetscInt nf = 1;
      const char *fname[] = {"field1"};
      
      ierr = DMTetViewFields_VTU(dmp,nf,&x,fname,"insert_f_l2g_");CHKERRQ(ierr);
      ierr = DMTetViewFields_PVTU(dmp,nf,&x,fname,"insert_f_l2g_");CHKERRQ(ierr);
    }
  }

  /* insert setlocal */
  {
    PetscReal *coords;
    PetscInt e,i,nel,nbasis,*element;
    PetscReal bj[30];
    
    ierr = DMTetSpaceElement(dmp,&nel,&nbasis,&element,0,0,&coords);CHKERRQ(ierr);
    
    ierr = VecZeroEntries(x);CHKERRQ(ierr);

    for (e=0; e<nel; e++) {
      PetscInt *ebasis = &element[nbasis*e];
      for (i=0; i<nbasis; i++) {
        PetscReal xx,yy,value;
        PetscInt bidx;
        
        bidx = ebasis[i];
        xx = coords[2*bidx+0];
        yy = coords[2*bidx+1];
        
        value = PetscSinReal(3.3*M_PI*xx) * PetscCosReal(1.3*M_PI*yy);
        bj[i] = value;
      }
      ierr = VecSetValuesLocal(x,nbasis,ebasis,bj,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = VecAssemblyBegin(x);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(x);CHKERRQ(ierr);
    
    {
      PetscInt nf = 1;
      const char *fname[] = {"field1"};
      
      ierr = DMTetViewFields_VTU(dmp,nf,&x,fname,"insert_f_setlocal_");CHKERRQ(ierr);
      ierr = DMTetViewFields_PVTU(dmp,nf,&x,fname,"insert_f_setlocal_");CHKERRQ(ierr);
    }
  }

  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 [Test]
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 2 ./arch-gnu-c-debug/bin/test_dmtet_partition.app -dmtet.partition_type.ptscotch  -test_id 4 -tet_basis_order 1
 */
#undef __FUNCT__
#define __FUNCT__ "example4"
PetscErrorCode example4(void)
{
  PetscErrorCode ierr;
  DM dm = NULL;
  DM dmp = NULL;
  PetscMPIInt commrank;
  Mat A;
  PetscInt degree;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  if (commrank == 0) {
    //ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-0.1,1.1,0.0,1.0,32,32,PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
    ierr = DMSetUp(dm);CHKERRQ(ierr);
  }
  
  degree = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-tet_basis_order",&degree,NULL);CHKERRQ(ierr);
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dm,1,DMTET_CWARP_AND_BLEND,degree,&dmp);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMSetUp(dmp);CHKERRQ(ierr);

  /*
  {
    Vec x;
    PetscInt nf = 1;
    const char *fname[] = {"field1"};
    
    ierr = DMCreateGlobalVector(dmp,&x);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dmp,nf,&x,fname,"l2g_");CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  */
   
  ierr = DMCreateMatrix(dmp,&A);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  
  
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt test_code = 1;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-test_id",&test_code,NULL);CHKERRQ(ierr);
  switch (test_code) {
    case 1:
      ierr = example1();CHKERRQ(ierr);
      break;
    case 2:
      ierr = example2();CHKERRQ(ierr);
      break;
    case 3:
      ierr = example3();CHKERRQ(ierr);
      break;
    case 4:
      ierr = example4();CHKERRQ(ierr);
      break;
    
    default:
    break;
  }
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

