
#include <petsc.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>


#include <petscdmtet.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>

#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  PetscErrorCode ierr;
  DM dmu,dmp;
  Vec Xu,Xp;
  PetscInt order_k;
  
  order_k = 1;
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r1/solcx.1",2,DMTET_CWARP_AND_BLEND,order_k+1,&dmu);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmu,"u_");CHKERRQ(ierr);

  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-1.0,1.0,-1.0,1.0, 3,4, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,order_k,&dmp);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(dmp,"p_");CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dmu,&Xu);CHKERRQ(ierr);
  ierr = VecSetRandom(Xu,NULL);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmp,&Xp);CHKERRQ(ierr);
  ierr = VecSetRandom(Xp,NULL);CHKERRQ(ierr);
  
  ierr = DMTetView_HDF(dmu,"dmu.h5");CHKERRQ(ierr);

  ierr = DMTetSpaceAddGroupNameHDF5("dmu.h5","timestep0");CHKERRQ(ierr);

  ierr = DMTetFieldSpaceView_HDF(dmu,Xu,"timestep0/velocity","dmu.h5");CHKERRQ(ierr);
  
  ierr = DMTetSpaceAddGroupNameHDF5("dmu.h5","timestep1");CHKERRQ(ierr);

  ierr = VecShift(Xu,1.1);CHKERRQ(ierr);  
  ierr = DMTetFieldSpaceView_HDF(dmu,Xu,"timestep1/velocity","dmu.h5");CHKERRQ(ierr);
  
  ierr = DMTetView_HDF(dmp,"dmp.h5");CHKERRQ(ierr);
  ierr = DMTetFieldSpaceView_HDF(dmp,Xp,"pressure","dmp.h5");CHKERRQ(ierr);
  ierr = VecScale(Xp,11.1);CHKERRQ(ierr);
  ierr = DMTetFieldSpaceView_HDF(dmp,Xp,"10xScaledPressure","dmp.h5");CHKERRQ(ierr);
  
  
  ierr = VecDestroy(&Xu);CHKERRQ(ierr);
  ierr = VecDestroy(&Xp);CHKERRQ(ierr);
  ierr = DMDestroy(&dmu);CHKERRQ(ierr);
  ierr = DMDestroy(&dmp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = example1();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

