
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <proj_init_finalize.h>

#undef __FUNCT__
#define __FUNCT__ "test_submesh"
PetscErrorCode test_submesh(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT,dmTsub;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xsub;
  PetscInt nr,regions[] = { 3 };
  Mat inject_sub2g,inject_g2sub;
  
  /* thermal problem */
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  
  /* View mesh */
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);

  nr = sizeof(regions) / sizeof(PetscInt);
  ierr = DMTetCreateSubmeshByRegions(dmT,nr,regions,&dmTsub);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmTsub,&Xsub);CHKERRQ(ierr);

  PetscSNPrintf(prefix,127,"%s_sub_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);

  /* scatter from sub to parent */
  ierr = VecSet(X,-20.0);CHKERRQ(ierr);
  ierr = VecSet(Xsub,111.1);CHKERRQ(ierr);
  ierr = DMCreateInjection(dmT,dmTsub,&inject_sub2g);CHKERRQ(ierr);
  ierr = MatMult(inject_sub2g,Xsub,X);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_sub2p_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);

  {
    VecScatter scat;
    Vec Xunion;

    ierr = VecDuplicate(X,&Xunion);CHKERRQ(ierr);
    
    /* scatter from sub to parent -- additive */
    ierr = VecSet(X,-20.0);CHKERRQ(ierr);
    ierr = VecCopy(X,Xunion);CHKERRQ(ierr);
    ierr = VecSet(Xsub,111.1);CHKERRQ(ierr);

    ierr = MatScatterGetVecScatter(inject_sub2g,&scat);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,Xsub,Xunion,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,Xsub,Xunion,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    
    PetscSNPrintf(prefix,127,"%s_sub2pu_",fieldsT[0]);
    ierr = DMTetViewFields_VTU(dmT,1,&Xunion,fieldsT,prefix);CHKERRQ(ierr);
    
    ierr = VecDestroy(&Xunion);CHKERRQ(ierr);
  }
  
  /* scatter from parent to sub */
  ierr = VecSet(X,33.0);CHKERRQ(ierr);
  ierr = VecSet(Xsub,0.0);CHKERRQ(ierr);
  ierr = DMCreateInjection(dmTsub,dmT,&inject_g2sub);CHKERRQ(ierr);
  ierr = MatMult(inject_g2sub,X,Xsub);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_p2sub_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  
  
  
  /* clean up */
  ierr = MatDestroy(&inject_g2sub);CHKERRQ(ierr);
  ierr = MatDestroy(&inject_sub2g);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsub);CHKERRQ(ierr);
  ierr = DMDestroy(&dmTsub);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_submesh_mpi"
PetscErrorCode test_submesh_mpi(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT_s=NULL,dmT=NULL,dmTsub;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xsub;
  PetscInt nr,regions[] = { 3 };
  Mat inject_sub2g,inject_g2sub;
  PetscMPIInt commrank;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);

  /* thermal problem */
  T_p = 1;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT_s);CHKERRQ(ierr);
    ierr = DMSetUp(dmT_s);CHKERRQ(ierr);
  }
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dmT_s,1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT_s);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);
  
  /* View mesh */
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  nr = sizeof(regions) / sizeof(PetscInt);
  ierr = DMTetCreateSubmeshByRegions(dmT,nr,regions,&dmTsub);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmTsub,&Xsub);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_sub_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);

  /* scatter from sub to parent */
  ierr = VecSet(X,-20.0);CHKERRQ(ierr);
  ierr = VecSet(Xsub,111.1);CHKERRQ(ierr);
  ierr = DMCreateInjection(dmT,dmTsub,&inject_sub2g);CHKERRQ(ierr);
  ierr = MatMult(inject_sub2g,Xsub,X);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_sub2p_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  {
    VecScatter scat;
    Vec Xunion;
    
    ierr = VecDuplicate(X,&Xunion);CHKERRQ(ierr);
    
    /* scatter from sub to parent -- additive */
    ierr = VecSet(X,-20.0);CHKERRQ(ierr);
    ierr = VecCopy(X,Xunion);CHKERRQ(ierr);
    ierr = VecSet(Xsub,111.1);CHKERRQ(ierr);
    
    ierr = MatScatterGetVecScatter(inject_sub2g,&scat);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,Xsub,Xunion,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,Xsub,Xunion,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    
    PetscSNPrintf(prefix,127,"%s_sub2pu_",fieldsT[0]);
    ierr = DMTetViewFields_VTU(dmT,1,&Xunion,fieldsT,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dmT,1,&Xunion,fieldsT,prefix);CHKERRQ(ierr);
    
    ierr = VecDestroy(&Xunion);CHKERRQ(ierr);
  }
  
  /* scatter from parent to sub */
  ierr = VecSet(X,33.0);CHKERRQ(ierr);
  ierr = VecSet(Xsub,0.0);CHKERRQ(ierr);
  ierr = DMCreateInjection(dmTsub,dmT,&inject_g2sub);CHKERRQ(ierr);

  ierr = MatMult(inject_g2sub,X,Xsub);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_p2sub_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  
  /* clean up */
  ierr = MatDestroy(&inject_g2sub);CHKERRQ(ierr);
  ierr = MatDestroy(&inject_sub2g);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsub);CHKERRQ(ierr);
  ierr = DMDestroy(&dmTsub);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func1"
PetscErrorCode func1(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = PetscSinReal(PETSC_PI * x[0]) * x[1] * PetscCosReal(3.0 * PETSC_PI * x[0] * x[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func2"
PetscErrorCode func2(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = PetscSinReal(4.4 * PETSC_PI * x[0]) + 3.3;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func3"
PetscErrorCode func3(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = x[0] * x[1] * x[1] + 2.0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_submesh_mpi_ex2"
PetscErrorCode test_submesh_mpi_ex2(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT_s=NULL,dmT=NULL,dmTsub;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec X,Xsub;
  PetscInt nr,regions[] = { 3 };
  Mat inject_sub2g,inject_g2sub;
  PetscMPIInt commrank;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  /* thermal problem */
  T_p = 1;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r3/wedge.2",
                                     1,DMTET_CWARP_AND_BLEND,T_p,&dmT_s);CHKERRQ(ierr);
    ierr = DMSetUp(dmT_s);CHKERRQ(ierr);
  }
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dmT_s,1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT_s);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);
  
  /* View mesh */
  ierr = DMCreateGlobalVector(dmT,&X);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"%s_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  nr = sizeof(regions) / sizeof(PetscInt);
  ierr = DMTetCreateSubmeshByRegions(dmT,nr,regions,&dmTsub);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmTsub,&Xsub);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_sub_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  
  /* scatter from sub to parent */
  //ierr = VecSet(X,-20.0);CHKERRQ(ierr);
  //ierr = VecSet(Xsub,111.1);CHKERRQ(ierr);
  ierr = DMTetVecTraverse(dmTsub,Xsub,func2,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverse(dmT,X,func3,NULL);CHKERRQ(ierr);
  ierr = DMCreateInjection(dmT,dmTsub,&inject_sub2g);CHKERRQ(ierr);
  //ierr = MatMult(inject_sub2g,Xsub,X);CHKERRQ(ierr);
  {
    VecScatter vscat;
    
    ierr = MatScatterGetVecScatter(inject_sub2g,&vscat);CHKERRQ(ierr);
    ierr = VecScatterBegin(vscat,Xsub,X,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(vscat,Xsub,X,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  }
  PetscSNPrintf(prefix,127,"%s_sub2p_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  
  {
    VecScatter scat;
    Vec Xunion;
    
    ierr = VecDuplicate(X,&Xunion);CHKERRQ(ierr);
    
    /* scatter from sub to parent -- additive */
    ierr = VecSet(X,-20.0);CHKERRQ(ierr);
    ierr = VecCopy(X,Xunion);CHKERRQ(ierr);
    ierr = VecSet(Xsub,111.1);CHKERRQ(ierr);
    
    ierr = MatScatterGetVecScatter(inject_sub2g,&scat);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,Xsub,Xunion,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,Xsub,Xunion,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    
    PetscSNPrintf(prefix,127,"%s_sub2pu_",fieldsT[0]);
    ierr = DMTetViewFields_VTU(dmT,1,&Xunion,fieldsT,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dmT,1,&Xunion,fieldsT,prefix);CHKERRQ(ierr);
    
    ierr = VecDestroy(&Xunion);CHKERRQ(ierr);
  }
  
  /* scatter from parent to sub */
  //ierr = VecSet(X,33.0);CHKERRQ(ierr);
  ierr = DMTetVecTraverse(dmT,X,func1,NULL);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_p_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&X,fieldsT,prefix);CHKERRQ(ierr);

  ierr = VecSet(Xsub,0.0);CHKERRQ(ierr);
  ierr = DMCreateInjection(dmTsub,dmT,&inject_g2sub);CHKERRQ(ierr);
  
  /*
   MatMult uses ADD_VALUES, thus if there are basis counted multiple times  (ncount)
   when defining the scatter, the result at these basis will be multiplicative factor of ncount
   larger than it should be.
  */
  //ierr = MatMult(inject_g2sub,X,Xsub);CHKERRQ(ierr);
  {
    VecScatter vscat;
    
    ierr = MatScatterGetVecScatter(inject_g2sub,&vscat);CHKERRQ(ierr);
    ierr = VecScatterBegin(vscat,X,Xsub,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(vscat,X,Xsub,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  }
  
  PetscSNPrintf(prefix,127,"%s_p2sub_",fieldsT[0]);
  ierr = DMTetViewFields_VTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmTsub,1,&Xsub,fieldsT,prefix);CHKERRQ(ierr);
  
  /* clean up */
  ierr = MatDestroy(&inject_g2sub);CHKERRQ(ierr);
  ierr = MatDestroy(&inject_sub2g);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsub);CHKERRQ(ierr);
  ierr = DMDestroy(&dmTsub);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 [from] inputvalue
 [0] 1  [1] 2  [2] 3  [3] 4  [4] 5  [5] 6  [6] 7  [7] 8
  |      |      |      |      |      |      |      |
  *      *      *      *      *      *      *      *
 [4]    [5]    [3]           [7]    [1]           [0]
 [to]
 output:
    8      6       0      3      1      2      0      5      0      0      0      0
*/
PetscErrorCode test_vs(void)
{
  PetscErrorCode ierr;
  PetscInt *from,*to;
  PetscMPIInt csize,crank;
  PetscInt nto,nfrom,nl_from,nl_to,k,N;
  Vec from_v,to_v;
  IS from_is,to_is;
  VecScatter scat;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&csize);CHKERRQ(ierr);
  if (csize != 3) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Test only valid for comm.size = 3");
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&crank);CHKERRQ(ierr);
  
  if (crank == 0) {
    nfrom = 2;
    ierr = PetscMalloc1(nfrom,&from);CHKERRQ(ierr);
    from[0] = 7;
    from[1] = 5;

    nto = 2;
    ierr = PetscMalloc1(nto,&to);CHKERRQ(ierr);
    to[0] = 0;
    to[1] = 1;
    
    nl_from = 3;
    nl_to = 4;
  } else if (crank == 1) {
    nfrom = 3;
    ierr = PetscMalloc1(nfrom,&from);CHKERRQ(ierr);
    from[0] = 2;
    from[1] = 0;
    from[2] = 1;

    nto = 3;
    ierr = PetscMalloc1(nto,&to);CHKERRQ(ierr);
    to[0] = 3;
    to[1] = 4;
    to[2] = 5;
    
    nl_from = 2;
    nl_to = 4;
  } else {
    nfrom = 1;
    ierr = PetscMalloc1(nfrom,&from);CHKERRQ(ierr);
    from[0] = 4;

    nto = 1;
    ierr = PetscMalloc1(nto,&to);CHKERRQ(ierr);
    to[0] = 7;

    nl_from = 3;
    nl_to = 4;
  }
  
  ierr = VecCreate(PETSC_COMM_WORLD,&from_v);CHKERRQ(ierr);
  ierr = VecSetSizes(from_v,nl_from,PETSC_DETERMINE);CHKERRQ(ierr);
  ierr = VecSetType(from_v,VECMPI);CHKERRQ(ierr);

  ierr = VecGetSize(from_v,&N);CHKERRQ(ierr);
  for (k=0; k<N; k++) {
    ierr = VecSetValue(from_v,k,(PetscReal)(k+1),INSERT_VALUES);CHKERRQ(ierr);
  }
  ierr = VecAssemblyBegin(from_v);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(from_v);CHKERRQ(ierr);
  ierr = VecView(from_v,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = VecCreate(PETSC_COMM_WORLD,&to_v);CHKERRQ(ierr);
  ierr = VecSetSizes(to_v,nl_to,PETSC_DETERMINE);CHKERRQ(ierr);
  ierr = VecSetType(to_v,VECMPI);CHKERRQ(ierr);

  ierr = ISCreateGeneral(PETSC_COMM_SELF,nfrom,from,PETSC_COPY_VALUES,&from_is);CHKERRQ(ierr);
  ierr = ISCreateGeneral(PETSC_COMM_SELF,nto,to,PETSC_COPY_VALUES,&to_is);CHKERRQ(ierr);
  
  ierr = VecScatterCreate(from_v,from_is , to_v,to_is,&scat);CHKERRQ(ierr);
  
  ierr = VecScatterBegin(scat,from_v,to_v,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scat,from_v,to_v,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);

  ierr = VecView(to_v,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = PetscFree(from);CHKERRQ(ierr);
  ierr = PetscFree(to);CHKERRQ(ierr);
  ierr = VecDestroy(&from_v);CHKERRQ(ierr);
  ierr = VecDestroy(&to_v);CHKERRQ(ierr);
  ierr = ISDestroy(&from_is);CHKERRQ(ierr);
  ierr = ISDestroy(&to_is);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = test_submesh();CHKERRQ(ierr);
  } else {
    //ierr = test_vs();CHKERRQ(ierr);
    //ierr = test_submesh_mpi();CHKERRQ(ierr);
    ierr = test_submesh_mpi_ex2();CHKERRQ(ierr);
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

