
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <petscutils.h>
#include <pde.h>
#include <pdedarcy.h>
#include <dmbcs.h>
#include <fe_geometry_utils.h>
#include <inorms.h>
#include <element_container.h>
#include <index.h>

/* private prototypes */
PetscErrorCode PDEDarcyFieldView_AsciiVTU(PDE pde,const char prefix[]);


#define DOMAIN_XMIN  0.08333333333333
#define DOMAIN_XMAX  1.1
#define DOMAIN_YMIN -1.0
#define DOMAIN_YMAX -0.08333333333333
#define SLAB_DIP        45.0
#define PLATE_THICKNESS 0.08333333333333

PetscErrorCode SNESSetNullSpace(DM dmS,SNES snes);


#undef __FUNCT__
#define __FUNCT__ "a_evaluate_quadrature"
PetscErrorCode a_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff,a;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dmS,dmV,dmP;
  PDE pde;
  
  PetscFunctionBegin;
  if (data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected NULL pointer as last argument");

  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"a",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);

  ierr = DMTetGeometryElement(dmP,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dmP,quadrature,&ele_container_g);CHKERRQ(ierr);

  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      a = 1.0;
      coeff[e*nqp + q] = a;
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "f_evaluate_quadrature"
PetscErrorCode f_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dmS,dmV,dmP;
  PDE pde;
  
  PetscFunctionBegin;
  if (data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected NULL pointer as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"f",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dmP,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dmP,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      coeff[2*(e*nqp + q)+0] = 0.0;
      coeff[2*(e*nqp + q)+1] = 0.0;
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "g_evaluate_quadrature"
PetscErrorCode g_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff,g;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dmS,dmV,dmP;
  PDE pde;
  
  PetscFunctionBegin;
  if (data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected NULL pointer as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"g",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dmP,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dmP,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      g = 0.0;
      coeff[e*nqp + q] = g;
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "u_neumann_evaluate_quadrature"
PetscErrorCode u_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,f;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp,nfacets_region,nfacets_f,nbasis_face_f;
  const PetscInt *facet_indices_region;
  DM dmS,dmV,dmP;
  PDE pde;
  BFacetList facet_list;
  PetscReal *normal,*tangent;
  
  
  PetscFunctionBegin;
  if (data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected NULL pointer as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"uN",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  ierr = DMTetGetGeometryBFacetList(dmP,&facet_list);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(facet_list,&nfacets_f,&nbasis_face_f);CHKERRQ(ierr);
  ierr = BFacetListGetOrientations(facet_list,&normal,&tangent);CHKERRQ(ierr);

  printf("q->nfacets %d : f->nfacets %d : f->nbasis %d\n",nfacets,nfacets_f,nbasis_face_f);
  
  ierr = DMGListGetIndices(dmP,"edge:SlabWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (f=0; f<nfacets_region; f++) {
    PetscReal *q_coeff_e;
    PetscInt  fidx;
    //PetscReal *normal_qp;
    
    fidx = facet_indices_region[f];
    
    //normal_qp = &normal[fidx*2];
    
    FIELD_GET_OFFSET(q_coeff,fidx,nqp,2,q_coeff_e);
    for (q=0; q<nqp; q++) {
      PetscReal flux[2];
      //PetscReal *x_qp;
      
      /* get x,y coords at each quadrature point */
      //FIELD_GET_OFFSET(q_coor,fidx,nqp,2,x_qp);
      //printf("%+1.4e %+1.4e %+1.4e %+1.4e 0\n",x_qp[0],x_qp[1],normal_qp[0],normal_qp[1]);
      flux[0] = 1.0;
      flux[1] = 1.0;
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }

  ierr = DMGListGetIndices(dmP,"edge:PlateWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (f=0; f<nfacets_region; f++) {
    PetscReal *q_coeff_e;
    PetscInt  fidx;
    //PetscReal *normal_qp;
    
    fidx = facet_indices_region[f];
    
    //normal_qp = &normal[fidx*2];

    FIELD_GET_OFFSET(q_coeff,fidx,nqp,2,q_coeff_e);
    for (q=0; q<nqp; q++) {
      PetscReal flux[2];
      //PetscReal *x_qp;
      PetscReal x0[2],x1[2],L,Lp;
      
      x0[0] = DOMAIN_XMIN;
      x0[1] = DOMAIN_YMAX;
      x1[0] =  1.0;
      x1[1] = -1.0;
      L = PetscSqrtReal( (x1[0]-x0[0])*(x1[0]-x0[0]) + (x1[1]-x0[1])*(x1[1]-x0[1]) );

      Lp = PetscSqrtReal( (1.1-x0[0])*(1.1-x0[0]) );

      
      /* get x,y coords at each quadrature point */
      //FIELD_GET_OFFSET(q_coor,fidx,nqp,2,x_qp);
      //printf("%+1.4e %+1.4e %+1.4e %+1.4e 1\n",x_qp[0],x_qp[1],normal_qp[0],normal_qp[1]);
      flux[0] = 0.0;
      flux[1] = sqrt(2.0) * (L/Lp);
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "u_neumann_non_matching_evaluate_quadrature"
PetscErrorCode u_neumann_non_matching_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,f;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp,nfacets_region,nfacets_f,nbasis_face_f;
  const PetscInt *facet_indices_region;
  DM dmS,dmV,dmP;
  PDE pde;
  BFacetList facet_list;
  PetscReal *normal,*tangent;
  
  
  PetscFunctionBegin;
  if (data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected NULL pointer as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"uN",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  ierr = DMTetGetGeometryBFacetList(dmP,&facet_list);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(facet_list,&nfacets_f,&nbasis_face_f);CHKERRQ(ierr);
  ierr = BFacetListGetOrientations(facet_list,&normal,&tangent);CHKERRQ(ierr);
  
  printf("q->nfacets %d : f->nfacets %d : f->nbasis %d\n",nfacets,nfacets_f,nbasis_face_f);
  
  ierr = DMGListGetIndices(dmP,"edge:SlabWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (f=0; f<nfacets_region; f++) {
    PetscReal *q_coeff_e;
    PetscInt  fidx;
    //PetscReal *normal_qp;
    
    fidx = facet_indices_region[f];
    
    //normal_qp = &normal[fidx*2];
    
    FIELD_GET_OFFSET(q_coeff,fidx,nqp,2,q_coeff_e);
    for (q=0; q<nqp; q++) {
      PetscReal flux[2];
      //PetscReal *x_qp;
      
      /* get x,y coords at each quadrature point */
      //FIELD_GET_OFFSET(q_coor,fidx,nqp,2,x_qp);
      flux[0] = 1.0;
      flux[1] = 1.0;
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }
  
  ierr = DMGListGetIndices(dmP,"edge:PlateWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (f=0; f<nfacets_region; f++) {
    PetscReal *q_coeff_e;
    PetscInt  fidx;
    //PetscReal *normal_qp;
    
    fidx = facet_indices_region[f];
    
    //normal_qp = &normal[fidx*2];
    
    FIELD_GET_OFFSET(q_coeff,fidx,nqp,2,q_coeff_e);
    for (q=0; q<nqp; q++) {
      PetscReal flux[2];
      //PetscReal *x_qp;
      
      /* get x,y coords at each quadrature point */
      //FIELD_GET_OFFSET(q_coor,fidx,nqp,2,x_qp);
      flux[0] = 0.0;
      flux[1] = 1.0;
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_pin_point_A"
PetscErrorCode EvalDirichletBoundary_pin_point_A(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *data)
{
  *constrain = PETSC_FALSE;
  if (coor[0]-1.0e-6 < DOMAIN_XMIN) {
    if (-coor[1] - 1.0e-6 < -DOMAIN_YMAX) {
      *constrain = PETSC_TRUE;
      *val = 0.0;
      printf("================= pin point p at A (%+1.6e,%+1.6e) =================\n",coor[0],coor[1]);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_pin_point_B"
PetscErrorCode EvalDirichletBoundary_pin_point_B(PetscScalar coor[],PetscInt dof,
                                                 PetscScalar *val,PetscBool *constrain,void *data)
{
  *constrain = PETSC_FALSE;
  if (coor[0]+1.0e-6 > DOMAIN_XMAX) {
    if (-coor[1] + 1.0e-6 > -DOMAIN_YMIN) {
      *constrain = PETSC_TRUE;
      *val = 0.0;
      printf("================= pin point p at B (%+1.6e,%+1.6e) =================\n",coor[0],coor[1]);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_mantle"
PetscErrorCode EvalDirichletBoundary_mantle(PetscScalar coor[],PetscInt dof,
                                                 PetscScalar *val,PetscBool *constrain,void *data)
{
  *constrain = PETSC_FALSE;
  if (coor[0] > 1.0) {
    if (-coor[1] + 1.0e-6 > -DOMAIN_YMIN) {
      *constrain = PETSC_TRUE;
      *val = 0.0;
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_SlabWedge(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  PetscReal depth,slab_dip;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};
  
  slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  depth = -coor[1];
  if (depth > (coor[0]*PetscTanReal(slab_dip) - 1.0e-6)) { /* slab-wedge */
    if ((facet->x1[0] - 1.0e-6 < 1.0) && (facet->x0[0] - 1.0e-6 < 1.0)) { /* hard-coded for 45 degrees */
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_PlateWedge(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  PetscReal depth;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};
  
  depth = -coor[1];
  // top
  if (depth < (PLATE_THICKNESS+1.0e-10)) {
    *tag = PETSC_TRUE;
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_Mantle(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  //PetscReal depth,slab_dip;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};
  
  //depth = -coor[1];
  //slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  
  //right
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) {
    *tag = PETSC_TRUE;
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_MantleWedge(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  //PetscReal depth,slab_dip;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};
  
  //depth = -coor[1];
  //slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  
  if (coor[1] < (DOMAIN_YMIN+1.0e-10)) {
    if ((facet->x1[0] >= 1.0-1.0e-6) && (facet->x0[0] >= 1.0-1.0e-6)) { /* hard-coded for 45 degrees */
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

/*
 [Test]
 ./test_pde_darcy.app  -test_id 1 -dcy_ksp_type fgmres  -dcy_ksp_rtol 1.0e-10 -dcy_ksp_converged_reason -dcy_fieldsplit_u_pc_type lu -dcy_ksp_type fgmres -dcy_fieldsplit_p_ksp_type gcr  -dcy_fieldsplit_p_ksp_rtol 1.0e-12 -dcy_fieldsplit_p_pc_type lu -dcy_ksp_monitor -dcy_pc_fieldsplit_schur_precondition SELFP  -pk 2 -dcy_fieldsplit_p_ksp_constant_null_space -dcy_fieldsplit_p_ksp_monitor_true_residual  -dcy_fieldsplit_p_ksp_max_it 1000 -mode 0
*/
#undef __FUNCT__
#define __FUNCT__ "pde_darcyL2H1_mixedbc"
PetscErrorCode pde_darcyL2H1_mixedbc(void)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS,dm,dmref;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcP;
  SNES snes;
  PetscBool view = PETSC_TRUE;
  PetscInt pk,pkv,pkp,mode;
  const char meshfilename[] = "subfusc/models/wedge-0/meshes/examples/ref.1";
  PetscInt nregions = 1;
  PetscInt regionlist[] = {2};
  Coefficient a,f,g,uN;

  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  mode = 0;
  ierr = PetscOptionsGetInt(NULL,NULL,"-mode",&mode,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  pkv = pk+1;
  pkp = pk;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pkv",&pkv,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-pkp",&pkp,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Darcy form L2H1 : Darcy element: P%D-P%D [L2-H1]\n",pkv,pkp);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  /* Extract a sub mesh from the wedge domain */
  ierr = DMTetCreateSubmeshByRegions(dm,nregions,regionlist,&dmref);CHKERRQ(ierr);

  ierr = DMTetCloneGeometry(dmref,2,DMTET_DG,             pkv,&dmV);CHKERRQ(ierr);
  ierr = DMTetCloneGeometry(dmref,1,DMTET_CWARP_AND_BLEND,pkp,  &dmP);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dmP);CHKERRQ(ierr); /* required for surface integration */

  ierr = DMBCListCreate(dmP,&bcP);CHKERRQ(ierr);

  ierr = DMGListCreate(dmP,0,"edge:SlabWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,1,"edge:PlateWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,2,"edge:Mantle");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,3,"edge:MantleWedge");CHKERRQ(ierr);

  ierr = DMGListCreate(dmP,4,"basis:SlabWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,5,"basis:PlateWedge");CHKERRQ(ierr);

  ierr = DMTetBFacetListAssign_User(dmP,"edge:SlabWedge",  SelectBoundary_SlabWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dmP,"edge:PlateWedge", SelectBoundary_PlateWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dmP,"edge:Mantle",     SelectBoundary_Mantle,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dmP,"edge:MantleWedge",SelectBoundary_MantleWedge,NULL);CHKERRQ(ierr);

  ierr = DMTetBFacetBasisListAssign(dmP,"edge:SlabWedge", "basis:SlabWedge");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssign(dmP,"edge:PlateWedge","basis:PlateWedge");CHKERRQ(ierr);
  
  /* pde for darcy */
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,dmV,dmP,NULL,bcP,DARCY_BFORM_L2H1,&pde);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde,"dcy_");CHKERRQ(ierr);
  
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  
  /* volume coefficients */
  ierr = PDEDarcyGetVCoefficients(pde,&a,&f,&g);CHKERRQ(ierr);
  /* inverse permability */
  ierr = CoefficientSetComputeQuadrature(a,a_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);
  /* u-momentum right hand side */
  ierr = CoefficientSetComputeQuadrature(f,f_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);
  /* p-continuity right hand side */
  ierr = CoefficientSetComputeQuadrature(g,g_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);

  /* surface coefficients */
  ierr = PDEDarcyGetSCoefficients(pde,&uN,NULL);CHKERRQ(ierr);
 
  switch (mode) {
    case 0:
    printf("Mode 0: Pin point (top left) / matching flux u.n\n");
    /* Define Dirichlet bc */
    ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundary_pin_point_A,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(uN,u_neumann_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);
    break;

    case 1:
    printf("Mode 1: Pin point (bottom right) / matching flux u.n\n");
    /* Define Dirichlet bc */
    ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundary_pin_point_B,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(uN,u_neumann_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);
    break;

    case 2:
    printf("Mode 2: Pressure null space / matching flux u.n\n");
    /* Define Dirichlet bc */
    //ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundary_pin_point_A,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(uN,u_neumann_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);
    break;

    case 3:
    printf("Mode 3: Pin point (bottom right) / non-matching flux u.n\n");
    /* Define Dirichlet bc */
    ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundary_pin_point_B,NULL);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(uN,u_neumann_non_matching_evaluate_quadrature,NULL,NULL);CHKERRQ(ierr);
    break;

    default:
    break;
  }
  
  /* Darcy solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  if (view) {
    Vec        Xv,Xp = NULL;
    const char *fields_u[] = { "vD" };
    const char *fields_p[] = { "p" };
    char       prefix[PETSC_MAX_PATH_LEN];

    ierr = PDEDarcyFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);

    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s_",fields_u[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dmV,1,&Xv,fields_u,prefix);CHKERRQ(ierr);
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s_",fields_p[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dmP,1,&Xp,fields_p,prefix);CHKERRQ(ierr);

    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

typedef struct {
  PetscReal phi0,K0,n,delta_rho,g[2];
  PetscInt slab_flux_bc_type;
  PetscReal g0,g1,k;
} TwoPhaseCtx;

#undef __FUNCT__
#define __FUNCT__ "a_evaluate_katz_demo1"
PetscErrorCode a_evaluate_katz_demo1(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff_a,*coeff_k,*coeff_phi,kstar,phi,a;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dmS,dmV,dmP;
  PDE pde;
  /*TwoPhaseCtx *ctx = (TwoPhaseCtx*)data;*/
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type TwoPhaseCtx as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"a",&nen,NULL,NULL,&coeff_a);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"k*",&nen,NULL,NULL,&coeff_k);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",&nen,NULL,NULL,&coeff_phi);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dmP,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dmP,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      
      /*
      phi = ctx->phi0;
      kstar = ctx->K0 * PetscPowReal(phi,ctx->n);
      a = 1.0/kstar;
      */
      
      phi   = 1.0;
      kstar = 1.0;
      a     = 1.0/kstar;
      
      coeff_k[e*nqp + q]   = kstar;
      coeff_phi[e*nqp + q] = phi;
      coeff_a[e*nqp + q]   = a;
      //printf("phi %+1.4e k* %+1.4e a %+1.4e\n",phi,kstar,a);
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "f_evaluate_katz_demo1"
PetscErrorCode f_evaluate_katz_demo1(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff_f,*coeff_k,*coeff_phi,f[2];
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dmS,dmV,dmP;
  PDE pde;
  Coefficient a;
  Quadrature quadrature_a;
  TwoPhaseCtx *ctx = (TwoPhaseCtx*)data;
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type TwoPhaseCtx as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"f",&nen,NULL,NULL,&coeff_f);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dmP,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dmP,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  
  ierr = PDEDarcyGetVCoefficients(pde,&a,NULL,NULL);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(a,&quadrature_a);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_a,"k*",NULL,NULL,NULL,&coeff_k);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_a,"phi",NULL,NULL,NULL,&coeff_phi);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      /*
      f[0] = -coeff_k[e*nqp + q] * (1.0 - ctx->phi0) * ctx->delta_rho * ctx->g[0];
      f[1] = -coeff_k[e*nqp + q] * (1.0 - ctx->phi0) * ctx->delta_rho * ctx->g[1];
      */
      f[0] = (1.0 - ctx->phi0) * 0.0;
      f[1] = (1.0 - ctx->phi0) * 1.0;
      
      coeff_f[2*(e*nqp + q)+0] = f[0];
      coeff_f[2*(e*nqp + q)+1] = f[1];
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "u_neumann_katz_demo1"
PetscErrorCode u_neumann_katz_demo1(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,f;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp,nfacets_region,nfacets_f,nbasis_face_f;
  const PetscInt *facet_indices_region;
  DM dmS,dmV,dmP;
  PDE pde;
  BFacetList facet_list;
  PetscReal *normal,*tangent;
  TwoPhaseCtx *ctx = (TwoPhaseCtx*)data;
  
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type TwoPhaseCtx as last argument");
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCompositeGetEntries(dmS,&dmV,&dmP);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"uN",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  ierr = DMTetGetGeometryBFacetList(dmP,&facet_list);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(facet_list,&nfacets_f,&nbasis_face_f);CHKERRQ(ierr);
  ierr = BFacetListGetOrientations(facet_list,&normal,&tangent);CHKERRQ(ierr);
  
  printf("q->nfacets %d : f->nfacets %d : f->nbasis %d\n",nfacets,nfacets_f,nbasis_face_f);
  
  ierr = DMGListGetIndices(dmP,"edge:SlabWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (f=0; f<nfacets_region; f++) {
    PetscReal *q_coeff_e;
    PetscInt  fidx;
    PetscReal *normal_qp;
    
    fidx = facet_indices_region[f];
    
    normal_qp = &normal[fidx*2];
    
    FIELD_GET_OFFSET(q_coeff,fidx,nqp,2,q_coeff_e);
    for (q=0; q<nqp; q++) {
      PetscReal *x_qp,flux[2],nsum,fcheck,g_bc;
      
      /* get x,y coords at each quadrature point */
      FIELD_GET_OFFSET(q_coor,fidx,nqp,2,x_qp);
      
      nsum = normal_qp[0] + normal_qp[1];
      
      switch (ctx->slab_flux_bc_type) {
        case 0:
        
        g_bc = -ctx->g0;
        flux[0] = g_bc/nsum;
        flux[1] = g_bc/nsum;
        
        fcheck = flux[0]*normal_qp[0] + flux[1]*normal_qp[1];
        //printf("g_bc %+1.4e : f.n = %+1.4e \n",g_bc,fcheck);
        break;

        case 1:
        g_bc = -(ctx->g0 + ctx->g1 * PetscSinReal(ctx->k * x_qp[0]));
        flux[0] = g_bc/nsum;
        flux[1] = g_bc/nsum;
        
        fcheck = flux[0]*normal_qp[0] + flux[1]*normal_qp[1];
        //printf("g_bc %+1.4e : f.n = %+1.4e \n",g_bc,fcheck);
        break;

        case 2:
        g_bc = -(ctx->g1 * PetscSinReal(ctx->k * x_qp[0]));
        flux[0] = g_bc/nsum;
        flux[1] = g_bc/nsum;
        
        fcheck = flux[0]*normal_qp[0] + flux[1]*normal_qp[1];
        printf("g_bc %+1.4e : f.n = %+1.4e \n",g_bc,fcheck);
        break;
        
        default:
          flux[0] = 0.0;
          flux[1] = 0.0;
          SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Valid values for ctx->slab_flux_bc_type are 0,1,2. Found %D",ctx->slab_flux_bc_type);
        break;
      }
      
      //printf("%+1.4e %+1.4e %+1.4e %+1.4e 0\n",x_qp[0],x_qp[1],normal_qp[0],normal_qp[1]);
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }
  
  ierr = DMGListGetIndices(dmP,"edge:PlateWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (f=0; f<nfacets_region; f++) {
    PetscReal *q_coeff_e;
    PetscInt  fidx;
    
    fidx = facet_indices_region[f];
    
    FIELD_GET_OFFSET(q_coeff,fidx,nqp,2,q_coeff_e);
    for (q=0; q<nqp; q++) {
      PetscReal flux[2];
      
      //printf("%+1.4e %+1.4e %+1.4e %+1.4e 1\n",x_qp[0],x_qp[1],normal_qp[0],normal_qp[1]);
      flux[0] = 0.0;
      flux[1] = 1.0 - ctx->phi0;
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SNESSetNullSpace"
PetscErrorCode SNESSetNullSpace(DM dmS,SNES snes)
{
  Vec constant,Xv,Xp;
  MatNullSpace nsp;
  Mat J;
  PetscInt N;
  PetscErrorCode ierr;
  
  ierr = DMCreateGlobalVector(dmS,&constant);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dmS,constant,&Xv,&Xp);CHKERRQ(ierr);
  ierr = VecGetSize(Xp,&N);CHKERRQ(ierr);
  ierr = VecSet(Xp, 1.0/PetscSqrtReal((PetscScalar)(1.0*N)) );CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(dmS,constant,&Xv,&Xp);CHKERRQ(ierr);

  ierr = MatNullSpaceCreate(PETSC_COMM_WORLD,PETSC_FALSE,1,&constant,&nsp);CHKERRQ(ierr);
  ierr = VecDestroy(&constant);CHKERRQ(ierr);
  
  ierr = SNESGetJacobian(snes,&J,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = MatSetNullSpace(J,nsp);CHKERRQ(ierr);
  ierr = MatNullSpaceDestroy(&nsp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 ./arch-gnu-c-debug/bin/test_pde_darcy.app -test_id 2  -dcy_ksp_type fgmres  -dcy_ksp_rtol 1.0e-10 -dcy_ksp_converged_reason -dcy_fieldsplit_u_pc_type lu -dcy_ksp_type gcr -dcy_fieldsplit_p_ksp_type gcr  -dcy_fieldsplit_p_ksp_rtol 1.0e-12 -dcy_fieldsplit_p_pc_type gamg  -dcy_ksp_monitor -dcy_pc_fieldsplit_schur_precondition SELFP  -pk 2  -dcy_fieldsplit_p_ksp_converged_reason  -dcy_fieldsplit_p_ksp_max_it 10000 -slab_flux_bc_type 0  -dcy_fieldsplit_p_ksp_max_it 1000 -dcy_ksp_view -dcy_ksp_rtol 1.0e-8 -dcy_fieldsplit_p_ksp_rtol 1.0e-6  -dcy_fieldsplit_p_mg_coarse_pc_type svd -dcy_fieldsplit_p_ksp_monitor_true_residual

The option
 -slab_flux_balanced
will compute a value of g0 which matches the flux out of the wedge and into the plate.

*/
#undef __FUNCT__
#define __FUNCT__ "pde_darcyL2H1_katz_demo1"
PetscErrorCode pde_darcyL2H1_katz_demo1(void)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS,dm,dmref;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcP;
  SNES snes;
  PetscBool balance,view = PETSC_TRUE;
  PetscInt pk,pkv,pkp,nelements;
  const char meshfilename[] = "subfusc/models/wedge-0/meshes/examples/ref.1";
  PetscInt nregions = 1;
  PetscInt regionlist[] = {2};
  Coefficient a,f,g,uN;
  Quadrature quadrature;
  TwoPhaseCtx ctx;
  
  ctx.phi0      = 0.1;
  ctx.K0        = 1.0e-2;
  ctx.n         = 3.0;
  ctx.delta_rho = 200.0;
  ctx.g[0]      = 0.0;
  ctx.g[1]      = -9.8;
  
  ctx.slab_flux_bc_type = 0;
  ctx.g0 = 1.0;
  ctx.g1 = 1.0/100.0;
  ctx.k  = 4.3 * PETSC_PI;
  ierr = PetscOptionsGetInt(NULL,NULL,"-slab_flux_bc_type",&ctx.slab_flux_bc_type,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-slab_flux_bc_g0",&ctx.g0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-slab_flux_bc_g1",&ctx.g1,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-slab_flux_bc_k",&ctx.k,NULL);CHKERRQ(ierr);

  balance = PETSC_FALSE;
  ierr = PetscOptionsGetBool(NULL,NULL,"-slab_flux_balanced",&balance,NULL);CHKERRQ(ierr);
  if (balance && (ctx.slab_flux_bc_type == 0)) {
    PetscReal x0[2],x1[2],L,Lp,g_surface;
    
    x0[0] = DOMAIN_XMIN;
    x0[1] = DOMAIN_YMAX;
    x1[0] =  1.0;
    x1[1] = -1.0;
    L = PetscSqrtReal( (x1[0]-x0[0])*(x1[0]-x0[0]) + (x1[1]-x0[1])*(x1[1]-x0[1]) );
    Lp = PetscSqrtReal( (1.1-x0[0])*(1.1-x0[0]) );

    g_surface = Lp * (1.0 - ctx.phi0);
    
    ctx.g0 = g_surface/L;
  } else if (balance && (ctx.slab_flux_bc_type != 0)) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"No support to match the slab-wedge flux for anything other than -slab_flux_bc_type 0");
  }

  
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  pkv = pk+1;
  pkp = pk;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pkv",&pkv,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-pkp",&pkp,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# <Katz demo 1> Darcy form L2H1 : Darcy element: P%D-P%D [L2-H1]\n",pkv,pkp);
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  /* Extract a sub mesh from the wedge domain */
  ierr = DMTetCreateSubmeshByRegions(dm,nregions,regionlist,&dmref);CHKERRQ(ierr);
  
  ierr = DMTetCloneGeometry(dmref,2,DMTET_DG,             pkv,&dmV);CHKERRQ(ierr);
  ierr = DMTetCloneGeometry(dmref,1,DMTET_CWARP_AND_BLEND,pkp,  &dmP);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dmP);CHKERRQ(ierr); /* required for surface integration */
  
  ierr = DMBCListCreate(dmP,&bcP);CHKERRQ(ierr);
  
  ierr = DMGListCreate(dmP,0,"edge:SlabWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,1,"edge:PlateWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,2,"edge:Mantle");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,3,"edge:MantleWedge");CHKERRQ(ierr);
  
  ierr = DMGListCreate(dmP,4,"basis:SlabWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dmP,5,"basis:PlateWedge");CHKERRQ(ierr);
  
  ierr = DMTetBFacetListAssign_User(dmP,"edge:SlabWedge",  SelectBoundary_SlabWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dmP,"edge:PlateWedge", SelectBoundary_PlateWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dmP,"edge:Mantle",     SelectBoundary_Mantle,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dmP,"edge:MantleWedge",SelectBoundary_MantleWedge,NULL);CHKERRQ(ierr);
  
  ierr = DMTetBFacetBasisListAssign(dmP,"edge:SlabWedge", "basis:SlabWedge");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssign(dmP,"edge:PlateWedge","basis:PlateWedge");CHKERRQ(ierr);
  
  /* pde for darcy - note that the pressure BC passed in is NULL */
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,dmV,dmP,NULL,NULL,DARCY_BFORM_L2H1,&pde);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde,"dcy_");CHKERRQ(ierr);
  
  ierr = PDEDarcyGetVCoefficients(pde,&a,NULL,NULL);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(a,&quadrature);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmV,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature,nelements,"k*",1);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature,nelements,"phi",1);CHKERRQ(ierr);
  
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  
  /* volume coefficients */
  ierr = PDEDarcyGetVCoefficients(pde,&a,&f,&g);CHKERRQ(ierr);
  /* inverse permability */
  ierr = CoefficientSetComputeQuadrature(a,a_evaluate_katz_demo1,NULL,(void*)&ctx);CHKERRQ(ierr);
  /* u-momentum right hand side */
  ierr = CoefficientSetComputeQuadrature(f,f_evaluate_katz_demo1,NULL,(void*)&ctx);CHKERRQ(ierr);
  /* p-continuity right hand side */
  ierr = CoefficientSetComputeQuadrature(g,g_evaluate_quadrature,NULL,(void*)NULL);CHKERRQ(ierr);
  
  /* surface coefficients */
  ierr = PDEDarcyGetSCoefficients(pde,&uN,NULL);CHKERRQ(ierr);
  
  /* Define Dirichlet bc */
  ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundary_pin_point_B,NULL);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadrature(uN,u_neumann_katz_demo1,NULL,(void*)&ctx);CHKERRQ(ierr);
  
  /* Darcy solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  /* ierr = SNESSetNullSpace(dmS,snes);CHKERRQ(ierr); */// deprecated //
  ierr = SaddlePointSetConstantNullSpace_SNES(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  if (view) {
    ierr = PDEDarcyFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
  }
  
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscInt mxy,tid;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  mxy = 6;
  ierr = PetscOptionsGetInt(NULL,NULL,"-mxy",&mxy,NULL);CHKERRQ(ierr);
  tid = 2;
  ierr = PetscOptionsGetInt(NULL,NULL,"-test_id",&tid,NULL);CHKERRQ(ierr);

  switch (tid) {
    case 1:
    ierr = pde_darcyL2H1_mixedbc();CHKERRQ(ierr);
    break;
    case 2:
    ierr = pde_darcyL2H1_katz_demo1();CHKERRQ(ierr);
    break;
    
    default:
    break;
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
  return(0);
}


