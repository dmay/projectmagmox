

#include <petsc.h>
#include <petscdm.h>

#include <dmbcs.h>
#include <dmsl.h>
#include <proj_init_finalize.h>

/* 
 
 Impose BC on c_bulk
 Define IC on c_bulk

 Define IC for cs,cl via Extract() method
 Define evaluateBC via Extract() method
 
 for each time step
   decmpose c_bulk into phi.cl and (1-phi).cs
 
   copy cs -> phi_dep_s
   copy cl -> phi_dep_l
 
   update cs
   update cl
 
  sum result into c_bulk
 
*/


PetscErrorCode box(PetscReal pos[],PetscBool *inside,void *ctx)
{
  *inside = PETSC_TRUE;
  
  if ((0.0 - pos[0]) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((pos[0] - 1.0) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((0.0 - pos[1]) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((pos[1] - 1.0) > 1.0e-10) { *inside = PETSC_FALSE; }
  
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_rigidRotation_acwise(PetscReal pos[],PetscReal value[])
{
  // solid body rotation about origin
  value[0] = -(pos[1]-0.5);
  value[1] =  (pos[0]-0.5);
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_rigidRotation_cwise(PetscReal pos[],PetscReal value[])
{
  // solid body rotation about origin (clock-wise)
  value[0] =  (pos[1]-0.5);
  value[1] = -(pos[0]-0.5);
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_peak_l(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  G_xy = 0.0;
  
  /* smooth peak */
  xn0[0] = 0.5;
  xn0[1] = 0.75;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 0.25*(1.0 + cos(M_PI*r_xy));
  }
  
  *constrain = PETSC_TRUE;
  *value = G_xy;
  
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_peak_s(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  G_xy = 0.0;
  
  /* smooth peak */
  xn0[0] = 0.5;
  xn0[1] = 0.25;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 0.25*(1.0 + cos(M_PI*r_xy));
  }
  
  *constrain = PETSC_TRUE;
  *value = G_xy;
  
  PetscFunctionReturn(0);
}

PetscErrorCode ExtractPhaseConcentrations(PetscReal c_bulk[],PetscReal P[],PetscReal T[],PetscReal phi[],PetscReal c_l[],PetscReal c_s[])
{
  PetscReal phi_ = 0.1;
  PetscReal D_   = 0.333;
  
  c_l[0] = c_bulk[0] / (phi_ + (1.0 - phi_)*D_);
  c_s[0] = D_ * c_bulk[0] / (phi_ + (1.0 - phi_)*D_);
  phi[0] = phi_;
  
  PetscFunctionReturn(0);
}

PetscErrorCode Decompose(DMSemiLagrangian dsl_l,DMSemiLagrangian dsl_s,Vec v_cbulk,Vec v_cl,Vec v_cs)
{
  DM dm;
  PetscInt nx,ny,i,j;
  Vec coor;
  const PetscScalar *LA_coor;
  PetscScalar *LA_field,*LA_field_l,*LA_field_s;
  PetscErrorCode ierr;
  
  ierr = DMDASemiLagrangianGetSLDM(dsl_l,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);
  VecGetArray(v_cbulk,&LA_field);
  VecGetArray(v_cl,&LA_field_l);
  VecGetArray(v_cs,&LA_field_s);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      PetscReal cbulk,cl,cs,phi;
      
      cbulk = LA_field[idx];
      ierr = ExtractPhaseConcentrations(&cbulk,NULL,NULL,&phi,&cl,&cs);CHKERRQ(ierr);
      LA_field_l[idx] = cl * phi;
      LA_field_s[idx] = cs * (1.0 - phi);
    }
  }
  VecRestoreArray(v_cs,&LA_field_s);
  VecRestoreArray(v_cl,&LA_field_l);
  VecRestoreArray(v_cbulk,&LA_field);
  VecRestoreArrayRead(coor,&LA_coor);

  PetscFunctionReturn(0);
}

PetscErrorCode Fuse(DMSemiLagrangian dsl_l,DMSemiLagrangian dsl_s,Vec v_cbulk,Vec v_cl,Vec v_cs)
{
  PetscErrorCode ierr;
  
  ierr = VecZeroEntries(v_cbulk);CHKERRQ(ierr);
  ierr = VecAXPY(v_cbulk,1.0,v_cl);CHKERRQ(ierr);
  ierr = VecAXPY(v_cbulk,1.0,v_cs);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_multiphase"
PetscErrorCode example_multiphase(void)
{
  DMSemiLagrangian dsl_l,dsl_s;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  char filename[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;
  Vec phi,phi_l,phi_s,coor;
  const PetscScalar *LA_coor;
  PetscScalar *LA_field;
  PetscInt i,j,k,M,N,nx,ny;
  DM dm;
  PetscErrorCode (*inside_domain)(PetscReal*,PetscBool*,void*);
  PetscErrorCode (*dirichlet_evaluator_l)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*);
  PetscErrorCode (*dirichlet_evaluator_s)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator_l)(PetscReal*,PetscReal*);
  PetscErrorCode (*vel_evaluator_s)(PetscReal*,PetscReal*);
  PetscInt model,nrev,nsteps_per_rev;
  PetscScalar sum,dx,dy,dv;
  
  model = 0;
  PetscOptionsGetInt(NULL,NULL,"-model",&model,NULL);
  M = 129;
  N = 129;
  PetscOptionsGetInt(NULL,NULL,"-nx",&M,NULL);
  PetscOptionsGetInt(NULL,NULL,"-ny",&N,NULL);
  
  dx = (xr[1] - xr[0])/((PetscReal)(M-1));
  dy = (yr[1] - yr[0])/((PetscReal)(N-1));
  dv = dx * dy;
  
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl_l);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianSetType(dsl_l,SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE);CHKERRQ(ierr);

  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl_s);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianSetType(dsl_s,SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE);CHKERRQ(ierr);

  inside_domain         = box;
  dirichlet_evaluator_l = dirichlet_evaluator_peak_l;
  dirichlet_evaluator_s = dirichlet_evaluator_peak_s;
  vel_evaluator_l       = vel_evaluator_rigidRotation_acwise;
  vel_evaluator_s       = vel_evaluator_rigidRotation_cwise;

  
  ierr = DMDASemiLagrangian_FVConservative_SetDomainIdentifier(dsl_l,inside_domain,NULL);CHKERRQ(ierr);
  ierr = DMDASemiLagrangian_FVConservative_SetDomainIdentifier(dsl_s,inside_domain,NULL);CHKERRQ(ierr);
  
  ierr = DMDASemiLagrangian_FVConservative_SetDirichletEvaluator(dsl_l,dirichlet_evaluator_l,NULL);CHKERRQ(ierr);
  ierr = DMDASemiLagrangian_FVConservative_SetDirichletEvaluator(dsl_s,dirichlet_evaluator_s,NULL);CHKERRQ(ierr);
  
  /* set initial velocity field */
  // liquid
  ierr = DMDASemiLagrangianGetVertexDM(dsl_l,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);
  
  VecGetArray(dsl_l->velocity,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      const PetscReal *pos = &LA_coor[2*idx];
      
      ierr = vel_evaluator_l((PetscReal*)pos,&LA_field[2*idx+0]);CHKERRQ(ierr);
    }
  }
  VecRestoreArray(dsl_l->velocity,&LA_field);
  VecRestoreArrayRead(coor,&LA_coor);

  // solid
  ierr = DMDASemiLagrangianGetVertexDM(dsl_s,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);
  
  VecGetArray(dsl_s->velocity,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      const PetscReal *pos = &LA_coor[2*idx];
      
      ierr = vel_evaluator_s((PetscReal*)pos,&LA_field[2*idx+0]);CHKERRQ(ierr);
    }
  }
  VecRestoreArray(dsl_s->velocity,&LA_field);
  VecRestoreArrayRead(coor,&LA_coor);

  /* set initial phi */
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl_l,&phi);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl_l,&phi_l);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl_s,&phi_s);CHKERRQ(ierr);
  
  
  /* define bulk */
  ierr = DMDASemiLagrangianGetSLDM(dsl_l,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);
  VecGetArray(phi,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscReal G_xy;
      PetscBool constrain;
      PetscInt idx = i + j * nx;
      const PetscReal *xn = &LA_coor[2*idx];
      
      ierr = dirichlet_evaluator_s((PetscReal*)xn,0.0,&constrain,&G_xy,NULL);CHKERRQ(ierr); // ?
      LA_field[idx] = G_xy;
    }
  }
  VecRestoreArray(phi,&LA_field);
  VecRestoreArrayRead(coor,&LA_coor);

  // c_bulk -> (phi.c_l, (1-phi).c_s)
  ierr = Decompose(dsl_l,dsl_s,phi,phi_l,phi_s);CHKERRQ(ierr);

  /* copy initial condition into SL implementations */
  ierr = VecCopy(phi_l,dsl_l->phi_dep);CHKERRQ(ierr);
  ierr = VecCopy(phi_s,dsl_s->phi_dep);CHKERRQ(ierr);

  
  // liquid
  ierr = DMSLSetup(dsl_l);CHKERRQ(ierr);
  ierr = VecCopy(dsl_l->phi_dep,phi_l);CHKERRQ(ierr);
  
  ierr = SLDMView(dsl_l,"dmsl_l_0");CHKERRQ(ierr);
  ierr = ConservativeSLView(dsl_l,"dmcsldomain_ic_l");CHKERRQ(ierr);

  // solid
  ierr = DMSLSetup(dsl_s);CHKERRQ(ierr);
  ierr = VecCopy(dsl_s->phi_dep,phi_s);CHKERRQ(ierr);
  
  ierr = SLDMView(dsl_s,"dmsl_s_0");CHKERRQ(ierr);
  ierr = ConservativeSLView(dsl_s,"dmcsldomain_ic_s");CHKERRQ(ierr);
  
  ierr = PetscSNPrintf(filename,127,"cbulk_%D.vtr",0);CHKERRQ(ierr);
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,filename);CHKERRQ(ierr);
  ierr = VecView(phi,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  ierr = VecSum(phi,&sum);
  PetscPrintf(PETSC_COMM_WORLD,"Step %.4D : \\sum c_bulk dv = %+1.4e\n",0,sum*dv);

  
  nrev = 4;
  nsteps_per_rev = 160;
  for (k=1; k<=nrev*nsteps_per_rev; k++) {
    PetscReal dt;
    
    ierr = Decompose(dsl_l,dsl_s,phi,phi_l,phi_s);CHKERRQ(ierr);
    
    dt = 2.0 * M_PI / ((PetscReal)nsteps_per_rev);
    ierr = DMDASemiLagrangianUpdate(dsl_l,(k-1)*dt,dt,phi_l);CHKERRQ(ierr);
    ierr = DMDASemiLagrangianUpdate(dsl_s,(k-1)*dt,dt,phi_s);CHKERRQ(ierr);

    ierr = Fuse(dsl_l,dsl_s,phi,phi_l,phi_s);CHKERRQ(ierr);

    
    
    if (k%(nsteps_per_rev/5) == 0) {

      ierr = VecSum(phi,&sum);
      PetscPrintf(PETSC_COMM_WORLD,"Step %.4D : \\sum c_bulk dv = %+1.4e\n",k,sum*dv);
      
      //printf("*** dumping vts at step %d\n",k);

      PetscSNPrintf(filename,127,"cbulk_%D.vtr",k);
      
      ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,filename);CHKERRQ(ierr);
      ierr = VecView(phi,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      
      
      PetscSNPrintf(filename,127,"dmsl_l_%D",k);
      ierr = SLDMView(dsl_l,filename);CHKERRQ(ierr);

      PetscSNPrintf(filename,127,"dmsl_s_%D",k);
      ierr = SLDMView(dsl_s,filename);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = example_multiphase();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

