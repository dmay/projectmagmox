

#include <petsc.h>
#include <proj_init_finalize.h>
#include <cslpoly.h>
#include <multicomponent.h>


/*
 Create the multi-component object
 Set 3 componenets
 View multi-component object
 Destroy multi-component object
 */
#undef __FUNCT__
#define __FUNCT__ "demo1"
PetscErrorCode demo1(void)
{
  PetscErrorCode ierr;
  TPMC m;
  PetscBool active[2];
  
  ierr = TPMCCreate(PETSC_COMM_WORLD,&m);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"H2O");CHKERRQ(ierr);
  active[0] = PETSC_TRUE; active[1] = PETSC_FALSE;
  ierr = TPMCComponentSetPhaseActive(m,0,active);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"Crf");CHKERRQ(ierr);
  active[0] = PETSC_TRUE; active[1] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,1,active);CHKERRQ(ierr);
  ierr = TPMCComponentEliminate(m,1);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"C");CHKERRQ(ierr);
  active[0] = PETSC_FALSE; active[1] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,2,active);CHKERRQ(ierr);

  //ierr = TPMCSetup_ActiveSets(m);CHKERRQ(ierr);
  //ierr = TPMCSetup(m);CHKERRQ(ierr);
  
  ierr = TPMCView(m);CHKERRQ(ierr);
  ierr = TPMCDestroy(&m);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#define POROSITY 0.1

/*
 Assumes the following simplied partition
 
 c_s = D c_l, where D is a constant
 
 using
 c_bulk = phi c_l + (1-phi) c_s
 where
 phi = constant
 
 we can solve directly for c_s and c_l
 */
#undef __FUNCT__
#define __FUNCT__ "UserThermoCalcModule_example1"
PetscErrorCode UserThermoCalcModule_example1(
                                             const PetscReal P[], const PetscReal T[],
                                             const PetscInt ncomponents,
                                             const PetscReal C_bulk[],
                                             PetscReal C_liquid[],PetscReal C_solid[],
                                             PetscReal *phi,
                                             PetscBool *valid,
                                             void *ctx)
{
  PetscReal porosity;
  PetscReal D;
  PetscReal c_bulk_H2O,c_l,c_s;
  
  porosity = POROSITY;
  D = 0.003/0.001;
  
  c_bulk_H2O = C_bulk[0]; /* H2O */
  
  c_l = c_bulk_H2O / (porosity + (1.0 - porosity)*D);
  c_s = D * c_bulk_H2O / (porosity + (1.0 - porosity)*D);
  
  /* note - the second component is eliminated and not updated here - we just set its value to zero */
  C_liquid[0] = c_l;
  C_liquid[1] = 0.0;

  C_solid[0]  = c_s;
  C_solid[1]  = 0.0;
  
  *phi = porosity;
  *valid = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "box_polygon"
PetscErrorCode box_polygon(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 4;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);
  
  coor[2*0+0] = 0.0;       coor[2*0+1] = 0.0;    edge[0] = 0;
  coor[2*1+0] = 1.0;       coor[2*1+1] = 0.0;    edge[1] = 1;
  coor[2*2+0] = 1.0;       coor[2*2+1] = 1.0;    edge[2] = 2;
  coor[2*3+0] = 0.0;       coor[2*3+1] = 1.0;    edge[3] = 3;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "evaluator_expy"
PetscErrorCode evaluator_expy(PetscReal xn[],PetscReal *value)
{
  *value = POROSITY * (xn[1] * 0.001) + (1.0-POROSITY)*(xn[1] * 0.003);
  PetscFunctionReturn(0);
}

void F(const PetscReal coor[],const PetscReal n[],const PetscReal vel[],PetscReal v[],void *ctx)
{
  evaluator_expy((PetscReal*)coor,&v[0]);
  v[1] = 0.8;
}

#undef __FUNCT__
#define __FUNCT__ "demo3_column"
PetscErrorCode demo3_column(void)
{
  PetscErrorCode ierr;
  TPMC m;
  PetscBool active[PHASE_MAX];
  PetscReal ds;
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time,dt;
  CSLPoly csl;
  PetscInt i,k;
  
  ds = 1.0/32.0;
  
  ierr = TPMCCreate(PETSC_COMM_WORLD,&m);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"H2O");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,0,active);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"RF");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,1,active);CHKERRQ(ierr);
  
  ierr = TPMCSetThermoDynamicMethod(m,UserThermoCalcModule_example1,NULL);CHKERRQ(ierr);

  ierr = box_polygon(&npoly,&polycoor,&edgelabel);CHKERRQ(ierr);
  ierr = TPMCConfigureCSLPoly(m,npoly,polycoor,edgelabel,ds,2,2);CHKERRQ(ierr);
  ierr = TPMCSetup(m);CHKERRQ(ierr);
  
  ierr = TPMCView(m);CHKERRQ(ierr);
  
  
  /* initial conditions */
  {
    PetscInt c;
    
    csl = m->csl[PHASE_IDX_LIQUID];
    for (c=0; c<csl->ncells; c++) {
      PetscReal *coor,v;
      
      coor = csl->cell_list[c]->coor;

      
      // CH2O bulk
      evaluator_expy(coor,&v);
      m->cbar_c[m->ncomponents*c + 0] = v;
      // CRF bulk
      m->cbar_c[m->ncomponents*c + 1] = 0.8;
    }
  }
  
  /* P-T */
  for (i=0; i<m->nvert; i++) {
    m->temperature_v[i] = 1200.0;
    m->pressure_v[i] = 1.0;
  }
  
  ierr = TPMCEquilibrateInitialCondition(m);CHKERRQ(ierr);
  
  /* velocity */
  csl = m->csl[PHASE_IDX_LIQUID];
  for (i=0; i<csl->nvert; i++) {
    csl->vel_v[2*i+0] = 0.0;
    csl->vel_v[2*i+1] = 1.0e-8;
  }

  csl = m->csl[PHASE_IDX_SOLID];
  for (i=0; i<csl->nvert; i++) {
    csl->vel_v[2*i+0] = 0.0;
    csl->vel_v[2*i+1] = -0.4;
  }
  
  
  dt = 2.0 * M_PI / ((PetscReal)160.0);
  time = dt;
  for (k=1; k<=70; k++) {
    char prefix[300];

    
    ierr = TPMCUpdateVelocity(m);CHKERRQ(ierr);

    ierr = TPMCInitializeBC(m);CHKERRQ(ierr);
    ierr = TPMCGenericFacetBCIterator(m,2,F,NULL);CHKERRQ(ierr);
    ierr = TPMCGenericFacetBCIterator(m,0,F,NULL);CHKERRQ(ierr);
    ierr = TPMCVerifyBC(m);CHKERRQ(ierr);
    
    
    ierr = TPMCSolve(m,k,dt,time);CHKERRQ(ierr);
    
    sprintf(prefix,"demo3_s%d.vts",k);
    ierr = TPMCViewAllVTS(m,prefix);CHKERRQ(ierr);

    csl = m->csl[PHASE_IDX_SOLID];
    sprintf(prefix,"S%.6d.vts",k);
    CSLPolyCellViewVTS(csl,prefix);
    //CSLPolyFacetViewVTS(csl,"fS.vtu");

    
    csl = m->csl[PHASE_IDX_LIQUID];
    sprintf(prefix,"L%.6d.vts",k);
    CSLPolyCellViewVTS(csl,prefix);
    //CSLPolyFacetViewVTS(csl,"fL.vtu");

    time += dt;
  }
  
  
  ierr = TPMCDestroy(&m);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "UserThermoCalcModule_example2"
PetscErrorCode UserThermoCalcModule_example2(
                                             const PetscReal P[], const PetscReal T[],
                                             const PetscInt ncomponents,
                                             const PetscReal C_bulk[],
                                             PetscReal C_liquid[],PetscReal C_solid[],
                                             PetscReal *phi,
                                             PetscBool *valid,
                                             void *ctx)
{
  PetscReal porosity = 0.1;
  
  C_liquid[0] = C_bulk[0]/porosity; // h2o
  C_solid[0]  = 0.0;

  C_liquid[1] = 0.0; // rf
  C_solid[1]  = C_bulk[1]/(1.0-porosity);

  /* The third component is eliminated - apply unity sum */
  C_liquid[2] = 1.0 - C_liquid[0] - C_liquid[1]; // ft
  C_solid[2]  = 1.0 - C_solid[0]  - C_solid[1];
  
  /*
  printf("%+1.4e %+1.4e | %+1.4e %+1.4e | %+1.4e %+1.4e \n",C_liquid[0],C_solid[0],
         C_liquid[1],C_solid[1], C_liquid[2],C_solid[2]);
  */
   
  *phi = porosity;
  *valid = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "UserThermoCalcModule_example2a"
PetscErrorCode UserThermoCalcModule_example2a(
                                             const PetscReal P[], const PetscReal T[],
                                             const PetscInt ncomponents,
                                             const PetscReal C_bulk[],
                                             PetscReal C_liquid[],PetscReal C_solid[],
                                             PetscReal *phi,
                                             PetscBool *valid,
                                             void *ctx)
{
  PetscReal porosity = 0.1;
  
  C_liquid[0] = C_bulk[0]/porosity; // h2o
  C_solid[0]  = 0.0;

  C_liquid[2] = 1.0 - C_bulk[0]/porosity; // ft
  C_solid[2]  = 1.0 - C_bulk[1]/(1.0-porosity);

  /* The second component is eliminated - apply unity sum */
  C_liquid[1] = 1.0 - C_liquid[0] - C_liquid[2]; // rf
  C_solid[1]  = 1.0 - C_solid[0]  - C_solid[2];

  /*
  printf("%+1.4e %+1.4e | %+1.4e %+1.4e | %+1.4e %+1.4e \n",C_liquid[0],C_solid[0],
         C_liquid[1],C_solid[1], C_liquid[2],C_solid[2]);
  */
  
  *phi = porosity;
  *valid = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

void F4(const PetscReal coor[],const PetscReal n[],const PetscReal vel[],PetscReal v[],void *ctx)
{
  evaluator_expy((PetscReal*)coor,&v[0]);
  v[0] = 100.0*v[0] + 0.1;
  
  v[1] = 0.4;
  if (coor[1] < 0.5) {
    v[1] = 0.8;
  }
  
  v[2] = 1.0 - v[0] - v[1];
}


#undef __FUNCT__
#define __FUNCT__ "demo4_column"
PetscErrorCode demo4_column(void)
{
  PetscErrorCode ierr;
  TPMC m;
  PetscBool active[PHASE_MAX];
  PetscReal ds;
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time,dt;
  CSLPoly csl;
  PetscInt i,k;
  
  ds = 1.0/32.0;
  
  ierr = TPMCCreate(PETSC_COMM_WORLD,&m);CHKERRQ(ierr);

  /*
  ierr = TPMCAddComponent(m,"H2O");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_FALSE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,0,active);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"rf");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_FALSE;
  ierr = TPMCComponentSetPhaseActive(m,1,active);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"ft");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,2,active);CHKERRQ(ierr);
  ierr = TPMCComponentEliminate(m,2);CHKERRQ(ierr);
  
  ierr = TPMCSetThermoDynamicMethod(m,UserThermoCalcModule_example2,NULL);CHKERRQ(ierr);
  */
  
  //
  // alternative configuration with different choices of
  // components eliminated wrt thermo-calc and components active
  ierr = TPMCAddComponent(m,"H2O");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,0,active);CHKERRQ(ierr);
  
  ierr = TPMCAddComponent(m,"rf");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,1,active);CHKERRQ(ierr);
  ierr = TPMCComponentEliminate(m,1);CHKERRQ(ierr);

  ierr = TPMCAddComponent(m,"ft");CHKERRQ(ierr);
  active[PHASE_IDX_SOLID] = PETSC_TRUE; active[PHASE_IDX_LIQUID] = PETSC_TRUE;
  ierr = TPMCComponentSetPhaseActive(m,2,active);CHKERRQ(ierr);

  ierr = TPMCSetThermoDynamicMethod(m,UserThermoCalcModule_example2a,NULL);CHKERRQ(ierr);
  //
  
  ierr = box_polygon(&npoly,&polycoor,&edgelabel);CHKERRQ(ierr);
  ierr = TPMCConfigureCSLPoly(m,npoly,polycoor,edgelabel,ds,2,2);CHKERRQ(ierr);
  ierr = TPMCSetup(m);CHKERRQ(ierr);
  
  ierr = TPMCView(m);CHKERRQ(ierr);
  
  ierr = TPMCSetViewVelocities(m,PETSC_FALSE);CHKERRQ(ierr);
  ierr = TPMCSetViewInteriorPoints(m,PETSC_TRUE,PETSC_FALSE);CHKERRQ(ierr);
  ierr = TPMCSetViewGammaPhi(m,PETSC_FALSE,PETSC_FALSE);CHKERRQ(ierr);
  
  /* initial conditions */
  {
    PetscInt c;
    
    csl = m->csl[PHASE_IDX_LIQUID];
    for (c=0; c<csl->ncells; c++) {
      PetscReal *coor,v;
      
      coor = csl->cell_list[c]->coor;
      
      // CH2O bulk
      evaluator_expy(coor,&v);
      m->cbar_c[m->ncomponents*c + 0] = 100.0*v + 0.1;

      // CRF bulk
      m->cbar_c[m->ncomponents*c + 1] = 0.4;
      if (coor[1] < 0.5) {
        m->cbar_c[m->ncomponents*c + 1] = 0.8;
      }
      // CFT bulk
      m->cbar_c[m->ncomponents*c + 2] = 1.0 - m->cbar_c[m->ncomponents*c + 0] - m->cbar_c[m->ncomponents*c + 1];
    }
  }
  
  /* P-T */
  for (i=0; i<m->nvert; i++) {
    m->temperature_v[i] = 1200.0;
    m->pressure_v[i] = 1.0;
  }
  
  ierr = TPMCEquilibrateInitialCondition(m);CHKERRQ(ierr);
  
  /* velocity */
  csl = m->csl[PHASE_IDX_LIQUID];
  for (i=0; i<csl->nvert; i++) {
    csl->vel_v[2*i+0] = 0.0;
    csl->vel_v[2*i+1] = 0.4;
  }
  
  csl = m->csl[PHASE_IDX_SOLID];
  for (i=0; i<csl->nvert; i++) {
    csl->vel_v[2*i+0] = 0.0;
    csl->vel_v[2*i+1] = -0.4;
  }

  ierr = TPMCViewAllVTS(m,"demo4_s0.vts");CHKERRQ(ierr);

  
  dt = 2.0 * M_PI / ((PetscReal)160.0);
  time = dt;
  for (k=1; k<=70; k++) {
    char prefix[300];
    
    
    ierr = TPMCUpdateVelocity(m);CHKERRQ(ierr);
    
    ierr = TPMCInitializeBC(m);CHKERRQ(ierr);
    ierr = TPMCGenericFacetBCIterator(m,2,F4,NULL);CHKERRQ(ierr);
    ierr = TPMCGenericFacetBCIterator(m,0,F4,NULL);CHKERRQ(ierr);
    ierr = TPMCVerifyBC(m);CHKERRQ(ierr);
    
    
    ierr = TPMCSolve(m,k,dt,time);CHKERRQ(ierr);
    
    sprintf(prefix,"demo4_s%d.vts",k);
    ierr = TPMCViewAllVTS(m,prefix);CHKERRQ(ierr);
    
    //csl = m->csl[PHASE_IDX_SOLID];
    //sprintf(prefix,"S%.6d.vts",k);
    //CSLPolyCellViewVTS(csl,prefix);
    //CSLPolyFacetViewVTS(csl,"fS.vtu");
    
    
    //csl = m->csl[PHASE_IDX_LIQUID];
    //sprintf(prefix,"L%.6d.vts",k);
    //CSLPolyCellViewVTS(csl,prefix);
    //CSLPolyFacetViewVTS(csl,"fL.vtu");
    
    time += dt;
  }
  
  
  ierr = TPMCDestroy(&m);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = demo1();CHKERRQ(ierr);
  //ierr = demo3_column();CHKERRQ(ierr);
  ierr = demo4_column();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}
