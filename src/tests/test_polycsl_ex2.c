
#include <petsc.h>
#include <petscdm.h>
#include <dmtetdt.h>
#include <cslpoly.h>
#include <proj_init_finalize.h>

/*
 Defines the example shown in Figure 6 described here:
 
 "An Unconditionally Stable Fully Conservative Semi-Lagrangian Method"
 Michael Lentine, Jon Tomas Gretarsson, Ronald Fedkiw

 Journal of Computational Physics
 Volume 230 Issue 8, April, 2011
 Pages 2857-2879
 
*/

PetscReal DOMAIN_WIDTH = 0.25;

/* domains */
PetscErrorCode box_polygon(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 4;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);

  coor[2*0+0] = 0.0;   coor[2*0+1] = 0.0;    edge[0] = 0;
  coor[2*1+0] = 5.0;   coor[2*1+1] = 0.0;    edge[1] = 1;
  coor[2*2+0] = 5.0;   coor[2*2+1] = DOMAIN_WIDTH;   edge[2] = 2;
  coor[2*3+0] = 0.0;   coor[2*3+1] = DOMAIN_WIDTH;   edge[3] = 3;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}


/* ic/bc functions */
void dirichlet_evaluator_hat(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal G_xy;
  
  G_xy = 0.0;
  if (xn[0] >= 0.25) {
    if (xn[0] <= 0.75) {
      G_xy = 1.0;
    }
  }
  value[0] = G_xy;
}

/* velocity functions */
PetscErrorCode vel_evaluator_sin(PetscReal pos[],PetscReal value[])
{
  value[0] = PetscSinReal(PETSC_PI * pos[0] / 5.0);
  value[1] = 0.0;
  PetscFunctionReturn(0);
}


void bc_dirichlet(CSLPoly p,PetscReal time,void *ctx)
{
  PetscInt pe,edge;
  void (*my_eval)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  
  my_eval = (void (*)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*))ctx;
  
  for (pe=0; pe<p->poly_npoints; pe++) {
    edge = p->poly_edge_label[pe];
    CSLPolyGenericFacetBCIterator(p,edge,my_eval,(void*)&time);
  }
}

void eval_analytic_divu(CSLPoly sl,PetscInt cellid,PetscReal x[],PetscReal *divu)
{
  *divu = (PETSC_PI / 5.0) * PetscCosReal(PETSC_PI * x[0] / 5.0);
}

void CSLSetAnalyticDivergenceMethod(CSLPoly sl)
{
  
  sl->compute_div_u = eval_analytic_divu;
}


/* compute min,max,integral over active domain */
#undef __FUNCT__
#define __FUNCT__ "ComputeMetrics"
PetscErrorCode ComputeMetrics(CSLPoly dsl,PetscReal metric[])
{
  PetscReal cell_dv = dsl->dx[0] * dsl->dx[1];
  PetscInt c;
  PetscReal min_c,max_c,integral_c;
  
  integral_c = 0.0;
  min_c = 1.0e32;
  max_c = -1.0e32;
  for (c=0; c<dsl->ncells; c++) {
    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      integral_c += dsl->value_c[c] * cell_dv;
      
      if (dsl->value_c[c] < min_c) { min_c = dsl->value_c[c]; }
      if (dsl->value_c[c] > max_c) { max_c = dsl->value_c[c]; }
    }
  }
  integral_c /= DOMAIN_WIDTH;
  metric[0] = min_c;
  metric[1] = max_c;
  metric[2] = integral_c;

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ComputeIntegralMetrics"
PetscErrorCode ComputeIntegralMetrics(CSLPoly sl,PetscReal metric[])
{
  PetscReal cell_dv = sl->dx[0] * sl->dx[1];
  PetscInt c;
  PetscReal integral_c;
  PetscInt qi,qj,k;
  PetscReal dJ_q;
  PetscReal *xi1,*w1;
  PetscInt nqp1 = 8;
  
  _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
  
  dJ_q = 0.25 * cell_dv;
  
  integral_c = 0.0;
  for (c=0; c<sl->ncells; c++) {
    if (sl->cell_list[c]->type == COOR_INTERIOR) {

      for (qj=0; qj<nqp1; qj++) {
      for (qi=0; qi<nqp1; qi++) {
        PetscReal coor_q[2],phi_q;
        PetscReal x0[2],Ni[4],coor_v[2*4];
        
        // get quadrature coordinates
        x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
        x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
        
        Ni[0] = 0.25 * (1.0 - xi1[qi]) * (1.0 - xi1[qj]);
        Ni[1] = 0.25 * (1.0 + xi1[qi]) * (1.0 - xi1[qj]);
        Ni[2] = 0.25 * (1.0 - xi1[qi]) * (1.0 + xi1[qj]);
        Ni[3] = 0.25 * (1.0 + xi1[qi]) * (1.0 + xi1[qj]);
        
        coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
        coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
        coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
        coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
        
        coor_q[0] = coor_q[1] = 0.0;
        for (k=0; k<4; k++) {
          coor_q[0] += Ni[k] * coor_v[2*k+0];
          coor_q[1] += Ni[k] * coor_v[2*k+1];
        }

        CSLPolyInterpolateAtPoint(sl,coor_q,&phi_q);
        integral_c += w1[qi]*w1[qj] * (phi_q) * dJ_q;
      }}
    }
  }
  integral_c /= DOMAIN_WIDTH;
  metric[0] = integral_c;
    
  PetscFree(xi1);
  PetscFree(w1);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "example_Lentine"
PetscErrorCode example_Lentine(void)
{
  CSLPoly dsl;
  PetscInt i,k,c;
  PetscErrorCode (*create_polygon)(PetscInt*,PetscReal**,PetscInt**);
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time,dt;
  PetscReal ds;
  PetscInt nt = 260;
  PetscInt nout = 10;
  PetscReal metric[3],intphi;
  PetscInt nsubcell = 2;
  PetscInt mx = 128;
  PetscErrorCode ierr;

  PetscOptionsGetInt(NULL,NULL,"-mx",&mx,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nsubcell",&nsubcell,NULL);
  
  create_polygon      = box_polygon;
  dirichlet_evaluator = dirichlet_evaluator_hat;
  vel_evaluator       = vel_evaluator_sin;

  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(dsl,I_WENO3);
  CSLPolySetSubcellResolution(dsl,nsubcell,1);
 
  //CSLPolySetDefaultDivergenceMethod(dsl);
  CSLSetAnalyticDivergenceMethod(dsl);
  
  ds = 5.0 / ((PetscReal)mx);

  ierr = create_polygon(&npoly,&polycoor,&edgelabel);
  /* 
   Force five cells in the y-direction.
  */
  DOMAIN_WIDTH = 5.0 * ds;
  polycoor[2*2+1] = DOMAIN_WIDTH;
  polycoor[2*3+1] = DOMAIN_WIDTH;
  CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
  
  CSLPolySetDomainDefault(dsl,ds);

  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  time = 0.0;
  /* set initial velocity field */
  for (i=0; i<dsl->nvert; i++) {
    PetscReal *pos = &dsl->coor_v[2*i];
    ierr = vel_evaluator(pos,&dsl->vel_v[2*i]);CHKERRQ(ierr);
  }
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;

    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;
  }

  /* setup - time dependent */
  CSLPolyComputeCellAverageDivVelocity(dsl);
  /* or use exact div */
  /*
  {
    PetscInt c;
    PetscReal divu,coor[2];
    
    for (c=0; c<dsl->ncells; c++) {
      coor[0] = dsl->cell_list[c]->coor[0];
      coor[1] = dsl->cell_list[c]->coor[1];
    
      divu = (PETSC_PI / 5.0) * PetscCosReal(PETSC_PI * coor[0] / 5.0);
      dsl->divu_c[c] = divu;
    }
  }
  */
  
  CSLPolyMarkInflowFacets(dsl);
  {
    PetscInt s;
    for (s=0; s<dsl->boundary_nfacets; s++) {
      dsl->boundary_facet_list[s]->is_inflow = 0;
    }
  }
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");

  ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics(dsl,&intphi);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"[step %4D] time = %1.2e : min(c) %+1.4e : max(c) %+1.4e : approx int(c) %+1.12e : L2[int(c)] %+1.12e\n",0,0.0,metric[0],metric[1],metric[2],PetscAbsReal(intphi-0.5));

  dt = 5.0/256.0;
  PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);
  
  time = dt;
  for (k=1; k<=nt; k++) {
    char filename[128];
    
    // change velocity
    //CSLPolyMarkInflowFacets(dsl);
    
    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    //CSLPolyUpdate(dsl,time,dt);
    //CSLPolyUpdate_Conservative(dsl,time,dt);
    CSLPolyUpdate_Strang2Multiplicative(dsl,time,dt);

    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);

    
    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    if (k%nout == 0) {
      ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
      ierr = ComputeIntegralMetrics(dsl,&intphi);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"[step %4D] time = %1.6e : min(c) %+1.4e : max(c) %+1.4e : approx int(c) %+1.12e : L2[int(c)] %+1.12e\n",k,time,metric[0],metric[1],metric[2],PetscAbsReal(intphi-0.5));
    }

    if ((time+dt) > 5.0) {
      ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
      ierr = ComputeIntegralMetrics(dsl,&intphi);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"[step %4D] time = %1.6e : min(c) %+1.4e : max(c) %+1.4e : approx int(c) %+1.12e : L2[int(c)] %+1.12e\n",k,time,metric[0],metric[1],metric[2],PetscAbsReal(intphi-0.5));
      break;
    }
    time += dt;
  }
  
  PetscFunctionReturn(0);
}


PetscErrorCode box_polygon_expi(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 4;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);
  
  coor[2*0+0] = -PETSC_PI;   coor[2*0+1] = 0.0;            edge[0] = 0;
  coor[2*1+0] =  PETSC_PI;   coor[2*1+1] = 0.0;            edge[1] = 1;
  coor[2*2+0] =  PETSC_PI;   coor[2*2+1] = DOMAIN_WIDTH;   edge[2] = 2;
  coor[2*3+0] = -PETSC_PI;   coor[2*3+1] = DOMAIN_WIDTH;   edge[3] = 3;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_sin_expi(PetscReal pos[],PetscReal value[])
{
  value[0] = PetscSinReal(pos[0]);
  value[1] = 0.0;
  PetscFunctionReturn(0);
}

#define PetscCotReal(a)  1.0/PetscTanReal((a))
#define PetscAcotReal(a) PetscAtanReal(1.0/(a))

void dirichlet_evaluator_exppi(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal time,r,z,x,phi0;
  
  time = *((PetscReal*)ctx);
  x = xn[0];
  
  r = 2.0 * PetscAcotReal( PetscExpReal(time)*PetscCotReal(x/2.0) );
  
  phi0 = PetscExpReal(-r*r)*(PetscSinReal(r)*PetscSinReal(r));
  
  if (PetscAbsReal(x) == 0.0) {
    z = 0.0;
  } else {
    z = PetscSinReal(r) * phi0 / PetscSinReal(x);
  }
  //printf("x %+1.12e : r %+1.12e : phi0 %+1.12e : z %+1.12e\n",x,r,phi0,z);
  value[0] = z;
}

#undef __FUNCT__
#define __FUNCT__ "ComputeIntegralMetrics_exppi"
PetscErrorCode ComputeIntegralMetrics_exppi(CSLPoly sl,PetscReal time,PetscReal metric[])
{
  PetscReal cell_dv = sl->dx[0] * sl->dx[1];
  PetscInt c;
  PetscReal integral_c[4];
  PetscInt qi,qj,k;
  PetscReal dJ_q;
  PetscReal *xi1,*w1;
  PetscInt nqp1 = 8;
  
  _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
  
  dJ_q = 0.25 * cell_dv;
  
  integral_c[0] = 0.0;
  integral_c[1] = 0.0;
  integral_c[2] = 0.0;
  integral_c[3] = 0.0;
  for (c=0; c<sl->ncells; c++) {
    PetscReal int_cell_phi = 0.0;
    
    if (sl->cell_list[c]->type == COOR_INTERIOR) {
      
      int_cell_phi = 0.0;
      for (qj=0; qj<nqp1; qj++) {
      for (qi=0; qi<nqp1; qi++) {
        PetscReal coor_q[2],phi_q,phi_a;
        PetscReal x0[2],Ni[4],coor_v[2*4];
        
        // get quadrature coordinates
        x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
        x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
        
        Ni[0] = 0.25 * (1.0 - xi1[qi]) * (1.0 - xi1[qj]);
        Ni[1] = 0.25 * (1.0 + xi1[qi]) * (1.0 - xi1[qj]);
        Ni[2] = 0.25 * (1.0 - xi1[qi]) * (1.0 + xi1[qj]);
        Ni[3] = 0.25 * (1.0 + xi1[qi]) * (1.0 + xi1[qj]);
        
        coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
        coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
        coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
        coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
        
        coor_q[0] = coor_q[1] = 0.0;
        for (k=0; k<4; k++) {
          coor_q[0] += Ni[k] * coor_v[2*k+0];
          coor_q[1] += Ni[k] * coor_v[2*k+1];
        }
        
        CSLPolyInterpolateAtPoint(sl,coor_q,&phi_q);
        dirichlet_evaluator_exppi((const PetscReal*)coor_q,NULL,NULL,&phi_a,(void*)&time);
        
        integral_c[0] += w1[qi]*w1[qj] * (phi_q) * dJ_q;
        integral_c[1] += w1[qi]*w1[qj] * (phi_a) * dJ_q;
        integral_c[2] += w1[qi]*w1[qj] * (phi_q - phi_a)*(phi_q - phi_a) * dJ_q;
        int_cell_phi  += w1[qi]*w1[qj] * (phi_a) * dJ_q;
      }}
      int_cell_phi = int_cell_phi / cell_dv;
      integral_c[3] += (int_cell_phi - sl->value_c[c])*(int_cell_phi - sl->value_c[c]) * cell_dv;
      
    }
  }
  integral_c[0] /= DOMAIN_WIDTH;
  integral_c[1] /= DOMAIN_WIDTH;
  integral_c[2] /= DOMAIN_WIDTH;
  integral_c[3] /= DOMAIN_WIDTH;
  metric[0] = integral_c[0];
  metric[1] = integral_c[1];
  metric[2] = PetscSqrtReal(integral_c[2]);
  metric[3] = PetscSqrtReal(integral_c[3]);

  PetscFree(xi1);
  PetscFree(w1);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetCellConstantIC_exppi"
PetscErrorCode SetCellConstantIC_exppi(CSLPoly sl,PetscReal time)
{
  PetscReal cell_dv = sl->dx[0] * sl->dx[1];
  PetscInt c;
  PetscReal integral_c;
  PetscInt qi,qj,k;
  PetscReal dJ_q;
  PetscReal *xi1,*w1;
  PetscInt nqp1 = 8;
  
  _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
  
  dJ_q = 0.25 * cell_dv;
  
  for (c=0; c<sl->ncells; c++) {
    if (sl->cell_list[c]->type == COOR_INTERIOR) {
      
      integral_c = 0.0;
      for (qj=0; qj<nqp1; qj++) {
      for (qi=0; qi<nqp1; qi++) {
        PetscReal coor_q[2],phi_q,phi_a;
        PetscReal x0[2],Ni[4],coor_v[2*4];
        
        // get quadrature coordinates
        x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
        x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
        
        Ni[0] = 0.25 * (1.0 - xi1[qi]) * (1.0 - xi1[qj]);
        Ni[1] = 0.25 * (1.0 + xi1[qi]) * (1.0 - xi1[qj]);
        Ni[2] = 0.25 * (1.0 - xi1[qi]) * (1.0 + xi1[qj]);
        Ni[3] = 0.25 * (1.0 + xi1[qi]) * (1.0 + xi1[qj]);
        
        coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
        coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
        coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
        coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
        
        coor_q[0] = coor_q[1] = 0.0;
        for (k=0; k<4; k++) {
          coor_q[0] += Ni[k] * coor_v[2*k+0];
          coor_q[1] += Ni[k] * coor_v[2*k+1];
        }
        
        CSLPolyInterpolateAtPoint(sl,coor_q,&phi_q);
        dirichlet_evaluator_exppi((const PetscReal*)coor_q,NULL,NULL,&phi_a,(void*)&time);
        
        integral_c += w1[qi]*w1[qj] * (phi_a) * dJ_q;
      }}
      sl->value_c[c] = integral_c/cell_dv;
    } else {
      sl->value_c[c] = 0.0;
    }
  }
  PetscFree(xi1);
  PetscFree(w1);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_ReesJones"
PetscErrorCode example_ReesJones(void)
{
  CSLPoly dsl;
  PetscInt i,k,c;
  PetscErrorCode (*create_polygon)(PetscInt*,PetscReal**,PetscInt**);
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time = 0.0;
  PetscReal dt = 4.0/100.0,user_dt,user_cfldt;
  PetscReal ds = 0.1;
  PetscInt nt = 50000;
  PetscInt nout = 10;
  PetscReal metric0[3],metric[3],intphi0[4],intphi[4];
  PetscInt nsubcell = 2;
  PetscInt mx = 128;
  PetscReal tmax = 4.0;
  PetscReal cfl = 1.0;
  PetscBool use_cfl = PETSC_FALSE,use_user_dt = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscOptionsGetInt(NULL,NULL,"-mx",&mx,NULL);
  ds = (2.0*PETSC_PI) / ((PetscReal)mx);

  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nsubcell",&nsubcell,NULL);
  PetscOptionsGetReal(NULL,NULL,"-tmax",&tmax,NULL);

  PetscOptionsGetReal(NULL,NULL,"-cfl",&cfl,&use_cfl);
  PetscOptionsGetReal(NULL,NULL,"-dt",&user_dt,&use_user_dt);
  
  if (!use_user_dt && !use_cfl) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a time-step via either -dt <value> or -cfl <value>\n");
  if (use_user_dt && use_cfl) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a time-step method via either -dt <value> or -cfl <value>, not BOTH\n");
  
  if (use_cfl) {
    user_cfldt = cfl * (ds / 1.0);
    dt = user_cfldt;
  }
  if (use_user_dt) {
    dt = user_dt;
  }
  
  create_polygon      = box_polygon_expi;
  dirichlet_evaluator = dirichlet_evaluator_exppi;
  vel_evaluator       = vel_evaluator_sin_expi;
  
  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(dsl,I_WENO3);
  CSLPolySetSubcellResolution(dsl,nsubcell,1);
  
  CSLPolySetDefaultDivergenceMethod(dsl);
  //CSLSetAnalyticDivergenceMethod(dsl);
  
  ierr = create_polygon(&npoly,&polycoor,&edgelabel);
  /*
   Force five cells in the y-direction.
   */
  DOMAIN_WIDTH = 5.0 * ds;
  polycoor[2*2+1] = DOMAIN_WIDTH;
  polycoor[2*3+1] = DOMAIN_WIDTH;
  CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
  
  CSLPolySetDomainDefault(dsl,ds);
  
  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  time = 0.0;
  /* set initial velocity field */
  for (i=0; i<dsl->nvert; i++) {
    PetscReal *pos = &dsl->coor_v[2*i];
    ierr = vel_evaluator(pos,&dsl->vel_v[2*i]);CHKERRQ(ierr);
  }
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;
    
    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;
  }
  //SetCellConstantIC_exppi(dsl,time);
  
  /* setup - time dependent */
  CSLPolyComputeCellAverageDivVelocity(dsl);
  
  CSLPolyMarkInflowFacets(dsl);
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");
  
  PetscPrintf(PETSC_COMM_WORLD,"# Executing until time %1.4e (max steps %D)\n",tmax,nt);
  PetscPrintf(PETSC_COMM_WORLD,"# Using time-step corresponding to a CFL of %1.2e\n",dt/(ds/1.0));
  
  ierr = ComputeMetrics(dsl,metric0);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics_exppi(dsl,time,intphi0);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"# Initial condition:\n");
  PetscPrintf(PETSC_COMM_WORLD,"#   bounds           [%+1.2e , %+1.2e]\n",metric0[0],metric0[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   sum_K(phi_K.|K|)  %+1.12e\n",metric0[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int R[phi] dV    %+1.12e\n",intphi0[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int phi_anl dV   %+1.12e\n",intphi0[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(R[phi])        %+1.12e\n",intphi0[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(phi_K)         %+1.12e\n",intphi0[3]);

  PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : sum_K(phi_K.|K|) %+1.12e\n",0,0.0,metric0[0],metric0[1],metric0[2]);
  
  time = dt;
  for (k=1; k<=nt; k++) {
    char filename[128];

    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    //CSLPolyUpdate_Conservative(dsl,time,dt);
    CSLPolyUpdate_Strang2Multiplicative(dsl,time,dt);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    
    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    if (k%10 == 0) {
      ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
      //ierr = ComputeIntegralMetrics_exppi(dsl,time,intphi);CHKERRQ(ierr);
      //PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : approx int(c) %+1.4e : int(c) %+1.12e : int(c_exact) %+1.12e : L2(c) %+1.12e\n",k,time,metric[0],metric[1],metric[2],intphi[0],intphi[1],intphi[2]);
      PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : sum_K(phi_K.|K|) %+1.12e\n",k,time,metric[0],metric[1],metric[2]);
    }
    
    if (time >= tmax) { break; }
    
    time += dt;
  }
  
  ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics_exppi(dsl,time,intphi);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Final state:\n");
  PetscPrintf(PETSC_COMM_WORLD,"#   bounds           [%+1.2e , %+1.2e]\n",metric[0],metric[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   sum_K(phi_K.|K|)  %+1.12e\n",metric[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#     ratio           %+1.12e\n",metric0[2]/metric[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int R[phi] dV    %+1.12e\n",intphi[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#     ratio           %+1.12e\n",intphi0[0]/intphi[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int phi_anl dV   %+1.12e\n",intphi[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(R[phi])        %+1.12e\n",intphi[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(phi_K)         %+1.12e\n",intphi[3]);
  
  PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : approx int(c) %+1.4e : int(c) %+1.12e : int(c_exact) %+1.12e : L2(c) %+1.12e\n",k,time,metric[0],metric[1],metric[2],intphi[0],intphi[1],intphi[2]);
  
  PetscFunctionReturn(0);
}


PetscErrorCode box_polygon_divconst(PetscInt *_np,PetscReal **_coor,PetscInt **_edge)
{
  PetscInt np;
  PetscReal *coor;
  PetscInt *edge;
  
  np = 4;
  PetscMalloc1(np*2,&coor);
  PetscMalloc1(np,&edge);
  
  coor[2*0+0] = -10.0;   coor[2*0+1] = 0.0;            edge[0] = 0;
  coor[2*1+0] =  10.0;   coor[2*1+1] = 0.0;            edge[1] = 1;
  coor[2*2+0] =  10.0;   coor[2*2+1] = DOMAIN_WIDTH;   edge[2] = 2;
  coor[2*3+0] = -10.0;   coor[2*3+1] = DOMAIN_WIDTH;   edge[3] = 3;
  *_np = np;
  *_coor = coor;
  *_edge = edge;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_divconst(PetscReal pos[],PetscReal value[])
{
  value[0] = pos[0];
  value[1] = 0.0;
  PetscFunctionReturn(0);
}

#define PetscCotReal(a)  1.0/PetscTanReal((a))
#define PetscAcotReal(a) PetscAtanReal(1.0/(a))

void dirichlet_evaluator_divconst(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal time,r,z,x,phi0;
  
  time = *((PetscReal*)ctx);
  x = xn[0];
  
  r = x * PetscExpReal(-time);
  
  phi0 = PetscExpReal(-100.0*r*r);
  
  z = PetscExpReal(-time) * phi0 ;
  value[0] = z;
}

#undef __FUNCT__
#define __FUNCT__ "ComputeIntegralMetrics_divconst"
PetscErrorCode ComputeIntegralMetrics_divconst(CSLPoly sl,PetscReal time,PetscReal metric[])
{
  PetscReal cell_dv = sl->dx[0] * sl->dx[1];
  PetscInt c;
  PetscReal integral_c[4];
  PetscInt qi,qj,k;
  PetscReal dJ_q;
  PetscReal *xi1,*w1;
  PetscInt nqp1 = 8;
  
  _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
  
  dJ_q = 0.25 * cell_dv;
  
  integral_c[0] = 0.0;
  integral_c[1] = 0.0;
  integral_c[2] = 0.0;
  integral_c[3] = 0.0;
  for (c=0; c<sl->ncells; c++) {
    PetscReal int_cell_phi = 0.0;
    
    if (sl->cell_list[c]->type == COOR_INTERIOR) {
      
      int_cell_phi = 0.0;
      for (qj=0; qj<nqp1; qj++) {
        for (qi=0; qi<nqp1; qi++) {
          PetscReal coor_q[2],phi_q,phi_a;
          PetscReal x0[2],Ni[4],coor_v[2*4];
          
          // get quadrature coordinates
          x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
          x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
          
          Ni[0] = 0.25 * (1.0 - xi1[qi]) * (1.0 - xi1[qj]);
          Ni[1] = 0.25 * (1.0 + xi1[qi]) * (1.0 - xi1[qj]);
          Ni[2] = 0.25 * (1.0 - xi1[qi]) * (1.0 + xi1[qj]);
          Ni[3] = 0.25 * (1.0 + xi1[qi]) * (1.0 + xi1[qj]);
          
          coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
          coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
          coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
          coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
          
          coor_q[0] = coor_q[1] = 0.0;
          for (k=0; k<4; k++) {
            coor_q[0] += Ni[k] * coor_v[2*k+0];
            coor_q[1] += Ni[k] * coor_v[2*k+1];
          }
          
          CSLPolyInterpolateAtPoint(sl,coor_q,&phi_q);
          dirichlet_evaluator_divconst((const PetscReal*)coor_q,NULL,NULL,&phi_a,(void*)&time);
          
          integral_c[0] += w1[qi]*w1[qj] * (phi_q) * dJ_q;
          integral_c[1] += w1[qi]*w1[qj] * (phi_a) * dJ_q;
          integral_c[2] += w1[qi]*w1[qj] * (phi_q - phi_a)*(phi_q - phi_a) * dJ_q;
          int_cell_phi  += w1[qi]*w1[qj] * (phi_a) * dJ_q;
        }}
      int_cell_phi = int_cell_phi / cell_dv;
      integral_c[3] += (int_cell_phi - sl->value_c[c])*(int_cell_phi - sl->value_c[c]) * cell_dv;
      
    }
  }
  integral_c[0] /= DOMAIN_WIDTH;
  integral_c[1] /= DOMAIN_WIDTH;
  integral_c[2] /= DOMAIN_WIDTH;
  integral_c[3] /= DOMAIN_WIDTH;
  metric[0] = integral_c[0];
  metric[1] = integral_c[1];
  metric[2] = PetscSqrtReal(integral_c[2]);
  metric[3] = PetscSqrtReal(integral_c[3]);
  
  PetscFree(xi1);
  PetscFree(w1);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetCellConstantIC_divconst"
PetscErrorCode SetCellConstantIC_divconst(CSLPoly sl,PetscReal time)
{
  PetscReal cell_dv = sl->dx[0] * sl->dx[1];
  PetscInt c;
  PetscReal integral_c;
  PetscInt qi,qj,k;
  PetscReal dJ_q;
  PetscReal *xi1,*w1;
  PetscInt nqp1 = 8;
  
  _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
  
  dJ_q = 0.25 * cell_dv;
  
  for (c=0; c<sl->ncells; c++) {
    if (sl->cell_list[c]->type == COOR_INTERIOR) {
      
      integral_c = 0.0;
      for (qj=0; qj<nqp1; qj++) {
        for (qi=0; qi<nqp1; qi++) {
          PetscReal coor_q[2],phi_q,phi_a;
          PetscReal x0[2],Ni[4],coor_v[2*4];
          
          // get quadrature coordinates
          x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
          x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
          
          Ni[0] = 0.25 * (1.0 - xi1[qi]) * (1.0 - xi1[qj]);
          Ni[1] = 0.25 * (1.0 + xi1[qi]) * (1.0 - xi1[qj]);
          Ni[2] = 0.25 * (1.0 - xi1[qi]) * (1.0 + xi1[qj]);
          Ni[3] = 0.25 * (1.0 + xi1[qi]) * (1.0 + xi1[qj]);
          
          coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
          coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
          coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
          coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
          
          coor_q[0] = coor_q[1] = 0.0;
          for (k=0; k<4; k++) {
            coor_q[0] += Ni[k] * coor_v[2*k+0];
            coor_q[1] += Ni[k] * coor_v[2*k+1];
          }
          
          CSLPolyInterpolateAtPoint(sl,coor_q,&phi_q);
          dirichlet_evaluator_divconst((const PetscReal*)coor_q,NULL,NULL,&phi_a,(void*)&time);
          
          integral_c += w1[qi]*w1[qj] * (phi_a) * dJ_q;
        }}
      sl->value_c[c] = integral_c/cell_dv;
    } else {
      sl->value_c[c] = 0.0;
    }
  }
  PetscFree(xi1);
  PetscFree(w1);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_ReesJones2"
PetscErrorCode example_ReesJones2(void)
{
  CSLPoly dsl;
  PetscInt i,k,c;
  PetscErrorCode (*create_polygon)(PetscInt*,PetscReal**,PetscInt**);
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time = 0.0;
  PetscReal dt = 4.0/100.0,user_dt,user_cfldt;
  PetscReal ds = 0.1;
  PetscInt nt = 50000;
  PetscInt nout = 10;
  PetscReal metric0[3],metric[3],intphi0[4],intphi[4];
  PetscInt nsubcell = 2;
  PetscInt mx = 256;
  PetscReal tmax = 1.0;
  PetscReal cfl = 1.0;
  PetscBool use_cfl = PETSC_FALSE,use_user_dt = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscOptionsGetInt(NULL,NULL,"-mx",&mx,NULL);
  ds = (2.0*PETSC_PI) / ((PetscReal)mx);
  
  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nsubcell",&nsubcell,NULL);
  PetscOptionsGetReal(NULL,NULL,"-tmax",&tmax,NULL);
  
  PetscOptionsGetReal(NULL,NULL,"-cfl",&cfl,&use_cfl);
  PetscOptionsGetReal(NULL,NULL,"-dt",&user_dt,&use_user_dt);
  
  if (!use_user_dt && !use_cfl) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a time-step via either -dt <value> or -cfl <value>\n");
  if (use_user_dt && use_cfl) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a time-step method via either -dt <value> or -cfl <value>, not BOTH\n");
  
  if (use_cfl) {
    user_cfldt = cfl * (ds / 1.0);
    dt = user_cfldt;
  }
  if (use_user_dt) {
    dt = user_dt;
  }
  
  create_polygon      = box_polygon_divconst;
  dirichlet_evaluator = dirichlet_evaluator_divconst;
  vel_evaluator       = vel_evaluator_divconst;
  
  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(dsl,I_WENO3);
  CSLPolySetSubcellResolution(dsl,nsubcell,1);
  
  CSLPolySetDefaultDivergenceMethod(dsl);
  //CSLSetAnalyticDivergenceMethod(dsl);
  
  ierr = create_polygon(&npoly,&polycoor,&edgelabel);
  /*
   Force five cells in the y-direction.
   */
  DOMAIN_WIDTH = 5.0 * ds;
  polycoor[2*2+1] = DOMAIN_WIDTH;
  polycoor[2*3+1] = DOMAIN_WIDTH;
  CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
  
  CSLPolySetDomainDefault(dsl,ds);
  
  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  time = 0.0;
  /* set initial velocity field */
  for (i=0; i<dsl->nvert; i++) {
    PetscReal *pos = &dsl->coor_v[2*i];
    ierr = vel_evaluator(pos,&dsl->vel_v[2*i]);CHKERRQ(ierr);
  }
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;
    
    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;
  }
  //SetCellConstantIC_divconst(dsl,time);
  
  /* setup - time dependent */
  CSLPolyComputeCellAverageDivVelocity(dsl);
  
  CSLPolyMarkInflowFacets(dsl);
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");
  
  PetscPrintf(PETSC_COMM_WORLD,"# Executing until time %1.4e (max steps %D)\n",tmax,nt);
  PetscPrintf(PETSC_COMM_WORLD,"# Using time-step corresponding to a CFL of %1.2e\n",dt/(ds/1.0));
  
  ierr = ComputeMetrics(dsl,metric0);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics_divconst(dsl,time,intphi0);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"# Initial condition:\n");
  PetscPrintf(PETSC_COMM_WORLD,"#   bounds           [%+1.2e , %+1.2e]\n",metric0[0],metric0[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   sum_K(phi_K.|K|)  %+1.12e\n",metric0[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int R[phi] dV    %+1.12e\n",intphi0[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int phi_anl dV   %+1.12e\n",intphi0[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(R[phi])        %+1.12e\n",intphi0[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(phi_K)         %+1.12e\n",intphi0[3]);
  
  PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : sum_K(phi_K.|K|) %+1.12e\n",0,0.0,metric0[0],metric0[1],metric0[2]);
  
  time = dt;
  for (k=1; k<=nt; k++) {
    char filename[128];
    
    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    //CSLPolyUpdate_Conservative(dsl,time,dt);
    CSLPolyUpdate_Strang2Multiplicative(dsl,time,dt);

    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    
    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    if (k%10 == 0) {
      ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
      //ierr = ComputeIntegralMetrics_divconst(dsl,time,intphi);CHKERRQ(ierr);
      //PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : approx int(c) %+1.4e : int(c) %+1.12e : int(c_exact) %+1.12e : L2(c) %+1.12e\n",k,time,metric[0],metric[1],metric[2],intphi[0],intphi[1],intphi[2]);
      PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : sum_K(phi_K.|K|) %+1.12e\n",k,time,metric[0],metric[1],metric[2]);
    }
    
    if (time >= tmax) { break; }
    
    time += dt;
  }
  
  ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics_divconst(dsl,time,intphi);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Final state:\n");
  PetscPrintf(PETSC_COMM_WORLD,"#   bounds           [%+1.2e , %+1.2e]\n",metric[0],metric[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   sum_K(phi_K.|K|)  %+1.12e\n",metric[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#     ratio           %+1.12e\n",metric0[2]/metric[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int R[phi] dV    %+1.12e\n",intphi[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#     ratio           %+1.12e\n",intphi0[0]/intphi[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int phi_anl dV   %+1.12e\n",intphi[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(R[phi])        %+1.12e\n",intphi[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(phi_K)         %+1.12e\n",intphi[3]);
  
  PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : approx int(c) %+1.4e : int(c) %+1.12e : int(c_exact) %+1.12e : L2(c) %+1.12e\n",k,time,metric[0],metric[1],metric[2],intphi[0],intphi[1],intphi[2]);
  
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_const(PetscReal pos[],PetscReal value[])
{
  value[0] = 1.0;
  value[1] = 0.0;
  PetscFunctionReturn(0);
}

void dirichlet_evaluator_gauss(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  PetscReal time,x;
  
  time = *((PetscReal*)ctx);
  x = xn[0];
  
  value[0] = exp(-10.0*(x-time)*(x-time));
}

#undef __FUNCT__
#define __FUNCT__ "ComputeIntegralMetrics_gauss"
PetscErrorCode ComputeIntegralMetrics_gauss(CSLPoly sl,PetscReal time,PetscReal metric[])
{
  PetscReal cell_dv = sl->dx[0] * sl->dx[1];
  PetscInt c;
  PetscReal integral_c[4];
  PetscInt qi,qj,k;
  PetscReal dJ_q;
  PetscReal *xi1,*w1;
  PetscInt nqp1 = 8;
  
  _PetscDTGaussJacobiQuadrature(1,nqp1,-1.0,1.0,&xi1,&w1);
  
  dJ_q = 0.25 * cell_dv;
  
  integral_c[0] = 0.0;
  integral_c[1] = 0.0;
  integral_c[2] = 0.0;
  integral_c[3] = 0.0;
  for (c=0; c<sl->ncells; c++) {
    PetscReal int_cell_phi = 0.0;
    
    if (sl->cell_list[c]->type == COOR_INTERIOR) {
      
      int_cell_phi = 0.0;
      for (qj=0; qj<nqp1; qj++) {
        for (qi=0; qi<nqp1; qi++) {
          PetscReal coor_q[2],phi_q,phi_a;
          PetscReal x0[2],Ni[4],coor_v[2*4];
          
          // get quadrature coordinates
          x0[0] = sl->cell_list[c]->coor[0] - 0.5 * sl->dx[0];
          x0[1] = sl->cell_list[c]->coor[1] - 0.5 * sl->dx[1];
          
          Ni[0] = 0.25 * (1.0 - xi1[qi]) * (1.0 - xi1[qj]);
          Ni[1] = 0.25 * (1.0 + xi1[qi]) * (1.0 - xi1[qj]);
          Ni[2] = 0.25 * (1.0 - xi1[qi]) * (1.0 + xi1[qj]);
          Ni[3] = 0.25 * (1.0 + xi1[qi]) * (1.0 + xi1[qj]);
          
          coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
          coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
          coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
          coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
          
          coor_q[0] = coor_q[1] = 0.0;
          for (k=0; k<4; k++) {
            coor_q[0] += Ni[k] * coor_v[2*k+0];
            coor_q[1] += Ni[k] * coor_v[2*k+1];
          }
          
          CSLPolyInterpolateAtPoint(sl,coor_q,&phi_q);
          dirichlet_evaluator_gauss((const PetscReal*)coor_q,NULL,NULL,&phi_a,(void*)&time);
          
          integral_c[0] += w1[qi]*w1[qj] * (phi_q) * dJ_q;
          integral_c[1] += w1[qi]*w1[qj] * (phi_a) * dJ_q;
          integral_c[2] += w1[qi]*w1[qj] * (phi_q - phi_a)*(phi_q - phi_a) * dJ_q;
          int_cell_phi  += w1[qi]*w1[qj] * (phi_a) * dJ_q;
        }}
      int_cell_phi = int_cell_phi / cell_dv;
      integral_c[3] += (int_cell_phi - sl->value_c[c])*(int_cell_phi - sl->value_c[c]) * cell_dv;
      
    }
  }
  integral_c[0] /= DOMAIN_WIDTH;
  integral_c[1] /= DOMAIN_WIDTH;
  integral_c[2] /= DOMAIN_WIDTH;
  integral_c[3] /= DOMAIN_WIDTH;
  metric[0] = integral_c[0];
  metric[1] = integral_c[1];
  metric[2] = PetscSqrtReal(integral_c[2]);
  metric[3] = PetscSqrtReal(integral_c[3]);
  
  PetscFree(xi1);
  PetscFree(w1);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_May"
PetscErrorCode example_May(void)
{
  CSLPoly dsl;
  PetscInt i,k,c;
  PetscErrorCode (*create_polygon)(PetscInt*,PetscReal**,PetscInt**);
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt npoly;
  PetscReal *polycoor;
  PetscInt *edgelabel;
  PetscReal time = 0.0;
  PetscReal dt = 4.0/100.0,user_dt,user_cfldt;
  PetscReal ds = 0.1;
  PetscInt nt = 50000;
  PetscInt nout = 10;
  PetscReal metric0[3],metric[3],intphi0[4],intphi[4];
  PetscInt nsubcell = 2;
  PetscInt mx = 128;
  PetscReal tmax = 4.0;
  PetscReal cfl = 1.0;
  PetscBool use_cfl = PETSC_FALSE,use_user_dt = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscOptionsGetInt(NULL,NULL,"-mx",&mx,NULL);
  ds = (2.0*PETSC_PI) / ((PetscReal)mx);
  
  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nsubcell",&nsubcell,NULL);
  PetscOptionsGetReal(NULL,NULL,"-tmax",&tmax,NULL);
  
  PetscOptionsGetReal(NULL,NULL,"-cfl",&cfl,&use_cfl);
  PetscOptionsGetReal(NULL,NULL,"-dt",&user_dt,&use_user_dt);
  
  if (!use_user_dt && !use_cfl) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a time-step via either -dt <value> or -cfl <value>\n");
  if (use_user_dt && use_cfl) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a time-step method via either -dt <value> or -cfl <value>, not BOTH\n");
  
  if (use_cfl) {
    user_cfldt = cfl * (ds / 1.0);
    dt = user_cfldt;
  }
  if (use_user_dt) {
    dt = user_dt;
  }
  
  create_polygon      = box_polygon_expi;
  dirichlet_evaluator = dirichlet_evaluator_gauss;
  vel_evaluator       = vel_evaluator_const;
  
  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE);
  //CSLPolySetInterpolateType(dsl,I_CSPLINE_MONOTONE_BOUNDS);
  CSLPolySetInterpolateType(dsl,I_WENO3);
  CSLPolySetSubcellResolution(dsl,nsubcell,1);
  
  CSLPolySetDefaultDivergenceMethod(dsl);
  
  ierr = create_polygon(&npoly,&polycoor,&edgelabel);
  /*
   Force five cells in the y-direction.
   */
  DOMAIN_WIDTH = 5.0 * ds;
  polycoor[2*2+1] = DOMAIN_WIDTH;
  polycoor[2*3+1] = DOMAIN_WIDTH;
  CSLPolySetPolygonPoints(dsl,npoly,polycoor,edgelabel);
  
  CSLPolySetDomainDefault(dsl,ds);
  
  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  time = 0.0;
  /* set initial velocity field */
  for (i=0; i<dsl->nvert; i++) {
    PetscReal *pos = &dsl->coor_v[2*i];
    ierr = vel_evaluator(pos,&dsl->vel_v[2*i]);CHKERRQ(ierr);
  }
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;
    
    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;
  }
  
  /* setup - time dependent */
  CSLPolyComputeCellAverageDivVelocity(dsl);
  
  CSLPolyMarkInflowFacets(dsl);
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");
  
  PetscPrintf(PETSC_COMM_WORLD,"# Executing until time %1.4e (max steps %D)\n",tmax,nt);
  PetscPrintf(PETSC_COMM_WORLD,"# Using time-step corresponding to a CFL of %1.2e\n",dt/(ds/1.0));
  
  ierr = ComputeMetrics(dsl,metric0);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics_gauss(dsl,time,intphi0);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"# Initial condition:\n");
  PetscPrintf(PETSC_COMM_WORLD,"#   bounds           [%+1.2e , %+1.2e]\n",metric0[0],metric0[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   sum_K(phi_K.|K|)  %+1.12e\n",metric0[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int R[phi] dV    %+1.12e\n",intphi0[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int phi_anl dV   %+1.12e\n",intphi0[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(R[phi])        %+1.12e\n",intphi0[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(phi_K)         %+1.12e\n",intphi0[3]);
  
  PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : sum_K(phi_K.|K|) %+1.12e\n",0,0.0,metric0[0],metric0[1],metric0[2]);
  
  time = dt;
  for (k=1; k<=nt; k++) {
    char filename[128];
    
    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    //CSLPolyUpdate_Conservative(dsl,time,dt);
    CSLPolyUpdate_Strang2Multiplicative(dsl,time,dt);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    
    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    if (k%10 == 0) {
      ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
      //ierr = ComputeIntegralMetrics_const(dsl,time,intphi);CHKERRQ(ierr);
      //PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : approx int(c) %+1.4e : int(c) %+1.12e : int(c_exact) %+1.12e : L2(c) %+1.12e\n",k,time,metric[0],metric[1],metric[2],intphi[0],intphi[1],intphi[2]);
      PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : sum_K(phi_K.|K|) %+1.12e\n",k,time,metric[0],metric[1],metric[2]);
    }
    
    if (time >= tmax) { break; }
    
    time += dt;
  }
  
  ierr = ComputeMetrics(dsl,metric);CHKERRQ(ierr);
  ierr = ComputeIntegralMetrics_gauss(dsl,time,intphi);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Final state:\n");
  PetscPrintf(PETSC_COMM_WORLD,"#   bounds           [%+1.2e , %+1.2e]\n",metric[0],metric[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   sum_K(phi_K.|K|)  %+1.12e\n",metric[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#     ratio           %+1.12e\n",metric0[2]/metric[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int R[phi] dV    %+1.12e\n",intphi[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#     ratio           %+1.12e\n",intphi0[0]/intphi[0]);
  PetscPrintf(PETSC_COMM_WORLD,"#   \\int phi_anl dV   %+1.12e\n",intphi[1]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(R[phi])        %+1.12e\n",intphi[2]);
  PetscPrintf(PETSC_COMM_WORLD,"#   L2(phi_K)         %+1.12e\n",intphi[3]);
  
  PetscPrintf(PETSC_COMM_WORLD,"%4D t = %1.4e : [%+1.2e , %+1.2e] : approx int(c) %+1.4e : int(c) %+1.12e : int(c_exact) %+1.12e : L2(c) %+1.12e\n",k,time,metric[0],metric[1],metric[2],intphi[0],intphi[1],intphi[2]);
  
  PetscFunctionReturn(0);
}

void dirichlet_evaluator_ch2o(const PetscReal xn[],const PetscReal n[],const PetscReal vel[],PetscReal value[],void *ctx)
{
  value[0] = 2.0000e-03;
}

#undef __FUNCT__
#define __FUNCT__ "example_fromfile"
PetscErrorCode example_fromfile(void)
{
  CSLPoly dsl;
  PetscInt k,c;
  void (*dirichlet_evaluator)(const PetscReal*,const PetscReal*,const PetscReal*,PetscReal*,void*);
  
  const PetscReal ds = 8.05555833333333e-04;
  const PetscInt poly_nvert = 4;
  const PetscReal poly_vert[] = { 3.33330000000000e-02 , -3.33330000000000e-02, 1.00000000000000e+00 , -1.00000000000000e+00, 1.10000000000000e+00 , -1.00000000000000e+00, 1.10000000000000e+00 , -3.33330000000000e-02 };
  const PetscInt poly_edge_label[] = { 0 , 1 , 2 , 3 };
  
  PetscReal time = 0.0;
  const PetscReal dt = 8.33333333333334e-07;
  PetscInt nt = 50;
  PetscInt nout = 10;
  char filename[1024];
  const char prefix[] = "subfusc/models/wedge-0";
  FILE *vfp;
  PetscInt file_counter = 2;
  
  PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nout",&nout,NULL);
  
  dirichlet_evaluator = dirichlet_evaluator_ch2o;
  
  CSLPolyCreate(&dsl);
  CSLPolySetBlockSize(dsl,1);
  CSLPolySetInterpolateType(dsl,I_WENO3);
  CSLPolySetSubcellResolution(dsl,1,1);
  
  CSLPolySetDefaultDivergenceMethod(dsl);
  
  CSLPolySetPolygonPoints(dsl,poly_nvert,(PetscReal*)poly_vert,(PetscInt*)poly_edge_label);
  CSLPolySetDomainDefault(dsl,ds);
  
  /* setup - one time */
  CSLPolySetUpMesh(dsl);
  CSLPolyBoundaryPCreate(dsl);
  CSLPolySetUpFields(dsl);
  CSLPolyMarkCells(dsl);
  CSLPolyMarkVertices(dsl);
  CSLPolyLabelFacets(dsl);
  CSLPolyLabelCells(dsl);
  
  
  time = 0.0;
  /* set initial velocity field */
  sprintf(filename,"%s/csl_vl_%d.bin",prefix,file_counter);
  vfp = fopen(filename,"r");
  fread(dsl->vel_v,sizeof(PetscReal),2*dsl->nx[0]*dsl->nx[1],vfp);
  fclose(vfp);
  
  
  /* set initial phi */
  for (c=0; c<dsl->ncells; c++) {
    PetscReal *pos = dsl->cell_list[c]->coor;
    PetscReal G_xy = 0.0;
    
    if (dsl->cell_list[c]->type == COOR_INTERIOR) {
      dirichlet_evaluator((const PetscReal*)pos,NULL,NULL,&G_xy,(void*)&time);
    }
    dsl->value_c[c] = G_xy;
  }
  
  /* setup - time dependent */
  CSLPolyComputeCellAverageDivVelocity(dsl);
  
  CSLPolyMarkInflowFacets(dsl);
  
  CSLPolyApplyBC(dsl,0.0,bc_dirichlet,(void*)dirichlet_evaluator);
  
  CSLPolyReconstructExteriorCellFields(dsl);
  CSLPolyReconstructExteriorFacetFields(dsl);
  
  CSLPolyCellViewVTS(dsl,"c0.vts");
  CSLPolyFacetViewVTU(dsl,"f0.vtu");
  
  PetscPrintf(PETSC_COMM_WORLD,"# Executing (max steps %D)\n",nt);
  PetscPrintf(PETSC_COMM_WORLD,"# Using time-step %1.14e\n",dt);
  
  time = dt;
  for (k=1; k<=nt; k++) {
    char filename[128];
    printf("  step %d/%d\n",k,nt);
    
    CSLPolyApplyBC(dsl,time,bc_dirichlet,(void*)dirichlet_evaluator);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    CSLPolyUpdate_Strang2Multiplicative(dsl,time,dt);
    
    CSLPolyReconstructExteriorFacetFields(dsl);
    CSLPolyReconstructExteriorCellFields(dsl);
    
    
    if (k%nout==0) {
      sprintf(filename,"c%.6d.vts",k);
      CSLPolyCellViewVTS(dsl,filename);
    }
    
    file_counter++;
    time += dt;
  }
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt testid = 1;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  PetscOptionsGetInt(NULL,NULL,"-test",&testid,NULL);
  switch (testid) {
    case 0:
      ierr = example_Lentine();CHKERRQ(ierr);
      break;
    case 1:
      ierr = example_ReesJones();CHKERRQ(ierr);
      break;
    case 2:
      ierr = example_May();CHKERRQ(ierr);
      break;
    case 3:
      ierr = example_ReesJones2();CHKERRQ(ierr);
      break;
    case 4:
      ierr = example_fromfile();CHKERRQ(ierr);
      break;
    default:
      break;
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

