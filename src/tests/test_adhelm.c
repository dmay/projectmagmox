
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdehelmholtz.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <proj_init_finalize.h>

typedef struct {
  DM dm;
  Vec Xold;
} RHSContainer;

#undef __FUNCT__
#define __FUNCT__ "RHS_Xold"
PetscErrorCode RHS_Xold(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elX,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  Vec Xold;
  RHSContainer *data;
  const PetscScalar *LA_Xold;
  
  PetscFunctionBegin;
  
  data = (RHSContainer*)user_context;
  dm   = data->dm;
  Xold = data->Xold;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&elX);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Xold,&LA_Xold);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elX[i] = LA_Xold[nidx];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal Xold_q;
      
      /* get x,y coords at each quadrature point */
      Xold_q = 0.0;
      for (i=0; i<nbasis; i++) {
        Xold_q += tab_N[q][i] * elX[i];
      }
      
      coeff[e*nqp + q] = Xold_q;
    }
  }
  ierr = VecRestoreArrayRead(Xold,&LA_Xold);CHKERRQ(ierr);
  
  ierr = PetscFree(elX);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary"
PetscErrorCode EvalDirichletBoundary(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *ctx)
{
  *constrain = PETSC_FALSE;
  
  if (coor[1] < 1.0e-10) { /* bottom */
    *constrain = PETSC_TRUE;
    *val = 1.0;
  }
  if (coor[1] > 1.0 - 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

/*
 Advection diffusion example using finite elements and semi-Lagrangian.
 
 This example utilizes a finite element mesh with p=1 which is collocated with the
 underlying DMDA used to solve the transport term.
 Hence, no interpolation is required between the solution vector associated with the 
 diffusion solve and the transport update. Only injection is requied (here done via VecCopy()).
 
 ./test_adhelm.app -snes_monitor -ksp_monitor -ksp_type fgmres -dt 0.001 -log_view -nsteps 300
*/
#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  PetscInt k,M,N;
  PDE pde;
  DM dm,dmtet;
  Vec X,Xlocal,F,Xold;
  Mat JA;
  SNES snes;
  PetscInt nsteps = 10;
  PetscReal timestep = 0.001;
  BCList bc;
  RHSContainer *container;
  const char *fields[] = { "X" };
  char prefix[128];

  ierr = PetscOptionsGetInt(NULL,NULL,"-nsteps",&nsteps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&timestep,NULL);CHKERRQ(ierr);
  
  M = 64; PetscOptionsGetInt(NULL,NULL,"-M",&M,NULL);
  N = 64; PetscOptionsGetInt(NULL,NULL,"-N",&N,NULL);
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M+1,N+1,1,xr,yr,&dsl);CHKERRQ(ierr);
  /* reach in and jank the dm from the semi-lagrangian context (yuck - don't do this) */
  dm = dsl->dm;
  ierr = DMDASetFieldName(dm,0,"phi");CHKERRQ(ierr);
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],M,N, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dmtet);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmtet,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtet,&Xold);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmtet,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtet,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmtet,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);

  /* define Dirichlet bc */
  ierr = DMBCListCreate(dmtet,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);

  /* define velocity field <da> */
  {
    const DMDACoor2d **LA_coor;
    PetscScalar *LA_field;
    PetscInt i,j,xm,ym,xs,ys;
    DM dmc;
    Vec coor;
    
    ierr = DMDAGetCorners(dm,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(dm,&dmc);CHKERRQ(ierr);
    ierr = DMGetCoordinates(dm,&coor);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);

    ierr = VecGetArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    for (i=0; i<xm; i++) {
      for (j=0; j<ym; j++) {
        PetscInt idx = i + j * xm;
        const PetscReal xp = LA_coor[j][i].x;
        const PetscReal yp = LA_coor[j][i].y;
        
        LA_field[2*idx+0] =  (yp-0.5) * sin(M_PI*xp) * 500.0;
        LA_field[2*idx+1] = -(xp-0.5) * sin(M_PI*yp) * 500.0;
      }
    }
    ierr = VecRestoreArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  }
  
  /* define initial condition <tet> */
  {
    PetscScalar *LA_X;
    PetscReal *coords;
    PetscInt nnodes,i;

    ierr = DMTetSpaceElement(dmtet,NULL,NULL,NULL,NULL,&nnodes,&coords);CHKERRQ(ierr);
    ierr = VecZeroEntries(X);CHKERRQ(ierr);
    ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
    for (i=0; i<nnodes; i++) {
      const PetscReal xp = coords[2*i+0];
      const PetscReal yp = coords[2*i+1];
      
      LA_X[i] = (1.0 - yp) + 0.1*sin(M_PI*yp)*cos(M_PI*xp);
      //printf("%d %1.4e %1.4e\n",i,xp,yp);
      if (yp < 1.0e-10)     { LA_X[i] = 1.0; /* printf("*** i %d \n",i); */}
      if (yp > 1.0-1.0e-10) { LA_X[i] = 0.0; }
      
    }
    ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);

  }
  ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmtet,bc);CHKERRQ(ierr);
  
  {
    Coefficient k,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,timestep);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(1,&container);CHKERRQ(ierr);
    container->dm   = dmtet;
    container->Xold = Xold;
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(g,RHS_Xold,NULL,(void*)container);CHKERRQ(ierr);
  }
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"vel.vtr");CHKERRQ(ierr);
  ierr = VecView(dsl->velocity,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  PetscSNPrintf(prefix,127,"%s_step%D_",fields[0],0);
  ierr = DMTetViewFields_VTU(dmtet,1,&X,fields,prefix);CHKERRQ(ierr);

  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  for (k=1; k<=nsteps; k++) {
  
    PetscPrintf(PETSC_COMM_WORLD,"=== Step %D ===\n",k);
    ierr = DMSemiLagrangianUpdate_DMDA(dsl,timestep,X);CHKERRQ(ierr);
    
    ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);
    ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
    ierr = BCListInsert(bc,Xold);CHKERRQ(ierr);
    
    ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
    //ierr = VecView(X,viewer);CHKERRQ(ierr);
    
    if (k%20 == 0) {
      PetscSNPrintf(prefix,127,"%s_step%D_",fields[0],k);
      ierr = DMTetViewFields_VTU(dmtet,1,&X,fields,prefix);CHKERRQ(ierr);
    }

  }
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xold);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtet);CHKERRQ(ierr);
  ierr = PetscFree(container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 ./test_adhelm.app -snes_monitor -ksp_monitor -ksp_type fgmres -dt 0.001 -log_view -nsteps 200 -pc_type lu -Md 12 -Nd 12 -M 201 -N 201
*/
#undef __FUNCT__
#define __FUNCT__ "example1_noncollocated"
PetscErrorCode example1_noncollocated(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  PetscInt k,M,N,Md,Nd;
  PDE pde;
  DM dmda,dmtet;
  Vec X,Xsl,Xlocal,F,Xold;
  Mat JA;
  SNES snes;
  PetscInt nsteps = 10;
  PetscReal timestep = 0.001;
  BCList bc;
  RHSContainer *container;
  const char *fields[] = { "X" };
  char prefix[128];
  Mat interp_tet2da;
  Mat interp_da2tet;
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-nsteps",&nsteps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&timestep,NULL);CHKERRQ(ierr);
  
  M = 65; PetscOptionsGetInt(NULL,NULL,"-M",&M,NULL);
  N = 65; PetscOptionsGetInt(NULL,NULL,"-N",&N,NULL);
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  /* reach in and jank the dm from the semi-lagrangian context (yuck - don't do this) */
  dmda = dsl->dm;
  ierr = DMDASetFieldName(dmda,0,"phi");CHKERRQ(ierr);
  
  /* p = 3 */
  Md = 24; PetscOptionsGetInt(NULL,NULL,"-Md",&Md,NULL);
  Nd = 24; PetscOptionsGetInt(NULL,NULL,"-Nd",&Nd,NULL);
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],Md,Nd, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,6,&dmtet);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dmda,&Xsl);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dmtet,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtet,&Xold);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmtet,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtet,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmtet,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  ierr = DMCreateInterpolation(dmtet,dmda,&interp_tet2da,NULL);CHKERRQ(ierr);
  ierr = DMCreateInterpolation(dmda,dmtet,&interp_da2tet,NULL);CHKERRQ(ierr);
  
  /* define Dirichlet bc */
  ierr = DMBCListCreate(dmtet,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  
  /* define velocity field <da> */
  {
    const DMDACoor2d **LA_coor;
    PetscScalar *LA_field;
    PetscInt i,j,xm,ym,xs,ys;
    DM dmc;
    Vec coor;
    
    ierr = DMDAGetCorners(dmda,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(dmda,&dmc);CHKERRQ(ierr);
    ierr = DMGetCoordinates(dmda,&coor);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
    
    ierr = VecGetArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    for (i=0; i<xm; i++) {
      for (j=0; j<ym; j++) {
        PetscInt idx = i + j * xm;
        const PetscReal xp = LA_coor[j][i].x;
        const PetscReal yp = LA_coor[j][i].y;
        
        LA_field[2*idx+0] =  (yp-0.5) * sin(M_PI*xp) * 500.0;
        LA_field[2*idx+1] = -(xp-0.5) * sin(M_PI*yp) * 500.0;
      }
    }
    ierr = VecRestoreArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  }
  
  /* define initial condition <tet> */
  {
    PetscScalar *LA_X;
    PetscReal *coords;
    PetscInt nnodes,i;
    
    ierr = DMTetSpaceElement(dmtet,NULL,NULL,NULL,NULL,&nnodes,&coords);CHKERRQ(ierr);
    ierr = VecZeroEntries(X);CHKERRQ(ierr);
    ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
    for (i=0; i<nnodes; i++) {
      const PetscReal xp = coords[2*i+0];
      const PetscReal yp = coords[2*i+1];
      
      LA_X[i] = (1.0 - yp) + 0.1*sin(M_PI*yp)*cos(M_PI*xp);
      if (yp < 1.0e-10)     { LA_X[i] = 1.0; }
      if (yp > 1.0-1.0e-10) { LA_X[i] = 0.0; }
      
    }
    ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
    
  }
  ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmtet,bc);CHKERRQ(ierr);
  
  {
    Coefficient k,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,timestep);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(1,&container);CHKERRQ(ierr);
    container->dm   = dmtet;
    container->Xold = Xold;
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(g,RHS_Xold,NULL,(void*)container);CHKERRQ(ierr);
  }
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"vel.vtr");CHKERRQ(ierr);
  ierr = VecView(dsl->velocity,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_step%D_",fields[0],0);
  ierr = DMTetViewFields_VTU(dmtet,1,&X,fields,prefix);CHKERRQ(ierr);
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  for (k=1; k<=nsteps; k++) {
    
    PetscPrintf(PETSC_COMM_WORLD,"=== Step %D ===\n",k);
    ierr = MatMult(interp_tet2da,X,Xsl);CHKERRQ(ierr);
    ierr = DMSemiLagrangianUpdate_DMDA(dsl,timestep,Xsl);CHKERRQ(ierr);
    ierr = MatMult(interp_da2tet,Xsl,X);CHKERRQ(ierr);
    
    ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);
    ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
    ierr = BCListInsert(bc,Xold);CHKERRQ(ierr);
    
    ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
    //ierr = VecView(X,viewer);CHKERRQ(ierr);
    
    if (k%20 == 0) {
      PetscSNPrintf(prefix,127,"%s_step%D_",fields[0],k);
      ierr = DMTetViewFields_VTU(dmtet,1,&X,fields,prefix);CHKERRQ(ierr);
    }
  }
  ierr = MatDestroy(&interp_tet2da);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_da2tet);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xsl);CHKERRQ(ierr);
  ierr = VecDestroy(&Xold);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtet);CHKERRQ(ierr);
  ierr = PetscFree(container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* Interpolate from DMTET to DMTET with different h and p */
#undef __FUNCT__
#define __FUNCT__ "example2"
PetscErrorCode example2(void)
{
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscErrorCode ierr;
  PetscInt M,N;
  DM dmtetF,dmtetT;
  const char *fields[] = { "X" };
  char prefix[128];
  Vec X,Xinterp;
  Mat interp;
  
  M = 3;
  N = 4;
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],M,N, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,6,&dmtetF);CHKERRQ(ierr);

  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],2*M,2*N, PETSC_FALSE,
                                     1,DMTET_CWARP_AND_BLEND,4,&dmtetT);CHKERRQ(ierr);

  
  ierr = DMCreateGlobalVector(dmtetF,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtetT,&Xinterp);CHKERRQ(ierr);
  
  /* define initial condition <tet> */
  {
    PetscScalar *LA_X;
    PetscReal *coords;
    PetscInt nnodes,i;
    
    ierr = DMTetSpaceElement(dmtetF,NULL,NULL,NULL,NULL,&nnodes,&coords);CHKERRQ(ierr);
    ierr = VecZeroEntries(X);CHKERRQ(ierr);
    ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
    for (i=0; i<nnodes; i++) {
      const PetscReal xp = coords[2*i+0];
      const PetscReal yp = coords[2*i+1];
      
      LA_X[i] = (1.0 - yp) + 0.1*sin(M_PI*yp)*cos(M_PI*xp);
      if (yp < 1.0e-10)     { LA_X[i] = 1.0; /* printf("*** i %d \n",i); */}
      if (yp > 1.0-1.0e-10) { LA_X[i] = 0.0; }
      
    }
    ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
  }

  PetscSNPrintf(prefix,127,"%s_from_",fields[0]);
  ierr = DMTetViewFields_VTU(dmtetF,1,&X,fields,prefix);CHKERRQ(ierr);

  ierr = DMCreateInterpolation(dmtetF,dmtetT,&interp,NULL);CHKERRQ(ierr);
  
  ierr = MatMult(interp,X,Xinterp);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_to_",fields[0]);
  ierr = DMTetViewFields_VTU(dmtetT,1,&Xinterp,fields,prefix);CHKERRQ(ierr);

  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xinterp);CHKERRQ(ierr);
  ierr = MatDestroy(&interp);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtetF);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtetT);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* Interpolate from DMDA to DMTET */
#undef __FUNCT__
#define __FUNCT__ "example2a"
PetscErrorCode example2a(void)
{
  PetscErrorCode ierr;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscInt M,N;
  DMSemiLagrangian dsl;
  DM dmda,dmtet,dmF,dmT;
  const char *fields[] = { "X" };
  char prefix[128];
  Vec X,Xinterp;
  Mat interp;
  PetscViewer viewer;
  
  M = 33;
  N = 42;
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],M,N, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,6,&dmtet);CHKERRQ(ierr);
  
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,4*M+1,4*N+1,1,xr,yr,&dsl);CHKERRQ(ierr);
  dmda = dsl->dm;
  
  /* [1] Test from DMTET to DMDA */
  dmF = dmtet;
  dmT = dmda;
  
  ierr = DMCreateGlobalVector(dmF,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&Xinterp);CHKERRQ(ierr);
  
  /* define function <tet> */
  {
    PetscScalar *LA_X;
    PetscReal *coords;
    PetscInt nnodes,i;
    
    ierr = DMTetSpaceElement(dmF,NULL,NULL,NULL,NULL,&nnodes,&coords);CHKERRQ(ierr);
    ierr = VecZeroEntries(X);CHKERRQ(ierr);
    ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
    for (i=0; i<nnodes; i++) {
      const PetscReal xp = coords[2*i+0];
      const PetscReal yp = coords[2*i+1];
      
      LA_X[i] = (1.0 - yp) + 0.25*sin(M_PI*yp)*cos(M_PI*xp);
    }
    ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
  }
  
  /* view original */
  PetscSNPrintf(prefix,127,"%s_from_",fields[0]);
  ierr = DMTetViewFields_VTU(dmF,1,&X,fields,prefix);CHKERRQ(ierr);
  
  /* interpolate */
  ierr = DMCreateInterpolation(dmF,dmT,&interp,NULL);CHKERRQ(ierr);
  ierr = MatMult(interp,X,Xinterp);CHKERRQ(ierr);
  
  /* view result */
  PetscSNPrintf(prefix,127,"%s_to_dmda.vtr",fields[0]);
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,prefix);CHKERRQ(ierr);
  ierr = VecView(Xinterp,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xinterp);CHKERRQ(ierr);
  ierr = MatDestroy(&interp);CHKERRQ(ierr);
  
  
  /* [2] Test from DMTET to DMDA */
  dmF = dmda;
  dmT = dmtet;
  
  ierr = DMCreateGlobalVector(dmF,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmT,&Xinterp);CHKERRQ(ierr);
  
  /* define function <da> */
  {
    PetscScalar *LA_X;
    const PetscReal *LA_coords;
    Vec coor;
    PetscInt nnodes,i,M,N;
    
    ierr = DMDAGetInfo(dmF,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
    nnodes = M * N;
    ierr = DMGetCoordinates(dmF,&coor);CHKERRQ(ierr);
    ierr = VecGetArrayRead(coor,&LA_coords);CHKERRQ(ierr);
    ierr = VecZeroEntries(X);CHKERRQ(ierr);
    ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
    for (i=0; i<nnodes; i++) {
      const PetscReal xp = LA_coords[2*i+0];
      const PetscReal yp = LA_coords[2*i+1];
      
      LA_X[i] = sin(1.7*M_PI*xp)*cos(2.2*M_PI*yp*xp);
    }
    ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(coor,&LA_coords);CHKERRQ(ierr);
  }
  
  /* view original */
  PetscSNPrintf(prefix,127,"%s1_from_dmda.vtr",fields[0]);
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,prefix);CHKERRQ(ierr);
  ierr = VecView(X,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  /* interpolate */
  ierr = DMCreateInterpolation(dmF,dmT,&interp,NULL);CHKERRQ(ierr);
  ierr = MatMult(interp,X,Xinterp);CHKERRQ(ierr);
  
  /* view result */
  PetscSNPrintf(prefix,127,"%s1_to_",fields[0]);
  ierr = DMTetViewFields_VTU(dmT,1,&Xinterp,fields,prefix);CHKERRQ(ierr);
  
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xinterp);CHKERRQ(ierr);
  ierr = MatDestroy(&interp);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtet);CHKERRQ(ierr);
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* "bad" helmholtz solver example */
/*
 ./test_adhelm.app -pc_type mg -ksp_monitor -mg_levels_pc_type ilu -mg_levels_ksp_max_it 30 -ksp_type fgmres -snes_monitor -mg_levels_ksp_type gmres -ksp_rtol 1.0e-8 -M 32 -N 32
*/
#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  PetscInt k,M,N;
  PDE pde;
  DM dm,dmtet,dmtetH[3];
  Vec X,Xlocal,F,Xold;
  Mat JA;
  SNES snes;
  PetscInt nsteps = 1;
  PetscReal timestep = 0.01;
  BCList bc;
  RHSContainer *container;
  const char *fields[] = { "X" };
  char prefix[128];
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-nsteps",&nsteps,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dt",&timestep,NULL);CHKERRQ(ierr);
  
  M = 128; PetscOptionsGetInt(NULL,NULL,"-M",&M,NULL);
  N = 128; PetscOptionsGetInt(NULL,NULL,"-N",&N,NULL);
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M+1,N+1,1,xr,yr,&dsl);CHKERRQ(ierr);
  /* reach in and jank the dm from the semi-lagrangian context (yuck - don't do this) */
  dm = dsl->dm;
  ierr = DMDASetFieldName(dm,0,"phi");CHKERRQ(ierr);
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],M,N, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dmtet);CHKERRQ(ierr);
  dmtetH[2] = dmtet;
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],M/2,N/2, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dmtetH[1]);CHKERRQ(ierr);

  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,xr[0],xr[1],yr[0],yr[1],M/4,N/4, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dmtetH[0]);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmtet,&X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtet,&Xold);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dmtet,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmtet,&F);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmtet,&JA);CHKERRQ(ierr);
  ierr = MatSetOption(JA,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);CHKERRQ(ierr);
  
  /* define Dirichlet bc */
  ierr = DMBCListCreate(dmtet,&bc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  
  /* define velocity field <da> */
  {
    const DMDACoor2d **LA_coor;
    PetscScalar *LA_field;
    PetscInt i,j,xm,ym,xs,ys;
    DM dmc;
    Vec coor;
    
    ierr = DMDAGetCorners(dm,&xs,&ys,0,&xm,&ym,0);CHKERRQ(ierr);
    ierr = DMGetCoordinateDM(dm,&dmc);CHKERRQ(ierr);
    ierr = DMGetCoordinates(dm,&coor);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
    
    ierr = VecGetArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    for (i=0; i<xm; i++) {
      for (j=0; j<ym; j++) {
        PetscInt idx = i + j * xm;
        const PetscReal xp = LA_coor[j][i].x;
        const PetscReal yp = LA_coor[j][i].y;
        
        LA_field[2*idx+0] =  (yp-0.5) * sin(M_PI*xp) * 500.0;
        LA_field[2*idx+1] = -(xp-0.5) * sin(M_PI*yp) * 500.0;
      }
    }
    ierr = VecRestoreArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArrayRead(dmc,coor,&LA_coor);CHKERRQ(ierr);
  }
  
  /* define initial condition <tet> */
  {
    PetscScalar *LA_X;
    PetscReal *coords;
    PetscInt nnodes,i;
    
    ierr = DMTetSpaceElement(dmtet,NULL,NULL,NULL,NULL,&nnodes,&coords);CHKERRQ(ierr);
    ierr = VecZeroEntries(X);CHKERRQ(ierr);
    ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
    for (i=0; i<nnodes; i++) {
      const PetscReal xp = coords[2*i+0];
      const PetscReal yp = coords[2*i+1];
      
      LA_X[i] = (1.0 - yp) + 0.1*sin(M_PI*yp)*cos(M_PI*xp);
      if (yp < 1.0e-10)     { LA_X[i] = 1.0; }
      if (yp > 1.0-1.0e-10) { LA_X[i] = 0.0; }
      
    }
    ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
    
  }
  ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dmtet,bc);CHKERRQ(ierr);
  
  {
    Coefficient k,g;
    
    ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
    //ierr = CoefficientSetDomainConstant(k,timestep);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,-1.0/400.0);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(1,&container);CHKERRQ(ierr);
    container->dm   = dmtet;
    container->Xold = Xold;
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(g,RHS_Xold,NULL,(void*)container);CHKERRQ(ierr);
  }
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"vel.vtr");CHKERRQ(ierr);
  ierr = VecView(dsl->velocity,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_step%D_",fields[0],0);
  ierr = DMTetViewFields_VTU(dmtet,1,&X,fields,prefix);CHKERRQ(ierr);
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,JA,JA);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);

  {
    KSP ksp;
    PC  pc;
    Mat interp[3];
    
    ierr = DMCreateInterpolation(dmtetH[1],dmtetH[2],&interp[2],NULL);CHKERRQ(ierr);
    ierr = DMCreateInterpolation(dmtetH[0],dmtetH[1],&interp[1],NULL);CHKERRQ(ierr);
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    ierr = PCMGSetGalerkin(pc,PETSC_TRUE);CHKERRQ(ierr);
    ierr = PCMGSetLevels(pc,3,NULL);CHKERRQ(ierr);
    ierr = PCMGSetInterpolation(pc,2,interp[2]);CHKERRQ(ierr);
    ierr = PCMGSetInterpolation(pc,1,interp[1]);CHKERRQ(ierr);
    
    ierr = MatDestroy(&interp[2]);CHKERRQ(ierr);
    ierr = MatDestroy(&interp[1]);CHKERRQ(ierr);
  }
  
  for (k=1; k<=nsteps; k++) {
    
    PetscPrintf(PETSC_COMM_WORLD,"=== Step %D ===\n",k);
    //ierr = DMSemiLagrangianUpdate_DMDA(dsl,timestep,X);CHKERRQ(ierr);
    
    ierr = PDEUpdateLocalSolution(pde);CHKERRQ(ierr);
    //ierr = VecCopy(X,Xold);CHKERRQ(ierr); /* x --> x_old */
    //ierr = BCListInsert(bc,Xold);CHKERRQ(ierr);
    
    ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
    
    PetscSNPrintf(prefix,127,"%s_step%D_",fields[0],k);
    ierr = DMTetViewFields_VTU(dmtet,1,&X,fields,prefix);CHKERRQ(ierr);
    
  }
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = DMSemiLagrangianDestroy(&dsl);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xold);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = MatDestroy(&JA);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtetH[2]);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtetH[1]);CHKERRQ(ierr);
  ierr = DMDestroy(&dmtetH[0]);CHKERRQ(ierr);
  ierr = PetscFree(container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = example1();CHKERRQ(ierr);
  ierr = example1_noncollocated();CHKERRQ(ierr);
  //ierr = example2();CHKERRQ(ierr);
  //ierr = example2a();CHKERRQ(ierr);
  //ierr = example3();CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

