
#include <petsc.h>
#include <petscdm.h>

#include <dmbcs.h>
#include <dmsl.h>
#include <proj_init_finalize.h>

#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  Vec phi,coor;
  const PetscScalar *LA_coor;
  PetscScalar *LA_field;
  PetscInt i,j,k,M,N,nx,ny;
  DM dm;
  
  M = 512;
  N = 512;
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianSetType(dsl,SLTYPE_CLASSIC);CHKERRQ(ierr);
  //err = DMDASemiLagrangianSetType(dsl,SLTYPE_FVCONSERVATIVE);CHKERRQ(ierr);
  //dsl->dactx->interpolation_type = DASLInterp_QMonotone;
  
  /* set initial velocity field */
  ierr = DMDASemiLagrangianGetVertexDM(dsl,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);

  VecGetArray(dsl->velocity,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      const PetscReal *pos = &LA_coor[2*idx];

      LA_field[2*idx+0] = -(pos[1]-0.5);
      LA_field[2*idx+1] =  (pos[0]-0.5);
    }
  }
  VecRestoreArray(dsl->velocity,&LA_field);
  VecRestoreArrayRead(coor,&LA_coor);
  
  /* set initial phi */
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl,&phi);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianGetSLDM(dsl,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);

  VecGetArray(phi,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      const PetscReal *xn = &LA_coor[2*idx];
      PetscReal G_xy,r_xy,xn0[2];
      
      LA_field[idx] = 0.0;
      G_xy = 0.0;
      
      /* slotted cylinder */
      xn0[0] = 0.5;
      xn0[1] = 0.75;
      r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
      if (r_xy < 1.0) {
        if ((fabs(xn[0] - xn0[0]) >= 0.025) || (xn[1] >= 0.85)) {
          G_xy = 1.0;
        }
      }
      
      /* conical body */
      xn0[0] = 0.5;
      xn0[1] = 0.25;
      r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
      if (r_xy < 1.0) {
        G_xy = 1.0 - r_xy;
      }

      /* smooth peak */
      xn0[0] = 0.25;
      xn0[1] = 0.5;
      r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
      if (r_xy < 1.0) {
        G_xy = 0.25*(1.0 + cos(M_PI*r_xy));
      }
      
      LA_field[idx] = G_xy;
      
    }
  }
  VecRestoreArray(phi,&LA_field);

  VecRestoreArrayRead(coor,&LA_coor);

  ierr = VecCopy(phi,dsl->phi_dep);CHKERRQ(ierr);

  ierr = DMSLSetup(dsl);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"phi_0.vtr");CHKERRQ(ierr);
  ierr = VecView(phi,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = SLDMView(dsl,"dmsl_0");CHKERRQ(ierr);
  //ierr = ConservativeSLView(dsl,"dmcsl_ic");CHKERRQ(ierr);

  for (k=1; k<100; k++) {
    char filename[128];
    PetscReal dt;
    
    dt = (2.0*M_PI/99.0);
    //ierr = DMSemiLagrangianUpdate_DMDA(dsl,(k-1)*dt,dt,phi);CHKERRQ(ierr);
    ierr = DMDASemiLagrangianUpdate(dsl,(k-1)*dt,dt,phi);CHKERRQ(ierr);
    
    if (k%10 == 0) {
      PetscSNPrintf(filename,127,"phi_%D.vtr",k);
      ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,filename);CHKERRQ(ierr);
      ierr = VecView(phi,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      
      
      PetscSNPrintf(filename,127,"dmsl_%D",k);
      ierr = SLDMView(dsl,filename);CHKERRQ(ierr);
    }
    
  }
  /*
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"vel.vtr");CHKERRQ(ierr);
  ierr = VecView(dsl->velocity,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
*/
  PetscFunctionReturn(0);
}

PetscErrorCode box(PetscReal pos[],PetscBool *inside,void *ctx)
{
  *inside = PETSC_TRUE;
  
  if ((0.0 - pos[0]) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((pos[0] - 1.0) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((0.0 - pos[1]) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((pos[1] - 1.0) > 1.0e-10) { *inside = PETSC_FALSE; }
  
  PetscFunctionReturn(0);
}

PetscErrorCode box_small(PetscReal pos[],PetscBool *inside,void *ctx)
{
  *inside = PETSC_TRUE;
  
  if ((0.3 - pos[0]) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((pos[0] - 0.8) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((0.3 - pos[1]) > 1.0e-10) { *inside = PETSC_FALSE; }
  if ((pos[1] - 0.8) > 1.0e-10) { *inside = PETSC_FALSE; }
  
  PetscFunctionReturn(0);
}

PetscErrorCode circle(PetscReal pos[],PetscBool *inside,void *ctx)
{
  PetscReal rr;
  *inside = PETSC_TRUE;
  
  rr = PetscSqrtReal( (pos[0]-0.5)*(pos[0]-0.5) + (pos[1]-0.5)*(pos[1]-0.5)  );
  if (rr > 0.3) { *inside = PETSC_FALSE; }
  
  PetscFunctionReturn(0);
}

PetscErrorCode triangle(PetscReal pos[],PetscBool *inside,void *ctx)
{
  *inside = PETSC_TRUE;
  
  if (pos[1] < 0.23) { *inside = PETSC_FALSE; }
  if (pos[1] > 3.0*pos[0]) { *inside = PETSC_FALSE; }
  if (pos[1] > 1.1 - pos[0]) { *inside = PETSC_FALSE; }

  PetscFunctionReturn(0);
}

PetscErrorCode wedge(PetscReal pos[],PetscBool *inside,void *ctx)
{
  *inside = PETSC_TRUE;

  if (pos[1] < 1.0-2.2*pos[0]) { *inside = PETSC_FALSE; }
  if (pos[1] < 0.33) { *inside = PETSC_FALSE; }
  if (pos[1] > 0.8) { *inside = PETSC_FALSE; }

  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_cylinder(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.5;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  *constrain = PETSC_TRUE;
  *value = G_xy;
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_cylinderSmall(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.4;
  xn0[1] = 0.4;
  G_xy = 0.0;
  r_xy = (1.0/0.07)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  *constrain = PETSC_TRUE;
  *value = G_xy;
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_cylinderLeft(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  /* cylinder */
  xn0[0] = 0.0;
  xn0[1] = 0.5;
  G_xy = 0.0;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0;
  }
  *constrain = PETSC_TRUE;
  *value = G_xy;
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_3blobs(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  G_xy = 0.0;
  
  /* slotted cylinder */
  xn0[0] = 0.5;
  xn0[1] = 0.5;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    //if ((fabs(xn[0] - xn0[0]) >= 0.025) || (xn[1] >= 0.85)) {
      G_xy = 1.0;
    //}
  }
  
  /* conical body */
  xn0[0] = 0.5;
  xn0[1] = 0.25;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  /* smooth peak */
  xn0[0] = 0.25;
  xn0[1] = 0.5;
  r_xy = (1.0/0.15)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 0.25*(1.0 + cos(M_PI*r_xy));
  }

  *constrain = PETSC_TRUE;
  *value = G_xy;
  
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_4blobs(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2];
  
  G_xy = 0.0;
  
  /* conical body */
  xn0[0] = 0.25;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  xn0[0] = 0.4;
  xn0[1] = 0.38;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }

  xn0[0] = 0.15;
  xn0[1] = 0.65;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }

  xn0[0] = 0.7;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }

  *constrain = PETSC_TRUE;
  *value = G_xy;
  
  PetscFunctionReturn(0);
}

PetscErrorCode dirichlet_evaluator_4blobs_t(PetscReal xn[],PetscReal time,PetscBool *constrain,PetscReal *value,void *ctx)
{
  PetscReal G_xy,r_xy,xn0[2],v[2];
  
  G_xy = 0.0;
  
  /* conical body */
  xn0[0] = 0.25;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  xn0[0] = 0.4;
  xn0[1] = 0.38;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  /* make this one time dependent - it will move along the line y = 1-2.2x */
  v[0] = 1.0/2.2;
  v[1] = -1.0;
  xn0[0] = 0.15 + (v[0] * 0.3) * time;
  xn0[1] = 0.65 + (v[1] * 0.3) * time;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  xn0[0] = 0.7;
  xn0[1] = 0.5;
  r_xy = (1.0/0.05)*sqrt((xn[0]-xn0[0])*(xn[0]-xn0[0]) + (xn[1]-xn0[1])*(xn[1]-xn0[1]));
  if (r_xy < 1.0) {
    G_xy = 1.0 - r_xy;
  }
  
  *constrain = PETSC_TRUE;
  *value = G_xy;
  
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_down(PetscReal pos[],PetscReal value[])
{
  value[0] = -0.15;
  value[1] = -0.3;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_rigidRotation(PetscReal pos[],PetscReal value[])
{
  // solid body rotation about origin
  value[0] = -(pos[1]-0.5);
  value[1] =  (pos[0]-0.5);
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_45Deg(PetscReal pos[],PetscReal value[])
{
  // 45 degree flow
  value[0] = 0.5;
  value[1] = 0.5;
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_cornerFlow(PetscReal pos[],PetscReal value[])
{
  // corner flow
  value[0] =  pos[0];
  value[1] = -pos[1];
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_source(PetscReal pos[],PetscReal value[])
{
  // source
  value[0] = 0.3*(pos[0] - 0.5);
  value[1] = 0.3*(pos[1] - 0.5);
  PetscFunctionReturn(0);
}

PetscErrorCode vel_evaluator_liquidFlow(PetscReal pos[],PetscReal value[])
{
  PetscReal pp[2],vxr[2],cc[2],sink[2];
  
  pp[0] = 10.0*(pos[0]-0.5);
  pp[1] = 10.0*(pos[1]-0.5);

  /*
  value[0] = 1.0e-2*(1.0 - 11.0*pp[1] - 7.0*pp[0]);
  value[1] = 1.0e-2*(14.0 + 2.0*pp[0]*pp[0]);
  */
  vxr[0] = 1.0e-2*(1.0 - 11.0*pp[1] - 7.0*pp[0]);
  vxr[1] = 1.0e-2*(14.0 + 2.0*pp[0]*pp[0]);
  
  cc[0] = (pos[0] - 0.6);
  cc[1] = (pos[1] - 0.7);
  sink[0] = -(pos[0] - 0.6);
  sink[1] = -(pos[1] - 0.7);
  if (PetscSqrtReal(sink[0]*sink[0] + sink[1]*sink[1]) > 0.3) {
    sink[0] = 0.3;
    sink[1] = 0.3;
  }
  
  value[0] = 1.0*vxr[0] + 8.0*sink[0] * (exp(-100.0*(cc[0]*cc[0]+cc[1]*cc[1])));
  value[1] = 1.0*vxr[1] + 8.0*sink[1] * (exp(-100.0*(cc[0]*cc[0]+cc[1]*cc[1])));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example2"
PetscErrorCode example2(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  Vec phi,coor;
  const PetscScalar *LA_coor;
  PetscScalar *LA_field;
  PetscInt i,j,k,M,N,nx,ny;
  DM dm;
  PetscErrorCode (*inside_domain)(PetscReal*,PetscBool*,void*);
  PetscErrorCode (*dirichlet_evaluator)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt model;

  model = 0;
  PetscOptionsGetInt(NULL,NULL,"-model",&model,NULL);
  M = 129;
  N = 129;
  PetscOptionsGetInt(NULL,NULL,"-nx",&M,NULL);
  PetscOptionsGetInt(NULL,NULL,"-ny",&N,NULL);
  
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianSetType(dsl,SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE);CHKERRQ(ierr);
  //dsl->dactx->interpolation_type = DASLInterp_QMonotone;

  /*
  inside_domain = box;
  //inside_domain = box_small;
  //inside_domain = circle;
  //inside_domain = triangle;
  //inside_domain = wedge;
  
  dirichlet_evaluator = dirichlet_evaluator_cylinder;
  dirichlet_evaluator = dirichlet_evaluator_cylinderLeft;
  dirichlet_evaluator = dirichlet_evaluator_3blobs;
  
  vel_evaluator = vel_evaluator_rigidRotation;
  vel_evaluator = vel_evaluator_45Deg;
  vel_evaluator = vel_evaluator_cornerFlow;
  vel_evaluator = vel_evaluator_source;
  */
  switch (model) {
    case 0:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 1:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinderLeft;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 12:
      inside_domain = NULL;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;

    case 2:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_cornerFlow;
      break;
    case 3:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinderLeft;
      vel_evaluator = vel_evaluator_cornerFlow;
      break;
      
    case 4:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 5:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 6:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_rigidRotation;
      break;

    case 7:
      inside_domain = wedge;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_45Deg;
      break;
    case 11:
      inside_domain = wedge;
      dirichlet_evaluator = dirichlet_evaluator_4blobs;
      vel_evaluator = vel_evaluator_liquidFlow;
      break;
    case 13:
      inside_domain = wedge;
      dirichlet_evaluator = dirichlet_evaluator_4blobs_t;
      vel_evaluator = vel_evaluator_liquidFlow;
      break;

    case 8:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_3blobs;
      vel_evaluator = vel_evaluator_source;
      break;
    case 9:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_cylinderSmall;
      vel_evaluator = vel_evaluator_source;
      break;
    case 10:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_cylinderSmall;
      vel_evaluator = vel_evaluator_down;
      break;


    default:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
  }
   
  ierr = DMDASemiLagrangian_FVConservative_SetDomainIdentifier(dsl,inside_domain,NULL);CHKERRQ(ierr);
  ierr = DMDASemiLagrangian_FVConservative_SetDirichletEvaluator(dsl,dirichlet_evaluator,NULL);CHKERRQ(ierr);
  
  //ierr = DMDASemiLagrangian_FVConservative_SetUpBoundary(dsl);CHKERRQ(ierr);
  //ierr = DMDASemiLagrangian_FVConservative_DirichletEvaluate(dsl);CHKERRQ(ierr);
  
  /* set initial velocity field */
  ierr = DMDASemiLagrangianGetVertexDM(dsl,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);
  
  VecGetArray(dsl->velocity,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      const PetscReal *pos = &LA_coor[2*idx];
      
      ierr = vel_evaluator((PetscReal*)pos,&LA_field[2*idx+0]);CHKERRQ(ierr);
    }
  }
  VecRestoreArray(dsl->velocity,&LA_field);
  VecRestoreArrayRead(coor,&LA_coor);
  
  /* set initial phi */
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl,&phi);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianGetSLDM(dsl,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  VecGetArrayRead(coor,&LA_coor);
  
  VecGetArray(phi,&LA_field);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscReal G_xy;
      PetscBool constrain;
      PetscInt idx = i + j * nx;
      const PetscReal *xn = &LA_coor[2*idx];
      
      ierr = dirichlet_evaluator((PetscReal*)xn,0.0,&constrain,&G_xy,NULL);CHKERRQ(ierr);
      LA_field[idx] = G_xy;
    }
  }
  VecRestoreArray(phi,&LA_field);
  
  VecRestoreArrayRead(coor,&LA_coor);
  
  /* insert intial condition into the SL implementation */
  ierr = VecCopy(phi,dsl->phi_dep);CHKERRQ(ierr);
  
  ierr = DMSLSetup(dsl);CHKERRQ(ierr);

  /* copy the consistent intial condition into my solution vector */
  ierr = VecCopy(dsl->phi_dep,phi);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"phi_0.vtr");CHKERRQ(ierr);
  ierr = VecView(phi,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  ierr = SLDMView(dsl,"dmsl_0");CHKERRQ(ierr);
  ierr = ConservativeSLView(dsl,"dmcsldomain_ic");CHKERRQ(ierr);

  for (k=1; k<120; k++) {
    char filename[128];
    Vec solution;
    PetscReal sum;
    PetscReal dt;
    
    dt = 0.2*(2.0*M_PI/99.0);
    ierr = DMDASemiLagrangianUpdate(dsl,(k-1)*dt,dt,phi);CHKERRQ(ierr);
    ierr = DMSemiLagrangianGetSolution(dsl,&solution);CHKERRQ(ierr);
    ierr = VecSum(solution,&sum);CHKERRQ(ierr);
    if (k%10 == 0) {
      PetscPrintf(PETSC_COMM_WORLD,"[step %4D] sum(phi) = %1.6e\n",k,sum);
    }
    
    if (k%2 == 0) {
      PetscSNPrintf(filename,127,"phi_%D.vtr",k);
      /*
      ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
      ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer,filename);CHKERRQ(ierr);
      ierr = VecView(phi,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      */
      
      PetscSNPrintf(filename,127,"dmsl_%D",k);
      //printf("%s \n",filename);
      ierr = SLDMView(dsl,filename);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSemiLagrangianViewDeparturePoints"
PetscErrorCode DMSemiLagrangianViewDeparturePoints(DMSemiLagrangian dsl,const char filename[])
{
  PetscErrorCode ierr;
  DeparturePoint *points;
  PetscInt npoints,p;
  FILE *fp;
  
  ierr = DMSemiLagrangianGetDeparturePoints(dsl,&npoints,&points);CHKERRQ(ierr);
  
  fp = fopen(filename,"w");
  if (!fp) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"Failed to open file %s",filename);
  
  fprintf(fp,"# Departure point coordinates\n");
  fprintf(fp,"# npoints %d\n",npoints);
  fprintf(fp,"# Legend\n");
  fprintf(fp,"#   <owner> <coorx> <coory> <location_type> <origin_type>\n");
  for (p=0; p<npoints; p++) {
    fprintf(fp,"%.6d  %+1.6e  %+1.6e  %.2d  %.2d\n",
            (int)points[p].origin,
            points[p].departure_coor[0],points[p].departure_coor[1],
            (int)points[p].type,(int)points[p].origin_type);
  }
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

/*
 Test caching of departure points
*/
#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(void)
{
  DMSemiLagrangian dsl;
  PetscReal xr[] = {0.0, 1.0};
  PetscReal yr[] = {0.0, 1.0};
  PetscViewer viewer;
  PetscErrorCode ierr;
  Vec phi,coor;
  const PetscScalar *LA_coor;
  PetscScalar *LA_field;
  PetscInt i,j,k,M,N,nx,ny;
  DM dm;
  PetscErrorCode (*inside_domain)(PetscReal*,PetscBool*,void*);
  PetscErrorCode (*dirichlet_evaluator)(PetscReal*,PetscReal,PetscBool*,PetscReal*,void*);
  PetscErrorCode (*vel_evaluator)(PetscReal*,PetscReal*);
  PetscInt model;
  
  model = 0;
  PetscOptionsGetInt(NULL,NULL,"-model",&model,NULL);
  M = 129;
  N = 129;
  PetscOptionsGetInt(NULL,NULL,"-nx",&M,NULL);
  PetscOptionsGetInt(NULL,NULL,"-ny",&N,NULL);
  
  ierr = DMDASemiLagrangianCreate(PETSC_COMM_WORLD,M,N,1,xr,yr,&dsl);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianSetType(dsl,SLTYPE_FICTDOMAIN_CONSERVATIVE_POINTWISE);CHKERRQ(ierr);
  ierr = DMSemiLagrangianSetStoreDeparturePoints(dsl,PETSC_TRUE);CHKERRQ(ierr);
  
  switch (model) {
    case 0:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
      
    case 4:
      inside_domain = triangle;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
      
    case 11:
      inside_domain = wedge;
      dirichlet_evaluator = dirichlet_evaluator_4blobs;
      vel_evaluator = vel_evaluator_liquidFlow;
      break;
      
      
    default:
      inside_domain = box;
      dirichlet_evaluator = dirichlet_evaluator_cylinder;
      vel_evaluator = vel_evaluator_45Deg;
      break;
  }
  
  ierr = DMDASemiLagrangian_FVConservative_SetDomainIdentifier(dsl,inside_domain,NULL);CHKERRQ(ierr);
  ierr = DMDASemiLagrangian_FVConservative_SetDirichletEvaluator(dsl,dirichlet_evaluator,NULL);CHKERRQ(ierr);
  
  /* set initial velocity field */
  ierr = DMDASemiLagrangianGetVertexDM(dsl,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  DMGetCoordinates(dm,&coor);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr = VecGetArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscInt idx = i + j * nx;
      const PetscReal *pos = &LA_coor[2*idx];
      
      ierr = vel_evaluator((PetscReal*)pos,&LA_field[2*idx+0]);CHKERRQ(ierr);
    }
  }
  ierr = VecRestoreArray(dsl->velocity,&LA_field);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  /* set initial phi */
  ierr = DMDASemiLagrangianCreateGlobalVector(dsl,&phi);CHKERRQ(ierr);
  ierr = DMDASemiLagrangianGetSLDM(dsl,&dm);CHKERRQ(ierr);
  ierr = DMDAGetCorners(dm,NULL,NULL,NULL,&nx,&ny,NULL);CHKERRQ(ierr);
  ierr = DMGetCoordinates(dm,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  ierr =VecGetArray(phi,&LA_field);CHKERRQ(ierr);
  for (i=0; i<nx; i++) {
    for (j=0; j<ny; j++) {
      PetscReal G_xy;
      PetscBool constrain;
      PetscInt idx = i + j * nx;
      const PetscReal *xn = &LA_coor[2*idx];
      
      ierr = dirichlet_evaluator((PetscReal*)xn,0.0,&constrain,&G_xy,NULL);CHKERRQ(ierr);
      LA_field[idx] = G_xy;
    }
  }
  ierr = VecRestoreArray(phi,&LA_field);CHKERRQ(ierr);
  
  ierr = VecRestoreArrayRead(coor,&LA_coor);CHKERRQ(ierr);
  
  /* insert intial condition into the SL implementation */
  ierr = VecCopy(phi,dsl->phi_dep);CHKERRQ(ierr);
  
  ierr = DMSLSetup(dsl);CHKERRQ(ierr);
  
  /* copy the consistent intial condition into my solution vector */
  ierr = VecCopy(dsl->phi_dep,phi);CHKERRQ(ierr);
  
  ierr = PetscViewerCreate(PETSC_COMM_WORLD,&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,"phi_0.vtr");CHKERRQ(ierr);
  ierr = VecView(phi,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  
  ierr = SLDMView(dsl,"dmsl_0");CHKERRQ(ierr);
  ierr = ConservativeSLView(dsl,"dmcsldomain_ic");CHKERRQ(ierr);
  
  for (k=1; k<40; k++) {
    char filename[128];
    Vec solution;
    PetscReal sum;
    PetscReal dt;
    
    dt = 0.05;
    ierr = DMDASemiLagrangianUpdate(dsl,(k-1)*dt,dt,phi);CHKERRQ(ierr);
    ierr = DMSemiLagrangianGetSolution(dsl,&solution);CHKERRQ(ierr);
    ierr = VecSum(solution,&sum);CHKERRQ(ierr);
    if (k%2 == 0) {
      PetscPrintf(PETSC_COMM_WORLD,"[step %4D] sum(phi) = %1.6e\n",k,sum);
    }
    
    if (k%1 == 0) {
      PetscSNPrintf(filename,127,"dmsl_%D",k);
      ierr = SLDMView(dsl,filename);CHKERRQ(ierr);
      
      PetscSNPrintf(filename,127,"dmsl_departurepoints_%D.gp",k);
      ierr = DMSemiLagrangianViewDeparturePoints(dsl,filename);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt testid = 0;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  PetscOptionsGetInt(NULL,NULL,"-test",&testid,NULL);
  if (testid == 0) {
    ierr = example1();CHKERRQ(ierr);
  } else if (testid == 1){
    ierr = example2();CHKERRQ(ierr);
  } else if (testid == 2) {
    ierr = example3();CHKERRQ(ierr);
  } else {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"-test <value> : <value> must either 0, 1, 2");
  }
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

