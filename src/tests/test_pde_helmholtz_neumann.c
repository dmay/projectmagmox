
#include <petsc.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <quadrature.h>
#include <dmbcs.h>
#include <fe_geometry_utils.h>
#include <element_container.h>
#include <inorms.h>
#include <index.h>
#include <proj_init_finalize.h>


#undef __FUNCT__
#define __FUNCT__ "FindBoundary"
PetscBool FindBoundary(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[0] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[0] > (1.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution"
PetscErrorCode mms_evaluate_solution(PetscReal coor[],PetscReal *val,void *ctx)
{
  PetscReal u,x,y;

  PetscFunctionBegin;
  x = coor[0];
  y = coor[1];
  u =  PetscCosReal(PETSC_PI*y) * PetscSinReal(PETSC_PI*x);
  *val = u;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_solution_gradient"
PetscErrorCode mms_evaluate_solution_gradient(PetscReal coor[],PetscReal grad[],void *ctx)
{
  PetscReal x,y;
  
  PetscFunctionBegin;
  x = coor[0];
  y = coor[1];
  grad[0] = PETSC_PI * PetscCosReal(PETSC_PI*x) * PetscCosReal(PETSC_PI*y); // du/dx
  grad[1] = -PETSC_PI * PetscSinReal(PETSC_PI*y) * PetscSinReal(PETSC_PI*x); // du/dy
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_flux"
PetscErrorCode mms_evaluate_flux(PetscReal coor[],PetscReal f[])
{
  PetscReal grad[2];
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = mms_evaluate_solution_gradient(coor,grad,NULL);CHKERRQ(ierr);
  f[0] = -1.0 * grad[0];
  f[1] = -1.0 * grad[1];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "mms_evaluate_rhs"
PetscErrorCode mms_evaluate_rhs(PetscReal coor[],PetscReal *val)
{
  PetscReal f;
  PetscReal x,y;
  
  PetscFunctionBegin;
  x = coor[0];
  y = coor[1];
  f =  2.0 * pow(PETSC_PI,2.0) * PetscCosReal(PETSC_PI*y) * PetscSinReal(PETSC_PI*x);
  *val = f;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "rhs_vol_quadrature"
PetscErrorCode rhs_vol_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  dm = (DM)data;
 
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);

  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[FIELD_ID(i,0,2)] = coords[2*nidx+0];
      elcoords[FIELD_ID(i,1,2)] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2],rhs;
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * elcoords[FIELD_ID(i,0,2)];
        x_qp[1] += tab_N[q][i] * elcoords[FIELD_ID(i,1,2)];
      }
      
      ierr = mms_evaluate_rhs(x_qp,&rhs);CHKERRQ(ierr);
      coeff[e*nqp + q] = rhs;
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "flux_surf_quadrature"
PetscErrorCode flux_surf_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp;
  
  PetscFunctionBegin;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);

  for (e=0; e<nfacets; e++) {
    PetscReal *q_coeff_e;
    
    FIELD_GET_OFFSET(q_coeff,e,nqp,2,q_coeff_e); /* replaces q_coeff_e = &q_coeff[e*nqp*2] */
    for (q=0; q<nqp; q++) {
      PetscReal *x_qp,flux[2];
      
      /* get x,y coords at each quadrature point */
      //x_qp = &q_coor[e*nqp*2];
      FIELD_GET_OFFSET(q_coor,e,nqp,2,x_qp);
      
      ierr = mms_evaluate_flux(x_qp,flux);CHKERRQ(ierr);
      //q_coeff[e*nqp*2 + 2*q+0] = flux[0];
      //q_coeff[e*nqp*2 + 2*q+1] = flux[1];
      
      //q_coeff_e[FIELD_ID(q,0,2)] = flux[0];
      //q_coeff_e[FIELD_ID(q,1,2)] = flux[1];
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary"
PetscErrorCode EvalDirichletBoundary(PetscScalar coor[],PetscScalar *val,void *ctx)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = mms_evaluate_solution(coor,val,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test3_snessolve"
PetscErrorCode test3_snessolve(PetscInt mxy,PetscReal *h,PetscReal *uL2,PetscReal *uH1)
{
  PetscErrorCode ierr;
  DM dm;
  PDE pde;
  SNES snes;
  Mat A;
  Vec X,Xlocal,F;
  BCList bc;
  PetscInt pk;
  const char *field[] = { "phi" };
  PetscBool view = PETSC_TRUE;

  PetscFunctionBegin;
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"# Helmholtz mesh %.3D x %.3D : element: P%D\n",mxy,mxy,pk);
  ierr = DMTetCreatePartitionedSquareGeometry(PETSC_COMM_WORLD,-1.0,1.0,-1.0,1.0, mxy,mxy, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,pk,&dm);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dm);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm,&Xlocal);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundary,NULL,&bc);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  ierr = BCListInsert(bc,X);CHKERRQ(ierr);
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  ierr = PDESetLocalSolution(pde,Xlocal);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dm,bc);CHKERRQ(ierr);
  
  {
    Coefficient g;
    
    ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(g,rhs_vol_quadrature,NULL,(void*)dm);CHKERRQ(ierr);
  }
  {
    Coefficient alpha;

    ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);
  }
  {
    Coefficient qn;
    
    ierr = PDEHelmholtzGetCoefficient_qN(pde,&qn);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(qn,flux_surf_quadrature,NULL,(void*)dm);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,A,A);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);

  {
    PetscReal E[Q_NUM_ENTRIES];
    PetscInt order,nel,nbasis;
    
    ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(dm,&nel,0,0,0,&nbasis,0);CHKERRQ(ierr);
    
    ierr = ComputeINorms(E,dm,X,
                         mms_evaluate_solution,
                         mms_evaluate_solution_gradient,NULL);CHKERRQ(ierr);
    
    *h = 2.0/((PetscReal)mxy);
    *uL2 = E[Q_L2];
    *uH1 = E[Q_H1];
  }
  
  if (view) {
    ierr = DMTetViewFields_VTU(dm,1,&X,field,"helm_");CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dm,1,&X,field,"helm_");CHKERRQ(ierr);
  }

  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&Xlocal);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt mxy,L,k,m;
  PetscReal *res_h,*u_L2,*u_H1;
  PetscBool harness_viewer = PETSC_FALSE;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  mxy = 8;
  ierr = PetscOptionsGetInt(NULL,NULL,"-mxy",&mxy,NULL);CHKERRQ(ierr);

  L = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-meshes",&L,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&res_h);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&u_L2);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&u_H1);CHKERRQ(ierr);

  m = mxy;
  for (k=0; k<L; k++) {
    PetscReal h_k,uL2_k,uH1_k;
    ierr = test3_snessolve(m,&h_k,&uL2_k,&uH1_k);CHKERRQ(ierr);
    res_h[k] = h_k;
    u_L2[k] = uL2_k;
    u_H1[k] = uH1_k;
    m = m * 2;
  }
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-harness_viewer",&harness_viewer,NULL);CHKERRQ(ierr);
  if (!harness_viewer) {
    PetscPrintf(PETSC_COMM_WORLD,"      level          h              u(L2)              u(H1)           rates\n");
    PetscPrintf(PETSC_COMM_WORLD,"------------------------------------------------------------ | ---------------------------------------\n");
    for (k=0; k<L; k++) {
      PetscPrintf(PETSC_COMM_WORLD,"[mesh %.4D] %1.4e %1.12e %1.12e |",k,res_h[k],u_L2[k],u_H1[k]);
      if (k > 0) {
        PetscReal ruL2,ruH1;
        
        ruL2 = log10(u_L2[k]/u_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
        ruH1 = log10(u_H1[k]/u_H1[k-1]) / log10(res_h[k]/res_h[k-1]);
        
        PetscPrintf(PETSC_COMM_WORLD," %+1.12e %+1.12e",ruL2,ruH1);
      }
      PetscPrintf(PETSC_COMM_WORLD,"\n");
    }
  } else {
    for (k=0; k<L; k++) {
      PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-uL2 %1.12e\n",k,u_L2[k]);
      PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-uH1 %1.12e\n",k,u_H1[k]);
      if (k > 0) {
        PetscReal ruL2,ruH1;
        
        ruL2 = log10(u_L2[k]/u_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
        ruH1 = log10(u_H1[k]/u_H1[k-1]) / log10(res_h[k]/res_h[k-1]);
        PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-rate-uL2 %1.12e\n",k,ruL2);
        PetscPrintf(PETSC_COMM_WORLD,"mesh-%.4D-rate-uH1 %1.12e\n",k,ruH1);
      }
    }
  }
  
  ierr = PetscFree(res_h);CHKERRQ(ierr);
  ierr = PetscFree(u_L2);CHKERRQ(ierr);
  ierr = PetscFree(u_H1);CHKERRQ(ierr);
  
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

