
#include <petsc.h>
#include <petscdmtet.h>
#include <dmtet_geom_utils.h>
#include <dmbcs.h>
#include <quadrature.h>
#include <proj_init_finalize.h>


#undef __FUNCT__
#define __FUNCT__ "example1_RT"
PetscErrorCode example1_RT(void)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  PetscInt mx,my,nelements,basisperelement,nodesperelement,nnodes,e;
  PetscInt *elementdof;
  PetscReal *nodecoor;
  Mat A;
  
  PetscFunctionBegin;
  mx = 1;
  my = 1;
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.0,1.0,-3.0,0.0,mx,my,PETSC_TRUE,
                                     2,DMTET_RAVIART_THOMAS,0,&dm);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"span x: %+1.4e %+1.4e\n",gmin[0],gmax[0]);
  PetscPrintf(PETSC_COMM_WORLD,"span y: %+1.4e %+1.4e\n",gmin[1],gmax[1]);
  
  ierr = DMTetSpaceElement(dm,&nelements,&basisperelement,&elementdof,&nodesperelement,&nnodes,&nodecoor);CHKERRQ(ierr);

  //PetscPrint(PETSC_COMM_SELF,"#unique edge DOFS %D\n",basisperelement);
  for (e=0; e<nelements; e++) {
    PetscPrintf(PETSC_COMM_SELF,"e[%D] {%D , %D , %D}\n",e,elementdof[basisperelement*e+0],elementdof[basisperelement*e+1],elementdof[basisperelement*e+2]);
  }

  /* check the non-zero structure */
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleBilinearForm_MassMatrix"
PetscErrorCode AssembleBilinearForm_MassMatrix(Mat A,DM dm)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,nvert,nbasis,nb,i,j;
  PetscInt  *element,*gelement;
  PetscReal   *elcoords;
  PetscInt    *eldofs;
  PetscReal   *vcoors,*Me,detJ;
  PetscInt q,nqp;
  PetscReal *w,*xi,**tab_N,**tab_Nref,sum = 0.0;
  
  
  PetscFunctionBegin;
  ierr = MatZeroEntries(A);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,NULL,NULL,&gelement,&nvert,NULL,&vcoors);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetQuadratureRescaleToUnitTriangle(dm,nqp,w,xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_Nref);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nvert,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&eldofs);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*nbasis,&Me);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    PetscInt *gelnidx;
    
    /* get element -> dof map */
    //printf("e %d : dofid {%d %d %d}\n",e,element[3*e+0],element[3*e+1],element[3*e+2]);
    ierr = DMTetGetBasisDOF_RT(dm,e,eldofs);CHKERRQ(ierr);
    //printf("e %d : dofid* {%d %d %d}\n",e,eldofs[0],eldofs[1],eldofs[2]);
    /* get geometry element -> node map */
    gelnidx = &gelement[nvert*e];

    /* get element coordinates */
    for (i=0; i<nvert; i++) {
      PetscInt nidx = gelnidx[i];
      elcoords[2*i  ] = vcoors[2*nidx  ];
      elcoords[2*i+1] = vcoors[2*nidx+1];
    }
    
    /* compute coordinate transformation */
    ierr = PiolaTransformation_EvaluateAbsDetJ(elcoords,&detJ);
    
    ierr = PetscMemzero(Me,sizeof(PetscReal)*nbasis*nbasis);CHKERRQ(ierr);
    
    /* transform reference N via J */
    for (q=0; q<nqp; q++) {
      ierr = EvaluateBasis_RT0(dm,e,elcoords,tab_Nref[q],tab_N[q]);
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Nq;
      
      /* get access to tabulated quadrature points */
      Nq = tab_N[q];
      
      fac = detJ * w[q];
      
      for (i=0; i<nbasis; i++) {
        PetscReal *Ni = &Nq[2*i];
        
        for (j=0; j<nbasis; j++) {
          PetscReal *Nj = &Nq[2*j];
          
          Me[ i*nbasis + j ] += fac * ( Ni[0]*Nj[0] + Ni[1]*Nj[1] );
        }
      }
      sum += 1.0 *fac;
    } // quadrature
    for (i=0; i<nbasis; i++) {
      for (j=0; j<nbasis; j++) {
        if (PetscAbsReal(Me[ i*nbasis + j ]) < 1.0e-12) Me[ i*nbasis + j ] = 0.0;
      }
    }
    
    //printf("e:%.2d Me = [ %+1.4e %+1.4e %+1.4e \n",e,Me[0*nbasis+0],Me[0*nbasis+1],Me[0*nbasis+2]);
    //printf("            %+1.4e %+1.4e %+1.4e \n",Me[1*nbasis+0],Me[1*nbasis+1],Me[1*nbasis+2]);
    //printf("            %+1.4e %+1.4e %+1.4e ]\n\n",Me[2*nbasis+0],Me[2*nbasis+1],Me[2*nbasis+2]);
    
    ierr = MatSetValues(A,nbasis,eldofs,nbasis,eldofs,Me,ADD_VALUES);CHKERRQ(ierr);
  }
  //printf("fac %1.4e\n",sum);
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  PetscFree(w);
  PetscFree(xi);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Me);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    PetscFree(tab_Nref[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_Nref);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleLinearForm_SC"
PetscErrorCode AssembleLinearForm_SC(Vec F,DM dm)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,nvert,nbasis,nb,i;
  PetscInt  *element,*gelement;
  PetscReal   *elcoords;
  PetscInt    *eldofs;
  PetscReal   *vcoors,*Fe,detJ;
  PetscInt q,nqp;
  PetscReal *w,*xi,**tab_N,**tab_Nref;
  PetscInt order,np1d;
  
  PetscFunctionBegin;
  ierr = VecZeroEntries(F);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,NULL,NULL,&gelement,&nvert,NULL,&vcoors);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  
  //ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  np1d = order + 4; /* seemingly its hard to accurately integrate x.sin(x).cos(y) */
  ierr = _PetscDTGaussJacobiQuadrature(2,np1d,-1.0,1.0,&xi,&w);CHKERRQ(ierr);
  nqp = np1d * np1d;
  
  ierr = DMTetQuadratureRescaleToUnitTriangle(dm,nqp,w,xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_Nref);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nvert,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&eldofs);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&Fe);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    PetscInt *gelnidx;
    
    /* get element -> dof map */
    ierr = DMTetGetBasisDOF_RT(dm,e,eldofs);CHKERRQ(ierr);
    
    /* get geometry element -> node map */
    gelnidx = &gelement[nvert*e];
    
    /* get element coordinates */
    for (i=0; i<nvert; i++) {
      PetscInt nidx = gelnidx[i];
      
      elcoords[2*i  ] = vcoors[2*nidx  ];
      elcoords[2*i+1] = vcoors[2*nidx+1];
    }
    
    /* compute coordinate transformation */
    ierr = PiolaTransformation_EvaluateAbsDetJ(elcoords,&detJ);
    
    ierr = PetscMemzero(Fe,sizeof(PetscReal)*nbasis);CHKERRQ(ierr);
    
    /* transform reference N via J */
    for (q=0; q<nqp; q++) {
      ierr = EvaluateBasis_RT0(dm,e,elcoords,tab_Nref[q],tab_N[q]);
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal fac;
      PetscReal *Nq;
      PetscReal xq[2],value[] = {0.0 , 0.0};
      
      /* get access to tabulated quadrature points */
      Nq = tab_N[q];
      
      fac = detJ * w[q];

      /* evaluate function at the quadrature point */
      xq[0] = elcoords[2*0] + (elcoords[2*1]  - elcoords[2*0])*xi[2*q+0] + (elcoords[2*2] - elcoords[2*0])*xi[2*q+1];
      xq[1] = elcoords[2*0+1] + (elcoords[2*1+1]  - elcoords[2*0+1])*xi[2*q+0] + (elcoords[2*2+1] - elcoords[2*0+1])*xi[2*q+1];
      
      value[0] =  sin(M_PI * xq[0]) * cos(M_PI * xq[1]); // du/dx = pi.cos(pi.x).cos(pi.y)
      value[1] = -cos(M_PI * xq[0]) * sin(M_PI * xq[1]); // dv/dy = -pi.cos(pi.x).cos(pi.y)

      //value[0] = xq[0]*xq[1] - xq[1]*xq[1];
      //value[1] = xq[0] + xq[0]*xq[0] - 0.5*xq[1]*xq[1];
      
      for (i=0; i<nbasis; i++) {
        PetscReal *Ni = &Nq[2*i];
        
        Fe[i] += fac * ( Ni[0]*value[0] + Ni[1]*value[1] );
      }
    } // quadrature
    
    ierr = VecSetValues(F,nbasis,eldofs,Fe,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = VecAssemblyBegin(F);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(F);CHKERRQ(ierr);
  
  PetscFree(w);
  PetscFree(xi);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Fe);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    PetscFree(tab_Nref[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_Nref);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetView_RT"
PetscErrorCode DMTetView_RT(DM dm,Vec X,const char filename[])
{
  PetscErrorCode ierr;
  PetscInt   e,nel,nvert,nbasis,nb,i;
  PetscInt   *element,*gelement;
  PetscReal  *elcoords;
  PetscInt   *eldofs;
  PetscReal  *vcoors,*Xe,int_divu = 0.0,detJA;
  const PetscReal *LA_X;
  PetscInt   q,nqp;
  PetscReal  *w,*xi,**tab_N,**tab_Nref;
  PetscReal  **tab_dNx_ref,**tab_dNy_ref,**tab_dNx,**tab_dNy;
  FILE       *fp;
  
  PetscFunctionBegin;
  
  fp = fopen(filename,"w");
  if (!fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file %s",filename);

  ierr = DMTetGeometryElement(dm,NULL,NULL,&gelement,&nvert,NULL,&vcoors);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetQuadratureRescaleToUnitTriangle(dm,nqp,w,xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_Nref);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNx_ref,&tab_dNy_ref,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNx,&tab_dNy,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nvert,&elcoords);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&eldofs);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis,&Xe);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(X,&LA_X);CHKERRQ(ierr);
  
  for (e=0; e<nel; e++) {
    PetscInt *gelnidx;
    
    /* get element -> dof map */
    ierr = DMTetGetBasisDOF_RT(dm,e,eldofs);CHKERRQ(ierr);
    
    /* get element dofs */
    for (i=0; i<nbasis; i++) {
      Xe[i] = LA_X[ eldofs[i] ];
    }
    
    /* get geometry element -> node map */
    gelnidx = &gelement[nvert*e];
    
    /* get element coordinates */
    for (i=0; i<nvert; i++) {
      PetscInt nidx = gelnidx[i];
      
      elcoords[2*i  ] = vcoors[2*nidx  ];
      elcoords[2*i+1] = vcoors[2*nidx+1];
    }
    
    /* transform reference N via J */
    for (q=0; q<nqp; q++) {
      ierr = EvaluateBasis_RT0(dm,e,elcoords,tab_Nref[q],tab_N[q]);
      ierr = EvaluateBasisDerivative_RT0(dm,e,elcoords,tab_dNx_ref[q],tab_dNx[q],tab_dNy_ref[q],tab_dNy[q]);
    }
    
    ierr = PiolaTransformation_EvaluateAbsDetJ(elcoords,&detJA);

    for (q=0; q<nqp; q++) {
      PetscReal *Nq,*dNxq,*dNyq;
      PetscReal xq[2],div = 0.0,value[] = {0.0 , 0.0};
      
      /* get access to tabulated quadrature points */
      Nq = tab_N[q];
      dNxq = tab_dNx[q];
      dNyq = tab_dNy[q];
      
      /* evaluate coordinate at the quadrature point */
      xq[0] = elcoords[2*0] + (elcoords[2*1]  - elcoords[2*0])*xi[2*q+0] + (elcoords[2*2] - elcoords[2*0])*xi[2*q+1];
      xq[1] = elcoords[2*0+1] + (elcoords[2*1+1]  - elcoords[2*0+1])*xi[2*q+0] + (elcoords[2*2+1] - elcoords[2*0+1])*xi[2*q+1];
      
      /* approximate function at the quadrature point */
      for (i=0; i<nbasis; i++) {
        PetscReal *Ni   = &Nq[2*i];
        PetscReal *dNix = &dNxq[2*i];
        PetscReal *dNiy = &dNyq[2*i];
        
        value[0] += Ni[0]*Xe[i];
        value[1] += Ni[1]*Xe[i];
        
        div += (dNix[0] + dNiy[1]) * Xe[i];
      }
      //int_divu += w[q] * div * detJA;
      
//      if (e==0)
      {
        PetscReal phi[2],grad[2];
        
        ierr = EvaluateSolution_RT0(dm,e,elcoords,tab_Nref[q],Xe,phi);CHKERRQ(ierr);
        ierr = EvaluateSolutionGradient_RT0(dm,e,elcoords,tab_dNx_ref[q],tab_dNy_ref[q],Xe,grad);CHKERRQ(ierr);
        //           x      y    u     [4]v   [5]u v [7] div [8] div
        fprintf(fp,"%1.4e %1.4e %1.4e %1.4e %1.4e %1.4e %1.4e %1.4e\n",xq[0],xq[1],value[0],value[1],phi[0],phi[1],div,grad[0]+grad[1]);
        
        //printf("q %d | %1.4e %1.4e \n",q,grad[0],grad[1]);
        int_divu += w[q] * (grad[0] + grad[1]) * detJA;

      }
      
    } // quadrature
    
    // cell wise integral of u.n dS
    //
    {
      PetscInt sign[3];
      //PetscReal *v0,*v1,*v2,edgel[3];
      PetscReal surfint = 0.0;
      
      //v0 = &elcoords[2*0];
      //v1 = &elcoords[2*1];
      //v2 = &elcoords[2*2];
      GetBasisSign_RT0(&gelement[nvert*e],sign);
      //edgel[0] = PetscSqrtReal( (v1[0]-v2[0])*(v1[0]-v2[0]) + (v1[1]-v2[1])*(v1[1]-v2[1])  );
      //edgel[1] = PetscSqrtReal( (v0[0]-v2[0])*(v0[0]-v2[0]) + (v0[1]-v2[1])*(v0[1]-v2[1])  );
      //edgel[2] = PetscSqrtReal( (v1[0]-v0[0])*(v1[0]-v0[0]) + (v1[1]-v0[1])*(v1[1]-v0[1])  );
      
      surfint = surfint + Xe[0];
      surfint = surfint + Xe[1];
      surfint = surfint + Xe[2];
      //printf("[e %d] surf int = %1.4e\n",e,surfint);
    }
    //
    
/*
    // cell wise integral of u.n dS
     {
       PetscInt sign[3];
       PetscReal *v0,*v1,*v2,edgel[3];
       PetscReal surfint = 0.0;
       
       PetscInt _nb,_nqp = 3;
       PetscReal _xi[] = { 0.5,0.5 , 0.0,0.5, 0.5,0.0 };
       PetscReal **_tab_Nref,**_tab_N;
       
       v0 = &elcoords[2*0];
       v1 = &elcoords[2*1];
       v2 = &elcoords[2*2];
       edgel[0] = PetscSqrtReal( (v1[0]-v2[0])*(v1[0]-v2[0]) + (v1[1]-v2[1])*(v1[1]-v2[1])  );
       edgel[1] = PetscSqrtReal( (v0[0]-v2[0])*(v0[0]-v2[0]) + (v0[1]-v2[1])*(v0[1]-v2[1])  );
       edgel[2] = PetscSqrtReal( (v1[0]-v0[0])*(v1[0]-v0[0]) + (v1[1]-v0[1])*(v1[1]-v0[1])  );

       ierr = DMTetTabulateBasis(dm,_nqp,xi,&_nb,&_tab_Nref);CHKERRQ(ierr);
       ierr = DMTetTabulateBasis(dm,_nqp,xi,&_nb,&_tab_N);CHKERRQ(ierr);

       for (_q=0; _q<_nqp; _q++) {
         ierr = EvaluateBasis_RT0(dm,e,elcoords,_tab_Nref[_q],_tab_N[_q]);
       
         ierr = EvaluateSolution_RT0(dm,e,elcoords,_tab_Nref[_q],Xe,phi);CHKERRQ(ierr);

         surfint = surfint + Xe[0] * edgel[0] * sign[0];
       }
       printf("[e %d] surf int = %1.4e\n",e,surfint);
     }
*/
    
  }
  ierr = VecRestoreArrayRead(X,&LA_X);CHKERRQ(ierr);
  fclose(fp);
  
  printf("\\int div(u) = %1.10e\n",int_divu);
  
  PetscFree(w);
  PetscFree(xi);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Xe);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    PetscFree(tab_Nref[q]);
    PetscFree(tab_dNx[q]);
    PetscFree(tab_dNx_ref[q]);
    PetscFree(tab_dNy[q]);
    PetscFree(tab_dNy_ref[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_Nref);
  PetscFree(tab_dNx);
  PetscFree(tab_dNx_ref);
  PetscFree(tab_dNy);
  PetscFree(tab_dNy_ref);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_projectintRT"
PetscErrorCode example_projectintRT(void)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  PetscInt mx,my;
  Mat A;
  Vec x,F;
  KSP ksp;
  
  PetscFunctionBegin;
  mx = 4;
  my = 4;
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.0,1.0,0.0,1.0,mx,my,PETSC_TRUE,
                                     2,DMTET_RAVIART_THOMAS,0,&dm);CHKERRQ(ierr);
  
  /*
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/solcx/mesh-r4/solcx.1",2,DMTET_RAVIART_THOMAS,0,&dm);CHKERRQ(ierr);
  */
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(dm,&A);CHKERRQ(ierr);
  ierr = AssembleBilinearForm_MassMatrix(A,dm);CHKERRQ(ierr);
  
  ierr = MatCreateVecs(A,&x,&F);CHKERRQ(ierr);
  ierr = AssembleLinearForm_SC(F,dm);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,F,x);CHKERRQ(ierr);
  ierr = DMTetView_RT(dm,x,"proj.gp");CHKERRQ(ierr);
  
  
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscInt tidx = 2;
  PetscErrorCode ierr;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-tidx",&tidx,NULL);CHKERRQ(ierr);
  switch (tidx) {
    case 1:
      ierr = example1_RT();CHKERRQ(ierr);
      break;
      
    case 2:
      ierr = example_projectintRT();CHKERRQ(ierr);
      break;
      
    default:
      ierr = example1_RT();CHKERRQ(ierr);
      break;
  }
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

