
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <petscutils.h>
#include <pde.h>
#include <pdedarcy.h>
#include <dmbcs.h>
#include <fe_geometry_utils.h>
#include <inorms.h>
#include <element_container.h>
#include <index.h>


/* private prototypes */
PetscErrorCode PDEDarcyFieldView_AsciiVTU(PDE pde,const char prefix[]);
PetscErrorCode StokesUP_CG_AsciiVTU_g(DM dm,Vec field,const char filename[]);

typedef struct {
  DM dm;
  DM dmv,dmp;
  PDE pde;
} MMSCtx;


#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateV"
PetscErrorCode DarcySolution_EvaluateV(PetscReal x[],PetscReal uv[],void *ctx)
{
  uv[0] =  2.0*M_PI * cos(2.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  uv[1] = -3.0*M_PI * sin(2.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateP"
PetscErrorCode DarcySolution_EvaluateP(PetscReal x[],PetscReal p[],void *ctx)
{
  p[0] = -sin(2.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateInvK"
PetscErrorCode DarcySolution_EvaluateInvK(PetscReal x[],PetscReal ik[],void *ctx)
{
  ik[0] = 1.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateGradientV"
PetscErrorCode DarcySolution_EvaluateGradientV(PetscReal x[],PetscReal grad[],void *ctx)
{
  grad[0] = 0.0;
  grad[1] = 0.0;
  grad[2] = 0.0;
  grad[3] = 0.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateGradientP"
PetscErrorCode DarcySolution_EvaluateGradientP(PetscReal x[],PetscReal grad[],void *ctx)
{
  grad[0] = -2.0*M_PI * cos(2.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  grad[1] =  3.0*M_PI * sin(2.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateF"
PetscErrorCode DarcySolution_EvaluateF(PetscReal x[],PetscReal Fu[],void *ctx)
{
  Fu[0] = 0.0;
  Fu[1] = 0.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcySolution_EvaluateG"
PetscErrorCode DarcySolution_EvaluateG(PetscReal x[],PetscReal Fp[],void *ctx)
{
  Fp[0] = -(-4.0*M_PI*M_PI * sin(2.0*M_PI*x[0])*cos(3.0*M_PI*x[1])  - 9.0*M_PI*M_PI * sin(2.0*M_PI*x[0])*cos(3.0*M_PI*x[1]));
  
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "a_evaluate_quadrature"
PetscErrorCode a_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff,a;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dm;
  MMSCtx *ctx;
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type MMSCtx* as last argument");
  ctx = (MMSCtx*)data;
  dm = ctx->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"a",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);

  ierr = DMTetGeometryElement(dm,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dm,quadrature,&ele_container_g);CHKERRQ(ierr);

  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      ierr = DarcySolution_EvaluateInvK(x_qp,&a,NULL);CHKERRQ(ierr);
      coeff[e*nqp + q] = a;
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "f_evaluate_quadrature"
PetscErrorCode f_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dm;
  MMSCtx *ctx;
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type MMSCtx* as last argument");
  ctx = (MMSCtx*)data;
  dm = ctx->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"f",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dm,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }

      coeff[2*(e*nqp + q)+0] = 0.0;
      coeff[2*(e*nqp + q)+1] = 0.0;
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "g_evaluate_quadrature"
PetscErrorCode g_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e,i,nqp;
  PetscReal *coeff,g;
  PetscReal *coords_g,*elcoords;
  PetscInt *element_g,nen,npe;
  EContainer ele_container_g;
  DM dm;
  MMSCtx *ctx;
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type MMSCtx* as last argument");
  ctx = (MMSCtx*)data;
  dm = ctx->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"g",&nen,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,&nen,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dm,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords_g[2*nidx+0];
      elcoords[2*i+1] = coords_g[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe; i++) {
        x_qp[0] += ele_container_g->N[q][i] * elcoords[2*i+0];
        x_qp[1] += ele_container_g->N[q][i] * elcoords[2*i+1];
      }
      ierr = DarcySolution_EvaluateG(x_qp,&g,NULL);CHKERRQ(ierr);
      coeff[e*nqp + q] = g;
    }
  }
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  
  /*ierr = project_g(ctx->pde,ctx->dmp);CHKERRQ(ierr);*/
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_UseMMS_V"
PetscErrorCode EvalDirichletBoundary_UseMMS_V(PetscScalar coor[],PetscInt dof,
                                            PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscErrorCode ierr;
  PetscReal velocity[2];
  const PetscReal coor_eps = 1.0e-10;
  
  *constrain = PETSC_FALSE;
  *val = 0.0;
  
  ierr = DarcySolution_EvaluateV(coor,velocity,NULL);CHKERRQ(ierr);
  
  if ( PetscAbsReal(coor[0]) < coor_eps) { // left
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[0] - 1.0) < coor_eps) { // right
    *constrain = PETSC_TRUE;
  }

  if ( PetscAbsReal(coor[1]) < coor_eps) { // bottom
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[1] - 1.0) < coor_eps) { // top
    *constrain = PETSC_TRUE;
  }

  if (*constrain) {
    //printf("%1.4e %1.4e %1.4e %1.4e\n",coor[0],coor[1],velocity[0],velocity[1]);
    *val = velocity[dof];
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_UseMMS_Vdotn"
PetscErrorCode EvalDirichletBoundary_UseMMS_Vdotn(PetscScalar coor[],PetscInt dof,
                                              PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscErrorCode ierr;
  PetscReal velocity[2];
  const PetscReal coor_eps = 1.0e-10;
  
  *constrain = PETSC_FALSE;
  *val = 0.0;
  
  ierr = DarcySolution_EvaluateV(coor,velocity,NULL);CHKERRQ(ierr);
  
  if (dof == 0) {
    if ( PetscAbsReal(coor[0]) < coor_eps) { // left
      *constrain = PETSC_TRUE;
    }
    if ( PetscAbsReal(coor[0] - 1.0) < coor_eps) { // right
      *constrain = PETSC_TRUE;
    }
  }
  if (dof == 1) {
    if ( PetscAbsReal(coor[1]) < coor_eps) { // bottom
      *constrain = PETSC_TRUE;
    }
    if ( PetscAbsReal(coor[1] - 1.0) < coor_eps) { // top
      *constrain = PETSC_TRUE;
    }
  }
  
  if (*constrain) {
    //printf("%1.4e %1.4e %1.4e %1.4e\n",coor[0],coor[1],velocity[0],velocity[1]);
    *val = velocity[dof];
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary_UseMMS_P"
PetscErrorCode EvalDirichletBoundary_UseMMS_P(PetscScalar coor[],PetscInt dof,
                                            PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscErrorCode ierr;
  PetscReal pressure;
  const PetscReal coor_eps = 1.0e-10;
  
  *constrain = PETSC_FALSE;
  *val = 0.0;
  
  ierr = DarcySolution_EvaluateP(coor,&pressure,NULL);CHKERRQ(ierr);
  
  if ( PetscAbsReal(coor[0]) < coor_eps) { // left
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[0] - 1.0) < coor_eps) { // right
    *constrain = PETSC_TRUE;
  }
  
  if ( PetscAbsReal(coor[1]) < coor_eps) { // bottom
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[1] - 1.0) < coor_eps) { // top
    *constrain = PETSC_TRUE;
  }
  
  if (*constrain) {
    *val = pressure;
  }
  
  PetscFunctionReturn(0);
}

/*
  Sets p = \hat{P} on left/right/top/bottoms boundaries
*/
/*
 [Test]
 
 ./arch-gnu-c-opt/bin/test_pde_darcy_mms.app -mxy 12 -dcy_ksp_type fgmres  -dcy_ksp_rtol 1.0e-10 -dcy_ksp_converged_reason -dcy_fieldsplit_u_pc_type lu -dcy_ksp_type fgmres -dcy_fieldsplit_p_ksp_type fgmres  -dcy_fieldsplit_p_ksp_max_it 30 -dcy_fieldsplit_p_ksp_rtol 1.0e-2 -dcy_fieldsplit_p_pc_type lu -meshes 4 -test_id 1
 
 # Darcy mesh 012 x 012 : element: P2-P1
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 9
 # degree  01 8.3333e-02 | hdiv 9.240640277664e+01 | hdiv_Linf 3.923181885097e+02
 # Darcy mesh 024 x 024 : element: P2-P1
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 14
 # degree  01 4.1667e-02 | hdiv 7.939012758081e+01 | hdiv_Linf 4.829337861807e+02
 # Darcy mesh 048 x 048 : element: P2-P1
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 22
 # degree  01 2.0833e-02 | hdiv 7.040327601266e+01 | hdiv_Linf 5.111218945501e+02
 # Darcy mesh 096 x 096 : element: P2-P1
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 48
 # degree  01 1.0417e-02 | hdiv 6.663825046568e+01 | hdiv_Linf 5.187111752994e+02
 level          h              u(L2)              p(L2)              p(H1)            rates
 ------------------------------------------------------------------------------- | -----------------------------------------------------------
 [mesh 0000] 8.3333e-02 1.005313726339e+00 2.998961413072e-02 1.851060461712e+00 |
 [mesh 0001] 4.1667e-02 3.231576826708e-01 5.855736381667e-03 9.289257783159e-01 | +1.637335595350e+00 +2.356540447740e+00 +9.947167849417e-01
 [mesh 0002] 2.0833e-02 1.026610947936e-01 1.319035118135e-03 4.622910291432e-01 | +1.654348739435e+00 +2.150367631087e+00 +1.006761962042e+00
 [mesh 0003] 1.0417e-02 3.358576882465e-02 3.153931386755e-04 2.306525750490e-01 | +1.611967591178e+00 +2.064259795576e+00 +1.003079968024e+00
*/
#undef __FUNCT__
#define __FUNCT__ "pde_darcyL2H1_mms"
PetscErrorCode pde_darcyL2H1_mms(PetscInt mxy,PetscBool v_dg,PetscReal *h,PetscReal *uL2,PetscReal *pL2,PetscReal *pH1)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcV,bcP;
  SNES snes;
  PetscBool view = PETSC_FALSE;
  PetscInt pk,pku,pkp;
  MMSCtx ctx;
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  pku = pk + 1;
  pkp = pk;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pku",&pku,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-pkp",&pkp,NULL);CHKERRQ(ierr);

  if (v_dg) {
    PetscPrintf(PETSC_COMM_WORLD,"# Darcy mesh %.3D x %.3D : element: P%D^{disc}-P%D  [L2-H1]\n",mxy,mxy,pku,pkp);
    
    ierr = DMTetCreatePartitionedSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_DG,pku,&dmV);CHKERRQ(ierr);
  } else {
    PetscPrintf(PETSC_COMM_WORLD,"# Darcy mesh %.3D x %.3D : element: P%D-P%D  [L2-H1]\n",mxy,mxy,pku,pkp);
    
    ierr = DMTetCreatePartitionedSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pku,&dmV);CHKERRQ(ierr);
  }

  ierr = DMTetCreateSharedGeometry(dmV,1,DMTET_CWARP_AND_BLEND,pkp,&dmP);CHKERRQ(ierr);
  ctx.dm = dmV;

  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_UseMMS_V,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_UseMMS_V,NULL);CHKERRQ(ierr);

  ierr = DMBCListCreate(dmP,&bcP);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundary_UseMMS_P,NULL);CHKERRQ(ierr);

  /* pde for darcy */
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,dmV,dmP,NULL,bcP,DARCY_BFORM_L2H1,&pde);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde,"dcy_");CHKERRQ(ierr);

  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);

  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    //ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  {
    Coefficient a,f,g;
    
    ierr = PDEDarcyGetVCoefficients(pde,&a,&f,&g);CHKERRQ(ierr);
    
    /* inverse permability */
    ierr = CoefficientSetComputeQuadrature(a,a_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);

    if (view) {
      ierr = CoefficientEvaluate(a);CHKERRQ(ierr);
      ierr = CoefficientView_GP(a,dmP,"a-qp");CHKERRQ(ierr);
    }

    /* u-momentum right hand side */
    ierr = CoefficientSetComputeQuadrature(f,f_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    
    if (view) {
      ierr = CoefficientEvaluate(f);CHKERRQ(ierr);
      ierr = CoefficientView_GP(f,dmP,"f-qp");CHKERRQ(ierr);
    }

    /* p-continuity right hand side */
    ierr = CoefficientSetComputeQuadrature(g,g_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
  }
  
  /* stokes solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);

  {
    Vec Xv,Xp;
    PetscReal Eu[Q_NUM_ENTRIES];
    PetscReal Ep[Q_NUM_ENTRIES],p_avg;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Eu,dmV,Xv,
                         DarcySolution_EvaluateV,
                         NULL,NULL);CHKERRQ(ierr);

    ierr = IntegrateField(&p_avg,dmP,Xp);CHKERRQ(ierr);
    
    ierr = VecShift(Xp,-p_avg);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Ep,dmP,Xp,
                         DarcySolution_EvaluateP,
                         DarcySolution_EvaluateGradientP,NULL);CHKERRQ(ierr);

    {
      PetscReal Enorms[5];
      
      ierr = IntegrateFieldHdiv(Enorms,dmV,Xv);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"# degree  %.2D %1.4e | hdiv %1.12e | hdiv_Linf %1.12e\n",pk,1.0/((PetscReal)mxy),Enorms[3],Enorms[4]);
      
    }
    
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    //PetscPrintf(PETSC_COMM_WORLD,"degree  %.2D %1.4e  %1.12e %1.12e %1.12e\n",pk,1.0/((PetscReal)mxy),Eu[Q_L2],Eu[Q_H1],Ep[Q_L2]);

    *h = 1.0/((PetscReal)mxy);
    *uL2 = Eu[Q_L2];
    *pL2 = Ep[Q_L2];
    *pH1 = Ep[Q_H1];
  }
  
  if (view) {
    ierr = PDEDarcyFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
  
    {
      DM dmVp;
      Vec Xv,Xp,vs = NULL;
      const char *fields[] = { "pvs" };
      char       prefix[PETSC_MAX_PATH_LEN];
      
      ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                         2,DMTET_CWARP_AND_BLEND,pk,&dmVp);CHKERRQ(ierr);
  
      ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
      ierr = DMTetProjectField(dmV,Xv,PRJTYPE_NATIVE,dmVp,&vs);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);

      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s_",fields[0]);CHKERRQ(ierr);
      ierr = DMTetViewFields_VTU(dmVp,1,&vs,fields,prefix);CHKERRQ(ierr);
      
      ierr = DMDestroy(&dmVp);CHKERRQ(ierr);
      ierr = VecDestroy(&vs);CHKERRQ(ierr);
    }
  }
  
  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundaryLR_UseMMS_P"
PetscErrorCode EvalDirichletBoundaryLR_UseMMS_P(PetscScalar coor[],PetscInt dof,
                                              PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscErrorCode ierr;
  PetscReal pressure;
  const PetscReal coor_eps = 1.0e-10;
  
  *constrain = PETSC_FALSE;
  *val = 0.0;
  
  ierr = DarcySolution_EvaluateP(coor,&pressure,NULL);CHKERRQ(ierr);
  
  if ( PetscAbsReal(coor[0]) < coor_eps) { // left
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[0] - 1.0) < coor_eps) { // right
    *constrain = PETSC_TRUE;
  }
  
  
  if (*constrain) {
    *val = pressure;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "u_neumann_evaluate_quadrature"
PetscErrorCode u_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp;
  //MMSCtx *ctx = (MMSCtx*)data;
  
  PetscFunctionBegin;
  //dm = ctx->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"uN",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  for (e=0; e<nfacets; e++) {
    PetscReal *q_coeff_e;
    
    FIELD_GET_OFFSET(q_coeff,e,nqp,2,q_coeff_e); /* replaces q_coeff_e = &q_coeff[e*nqp*2] */
    for (q=0; q<nqp; q++) {
      PetscReal *x_qp,flux[2];
      
      /* get x,y coords at each quadrature point */
      //x_qp = &q_coor[e*nqp*2];
      FIELD_GET_OFFSET(q_coor,e,nqp,2,x_qp);
      
      ierr = DarcySolution_EvaluateV(x_qp,flux,NULL);
      //q_coeff[e*nqp*2 + 2*q+0] = flux[0];
      //q_coeff[e*nqp*2 + 2*q+1] = flux[1];
      
      //q_coeff_e[FIELD_ID(q,0,2)] = flux[0];
      //q_coeff_e[FIELD_ID(q,1,2)] = flux[1];
      FIELD_SET_VALS2(flux,q_coeff_e,q);
    }
  }
  
  PetscFunctionReturn(0);
}

/*
  Sets p = \hat{P}     on left/right boundaries
  Sets u.n = \hat{U}.n on top/bottom boundaries
*/
/*
 [Test]
 
 ./arch-gnu-c-opt/bin/test_pde_darcy_mms.app -dcy_ksp_type fgmres  -dcy_ksp_rtol 1.0e-10 -dcy_ksp_converged_reason -dcy_fieldsplit_u_pc_type lu -dcy_ksp_type fgmres -dcy_fieldsplit_p_ksp_type fgmres  -dcy_fieldsplit_p_ksp_max_it 30 -dcy_fieldsplit_p_ksp_rtol 1.0e-2 -dcy_fieldsplit_p_pc_type lu -meshes 3  -test_id 3 -mxy 12
 
 # Darcy mesh 012 x 012 : element: P2-P1 [L2-H1]
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 9
 # degree  01 8.3333e-02 | hdiv 9.248431395491e+01 | hdiv_Linf 3.918995403127e+02
 # Darcy mesh 024 x 024 : element: P2-P1 [L2-H1]
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 14
 # degree  01 4.1667e-02 | hdiv 7.938971282321e+01 | hdiv_Linf 4.830198337135e+02
 # Darcy mesh 048 x 048 : element: P2-P1 [L2-H1]
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 21
 # degree  01 2.0833e-02 | hdiv 7.040253921663e+01 | hdiv_Linf 5.111144964886e+02
 level          h              u(L2)              p(L2)              p(H1)            rates
 ------------------------------------------------------------------------------- | -----------------------------------------------------------
 [mesh 0000] 8.3333e-02 1.002585772218e+00 2.752810791156e-02 1.853016652563e+00 |
 [mesh 0001] 4.1667e-02 3.226656134936e-01 5.283202553279e-03 9.292773194761e-01 | +1.635613922991e+00 +2.381420824930e+00 +9.956947444613e-01
 [mesh 0002] 2.0833e-02 1.025803386347e-01 1.208095171794e-03 4.623399083448e-01 | +1.653285598852e+00 +2.128678610258e+00 +1.007155298186e+00
*/
#undef __FUNCT__
#define __FUNCT__ "pde_darcyL2H1_mixedbc_mms"
PetscErrorCode pde_darcyL2H1_mixedbc_mms(PetscInt mxy,PetscReal *h,PetscReal *uL2,PetscReal *pL2,PetscReal *pH1)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcV,bcP;
  SNES snes;
  PetscBool view = PETSC_FALSE;
  PetscInt pk;
  MMSCtx ctx;
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Darcy mesh %.3D x %.3D : element: P%D-P%D [L2-H1]\n",mxy,mxy,pk+1,pk);
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                     2,DMTET_CWARP_AND_BLEND,pk+1,&dmV);CHKERRQ(ierr);
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,pk,&dmP);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dmP);CHKERRQ(ierr); /* required for surface integration */
  ctx.dm = dmV;
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_UseMMS_V,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_UseMMS_V,NULL);CHKERRQ(ierr);
  
  ierr = DMBCListCreate(dmP,&bcP);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcP,0,EvalDirichletBoundaryLR_UseMMS_P,NULL);CHKERRQ(ierr);
  
  /* pde for darcy */
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,dmV,dmP,NULL,bcP,DARCY_BFORM_L2H1,&pde);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde,"dcy_");CHKERRQ(ierr);
  
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    //ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  /* volume coefficients */
  {
    Coefficient a,f,g;
    
    ierr = PDEDarcyGetVCoefficients(pde,&a,&f,&g);CHKERRQ(ierr);
    
    /* inverse permability */
    ierr = CoefficientSetComputeQuadrature(a,a_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    
    if (view) {
      ierr = CoefficientEvaluate(a);CHKERRQ(ierr);
      ierr = CoefficientView_GP(a,dmP,"a-qp");CHKERRQ(ierr);
    }
    
    /* u-momentum right hand side */
    ierr = CoefficientSetComputeQuadrature(f,f_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    
    if (view) {
      ierr = CoefficientEvaluate(f);CHKERRQ(ierr);
      ierr = CoefficientView_GP(f,dmP,"f-qp");CHKERRQ(ierr);
    }
    
    /* p-continuity right hand side */
    ierr = CoefficientSetComputeQuadrature(g,g_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
  }
  /* surface coefficients */
  {
    Coefficient uN,pN;
    
    ierr = PDEDarcyGetSCoefficients(pde,&uN,&pN);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(uN,u_neumann_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
  }
  
  /* stokes solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  {
    Vec Xv,Xp;
    PetscReal Eu[Q_NUM_ENTRIES];
    PetscReal Ep[Q_NUM_ENTRIES],p_avg;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Eu,dmV,Xv,
                         DarcySolution_EvaluateV,
                         NULL,NULL);CHKERRQ(ierr);
    
    ierr = IntegrateField(&p_avg,dmP,Xp);CHKERRQ(ierr);
    
    ierr = VecShift(Xp,-p_avg);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Ep,dmP,Xp,
                         DarcySolution_EvaluateP,
                         DarcySolution_EvaluateGradientP,NULL);CHKERRQ(ierr);
    
    {
      PetscReal Enorms[5];
      
      ierr = IntegrateFieldHdiv(Enorms,dmV,Xv);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"# degree  %.2D %1.4e | hdiv %1.12e | hdiv_Linf %1.12e\n",pk,1.0/((PetscReal)mxy),Enorms[3],Enorms[4]);
      
    }
    
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    //PetscPrintf(PETSC_COMM_WORLD,"degree  %.2D %1.4e  %1.12e %1.12e %1.12e\n",pk,1.0/((PetscReal)mxy),Eu[Q_L2],Eu[Q_H1],Ep[Q_L2]);
    
    *h = 1.0/((PetscReal)mxy);
    *uL2 = Eu[Q_L2];
    *pL2 = Ep[Q_L2];
    *pH1 = Ep[Q_H1];
  }
  
  if (view) {
    ierr = PDEDarcyFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
    
    {
      DM dmVp;
      Vec Xv,Xp,vs = NULL;
      const char *fields[] = { "pvs" };
      char       prefix[PETSC_MAX_PATH_LEN];
      
      ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                         2,DMTET_CWARP_AND_BLEND,pk,&dmVp);CHKERRQ(ierr);
      
      ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
      ierr = DMTetProjectField(dmV,Xv,PRJTYPE_NATIVE,dmVp,&vs);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
      
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s_",fields[0]);CHKERRQ(ierr);
      ierr = DMTetViewFields_VTU(dmVp,1,&vs,fields,prefix);CHKERRQ(ierr);
      
      ierr = DMDestroy(&dmVp);CHKERRQ(ierr);
      ierr = VecDestroy(&vs);CHKERRQ(ierr);
    }
  }
  
  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "p_neumann_evaluate_quadrature"
PetscErrorCode p_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt q,e;
  PetscInt ncomp_q,ncomp_x;
  PetscReal *q_coeff,*q_coor;
  PetscInt nfacets,nqp;
  //MMSCtx *ctx = (MMSCtx*)data;
  
  PetscFunctionBegin;
  //dm = ctx->dm;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"pN",NULL,NULL,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",&nfacets,&nqp,&ncomp_x,&q_coor);CHKERRQ(ierr);
  
  for (e=0; e<nfacets; e++) {
    //PetscReal *q_coeff_e;
    
    //FIELD_GET_OFFSET(q_coeff,e,nqp,1,q_coeff_e); /* replaces q_coeff_e = &q_coeff[e*nqp*2] */
    for (q=0; q<nqp; q++) {
      PetscReal *x_qp,pressure;
      
      /* get x,y coords at each quadrature point */
      //x_qp = &q_coor[e*nqp*2];
      FIELD_GET_OFFSET(q_coor,e,nqp,2,x_qp);
      
      ierr = DarcySolution_EvaluateP(x_qp,&pressure,NULL);
      //q_coeff[e*nqp*2 + 2*q+0] = flux[0];
      //q_coeff[e*nqp*2 + 2*q+1] = flux[1];
      
      //q_coeff_e[FIELD_ID(q,0,2)] = flux[0];
      //q_coeff_e[FIELD_ID(q,1,2)] = flux[1];
      //FIELD_SET_VALS2(flux,q_coeff_e,q);
      q_coeff[e*nqp + q] = pressure;
    }
  }
  
  PetscFunctionReturn(0);
}

PetscErrorCode StokesUP_DG_AsciiVTU_g(DM dm,Vec field,const char filename[]);

#undef __FUNCT__
#define __FUNCT__ "project_g"
PetscErrorCode project_g(PDE pde,DM dm)
{
  PetscErrorCode ierr;
  Coefficient g;
  Quadrature quadrature;
  Vec projG = NULL;
  const PetscReal *LA_g;
  PetscInt e,nen,*element,nbasis,i,q,nqp;
  PetscReal *elfield,*coeff_g;
  EContainer ec;
  
  ierr = PDEDarcyGetVCoefficients(pde,NULL,NULL,&g);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
  ierr = DMTetProjectQuadratureField(quadrature,"g",dm,&projG);CHKERRQ(ierr);
  // plot
  ierr = StokesUP_DG_AsciiVTU_g(dm,projG,"projG.vtu");CHKERRQ(ierr);
  
  // interpolate
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,0,0,0);CHKERRQ(ierr);
  ierr = EContainerCreate(dm,quadrature,&ec);CHKERRQ(ierr);
  elfield = ec->buf_basis_scalar_a;
  ierr = QuadratureGetProperty(quadrature,"g",NULL,&nqp,NULL,&coeff_g);CHKERRQ(ierr);
  ierr = VecGetArrayRead(projG,&LA_g);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    idx = &element[nbasis*e];
    for (i=0; i<nbasis; i++) {
      elfield[i] = LA_g[idx[i]];
    }
    PetscReal g_p0 = 0.0;
    for (q=0; q<nqp; q++) {
      PetscReal g_qp = 0.0;
      for (i=0; i<nbasis; i++) {
        g_qp += ec->N[q][i] * elfield[i];
      }
      coeff_g[nqp*e+q] = g_qp;
      g_p0 = g_p0 + g_qp/((PetscReal)nqp);
    }
    coeff_g[nqp*e+q] = g_p0; /* set the constant? */
  }
  ierr = VecRestoreArrayRead(projG,&LA_g);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ec);CHKERRQ(ierr);

  
  ierr = VecDestroy(&projG);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/* -------------------------------- */
/* Auxillary space solve demo */
#include <pdeimpl.h>
#include <pdedarcyimpl.h>

typedef struct _p_LSolve *LSolve;
struct _p_LSolve {
  Vec diagA11;
  DM dm_p_dg,dm_p_cg;
  Vec b_cg,x_cg;
  Vec x_dg;
  KSP ksp;
  Mat L_cg;
  PDE pde;
  BCList dirichletbc;
  Mat P;
  PetscInt pre_smooth_its;
  PetscReal pre_smooth_factor,post_smooth_factor;
  PetscBool setup;
  PetscBool custom_setup_called;
};

static PetscErrorCode PCSetup_AuxSpace(PC pc);
static PetscErrorCode PCApply_LSolve(PC pc,Vec b,Vec x);
static PetscErrorCode PCApply_LSolve_v0(PC pc,Vec b,Vec x);
static PetscErrorCode PCView_AuxSpace(PC pc,PetscViewer viewer);

PetscErrorCode PCAuxSpaceSetOperators_demo(PC pc,Mat A,Mat B);
PetscErrorCode PCAuxSpaceSetProjection_demo(PC pc,Mat P);
PetscErrorCode PCAuxSpaceSetDMS_demo(PC pc,DM dm1,DM dm2);

#undef __FUNCT__
#define __FUNCT__ "PCApply_LSolve"
static PetscErrorCode PCApply_LSolve(PC pc,Vec b,Vec x)
{
  PetscErrorCode ierr;
  LSolve ctx;
  Vec r,diagS,x_s;
  Mat S,Spc;
  PetscInt k,max_it = 25;
  
  ierr = PCGetOperators(pc,&S,&Spc);CHKERRQ(ierr);
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  
  if (!ctx->custom_setup_called) {
    ierr = PCSetup_AuxSpace(pc);CHKERRQ(ierr);
  }
  if (!ctx->setup) {
    Mat A = NULL;
    
    ierr = KSPGetOperators(ctx->ksp,&A,NULL);CHKERRQ(ierr);
    ierr = MatCreateVecs(A,&ctx->b_cg,&ctx->x_cg);CHKERRQ(ierr);
    ctx->setup = PETSC_TRUE;
  }
  
  /*ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,b,"b_dg.vtu");CHKERRQ(ierr);*/
  
  ierr = VecDuplicate(b,&r);CHKERRQ(ierr);
  ierr = VecDuplicate(b,&x_s);CHKERRQ(ierr);
  ierr = VecDuplicate(b,&diagS);CHKERRQ(ierr);
  
  /* pre-smooth */
  {
    PetscErrorCode (*matop_get_diagonal)(Mat,Vec) = NULL;
    
    ierr = MatGetOperation(Spc,MATOP_GET_DIAGONAL,(void(**)(void))&matop_get_diagonal);CHKERRQ(ierr);
    if (matop_get_diagonal) {
      ierr = MatGetDiagonal(Spc,diagS);CHKERRQ(ierr);
    } else {
      PetscPrintf(PETSC_COMM_WORLD,"PCApply_AuxSpace[Warning]: Bmat does not support MatGetDiagonal, using diag_ii = 1.0\n");
      ierr = VecSet(diagS,1.0);CHKERRQ(ierr);
    }
  }
  
  // r = b - Ax
  ierr = MatMult(S,x,r);CHKERRQ(ierr);
  ierr = VecAYPX(r,-1.0,b);CHKERRQ(ierr); //r = -r + b
  
  ierr = VecCopy(x,x_s);CHKERRQ(ierr);
  
  max_it = ctx->pre_smooth_its;
  for (k=0; k<max_it; k++) {
    //x = x + 0.3 * inv(diag(S)) r
    ierr = VecPointwiseDivide(r,r,diagS);CHKERRQ(ierr);
    ierr = VecAXPY(x_s,ctx->pre_smooth_factor,r);CHKERRQ(ierr);
    
    // r = b - Ax
    ierr = MatMult(S,x_s,r);CHKERRQ(ierr);
    ierr = VecAYPX(r,-1.0,b);CHKERRQ(ierr); //r = -r + b
  }

  /*ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,x_s,"xs_dg.vtu");CHKERRQ(ierr);*/
  /*ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,b,"r_dg.vtu");CHKERRQ(ierr);*/

  //ierr = DMTetProjectField(ctx->dm_p_dg,r,PRJTYPE_NATIVE,ctx->dm_p_cg,&ctx->b_cg);CHKERRQ(ierr);
  ierr = MatMultTranspose(ctx->P,r,ctx->b_cg);CHKERRQ(ierr);
  /*ierr = StokesUP_CG_AsciiVTU_g(ctx->dm_p_cg,ctx->b_cg,"r_cg.vtu");CHKERRQ(ierr);*/
  
  //KSPSetInitialGuessNonzero(ctx->ksp,PETSC_TRUE);
  ierr = KSPSolve(ctx->ksp,ctx->b_cg,ctx->x_cg);CHKERRQ(ierr);
  
  /*ierr = StokesUP_CG_AsciiVTU_g(ctx->dm_p_cg,ctx->x_cg,"e_cg.vtu");CHKERRQ(ierr);*/
  
  //ierr = DMTetProjectField(ctx->dm_p_cg,ctx->x_cg,PRJTYPE_NATIVE,ctx->dm_p_dg,&ctx->x_dg);CHKERRQ(ierr);
  ierr = MatMult(ctx->P,ctx->x_cg,ctx->x_dg);CHKERRQ(ierr);
  /*ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,ctx->x_dg,"e_dg.vtu");CHKERRQ(ierr);*/
  
  ierr = VecAXPY(x_s,ctx->post_smooth_factor,ctx->x_dg);CHKERRQ(ierr);
  /*ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,x_s,"x_up_dg.vtu");CHKERRQ(ierr);*/
  
  ierr = VecCopy(x_s,x);CHKERRQ(ierr);
  
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  ierr = VecDestroy(&x_s);CHKERRQ(ierr);
  ierr = VecDestroy(&diagS);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCApply_LSolve_v0"
static PetscErrorCode PCApply_LSolve_v0(PC pc,Vec b,Vec x)
{
  PetscErrorCode ierr;
  LSolve ctx;
  
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  
  ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,b,"b_dg.vtu");CHKERRQ(ierr);
  
  ierr = DMTetProjectField(ctx->dm_p_dg,b,PRJTYPE_NATIVE,ctx->dm_p_cg,&ctx->b_cg);CHKERRQ(ierr);
  ierr = StokesUP_CG_AsciiVTU_g(ctx->dm_p_cg,ctx->b_cg,"r_cg.vtu");CHKERRQ(ierr);
  
  ierr = KSPSolve(ctx->ksp,ctx->b_cg,ctx->x_cg);CHKERRQ(ierr);
  ierr = StokesUP_CG_AsciiVTU_g(ctx->dm_p_cg,ctx->x_cg,"e_cg.vtu");CHKERRQ(ierr);
  
  ierr = DMTetProjectField(ctx->dm_p_cg,ctx->x_cg,PRJTYPE_NATIVE,ctx->dm_p_dg,&ctx->x_dg);CHKERRQ(ierr);
  ierr = StokesUP_DG_AsciiVTU_g(ctx->dm_p_dg,ctx->x_dg,"e_dg.vtu");CHKERRQ(ierr);
  
  ierr = VecCopy(ctx->x_dg,x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
  x_cg = P x_dg
*/
#undef __FUNCT__
#define __FUNCT__ "MatMult_ProjectDGCG"
PetscErrorCode MatMult_ProjectDGCG(Mat A,Vec b,Vec x)
{
  PetscErrorCode ierr;
  LSolve ctx;
  Vec b_cg = NULL;
  
  ierr = MatShellGetContext(A,(void**)&ctx);CHKERRQ(ierr);
  ierr = DMTetProjectField(ctx->dm_p_dg,b,PRJTYPE_NATIVE,ctx->dm_p_cg,&b_cg);CHKERRQ(ierr);
  ierr = VecCopy(b_cg,x);CHKERRQ(ierr);
  VecDestroy(&b_cg);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatProjectCreate_D2C"
PetscErrorCode MatProjectCreate_D2C(LSolve ctx,Mat *A)
{
  PetscErrorCode ierr;
  PetscInt M_dg,M_cg,m_dg,m_cg;
  Vec x_cg,x_dg;
  
  DMCreateGlobalVector(ctx->dm_p_dg,&x_dg);
  DMCreateGlobalVector(ctx->dm_p_cg,&x_cg);
  VecGetSize(x_dg,&M_dg);
  VecGetSize(x_cg,&M_cg);
  VecGetLocalSize(x_dg,&m_dg);
  VecGetLocalSize(x_cg,&m_cg);
  VecDestroy(&x_dg);
  VecDestroy(&x_cg);
  
  printf("D-2-C : %d x %d\n",M_cg,M_dg);
  ierr = MatCreateShell(PETSC_COMM_WORLD,m_cg,m_dg,M_cg,M_dg,(void*)ctx,A);CHKERRQ(ierr);
  MatShellSetOperation(*A,MATOP_MULT,(void(*)(void))MatMult_ProjectDGCG);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatMult_ProjectCGDG"
PetscErrorCode MatMult_ProjectCGDG(Mat A,Vec b,Vec x)
{
  PetscErrorCode ierr;
  LSolve ctx;
  Vec b_dg = NULL;
  
  ierr = MatShellGetContext(A,(void**)&ctx);CHKERRQ(ierr);
  ierr = DMTetProjectField(ctx->dm_p_cg,b,PRJTYPE_NATIVE,ctx->dm_p_dg,&b_dg);CHKERRQ(ierr);
  ierr = VecCopy(b_dg,x);CHKERRQ(ierr);
  VecDestroy(&b_dg);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatProjectCreate_C2D"
PetscErrorCode MatProjectCreate_C2D(LSolve ctx,Mat *A)
{
  PetscErrorCode ierr;
  PetscInt M_dg,M_cg,m_dg,m_cg;
  Vec x_cg,x_dg;
  
  DMCreateGlobalVector(ctx->dm_p_dg,&x_dg);
  DMCreateGlobalVector(ctx->dm_p_cg,&x_cg);
  VecGetSize(x_dg,&M_dg);
  VecGetSize(x_cg,&M_cg);
  VecGetLocalSize(x_dg,&m_dg);
  VecGetLocalSize(x_cg,&m_cg);
  VecDestroy(&x_dg);
  VecDestroy(&x_cg);
  
  printf("C-2-D : %d x %d\n",M_dg,M_cg);
  ierr = MatCreateShell(PETSC_COMM_WORLD,m_dg,m_cg,M_dg,M_cg,(void*)ctx,A);CHKERRQ(ierr);
  MatShellSetOperation(*A,MATOP_MULT,(void(*)(void))MatMult_ProjectCGDG);
  PetscFunctionReturn(0);
}

#if 0
#undef __FUNCT__
#define __FUNCT__ "FormBilinear_LCG"
PetscErrorCode FormBilinear_LCG(Mat B,LSolve data)
{
  PetscErrorCode ierr;
  PetscInt  e,nel,npe,i,j;
  PetscInt  nbasis;
  PetscInt  *element;
  PetscReal   *elcoords;
  PetscInt    *elnidx,*eldofs;
  PetscReal   *coords,*Ke;
  PetscInt q,nqp,nb;
  PetscReal *w,*xi,*detJ,**tab_N,**tab_dNdxi,**tab_dNdeta,**tab_dNdx,**tab_dNdy;
  IS ltog;
  const PetscInt *GINDICES;
  PetscInt       NUM_GINDICES;
  BCList bc = NULL;
  DM dm = NULL;
  PetscReal *LA_diag;
  
  dm = data->dm_p_cg;
  bc = data->dirichletbc;
  
  ierr = MatZeroEntries(B);CHKERRQ(ierr);
  //VecView(data->diagA11,PETSC_VIEWER_STDOUT_WORLD);
  ierr = DMTetSpaceElement(dm,&nel,0,&element,&npe,&NUM_GINDICES,&coords);CHKERRQ(ierr);
  nbasis = npe;
  
  PetscMalloc1(2*nbasis,&elcoords);
  PetscMalloc1(nbasis,&eldofs);
  PetscMalloc1(nbasis*nbasis,&Ke);
  
  ierr = DMTetQuadratureCreate_2d(dm,&nqp,&w,&xi);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nb,&tab_N);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdxi,&tab_dNdeta,NULL);CHKERRQ(ierr);
  ierr = DMTetTabulateBasisDerivatives(dm,nqp,xi,&nb,&tab_dNdx,&tab_dNdy,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nqp,&detJ);CHKERRQ(ierr);
  
  ierr = ISCreateStride(PETSC_COMM_WORLD,NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
  ierr = ISGetIndices(ltog, &GINDICES);CHKERRQ(ierr);
  if (bc) {
    ierr = BCListApplyDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  
  VecGetArray(data->diagA11,&LA_diag);
  
  for (e=0; e<nel; e++) {
    PetscReal eldiag[40];
    
    /* get element -> node map */
    elnidx = &element[nbasis*e];
    
    for (i=0; i<nbasis; i++) {
      eldiag[i] = LA_diag[ elnidx[i] ];
    }
    
    /* generate dofs */
    for (i=0; i<nbasis; i++) {
      const int NID = elnidx[i];
      
      eldofs[i] = GINDICES[NID];
    }
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = elnidx[i];
      elcoords[2*i  ] = coords[2*nidx  ];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    /* compute derivatives */
    ElementEvaluateGeometry_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,detJ);
    ElementEvaluateDerivatives_2d(nqp,nbasis,elcoords,tab_dNdxi,tab_dNdeta,tab_dNdx,tab_dNdy);
    
    PetscMemzero(Ke,sizeof(PetscReal)*nbasis*nbasis);
    
    for (q=0; q<nqp; q++) {
      PetscReal fac,dv;
      PetscReal *Ni,*dNidx,*dNidy;
      
      /* get access to element->quadrature points */
      Ni    = tab_N[q];
      dNidx = tab_dNdx[q];
      dNidy = tab_dNdy[q];
      
      fac = PetscAbsReal(detJ[q]) * w[q];
      
      dv = 0.0;
      for (i=0; i<nbasis; i++) {
        dv += Ni[i] * eldiag[i];
      }
      if (e == 0) {
        printf("e = 0 : using diag_q[%d] %+1.4e\n",q,dv);
      }
      
      for (i=0; i<nbasis; i++) {
        for (j=0; j<nbasis; j++) {
          Ke[ i*nbasis + j ] += fac * dv * (dNidx[i]*dNidx[j] + dNidy[i]*dNidy[j]);
        }
      }
      
    } // quadrature
    ierr = MatSetValues(B,nbasis,eldofs,nbasis,eldofs,Ke,ADD_VALUES);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
  
  if (bc) {
    ierr = BCListRemoveDirichletMask(NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
    ierr = BCListInsertScaling(B,NUM_GINDICES,(PetscInt*)GINDICES,bc);CHKERRQ(ierr);
  }
  ierr = ISRestoreIndices(ltog, &GINDICES);CHKERRQ(ierr);
  VecRestoreArray(data->diagA11,&LA_diag);
  
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  //MatView(B,PETSC_VIEWER_STDOUT_WORLD);
  ierr = ISDestroy(&ltog);CHKERRQ(ierr);
  PetscFree(w);
  PetscFree(xi);
  PetscFree(detJ);
  PetscFree(elcoords);
  PetscFree(eldofs);
  PetscFree(Ke);
  for (q=0; q<nqp; q++) {
    PetscFree(tab_N[q]);
    
    PetscFree(tab_dNdxi[q]);
    PetscFree(tab_dNdeta[q]);
    
    PetscFree(tab_dNdx[q]);
    PetscFree(tab_dNdy[q]);
  }
  PetscFree(tab_N);
  PetscFree(tab_dNdxi);
  PetscFree(tab_dNdeta);
  PetscFree(tab_dNdx);
  PetscFree(tab_dNdy);
  
  PetscFunctionReturn(0);
}
#endif

#undef __FUNCT__
#define __FUNCT__ "__hack__DarcySolution_EvaluateP"
PetscErrorCode __hack__DarcySolution_EvaluateP(PetscReal x[],PetscReal p[],void *ctx)
{
  p[0] = -sin(2.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__hack__EvalDirichletBoundaryLR_UseMMS_P"
PetscErrorCode __hack__EvalDirichletBoundaryLR_UseMMS_P(PetscScalar coor[],PetscInt dof,
                                                        PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscErrorCode ierr;
  PetscReal pressure;
  const PetscReal coor_eps = 1.0e-10;
  
  *constrain = PETSC_FALSE;
  *val = 0.0;
  
  ierr = __hack__DarcySolution_EvaluateP(coor,&pressure,NULL);CHKERRQ(ierr);
  
  if ( PetscAbsReal(coor[0]) < coor_eps) { // left
    *constrain = PETSC_TRUE;
  }
  if ( PetscAbsReal(coor[0] - 1.0) < coor_eps) { // right
    *constrain = PETSC_TRUE;
  }
  if (*constrain) {
    *val = pressure;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCView_AuxSpace"
static PetscErrorCode PCView_AuxSpace(PC pc,PetscViewer viewer)
{
  PetscErrorCode ierr;
  LSolve ctx;
  
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);

  PetscViewerASCIIPushTab(viewer);
  PetscViewerASCIIPrintf(viewer,"AuxSpace: pre-smooth iterations = %D\n",ctx->pre_smooth_its);
  PetscViewerASCIIPrintf(viewer,"AuxSpace: pre-smooth factor = %1.4e\n",ctx->pre_smooth_factor);
  PetscViewerASCIIPrintf(viewer,"AuxSpace: post-smooth factor = %1.4e\n",ctx->post_smooth_factor);
  PetscViewerASCIIPopTab(viewer);

  PetscViewerASCIIPrintf(viewer,"Natural space:\n");
  PetscViewerASCIIPushTab(viewer);
  if (ctx->dm_p_dg) {
    ierr = DMView(ctx->dm_p_dg,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  } else {
    PetscViewerASCIIPushTab(viewer);
    PetscViewerASCIIPrintf(viewer,"No DM provided\n");
    PetscViewerASCIIPopTab(viewer);
  }
  PetscViewerASCIIPopTab(viewer);
  
  PetscViewerASCIIPrintf(viewer,"Auxiliary space:\n");
  PetscViewerASCIIPushTab(viewer);
  if (ctx->dm_p_cg) {
    ierr = DMView(ctx->dm_p_cg,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  } else {
    PetscViewerASCIIPushTab(viewer);
    PetscViewerASCIIPrintf(viewer,"No DM provided\n");
    PetscViewerASCIIPopTab(viewer);
  }
  

  PetscViewerASCIIPopTab(viewer);

  ierr = KSPView(ctx->ksp,viewer);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCSetup_AuxSpace"
static PetscErrorCode PCSetup_AuxSpace(PC pc)
{
  PetscErrorCode ierr;
  LSolve ctx;
  Mat Amat,Bmat;
  PetscViewer viewer = PETSC_VIEWER_STDOUT_WORLD;
  
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  if (ctx->custom_setup_called) PetscFunctionReturn(0);

  ierr = PCGetOperators(pc,&Amat,&Bmat);CHKERRQ(ierr);
  
  if (!ctx->P) {
    Mat c_P = NULL;

    ierr = PetscObjectQuery((PetscObject)Bmat,"PCAuxSpace_P",(PetscObject*)&c_P);CHKERRQ(ierr);
    
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_INFO);
    PetscViewerASCIIPrintf(viewer,"PCSetup_AuxSpace: Querying operator Bmat for composed object name \"PCAuxSpace_P\"\n");
    PetscViewerASCIIPushTab(viewer);
    MatView(Bmat,viewer);
    PetscViewerASCIIPopTab(viewer);
    PetscViewerPopFormat(viewer);
    
    if (c_P) {
      ierr = PCAuxSpaceSetProjection_demo(pc,c_P);CHKERRQ(ierr);
    } else {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Projection (P) not provided");
    }
  }
  if (!ctx->L_cg) {
    Mat c_A = NULL;

    ierr = PetscObjectQuery((PetscObject)Bmat,"PCAuxSpace_PtAP",(PetscObject*)&c_A);CHKERRQ(ierr);
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_INFO);
    PetscViewerASCIIPrintf(viewer,"PCSetup_AuxSpace: Querying operator Bmat for composed object name \"PCAuxSpace_PtAP\"\n");
    PetscViewerASCIIPushTab(viewer);
    MatView(Bmat,viewer);
    PetscViewerASCIIPopTab(viewer);
    PetscViewerPopFormat(viewer);
    
    if (c_A) {
      ierr = PCAuxSpaceSetProjection_demo(pc,c_A);CHKERRQ(ierr);
    } else {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Auxiliary projected operator (Pt.A.P) not provided");
    }
  }
  ctx->custom_setup_called = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCCreate_LSolve"
PetscErrorCode PCCreate_LSolve(PDE pde,Vec diag,Mat S,PC *pc)
{
  
  LSolve ctx;
  PDEDarcy *data;
  Mat PtSP = NULL;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_LSolve));CHKERRQ(ierr);
  ctx->pre_smooth_its = 5;
  ctx->pre_smooth_factor = 0.3;
  ctx->post_smooth_factor = 1.0;
  PetscOptionsGetInt(NULL,NULL,"-aux_presmooth_its",&ctx->pre_smooth_its,NULL);
  PetscOptionsGetReal(NULL,NULL,"-aux_presmooth_fac",&ctx->pre_smooth_factor,NULL);
  PetscOptionsGetReal(NULL,NULL,"-aux_postsmooth_fac",&ctx->post_smooth_factor,NULL);
  
  ctx->dirichletbc = NULL;
  
  ctx->pde = pde;
  data = (PDEDarcy*)pde->data;
  
  ctx->dm_p_dg = data->dmp;
  {
    DM dmP;
    PetscInt pk;
    //PetscInt mxy;
    //DM dmref;
    
    pk = 1;
    PetscOptionsGetInt(NULL,NULL,"-aux_pk",&pk,NULL);
    /*
    mxy = 6;
    ierr = PetscOptionsGetInt(NULL,NULL,"-mxy",&mxy,NULL);CHKERRQ(ierr);
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,pk,&dmref);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dmref,1,&dmP);CHKERRQ(ierr);
    ierr = DMTetBFacetSetup(dmP);CHKERRQ(ierr); // required for surface integration //
    ierr = DMDestroy(&dmref);CHKERRQ(ierr);
    */
    ierr = DMTetCloneGeometry(ctx->dm_p_dg,1,DMTET_CWARP_AND_BLEND,pk,&dmP);CHKERRQ(ierr);
    ctx->dm_p_cg = dmP;
  }
  
  ierr = DMBCListCreate(ctx->dm_p_cg,&ctx->dirichletbc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(ctx->dirichletbc,0,__hack__EvalDirichletBoundaryLR_UseMMS_P,NULL);CHKERRQ(ierr);

  /*
  ierr = DMCreateGlobalVector(data->dmu,&ctx->diagA11);CHKERRQ(ierr);
  ierr = VecSet(ctx->diagA11,1.0);CHKERRQ(ierr);
  if (diag) {
    ierr = VecCopy(diag,ctx->diagA11);CHKERRQ(ierr);
  }
  */
  ierr = DMCreateGlobalVector(data->dmp,&ctx->diagA11);CHKERRQ(ierr);
  ierr = VecSet(ctx->diagA11,1.0);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(ctx->dm_p_cg,&ctx->L_cg);CHKERRQ(ierr);
  ierr = MatCreateVecs(ctx->L_cg,&ctx->b_cg,&ctx->x_cg);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(ctx->dm_p_dg,&ctx->x_dg);CHKERRQ(ierr);
  
  /*ierr = FormBilinear_LCG(ctx->L_cg,ctx);CHKERRQ(ierr);*/
  
  {
    Mat P=NULL,Pe=NULL,Pt=NULL,R=NULL,Re=NULL,SP=NULL;
    PetscInt MS,MPtSP;
    
    /* this can be explicitly assembled without resorting to jacobi approx of the mass matrix */
    //ierr = MatProjectCreate_C2D(ctx,&P);CHKERRQ(ierr);
    //ierr = MatComputeExplicitOperator(P,&Pe);CHKERRQ(ierr);
    //ierr = MatConvert(Pe,MATAIJ,MAT_INITIAL_MATRIX,&dummy);CHKERRQ(ierr);
    //MatDestroy(&Pe);
    //Pe = dummy;
    ierr = DMTetAssembleProjection(ctx->dm_p_cg,ctx->dm_p_dg,&Pe);CHKERRQ(ierr);
    ctx->P = Pe;
    
    //ierr = MatProjectCreate_D2C(ctx,&R);CHKERRQ(ierr);
    //ierr = MatComputeExplicitOperator(R,&Re);CHKERRQ(ierr);
    //ierr = MatConvert(Re,MATAIJ,MAT_INITIAL_MATRIX,&dummy);CHKERRQ(ierr);
    //MatDestroy(&Re);
    //Re = dummy;
    
    // also works also with -proj_f_pc_type jacobi
    ierr = MatMatMult(S,Pe,MAT_INITIAL_MATRIX,1.0,&SP);CHKERRQ(ierr);
    ierr = MatTranspose(Pe,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
    ierr = MatMatMult(Pt,SP,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);

    // winner: works with -proj_f_pc_type jacobi
    //ierr = MatMatMult(S,Pe,MAT_INITIAL_MATRIX,1.0,&SP);CHKERRQ(ierr);
    //ierr = MatMatMult(Re,SP,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);

    // convergence is slow (~3x more than the above)
    //ierr = MatTranspose(Re,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
    //ierr = MatMatMult(S,Pt,MAT_INITIAL_MATRIX,1.0,&SP);CHKERRQ(ierr);
    //ierr = MatMatMult(Re,SP,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);

    /* When using S = D inv(diag(M)) G - this is not required, in fact it ruins convergence */
    /* When using S = D G - we need to impose the BCs explicitly (at least when R != Pt) */
    /*
    {
      IS ltog;
      const PetscInt *GINDICES;
      PetscInt       NUM_GINDICES;

      ierr = DMTetSpaceElement(ctx->dm_p_cg,0,0,0,0,&NUM_GINDICES,0);CHKERRQ(ierr);
      ierr = ISCreateStride(PETSC_COMM_WORLD,NUM_GINDICES,0,1,&ltog);CHKERRQ(ierr);
      ierr = ISGetIndices(ltog,&GINDICES);CHKERRQ(ierr);

      if (ctx->dirichletbc) {
        PetscInt k,L;
        PetscInt *idx;
        BCList bclist = ctx->dirichletbc;
        //ierr = BCListInsertScaling(PtSP,NUM_GINDICES,(PetscInt*)GINDICES,bclist);CHKERRQ(ierr);

        L   = bclist->L_local;
        idx = bclist->dofidx_local;
        for (k=0; k<L; k++) {
          if (idx[k] == BCList_DIRICHLET) {
            ierr = MatZeroRowsColumns(PtSP,1,&GINDICES[k],1.0,NULL,NULL);CHKERRQ(ierr);
          }
        }
        ierr = ISRestoreIndices(ltog,&GINDICES);CHKERRQ(ierr);
        ierr = ISDestroy(&ltog);CHKERRQ(ierr);
      }
    }
    */
     
    MatGetSize(S,&MS,NULL);
    MatGetSize(PtSP,&MPtSP,NULL);
    printf("[fine] S %d x %d | [coarse] Pt.S.P %d x %d\n",MS,MS,MPtSP,MPtSP);
    MatDestroy(&P);
    //MatDestroy(&Pe);
    MatDestroy(&R);
    MatDestroy(&Re);
    MatDestroy(&Pt);
    MatDestroy(&SP);
  }
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ctx->ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ctx->ksp,"L_");CHKERRQ(ierr);
  //ierr = KSPSetOperators(ctx->ksp,ctx->L_cg,ctx->L_cg);CHKERRQ(ierr);
  ierr = KSPSetOperators(ctx->ksp,PtSP,PtSP);CHKERRQ(ierr);
  ierr = KSPSetType(ctx->ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ctx->ksp);CHKERRQ(ierr);
  
  ctx->setup = PETSC_TRUE;
  
  ierr = PCCreate(PETSC_COMM_WORLD,pc);CHKERRQ(ierr);
  ierr = PCSetOperators(*pc,S,S);CHKERRQ(ierr);
  ierr = PCSetType(*pc,PCSHELL);CHKERRQ(ierr);
  ierr = PCShellSetName(*pc,"AuxiliarySpace");CHKERRQ(ierr);
  ierr = PCShellSetContext(*pc,(void*)ctx);CHKERRQ(ierr);
  ierr = PCShellSetView(*pc,PCView_AuxSpace);CHKERRQ(ierr);
  ierr = PCShellSetApply(*pc,PCApply_LSolve);CHKERRQ(ierr);
  //ierr = PCShellSetApply(*pc,PCApply_LSolve_v0);CHKERRQ(ierr);
  ierr = PCShellSetSetUp(*pc,PCSetup_AuxSpace);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_SetOperators"
static PetscErrorCode PCAuxSpace_SetOperators(PC pc,Mat A,Mat B)
{
  PetscErrorCode ierr;
  LSolve ctx;
  
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ctx->L_cg = B;
  ierr = KSPSetOperators(ctx->ksp,ctx->L_cg,ctx->L_cg);CHKERRQ(ierr);

  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceSetOperators_demo"
PetscErrorCode PCAuxSpaceSetOperators_demo(PC pc,Mat A,Mat B)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceSetOperators_C",(PC,Mat,Mat),(pc,A,B));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_SetProjection"
static PetscErrorCode PCAuxSpace_SetProjection(PC pc,Mat P)
{
  PetscErrorCode ierr;
  LSolve ctx;
  
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ctx->P = P;
  
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceSetProjection_demo"
PetscErrorCode PCAuxSpaceSetProjection_demo(PC pc,Mat P)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceSetProjection_C",(PC,Mat),(pc,P));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_SetDMS"
static PetscErrorCode PCAuxSpace_SetDMS(PC pc,DM dm,DM dm_aux)
{
  PetscErrorCode ierr;
  LSolve ctx;
  
  ierr = PCShellGetContext(pc,(void**)&ctx);CHKERRQ(ierr);
  ctx->dm_p_dg = dm;
  ctx->dm_p_cg = dm_aux;
  
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceSetDMS_demo"
PetscErrorCode PCAuxSpaceSetDMS_demo(PC pc,DM dm1,DM dm2)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceSetDMS_C",(PC,DM,DM),(pc,dm1,dm2));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceConfigure"
PetscErrorCode PCAuxSpaceConfigure(PDE pde,Mat S,PC pc)
{
  
  LSolve ctx;
  PDEDarcy *data;
  PetscInt pk;
  Mat Smf;
  PetscBool isshell;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_LSolve));CHKERRQ(ierr);
  ctx->pre_smooth_its = 5;
  ctx->pre_smooth_factor = 0.3;
  ctx->post_smooth_factor = 1.0;
  PetscOptionsGetInt(NULL,NULL,"-aux_presmooth_its",&ctx->pre_smooth_its,NULL);
  PetscOptionsGetReal(NULL,NULL,"-aux_presmooth_fac",&ctx->pre_smooth_factor,NULL);
  PetscOptionsGetReal(NULL,NULL,"-aux_postsmooth_fac",&ctx->post_smooth_factor,NULL);
  
  ctx->dirichletbc = NULL;
  
  ctx->pde = pde;
  data = (PDEDarcy*)pde->data;
  
  ctx->dm_p_dg = data->dmp;
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-aux_pk",&pk,NULL);CHKERRQ(ierr);
  ierr = DMTetCloneGeometry(ctx->dm_p_dg,1,DMTET_CWARP_AND_BLEND,pk,&ctx->dm_p_cg);CHKERRQ(ierr);
  
  ierr = DMBCListCreate(ctx->dm_p_cg,&ctx->dirichletbc);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(ctx->dirichletbc,0,__hack__EvalDirichletBoundaryLR_UseMMS_P,NULL);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(data->dmp,&ctx->diagA11);CHKERRQ(ierr);
  ierr = VecSet(ctx->diagA11,1.0);CHKERRQ(ierr);
  
  ctx->L_cg = NULL;
  
  {
    Mat P=NULL,Pe=NULL,Pt=NULL,R=NULL,Re=NULL,SP=NULL,PtSP=NULL;
    PetscInt MS,MPtSP;
    
    ierr = DMTetAssembleProjection(ctx->dm_p_cg,ctx->dm_p_dg,&Pe);CHKERRQ(ierr);
    ctx->P = Pe;
    
    //ierr = MatProjectCreate_D2C(ctx,&R);CHKERRQ(ierr);
    //ierr = MatComputeExplicitOperator(R,&Re);CHKERRQ(ierr);
    //ierr = MatConvert(Re,MATAIJ,MAT_INITIAL_MATRIX,&dummy);CHKERRQ(ierr);
    //MatDestroy(&Re);
    //Re = dummy;
    
    // also works also with -proj_f_pc_type jacobi
    ierr = MatMatMult(S,Pe,MAT_INITIAL_MATRIX,1.0,&SP);CHKERRQ(ierr);
    ierr = MatTranspose(Pe,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
    ierr = MatMatMult(Pt,SP,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);
    
  
    MatGetSize(S,&MS,NULL);
    MatGetSize(PtSP,&MPtSP,NULL);
    printf("[fine] S %d x %d | [coarse] Pt.S.P %d x %d\n",MS,MS,MPtSP,MPtSP);
    MatDestroy(&P);
    //MatDestroy(&Pe);
    MatDestroy(&R);
    MatDestroy(&Re);
    MatDestroy(&Pt);
    MatDestroy(&SP);
    
    ctx->L_cg = PtSP;
  }
  
  ierr = MatCreateVecs(ctx->L_cg,&ctx->b_cg,&ctx->x_cg);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(ctx->dm_p_dg,&ctx->x_dg);CHKERRQ(ierr);
  
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ctx->ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ctx->ksp,"L_");CHKERRQ(ierr);
  ierr = KSPSetOperators(ctx->ksp,ctx->L_cg,ctx->L_cg);CHKERRQ(ierr);
  ierr = KSPSetType(ctx->ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ctx->ksp);CHKERRQ(ierr);
  
  ierr = PCGetOperators(pc,&Smf,NULL);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)Smf);CHKERRQ(ierr);
  ierr = PCSetOperators(pc,Smf,S);CHKERRQ(ierr);

  ctx->setup = PETSC_TRUE;
  
  ierr = PetscObjectTypeCompare((PetscObject)pc,PCSHELL,&isshell);CHKERRQ(ierr);
  if (isshell) {
    ierr = PCShellSetName(pc,"AuxiliarySpace");CHKERRQ(ierr);
    ierr = PCShellSetContext(pc,(void*)ctx);CHKERRQ(ierr);
    ierr = PCShellSetView(pc,PCView_AuxSpace);CHKERRQ(ierr);
    ierr = PCShellSetApply(pc,PCApply_LSolve);CHKERRQ(ierr);
    ierr = PCShellSetSetUp(pc,PCSetup_AuxSpace);CHKERRQ(ierr);
   
    ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetOperators_C",PCAuxSpace_SetOperators);CHKERRQ(ierr);
    ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetProjection_C",PCAuxSpace_SetProjection);CHKERRQ(ierr);
    ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetDMS_C",PCAuxSpace_SetDMS);CHKERRQ(ierr);
  }
  ierr = PCAuxSpaceSetOperators_demo(pc,ctx->L_cg,ctx->L_cg);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceConfigure_v2"
PetscErrorCode PCAuxSpaceConfigure_v2(PC pc)
{
  
  LSolve ctx;
  Mat A;
  PetscBool isshell;
  PetscErrorCode ierr;

  ierr = PetscObjectTypeCompare((PetscObject)pc,PCSHELL,&isshell);CHKERRQ(ierr);
  if (!isshell) { PetscFunctionReturn(0); }
  
  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_LSolve));CHKERRQ(ierr);
  ctx->pre_smooth_its = 5;
  ctx->pre_smooth_factor = 0.3;
  ctx->post_smooth_factor = 1.0;
  PetscOptionsGetInt(NULL,NULL,"-aux_presmooth_its",&ctx->pre_smooth_its,NULL);
  PetscOptionsGetReal(NULL,NULL,"-aux_presmooth_fac",&ctx->pre_smooth_factor,NULL);
  PetscOptionsGetReal(NULL,NULL,"-aux_postsmooth_fac",&ctx->post_smooth_factor,NULL);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ctx->ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ctx->ksp,"L_");CHKERRQ(ierr);
  ierr = KSPSetType(ctx->ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ctx->ksp);CHKERRQ(ierr);

  ierr = KSPSetOperators(ctx->ksp,NULL,NULL);CHKERRQ(ierr);
  
  /* temp work vectors */
  ierr = PCGetOperators(pc,&A,NULL);CHKERRQ(ierr);
  ierr = MatCreateVecs(A,&ctx->x_dg,NULL);CHKERRQ(ierr);
  
  ierr = PCShellSetName(pc,"AuxiliarySpace");CHKERRQ(ierr);
  ierr = PCShellSetContext(pc,(void*)ctx);CHKERRQ(ierr);
  ierr = PCShellSetView(pc,PCView_AuxSpace);CHKERRQ(ierr);
  ierr = PCShellSetApply(pc,PCApply_LSolve);CHKERRQ(ierr);
  ierr = PCShellSetSetUp(pc,PCSetup_AuxSpace);CHKERRQ(ierr);
  
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetOperators_C",PCAuxSpace_SetOperators);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetProjection_C",PCAuxSpace_SetProjection);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetDMS_C",PCAuxSpace_SetDMS);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceConfigure_Darcy"
PetscErrorCode PCAuxSpaceConfigure_Darcy(PDE pde,Mat S,PC pc)
{
  PDEDarcy *data;
  PetscInt pk;
  DM dm_p_dg,dm_p_cg;
  Mat P,PtSP;
  PetscErrorCode ierr;
  
  data = (PDEDarcy*)pde->data;
  dm_p_dg = data->dmp;
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-aux_pk",&pk,NULL);CHKERRQ(ierr);
  ierr = DMTetCloneGeometry(dm_p_dg,1,DMTET_CWARP_AND_BLEND,pk,&dm_p_cg);CHKERRQ(ierr);

  ierr = PCAuxSpaceSetDMS_demo(pc,dm_p_dg,dm_p_cg);CHKERRQ(ierr);

  ierr = DMTetAssembleProjection(dm_p_cg,dm_p_dg,&P);CHKERRQ(ierr);
  ierr = PCAuxSpaceSetProjection_demo(pc,P);CHKERRQ(ierr);

  {
    Mat Pt,SP=NULL;
    PetscInt MS,MPtSP;
    
    
    // also works also with -proj_f_pc_type jacobi
    ierr = MatMatMult(S,P,MAT_INITIAL_MATRIX,1.0,&SP);CHKERRQ(ierr);
    ierr = MatTranspose(P,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
    ierr = MatMatMult(Pt,SP,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);
    
    MatGetSize(S,&MS,NULL);
    MatGetSize(PtSP,&MPtSP,NULL);
    printf("[fine] S %d x %d | [coarse] Pt.S.P %d x %d\n",MS,MS,MPtSP,MPtSP);

    MatDestroy(&Pt);
    MatDestroy(&SP);
    
    ierr = PCAuxSpaceSetOperators_demo(pc,PtSP,PtSP);CHKERRQ(ierr);
  }
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceConfigure_Darcy2"
PetscErrorCode PCAuxSpaceConfigure_Darcy2(PDE pde,Mat A11,Mat A12,Mat A21,PC pc)
{  
  PDEDarcy *data;
  PetscInt pk;
  DM dm_p_dg,dm_p_cg;
  Mat P,PtSP;
  PetscErrorCode ierr;
  
  data = (PDEDarcy*)pde->data;
  dm_p_dg = data->dmp;
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-aux_pk",&pk,NULL);CHKERRQ(ierr);
  ierr = DMTetCloneGeometry(dm_p_dg,1,DMTET_CWARP_AND_BLEND,pk,&dm_p_cg);CHKERRQ(ierr);
  
  ierr = PCAuxSpaceSetDMS_demo(pc,dm_p_dg,dm_p_cg);CHKERRQ(ierr);
  
  ierr = DMTetAssembleProjection(dm_p_cg,dm_p_dg,&P);CHKERRQ(ierr);
  ierr = PCAuxSpaceSetProjection_demo(pc,P);CHKERRQ(ierr);
  
  {
    Mat A12P,Pt,A21_d_A12P;
    Vec diag;
    PetscInt MS,MPtSP;
    
    
    ierr = MatMatMult(A12,P,MAT_INITIAL_MATRIX,1.0,&A12P);CHKERRQ(ierr);
 
    ierr = MatCreateVecs(A11,&diag,NULL);CHKERRQ(ierr);
    ierr = MatGetDiagonal(A11,diag);CHKERRQ(ierr);
    ierr = VecReciprocal(diag);CHKERRQ(ierr);
    ierr = VecScale(diag,-1.0);CHKERRQ(ierr);
    ierr = MatDiagonalScale(A12P,diag,NULL);CHKERRQ(ierr);
    
    ierr = MatMatMult(A21,A12P,MAT_INITIAL_MATRIX,1.0,&A21_d_A12P);CHKERRQ(ierr);
    ierr = MatDestroy(&A12P);CHKERRQ(ierr);

    ierr = MatTranspose(P,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
    ierr = MatMatMult(Pt,A21_d_A12P,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);
    ierr = MatDestroy(&A21_d_A12P);CHKERRQ(ierr);
    ierr = MatDestroy(&Pt);CHKERRQ(ierr);
    
    MatGetSize(A21,&MS,NULL);
    MatGetSize(PtSP,&MPtSP,NULL);
    printf("[fine] S %d x %d | [coarse] Pt.S.P %d x %d\n",MS,MS,MPtSP,MPtSP);
    
    VecDestroy(&diag);
    
    ierr = PCAuxSpaceSetOperators_demo(pc,PtSP,PtSP);CHKERRQ(ierr);
  }
  
  
  PetscFunctionReturn(0);
}

PetscErrorCode AuxSolve_old(PDE pde,DM dmS,DM dmp,Mat A,Vec X,Vec F)
{
  PetscErrorCode ierr;
  Vec g,p,Xv,Xp,diag;
  Mat Buu,Bup,Bpu;
  Mat S;
  PC pc;
  KSP ksp;
  IS *is;
  
  ierr = DMCompositeGetAccess(dmS,F,&Xv,&Xp);CHKERRQ(ierr);
  ierr = VecDuplicate(Xp,&g);CHKERRQ(ierr);
  ierr = VecCopy(Xp,g);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(dmS,F,&Xv,&Xp);CHKERRQ(ierr);
  
  ierr = VecDuplicate(g,&p);CHKERRQ(ierr);

  ierr = DMCompositeGetGlobalISs(dmS,&is);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);

  ierr = MatCreateVecs(Buu,&diag,NULL);CHKERRQ(ierr);
  ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
  ierr = VecReciprocal(diag);CHKERRQ(ierr);
  
  ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
  ierr = MatMatMult(Bpu,Bup,MAT_INITIAL_MATRIX,1.0,&S);CHKERRQ(ierr);

  ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
  ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);

  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"aux_");CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,S,S);CHKERRQ(ierr);
  ierr = PCCreate_LSolve(pde,NULL,S,&pc);CHKERRQ(ierr);
  
  //
  ierr = KSPSetPC(ksp,pc);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  ierr = StokesUP_DG_AsciiVTU_g(dmp,g,"rhs.vtu");CHKERRQ(ierr);
  ierr = KSPSolve(ksp,g,p);CHKERRQ(ierr);
  ierr = StokesUP_DG_AsciiVTU_g(dmp,p,"p_dg.vtu");CHKERRQ(ierr);
  //
  
  //PCApply(pc,g,p);
  
  ierr = MatDestroy(&Buu);CHKERRQ(ierr);
  ierr = MatDestroy(&Bup);CHKERRQ(ierr);
  ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode AuxSolve(PDE pde,DM dmS,DM dmp,Mat A,Vec X,Vec F,Mat schur_mf)
{
  PetscErrorCode ierr;
  Vec g,p,Xv,Xp,diag;
  Mat Buu,Bup,Bpu;
  Mat S;
  PC pc;
  KSP ksp;
  IS *is;
  
  ierr = DMCompositeGetAccess(dmS,F,&Xv,&Xp);CHKERRQ(ierr);
  ierr = VecDuplicate(Xp,&g);CHKERRQ(ierr);
  ierr = VecCopy(Xp,g);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(dmS,F,&Xv,&Xp);CHKERRQ(ierr);
  
  ierr = VecDuplicate(g,&p);CHKERRQ(ierr);
  
  ierr = DMCompositeGetGlobalISs(dmS,&is);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
  
  ierr = MatCreateVecs(Buu,&diag,NULL);CHKERRQ(ierr);
  ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
  ierr = VecReciprocal(diag);CHKERRQ(ierr);
  
  ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
  ierr = MatMatMult(Bpu,Bup,MAT_INITIAL_MATRIX,1.0,&S);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)S,"S*_");CHKERRQ(ierr);
  
  ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
  ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
  
  ierr = MatScale(S,-1.0);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"aux_");CHKERRQ(ierr);
  if (schur_mf) { ierr = KSPSetOperators(ksp,schur_mf,S);CHKERRQ(ierr);
  } else {        ierr = KSPSetOperators(ksp,S,S);CHKERRQ(ierr); }
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  
  ierr = PCAuxSpaceConfigure_v2(pc);CHKERRQ(ierr);
  
  //ierr = PCAuxSpaceConfigure_Darcy(pde,S,pc);CHKERRQ(ierr);
  ierr = PCAuxSpaceConfigure_Darcy2(pde,Buu,Bup,Bpu,pc);CHKERRQ(ierr);
  
  ierr = StokesUP_DG_AsciiVTU_g(dmp,g,"rhs.vtu");CHKERRQ(ierr);
  ierr = KSPSolve(ksp,g,p);CHKERRQ(ierr);
  ierr = StokesUP_DG_AsciiVTU_g(dmp,p,"p_dg.vtu");CHKERRQ(ierr);
  //
  
  //PCApply(pc,g,p);
  
  ierr = MatDestroy(&Buu);CHKERRQ(ierr);
  ierr = MatDestroy(&Bup);CHKERRQ(ierr);
  ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode AuxSolve_petsc(PDE pde,DM dmS,DM dmp,Mat A,Vec X,Vec F,Mat schur_mf)
{
  PetscErrorCode ierr;
  Vec g,p,Xv,Xp,diag;
  Mat Buu,Bup,Bpu;
  Mat S;
  KSP ksp;
  IS *is;
  
  ierr = DMCompositeGetAccess(dmS,F,&Xv,&Xp);CHKERRQ(ierr);
  ierr = VecDuplicate(Xp,&g);CHKERRQ(ierr);
  ierr = VecCopy(Xp,g);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(dmS,F,&Xv,&Xp);CHKERRQ(ierr);
  
  ierr = VecDuplicate(g,&p);CHKERRQ(ierr);
  
  ierr = DMCompositeGetGlobalISs(dmS,&is);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
  ierr = MatGetSubMatrix(A,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
  
  ierr = MatCreateVecs(Buu,&diag,NULL);CHKERRQ(ierr);
  ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
  ierr = VecReciprocal(diag);CHKERRQ(ierr);
  
  ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
  ierr = MatMatMult(Bpu,Bup,MAT_INITIAL_MATRIX,1.0,&S);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)S,"S*_");CHKERRQ(ierr);
  
  ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
  ierr = MatDiagonalScale(Bup,diag,NULL);CHKERRQ(ierr);
  
  ierr = MatScale(S,-1.0);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"schur_");CHKERRQ(ierr);
  if (schur_mf) { ierr = KSPSetOperators(ksp,schur_mf,S);CHKERRQ(ierr);
  } else {        ierr = KSPSetOperators(ksp,S,S);CHKERRQ(ierr); }
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  Mat      P,PtSP;

  {
    PetscInt pk;
    DM       dm_p_cg;
    
    pk = 1;
    ierr = PetscOptionsGetInt(NULL,NULL,"-aux_pk",&pk,NULL);CHKERRQ(ierr);
    ierr = DMTetCloneGeometry(dmp,1,DMTET_CWARP_AND_BLEND,pk,&dm_p_cg);CHKERRQ(ierr);
    
    ierr = PetscObjectCompose((PetscObject)S,"PCAuxSpace_DMNatural",(PetscObject)dmp);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject)S,"PCAuxSpace_DMAux",(PetscObject)dm_p_cg);CHKERRQ(ierr);
    
    ierr = DMTetAssembleProjection(dm_p_cg,dmp,&P);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject)S,"PCAuxSpace_P",(PetscObject)P);CHKERRQ(ierr);

    ierr = DMDestroy(&dm_p_cg);CHKERRQ(ierr);
  }
  
  {
    Mat A12P,Pt,A21_d_A12P;
    Vec diag;
    PetscInt MS,MPtSP;
    
    
    ierr = MatMatMult(Bup,P,MAT_INITIAL_MATRIX,1.0,&A12P);CHKERRQ(ierr);
    
    ierr = MatCreateVecs(Buu,&diag,NULL);CHKERRQ(ierr);
    ierr = MatGetDiagonal(Buu,diag);CHKERRQ(ierr);
    ierr = VecReciprocal(diag);CHKERRQ(ierr);
    ierr = VecScale(diag,-1.0);CHKERRQ(ierr);
    ierr = MatDiagonalScale(A12P,diag,NULL);CHKERRQ(ierr);
    
    ierr = MatMatMult(Bpu,A12P,MAT_INITIAL_MATRIX,1.0,&A21_d_A12P);CHKERRQ(ierr);
    ierr = MatDestroy(&A12P);CHKERRQ(ierr);
    
    ierr = MatTranspose(P,MAT_INITIAL_MATRIX,&Pt);CHKERRQ(ierr);
    ierr = MatMatMult(Pt,A21_d_A12P,MAT_INITIAL_MATRIX,1.0,&PtSP);CHKERRQ(ierr);
    ierr = MatDestroy(&A21_d_A12P);CHKERRQ(ierr);
    ierr = MatDestroy(&Pt);CHKERRQ(ierr);
    
    MatGetSize(Bpu,&MS,NULL);
    MatGetSize(PtSP,&MPtSP,NULL);
    printf("[fine] S %d x %d | [coarse] Pt.S.P %d x %d\n",MS,MS,MPtSP,MPtSP);
    
    VecDestroy(&diag);
    
    //ierr = PCAuxSpaceSetOperators_demo(pc,PtSP,PtSP);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject)S,"PCAuxSpace_PtAP",(PetscObject)PtSP);CHKERRQ(ierr);
  }
  
  ierr = StokesUP_DG_AsciiVTU_g(dmp,g,"rhs.vtu");CHKERRQ(ierr);
  ierr = KSPSolve(ksp,g,p);CHKERRQ(ierr);
  ierr = StokesUP_DG_AsciiVTU_g(dmp,p,"p_dg.vtu");CHKERRQ(ierr);
  //
  
  //PCApply(pc,g,p);
  
  ierr = MatDestroy(&Buu);CHKERRQ(ierr);
  ierr = MatDestroy(&Bup);CHKERRQ(ierr);
  ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* -------------------------------- */
/*
./arch-gnu-c-opt/bin/test_pde_darcy_mms.app -mxy 24 -dcy_ksp_type fgmres  -dcy_ksp_rtol 1.0e-10 -dcy_ksp_converged_reason -dcy_fieldsplit_u_pc_type lu -dcy_ksp_type fgmres -dcy_fieldsplit_p_ksp_type fgmres -dcy_fieldsplit_p_pc_type none  -dcy_ksp_rtol 1.0e-6 -mxy 12 -meshes 2 -view   -dcy_snes_type newtonls -dcy_ksp_monitor -dcy_pc_fieldsplit_type SCHUR -dcy_snes_type ksponly -dcy_ksp_viewx -dcy_fieldsplit_p_ksp_rtol 1.0e-6 -dcy_fieldsplit_p_ksp_converged_reason -dcy_fieldsplit_p_ksp_viewx -dcy_fieldsplit_p_ksp_max_it 2000  -mxy 8   -meshes 3 -dcy_pc_fieldsplit_schur_precondition SELFP -dcy_fieldsplit_p_pc_type lu   -dcy_pc_fieldsplit_schur_fact_type UPPER -dcy_fieldsplit_u_pc_type ilu -dcy_fieldsplit_u_ksp_type cg -dcy_fieldsplit_u_ksp_rtol 1.0e-6 -dcy_fieldsplit_p_ksp_type preonly -meshes 1 -mxy 40 -dcy_fieldsplit_p_ksp_rtol 1.0e-2  -dcy_fieldsplit_p_ksp_type fgmres -dcy_ksp_viewx -dcy_fieldsplit_u_pc_type ilu -dcy_pc_fieldsplit_schur_fact_type UPPER -dcy_fieldsplit_p_ksp_max_it 100 -pk 1 -dcy_fieldsplit_p_ksp_monitor_true_residual -dcy_fieldsplit_p_pc_type gamg -dcy_fieldsplit_u_ksp_type preonly -dcy_fieldsplit_p_ksp_type preonly  -mxy 8 -dcy_fieldsplit_u_pc_type jacobi  -dcy_ksp_max_it 4 -aux_ksp_monitor_true_residual -L_pc_type lu -aux_ksp_type fgmres -L_ksp_type cg -aux_ksp_converged_reason -L_ksp_monitor  -proj_f_pc_type jacobi -proj_f_ksp_type preonly  -L_pc_type lu -L_ksp_monitor -L_ksp_type fgmres -aux_ksp_max_it 200 -mxy 12 -dcy_ksp_view -dcy_fieldsplit_p_pc_type jacobi -dcy_pc_type none -mxy 30 -L_ksp_type preonly -aux_ksp_view -pk 1 -aux_pk 1 -L_pc_type gamg -mxy 24
*/

/*
 [Test]
 
 ./arch-gnu-c-opt/bin/test_pde_darcy_mms.app -dcy_ksp_type fgmres  -dcy_ksp_rtol 1.0e-10 -dcy_ksp_converged_reason -dcy_fieldsplit_u_pc_type lu -dcy_ksp_type fgmres -dcy_fieldsplit_p_ksp_type fgmres  -dcy_fieldsplit_p_ksp_max_it 30 -dcy_fieldsplit_p_ksp_rtol 1.0e-2 -dcy_fieldsplit_p_pc_type lu -test_id 4 -mxy 12 -meshes 3 -dcy_pc_fieldsplit_schur_precondition SELFP

 # Darcy mesh 012 x 012 : element: P2-P1{disc} [Hdiv-L2]
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 5
 # degree  01 8.3333e-02 | hdiv 6.414217234590e+01 | hdiv_Linf 1.359380733158e+02
 # Darcy mesh 024 x 024 : element: P2-P1{disc} [Hdiv-L2]
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 5
 # degree  01 4.1667e-02 | hdiv 6.415177680854e+01 | hdiv_Linf 1.304287771015e+02
 # Darcy mesh 048 x 048 : element: P2-P1{disc} [Hdiv-L2]
 Linear dcy_ solve converged due to CONVERGED_RTOL iterations 5
 # degree  01 2.0833e-02 | hdiv 6.415238769870e+01 | hdiv_Linf 1.288498329136e+02
 level          h              u(L2)              p(L2)              p(H1)            rates
 ------------------------------------------------------------------------------- | -----------------------------------------------------------
 [mesh 0000] 8.3333e-02 2.125687156986e-02 8.931135094198e-03 1.305934730573e+00 |
 [mesh 0001] 4.1667e-02 3.016169461309e-03 2.253069491510e-03 6.614564406549e-01 | +2.817139894460e+00 +1.986951733607e+00 +9.813647361460e-01
 [mesh 0002] 2.0833e-02 3.974297326252e-04 5.645974396041e-04 3.319352986684e-01 | +2.923945774620e+00 +1.996597319929e+00 +9.947440961739e-01
*/
/*
 [Test]

 ./arch-gnu-c-opt/bin/test_pde_darcy_mms.app -mxy 24 -pk 1 -test_id 4
 -meshes 1
 -dcy_ksp_type fgmres
 -dcy_ksp_rtol 1.0e-10
 -dcy_pc_type fieldsplit
 -dcy_pc_fieldsplit_schur_precondition USER
 -dcy_fieldsplit_u_ksp_type preonly
 -dcy_fieldsplit_u_pc_type jacobi
 -dcy_fieldsplit_p_ksp_type preonly
 -dcy_fieldsplit_p_pc_type auxspace
 -aux_pk 1
 -dcy_fieldsplit_p_pc_auxspace_presmooth_it 5
 -dcy_fieldsplit_p_sub_pc_type gamg
 -dcy_ksp_monitor_true_residual
 -dcy_ksp_view
 -log_view
*/
#undef __FUNCT__
#define __FUNCT__ "pde_darcyHdivL2_mixedbc_mms"
PetscErrorCode pde_darcyHdivL2_mixedbc_mms(PetscBool use_sv,PetscInt mxy,PetscReal *h,PetscReal *uL2,PetscReal *pL2,PetscReal *pH1)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcV;
  SNES snes;
  PetscBool view = PETSC_FALSE;
  PetscInt pk;
  MMSCtx ctx;
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Darcy mesh %.3D x %.3D : element: P%D-P%D^{disc} [Hdiv-L2]\n",mxy,mxy,pk+1,pk);
  
  if (!use_sv) {
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pk+1,&dmV);CHKERRQ(ierr);
    ierr = DMTetBFacetSetup(dmV);CHKERRQ(ierr); /* required for surface integration p.n */
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,pk,&dmP);CHKERRQ(ierr);
    //ierr = DMTetBFacetSetup(dmP);CHKERRQ(ierr); /* required for surface integration u.n */
  } else {
    DM dmV_sv,dmP_sv;
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pk+1,&dmV);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dmV,1,&dmV_sv);CHKERRQ(ierr);
    ierr = DMTetBFacetSetup(dmV_sv);CHKERRQ(ierr); /* required for surface integration p.n */
    ierr = DMTetBFacetSetup(dmV_sv);CHKERRQ(ierr); /* required for surface integration p.n */
    ierr = DMDestroy(&dmV);CHKERRQ(ierr);
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_DG,pk,&dmP);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dmP,1,&dmP_sv);CHKERRQ(ierr);
    //ierr = DMTetBFacetSetup(dmP_sv);CHKERRQ(ierr); /* required for surface integration u.n */
    ierr = DMDestroy(&dmP);CHKERRQ(ierr);

    dmV = dmV_sv;
    dmP = dmP_sv;
  }
  ctx.dm  = dmV;
  ctx.dmv = dmV;
  ctx.dmp = dmP;
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_UseMMS_Vdotn,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_UseMMS_Vdotn,NULL);CHKERRQ(ierr);
  
  /* pde for darcy */
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,dmV,dmP,bcV,NULL,DARCY_BFORM_HDIVL2,&pde);CHKERRQ(ierr);
  ctx.pde = pde;
  ierr = PDESetOptionsPrefix(pde,"dcy_");CHKERRQ(ierr);
  
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  /* volume coefficients */
  {
    Coefficient a,f,g;
    
    ierr = PDEDarcyGetVCoefficients(pde,&a,&f,&g);CHKERRQ(ierr);
    
    /* inverse permability */
    ierr = CoefficientSetComputeQuadrature(a,a_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    if (view) {
      ierr = CoefficientEvaluate(a);CHKERRQ(ierr);
      //ierr = CoefficientView_GP(a,dmP,"a-qp");CHKERRQ(ierr);
    }
    
    /* u-momentum right hand side */
    ierr = CoefficientSetComputeQuadrature(f,f_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    if (view) {
      ierr = CoefficientEvaluate(f);CHKERRQ(ierr);
      //ierr = CoefficientView_GP(f,dmP,"f-qp");CHKERRQ(ierr);
    }
    
    /* p-continuity right hand side */
    ierr = CoefficientSetComputeQuadrature(g,g_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    if (view) {
      ierr = CoefficientEvaluate(g);CHKERRQ(ierr);
      //ierr = CoefficientView_GP(g,dmP,"g-qp");CHKERRQ(ierr);
    }
  }
  /* surface coefficients */
  {
    Coefficient uN,pN;
    
    ierr = PDEDarcyGetSCoefficients(pde,&uN,&pN);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(pN,p_neumann_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
  }
  
  /* stokes solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(pde,snes);CHKERRQ(ierr);
  
  /*ierr = AuxSolve(pde,dmS,dmP,J,X,F,NULL);CHKERRQ(ierr);*/
  
  {
    Vec Xv,Xp;
    PetscReal Eu[Q_NUM_ENTRIES];
    PetscReal Ep[Q_NUM_ENTRIES],p_avg;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Eu,dmV,Xv,
                         DarcySolution_EvaluateV,
                         NULL,NULL);CHKERRQ(ierr);
    
    ierr = IntegrateField(&p_avg,dmP,Xp);CHKERRQ(ierr);
    
    ierr = VecShift(Xp,-p_avg);CHKERRQ(ierr);
    
    ierr = ComputeINorms(Ep,dmP,Xp,
                         DarcySolution_EvaluateP,
                         DarcySolution_EvaluateGradientP,NULL);CHKERRQ(ierr);
    
    {
      PetscReal Enorms[5];
      
      ierr = IntegrateFieldHdiv(Enorms,dmV,Xv);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"# degree  %.2D %1.4e | hdiv %1.12e | hdiv_Linf %1.12e\n",pk,1.0/((PetscReal)mxy),Enorms[3],Enorms[4]);
      
    }

    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    
    //PetscPrintf(PETSC_COMM_WORLD,"degree  %.2D %1.4e  %1.12e %1.12e %1.12e\n",pk,1.0/((PetscReal)mxy),Eu[Q_L2],Eu[Q_H1],Ep[Q_L2]);
    
    *h = 1.0/((PetscReal)mxy);
    *uL2 = Eu[Q_L2];
    *pL2 = Ep[Q_L2];
    *pH1 = Ep[Q_H1];
  }
  
  if (view) {
    ierr = PDEDarcyFieldView_AsciiVTU(pde,NULL);CHKERRQ(ierr);
    
    {
      DM dmVp;
      Vec Xv,Xp,vs = NULL;
      const char *fields[] = { "pvs" };
      char       prefix[PETSC_MAX_PATH_LEN];
      
      ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                         2,DMTET_CWARP_AND_BLEND,pk,&dmVp);CHKERRQ(ierr);
      
      ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
      ierr = DMTetProjectField(dmV,Xv,PRJTYPE_NATIVE,dmVp,&vs);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
      
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s_",fields[0]);CHKERRQ(ierr);
      ierr = DMTetViewFields_VTU(dmVp,1,&vs,fields,prefix);CHKERRQ(ierr);
      
      ierr = DMDestroy(&dmVp);CHKERRQ(ierr);
      ierr = VecDestroy(&vs);CHKERRQ(ierr);
    }
  }
  
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pde_darcyHdivL2_mixedbc_mms_aux_demo"
PetscErrorCode pde_darcyHdivL2_mixedbc_mms_aux_demo(PetscBool use_sv,PetscInt mxy,PetscReal *h,PetscReal *uL2,PetscReal *pL2,PetscReal *pH1)
{
  PetscErrorCode ierr;
  DM dmV,dmP,dmS;
  Vec X,F;
  Mat J;
  PDE pde;
  BCList bcV;
  SNES snes;
  PetscBool view = PETSC_FALSE;
  PetscInt pk;
  MMSCtx ctx;
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-view",&view,NULL);CHKERRQ(ierr);
  
  pk = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_WORLD,"# Darcy mesh %.3D x %.3D : element: P%D-P%D [Hdiv-L2]\n",mxy,mxy,pk+1,pk);
  
  if (!use_sv) {
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pk+1,&dmV);CHKERRQ(ierr);
    ierr = DMTetBFacetSetup(dmV);CHKERRQ(ierr); /* required for surface integration p.n */
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,pk,&dmP);CHKERRQ(ierr);
    //ierr = DMTetBFacetSetup(dmP);CHKERRQ(ierr); /* required for surface integration u.n */
  } else {
    DM dmV_sv,dmP_sv;
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       2,DMTET_CWARP_AND_BLEND,pk+1,&dmV);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dmV,1,&dmV_sv);CHKERRQ(ierr);
    ierr = DMTetBFacetSetup(dmV_sv);CHKERRQ(ierr); /* required for surface integration p.n */
    ierr = DMTetBFacetSetup(dmV_sv);CHKERRQ(ierr); /* required for surface integration p.n */
    ierr = DMDestroy(&dmV);CHKERRQ(ierr);
    
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, mxy,mxy, PETSC_TRUE,
                                       1,DMTET_DG,pk,&dmP);CHKERRQ(ierr);
    ierr = DMTetCreateRefinement(dmP,1,&dmP_sv);CHKERRQ(ierr);
    //ierr = DMTetBFacetSetup(dmP_sv);CHKERRQ(ierr); /* required for surface integration u.n */
    ierr = DMDestroy(&dmP);CHKERRQ(ierr);
    
    dmV = dmV_sv;
    dmP = dmP_sv;
  }
  ctx.dm  = dmV;
  ctx.dmv = dmV;
  ctx.dmp = dmP;
  
  /* Define Dirichlet bc */
  ierr = DMBCListCreate(dmV,&bcV);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverse(bcV,0,EvalDirichletBoundary_UseMMS_Vdotn,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bcV,1,EvalDirichletBoundary_UseMMS_Vdotn,NULL);CHKERRQ(ierr);
  
  /* pde for darcy */
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,dmV,dmP,bcV,NULL,DARCY_BFORM_HDIVL2,&pde);CHKERRQ(ierr);
  ctx.pde = pde;
  ierr = PDESetOptionsPrefix(pde,"dcy_");CHKERRQ(ierr);
  
  ierr = PDEDarcyGetDM(pde,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&J);CHKERRQ(ierr);
  ierr = MatCreateVecs(J,&X,&F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde,X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(dmV,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(dmP,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }
  /* boundary condition for V */
  {
    Vec Xv,Xp;
    
    ierr = DMCompositeGetAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(bcV,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,X,&Xv,&Xp);CHKERRQ(ierr);
  }
  
  /* volume coefficients */
  {
    Coefficient a,f,g;
    
    ierr = PDEDarcyGetVCoefficients(pde,&a,&f,&g);CHKERRQ(ierr);
    
    /* inverse permability */
    ierr = CoefficientSetComputeQuadrature(a,a_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    
    /* u-momentum right hand side */
    ierr = CoefficientSetComputeQuadrature(f,f_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
    
    /* p-continuity right hand side */
    ierr = CoefficientSetComputeQuadrature(g,g_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
  }
  /* surface coefficients */
  {
    Coefficient uN,pN;
    
    ierr = PDEDarcyGetSCoefficients(pde,&uN,&pN);CHKERRQ(ierr);
    ierr = CoefficientSetComputeQuadrature(pN,p_neumann_evaluate_quadrature,NULL,(void*)&ctx);CHKERRQ(ierr);
  }
  
  /* stokes solver */
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde,snes,F,J,J);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  /* force solver setup */
  ierr = SNESSetUp(snes);CHKERRQ(ierr);
  ierr = SNESComputeFunction(snes,X,F);CHKERRQ(ierr);
  ierr = SNESComputeJacobian(snes,X,J,J);CHKERRQ(ierr);
  
  Mat schur_mf = NULL;
  {
    PC pc;
    PetscInt nsplits;
    KSP ksp,*subksp;
    
    SNESGetKSP(snes,&ksp);
    KSPSetUp(ksp);
    ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
    ierr = PCFieldSplitGetSubKSP(pc,&nsplits,&subksp);CHKERRQ(ierr);
    KSPGetOperators(subksp[1],&schur_mf,NULL);
  }
  
  /* perform auxillary space solve (demo) */
  //ierr = AuxSolve(pde,dmS,dmP,J,X,F,schur_mf);CHKERRQ(ierr);
  ierr = AuxSolve_petsc(pde,dmS,dmP,J,X,F,schur_mf);CHKERRQ(ierr);
  
  *h = 1.0/((PetscReal)mxy);
  *uL2 = 1.0;
  *pL2 = 1.0;
  *pH1 = 1.0;

  ierr = BCListDestroy(&bcV);CHKERRQ(ierr);
  ierr = DMDestroy(&dmP);CHKERRQ(ierr);
  ierr = DMDestroy(&dmV);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscInt mxy,L,k,m,tid;
  PetscReal *res_h,*u_L2,*p_L2,*p_H1;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);

  mxy = 6;
  ierr = PetscOptionsGetInt(NULL,NULL,"-mxy",&mxy,NULL);CHKERRQ(ierr);

  L = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-meshes",&L,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&res_h);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&u_L2);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&p_L2);CHKERRQ(ierr);
  ierr = PetscMalloc1(L,&p_H1);CHKERRQ(ierr);

  tid = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-test_id",&tid,NULL);CHKERRQ(ierr);

  m = mxy;
  for (k=0; k<L; k++) {
    if (tid == 1) { ierr = pde_darcyL2H1_mms(m,PETSC_FALSE,&res_h[k],&u_L2[k],&p_L2[k],&p_H1[k]);CHKERRQ(ierr); }
    else if (tid == 2) { ierr = pde_darcyL2H1_mms(m,PETSC_TRUE,&res_h[k],&u_L2[k],&p_L2[k],&p_H1[k]);CHKERRQ(ierr); }
    else if (tid == 3) { ierr = pde_darcyL2H1_mixedbc_mms(m,&res_h[k],&u_L2[k],&p_L2[k],&p_H1[k]);CHKERRQ(ierr); }
    else if (tid == 4) { ierr = pde_darcyHdivL2_mixedbc_mms(PETSC_TRUE,m,&res_h[k],&u_L2[k],&p_L2[k],&p_H1[k]);CHKERRQ(ierr); }
    else if (tid == 5) { ierr = pde_darcyHdivL2_mixedbc_mms_aux_demo(PETSC_TRUE,m,&res_h[k],&u_L2[k],&p_L2[k],&p_H1[k]);CHKERRQ(ierr); }
    else SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown value for -test_id found. Use {1,2,3,4,5}");
    m = m * 2;
  }

  PetscPrintf(PETSC_COMM_WORLD,"      level          h              u(L2)              p(L2)              p(H1)            rates\n");
  PetscPrintf(PETSC_COMM_WORLD,"------------------------------------------------------------------------------- | -----------------------------------------------------------\n");
  for (k=0; k<L; k++) {
    PetscPrintf(PETSC_COMM_WORLD,"[mesh %.4D] %1.4e %1.12e %1.12e %1.12e |",k,res_h[k],u_L2[k],p_L2[k],p_H1[k]);
    if (k > 0) {
      PetscReal ruL2,rpL2,rpH1;
      
      ruL2 = log10(u_L2[k]/u_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
      rpL2 = log10(p_L2[k]/p_L2[k-1]) / log10(res_h[k]/res_h[k-1]);
      rpH1 = log10(p_H1[k]/p_H1[k-1]) / log10(res_h[k]/res_h[k-1]);
      
      PetscPrintf(PETSC_COMM_WORLD," %+1.12e %+1.12e %+1.12e",ruL2,rpL2,rpH1);
    }
    PetscPrintf(PETSC_COMM_WORLD,"\n");
  }

  ierr = PetscFree(res_h);CHKERRQ(ierr);
  ierr = PetscFree(u_L2);CHKERRQ(ierr);
  ierr = PetscFree(p_L2);CHKERRQ(ierr);
  ierr = PetscFree(p_H1);CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return(0);
}


