
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <proj_init_finalize.h>
#include <pde.h>
#include <mpiio_blocking.h>

/*
 [Test]
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmproject.app -test_id 1
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmproject.app -test_id 2
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmproject.app -test_id 3
 ${PETSC_DIR}/arch-gnu-c-debug/bin/mpiexec -n 3 ./arch-gnu-c-debug/bin/test_dmproject.app -test_id 4
*/

#undef __FUNCT__
#define __FUNCT__ "func1"
PetscErrorCode func1(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = PetscSinReal(PETSC_PI * x[0]) * x[1] * PetscCosReal(3.0 * PETSC_PI * x[0] * x[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func2"
PetscErrorCode func2(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = PetscSinReal(4.4 * PETSC_PI * x[0]) + 3.3;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "func3"
PetscErrorCode func3(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = x[0] * x[1] * x[1] + 2.0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "poly_p3"
PetscErrorCode poly_p3(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = 2.0 + x[0] + x[0]*x[1] + x[0]*x[0]*x[0] + 0.33 * x[1]*x[1]*x[1] + 0.11 * x[0]*x[1]*x[1];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "poly_p3_vec"
PetscErrorCode poly_p3_vec(PetscScalar x[],PetscScalar v[],void *ctx)
{
  v[0] = 2.0 + x[0]     + x[0]*x[1]     + x[0]*x[0]*x[0]     + 0.33 * x[1]*x[1]*x[1] + 0.11 * x[0]*x[1]*x[1];
  v[1] =       1.2*x[0] - 3.3*x[0]*x[1] + 1.4*x[0]*x[0]*x[0] - 5.33 * x[1]*x[1]*x[1] + 2.11 * x[0]*x[1]*x[1];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_project_s_to_s"
PetscErrorCode test_project_s_to_s(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT,dmF;
  const char *fieldsT[] = { "T" };
  char prefix[128];
  Vec XT = NULL,XF = NULL;
  
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);

  ierr = DMTetCloneGeometry(dmT,1,DMTET_CWARP_AND_BLEND,T_p+2,&dmF);CHKERRQ(ierr);
  //ierr = DMTetCreateSharedGeometry(dmT,1,DMTET_CWARP_AND_BLEND,T_p+2,&dmF);CHKERRQ(ierr); /* also valid */
  ierr = DMSetUp(dmF);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmT,&XT);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dmF,&XF);CHKERRQ(ierr);

  /* interpolate from sub to parent */
  ierr = DMTetVecTraverse(dmF,XF,poly_p3,NULL);CHKERRQ(ierr);
  ierr = VecSet(XT,-20.0);CHKERRQ(ierr);

  ierr = DMTetProjectField(dmF,XF,PRJTYPE_NATIVE,dmT,&XT);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_from_",fieldsT[0]);
  ierr = DMTetViewFieldsPV(dmF,1,&XF,PETSC_FALSE,fieldsT,prefix);CHKERRQ(ierr);

  PetscSNPrintf(prefix,127,"%s_to_",fieldsT[0]);
  ierr = DMTetViewFieldsPV(dmT,1,&XT,PETSC_TRUE,fieldsT,prefix);CHKERRQ(ierr);

  //ierr = DMTetViewFieldsXDMF(dmT,13.33,1,&XT,fieldsT,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFieldsXDMFBinary(dmT,13.33,1,&XT,fieldsT,NULL,prefix,PETSC_FALSE);CHKERRQ(ierr);
  
  /*
  {
    Vec fields[] = { XT, XT };
    const char *field_names[] = { "f1","f2" };
    ierr = DMTetViewFieldsXDMF(dmT,13.33,2,fields,field_names,prefix);CHKERRQ(ierr);
  }
  */
  
  /* clean up */
  ierr = VecDestroy(&XF);CHKERRQ(ierr);
  ierr = VecDestroy(&XT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmF);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_project_s_to_v"
PetscErrorCode test_project_s_to_v(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT,dmTvec,dmF;
  const char *fieldsT[] = { "T", "gradT" };
  char prefix[128];
  Vec XT = NULL,XF = NULL;
  
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   1,DMTET_CWARP_AND_BLEND,T_p,&dmT);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);

  ierr = DMTetCloneGeometry(dmT,1,DMTET_CWARP_AND_BLEND,T_p,&dmF);CHKERRQ(ierr);
  ierr = DMSetUp(dmF);CHKERRQ(ierr);
  
  ierr = DMTetCreateSharedSpace(dmT,2,&dmTvec);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmF,&XF);CHKERRQ(ierr);
  
  /* interpolate from sub to parent */
  ierr = DMTetVecTraverse(dmF,XF,poly_p3,NULL);CHKERRQ(ierr);
  
  ierr = DMTetProjectField(dmF,XF,PRJTYPE_GRADIENT,dmTvec,&XT);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_from_",fieldsT[0]);
  ierr = DMTetViewFieldsPV(dmF,1,&XF,PETSC_FALSE,&fieldsT[0],prefix);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_to_",fieldsT[1]);
  ierr = DMTetViewFieldsPV(dmTvec,1,&XT,PETSC_FALSE,&fieldsT[1],prefix);CHKERRQ(ierr);
  
  //ierr = DMTetViewFieldsXDMF(dmTvec,16.4,1,&XT,&fieldsT[1],prefix);CHKERRQ(ierr);
  ierr = DMTetViewFieldsXDMFBinary(dmTvec,16.4,1,&XT,&fieldsT[1],NULL,prefix,PETSC_FALSE);CHKERRQ(ierr);

  /* clean up */
  ierr = VecDestroy(&XF);CHKERRQ(ierr);
  ierr = VecDestroy(&XT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmF);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmTvec);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_project_v_to_div"
PetscErrorCode test_project_v_to_div(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT,dmFvec;
  const char *fields[] = { "vec", "div" };
  char prefix[128];
  Vec XT = NULL,XF = NULL;
  
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   2,DMTET_CWARP_AND_BLEND,T_p,&dmFvec);CHKERRQ(ierr);
  ierr = DMSetUp(dmFvec);CHKERRQ(ierr);
  
  ierr = DMTetCreateSharedSpace(dmFvec,1,&dmT);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmFvec,&XF);CHKERRQ(ierr);
  
  /* interpolate from sub to parent */
  ierr = DMTetVecTraverse(dmFvec,XF,poly_p3_vec,NULL);CHKERRQ(ierr);
  
  ierr = DMTetProjectField(dmFvec,XF,PRJTYPE_DIVERGENCE,dmT,&XT);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_from_",fields[0]);
  ierr = DMTetViewFieldsPV(dmFvec,1,&XF,PETSC_FALSE,&fields[0],prefix);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_to_",fields[1]);
  ierr = DMTetViewFieldsPV(dmT,1,&XT,PETSC_FALSE,&fields[1],prefix);CHKERRQ(ierr);
  
  /* clean up */
  ierr = VecDestroy(&XF);CHKERRQ(ierr);
  ierr = VecDestroy(&XT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmFvec);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_project_v_to_gradvv"
PetscErrorCode test_project_v_to_gradvv(void)
{
  PetscErrorCode ierr;
  PetscInt T_p;
  DM dmT,dmFvec;
  const char *fields[] = { "vec", "grad" };
  char prefix[128];
  Vec XT = NULL,XF = NULL;
  PetscMPIInt commrank;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  
  T_p = 2;
  PetscOptionsGetInt(NULL,NULL,"-T_p",&T_p,NULL);
  
  
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                   2,DMTET_CWARP_AND_BLEND,T_p,&dmFvec);CHKERRQ(ierr);
  ierr = DMSetUp(dmFvec);CHKERRQ(ierr);
  
/*
  {
  dmFvec_s = NULL;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);CHKERRQ(ierr);
  dmFvec_s = NULL;
  if (commrank == 0) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/subduction-1a/mesh-r1/wedge.1",
                                     2,DMTET_CWARP_AND_BLEND,T_p,&dmFvec_s);CHKERRQ(ierr);
    ierr = DMSetUp(dmFvec_s);CHKERRQ(ierr);
  }
  ierr = DMTetCreatePartitioned(PETSC_COMM_WORLD,dmFvec_s,2,DMTET_CWARP_AND_BLEND,T_p,&dmFvec);CHKERRQ(ierr);
  ierr = DMDestroy(&dmFvec_s);CHKERRQ(ierr);
  ierr = DMSetUp(dmFvec);CHKERRQ(ierr);
  }
*/

  ierr = DMTetCreateSharedSpace(dmFvec,4,&dmT);CHKERRQ(ierr);
  ierr = DMSetUp(dmT);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dmFvec,&XF);CHKERRQ(ierr);
  
  /* interpolate from sub to parent */
  ierr = DMTetVecTraverse(dmFvec,XF,poly_p3_vec,NULL);CHKERRQ(ierr);
  
  ierr = DMTetProjectField(dmFvec,XF,PRJTYPE_GRADIENT,dmT,&XT);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_from_",fields[0]);
  ierr = DMTetViewFields_VTU(dmFvec,1,&XF,&fields[0],prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmFvec,1,&XF,&fields[0],prefix);CHKERRQ(ierr);
  
  PetscSNPrintf(prefix,127,"%s_to_",fields[1]);
  ierr = DMTetViewFields_VTU(dmT,1,&XT,&fields[1],prefix);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU(dmT,1,&XT,&fields[1],prefix);CHKERRQ(ierr);

  /* Test viewing of multiple vectors of different DOF defined on the same DM */
  /*
  {
    Vec vec_pack[] = {XF,XT};
    PetscSNPrintf(prefix,127,"project_");
    ierr = DMTetViewFields_VTU(dmFvec,2,vec_pack,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dmFvec,2,vec_pack,fields,prefix);CHKERRQ(ierr);
  }
  */
  
  //ierr = DMTetViewFieldsXDMF(dmT,16.4,1,&XT,&fields[1],prefix);CHKERRQ(ierr);
  ierr = DMTetViewFieldsXDMFBinary(dmT,16.4,1,&XT,&fields[1],NULL,prefix,PETSC_FALSE);CHKERRQ(ierr);

  /*
  ierr = DMTetViewMeshTopologyCommonXDMFBinary(dmT,"output/");CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"step0_",fields[1]);
  ierr = DMTetViewFieldsXDMFBinary(dmT,18.4,1,&XT,&fields[1],"output/",prefix,PETSC_TRUE);CHKERRQ(ierr);
  PetscSNPrintf(prefix,127,"step1_",fields[1]);
  ierr = DMTetViewFieldsXDMFBinary(dmT,20.4,1,&XT,&fields[1],"output/",prefix,PETSC_TRUE);CHKERRQ(ierr);
  //Not sure how best to support the following use case
  //PetscSNPrintf(prefix,127,"step2_",fields[1]);
  //ierr = DMTetViewFieldsXDMFBinary(dmT,24.4,1,&XT,&fields[1],"output/step2",prefix,PETSC_TRUE);CHKERRQ(ierr);
  */
   
  /* clean up */
  ierr = VecDestroy(&XF);CHKERRQ(ierr);
  ierr = VecDestroy(&XT);CHKERRQ(ierr);
  ierr = DMDestroy(&dmFvec);CHKERRQ(ierr);
  ierr = DMDestroy(&dmT);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  PetscInt test_id = 1;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-test_id",&test_id,NULL);CHKERRQ(ierr);
  switch (test_id) {
    case 1:
      ierr = test_project_s_to_s();CHKERRQ(ierr);
      break;
    case 2:
      ierr = test_project_s_to_v();CHKERRQ(ierr);
      break;
    case 3:
      ierr = test_project_v_to_div();CHKERRQ(ierr);
      break;
    case 4:
      ierr = test_project_v_to_gradvv();CHKERRQ(ierr);
      break;
    case 5:
    {
      PetscInt nen,bpe,*element,npe,nvert;
      PetscReal *coords;
      FILE *fp;
      PetscMPIInt rank;
      DM dmtet;
      
      ierr = DMTetCreatePartitionedSquareGeometry(PETSC_COMM_WORLD,0.0,1.0,0.0,1.0, 2,3, PETSC_TRUE,
                                                  1,DMTET_CWARP_AND_BLEND,1,&dmtet);CHKERRQ(ierr);
      
      
      ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
      
      ierr = DMTetSpaceElement(dmtet,&nen,&bpe,&element,&npe,&nvert,&coords);CHKERRQ(ierr);
      printf("nen %d\n",nen);
      printf("bpe %d\n",bpe);
      printf("nvert %d\n",nvert);
      
      fp = NULL;
      if (rank == 0) { fp = fopen("geom.dat","w"); }
      ierr = MPIWrite_Blocking_PetscReal(fp,coords,2*nvert,0,PETSC_TRUE,PETSC_FALSE,PETSC_COMM_WORLD);CHKERRQ(ierr);
      if (rank == 0) { fclose(fp); }
      
      fp = NULL;
      if (rank == 0) { fp = fopen("topo.dat","w"); }
      ierr = MPIWrite_Blocking_PetscInt(fp,element,3*nen,0,PETSC_TRUE,PETSC_FALSE,PETSC_COMM_WORLD);CHKERRQ(ierr);
      if (rank == 0) { fclose(fp); }
      
    }
      break;
    default:
      break;
  }
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

