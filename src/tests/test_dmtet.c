
#include <petsc.h>
#include <petscdmtet.h>
#include <dmbcs.h>
#include <proj_init_finalize.h>

PetscErrorCode DMTetViewBasisFunctions2d_CWARPANDBLEND(DM dm);

#undef __FUNCT__
#define __FUNCT__ "example1"
PetscErrorCode example1(void)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);

  ierr = DMTetViewBasisFunctions2d_CWARPANDBLEND(dm);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"span x: %+1.4e %+1.4e\n",gmin[0],gmax[0]);
  PetscPrintf(PETSC_COMM_WORLD,"span y: %+1.4e %+1.4e\n",gmin[1],gmax[1]);
  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example2"
PetscErrorCode example2(void)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-0.1,1.1,-3.0,0.0,32,32,PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"span x: %+1.4e %+1.4e\n",gmin[0],gmax[0]);
  PetscPrintf(PETSC_COMM_WORLD,"span y: %+1.4e %+1.4e\n",gmin[1],gmax[1]);
  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FindBoundary"
PetscBool FindBoundary(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[0] < (0.1 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[0] > (1.1 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] > (0.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "FindBoundaryLR"
PetscBool FindBoundaryLR(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[0] < (0.1 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[0] > (1.1 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "FindBoundaryTB"
PetscBool FindBoundaryTB(PetscScalar coor[],void *ctx)
{
  PetscBool is_b = PETSC_FALSE;
  
  if (coor[1] < (-1.0 + PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  if (coor[1] > (0.0 - PETSC_MACHINE_EPSILON)) { is_b = PETSC_TRUE; }
  
  return is_b;
}

#undef __FUNCT__
#define __FUNCT__ "EvalDirichletBoundary"
PetscErrorCode EvalDirichletBoundary(PetscScalar coor[],PetscScalar *val,void *ctx)
{
  *val = coor[0] + coor[0] * coor[1];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example3"
PetscErrorCode example3(void)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  BCList bc,bcsep[2];
  Vec x,y;
  PetscReal nrm;
  
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,0.1,1.1,-1.0,0.0,2,2,PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  
  //DMTetBasisView2d_Continuous_VTK(dm,"test.vtu");
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"span x: %+1.4e %+1.4e\n",gmin[0],gmax[0]);
  PetscPrintf(PETSC_COMM_WORLD,"span y: %+1.4e %+1.4e\n",gmin[1],gmax[1]);
  
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundary,NULL,&bc);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dm,&x);CHKERRQ(ierr);
  ierr = BCListInsert(bc,x);CHKERRQ(ierr);
  ierr = VecView(x,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundaryLR,NULL,&bcsep[0]);CHKERRQ(ierr);
  ierr = DMTetCreateDirichletList_TraverseAllNodes(dm,FindBoundaryTB,NULL,&bcsep[1]);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bcsep[0],0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  ierr = BCListDefineDirichletValue(bcsep[1],0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  ierr = BCListMerge(2,bcsep,&bc);CHKERRQ(ierr);

  ierr = BCListDefineDirichletValue(bc,0,EvalDirichletBoundary,NULL);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dm,&y);CHKERRQ(ierr);
  ierr = BCListInsert(bc,y);CHKERRQ(ierr);
  ierr = VecView(y,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = BCListDestroy(&bcsep[0]);CHKERRQ(ierr);
  ierr = BCListDestroy(&bcsep[1]);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc);CHKERRQ(ierr);
  
  ierr = VecAXPY(y,-1.0,x);CHKERRQ(ierr);
  ierr = VecNorm(y,NORM_2,&nrm);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"nrm(x-y) %1.6e\n",nrm);
  
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode example4(PetscInt testid)
{
  PetscErrorCode ierr;
  DM dmc,dmf;
  PetscReal gmin[3],gmax[3];
  PetscInt ref_type;
  
  if (testid == 0) {
    ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-0.1,1.1,-3.0,0.0,4,4,PETSC_TRUE,
                                       1,DMTET_CWARP_AND_BLEND,1,&dmc);CHKERRQ(ierr);
    
  } else if (testid == 1) {
    ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,"examples/reference_meshes/solcx/mesh-r1/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dmc);CHKERRQ(ierr);
  } else {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unknown test id");
  }
  
  ref_type = 0;
  ierr = PetscOptionsGetInt(NULL,NULL,"-ref_type",&ref_type,NULL);CHKERRQ(ierr);
  ierr = DMTetCreateRefinement(dmc,ref_type,&dmf);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dmf,gmin,gmax);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"span x: %+1.4e %+1.4e\n",gmin[0],gmax[0]);
  PetscPrintf(PETSC_COMM_WORLD,"span y: %+1.4e %+1.4e\n",gmin[1],gmax[1]);

  ierr = DMTetGeometryView2d_VTK(dmc,"dmc.vtu");CHKERRQ(ierr);
  ierr = DMTetGeometryView2d_VTK(dmf,"dmf.vtu");CHKERRQ(ierr);
  ierr = DMTetViewBasisFunctions2d_CWARPANDBLEND(dmf);CHKERRQ(ierr);

  ierr = DMDestroy(&dmc);CHKERRQ(ierr);
  ierr = DMDestroy(&dmf);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "mark_NorthFace"
PetscErrorCode mark_NorthFace(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  //printf("y %+1.4e %+1.4e\n",facet->x0[1],facet->x1[1]);
  if ((facet->x0[1]+1.0e-10) > 0.0) {
    if ((facet->x1[1]+1.0e-10) > 0.0) {
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "mark_NorthFace_ex7"
PetscErrorCode mark_NorthFace_ex7(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  if ((facet->x0[1]+1.0e-10) > 1.0) {
    if ((facet->x1[1]+1.0e-10) > 1.0) {
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "mark_WestFace"
PetscErrorCode mark_WestFace(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  if ((facet->x0[0]-1.0e-10) < -0.1) {
    if ((facet->x1[0]-1.0e-10) < -0.1) {
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example5_list"
PetscErrorCode example5_list(void)
{
  PetscErrorCode ierr;
  DM dm;
  PetscReal gmin[3],gmax[3];
  PetscInt len,k;
  const PetscInt *indices;
  PetscReal *mesh_coor_g,*mesh_coor_b;
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-0.1,1.1,-3.0,0.0,4,4,PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,2,&dm);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dm);CHKERRQ(ierr);
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"span x: %+1.4e %+1.4e\n",gmin[0],gmax[0]);
  PetscPrintf(PETSC_COMM_WORLD,"span y: %+1.4e %+1.4e\n",gmin[1],gmax[1]);
  
  ierr = DMGListCreate(dm,0,"bfacet:north_face");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,1,"bfacet_b:north_face");CHKERRQ(ierr);
  
  //ierr = DMGListCreate(dm,2,"bfacet:west_face");CHKERRQ(ierr);

  ierr = DMGListCreate(dm,4,"bfacet_g:default");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,5,"bfacet_b:default");CHKERRQ(ierr);
  
  
  /* assignment function */
  ierr = DMTetBFacetListAssign_User(dm,"bfacet:north_face",mark_NorthFace,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssign(dm,"bfacet:north_face","bfacet_b:north_face");CHKERRQ(ierr);

  //ierr = DMTetBFacetListAssign_User(dm,"bfacet:west_face",mark_WestFace,NULL);CHKERRQ(ierr);

  ierr = DMTetBFacetListAssign_Default(dm,"bfacet_g:default");
  ierr = DMTetBFacetBasisListAssign(dm,"bfacet_g:default","bfacet_b:default");CHKERRQ(ierr);
  
  ierr = DMGListView(dm);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,NULL,NULL,NULL,NULL,NULL,&mesh_coor_g);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,NULL,NULL,NULL,NULL,NULL,&mesh_coor_b);CHKERRQ(ierr);
  
  /* verify */
  ierr = DMGListGetIndices(dm,"bfacet_b:north_face",&len,&indices);CHKERRQ(ierr);
  for (k=0; k<len; k++) {
    PetscReal *coor;
    
    coor = &mesh_coor_b[2*indices[k]];
    printf("  bfacet_b:north_face index %d : coor %+1.4e %+1.4e\n",indices[k],coor[0],coor[1]);
  }

  ierr = DMGListGetIndices(dm,"bfacet_b:default",&len,&indices);CHKERRQ(ierr);
  for (k=0; k<len; k++) {
    PetscReal *coor;
    
    coor = &mesh_coor_b[2*indices[k]];
    printf("  bfacet_b:default index %d : coor %+1.4e %+1.4e\n",indices[k],coor[0],coor[1]);
  }

  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example6_list_inflow"
PetscErrorCode example6_list_inflow(void)
{
  PetscErrorCode ierr;
  DM dm,dm_v;
  Vec velocity;
  PetscInt len,k;
  const PetscInt *indices;
  PetscReal *mesh_coor_g,*mesh_coor_b;
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-1.0,1.0,-1.0,1.0,4,4,PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,2,&dm);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dm);CHKERRQ(ierr);

  ierr = DMTetCloneGeometry(dm,2,DMTET_CWARP_AND_BLEND,4,&dm_v);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_v,&velocity);CHKERRQ(ierr);
  {
    PetscReal *LA_v,*coor;
    PetscInt nnodes;
    
    ierr = DMTetSpaceElement(dm_v,NULL,NULL,NULL,NULL,&nnodes,&coor);CHKERRQ(ierr);
    ierr = VecGetArray(velocity,&LA_v);CHKERRQ(ierr);
    for (k=0; k<nnodes; k++) {
      const PetscReal *x = &coor[2*k];
      
      LA_v[2*k+0] =  x[1];
      LA_v[2*k+1] = -x[0];
    }
    ierr = VecRestoreArray(velocity,&LA_v);CHKERRQ(ierr);
  }
  {
    const char *fname[] = {"velocity"};
    ierr = DMTetViewFields_VTU(dm_v,1,&velocity,fname,"inflow");CHKERRQ(ierr);
  }
  
  ierr = DMGListCreate(dm,0,"bfacet:inflow");CHKERRQ(ierr);
  
  /* assignment function */
  ierr = DMTetBFacetBasisListAssignFromInflow(dm,"bfacet:inflow",dm_v,velocity);CHKERRQ(ierr);
  
  ierr = DMGListView(dm);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,NULL,NULL,NULL,NULL,NULL,&mesh_coor_g);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,NULL,NULL,NULL,NULL,NULL,&mesh_coor_b);CHKERRQ(ierr);
  
  /* verify */
  ierr = DMGListGetIndices(dm,"bfacet:inflow",&len,&indices);CHKERRQ(ierr);
  for (k=0; k<len; k++) {
    PetscReal *coor;
    
    coor = &mesh_coor_b[2*indices[k]];
    printf("  bfacet:inflow index %d : coor %+1.4e %+1.4e\n",indices[k],coor[0],coor[1]);
  }
  
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_v);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example7_list_inflow_subset"
PetscErrorCode example7_list_inflow_subset(void)
{
  PetscErrorCode ierr;
  DM dm,dm_v;
  Vec velocity;
  PetscInt len,k;
  const PetscInt *indices;
  PetscReal *mesh_coor_g,*mesh_coor_b;
  
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,-1.0,1.0,-1.0,1.0,4,4,PETSC_TRUE,
                                     1,DMTET_CWARP_AND_BLEND,2,&dm);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(dm);CHKERRQ(ierr);
  
  ierr = DMTetCloneGeometry(dm,2,DMTET_CWARP_AND_BLEND,4,&dm_v);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_v,&velocity);CHKERRQ(ierr);
  {
    PetscReal *LA_v,*coor;
    PetscInt nnodes;
    
    ierr = DMTetSpaceElement(dm_v,NULL,NULL,NULL,NULL,&nnodes,&coor);CHKERRQ(ierr);
    ierr = VecGetArray(velocity,&LA_v);CHKERRQ(ierr);
    for (k=0; k<nnodes; k++) {
      const PetscReal *x = &coor[2*k];
      
      LA_v[2*k+0] =  x[1];
      LA_v[2*k+1] = -x[0];
    }
    ierr = VecRestoreArray(velocity,&LA_v);CHKERRQ(ierr);
  }
  {
    const char *fname[] = {"velocity"};
    ierr = DMTetViewFields_VTU(dm_v,1,&velocity,fname,"inflow");CHKERRQ(ierr);
  }
  
  ierr = DMGListCreate(dm,0,"bfacet:north_face");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,1,"bfacet:north_inflow");CHKERRQ(ierr);

  /* assignment function */
  ierr = DMTetBFacetListAssign_User(dm,"bfacet:north_face",mark_NorthFace_ex7,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssignFromInflowSubset(dm,"bfacet:north_face","bfacet:north_inflow",dm_v,velocity);CHKERRQ(ierr);
  
  ierr = DMGListView(dm);CHKERRQ(ierr);
  
  ierr = DMTetGeometryElement(dm,NULL,NULL,NULL,NULL,NULL,&mesh_coor_g);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,NULL,NULL,NULL,NULL,NULL,&mesh_coor_b);CHKERRQ(ierr);
  
  /* verify */
  ierr = DMGListGetIndices(dm,"bfacet:north_inflow",&len,&indices);CHKERRQ(ierr);
  for (k=0; k<len; k++) {
    PetscReal *coor;
    
    coor = &mesh_coor_b[2*indices[k]];
    printf("  bfacet:north_inflow index %d : coor %+1.4e %+1.4e\n",indices[k],coor[0],coor[1]);
  }
  
  ierr = VecDestroy(&velocity);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_v);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_seq_shared"
PetscErrorCode example_seq_shared(void)
{
  PetscErrorCode ierr;
  DM dm,geom_s,space_s;
  Vec x,xl;
  
  ierr = DMTetCreate(PETSC_COMM_SELF,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,2);CHKERRQ(ierr);
  ierr = DMTetSetDof(dm,1);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(dm,DMTET_CWARP_AND_BLEND);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dm,14);CHKERRQ(ierr);
  ierr = DMTetCreateGeometryFromTriangle(dm,"examples/reference_meshes/solcx/mesh-r4/solcx.1");CHKERRQ(ierr);
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);
  //ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);

  ierr = DMTetCreateSharedGeometry(dm,3,DMTET_CWARP_AND_BLEND,4,&geom_s);CHKERRQ(ierr);
  ierr = DMTetCreateSharedSpace(geom_s,10,&space_s);CHKERRQ(ierr);

  /* create vector */
  ierr = DMCreateLocalVector(geom_s,&xl);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(geom_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);

  ierr = DMCreateLocalVector(space_s,&xl);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(space_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);

  /* get vecs */
  ierr = DMGetLocalVector(geom_s,&xl);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(geom_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(geom_s,&xl);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(geom_s,&x);CHKERRQ(ierr);
  
  ierr = DMGetLocalVector(space_s,&xl);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(space_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(space_s,&xl);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(space_s,&x);CHKERRQ(ierr);
  
  ierr = DMDestroy(&space_s);CHKERRQ(ierr);
  ierr = DMDestroy(&geom_s);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "example_mpi_shared"
PetscErrorCode example_mpi_shared(void)
{
  PetscErrorCode ierr;
  DM dm,geom_s,space_s;
  Vec x,xl;
  
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"examples/reference_meshes/solcx/mesh-r4/solcx.1",1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
  ierr = DMTetCreateSharedGeometry(dm,3,DMTET_CWARP_AND_BLEND,4,&geom_s);CHKERRQ(ierr);
  ierr = DMTetCreateSharedSpace(geom_s,10,&space_s);CHKERRQ(ierr);

  /* create vector */
  ierr = DMCreateLocalVector(geom_s,&xl);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(geom_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(space_s,&xl);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(space_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  
  /* get vecs */
  ierr = DMGetLocalVector(geom_s,&xl);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(geom_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(geom_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(geom_s,&xl);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(geom_s,&x);CHKERRQ(ierr);
  
  ierr = DMGetLocalVector(space_s,&xl);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(space_s,&x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(space_s,xl,ADD_VALUES,x);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(space_s,&xl);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(space_s,&x);CHKERRQ(ierr);
  
  ierr = DMDestroy(&space_s);CHKERRQ(ierr);
  ierr = DMDestroy(&geom_s);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


int main(int argc,char **argv)
{
	PetscErrorCode ierr;
  PetscInt test_code = 3;
  
	ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-test_id",&test_code,NULL);CHKERRQ(ierr);
  switch (test_code) {
    case 1:
    ierr = example1();CHKERRQ(ierr);
    break;
    case 2:
    ierr = example2();CHKERRQ(ierr);
    break;
    case 3:
    ierr = example3();CHKERRQ(ierr);
    break;
    case 4:
    ierr = example4(1);CHKERRQ(ierr);
    break;
    case 5:
    ierr = example5_list();CHKERRQ(ierr);
    break;
    case 6:
    ierr = example6_list_inflow();CHKERRQ(ierr);
    break;
    case 7:
    ierr = example7_list_inflow_subset();CHKERRQ(ierr);
    break;
    case 8: {
      PetscInt k,nloops = 1000;
      ierr = PetscOptionsGetInt(NULL,NULL,"-nloops",&nloops,NULL);CHKERRQ(ierr);
      for (k=0; k<nloops; k++) {
        ierr = example1();CHKERRQ(ierr);
      }
    }
      break;
      
    case 9:
    {
      PetscInt k,nloops = 1000;
      ierr = PetscOptionsGetInt(NULL,NULL,"-nloops",&nloops,NULL);CHKERRQ(ierr);
      for (k=0; k<nloops; k++) {
        ierr = example_seq_shared();CHKERRQ(ierr);
      }
    }
      break;
    case 10:
    {
      PetscInt its = 0,nloops = -1;
      PetscReal wtime_mins = 10.0; /* wall time in mins */
      PetscLogDouble time = 0.0,t0,t1;
      
      ierr = PetscOptionsGetReal(NULL,NULL,"-wtime",&wtime_mins,NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetInt(NULL,NULL,"-nloops",&nloops,NULL);CHKERRQ(ierr);
      do {
        PetscTime(&t0);
        ierr = example_mpi_shared();CHKERRQ(ierr);
        PetscTime(&t1);
        time += (t1-t0)/60.0;
        its++;
        if (its == nloops) break;
      } while (time < wtime_mins);
    }
      break;
      
    default:
    break;
  }
	ierr = projFinalize();CHKERRQ(ierr);
	return 0;
}

