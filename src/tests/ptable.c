
#include <petsc.h>
#include <khash.h>
#include <ksort.h>
#include <data_exchanger.h>
#include <ptable.h>

#if 0
KHASH_MAP_INIT_INT64(h64, PTableValues*)

void hashdemo(void)
{
  khash_t(h64)  *hash;
  PTableValues *dummy;
  PTableValues *row;
  int ret;
  khiter_t iterator;
  long int key;
  long int size;
  khint_t k;
  
  hash = kh_init(h64);

  dummy = malloc(sizeof(PTableValues));
  dummy->len = 1;
  dummy->allocated_len = 1;
  dummy->values = NULL;
  
  /* insert key into slot 1 */
  iterator = kh_put(h64, hash, -1, &ret);
  row = malloc(sizeof(PTableValues));
  row->len = -1;
  row->allocated_len = -1;
  row->values = NULL;
  (kh_value(hash, iterator)) = row;

  
  key = 10;
  row = malloc(sizeof(PTableValues));
  row->len = 10;
  row->allocated_len = 10;
  row->values = NULL;
  printf("key %ld : row %p\n",key,row);
  
  iterator = kh_put(h64, hash, key, &ret);
  (kh_value(hash, iterator)) = row;

  key = 100000;
  row = malloc(sizeof(PTableValues));
  row->len = 100000;
  row->allocated_len = 100000;
  row->values = NULL;
  printf("key %ld : row %p\n",key,row);

  iterator = kh_put(h64, hash, key, &ret);
  (kh_value(hash, iterator)) = row;

  key = -2;
  row = malloc(sizeof(PTableValues));
  row->len = -2;
  row->allocated_len = -2;
  row->values = NULL;
  printf("key %ld : row %p\n",key,row);
  
  iterator = kh_put(h64, hash, key, &ret);
  (kh_value(hash, iterator)) = row;

  
  key = 100;
  row = malloc(sizeof(PTableValues));
  row->len = 100;
  row->allocated_len = 100;
  row->values = NULL;
  printf("key %ld : row %p\n",key,row);
  
  iterator = kh_put(h64, hash, key, &ret);
  (kh_value(hash, iterator)) = row;

  key = 15;
  row = malloc(sizeof(PTableValues));
  row->len = 15;
  row->allocated_len = 15;
  row->values = NULL;
  printf("key %ld : row %p\n",key,row);
  
  iterator = kh_put(h64, hash, key, &ret);
  (kh_value(hash, iterator)) = row;

  
  row = NULL;
  
  /* fetch */
  key = 10;
  iterator = kh_get(h64, hash, key);
  row = kh_value(hash, iterator);
  printf("key %ld --> row %p\n",key,row);
  printf("key %ld --> %ld %ld\n",key,row->len,row->allocated_len);
  

  size = kh_size(hash);
  printf("size %ld \n",size);

  // delete thing with key = 10
  free(row);
  kh_del(ptable64, hash, iterator);

  size = kh_size(hash);
  printf("size %ld \n",size);
  //
  
  
  khiter_t k0,k1;
  
  k0 = kh_begin(hash);
  k1 = kh_end(hash);
  
  for (k=k0; k<k1; k++) {
    key = 0;
    key = kh_key(hash, k);
    //iterator = kh_get(ptable64, hash, key);
    if (kh_exist(hash, k)) {
      printf("k %u : key %ld\n",k,key);
      
      iterator = kh_get(h64, hash, key);
      row = kh_value(hash, iterator);
      printf("   --> row %p\n",row);
    }
    
  }
  
  kh_destroy(h64, hash);
}
#endif

PetscErrorCode ex1(void)
{
  PTable table;
  PetscErrorCode ierr;
  
  ierr = PTableCreate(PETSC_COMM_SELF,&table);CHKERRQ(ierr);
  ierr = PTableSetRange(table,0,30);CHKERRQ(ierr);
  ierr = PTableSetType(table,PTABLE_SPARSE);CHKERRQ(ierr);
  ierr = PTableSetup(table);CHKERRQ(ierr);
  
  ierr = PTableSetValue(table,5,0);CHKERRQ(ierr);
  ierr = PTableSetValue(table,6,19);CHKERRQ(ierr);
  // ierr = PTableSetValue(table,7,101);CHKERRQ(ierr);
  //ierr = PTableSetValue(table,7,-1);CHKERRQ(ierr);
  //for (int i=0; i<128*3; i++) {
  for (int i=0; i<5000000; i++) {
    ierr = PTableSetValue(table,7,i);CHKERRQ(ierr);
  }

  ierr = PTableSynchronize(table);CHKERRQ(ierr);
  
  //ierr = PTableView(table);CHKERRQ(ierr);
  //ierr = PTableViewLite(table);CHKERRQ(ierr);
  ierr = PTableDestroy(&table);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode ex2(void)
{
  PTable table;
  PetscErrorCode ierr;
  PetscMPIInt rank;
  
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  
  ierr = PTableCreate(PETSC_COMM_WORLD,&table);CHKERRQ(ierr);
  ierr = PTableSetRange(table,rank*30,rank*30+30);CHKERRQ(ierr);
  ierr = PTableSetType(table,PTABLE_SPARSE);CHKERRQ(ierr);
  ierr = PTableSetup(table);CHKERRQ(ierr);
  
  ierr = PTableSetValue(table,5,8870);CHKERRQ(ierr);
  /*
  ierr = PTableSetValue(table,5,0);CHKERRQ(ierr);
  ierr = PTableSetValue(table,6,19);CHKERRQ(ierr);
  // ierr = PTableSetValue(table,7,101);CHKERRQ(ierr);
  //ierr = PTableSetValue(table,7,-1);CHKERRQ(ierr);
  for (int i=0; i<21; i++) {
    ierr = PTableSetValue(table,7,i);CHKERRQ(ierr);
  }

   
  */
  if (rank == 0) {
    ierr = PTableSetValue(table,55,501);CHKERRQ(ierr);
    ierr = PTableSetValue(table,55,502);CHKERRQ(ierr);

    ierr = PTableSetValue(table,50,2);CHKERRQ(ierr);
    ierr = PTableSetValue(table,50,22);CHKERRQ(ierr);
    ierr = PTableSetValue(table,50,222);CHKERRQ(ierr);
    for (int i=0; i<128*3; i++) {
      ierr = PTableSetValue(table,59,i+100000);CHKERRQ(ierr);
    }
  }
  
  ierr = PTableSynchronize(table);CHKERRQ(ierr);
  
  MPI_Barrier(table->comm);
  ierr = PTableView(table);CHKERRQ(ierr);
  if (rank == 0) {
    //  ierr = PTableView_Self(table->cache);CHKERRQ(ierr);
  }
  
  ierr = PTableDestroy(&table);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ex3(void)
{
  PTable table;
  IS fis,tis;
  PetscErrorCode ierr;
  
  ierr = PTableCreate(PETSC_COMM_SELF,&table);CHKERRQ(ierr);
  ierr = PTableSetRange(table,0,30);CHKERRQ(ierr);
  ierr = PTableSetType(table,PTABLE_SPARSE);CHKERRQ(ierr);
  ierr = PTableSetup(table);CHKERRQ(ierr);
  
  ierr = PTableSetValue(table,5,23);CHKERRQ(ierr);
  ierr = PTableSetValue(table,6,19);CHKERRQ(ierr);
  ierr = PTableSetValue(table,7,101);CHKERRQ(ierr);
  
  ierr = PTableSynchronize(table);CHKERRQ(ierr);

  ierr = PTableView(table);CHKERRQ(ierr);

  ierr = PTableFlattenIntoIS(table,PETSC_TRUE,&fis,&tis);CHKERRQ(ierr);
  //ierr = PTableFlattenIntoIS(table,PETSC_FALSE,&fis,&tis);CHKERRQ(ierr);
  ierr = ISView(fis,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = ISView(tis,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = PTableDestroy(&table);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


/* overlapping ranges */
PetscErrorCode ex4(void)
{
  PTable table;
  PetscErrorCode ierr;
  PetscMPIInt rank,size;
  
  ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);
  if (size > 2) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only for comm.size=2");
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  
  ierr = PTableCreate(PETSC_COMM_WORLD,&table);CHKERRQ(ierr);
  if (rank == 0) {
    ierr = PTableSetRange(table,0,15);CHKERRQ(ierr);
  } else {
    ierr = PTableSetRange(table,10,30);CHKERRQ(ierr);
  }
  ierr = PTableSetType(table,PTABLE_SPARSE);CHKERRQ(ierr);
  ierr = PTableSetup(table);CHKERRQ(ierr);
  
  if (rank == 0) {
    ierr = PTableSetValue(table,5,8870);CHKERRQ(ierr);
  }
  if (rank == 1) {
    ierr = PTableSetValue(table,12,8);CHKERRQ(ierr);
  }
  
  ierr = PTableSynchronize(table);CHKERRQ(ierr);
  
  MPI_Barrier(table->comm);
  ierr = PTableView(table);CHKERRQ(ierr);
  ierr = PTableDestroy(&table);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

//KSORT_INIT(ptableSort64,long int,)
KSORT_INIT_GENERIC(long)

void ex5_ksort(void)
{
  PetscLogDouble t0,t1;
  long int *vals,*init;
  long int i,N = 25000000;
  
  PetscMalloc1(N,&vals);
  PetscMalloc1(N,&init);

  for (i=0; i<N; i++) {
    init[i] = i + 10;
    init[i] = rand();
  }
  
  PetscMemcpy(vals,init,sizeof(long int)*N);
  PetscTime(&t0);
  PetscSortLongInt(vals,N-1);
  PetscTime(&t1);
  printf("recursive    %1.4e (sec) (%ld,%ld)\n",t1-t0,vals[0],vals[N-1]);
  
  
  PetscMemcpy(vals,init,sizeof(long int)*N);
  PetscTime(&t0);
  ks_mergesort(long, N, vals, 0);
  PetscTime(&t1);
  printf("ks_mergesort %1.4e (sec) (%ld,%ld)\n",t1-t0,vals[0],vals[N-1]);

  
  PetscMemcpy(vals,init,sizeof(long int)*N);
  PetscTime(&t0);
  ks_combsort(long, N, vals);
  PetscTime(&t1);
  printf("ks_combsort  %1.4e (sec) (%ld,%ld)\n",t1-t0,vals[0],vals[N-1]);

  
  PetscMemcpy(vals,init,sizeof(long int)*N);
  PetscTime(&t0);
  ks_introsort(long, N, vals);
  PetscTime(&t1);
  printf("ks_introsort %1.4e (sec) (%ld,%ld)\n",t1-t0,vals[0],vals[N-1]);

  
  PetscMemcpy(vals,init,sizeof(long int)*N);
  PetscTime(&t0);
  ks_ksmall(long, N, vals, 10500);
  PetscTime(&t1);
  printf("ks_ksmall    %1.4e (sec) (%ld,%ld)\n",t1-t0,vals[0],vals[N-1]);

  
  PetscMemcpy(vals,init,sizeof(long int)*N);
  PetscTime(&t0);
  ks_heapmake(long, N, vals);
  PetscTime(&t1);
  printf("ks_heapmake  %1.4e (sec)\n",t1-t0);
  PetscTime(&t0);
  ks_heapsort(long, N, vals);
  PetscTime(&t1);
  printf("ks_heapsort  %1.4e (sec) (%ld,%ld)\n",t1-t0,vals[0],vals[N-1]);
  
  PetscFree(vals);
  PetscFree(init);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = PetscInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
#if 0
  //hashdemo();
#endif
  ex1(); /* test sparse/dense in serial */
  //ex2(); /* test sparse/dense in parallel */
  //ex3(); /* test for flatten PTable into IS */
  //ex4(); /* test for overlapping ranges */
  //ex5_ksort();
  
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}


