
#include <petscdmtet.h>
#include <coefficient.h>
#include <coefficientimpl.h>
#include <data_bucket.h>
#include <quadrature.h>
#include <quadratureimpl.h>

#undef __FUNCT__
#define __FUNCT__ "CoefficientCreate"
PetscErrorCode CoefficientCreate(MPI_Comm comm,Coefficient *_c)
{
  Coefficient c;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&c);CHKERRQ(ierr);
  ierr = PetscMemzero(c,sizeof(struct _p_Coefficient));CHKERRQ(ierr);
  c->constant_set_by_user = PETSC_FALSE;
  c->region = NULL;
  *_c = c;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetType"
PetscErrorCode CoefficientSetType(Coefficient c,CoefficientType type)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  c->type = type;
  if (c->type == COEFF_QUADRATURE) {
    if (!c->quadrature) {
      ierr = QuadratureCreate(&c->quadrature);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetDomainConstant"
PetscErrorCode CoefficientSetDomainConstant(Coefficient c,PetscReal val)
{
  PetscFunctionBegin;
  c->domain_const = val;
  c->constant_set_by_user = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetComputeDomainConstant"
PetscErrorCode CoefficientSetComputeDomainConstant(Coefficient c,
                                                   PetscErrorCode (*evaluate_domain_const)(Coefficient,PetscReal*,void*),
                                                   PetscErrorCode (*destroy_domain_const)(Coefficient),
                                                   void *data)
{
  PetscFunctionBegin;
  if (c->type != COEFF_DOMAIN_CONST) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient is not set to be of type COEFF_DOMAIN_CONST");
  c->evaluate_domain_const = evaluate_domain_const;
  c->destroy_user_ctx = destroy_domain_const;
  c->domain_const_ctx = data;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetComputeQuadrature"
PetscErrorCode CoefficientSetComputeQuadrature(Coefficient c,
                                                   PetscErrorCode (*evaluate_quadrature)(Coefficient,Quadrature,PetscInt*,void*),
                                                   PetscErrorCode (*destroy_quadrature)(Coefficient),
                                                   void *data)
{
  PetscFunctionBegin;
  if (c->type != COEFF_QUADRATURE) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient is not set to be of type COEFF_QUADRATURE");
  c->evaluate_quadrature = evaluate_quadrature;
  c->destroy_user_ctx = destroy_quadrature;
  c->quadrature_ctx = data;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientEvaluate"
PetscErrorCode CoefficientEvaluate(Coefficient c)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  switch (c->type) {
    case COEFF_NONE:
    break;

    case COEFF_DOMAIN_CONST:
      if (c->evaluate_domain_const) {
        ierr = c->evaluate_domain_const(c,&c->domain_const,c->domain_const_ctx);CHKERRQ(ierr);
      } else {
        /* use stored value */
        if (!c->constant_set_by_user) {
          SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Domain constant must be explicitly set by user");
        }
      }
    break;

    case COEFF_QUADRATURE:
      if (c->evaluate_quadrature) {
        ierr = c->evaluate_quadrature(c,c->quadrature,c->region,c->quadrature_ctx);CHKERRQ(ierr);
      } else {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"No method was provided by user to evaluate quadrature point properties");
      }
    break;

  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientDestroy"
PetscErrorCode CoefficientDestroy(Coefficient *coeff)
{
  Coefficient c;
  PetscErrorCode ierr;
  PetscFunctionBegin;

  if (!coeff) PetscFunctionReturn(0);
  if (!*coeff) PetscFunctionReturn(0);
  c = *coeff;
  
  if (c->destroy_user_ctx) {
    ierr = c->destroy_user_ctx(c);CHKERRQ(ierr);
  }
  if (c->quadrature) {
    ierr = QuadratureDestroy(&c->quadrature);CHKERRQ(ierr);
  }
  ierr = PetscFree(c);CHKERRQ(ierr);
  *coeff = NULL;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetPDE"
PetscErrorCode CoefficientSetPDE(Coefficient c,PDE p)
{
  PetscFunctionBegin;
  c->pde = p;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientGetPDE"
PetscErrorCode CoefficientGetPDE(Coefficient c,PDE *p)
{
  PetscFunctionBegin;
  if (p) {
    *p = c->pde;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientGetQuadrature"
PetscErrorCode CoefficientGetQuadrature(Coefficient c,Quadrature *q)
{
  PetscFunctionBegin;
  if (q) {
    if (c->type != COEFF_QUADRATURE) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient is not set to be of type COEFF_QUADRATURE");
    *q = c->quadrature;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientGetDomainConstant"
PetscErrorCode CoefficientGetDomainConstant(Coefficient c,PetscReal *val)
{
  PetscFunctionBegin;
  if (val) {
    if (c->type != COEFF_DOMAIN_CONST) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient is not set to be of type COEFF_DOMAIN_CONST");
    if (!c->constant_set_by_user) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient domain constant has not been set");
    *val = c->domain_const;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientQuadratureEvaluator_Empty"
PetscErrorCode CoefficientQuadratureEvaluator_Empty(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientQuadratureEvaluator_NULL"
PetscErrorCode CoefficientQuadratureEvaluator_NULL(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  PetscInt nqp,nelements,ncomponents,q,e,ci;
  PetscReal *coeff;
  int nfields,f;
  size_t    asize;
  DataField *gfield;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  nelements = quadrature->nelements;
  DataBucketGetDataFields(quadrature->properties,&nfields,&gfield);
  for (f=0; f<nfields; f++) {
    DataFieldGetEntries(gfield[f],(void**)&coeff);
    DataFieldGetAtomicSize(gfield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);

    for (e=0; e<nelements; e++) {
      for (q=0; q<nqp; q++) {
        for (ci=0; ci<ncomponents; ci++) {
          coeff[ncomponents*(e*nqp + q) + ci] = 0.0;
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetComputeQuadratureNULL"
PetscErrorCode CoefficientSetComputeQuadratureNULL(Coefficient c)
{
  PetscFunctionBegin;
  if (c->type != COEFF_QUADRATURE) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient is not set to be of type COEFF_QUADRATURE");
  c->evaluate_quadrature = CoefficientQuadratureEvaluator_NULL;
  c->destroy_user_ctx = NULL;
  c->quadrature_ctx = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetComputeQuadratureEmpty"
PetscErrorCode CoefficientSetComputeQuadratureEmpty(Coefficient c)
{
  PetscFunctionBegin;
  if (c->type != COEFF_QUADRATURE) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Coefficient is not set to be of type COEFF_QUADRATURE");
  c->evaluate_quadrature = CoefficientQuadratureEvaluator_Empty;
  c->destroy_user_ctx = NULL;
  c->quadrature_ctx = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientSetRegionsFromDM"
PetscErrorCode CoefficientSetRegionsFromDM(Coefficient c,DM dm)
{
  PetscInt q_ncells,dm_ncells,*dm_region_id;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  /* check c->quadrature->cells matches cells in dm */
  ierr = QuadratureGetSizes(c->quadrature,NULL,NULL,NULL,&q_ncells);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&dm_ncells,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  if (q_ncells != dm_ncells) {
    if (q_ncells != -1) {
      SETERRQ2(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Number of cells in coeff->quadrature (%D) must match number of cells in dm (%D)",q_ncells,dm_ncells);
    }
  }

  //ierr = DMTetSpaceGetAttributes(dm,NULL,&dm_region_id,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dm,NULL,&dm_region_id,NULL);CHKERRQ(ierr);
  /* check c->region is NULL */
  if (c->region) {
    if (c->region != dm_region_id)
    SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"An existing, non-matching region list was detected. You must call CoefficientResetRegionsFromDM() before CoefficientSetRegionsFromDM() unless using the same DM");
  }
  
  c->region = dm_region_id;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientResetRegionsFromDM"
PetscErrorCode CoefficientResetRegionsFromDM(Coefficient c,DM dm)
{
  PetscInt *dm_region_id;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  /* check c->region is !NULL */
  if (!c->region) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"A region list was not detected. You must call CoefficientSetRegionsFromDM() before CoefficientResetRegionsFromDM()");

  /* check dm->regions_tags matches c->region */
  //ierr = DMTetSpaceGetAttributes(dm,NULL,&dm_region_id,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dm,NULL,&dm_region_id,NULL);CHKERRQ(ierr);
  if (c->region != dm_region_id) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"dm->region_list does not match the region list set via CoefficientSetRegionsFromDM()");

  c->region = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientGetRegions"
PetscErrorCode CoefficientGetRegions(Coefficient c,const PetscInt **r)
{

  if (r) {
    (*r) = c->region;
  }
  PetscFunctionReturn(0);
}
