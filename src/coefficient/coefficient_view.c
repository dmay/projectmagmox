
#include <petsc.h>
#include <petscdmtet.h>
#include <coefficient.h>
#include <coefficientimpl.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <data_bucket.h>
#include <element_container.h>



/* 
 
 Write all quadrature defined into an ascii file.
 Data is written out element wise, so the file is ordered as follows
 
 for each element
   for each quadrature point
     write coords
     write field 1
     write field 2
 
 
 note: probably should live in the quadrature source file
 
*/
#undef __FUNCT__
#define __FUNCT__ "_QuadratureView_GP"
PetscErrorCode _QuadratureView_GP(Quadrature rule,DM dm,FILE *fp)
{
  PetscErrorCode ierr;
  PetscInt i,f,c,ncomponents,*nc,nfields,q,nqp,e,nelements;
  DataField *datafield;
  size_t asize;
  PetscReal *xiq;
  PetscInt *element;
  PetscReal *coor,*elcoor,*pos;
  PetscInt nbasis,npe,nen;
  EContainer space;
  PetscReal **data,**data_e;
  
  
  DataBucketGetDataFields(rule->properties,&nfields,&datafield);
  ierr = PetscMalloc1(nfields,&nc);CHKERRQ(ierr);
  ierr = PetscMalloc1(nfields,&data);CHKERRQ(ierr);
  ierr = PetscMalloc1(nfields,&data_e);CHKERRQ(ierr);
  
  fprintf(fp,"#\n");
  fprintf(fp,"# Fields registered\n");
  fprintf(fp,"# |");
  for (f=0; f<nfields; f++) {
    DataFieldGetAtomicSize(datafield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);
    nc[f] = ncomponents;
    fprintf(fp,"   %s [%d]   |",datafield[f]->name,ncomponents);
    if (f == (nfields-1)) { fprintf(fp,"\n"); }
  }
  fprintf(fp,"#\n");
  
  fprintf(fp,"# Field data in file\n");
  fprintf(fp,"# |     coor [2]     |");
  for (f=0; f<nfields; f++) {
    DataFieldGetAtomicSize(datafield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);
    nc[f] = ncomponents;
    fprintf(fp,"   %s [%d]   |",datafield[f]->name,ncomponents);
    if (f == (nfields-1)) { fprintf(fp,"\n"); }
  }
  fprintf(fp,"# -------------------");
  for (f=0; f<nfields; f++) {
    DataFieldGetAtomicSize(datafield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);
    for (c=0; c<ncomponents; c++) {
      fprintf(fp,"-------------");
    }
  }
  fprintf(fp,"\n");

  nelements = rule->nelements;
  ierr = QuadratureGetRule(rule,&nqp,&xiq,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,&npe,0,&coor);CHKERRQ(ierr);
  if (nen != nelements) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"DM is not compatable with quadrature rule");
  ierr = PetscMalloc1(nqp*2,&pos);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoor);CHKERRQ(ierr);
  ierr = EContainerCreate(dm,rule,&space);CHKERRQ(ierr);
  
  
  for (f=0; f<nfields; f++) {
    ierr = QuadratureGetProperty(rule,datafield[f]->name,NULL,NULL,NULL,&data[f]);CHKERRQ(ierr);
  }
  
  for (e=0; e<nelements; e++) {
    PetscInt *idx;
    
    
    /* generate coordinates for all quadrature points in this element */
    /* get basis dofs */
    idx = &element[nbasis*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoor[2*i+0] = coor[2*nidx+0];
      elcoor[2*i+1] = coor[2*nidx+1];
    }

    for (q=0; q<nqp; q++) {
      pos[2*q+0] = pos[2*q+1] = 0.0;
      for (i=0; i<nbasis; i++) {
        pos[2*q+0] += space->N[q][i] * elcoor[2*i+0];
        pos[2*q+1] += space->N[q][i] * elcoor[2*i+1];
      }
    }

    for (f=0; f<nfields; f++) {
      ierr = QuadratureGetElementValues(rule,e,nc[f],data[f],&data_e[f]);CHKERRQ(ierr);

    }
    
    for (q=0; q<nqp; q++) {

      /* plot coordinates */
      fprintf(fp,"%+1.4e %+1.4e ",pos[2*q+0],pos[2*q+1]);

      for (f=0; f<nfields; f++) {
        for (c=0; c<nc[f]; c++) {
          PetscReal value;
          
          value = data_e[f][nc[f]*q+c];
          
          fprintf(fp,"%+1.4e ",value);
        }
      }
      fprintf(fp,"\n");

    }
  }
  ierr = PetscFree(data_e);CHKERRQ(ierr);
  ierr = PetscFree(data);CHKERRQ(ierr);
  ierr = EContainerDestroy(&space);CHKERRQ(ierr);
  ierr = PetscFree(elcoor);CHKERRQ(ierr);
  ierr = PetscFree(pos);CHKERRQ(ierr);
  ierr = PetscFree(nc);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_QuadratureView_SURFACE_GP"
PetscErrorCode _QuadratureView_SURFACE_GP(Quadrature rule,DM dmv,FILE *fp)
{
  PetscErrorCode ierr;
  PetscInt f,c,ncomponents,*nc,nfields,q,nqp,e,nelements;
  DataField *datafield;
  size_t asize;
  PetscReal **data,**data_e;
  
  
  DataBucketGetDataFields(rule->properties,&nfields,&datafield);
  ierr = PetscMalloc1(nfields,&nc);CHKERRQ(ierr);
  ierr = PetscMalloc1(nfields,&data);CHKERRQ(ierr);
  ierr = PetscMalloc1(nfields,&data_e);CHKERRQ(ierr);
  
  fprintf(fp,"#\n");
  fprintf(fp,"# Fields registered\n");
  fprintf(fp,"# |");
  for (f=0; f<nfields; f++) {
    DataFieldGetAtomicSize(datafield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);
    nc[f] = ncomponents;
    fprintf(fp,"   %s [%d]   |",datafield[f]->name,ncomponents);
    if (f == (nfields-1)) { fprintf(fp,"\n"); }
  }
  fprintf(fp,"#\n");
  
  fprintf(fp,"# Field data in file\n");
  fprintf(fp,"# |");
  for (f=0; f<nfields; f++) {
    DataFieldGetAtomicSize(datafield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);
    nc[f] = ncomponents;
    fprintf(fp,"   %s [%d]   |",datafield[f]->name,ncomponents);
    if (f == (nfields-1)) { fprintf(fp,"\n"); }
  }
  fprintf(fp,"# ");
  for (f=0; f<nfields; f++) {
    DataFieldGetAtomicSize(datafield[f],&asize);
    ncomponents = asize/sizeof(PetscReal);
    for (c=0; c<ncomponents; c++) {
      fprintf(fp,"-------------");
    }
  }
  fprintf(fp,"\n");
  
  nelements = rule->nelements;
  ierr = QuadratureGetRule(rule,&nqp,NULL,NULL);CHKERRQ(ierr);
  
  for (f=0; f<nfields; f++) {
    ierr = QuadratureGetProperty(rule,datafield[f]->name,NULL,NULL,NULL,&data[f]);CHKERRQ(ierr);
  }
  
  for (e=0; e<nelements; e++) {
    
    for (f=0; f<nfields; f++) {
      ierr = QuadratureGetElementValues(rule,e,nc[f],data[f],&data_e[f]);CHKERRQ(ierr);
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal coor_q[2];
      
      coor_q[0] = data_e[0][2*q+0];
      coor_q[1] = data_e[0][2*q+1];
      
      /* plot coordinates */
      fprintf(fp,"%+1.4e %+1.4e ",coor_q[0],coor_q[1]);
      
      for (f=1; f<nfields; f++) {
        for (c=0; c<nc[f]; c++) {
          PetscReal value;
          
          value = data_e[f][nc[f]*q+c];
          
          fprintf(fp,"%+1.4e ",value);
        }
        if (f != (nfields-1)) { fprintf(fp,"    "); }
      }
      fprintf(fp,"\n");
      
    }
  }
  ierr = PetscFree(data_e);CHKERRQ(ierr);
  ierr = PetscFree(data);CHKERRQ(ierr);
  ierr = PetscFree(nc);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CoefficientView_GP"
PetscErrorCode CoefficientView_GP(Coefficient c,DM dm,const char prefix[])
{
  PetscErrorCode ierr;
  FILE *fp;
  char fname[PETSC_MAX_PATH_LEN];
  
  if (prefix) {
    ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"%s-coeff.gp",prefix);CHKERRQ(ierr);
  } else {
    PetscPrintf(PETSC_COMM_WORLD,"[warning] CoefficientView_GP will use default file name \"coeff.gp\"\n");
    ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"coeff.gp",prefix);CHKERRQ(ierr);
  }
  
  fp = fopen(fname,"w");
  if (!fp) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Failed to open file %s",fname);
  
  fprintf(fp,"# Coefficient \n");
  fprintf(fp,"#   type:  \n");
  fprintf(fp,"#     \n");
  
  if (c->quadrature) {
    if (c->quadrature->gtype == QUADRATURE_VOLUME) {
      fprintf(fp,"# Quadrature [volume]\n");
      ierr = _QuadratureView_GP(c->quadrature,dm,fp);CHKERRQ(ierr);
    }
    if (c->quadrature->gtype == QUADRATURE_SURFACE) {
      fprintf(fp,"# Quadrature [surface]\n");
      ierr = _QuadratureView_SURFACE_GP(c->quadrature,dm,fp);CHKERRQ(ierr);
    }
  }
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}
