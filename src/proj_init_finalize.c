
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <sys/stat.h>
#include <time.h>

#include <petsc.h>
#include <petsclog.h>
#include <proj_version_info.h>

#define STR_VALUE(arg)      #arg
#define STRINGIFY_ARG(name) STR_VALUE(name)

char project_output_path[PETSC_MAX_PATH_LEN];

#undef __FUNCT__
#define __FUNCT__ "projCheckCompilationFlags"
PetscErrorCode projCheckCompilationFlags(PetscViewer v,const char flags[])
{
  char *loc = NULL;
  int throw_warning = 0;
  
  PetscFunctionBegin;
  
  loc = strstr(flags,"-O0");
  if (loc != NULL) {
    throw_warning = 1;
  }
  if (throw_warning == 1) {
    PetscViewerASCIIPrintf(v,"## WARNING project appears to have been compiled with debug options\n");
    PetscViewerASCIIPrintf(v,"## For significant performance improvements, please consult the file makefile.arch\n");
    PetscViewerASCIIPrintf(v,"## Adjust PROJ_CFLAGS to include aggressive compiler optimizations\n");
    PetscViewerASCIIPrintf(v,"##\n");
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "projWritePreamble"
PetscErrorCode projWritePreamble(PetscViewer v)
{
  PetscFunctionBegin;
  
  PetscViewerASCIIPrintf(v,"## ====================================================================================== \n");
  PetscViewerASCIIPrintf(v,"##\n");
  PetscViewerASCIIPrintf(v,"## SubFUSC: Long-time Evolutionary Analysis of Subduction Zones\n");
  PetscViewerASCIIPrintf(v,"##\n");
  PetscViewerASCIIPrintf(v,"## Authors:  Dave A. May       [david.may@earth.ox.ac.uk]\n");
  PetscViewerASCIIPrintf(v,"##           David Rees Jones  [david.reesjones@earth.ox.ac.uk]\n");
  PetscViewerASCIIPrintf(v,"##           Meng Tian         [meng.tian@earth.ox.ac.uk]\n");
  PetscViewerASCIIPrintf(v,"##           John Rudge        [jfr23@cam.ac.uk]\n");
  PetscViewerASCIIPrintf(v,"##           Richard F. Katz   [richard.katz@earth.ox.ac.uk]\n");
  PetscViewerASCIIPrintf(v,"##\n");
  
  PetscViewerASCIIPrintf(v,"## %s\n", PROJ_VERSION_CNTR_REPO);
#ifdef PROJ_DEVELOPMENT_VERSION
  PetscViewerASCIIPrintf(v,"## %s\n", PROJ_VERSION_CNTR_REVISION);
  PetscViewerASCIIPrintf(v,"## %s\n", PROJ_VERSION_CNTR_LOG);
#endif
#ifdef PROJ_RELEASE
  PetscViewerASCIIPrintf(v,"## %s\n", PROJ_VERSION_CNTR_REVISION);
  PetscViewerASCIIPrintf(v,"## Release v%d.%d-p%d\n", PROJ_VERSION_MAJOR,PROJ_VERSION_MINOR,PROJ_VERSION_PATCH);
#endif
  
#ifdef COMPFLAGS
#define STR_ARG_NAME STRINGIFY_ARG(COMPFLAGS)
  PetscViewerASCIIPrintf(v,"##                                                                       \n");
  PetscViewerASCIIPrintf(v,"## PROJ_CFLAGS = %s\n",STR_ARG_NAME);
  PetscViewerASCIIPrintf(v,"##                                                                       \n");
  ierr = projCheckCompilationFlags(v,STR_ARG_NAME);CHKERRQ(ierr);
#undef STR_ARG_NAME
#endif
  PetscViewerASCIIPrintf(v,"## ====================================================================================== \n");
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "projCreateDirectory"
PetscErrorCode projCreateDirectory(const char dirname[])
{
  PetscMPIInt rank;
  int num,error_number;
  PetscBool proj_log = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  
  /* Let rank 0 create a new directory on proc 0 */
  if (rank == 0) {
    num = mkdir(dirname,S_IRWXU);
    error_number = errno;
  }
  ierr = MPI_Bcast(&num,1,MPI_INT,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  ierr = MPI_Bcast(&error_number,1,MPI_INT,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  
  if (error_number == EEXIST) {
    if (proj_log) PetscPrintf(PETSC_COMM_WORLD,"[proj] Writing output to existing directory: %s\n",dirname);
  } else if (error_number == EACCES) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[proj] Write permission is denied for the parent directory in which the new directory is to be added");
  } else if (error_number == EMLINK) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[proj] The parent directory has too many links (entries)");
  } else if (error_number == ENOSPC) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[proj] The file system doesn't have enough room to create the new directory");
  } else if (error_number == ENOSPC) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"[proj] The parent directory of the directory being created is on a read-only file system and cannot be modified");
  } else {
    if (proj_log) PetscPrintf(PETSC_COMM_WORLD,"[proj] Created output directory: %s\n",dirname);
  }
  
  ierr = MPI_Barrier(PETSC_COMM_WORLD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "projGenerateFormattedTimestamp"
PetscErrorCode projGenerateFormattedTimestamp(char date_time[])
{
  time_t      currTime;
  struct tm*  timeInfo;
  int         adjustedYear;
  int         adjustedMonth;
  
  currTime = time( NULL );
  timeInfo = localtime( &currTime );
  /* See man localtime() for why to adjust these */
  adjustedYear = 1900 + timeInfo->tm_year;
  adjustedMonth = 1 + timeInfo->tm_mon;
  /* Format; MM(string) DD HH:MM:SS YYYY */
  /*
   printf( "%s %.2d %.2d:%.2d:%.2d %.4d \n",
   months[adjustedMonth], timeInfo->tm_mday,
   timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec, adjustedYear );
   */
  sprintf( date_time, "%.4d.%.2d.%.2d_%.2d:%.2d:%.2d",
          adjustedYear, adjustedMonth, timeInfo->tm_mday,
          timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec );
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "projWriteOptionsFile"
PetscErrorCode projWriteOptionsFile(const char filename[])
{
  PetscViewer viewer;
  char username[PETSC_MAX_PATH_LEN];
  char date[PETSC_MAX_PATH_LEN];
  char machine[PETSC_MAX_PATH_LEN];
  char prgname[PETSC_MAX_PATH_LEN];
  PetscBool proj_log = PETSC_FALSE;
  PetscErrorCode ierr;
  
  if (!filename) {
    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"proj.options",&viewer);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,filename,&viewer);CHKERRQ(ierr);
  }
  ierr = projWritePreamble(viewer);CHKERRQ(ierr);
  
  /* write header into options file */
  ierr = PetscGetUserName(username,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
  ierr = PetscGetDate(date,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
  ierr = PetscGetHostName(machine,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
  ierr = PetscGetProgramName(prgname,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"## ======================================================================================\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##   SubFUSC Options File\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##   Generated by user: %s\n",username);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##   Date             : %s\n",date);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##   Machine          : %s\n",machine);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##   Executable       : %s\n",prgname);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"##\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"## ======================================================================================\n");CHKERRQ(ierr);
  
  /* write options */
  ierr = PetscOptionsView(NULL,viewer);CHKERRQ(ierr);
  
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  if (proj_log) PetscPrintf(PETSC_COMM_WORLD,"[proj] Created options file: %s\n",filename);
  
  PetscFunctionReturn(0);
}

/* global variables for even logging */
PetscClassId PROJ_CLASSID;

extern PetscLogEvent _DataExchangerTopologySetup;
extern PetscLogEvent _DataExchangerBegin;
extern PetscLogEvent _DataExchangerEnd;
extern PetscLogEvent DSDE_DataExchangerBegin;
extern PetscLogEvent DSDE_DataExchangerEnd;

PetscLogStage stageLog_DMTET_Basis;
extern PetscLogEvent DMTET_BasisEvaluate;
extern PetscLogEvent DMTET_BasisDerivativeEvaluate;

PetscLogStage stageLog_PDE_Project;
extern PetscLogEvent PDE_ProjectF;
extern PetscLogEvent PDE_ProjectQ;
extern PetscLogEvent PDE_ProjectRHS;
extern PetscLogEvent PDE_ProjectAssemble;


/* prototypes for petsc implementations */
extern PetscErrorCode DMCreate_Tet(DM);
extern PetscErrorCode PCCreate_AuxSpace(PC pc);

#undef __FUNCT__
#define __FUNCT__ "projInitialize"
PetscErrorCode projInitialize(int *argc,char ***args,const char file[],const char help[])
{
  PetscErrorCode ierr;
  PetscBool isset,isset2,ignore_preamble = PETSC_FALSE,ignore_preamble2 = PETSC_FALSE;
  PetscBool ignore_logs = PETSC_FALSE,ignore_logs2 = PETSC_FALSE;
  PetscFunctionBegin;
  
  ierr = PetscInitialize(argc,args,file,help);CHKERRQ(ierr);
  
  ierr = PetscLogDefaultBegin();CHKERRQ(ierr);
  
  ierr = DMRegister("tet",DMCreate_Tet);CHKERRQ(ierr);
  ierr = PCRegister("auxspace",PCCreate_AuxSpace);CHKERRQ(ierr);
  
  ierr = PetscClassIdRegister("proj",&PROJ_CLASSID);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("DataExTopoSetup",PROJ_CLASSID,&_DataExchangerTopologySetup);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("DataExBegin",PROJ_CLASSID,&_DataExchangerBegin);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("DataExEnd",PROJ_CLASSID,&_DataExchangerEnd);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("DSDEDataExBegin",PROJ_CLASSID,&DSDE_DataExchangerBegin);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("DSDEDataExEnd",PROJ_CLASSID,&DSDE_DataExchangerEnd);CHKERRQ(ierr);

  
  ierr = PetscLogStageRegister("DMTetBasis",&stageLog_DMTET_Basis);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("BasisEval",PROJ_CLASSID,&DMTET_BasisEvaluate);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("BasisDerivEval",PROJ_CLASSID,&DMTET_BasisDerivativeEvaluate);CHKERRQ(ierr);
  
  ierr = PetscLogStageRegister("PDEProject",&stageLog_PDE_Project);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("ProjectField",PROJ_CLASSID,&PDE_ProjectF);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("ProjectQuadrature",PROJ_CLASSID,&PDE_ProjectQ);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("ProjectFormRHS",PROJ_CLASSID,&PDE_ProjectRHS);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("ProjectFormLHS",PROJ_CLASSID,&PDE_ProjectAssemble);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-ignore_preamble",&ignore_preamble,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-proj.ignore_preamble",&ignore_preamble2,NULL);CHKERRQ(ierr);
  if (!ignore_preamble && !ignore_preamble2) {
    ierr = projWritePreamble(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  
  project_output_path[0] = '\0';
  ierr = PetscOptionsGetString(NULL,NULL,"-output_path",project_output_path,PETSC_MAX_PATH_LEN,&isset);CHKERRQ(ierr);
  ierr = PetscOptionsGetString(NULL,NULL,"-proj.output_path",project_output_path,PETSC_MAX_PATH_LEN,&isset2);CHKERRQ(ierr);
  if (!isset && !isset2) {
    ierr = PetscSNPrintf(project_output_path,PETSC_MAX_PATH_LEN,"./output");CHKERRQ(ierr);
  }
  ierr = projCreateDirectory((const char*)project_output_path);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-ignore_option_view",&ignore_logs,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-proj.ignore_option_view",&ignore_logs2,NULL);CHKERRQ(ierr);
  if (!ignore_logs && !ignore_logs2) {
    char optionsfile[PETSC_MAX_PATH_LEN];
    char formatted_timestamp[PETSC_MAX_PATH_LEN];
    
    projGenerateFormattedTimestamp(formatted_timestamp);
    PetscSNPrintf(optionsfile,PETSC_MAX_PATH_LEN-1,"%s/proj.options-%s",project_output_path,formatted_timestamp);
    ierr = projWriteOptionsFile(optionsfile);CHKERRQ(ierr);
    
    PetscSNPrintf(optionsfile,PETSC_MAX_PATH_LEN-1,"%s/proj.options",project_output_path);
    ierr = projWriteOptionsFile(optionsfile);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "projGetOutputPath"
PetscErrorCode projGetOutputPath(const char **dname)
{
  if (!dname) PetscFunctionReturn(0);
  *dname = project_output_path;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "projFinalize"
PetscErrorCode projFinalize(void)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = PetscFinalize();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
