
#include <petsc.h>
#include <petscdm.h>
#include <dmbcs.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>

#undef __FUNCT__
#define __FUNCT__ "BCListIsDirichlet"
PetscErrorCode BCListIsDirichlet(PetscInt value,PetscBool *flg)
{
  PetscFunctionBegin;
  if (value==BCList_DIRICHLET) { *flg = PETSC_TRUE;  }
  else                         { *flg = PETSC_FALSE; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListInitialize"
PetscErrorCode BCListInitialize(BCList list)
{
  PetscInt n;
  
  PetscFunctionBegin;
  for (n=0; n<list->L; n++) {
    list->dofidx_global[n] = 0;
    list->scale_global[n]  = 1.0;
  }
  
  for (n=0; n<list->L_local; n++) {
    list->dofidx_local[n] = 0;
    list->vals_local[n]   = 0.0;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListCreate"
PetscErrorCode BCListCreate(MPI_Comm comm,BCList *list)
{
  BCList ll;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscMalloc( sizeof(struct _p_BCList),&ll);CHKERRQ(ierr);
  ierr = PetscMemzero(ll,sizeof(struct _p_BCList));CHKERRQ(ierr);
  ll->comm = comm;
  ll->allEmpty = PETSC_FALSE;
  *list = ll;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListDestroy"
PetscErrorCode BCListDestroy(BCList *list)
{
  BCList         ll = *list;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (ll->vals_global != ll->vals_local) {
    ierr = PetscFree(ll->vals_global);CHKERRQ(ierr);
    ierr = PetscFree(ll->vals_local);CHKERRQ(ierr);
  } else {
    ierr = PetscFree(ll->vals_global);CHKERRQ(ierr);
  }
  
  if (ll->dofidx_global != ll->dofidx_local) {
    ierr = PetscFree(ll->dofidx_global);CHKERRQ(ierr);
    ierr = PetscFree(ll->dofidx_local);CHKERRQ(ierr);
  } else {
    ierr = PetscFree(ll->dofidx_global);CHKERRQ(ierr);
  }
  ierr = PetscFree(ll->scale_global);CHKERRQ(ierr);
  
  ierr = DMDestroy(&ll->dm);CHKERRQ(ierr);
  ierr = PetscFree(ll);CHKERRQ(ierr);
  *list = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListSetSizes"
PetscErrorCode BCListSetSizes(BCList list,PetscInt bs,PetscInt N,PetscInt N_local)
{
  PetscErrorCode ierr;
  PetscReal mem_usage = 0.0;
  PetscMPIInt size;
  
  PetscFunctionBegin;
  list->blocksize = bs;
  list->N  = N;
  list->L  = bs * N;
  ierr = PetscMalloc( sizeof(PetscInt)*list->L,    &list->dofidx_global);CHKERRQ(ierr);   mem_usage += (PetscReal)(sizeof(PetscInt)*list->L);
  ierr = PetscMalloc( sizeof(PetscScalar)*list->L, &list->vals_global);CHKERRQ(ierr);     mem_usage += (PetscReal)(sizeof(PetscScalar)*list->L);
  ierr = PetscMalloc( sizeof(PetscScalar)*list->L, &list->scale_global); CHKERRQ(ierr);   mem_usage += (PetscReal)(sizeof(PetscScalar)*list->L);
  
  ierr = PetscMemzero(list->dofidx_global,sizeof(PetscInt)*list->L);CHKERRQ(ierr);
  ierr = PetscMemzero(list->vals_global,sizeof(PetscScalar)*list->L);CHKERRQ(ierr);
  ierr = PetscMemzero(list->scale_global,sizeof(PetscScalar)*list->L);CHKERRQ(ierr);
  
  ierr = MPI_Comm_size(list->comm,&size);CHKERRQ(ierr);
  if (size != 1) {
    list->N_local  = N_local;
    list->L_local  = bs * N_local;
    ierr = PetscMalloc( sizeof(PetscInt)*list->L_local,    &list->dofidx_local);CHKERRQ(ierr);  mem_usage += (PetscReal)(sizeof(PetscInt)*list->L_local);
    ierr = PetscMalloc( sizeof(PetscScalar)*list->L_local, &list->vals_local);CHKERRQ(ierr);    mem_usage += (PetscReal)(sizeof(PetscScalar)*list->L_local);
    
    ierr = PetscMemzero(list->dofidx_local,sizeof(PetscInt)*list->L_local);CHKERRQ(ierr);
    ierr = PetscMemzero(list->vals_local,sizeof(PetscScalar)*list->L_local);CHKERRQ(ierr);
  } else {
    list->N_local = list->N;
    list->L_local = list->L;
    list->dofidx_local = list->dofidx_global;
    list->vals_local = list->vals_global;
  }
  ierr = BCListInitialize(list);CHKERRQ(ierr);

  mem_usage = mem_usage * 1.0e-6;
  /*
  {
    PetscReal mem_usage_min,mem_usage_max;

    ierr = MPI_Allreduce( &mem_usage,&mem_usage_min,1,MPIU_REAL,MPIU_MIN,list->comm);CHKERRQ(ierr);
    ierr = MPI_Allreduce( &mem_usage,&mem_usage_max,1,MPIU_REAL,MPIU_MAX,list->comm);CHKERRQ(ierr);
    PetscPrintf(PetscObjectComm((PetscObject)list->dm),"BCList: Mem. usage (min,max) = %1.2e,%1.2e (MB) \n", mem_usage_min, mem_usage_max );
  }
  */
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListUpdateCache"
PetscErrorCode BCListUpdateCache(BCList list)
{
  PetscInt       n,cnt;
  PetscBool      isdir;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  cnt = 0;
  for (n=0; n<list->L; n++) {
    ierr = BCListIsDirichlet(list->dofidx_global[n],&isdir);CHKERRQ(ierr);
    if (!isdir) { cnt++; }
  }
  list->allEmpty = PETSC_FALSE;
  if (cnt==list->L) { list->allEmpty = PETSC_TRUE; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListInitGlobal"
PetscErrorCode BCListInitGlobal(BCList list)
{
  ISLocalToGlobalMapping ltog;
  PetscInt i,max,lsize;
  const PetscInt *indices;
  Vec dindices,dindices_g;
  PetscScalar *_dindices;
  PetscMPIInt size;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(list->comm,&size);CHKERRQ(ierr);
  if (size == 1) {
    for (i=0; i<list->L; i++) {
      list->dofidx_global[i] = i;
    }
    PetscFunctionReturn(0);
  }
  
  ierr = DMGetLocalToGlobalMapping(list->dm, &ltog);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetSize(ltog, &max);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(ltog, &indices);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
  ierr = DMGetLocalVector(list->dm,&dindices);CHKERRQ(ierr);
  ierr = VecGetLocalSize(dindices,&lsize);CHKERRQ(ierr);
  if (lsize!=max) { SETERRQ2(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"[1] Sizes don't match. l2g (%D) : local vec (%D)",max,lsize); }
  ierr = VecGetArray(dindices,&_dindices);CHKERRQ(ierr);
  /* convert to scalar */
  for (i=0; i<lsize; i++) {
    _dindices[i] = (PetscScalar)indices[i] + 1.0e-3;
  }
  ierr = VecRestoreArray(dindices,&_dindices);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingRestoreIndices(ltog, &indices);CHKERRQ(ierr);
  
  /* scatter (ignore ghosts) */
  ierr = DMLocalToGlobalBegin(list->dm,dindices,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(  list->dm,dindices,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
  
  /* convert to int */
  ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
  if (list->L!=lsize) { SETERRQ2(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"[2] Sizes don't match. list->L (%D) : local vec (%D)",list->L,lsize); }
  ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
  for (i=0; i<lsize; i++) {
    list->dofidx_global[i] = (PetscInt)_dindices[i];
  }
  ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
  
  ierr = DMRestoreGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(list->dm,&dindices);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGlobalToLocal"
PetscErrorCode BCListGlobalToLocal(BCList list)
{
  PetscInt i,lsize;
  Vec dindices,dindices_g;
  PetscScalar *_dindices;
  PetscBool is_dirich;
  PetscMPIInt size;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(list->comm,&size);CHKERRQ(ierr);
  if (size == 1) {
    /*
    for (i=0; i<list->L; i++) {
      list->dofidx_local[i] = list->dofidx_global[i];
      list->vals_local[i] = list->vals_global[i];
    }
    */
    PetscFunctionReturn(0);
  }
  
  ierr = DMGetGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
  ierr = DMGetLocalVector(list->dm,&dindices);CHKERRQ(ierr);
  
  /* clean up indices (global -> local) */
  ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
  if (lsize!=list->L) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"Sizes don't match 1"); }
  ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
  /* convert to scalar and copy values */
  for (i=0; i<lsize; i++) {
    BCListIsDirichlet(list->dofidx_global[i],&is_dirich);
    if (is_dirich) {
      _dindices[i] = (PetscScalar)list->dofidx_global[i] - 1.0e-3;
    } else {
      _dindices[i] = (PetscScalar)list->dofidx_global[i] + 1.0e-3;
    }
  }
  ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
  
  /* scatter (ignore ghosts) */
  ierr = DMGlobalToLocalBegin(list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
  
  /* convert to int */
  ierr = VecGetLocalSize(dindices,&lsize);CHKERRQ(ierr);
  if (list->L_local!=lsize) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"Sizes don't match 2"); }
  ierr = VecGetArray(dindices,&_dindices);CHKERRQ(ierr);
  for (i=0; i<lsize; i++) {
    list->dofidx_local[i] = (PetscInt)_dindices[i];
  }
  ierr = VecRestoreArray(dindices,&_dindices);CHKERRQ(ierr);
  
  /* setup values (global -> local) */
  ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
  ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
  /* copy global values into a vector */
  for (i=0; i<lsize; i++) {
    _dindices[i] = list->vals_global[i];
  }
  ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
  
  /* scatter (ignore ghosts) */
  ierr = DMGlobalToLocalBegin(list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
  
  /* retrieve entries */
  ierr = VecGetLocalSize(dindices,&lsize);CHKERRQ(ierr);
  ierr = VecGetArray(dindices,&_dindices);CHKERRQ(ierr);
  for (i=0; i<lsize; i++) {
    list->vals_local[i] = _dindices[i];
  }
  ierr = VecRestoreArray(dindices,&_dindices);CHKERRQ(ierr);
  
  ierr = DMRestoreGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(list->dm,&dindices);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListLocalToGlobal"
PetscErrorCode BCListLocalToGlobal(BCList list)
{
  PetscInt i,lsize;
  Vec dindices_l,dindices_g;
  PetscScalar *_dindices;
  PetscBool is_dirich;
  PetscMPIInt size;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(list->comm,&size);CHKERRQ(ierr);
  if (size == 1) {
    PetscFunctionReturn(0);
  }
  
  ierr = DMGetGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
  ierr = DMGetLocalVector(list->dm,&dindices_l);CHKERRQ(ierr);
  
  /* clean up indices (local -> global) */
  ierr = VecGetLocalSize(dindices_l,&lsize);CHKERRQ(ierr);
  if (lsize!=list->L_local) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"Sizes don't match 1"); }
  ierr = VecGetArray(dindices_l,&_dindices);CHKERRQ(ierr);
  /* convert to scalar and copy values */
  for (i=0; i<lsize; i++) {
    BCListIsDirichlet(list->dofidx_local[i],&is_dirich);
    if (is_dirich) {
      _dindices[i] = (PetscScalar)list->dofidx_local[i] - 1.0e-3;
    } else {
      _dindices[i] = (PetscScalar)list->dofidx_local[i] + 1.0e-3;
    }
  }
  ierr = VecRestoreArray(dindices_l,&_dindices);CHKERRQ(ierr);
  
  /* scatter (ignore ghosts) */
  ierr = DMLocalToGlobalBegin(list->dm,dindices_l,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(  list->dm,dindices_l,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
  
  /* convert to int */
  ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
  if (list->L!=lsize) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"Sizes don't match 2"); }
  ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
  for (i=0; i<lsize; i++) {
    list->dofidx_global[i] = (PetscInt)_dindices[i];
  }
  ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
  
  /* setup values (local -> global) */
  ierr = VecGetLocalSize(dindices_l,&lsize);CHKERRQ(ierr);
  ierr = VecGetArray(dindices_l,&_dindices);CHKERRQ(ierr);
  /* copy global values into a vector */
  for (i=0; i<lsize; i++) {
    _dindices[i] = list->vals_local[i];
  }
  ierr = VecRestoreArray(dindices_l,&_dindices);CHKERRQ(ierr);
  
  /* scatter (ignore ghosts) */
  ierr = DMLocalToGlobalBegin(list->dm,dindices_l,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(  list->dm,dindices_l,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
  
  /* retrieve entries */
  ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
  ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
  for (i=0; i<lsize; i++) {
    list->vals_global[i] = _dindices[i];
  }
  ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
  
  ierr = DMRestoreGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(list->dm,&dindices_l);CHKERRQ(ierr);
  
  ierr = BCListUpdateCache(list);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* read/write */
#undef __FUNCT__
#define __FUNCT__ "BCListGetGlobalIndices"
PetscErrorCode BCListGetGlobalIndices(BCList list,PetscInt *n,PetscInt **idx)
{
  PetscFunctionBegin;
  if (n) {   *n   = list->L; }
  if (idx) { *idx = list->dofidx_global; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListRestoreGlobalIndices"
PetscErrorCode BCListRestoreGlobalIndices(BCList list,PetscInt *n,PetscInt **idx)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (idx) {
    if (*idx != list->dofidx_global) {
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"idx doesn't match");
    }
  }
  /* update cached info */
  ierr = BCListUpdateCache(list);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGetGlobalValues"
PetscErrorCode BCListGetGlobalValues(BCList list,PetscInt *n,PetscScalar **vals)
{
  PetscFunctionBegin;
  if (n) {   *n     = list->L; }
  if (vals) { *vals = list->vals_global; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGetLocalIndices"
PetscErrorCode BCListGetLocalIndices(BCList list,PetscInt *n,PetscInt **idx)
{
  PetscFunctionBegin;
  if (n) {   *n   = list->L_local; }
  if (idx) { *idx = list->dofidx_local; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGetLocalValues"
PetscErrorCode BCListGetLocalValues(BCList list,PetscInt *n,PetscScalar **vals)
{
  PetscFunctionBegin;
  if (n) {   *n     = list->L_local; }
  if (vals) { *vals = list->vals_local; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGetDofIdx"
PetscErrorCode BCListGetDofIdx(BCList list,PetscInt *Lg,PetscInt **dofidx_global,PetscInt *Ll,PetscInt **dofidx_local)
{
  PetscFunctionBegin;
  if (Lg)            { *Lg   = list->L; }
  if (dofidx_global) { *dofidx_global = list->dofidx_global; }
  if (Ll)            { *Ll   = list->L_local; }
  if (dofidx_local)  { *dofidx_local = list->dofidx_local; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListEvaluator_constant"
PetscBool BCListEvaluator_constant( PetscScalar position[], PetscScalar *value, void *ctx )
{
  PetscBool impose_dirichlet = PETSC_TRUE;
  PetscScalar dv = *((PetscScalar*)ctx);
  
  PetscFunctionBegin;
  *value = dv;
  return impose_dirichlet;
}

/* matrix free stuff */
/*
 if (isbc_local[i] == dirch) y[i] = xbc[i]
 else                        y[i] = y[i]
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsert"
PetscErrorCode BCListInsert(BCList list,Vec y)
{
  PetscInt m,k,L;
  PetscInt *idx;
  PetscScalar *LA_x,*LA_y;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = BCListGetGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetGlobalValues(list,&L,&LA_x);CHKERRQ(ierr);
  ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
  ierr = VecGetLocalSize(y,&m);CHKERRQ(ierr);
  for (k=0; k<m; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_y[k] = LA_x[k];
    }
  }
  ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
  ierr = BCListRestoreGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListInsertValueIntoDirichletSlot"
PetscErrorCode BCListInsertValueIntoDirichletSlot(BCList list,PetscScalar value,Vec y)
{
  PetscInt m,k,L;
  PetscInt *idx;
  PetscScalar *LA_y;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = BCListGetGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
  ierr = VecGetLocalSize(y,&m);CHKERRQ(ierr);
  
  if (L!=m) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"L != m");
  }
  for (k=0; k<m; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_y[k] = value;
    }
  }
  ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
  ierr = BCListRestoreGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 if (isbc_local[i] == dirch) y[i] = 0.0
 else                        y[i] = y[i]
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsertZero"
PetscErrorCode BCListInsertZero(BCList list,Vec y)
{
  PetscInt m,k,L;
  PetscInt *idx;
  PetscScalar *LA_x,*LA_y;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = BCListGetGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetGlobalValues(list,&L,&LA_x);CHKERRQ(ierr);
  ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
  ierr = VecGetLocalSize(y,&m);CHKERRQ(ierr);
  for (k=0; k<m; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_y[k] = 0.0;
    }
  }
  ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
  ierr = BCListRestoreGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 if (isbc_local[i] == dirch) y[i] = xbc[i]
 else                        y[i] = y[i]
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsertLocal"
PetscErrorCode BCListInsertLocal(BCList list,Vec y)
{
  PetscInt M,k,L;
  const PetscInt *idx;
  PetscScalar *LA_x,*LA_y;
  PetscBool is_seq = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  L    = list->L_local;
  idx  = list->dofidx_local;
  LA_x = list->vals_local;
  ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
  ierr = VecGetSize(y,&M);CHKERRQ(ierr);
  
  /* debug error checking */
  if (L!=M) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_SIZ,"Sizes do not match"); };
  ierr = PetscObjectTypeCompare((PetscObject)y,VECSEQ,&is_seq);CHKERRQ(ierr);
  if (!is_seq) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_WRONG,"Vec must be VECSEQ, i.e. a local (ghosted) vec"); };
  
  for (k=0; k<M; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_y[k] = LA_x[k];
    }
  }
  ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 if (isbc_local[i] == dirch) y[i] = 0.0
 else                        y[i] = y[i]
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsertLocalZero"
PetscErrorCode BCListInsertLocalZero(BCList list,Vec y)
{
  PetscInt M,k,L;
  const PetscInt *idx;
  PetscScalar *LA_y;
  PetscBool is_seq = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  L    = list->L_local;
  idx  = list->dofidx_local;
  ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
  ierr = VecGetSize(y,&M);CHKERRQ(ierr);
  
  /* debug error checking */
  if (L!=M) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_SIZ,"Sizes do not match"); };
  ierr = PetscObjectTypeCompare((PetscObject)y,VECSEQ,&is_seq);CHKERRQ(ierr);
  if (!is_seq) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_WRONG,"Vec must be VECSEQ, i.e. a local (ghosted) vec"); };
  
  for (k=0; k<M; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_y[k] = 0.0;
    }
  }
  ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 Apply's
 F = scale(X-phi) where ever a dirichlet is encountered.
 */
#undef __FUNCT__
#define __FUNCT__ "BCListResidualDirichlet"
PetscErrorCode BCListResidualDirichlet(BCList list,const Vec X,Vec F)
{
  PetscInt m,k,L;
  const PetscInt *idx;
  PetscScalar *LA_S,*LA_F,*LA_phi;
  const PetscScalar *LA_X;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!X) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_NULL,"Vec X cannot be NULL"); }
  if (!F) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_NULL,"Vec F cannot be NULL"); }
  
  L      = list->L;
  idx    = list->dofidx_global;
  LA_phi = list->vals_global;
  LA_S   = list->scale_global;
  
  ierr = VecGetArrayRead(X,&LA_X);CHKERRQ(ierr);
  ierr = VecGetArray    (F,&LA_F);CHKERRQ(ierr);
  
  /* debug error checking */
  ierr = VecGetLocalSize(X,&m);CHKERRQ(ierr);
  if (L!=m) { SETERRQ(PetscObjectComm((PetscObject)X),PETSC_ERR_ARG_SIZ,"Sizes do not match (X)"); };
  ierr = VecGetLocalSize(F,&m);CHKERRQ(ierr);
  if (L!=m) { SETERRQ(PetscObjectComm((PetscObject)F),PETSC_ERR_ARG_SIZ,"Sizes do not match (F)"); };
  
  for (k=0; k<m; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_F[k] = LA_S[k]*(LA_X[k] - LA_phi[k]);
    }
  }
  ierr = VecRestoreArrayRead(X,&LA_X);CHKERRQ(ierr);
  ierr = VecRestoreArray    (F,&LA_F);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 Apply's
 F = scale(X) where ever a dirichlet is encountered.
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsertDirichlet_MatMult"
PetscErrorCode BCListInsertDirichlet_MatMult(BCList list,const Vec X,Vec F)
{
  PetscInt m,k,L;
  const PetscInt *idx;
  PetscScalar *LA_S,*LA_F;
  const PetscScalar *LA_X;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!X) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_NULL,"Vec X cannot be NULL"); }
  if (!F) { SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_ARG_NULL,"Vec F cannot be NULL"); }
  
  L      = list->L;
  idx    = list->dofidx_global;
  LA_S   = list->scale_global;
  
  ierr = VecGetArrayRead(X,&LA_X);CHKERRQ(ierr);
  ierr = VecGetArray    (F,&LA_F);CHKERRQ(ierr);
  
  /* debug error checking */
  ierr = VecGetLocalSize(X,&m);CHKERRQ(ierr);
  if (L!=m) { SETERRQ(PetscObjectComm((PetscObject)X),PETSC_ERR_ARG_SIZ,"Sizes do not match (X)"); };
  ierr = VecGetLocalSize(F,&m);CHKERRQ(ierr);
  if (L!=m) { SETERRQ(PetscObjectComm((PetscObject)F),PETSC_ERR_ARG_SIZ,"Sizes do not match (F)"); };
  
  for (k=0; k<m; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      LA_F[k] = LA_S[k]*LA_X[k];
    }
  }
  ierr = VecRestoreArrayRead(X,&LA_X);CHKERRQ(ierr);
  ierr = VecRestoreArray    (F,&LA_F);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListApplyDirichletMask"
PetscErrorCode BCListApplyDirichletMask(PetscInt N_EQNS, PetscInt gidx[],BCList list)
{
  PetscInt k,L;
  PetscInt *idx;
  
  PetscFunctionBegin;
  L   = list->L_local;
  idx = list->dofidx_local;
  for (k=0; k<L; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      gidx[k] = - ( gidx[k] + 1 );
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListRemoveDirichletMask"
PetscErrorCode BCListRemoveDirichletMask(PetscInt N_EQNS, PetscInt gidx[],BCList list)
{
  PetscInt k,L;
  PetscInt *idx;
  
  PetscFunctionBegin;
  L   = list->L_local;
  idx = list->dofidx_local;
  for (k=0; k<L; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      gidx[k] = - gidx[k] - 1;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListInsertScaling"
PetscErrorCode BCListInsertScaling(Mat A,PetscInt N_EQNS, PetscInt gidx[],BCList list)
{
  PetscInt k,L;
  PetscInt *idx;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  L   = list->L_local;
  idx = list->dofidx_local;
  for (k=0; k<L; k++) {
    if (idx[k]==BCList_DIRICHLET) {
      //printf("local index %d is dirichlet--->inserted into %d\n", k,gidx[k]);
      ierr = MatSetValueLocal(A,gidx[k],gidx[k],1.0,INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListZeroRows"
PetscErrorCode BCListZeroRows(BCList list,Mat A)
{
  PetscInt count,i;
  PetscInt *ridx;
  PetscErrorCode ierr;
  ISLocalToGlobalMapping ltog;
  PetscInt max;
  const PetscInt *indices;

  ierr = DMGetLocalToGlobalMapping(list->dm, &ltog);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetSize(ltog, &max);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(ltog, &indices);CHKERRQ(ierr);

  ierr = PetscMalloc1(list->L_local,&ridx);CHKERRQ(ierr);
  count = 0;
  for (i=0; i<list->L_local; i++) {
    PetscInt gidx;
    
    gidx = list->dofidx_local[i];
    if (gidx == BCList_DIRICHLET) {
      /* convert */
      ridx[count] = indices[i];
      count++;
    }
  }
  ierr = ISLocalToGlobalMappingRestoreIndices(ltog, &indices);CHKERRQ(ierr);
  
  ierr = MatZeroRows(A,count,ridx,1.0,NULL,NULL);CHKERRQ(ierr);
  ierr = PetscFree(ridx);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListZeroRowsColumns"
PetscErrorCode BCListZeroRowsColumns(BCList list,Mat A)
{
  PetscInt count,i;
  PetscInt *ridx;
  PetscErrorCode ierr;
  ISLocalToGlobalMapping ltog;
  PetscInt max;
  const PetscInt *indices;
  
  ierr = DMGetLocalToGlobalMapping(list->dm, &ltog);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetSize(ltog, &max);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(ltog, &indices);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(list->L_local,&ridx);CHKERRQ(ierr);
  count = 0;
  for (i=0; i<list->L_local; i++) {
    PetscInt gidx;
    
    gidx = list->dofidx_local[i];
    if (gidx == BCList_DIRICHLET) {
      /* convert */
      ridx[count] = indices[i];
      count++;
    }
  }
  ierr = ISLocalToGlobalMappingRestoreIndices(ltog, &indices);CHKERRQ(ierr);
  
  ierr = MatZeroRowsColumns(A,count,ridx,1.0,NULL,NULL);CHKERRQ(ierr);
  ierr = PetscFree(ridx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMBCListCreate"
PetscErrorCode DMBCListCreate(DM da,BCList *list)
{
  BCList ll;
  MPI_Comm comm;
  Vec x;
  PetscInt N,Ng,bs;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = DMCreateGlobalVector(da,&x);CHKERRQ(ierr);
  ierr = VecGetLocalSize(x,&N);CHKERRQ(ierr);
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  N = N/bs;
  
  ierr = DMCreateLocalVector(da,&x);CHKERRQ(ierr);
  ierr = VecGetLocalSize(x,&Ng);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  Ng = Ng/bs;
  
  comm = PetscObjectComm((PetscObject)da);
  ierr = BCListCreate(comm,&ll);CHKERRQ(ierr);
  ll->dm = da;
  ierr = PetscObjectReference((PetscObject)da);CHKERRQ(ierr);
  ierr = BCListSetSizes(ll,bs,N,Ng);CHKERRQ(ierr);
  ierr = BCListInitialize(ll);CHKERRQ(ierr);
  
  ierr = BCListInitGlobal(ll);CHKERRQ(ierr);
  ierr = BCListGlobalToLocal(ll);CHKERRQ(ierr);
  
  *list = ll;
  PetscFunctionReturn(0);
}

#include <dmtetimpl.h>
#include <petscdmtet.h>

/*
 
 PetscBool *eval(PetscScalar*,PetscScalar*,void*)
 
 PetscBool my_bc_evaluator( PetscScalar position[], PetscScalar *value, void *ctx )
 {
 PetscBool impose_dirichlet = PETSC_FALSE;
 
 if (position[0]<1.0) {
 *value = 10.0;
 impose_dirichlet = PETSC_TRUE;
 }
 
 return impose_dirichlet;
 }
 
 */

/*
 PetscBool *eval(PetscScalar*,void*)
 PetscBool my_bc_evaluator(PetscScalar *value,void *ctx)
*/
#undef __FUNCT__
#define __FUNCT__ "BCListDefineDirichlet_TraverseEdges_DMTet"
PetscErrorCode BCListDefineDirichlet_TraverseEdges_DMTet(BCList list,DM dm,PetscInt marker,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,void*),void *ctx)
{
  PetscFunctionBegin;
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Not implemented yet");
  PetscFunctionReturn(0);
}

/*
 PetscBool *eval(PetscScalar*,void*)
 PetscBool my_bc_evaluator(PetscScalar *value,void *ctx)
 */
#undef __FUNCT__
#define __FUNCT__ "BCListDefineDirichlet_TraverseCells_DMTet"
PetscErrorCode BCListDefineDirichlet_TraverseCells_DMTet(BCList list,DM dm,PetscInt marker,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,void*),void *ctx)
{
  PetscFunctionBegin;
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Not implemented yet");
  PetscFunctionReturn(0);
}

/*
 PetscBool *eval(PetscScalar*,PetscScalar*,void*)
 PetscBool my_bc_evaluator(PetscScalar *coor,PetscScalar *value,void *ctx)
*/
#undef __FUNCT__
#define __FUNCT__ "BCListDefineDirichlet_TraverseAllNodes_DMTet"
PetscErrorCode BCListDefineDirichlet_TraverseAllNodes_DMTet(BCList list,DM dm,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx)
{
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Not implemented yet");
  PetscFunctionReturn(0);
}

/*
 PetscBool *eval(PetscScalar*,PetscScalar*,void*)
 PetscBool my_bc_evaluator(PetscScalar *coor,PetscScalar *value,void *ctx)
*/
#undef __FUNCT__
#define __FUNCT__ "BCListDefineDirichlet_TraverseBoundaryNodes_DMTet"
PetscErrorCode BCListDefineDirichlet_TraverseBoundaryNodes_DMTet(BCList list,DM dm,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx)
{
  PetscFunctionBegin;
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Not implemented yet");
  PetscFunctionReturn(0);
}

/*
 Dolfin style methods to
 ( i) create a boundary
 (ii) set values on the boundary
*/

/*
 PetscBool *boundary_query(PetscScalar*,PetscScalar*,void*)
 PetscBool boundary_query(PetscScalar *coor,void *ctx)
*/
#
#undef __FUNCT__
#define __FUNCT__ "DMTetCreateDirichletList_TraverseBoundaryNodes"
PetscErrorCode DMTetCreateDirichletList_TraverseBoundaryNodes(DM dm,PetscBool (*boundary_query)(PetscScalar*,void*),void *ctx,BCList *list)
{
  /*
  BCList bclist;
  DM_TET *tet;
  PetscInt i,d;
  PetscErrorCode ierr;
  */
  PetscFunctionBegin;
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"No longer supported");
#if 0
  ierr = DMBCListCreate(dm,&bclist);CHKERRQ(ierr);
  if (!boundary_query) SETERRQ(bclist->comm,PETSC_ERR_USER,"A valid boundary identifying function must be provided");
  tet = (DM_TET*)dm->data;
  
  if (!tet->space->node_marker) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"node_marker[] list for high order space is not implemented");
  
  /* traverse boundary nodes, apply bool eval, set candidate flag */
  for (i=0; i<tet->space->nnodes; i++) {
    if (tet->space->node_marker[i] > 0) {
      PetscBool is_dirich = PETSC_FALSE;
      PetscScalar coor[3];
      
      for (d=0; d<bclist->blocksize; d++) {
        bclist->dofidx_global[bclist->blocksize*i + d] = BCList_UNCONSTRAINED;
      }

      coor[0] = tet->space->coords[2*i+0];
      coor[1] = tet->space->coords[2*i+1];
      coor[2] = 0.0;
      is_dirich = boundary_query(coor,ctx);CHKERRQ(ierr);
      if (is_dirich) {
        for (d=0; d<bclist->blocksize; d++) {
          bclist->dofidx_global[bclist->blocksize*i + d] = BCList_DIRICHLET_CANDIDATE;
        }
      }
    }
  }
  bclist->allEmpty = PETSC_FALSE;
  *list = bclist;
#endif
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateDirichletList_Default"
PetscErrorCode DMTetCreateDirichletList_Default(DM dm,BCList *list)
{
  BCList bclist;
  DM_TET *tet;
  PetscInt i,d;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  tet = (DM_TET*)dm->data;
  ierr = DMBCListCreate(dm,&bclist);CHKERRQ(ierr);
  
  /* traverse all nodes and set them all as candidates */
  for (i=0; i<tet->space->nnodes; i++) {
    for (d=0; d<bclist->blocksize; d++) {
      bclist->dofidx_global[bclist->blocksize*i + d] = BCList_DIRICHLET_CANDIDATE;
    }
  }
  bclist->allEmpty = PETSC_FALSE;
  *list = bclist;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListSetDirichletValue"
PetscErrorCode BCListSetDirichletValue(BCList list,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),void *ctx)
{
  DM_TET *tet;
  DM dm;
  PetscInt i;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(list->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = list->dm;
  if (!dm) SETERRQ(list->comm,PETSC_ERR_USER,"No DM has been set");
  tet = (DM_TET*)dm->data;
  
  if (dof_idx > list->blocksize) SETERRQ1(list->comm,PETSC_ERR_USER,"DOF index must be less than %D",list->blocksize);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  for (i=0; i<list->N; i++) {
    PetscInt loc;
    PetscScalar coor[3],value;
    PetscBool constrain = PETSC_FALSE;
    
    loc = list->blocksize * i + dof_idx;
    
    coor[0] = tet->space->coords[2*i+0];
    coor[1] = tet->space->coords[2*i+1];
    coor[2] = 0.0;
    ierr = eval(coor,dof_idx,&value,&constrain,ctx);CHKERRQ(ierr);
    if (constrain) {
      list->dofidx_global[loc] = BCList_DIRICHLET;
      list->vals_global[loc]   = value;
    } else {
      list->dofidx_global[loc] = BCList_UNCONSTRAINED;
    }
  }
  list->allEmpty = PETSC_FALSE;
  
  ierr = BCListGlobalToLocal(list);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateDirichletList_TraverseAllNodes"
PetscErrorCode DMTetCreateDirichletList_TraverseAllNodes(DM dm,PetscBool (*boundary_query)(PetscScalar*,void*),void *ctx,BCList *list)
{
  BCList bclist;
  DM_TET *tet;
  PetscInt i,d;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = DMBCListCreate(dm,&bclist);CHKERRQ(ierr);
  if (!boundary_query) SETERRQ(bclist->comm,PETSC_ERR_USER,"A valid boundary identifying function must be provided");
  tet = (DM_TET*)dm->data;
  
  /* traverse boundary nodes, apply bool eval, set candidate flag */
  for (i=0; i<tet->space->nnodes; i++) {
    PetscBool is_dirich = PETSC_FALSE;
    PetscScalar coor[3];

    for (d=0; d<bclist->blocksize; d++) {
      bclist->dofidx_local[bclist->blocksize*i + d] = BCList_UNCONSTRAINED;
    }
    
    coor[0] = tet->space->coords[2*i+0];
    coor[1] = tet->space->coords[2*i+1];
    coor[2] = 0.0;
    is_dirich = boundary_query(coor,ctx);CHKERRQ(ierr);
    if (is_dirich) {
      for (d=0; d<bclist->blocksize; d++) {
        bclist->dofidx_local[bclist->blocksize*i + d] = BCList_DIRICHLET_CANDIDATE;
      }
    }
  }
  bclist->allEmpty = PETSC_FALSE;
  ierr = BCListLocalToGlobal(bclist);CHKERRQ(ierr);
  *list = bclist;
  PetscFunctionReturn(0);
}

/*
 PetscBool *eval(PetscScalar*,PetscScalar*,void*)
 PetscErrorCode eval(PetscScalar *coor,PetscScalar *val,void *ctx)
 */
#undef __FUNCT__
#define __FUNCT__ "BCListDefineDirichletValue"
PetscErrorCode BCListDefineDirichletValue(BCList list,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx)
{
  DM_TET *tet;
  DM dm;
  PetscInt i;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(list->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = list->dm;
  if (!dm) SETERRQ(list->comm,PETSC_ERR_USER,"No DM has been set");
  tet = (DM_TET*)dm->data;
  
  if (dof_idx > list->blocksize) SETERRQ1(list->comm,PETSC_ERR_USER,"DOF index must be less than %D",list->blocksize);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  for (i=0; i<list->N_local; i++) {
    PetscInt loc;
    PetscScalar coor[3],value;
    
    loc = list->blocksize * i + dof_idx;
    
    if (list->dofidx_local[loc] == BCList_UNCONSTRAINED) { continue; }
    
    list->dofidx_local[loc] = BCList_DIRICHLET;

    coor[0] = tet->space->coords[2*i+0];
    coor[1] = tet->space->coords[2*i+1];
    coor[2] = 0.0;
    ierr = eval(coor,&value,ctx);CHKERRQ(ierr);
    
    list->vals_local[loc] = value;
  }
  list->allEmpty = PETSC_FALSE;
  
	ierr = BCListLocalToGlobal(list);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListMerge"
PetscErrorCode BCListMerge(PetscInt n,BCList input[],BCList *list)
{
  BCList bclist;
  PetscInt i,j;
  DM dm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  dm = input[0]->dm;
  if (!dm) SETERRQ(input[0]->comm,PETSC_ERR_USER,"No DM has been set");
  for (i=1; i<n; i++) {
    if (dm != input[i]->dm) SETERRQ(input[0]->comm,PETSC_ERR_USER,"DM on all BCList must match");
  }
  
  ierr = DMBCListCreate(dm,&bclist);CHKERRQ(ierr);
  
  /* traverse entries - keep those markes as Dirichlet */
  for (i=0; i<bclist->L; i++) {
    bclist->dofidx_global[i] = BCList_UNCONSTRAINED;
  }
  for (j=0; j<n; j++) {
    for (i=0; i<bclist->L; i++) {
      if (input[j]->dofidx_global[i] != BCList_UNCONSTRAINED) {
        bclist->dofidx_global[i] = input[j]->dofidx_global[i];
      }
      /* superceed all choices with Dirichlet */
      if (input[j]->dofidx_global[i] == BCList_DIRICHLET) {
        bclist->dofidx_global[i] = BCList_DIRICHLET;
        bclist->vals_global[i] = input[j]->vals_global[i];
      }
    }
  }
	ierr = BCListGlobalToLocal(bclist);CHKERRQ(ierr);
  bclist->allEmpty = PETSC_FALSE;
  *list = bclist;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListReset"
PetscErrorCode BCListReset(BCList list)
{
  PetscInt n;
  
  PetscFunctionBegin;
  for (n=0; n<list->L; n++) {
    list->dofidx_global[n] = BCList_UNCONSTRAINED;
    list->scale_global[n]  = 1.0;
    list->vals_global[n]   = 0.0;
  }
  
  if (list->vals_global != list->vals_local) {
    for (n=0; n<list->L_local; n++) {
      list->vals_local[n] = 0.0;
    }
  }
  if (list->dofidx_global != list->dofidx_local) {
    for (n=0; n<list->L_local; n++) {
      list->dofidx_local[n] = BCList_UNCONSTRAINED;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBCListTraverse"
PetscErrorCode DMTetBCListTraverse(BCList list,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),void *ctx)
{
  DM_TET *tet;
  DM dm;
  PetscInt i;
  PetscBool istet = PETSC_FALSE;
  PetscInt L,*idx;
  PetscScalar *vals;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(list->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = list->dm;
  if (!dm) SETERRQ(list->comm,PETSC_ERR_USER,"No DM has been set");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(list->comm,PETSC_ERR_SUP,"Only valid for DMTET");
  
  tet = (DM_TET*)dm->data;
  
  if (dof_idx > list->blocksize) SETERRQ1(list->comm,PETSC_ERR_USER,"DOF index must be less than %D",list->blocksize);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  ierr = BCListGetLocalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetLocalValues(list,&L,&vals);CHKERRQ(ierr);

  for (i=0; i<tet->space->nnodes; i++) {
    PetscInt loc;
    PetscScalar coor[3],value;
    PetscBool constrain = PETSC_FALSE;
    
    loc = list->blocksize * i + dof_idx;
    
    coor[0] = tet->space->coords[2*i+0];
    coor[1] = tet->space->coords[2*i+1];
    coor[2] = 0.0;
    ierr = eval(coor,dof_idx,&value,&constrain,ctx);CHKERRQ(ierr);
    if (constrain) {
      idx[loc] = BCList_DIRICHLET;
      vals[loc]   = value;
    }
  }

  ierr = BCListLocalToGlobal(list);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBCListUserTraverse"
PetscErrorCode DMTetBCListUserTraverse(BCList list,PetscInt dof_idx,
                    PetscErrorCode (*eval)(DM,PetscInt,PetscScalar*,PetscInt*,PetscScalar*,void*),
                    void *ctx)
{
  DM_TET *tet;
  DM dm;
  PetscInt i;
  PetscBool istet = PETSC_FALSE;
  PetscInt L,*idx,*dof_idxlist;
  PetscScalar *vals,*dof_vallist;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(list->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = list->dm;
  if (!dm) SETERRQ(list->comm,PETSC_ERR_USER,"No DM has been set");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(list->comm,PETSC_ERR_SUP,"Only valid for DMTET");
  
  tet = (DM_TET*)dm->data;
  
  if (dof_idx > list->blocksize) SETERRQ1(list->comm,PETSC_ERR_USER,"DOF index must be less than %D",list->blocksize);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  ierr = BCListGetLocalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetLocalValues(list,&L,&vals);CHKERRQ(ierr);

  ierr = PetscMalloc1(tet->space->nnodes,&dof_idxlist);CHKERRQ(ierr);
  ierr = PetscMalloc1(tet->space->nnodes,&dof_vallist);CHKERRQ(ierr);
  
  /* initialize dof list by insert existing values/indices for dof_idx */
  for (i=0; i<tet->space->nnodes; i++) {
    PetscInt loc;
    
    loc = list->blocksize * i + dof_idx;
    dof_idxlist[i] = idx[loc];
    dof_vallist[i] = vals[loc];
  }
  
  /* call user function which makes additional insertitions */
  ierr = eval(dm,tet->space->nnodes,tet->space->coords,dof_idxlist,dof_vallist,ctx);CHKERRQ(ierr);

  /* insert user defined values/indices for dof_idx */
  for (i=0; i<tet->space->nnodes; i++) {
    PetscInt loc;
    
    loc = list->blocksize * i + dof_idx;
    
    if (dof_idxlist[i] == BCList_DIRICHLET) {
      idx[loc]  = BCList_DIRICHLET;
      vals[loc] = dof_vallist[i];
    }
  }

  ierr = BCListLocalToGlobal(list);CHKERRQ(ierr);
  
  ierr = PetscFree(dof_idxlist);CHKERRQ(ierr);
  ierr = PetscFree(dof_vallist);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBCListTraverseByRegionId"
PetscErrorCode DMTetBCListTraverseByRegionId(BCList list,PetscInt regionid,PetscInt dof_idx,PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),void *ctx)
{
  DM dm;
  PetscInt e,k;
  PetscBool istet = PETSC_FALSE;
  PetscInt L,*idx;
  PetscScalar *vals;
  PetscInt nen,*regionlist,npe,*element;
  PetscReal *coords;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(list->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = list->dm;
  if (!dm) SETERRQ(list->comm,PETSC_ERR_USER,"No DM has been set");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(list->comm,PETSC_ERR_SUP,"Only valid for DMTET");
  
  if (dof_idx > list->blocksize) SETERRQ1(list->comm,PETSC_ERR_USER,"DOF index must be less than %D",list->blocksize);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  ierr = BCListGetLocalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetLocalValues(list,&L,&vals);CHKERRQ(ierr);

  ierr = DMTetSpaceElement(dm,NULL,NULL,&element,&npe,NULL,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dm,&nen,&regionlist,NULL);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *eidx;
    
    if (regionlist[e] != regionid) continue;
    eidx = &element[e*npe];
    for (k=0; k<npe; k++) {
      PetscInt loc;
      PetscScalar coor_k[3],value;
      PetscBool constrain = PETSC_FALSE;
      
      loc = list->blocksize * eidx[k] + dof_idx;
      
      coor_k[0] = coords[2*eidx[k]+0];
      coor_k[1] = coords[2*eidx[k]+1];
      coor_k[2] = 0.0;
      ierr = eval(coor_k,dof_idx,&value,&constrain,ctx);CHKERRQ(ierr);
      if (constrain) {
        idx[loc] = BCList_DIRICHLET;
        vals[loc]   = value;
      }
      
    }
  }
  
  ierr = BCListLocalToGlobal(list);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 Function pointer has the following API
 
 PetscErrorCode eval(PetscScalar coor[],PetscScalar normal[],PetscScalar tangent[],
                     PetscInt dofidx,PetscScalar *val,PetscBool *is_constrainted,void *ctx)
 
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetFacetBCListTraverse"
PetscErrorCode DMTetFacetBCListTraverse(BCList list,PetscInt dof_idx,
                          PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),
                          void *ctx)
{
  DM dm;
  BFacetList bfacet;
  PetscInt f,fi;
  PetscBool istet = PETSC_FALSE;
  PetscInt L,*idx;
  PetscScalar *vals;
  PetscScalar *normal,*tangent;
  PetscInt nbasis,nfacets,nfacet_nodes,*facet_id,*facet_to_element;
  PetscInt *element;
  PetscReal *coords;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(list->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = list->dm;
  if (!dm) SETERRQ(list->comm,PETSC_ERR_USER,"No DM has been set");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(list->comm,PETSC_ERR_SUP,"Only valid for DMTET");
  
  if (dof_idx > list->blocksize) SETERRQ1(list->comm,PETSC_ERR_USER,"DOF index must be less than %D",list->blocksize);

  ierr = DMTetGetSpaceBFacetList(dm,&bfacet);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(bfacet,&nfacets,&nfacet_nodes);CHKERRQ(ierr);
  if (nfacets == 0) SETERRQ(list->comm,PETSC_ERR_SUP,"Only valid for a DMTET with an initialized BFacetList");
  
  ierr = BFacetListGetOrientations(bfacet,&normal,&tangent);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(bfacet,&facet_id);CHKERRQ(ierr);
  ierr = BFacetListGetFacetElementMap(bfacet,&facet_to_element);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,NULL,&nbasis,&element,NULL,NULL,&coords);CHKERRQ(ierr);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  ierr = BCListGetLocalIndices(list,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetLocalValues(list,&L,&vals);CHKERRQ(ierr);
  
  /*
   - loop over facets
     - get face_id, normal vector, tangent vector, element_face_idx
     - loop over face DOFs
       - call user evaluator
  */
  for (f=0; f<nfacets; f++) {
    PetscScalar *normal_f,*tangent_f;
    PetscInt fid,*cell_face_nid,eid;
    PetscInt *elbmap;
    
    eid = facet_to_element[f];
    elbmap = &element[nbasis*eid];
    
    fid = facet_id[f];
    ierr = BFacetListGetCellFaceBasisId(bfacet,fid,&cell_face_nid);CHKERRQ(ierr);
    normal_f = &normal[2*f];
    tangent_f = &tangent[2*f];
    
    for (fi=0; fi<nfacet_nodes; fi++) {
      PetscInt loc;
      PetscScalar coor_i[3],value;
      PetscBool constrain = PETSC_FALSE;
      PetscInt basis_idx;
      
      basis_idx = elbmap[ cell_face_nid[fi] ];
      
      loc = list->blocksize * basis_idx + dof_idx;
      
      coor_i[0] = coords[2*basis_idx+0];
      coor_i[1] = coords[2*basis_idx+1];
      coor_i[2] = 0.0;
      ierr = eval(coor_i,normal_f,tangent_f,dof_idx,&value,&constrain,ctx);CHKERRQ(ierr);
      if (constrain) {
        idx[loc] = BCList_DIRICHLET;
        vals[loc]   = value;
      }
    }
    
  }
  ierr = BCListLocalToGlobal(list);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBCListTraverseDMTetGList"
PetscErrorCode DMTetBCListTraverseDMTetGList(BCList bclist,PetscInt dof_idx,
                                             const char labelName[],
                                             PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,PetscBool*,void*),
                                             void *ctx)
{
  DM_TET         *tet;
  DM             dm;
  PetscBool      istet = PETSC_FALSE,issetup;
  PetscInt       i,k,L,*idx,list_n;
  const PetscInt *list_indices;
  PetscScalar    *vals;
  DMTetPrimitiveType primitive;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(bclist->comm,PETSC_ERR_USER,"A valid evaluation function must be provided");
  dm = bclist->dm;
  if (!dm) SETERRQ(bclist->comm,PETSC_ERR_USER,"No DM has been set");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(bclist->comm,PETSC_ERR_SUP,"Only valid for DMTET");
  tet = (DM_TET*)dm->data;
  
  if (dof_idx > bclist->blocksize) SETERRQ1(bclist->comm,PETSC_ERR_USER,"DOF index must be less than %D",bclist->blocksize);
  
  ierr = DMGListGetIsSetUp(dm,labelName,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(bclist->comm,PETSC_ERR_USER,"DMTetGlist is not yet setup");
  ierr = DMGListGetPrimitiveType(dm,labelName,&primitive);CHKERRQ(ierr);
  if (primitive != DMTET_BASIS) SETERRQ(bclist->comm,PETSC_ERR_USER,"DMTetGlist must refer to primitives of type DMTET_BASIS");
  
  ierr = DMGListGetIndices(dm,labelName,&list_n,&list_indices);CHKERRQ(ierr);
  
  /* traverse full list, apply bool eval, check flag, if Dirichlet apply eval function */
  ierr = BCListGetLocalIndices(bclist,&L,&idx);CHKERRQ(ierr);
  ierr = BCListGetLocalValues(bclist,&L,&vals);CHKERRQ(ierr);
  
  for (k=0; k<list_n; k++) {
    PetscInt loc;
    PetscScalar coor[3],value;
    PetscBool constrain = PETSC_FALSE;
    
    i = list_indices[k];
    loc = bclist->blocksize * i + dof_idx;
    
    coor[0] = tet->space->coords[2*i+0];
    coor[1] = tet->space->coords[2*i+1];
    coor[2] = 0.0;
    ierr = eval(coor,dof_idx,&value,&constrain,ctx);CHKERRQ(ierr);
    if (constrain) {
      idx[loc]  = BCList_DIRICHLET;
      vals[loc] = value;
    }
  }
  ierr = BCListLocalToGlobal(bclist);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
