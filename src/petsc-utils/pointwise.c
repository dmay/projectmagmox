
#include <petsc.h>
#include <petscvec.h>

#undef __FUNCT__
#define __FUNCT__ "VecPointwiseMultBS"
/*
 Compute x = y * z
 x, y and z can be the same vector
 
 In the case where x,y,z have the same block size, this function behaves exactly as VecPointwiseMult().
 One of either y or z can have a block size of 1, in this case we compute (assuming z has bs = 1)
 
 x[i][0] = y[i][0] * z[i][0]
 x[i][1] = y[i][1] * z[i][0]
 x[i][2] = y[i][2] * z[i][0]
 
 The only rule to enforce is that x does not have block size 1
*/
PetscErrorCode VecPointwiseMultBS(Vec x,Vec y,Vec z)
{
  PetscErrorCode ierr;
  PetscInt bs_x,bs_y,bs_z;
  PetscBool bs_same = PETSC_TRUE;
  
  ierr = VecGetBlockSize(x,&bs_x);CHKERRQ(ierr);
  ierr = VecGetBlockSize(y,&bs_y);CHKERRQ(ierr);
  ierr = VecGetBlockSize(z,&bs_z);CHKERRQ(ierr);
  
  if (bs_x != bs_y) bs_same = PETSC_FALSE;
  if (bs_x != bs_z) bs_same = PETSC_FALSE;
  if (bs_y != bs_z) bs_same = PETSC_FALSE;
  
  if (!bs_same) {
    /* check at least one of y or z has bs = 1 */
    if ((bs_y != 1) && (bs_z != 1)) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"One of y or z must have block size of 1");
    }
    /* check x has bs != 1 */
    if (bs_x == 1) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"x cannot have block size of 1");
    }
  }
  
  if (bs_same) {
    ierr = VecPointwiseMult(x,y,z);CHKERRQ(ierr);
  } else {
    PetscScalar *LA_x;
    const PetscScalar *LA_y;
    const PetscScalar *LA_z;
    PetscInt i,b,N;
    
    ierr = VecGetLocalSize(x,&N);CHKERRQ(ierr);
    N = N / bs_x;

    ierr = VecGetArrayRead(z,&LA_z);CHKERRQ(ierr);
    ierr = VecGetArrayRead(y,&LA_y);CHKERRQ(ierr);
    ierr = VecGetArray(x,&LA_x);CHKERRQ(ierr);

    if (bs_y == 1) {
      if (bs_x != bs_z) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"x and z must have the same block size");
      for (i=0; i<N; i++) {
        for (b=0; b<bs_x; b++) {
          LA_x[bs_x*i+b] = LA_y[i] * LA_z[bs_x*i+b];
        }
      }
    }
    
    if (bs_z == 1) {
      if (bs_x != bs_y) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"x and y must have the same block size");
      for (i=0; i<N; i++) {
        for (b=0; b<bs_x; b++) {
          LA_x[bs_x*i+b] = LA_y[bs_x*i+b] * LA_z[i];
        }
      }
    }

    ierr = VecRestoreArray(x,&LA_x);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(y,&LA_y);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(z,&LA_z);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}
