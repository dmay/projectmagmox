
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petsc/private/matimpl.h>

#undef __FUNCT__
#define __FUNCT__ "MatMult_Scatter_INSERT"
PetscErrorCode MatMult_Scatter_INSERT(Mat A,Vec x,Vec y)
{
  VecScatter     scatter = NULL;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatScatterGetVecScatter(A,&scatter);CHKERRQ(ierr);
  if (!scatter) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_ARG_WRONGSTATE,"Need to first call MatScatterSetScatter()");
  ierr = VecZeroEntries(y);CHKERRQ(ierr);
  ierr = VecScatterBegin(scatter,x,y,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter,x,y,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatMultAdd_Scatter_INSERT"
static PetscErrorCode MatMultAdd_Scatter_INSERT(Mat A,Vec x,Vec y,Vec z)
{
  VecScatter     scatter = NULL;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatScatterGetVecScatter(A,&scatter);CHKERRQ(ierr);
  if (!scatter) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_ARG_WRONGSTATE,"Need to first call MatScatterSetScatter()");
  ierr = VecScatterBegin(scatter,x,z,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter,x,z,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecAXPY(z,1.0,y);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatMultTranspose_Scatter_INSERT"
static PetscErrorCode MatMultTranspose_Scatter_INSERT(Mat A,Vec x,Vec y)
{
  VecScatter     scatter = NULL;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = MatScatterGetVecScatter(A,&scatter);CHKERRQ(ierr);
  if (!scatter) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_ARG_WRONGSTATE,"Need to first call MatScatterSetScatter()");
  ierr = VecZeroEntries(y);CHKERRQ(ierr);
  ierr = VecScatterBegin(scatter,x,y,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter,x,y,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatScatterSetUseInsertValues"
PetscErrorCode MatScatterSetUseInsertValues(Mat A,PetscBool value)
{
  PetscFunctionBegin;
  if (value) {
    A->ops->mult             = MatMult_Scatter_INSERT;
    A->ops->multadd          = MatMultAdd_Scatter_INSERT;
    A->ops->multtranspose    = MatMultTranspose_Scatter_INSERT;
    A->ops->multtransposeadd = NULL;
  }
  PetscFunctionReturn(0);
}
