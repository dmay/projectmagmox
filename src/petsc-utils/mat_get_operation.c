

#include <petscmat.h>
#include <petsc/private/matimpl.h>

/* Function exists in PETSc v3.9 */
/*@C
 MatGetOperation - Gets a matrix operation for any matrix type.
 
 Not Collective
 
 Input Parameters:
 +   mat - the matrix
 -   op - the name of the operation
 
 Output Parameter:
 .   f - the function that provides the operation
 
 Level: developer
 
 Usage:
 $      PetscErrorCode (*usermult)(Mat,Vec,Vec);
 $      ierr = MatGetOperation(A,MATOP_MULT,(void(**)(void))&usermult);
 
 Notes:
 See the file include/petscmat.h for a complete list of matrix
 operations, which all have the form MATOP_<OPERATION>, where
 <OPERATION> is the name (in all capital letters) of the
 user interface routine (e.g., MatMult() -> MATOP_MULT).
 
 This routine is distinct from MatShellGetOperation() in that it can be called on any matrix type.
 
 .keywords: matrix, get, operation
 
 .seealso: MatSetOperation(), MatCreateShell(), MatShellGetContext(), MatShellGetOperation()
@*/
#undef __FUNCT__
#define __FUNCT__ "MatGetOperation"
PetscErrorCode MatGetOperation(Mat mat,MatOperation op,void(**f)(void))
{
  *f = (((void (**)(void))mat->ops)[op]);
  return(0);
}
