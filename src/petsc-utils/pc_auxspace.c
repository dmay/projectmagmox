
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscpc.h>
#include <petsc/private/pcimpl.h>   /*I "petscpc.h" I*/

#include <petscutils.h>

/*
 xs_fine = xs_fine + pre_smooth_factor * inv(diag_A_fine)( x_fine - A_fine xs_fine ) // pre_smooth times
 r_fine = x_fine - A_fine xs_fine
 r_aux = P^T r_fine // project r_fine into coarse space
 Solve A_aux e_aux = r_aux
 e_fine = P e_aux // project e_aux into fine space
 xs_fine = xs_fine + post_smooth_factor * e_fine // add fine correction (no post smooth)
*/

typedef struct {
  Mat       P;
  PetscInt  pre_smooth_its;
  PetscReal pre_smooth_factor,post_smooth_factor;
  KSP       ksp_aux; /* xxx_sub_ */
  //Vec       r_fine,xs_fine,diag_B_fine;
  Vec       e_fine;
  Mat       A_aux;
  Vec       r_aux,e_aux;
  DM        dm_fine,dm_aux;
  PetscBool issetup,vecs_allocated;
} PCAuxSpace;


#undef __FUNCT__
#define __FUNCT__ "PCApply_AuxSpace"
static PetscErrorCode PCApply_AuxSpace(PC pc,Vec b,Vec x)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  Vec            r_fine,diag_B_fine,xs_fine;
  Mat            Amat,Bmat;
  PetscInt       k,max_it = 5;
  

  PetscFunctionBegin;
  ierr = PCGetOperators(pc,&Amat,&Bmat);CHKERRQ(ierr);
  
  ierr = VecDuplicate(b,&r_fine);CHKERRQ(ierr);
  ierr = VecDuplicate(b,&xs_fine);CHKERRQ(ierr);
  ierr = VecDuplicate(b,&diag_B_fine);CHKERRQ(ierr);
  
  /* pre-smooth */
  {
    PetscErrorCode (*matop_get_diagonal)(Mat,Vec) = NULL;
    
    ierr = MatGetOperation(Bmat,MATOP_GET_DIAGONAL,(void(**)(void))&matop_get_diagonal);CHKERRQ(ierr);
    if (matop_get_diagonal) {
      ierr = MatGetDiagonal(Bmat,diag_B_fine);CHKERRQ(ierr);
    } else {
      PetscPrintf(PETSC_COMM_WORLD,"PCApply_AuxSpace[Warning]: Bmat does not support MatGetDiagonal, using diag_ii = 1.0\n");
      ierr = VecSet(diag_B_fine,1.0);CHKERRQ(ierr);
    }
  }
  
  // r = b - Ax
  ierr = MatMult(Amat,x,r_fine);CHKERRQ(ierr);
  ierr = VecAYPX(r_fine,-1.0,b);CHKERRQ(ierr); //r = -r + b
  
  ierr = VecCopy(x,xs_fine);CHKERRQ(ierr);
  
  max_it = ctx->pre_smooth_its;
  for (k=0; k<max_it; k++) {
    ierr = VecPointwiseDivide(r_fine,r_fine,diag_B_fine);CHKERRQ(ierr);
    ierr = VecAXPY(xs_fine,ctx->pre_smooth_factor,r_fine);CHKERRQ(ierr);
    
    // r = b - Ax
    ierr = MatMult(Amat,xs_fine,r_fine);CHKERRQ(ierr);
    ierr = VecAYPX(r_fine,-1.0,b);CHKERRQ(ierr); //r = -r + b
  }
  
  ierr = MatMultTranspose(ctx->P,r_fine,ctx->r_aux);CHKERRQ(ierr);
  
  ierr = KSPSolve(ctx->ksp_aux,ctx->r_aux,ctx->e_aux);CHKERRQ(ierr);
  
  ierr = MatMult(ctx->P,ctx->e_aux,ctx->e_fine);CHKERRQ(ierr);
  
  ierr = VecAXPY(xs_fine,ctx->post_smooth_factor,ctx->e_fine);CHKERRQ(ierr);
  
  ierr = VecCopy(xs_fine,x);CHKERRQ(ierr);
  
  ierr = VecDestroy(&r_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&xs_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&diag_B_fine);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCView_AuxSpace"
static PetscErrorCode PCView_AuxSpace(PC pc,PetscViewer viewer)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;

  
  PetscFunctionBegin;
  PetscViewerASCIIPushTab(viewer);
  PetscViewerASCIIPrintf(viewer,"AuxSpace: pre-smooth iterations = %D\n",ctx->pre_smooth_its);
  PetscViewerASCIIPrintf(viewer,"AuxSpace: pre-smooth factor = %1.4e\n",ctx->pre_smooth_factor);
  PetscViewerASCIIPrintf(viewer,"AuxSpace: post-smooth factor = %1.4e\n",ctx->post_smooth_factor);
  PetscViewerASCIIPopTab(viewer);
  
  PetscViewerASCIIPrintf(viewer,"Natural space:\n");
  PetscViewerASCIIPushTab(viewer);
  if (ctx->dm_fine) {
    ierr = DMView(ctx->dm_fine,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  } else {
    PetscViewerASCIIPushTab(viewer);
    PetscViewerASCIIPrintf(viewer,"No DM provided\n");
    PetscViewerASCIIPopTab(viewer);
  }
  PetscViewerASCIIPopTab(viewer);
  
  PetscViewerASCIIPrintf(viewer,"Auxiliary space:\n");
  PetscViewerASCIIPushTab(viewer);
  if (ctx->dm_aux) {
    ierr = DMView(ctx->dm_aux,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  } else {
    PetscViewerASCIIPushTab(viewer);
    PetscViewerASCIIPrintf(viewer,"No DM provided\n");
    PetscViewerASCIIPopTab(viewer);
  }
  
  PetscViewerASCIIPopTab(viewer);
  
  ierr = KSPView(ctx->ksp_aux,viewer);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCSetup_AuxSpace"
static PetscErrorCode PCSetup_AuxSpace(PC pc)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  const char     *prefix;
  Mat            Amat,Bmat;
  PetscViewer    viewer = PETSC_VIEWER_STDOUT_WORLD;
  

  PetscFunctionBegin;
  if (!ctx->ksp_aux) {
    ierr = KSPCreate(PetscObjectComm((PetscObject)pc),&ctx->ksp_aux);CHKERRQ(ierr);
    ierr = PetscObjectIncrementTabLevel((PetscObject)ctx->ksp_aux,(PetscObject)pc,1);CHKERRQ(ierr);
    ierr = PCGetOptionsPrefix(pc,&prefix);CHKERRQ(ierr);
    ierr = KSPSetOptionsPrefix(ctx->ksp_aux,prefix);CHKERRQ(ierr);
    ierr = KSPAppendOptionsPrefix(ctx->ksp_aux,"sub_");CHKERRQ(ierr);
    ierr = KSPSetType(ctx->ksp_aux,KSPPREONLY);CHKERRQ(ierr);
  }
  ierr = KSPSetFromOptions(ctx->ksp_aux);CHKERRQ(ierr);
  
  ierr = PCGetOperators(pc,&Amat,&Bmat);CHKERRQ(ierr);
  
  if (!ctx->P) {
    Mat c_P = NULL;
    
    ierr = PetscObjectQuery((PetscObject)Bmat,"PCAuxSpace_P",(PetscObject*)&c_P);CHKERRQ(ierr);
    
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_INFO);
    PetscViewerASCIIPrintf(viewer,"PCSetup_AuxSpace: Querying operator Bmat for composed object name \"PCAuxSpace_P\"\n");
    PetscViewerASCIIPushTab(viewer);
    MatView(Bmat,viewer);
    PetscViewerASCIIPopTab(viewer);
    PetscViewerPopFormat(viewer);
    
    if (c_P) {
      ierr = PCAuxSpaceSetProjection(pc,c_P);CHKERRQ(ierr);
    } else {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Projection (P) not provided");
    }
  }
  
  if (!ctx->A_aux) {
    Mat c_A = NULL;
    
    ierr = PetscObjectQuery((PetscObject)Bmat,"PCAuxSpace_PtAP",(PetscObject*)&c_A);CHKERRQ(ierr);
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_INFO);
    PetscViewerASCIIPrintf(viewer,"PCSetup_AuxSpace: Querying operator Bmat for composed object name \"PCAuxSpace_PtAP\"\n");
    PetscViewerASCIIPushTab(viewer);
    MatView(Bmat,viewer);
    PetscViewerASCIIPopTab(viewer);
    PetscViewerPopFormat(viewer);
    
    if (c_A) {
      ierr = PCAuxSpaceSetOperators(pc,c_A,c_A);CHKERRQ(ierr);
    } else {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Auxiliary projected operator (Pt.A.P) not provided");
    }
  }
  
  ierr = KSPSetOperators(ctx->ksp_aux,ctx->A_aux,ctx->A_aux);CHKERRQ(ierr);

  if (!ctx->dm_fine) {
    DM dmfine;
    
    ierr = PetscObjectQuery((PetscObject)Bmat,"PCAuxSpace_DMNatural",(PetscObject*)&dmfine);CHKERRQ(ierr);
    ierr = PCAuxSpaceSetDMs(pc,dmfine,NULL);CHKERRQ(ierr);
  }
  if (!ctx->dm_aux) {
    DM dmaux;
    
    ierr = PetscObjectQuery((PetscObject)Bmat,"PCAuxSpace_DMAux",(PetscObject*)&dmaux);CHKERRQ(ierr);
    ierr = PCAuxSpaceSetDMs(pc,NULL,dmaux);CHKERRQ(ierr);
  }
  
  if (!ctx->vecs_allocated) {
    
    ierr = MatCreateVecs(Amat,&ctx->e_fine,NULL);CHKERRQ(ierr);

    ierr = MatCreateVecs(ctx->A_aux,&ctx->r_aux,&ctx->e_aux);CHKERRQ(ierr);

    ctx->vecs_allocated = PETSC_TRUE;
  }
  ctx->issetup = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCSetFromOptions_AuxSpace"
static PetscErrorCode PCSetFromOptions_AuxSpace(PetscOptionItems *PetscOptionsObject,PC pc)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  ierr = PetscOptionsHead(PetscOptionsObject,"AuxSpace options");CHKERRQ(ierr);

  ierr = PetscOptionsInt("-pc_auxspace_presmooth_it","Number of pre-smoothing iterations","PCJAuxSpaceSetPreSmoothIterations",ctx->pre_smooth_its,&ctx->pre_smooth_its,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-pc_auxspace_presmooth_fac","Factor applied during pre-smoothing iterations","PCJAuxSpaceSetPreSmoothFactor",ctx->pre_smooth_factor,&ctx->pre_smooth_factor,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-pc_auxspace_postsmooth_fac","Factor applied during single post-smoothing iteration","PCJAuxSpaceSetPostSmoothFactor",ctx->post_smooth_factor,&ctx->post_smooth_factor,NULL);CHKERRQ(ierr);
  
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCDestroy_AuxSpace"
static PetscErrorCode PCDestroy_AuxSpace(PC pc)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  ierr = MatDestroy(&ctx->A_aux);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->P);CHKERRQ(ierr);
  
  ierr = KSPDestroy(&ctx->ksp_aux);CHKERRQ(ierr);

  ierr = VecDestroy(&ctx->e_fine);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->r_aux);CHKERRQ(ierr);
  ierr = VecDestroy(&ctx->e_aux);CHKERRQ(ierr);

  if (ctx->dm_fine) { ierr = DMDestroy(&ctx->dm_fine);CHKERRQ(ierr); }
  if (ctx->dm_aux) { ierr = DMDestroy(&ctx->dm_aux);CHKERRQ(ierr); }
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_SetOperators"
static PetscErrorCode PCAuxSpace_SetOperators(PC pc,Mat A,Mat B)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  
  /* reference first in case the matrices are the same */
  if (B) { ierr = PetscObjectReference((PetscObject)B);CHKERRQ(ierr); }
  ierr = MatDestroy(&ctx->A_aux);CHKERRQ(ierr);
  ctx->A_aux  = B;
  //ctx->A_aux = B;
  //ierr = PetscObjectReference((PetscObject)B);CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceSetOperators"
PetscErrorCode PCAuxSpaceSetOperators(PC pc,Mat A,Mat B)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceSetOperators_C",(PC,Mat,Mat),(pc,A,B));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_GetOperators"
static PetscErrorCode PCAuxSpace_GetOperators(PC pc,Mat *A,Mat *B)
{
  PCAuxSpace *ctx = (PCAuxSpace*)pc->data;
  if (A) { *A = NULL; }
  if (B) { *B = ctx->A_aux; }
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceGetOperators"
PetscErrorCode PCAuxSpaceGetOperators(PC pc,Mat *A,Mat *B)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceGetOperators_C",(PC,Mat*,Mat*),(pc,A,B));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_SetProjection"
static PetscErrorCode PCAuxSpace_SetProjection(PC pc,Mat P)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  
  /* reference first in case the matrices are the same */
  if (P) { ierr = PetscObjectReference((PetscObject)P);CHKERRQ(ierr); }
  ierr = MatDestroy(&ctx->P);CHKERRQ(ierr);
  ctx->P  = P;
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceSetProjection"
PetscErrorCode PCAuxSpaceSetProjection(PC pc,Mat P)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceSetProjection_C",(PC,Mat),(pc,P));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_GetProjection"
static PetscErrorCode PCAuxSpace_GetProjection(PC pc,Mat *P)
{
  PCAuxSpace *ctx = (PCAuxSpace*)pc->data;
  if (P) { *P = ctx->P; }
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceGetProjection"
PetscErrorCode PCAuxSpaceGetProjection(PC pc,Mat *P)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceGetProjection_C",(PC,Mat*),(pc,P));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpace_SetDMs"
static PetscErrorCode PCAuxSpace_SetDMs(PC pc,DM dm,DM dm_aux)
{
  PCAuxSpace     *ctx = (PCAuxSpace*)pc->data;
  PetscErrorCode ierr;
  
  if (dm) {
    ctx->dm_fine = dm;
    ierr = PetscObjectReference((PetscObject)dm);CHKERRQ(ierr);
  }
  if (dm_aux) {
    ctx->dm_aux = dm_aux;
    ierr = PetscObjectReference((PetscObject)dm_aux);CHKERRQ(ierr);
  }
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCAuxSpaceSetDMs"
PetscErrorCode PCAuxSpaceSetDMs(PC pc,DM dm1,DM dm2)
{
  PetscErrorCode ierr;
  ierr = PetscUseMethod(pc,"PCAuxSpaceSetDMs_C",(PC,DM,DM),(pc,dm1,dm2));CHKERRQ(ierr);
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "PCCreate_AuxSpace"
PETSC_EXTERN PetscErrorCode PCCreate_AuxSpace(PC pc)
{
  PCAuxSpace     *ctx;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  ierr     = PetscNewLog(pc,&ctx);CHKERRQ(ierr);
  pc->data = (void*)ctx;
  
  /*
   Initialize the pointers to vectors to ZERO; these will be used to store
   diagonal entries of the matrix for fast preconditioner application.
   */
  ctx->P      = NULL;
  ctx->ksp_aux  = NULL;
  ctx->e_fine = NULL;
  ctx->A_aux = NULL;
  ctx->r_aux    = NULL;
  ctx->e_aux    = NULL;
  ctx->dm_fine = NULL;
  ctx->dm_aux = NULL;
  ctx->pre_smooth_its = 5;
  ctx->pre_smooth_factor = 0.3;
  ctx->post_smooth_factor = 1.0;
  ctx->issetup = PETSC_FALSE;
  ctx->vecs_allocated = PETSC_FALSE;
  
  /*
   Set the pointers for the functions that are provided above.
   Now when the user-level routines (such as PCApply(), PCDestroy(), etc.)
   are called, they will automatically call these functions.  Note we
   choose not to provide a couple of these functions since they are
   not needed.
   */
  pc->ops->apply               = PCApply_AuxSpace;
  pc->ops->applytranspose      = NULL;
  pc->ops->setup               = PCSetup_AuxSpace;
  pc->ops->reset               = NULL;//PCReset_AuxSpace;
  pc->ops->destroy             = PCDestroy_AuxSpace;
  pc->ops->setfromoptions      = PCSetFromOptions_AuxSpace;
  pc->ops->view                = PCView_AuxSpace;
  pc->ops->applyrichardson     = NULL;
  pc->ops->applysymmetricleft  = NULL;
  pc->ops->applysymmetricright = NULL;
  
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetOperators_C", PCAuxSpace_SetOperators);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetProjection_C",PCAuxSpace_SetProjection);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceSetDMs_C",       PCAuxSpace_SetDMs);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceGetOperators_C", PCAuxSpace_GetOperators);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject)pc,"PCAuxSpaceGetProjection_C",PCAuxSpace_GetProjection);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
