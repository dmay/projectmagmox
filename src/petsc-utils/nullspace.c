
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmcomposite.h>

#undef __FUNCT__
#define __FUNCT__ "MatNullSpaceCreate_Constant"
PetscErrorCode MatNullSpaceCreate_Constant(Vec x,MatNullSpace *nullspace)
{
  MatNullSpace nsp;
  PetscInt N;
  PetscErrorCode ierr;
  
  ierr = VecGetSize(x,&N);CHKERRQ(ierr);
  ierr = VecSet(x,1.0/PetscSqrtReal((PetscScalar)(1.0*N)) );CHKERRQ(ierr);
  ierr = MatNullSpaceCreate(PETSC_COMM_WORLD,PETSC_FALSE,1,&x,&nsp);CHKERRQ(ierr);
  *nullspace = nsp;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NullSpaceConstant_SetVec"
PetscErrorCode NullSpaceConstant_SetVec(Vec x)
{
  PetscInt N;
  PetscErrorCode ierr;
  
  ierr = VecGetSize(x,&N);CHKERRQ(ierr);
  ierr = VecSet(x,1.0/PetscSqrtReal((PetscScalar)(1.0*N)) );CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMComposite_NullSpaceConstant_SetVec"
PetscErrorCode DMComposite_NullSpaceConstant_SetVec(DM dm,Vec constant,PetscInt nc,PetscInt const_fields[])
{
  PetscErrorCode ierr;
  PetscInt n,f;
  Vec *X;
  
  ierr = DMCompositeGetNumberDM(dm,&n);CHKERRQ(ierr);
  ierr = PetscMalloc1(n,&X);CHKERRQ(ierr);
  for (f=0; f<n; f++) { X[f] = NULL; }
  ierr = DMCompositeGetAccessArray(dm,constant,n,NULL,X);CHKERRQ(ierr);
  for (f=0; f<nc; f++) {
    PetscInt field_index = const_fields[f];
    if (field_index > n) SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_USER,"Requested field %D is larger than max DMComposite fields (%D)",field_index,n);
    ierr = NullSpaceConstant_SetVec(X[field_index]);CHKERRQ(ierr);
  }
  ierr = DMCompositeRestoreAccessArray(dm,constant,n,NULL,X);CHKERRQ(ierr);
  ierr = PetscFree(X);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SaddlePointSetConstantNullSpace_SNES"
PetscErrorCode SaddlePointSetConstantNullSpace_SNES(SNES snes)
{
  Vec constant;
  MatNullSpace nsp;
  Mat J;
  DM dm = NULL;
  PetscBool is_composite = PETSC_FALSE;
  PetscInt n,field_p[] = { 1 };
  PetscErrorCode ierr;
  
  ierr = SNESGetDM(snes,&dm);CHKERRQ(ierr);
  if (!dm) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"SNES does not have a DM");
  ierr = PetscObjectTypeCompare((PetscObject)dm,"composite",&is_composite);CHKERRQ(ierr);
  if (!is_composite) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DM is expected to be of type DMCOMPOSITE");
  ierr = DMCompositeGetNumberDM(dm,&n);CHKERRQ(ierr);
  if (n != 2) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DMCOMPOSITE is expected to have 2 DMs - found %D",n);
  
  ierr = DMCreateGlobalVector(dm,&constant);CHKERRQ(ierr);
  
  ierr = DMComposite_NullSpaceConstant_SetVec(dm,constant,1,field_p);CHKERRQ(ierr);
  
  ierr = MatNullSpaceCreate(PETSC_COMM_WORLD,PETSC_FALSE,1,&constant,&nsp);CHKERRQ(ierr);
  ierr = VecDestroy(&constant);CHKERRQ(ierr);
  
  ierr = SNESGetJacobian(snes,&J,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = MatSetNullSpace(J,nsp);CHKERRQ(ierr);
  ierr = MatNullSpaceDestroy(&nsp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
