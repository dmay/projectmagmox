
#include <petscmat.h>
#include <petscutils.h>

/*
 Duplicates sizes of mat but defines a non-zero structure consisting only of the diagonal.
 mat must be a square matrix
*/
#undef __FUNCT__
#define __FUNCT__ "MatCreateDiagonal"
PetscErrorCode MatCreateDiagonal(Mat mat,MatType mtype,Mat *D)
{
  PetscInt M,N,m,n,bs,i,start,end;
  
  MatGetSize(mat,&M,&N);
  if (M != N) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Only valid for square matrices");
  MatGetLocalSize(mat,&m,&n);
  MatGetBlockSize(mat,&bs);
  
  MatCreate(PetscObjectComm((PetscObject)mat),D);
  MatSetSizes(*D,m,m,M,M);
  MatSetBlockSize(*D,bs);
  MatSetType(*D,mtype);
  MatSetFromOptions(*D);
  
  MatSeqAIJSetPreallocation(*D,1,NULL);
  MatSeqBAIJSetPreallocation(*D,bs,1,NULL);
  
  MatMPIAIJSetPreallocation(*D,1,NULL,0,NULL);
  MatMPIBAIJSetPreallocation(*D,bs,1,NULL,0,NULL);
  
  MatSeqSBAIJSetPreallocation(*D,bs,1,NULL);
  MatMPISBAIJSetPreallocation(*D,bs,1,NULL,0,NULL);
  
  MatGetOwnershipRange(*D,&start,&end);
  for (i=start; i<end; i++) {
    MatSetValue(*D,i,i,0.0,INSERT_VALUES);
  }
  MatAssemblyBegin(*D,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(*D,MAT_FINAL_ASSEMBLY);
  
  PetscFunctionReturn(0);
}

/*
 Duplicates sizes of mat but defines a non-zero structure consisting only of the diagonal
 Valid values for op are
   MAT_DO_NOT_COPY_VALUES,
   MAT_COPY_VALUES
*/
#undef __FUNCT__
#define __FUNCT__ "MatDuplicateDiagonal"
PetscErrorCode MatDuplicateDiagonal(Mat mat,MatType mtype,MatDuplicateOption op,Mat *D)
{
  MatCreateDiagonal(mat,mtype,D);
  switch (op) {
    case MAT_DO_NOT_COPY_VALUES:
      break;
    case MAT_COPY_VALUES:
    {
      Vec diag;
      
      MatCreateVecs(mat,&diag,NULL);
      MatGetDiagonal(mat,diag);
      MatDiagonalSet(*D,diag,INSERT_VALUES);
      VecDestroy(&diag);
    }
      break;
    case MAT_SHARE_NONZERO_PATTERN:
      SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Only valid for op = {MAT_DO_NOT_COPY_VALUES, MAT_COPY_VALUES}");
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Unknown operation specified");
      break;
  }
  PetscFunctionReturn(0);
}

/*
 Creates a square matirx D with global/local sizes consistent with vec.
 Allocates non-zero structure consisting only of the diagonal.
 Valid values for op are
   MAT_DO_NOT_COPY_VALUES,
   MAT_COPY_VALUES
*/
#undef __FUNCT__
#define __FUNCT__ "MatCreateDiagonalFromVec"
PetscErrorCode MatCreateDiagonalFromVec(Vec vec,MatType mtype,MatDuplicateOption op,Mat *D)
{
  PetscInt M,m,bs,i,start,end;
  
  VecGetSize(vec,&M);
  VecGetLocalSize(vec,&m);
  VecGetBlockSize(vec,&bs);

  MatCreate(PetscObjectComm((PetscObject)vec),D);
  MatSetSizes(*D,m,m,M,M);
  MatSetBlockSize(*D,bs);
  MatSetType(*D,mtype);
  MatSetFromOptions(*D);
  
  MatSeqAIJSetPreallocation(*D,1,NULL);
  MatSeqBAIJSetPreallocation(*D,bs,1,NULL);
  
  MatMPIAIJSetPreallocation(*D,1,NULL,0,NULL);
  MatMPIBAIJSetPreallocation(*D,bs,1,NULL,0,NULL);
  
  MatSeqSBAIJSetPreallocation(*D,bs,1,NULL);
  MatMPISBAIJSetPreallocation(*D,bs,1,NULL,0,NULL);
  
  MatGetOwnershipRange(*D,&start,&end);
  for (i=start; i<end; i++) {
    MatSetValue(*D,i,i,0.0,INSERT_VALUES);
  }
  MatAssemblyBegin(*D,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(*D,MAT_FINAL_ASSEMBLY);

  switch (op) {
    case MAT_DO_NOT_COPY_VALUES:
      break;
    case MAT_COPY_VALUES:
      MatDiagonalSet(*D,vec,INSERT_VALUES);
      break;
    case MAT_SHARE_NONZERO_PATTERN:
      SETERRQ(PetscObjectComm((PetscObject)vec),PETSC_ERR_SUP,"Only valid for op = {MAT_DO_NOT_COPY_VALUES, MAT_COPY_VALUES}");
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)vec),PETSC_ERR_SUP,"Unknown operation specified");
      break;
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatDuplicateDiagonalReciprocal"
PetscErrorCode MatDuplicateDiagonalReciprocal(Mat mat,MatType mtype,Mat *D)
{
  Vec rvec;
  
  MatCreateVecs(mat,&rvec,NULL);
  MatGetDiagonal(mat,rvec);
  MatCreateDiagonalFromVecReciprocal(rvec,mtype,D);
  VecDestroy(&rvec);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatCreateDiagonalFromVecReciprocal"
PetscErrorCode MatCreateDiagonalFromVecReciprocal(Vec vec,MatType mtype,Mat *D)
{
  Vec rvec;
  
  VecDuplicate(vec,&rvec);
  VecCopy(vec,rvec);
  VecReciprocal(rvec);
  MatCreateDiagonalFromVec(rvec,mtype,MAT_COPY_VALUES,D);
  VecDestroy(&rvec);
  PetscFunctionReturn(0);
}
