
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petsc/private/matimpl.h>

//
// 01
// 02
// 03
// 012
// 013
// 12
// 13
// 123
// 23
//

#undef __FUNCT__
#define __FUNCT__ "FindISViaFusedComparison"
PetscErrorCode FindISViaFusedComparison(IS is,PetscInt nis,IS islist[],PetscInt *n,IS isset[],PetscBool *found)
{
  IS istmp[4],is_fused;
  PetscInt k,issize[4],target,sum;
  PetscBool same;
  MPI_Comm comm;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  
  ierr = PetscObjectGetComm((PetscObject)is,&comm);CHKERRQ(ierr);
  
  sum = 0;
  ierr = ISGetSize(is,&target);CHKERRQ(ierr);
  for (k=0; k<nis; k++) {
    ierr = ISGetSize(islist[k],&issize[k]);CHKERRQ(ierr);
    sum = sum + issize[k];
  }
  if (target == sum) { /* full space */
    *n = nis;
    for (k=0; k<nis; k++) {
      isset[k] = islist[k];
    }
    *found = PETSC_TRUE;
    PetscFunctionReturn(0);
  }
  
  *found = PETSC_FALSE;
  
  // try pointer matching cases first (cheap)
  for (k=0; k<nis; k++) {
    if (is == islist[k]) {
      *n = 1;
      isset[0] = islist[k];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // try non-fused cases first (cheap)
  for (k=0; k<nis; k++) {
    ierr = ISEqual(is,islist[k],&same);CHKERRQ(ierr);
    if (same) {
      *n = 1;
      isset[0] = islist[k];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // common cases
  // 01 (vs,ps)
  if (issize[0] + issize[1] == target) {
    istmp[0] = islist[0];
    istmp[1] = islist[1];
    ierr = ISConcatenate(comm, 2, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 2;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // 23  (vD,pf)
  if (issize[2] + issize[3] == target) {
    istmp[0] = islist[2];
    istmp[1] = islist[3];
    ierr = ISConcatenate(comm, 2, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 2;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  
  // 02   (vs,vD)
  if (issize[0] + issize[2] == target) {
    istmp[0] = islist[0];
    istmp[1] = islist[2];
    ierr = ISConcatenate(comm, 2, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 2;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // 13   (ps,pf)
  if (issize[1] + issize[3] == target) {
    istmp[0] = islist[1];
    istmp[1] = islist[3];
    ierr = ISConcatenate(comm, 2, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 2;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // 03   (vs,pf)
  if (issize[1] + issize[3] == target) {
    istmp[0] = islist[0];
    istmp[1] = islist[3];
    ierr = ISConcatenate(comm, 2, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 2;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // 12  (ps,vD)
  if (issize[1] + issize[2] == target) {
    istmp[0] = islist[1];
    istmp[1] = islist[2];
    ierr = ISConcatenate(comm, 2, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 2;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // 013   (vs,ps,pf)
  if (issize[0] + issize[1] + issize[3] == target) {
    istmp[0] = islist[0];
    istmp[1] = islist[1];
    istmp[2] = islist[3];
    ierr = ISConcatenate(comm, 3, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 3;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      isset[2] = istmp[2];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  
  // 012  (vs,ps,vD)
  if (issize[0] + issize[1] + issize[2] == target) {
    istmp[0] = islist[0];
    istmp[1] = islist[1];
    istmp[2] = islist[2];
    ierr = ISConcatenate(comm, 3, istmp, &is_fused);CHKERRQ(ierr);
    ierr = ISEqual(is,is_fused,&same);CHKERRQ(ierr);
    ierr = ISDestroy(&is_fused);CHKERRQ(ierr);
    if (same) {
      *n = 3;
      isset[0] = istmp[0];
      isset[1] = istmp[1];
      isset[2] = istmp[2];
      *found = PETSC_TRUE;
      PetscFunctionReturn(0);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatGetSubMatrix_NestFusedIS"
PetscErrorCode MatGetSubMatrix_NestFusedIS(Mat mat,IS isrow,IS iscol,MatReuse cll,Mat *newmat)
{
  PetscInt m,n,i,ii,jj,k;
  IS *irow,*icol;
  PetscBool found,submat_from_fused_is;
  IS isset_r[] = {NULL,NULL,NULL,NULL};
  IS isset_c[] = {NULL,NULL,NULL,NULL};
  PetscInt setlen_r,setlen_c;
  Mat bA[4*4];
  Mat **submats;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  
  if (cll == MAT_INPLACE_MATRIX) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"In place fetching is not supported");
  
  ierr = MatNestGetSize(mat,&m,&n);CHKERRQ(ierr);
  if (m > 4) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Only for max of 4 row blocks");
  if (n > 4) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Only for max of 4 column blocks");
  
  ierr = PetscMalloc1(m,&irow);CHKERRQ(ierr);
  ierr = PetscMalloc1(n,&icol);CHKERRQ(ierr);
  ierr = MatNestGetISs(mat,irow,icol);CHKERRQ(ierr);
  for (k=0; k<m; k++) {
    PetscInfo2(mat,"Row IS %D: %p \n",k,irow[k]);
  }
  for (k=0; k<n; k++) {
    PetscInfo2(mat,"Col IS %D: %p \n",k,icol[k]);
  }
  
  ierr = FindISViaFusedComparison(isrow,m,irow,&setlen_r,isset_r,&found);CHKERRQ(ierr);
  if (!found) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Row concate search failed");
  if (setlen_r == 1) PetscInfo1(mat,"Identified single row block [%p]\n",isset_r[0]);
  if (setlen_r == 2) PetscInfo2(mat,"Identified row block [%p %p]\n",isset_r[0],isset_r[1]);
  if (setlen_r == 3) PetscInfo3(mat,"Identified row block [%p %p %p]\n",isset_r[0],isset_r[1],isset_r[2]);
  
  //ierr = ISEqual(isrow,iscol,&rowcolsame);CHKERRQ(ierr);
  if (iscol == isrow) {
    PetscInfo(mat,"Identified col IS as matching row IS (same pointer)\n");
    setlen_c = setlen_r;
    for (k=0; k<setlen_r; k++) {
      isset_c[k] = isset_r[k];
    }
  } else {
    ierr = FindISViaFusedComparison(iscol,n,icol,&setlen_c,isset_c,&found);CHKERRQ(ierr);
    if (!found) ISView(iscol,PETSC_VIEWER_STDOUT_(PetscObjectComm((PetscObject)iscol)));
    if (!found) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_SUP,"Column concate search failed");
    if (setlen_c == 1) PetscInfo1(mat,"Identified single col block [%p]\n",isset_c[0]);
    if (setlen_c == 2) PetscInfo2(mat,"Identified col block [%p %p]\n",isset_c[0],isset_c[1]);
    if (setlen_c == 3) PetscInfo3(mat,"Identified col block [%p %p %p]\n",isset_c[0],isset_c[1],isset_c[2]);
  }
  
  ierr = MatNestGetSubMats(mat,NULL,NULL,&submats);CHKERRQ(ierr);
  ii = jj = 0;
  for (ii=0; ii<setlen_r; ii++) {
    PetscInt i2grab = -1;
    
    for (k=0; k<m; k++) {
      if (isset_r[ii] == irow[k]) {
        i2grab = k;
        break;
      }
    }
    
    for (jj=0; jj<setlen_c; jj++) {
      PetscInt j2grab = -1;
      
      for (k=0; k<n; k++) {
        if (isset_c[jj] == icol[k]) {
          j2grab = k;
          break;
        }
      }
      
      bA[ setlen_c * ii +jj ] = submats[i2grab][j2grab];
      PetscInfo5(mat,"%D,%D <- %D %D : %p \n",ii,jj,i2grab,j2grab,bA[setlen_c * ii + jj]);
    }
  }
  
  submat_from_fused_is = PETSC_TRUE;
  if ( (setlen_r == 1) && (setlen_c == 1) ) {
    submat_from_fused_is = PETSC_FALSE;
  }
  
  if (!submat_from_fused_is) {
    
    if (cll == MAT_INITIAL_MATRIX) {
      *newmat = bA[0];
      ierr = PetscObjectReference((PetscObject)bA[0]);CHKERRQ(ierr);
    } else if (cll == MAT_REUSE_MATRIX) {
      if (bA[0] != *newmat) SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_ARG_WRONGSTATE,"Submatrix was not used before in this call");
    }
  }
  
  if (submat_from_fused_is && (cll == MAT_INITIAL_MATRIX) ) {
    IS *iirow,*iicol;
    PetscInt *idx,len;
    const PetscInt *cidx;
    PetscInt min,shift;
    
    ierr = PetscMalloc1(setlen_r,&iirow);CHKERRQ(ierr);
    ierr = PetscMalloc1(setlen_c,&iicol);CHKERRQ(ierr);
    
    shift = 0;
    for (i=0; i<setlen_r; i++) {
      ierr = ISGetMinMax(isset_r[i],&min,NULL);CHKERRQ(ierr);
      ierr = ISGetSize(isset_r[i],&len);CHKERRQ(ierr);
      ierr = ISGetIndices(isset_r[i],&cidx);CHKERRQ(ierr);
      ierr = PetscMalloc1(len,&idx);CHKERRQ(ierr);
      for (k=0; k<len; k++) {
        idx[k] = cidx[k] - min + shift;
      }
      ierr = ISRestoreIndices(isset_r[i],&cidx);CHKERRQ(ierr);
      ierr = ISCreateGeneral(PetscObjectComm((PetscObject)isrow),len,idx,PETSC_COPY_VALUES,&iirow[i]);CHKERRQ(ierr);
      ierr = PetscFree(idx);CHKERRQ(ierr);
      
      shift = shift + len;
    }
    
    shift = 0;
    for (i=0; i<setlen_c; i++) {
      
      ierr = ISGetMinMax(isset_c[i],&min,NULL);CHKERRQ(ierr);
      ierr = ISGetSize(isset_c[i],&len);CHKERRQ(ierr);
      ierr = ISGetIndices(isset_c[i],&cidx);CHKERRQ(ierr);
      ierr = PetscMalloc1(len,&idx);CHKERRQ(ierr);
      for (k=0; k<len; k++) {
        idx[k] = cidx[k] - min + shift;
      }
      ierr = ISRestoreIndices(isset_c[i],&cidx);CHKERRQ(ierr);
      ierr = ISCreateGeneral(PetscObjectComm((PetscObject)iscol),len,idx,PETSC_COPY_VALUES,&iicol[i]);CHKERRQ(ierr);
      ierr = PetscFree(idx);CHKERRQ(ierr);
      
      shift = shift + len;
    }

    // This referencing isn't need - MatCreateNest() will do it for us
    /*
    for (i=0; i<setlen_r*setlen_c; i++) {
      ierr = PetscObjectReference((PetscObject)bA[i]);CHKERRQ(ierr);
    }
    */
    //ierr = MatCreateNest(PetscObjectComm((PetscObject)mat),setlen_r,isset_r,setlen_c,isset_c,bA,newmat);CHKERRQ(ierr);
    ierr = MatCreateNest(PetscObjectComm((PetscObject)mat),setlen_r,iirow,setlen_c,iicol,bA,newmat);CHKERRQ(ierr);
    
    for (i=0; i<setlen_r; i++) { ierr = ISDestroy(&iirow[i]);CHKERRQ(ierr); }
    for (i=0; i<setlen_c; i++) { ierr = ISDestroy(&iicol[i]);CHKERRQ(ierr); }
    ierr = PetscFree(iirow);CHKERRQ(ierr);
    ierr = PetscFree(iicol);CHKERRQ(ierr);
  } else if (submat_from_fused_is && (cll == MAT_REUSE_MATRIX) ) {
    /* open the contents of reused matrix and do a pointer comparison - error if one of them doesn't match */
    Mat **bAreuse;
    PetscInt mreuse,nreuse;
 
    ierr = MatNestGetSubMats(*newmat,&mreuse,&nreuse,&bAreuse);CHKERRQ(ierr);
    /* check sizes match */
    if (setlen_r != mreuse) SETERRQ2(PetscObjectComm((PetscObject)mat),PETSC_ERR_ARG_WRONGSTATE,"Existing matrix row size is inconsistent. Expected %D found %D",setlen_r,mreuse);
    if (setlen_c != nreuse) SETERRQ2(PetscObjectComm((PetscObject)mat),PETSC_ERR_ARG_WRONGSTATE,"Existing matrix column size is inconsistent. Expected %D found %D",setlen_c,nreuse);
    
    for (ii=0; ii<setlen_r; ii++) {
      for (jj=0; jj<setlen_c; jj++) {
        
        if (bAreuse[ii][jj] != bA[ setlen_c * ii +jj ]) {
          SETERRQ(PetscObjectComm((PetscObject)mat),PETSC_ERR_ARG_WRONGSTATE,"Existing matrix contains stale sub-matrix pointers");
        }
      
      }
    }
    
  }
  
  ierr = MatAssemblyBegin(*newmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*newmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = PetscFree(irow);CHKERRQ(ierr);
  ierr = PetscFree(icol);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatNestConfigureFusedISGetSubMatrix"
PetscErrorCode MatNestConfigureFusedISGetSubMatrix(Mat B)
{
  PetscErrorCode ierr;
  PetscBool same;
  PetscInt m,n;
  
  /* check its a mat-nest */
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST,&same);CHKERRQ(ierr);
  if (!same) SETERRQ(PetscObjectComm((PetscObject)B),PETSC_ERR_SUP,"Only valid for MatNest");
  
  /* check its a 4x4 nest */
  ierr = MatNestGetSize(B,&m,&n);CHKERRQ(ierr);
  if ((m > 4) || (n > 4)) SETERRQ2(PetscObjectComm((PetscObject)B),PETSC_ERR_SUP,"Only block systems of 4x4 or less. Provided system was %D x %D",m,n);
  
  B->ops->getsubmatrix = MatGetSubMatrix_NestFusedIS;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_MatNestAssembly"
PetscErrorCode _MatNestAssembly(Mat B)
{
  PetscBool same;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST,&same);CHKERRQ(ierr);
  if (same) {
    PetscInt i,j,m,n;
    Mat **bA;
    
    ierr = MatNestGetSubMats(B,&m,&n,&bA);CHKERRQ(ierr);
    for (i=0; i<m; i++) {
      for (j=0; j<n; j++) {
        ierr = _MatNestAssembly(bA[i][j]);CHKERRQ(ierr);
        ierr = MatAssemblyBegin(bA[i][j],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
        ierr = MatAssemblyEnd(bA[i][j],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
      }
    }
  } else {
    ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatNestAssembly"
PetscErrorCode MatNestAssembly(Mat B)
{
  PetscBool same;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST,&same);CHKERRQ(ierr);
  if (!same) SETERRQ(PetscObjectComm((PetscObject)B),PETSC_ERR_SUP,"Only valid for MatNest");
  
  ierr = _MatNestAssembly(B);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCompositeFillMatNest"
PetscErrorCode DMCompositeFillMatNest(Mat B)
{
  DM dm,*dmlist;
  PetscBool same;
  PetscInt i,j,m,n;
  Mat **bA;
  Vec X,*Xlist;
  PetscInt ndms;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)B,MATNEST,&same);CHKERRQ(ierr);
  if (!same) SETERRQ(PetscObjectComm((PetscObject)B),PETSC_ERR_SUP,"Only valid for MatNest");
  ierr = MatGetDM(B,&dm);CHKERRQ(ierr);
  if (!dm) SETERRQ(PetscObjectComm((PetscObject)B),PETSC_ERR_SUP,"Only valid if MatNest has a DM attached");
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMCOMPOSITE,&same);CHKERRQ(ierr);
  if (!same) SETERRQ(PetscObjectComm((PetscObject)B),PETSC_ERR_SUP,"Only valid for attached DM is a DMCOMPOSITE");
  
  ierr = DMCompositeGetNumberDM(dm,&ndms);CHKERRQ(ierr);
  ierr = PetscMalloc1(ndms,&dmlist);CHKERRQ(ierr);
  ierr = PetscMalloc1(ndms,&Xlist);CHKERRQ(ierr);
  ierr = DMCompositeGetEntriesArray(dm,dmlist);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(dm,&X);CHKERRQ(ierr);
  ierr = DMCompositeGetAccessArray(dm,X,ndms,NULL,Xlist);CHKERRQ(ierr);
  
  ierr = MatNestGetSubMats(B,&m,&n,&bA);CHKERRQ(ierr);
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      if (!bA[i][j]) {
        Mat Aij;
        DM dmi,dmj;
        PetscInt Mv,mv,Nv,nv;
        const char *pre1;
        const char *pre2;
        char prefix[PETSC_MAX_PATH_LEN];
        
        // get row/col space associated with the DM
        dmi = dmlist[i];
        dmj = dmlist[j];
        
        ierr = VecGetSize(Xlist[i],&Mv);CHKERRQ(ierr);
        ierr = VecGetLocalSize(Xlist[i],&mv);CHKERRQ(ierr);
        ierr = VecGetSize(Xlist[j],&Nv);CHKERRQ(ierr);
        ierr = VecGetLocalSize(Xlist[j],&nv);CHKERRQ(ierr);
        
        
        ierr = MatCreate(PetscObjectComm((PetscObject)B),&Aij);CHKERRQ(ierr);
        
        /* Label the empty block via a prefix: -xxx_B_00_default or -xxx_B_u_u */
        ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"B_");CHKERRQ(ierr);
        ierr = DMGetOptionsPrefix(dmi,&pre1);CHKERRQ(ierr);
        if (pre1) {
          ierr = PetscStrcat(prefix,pre1);CHKERRQ(ierr);
        } else {
          char index[PETSC_MAX_PATH_LEN];
          
          ierr = PetscSNPrintf(index,PETSC_MAX_PATH_LEN-1,"%D",i);CHKERRQ(ierr);
          ierr = PetscStrcat(prefix,index);CHKERRQ(ierr);
        }
        
        ierr = DMGetOptionsPrefix(dmj,&pre2);CHKERRQ(ierr);
        if (pre2) {
          ierr = PetscStrcat(prefix,pre2);CHKERRQ(ierr);
        } else {
          char index[PETSC_MAX_PATH_LEN];
          
          ierr = PetscSNPrintf(index,PETSC_MAX_PATH_LEN-1,"%D_",j);CHKERRQ(ierr);
          ierr = PetscStrcat(prefix,index);CHKERRQ(ierr);
        }
        ierr = PetscStrcat(prefix,"default");CHKERRQ(ierr);
        ierr = PetscObjectSetName((PetscObject)Aij,prefix);CHKERRQ(ierr);
        
        ierr = MatSetSizes(Aij,mv,nv,Mv,Nv);CHKERRQ(ierr);
        ierr = MatSetType(Aij,MATAIJ);CHKERRQ(ierr);
        ierr = MatSeqAIJSetPreallocation(Aij,0,NULL);CHKERRQ(ierr);
        ierr = MatMPIAIJSetPreallocation(Aij,0,NULL,0,NULL);CHKERRQ(ierr);
        ierr = MatAssemblyBegin(Aij,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
        ierr = MatAssemblyEnd(Aij,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
        
        bA[i][j] = Aij;
      }
    }
  }
  ierr = MatAssemblyBegin(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreAccessArray(dm,X,ndms,NULL,Xlist);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = PetscFree(Xlist);CHKERRQ(ierr);
  ierr = PetscFree(dmlist);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

