
//
// Dynamic Sparse Data Exchanger
//
// * Supports communication patterns where only the sending rank knows the dest rank.
//   The receivers dynamically determine how they should receive from.
// * The communication topology is dynamically determined during the packing of data.
// * Packed buffers can be flushed at any time based on the provided API. That is you
//   can pack - send/recv multiple times during a single pass of your data structure.
// * Default behaviour is that the communication topology can be dynamically modified
//   at any time data is to be packed. Support is provided to lock the topology for
//   efficient re-use of the data exchanger. This also serves to prevent bugs in the user
//   code. Once the topology is locked, an error will occur if a new destination rank is
//   defined.
//
//

#include "dsde.h"

const char *dsde_status_names[] = { "initialized", "finalized", "unknown", "flushed" };

PetscLogEvent DSDE_DataExchangerBegin;
PetscLogEvent DSDE_DataExchangerEnd;

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackCreate"
PetscErrorCode DSDataExPackCreate(DSDataExPack *p)
{
  DSDataExPack pack;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&pack);CHKERRQ(ierr);
  ierr = PetscMemzero(pack,sizeof(struct _p_DSDataExPack));CHKERRQ(ierr);
  pack->src = -1;
  pack->dest = -1;
  pack->buffer = NULL;
  *p = pack;
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackInit"
PetscErrorCode DSDataExPackInit(DSDataExPack p,PetscMPIInt src,PetscMPIInt dest,size_t atomic_size)
{
  p->src = src;
  p->dest = dest;
  p->unit_message_size = atomic_size;
  p->nitems = 0;
  if (p->buffer) {
    free(p->buffer);
  }
  /* allocate something so realloc can be called later */
  p->buffer = malloc(sizeof(char));
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackInsert"
PetscErrorCode DSDataExPackInsert(DSDataExPack p,PetscInt n,void *data)
{
  size_t old_length,new_length,buff_length;
  void *tmp,*dest_buffer;
  
  old_length = (size_t)(p->nitems * p->unit_message_size);
  new_length = (size_t)((p->nitems + n) * p->unit_message_size);

  tmp = (void*)realloc( p->buffer, new_length );
  p->buffer = tmp;

  /* copy */
  buff_length = (size_t)(n * p->unit_message_size);
  dest_buffer = ((char*)p->buffer) + old_length;
  memcpy(dest_buffer,data,buff_length);
  
  p->nitems = p->nitems + n;
  
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackDestroy"
PetscErrorCode DSDataExPackDestroy(DSDataExPack *p)
{
  PetscErrorCode ierr;
  DSDataExPack pack;
  
  if (!p) PetscFunctionReturn(0);
  pack = *p;
  if (!pack) PetscFunctionReturn(0);
  if (pack->buffer) {
    free(pack->buffer);
    pack->buffer = NULL;
  }
  ierr = PetscFree(pack);CHKERRQ(ierr);
  
  *p = NULL;
  
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackData"
PetscErrorCode DSDataExPackData(DSDataEx de,PetscMPIInt dest,PetscInt n,void *data)
{
  PetscMPIInt    k,np;
  PetscBool      found;
  DSDataExPack   p = NULL;
  PetscErrorCode ierr;
  
  /* if pack status is not initialized - error */
  PetscFunctionBegin;
  if (de->packer_status != DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Packer not initialized. Must call DSDataExPackInitialize() first" );
  if (de->communication_status == DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Communication has not finished - cannot pack" );
  
  if (de->rank == dest) {
    PetscPrintf(PETSC_COMM_SELF,"  ** warning ** rank %D is attempting to send a message to itself - not packing this data\n",(PetscInt)de->rank);
    PetscFunctionReturn(0);
  }
  
  /* look for a buffer associated with dest */
  np = de->npacks;
  found  = PETSC_FALSE;
  for (k=0; k<np; k++) {
    if (de->pack[k]->dest == dest) {
      p = de->pack[k];
      found  = PETSC_TRUE;
      break;
    }
  }
  
  /* if not found - resize and create one */
  if (!found) {
    DSDataExPack *tmp;

    ierr = DSDataExPackCreate(&p);CHKERRQ(ierr);
    ierr = DSDataExPackInit(p,de->rank,dest,de->unit_message_size);CHKERRQ(ierr);
    
    tmp = (DSDataExPack*)realloc( de->pack, sizeof(DSDataExPack)*(de->npacks+1) );
    de->pack = tmp;
    de->pack[de->npacks] = p;
    de->npacks++;
  }
  
  /* add content */
  ierr = DSDataExPackInsert(p,n,data);CHKERRQ(ierr);
  
  /* update topology map */
  // 1's denote the known send_rank -> dest pair
  ierr = MatSetValue( de->topo, (PetscInt)de->rank, (PetscInt)dest, 1.0, INSERT_VALUES );CHKERRQ(ierr);
  /* Now force all other connections if they are not already there */
  // 2's denote the twin of the known send_rank -> dest pair
  ierr = MatSetValue( de->topo, (PetscInt)dest, (PetscInt)de->rank, 2.0, INSERT_VALUES );CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackFlush"
PetscErrorCode DSDataExPackFlush(DSDataEx de)
{
  PetscMPIInt    k,np;
  PetscErrorCode ierr;
  
  /* if pack status is not initialized - error */
  PetscFunctionBegin;
  if (de->communication_status != DSDEOBJECT_FINALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Communication has not finalized" );
  
  /* look for a buffer associated with dest */
  np = de->npacks;
  for (k=0; k<np; k++) {
    PetscMPIInt src,dest;

    src = de->pack[k]->src;
    dest = de->pack[k]->dest;
    ierr = DSDataExPackInit(de->pack[k],src,dest,de->unit_message_size);CHKERRQ(ierr);
  }

  ierr = MatZeroEntries(de->topo);CHKERRQ(ierr);
  //ierr = MatSetOption(de->topo,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);CHKERRQ(ierr);

  de->packer_status   = DSDEOBJECT_INITIALIZED;
  de->topology_status = DSDEOBJECT_INITIALIZED;

  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExTopologySetUseOnlyActiveNeighbours"
PetscErrorCode DSDataExTopologySetUseOnlyActiveNeighbours(DSDataEx de,PetscBool use_active)
{
  de->use_only_active_neighbours = use_active;
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExTopologySetMode"
PetscErrorCode DSDataExTopologySetMode(DSDataEx de,DSDETopologyMode mode)
{
  PetscErrorCode ierr;
  
  de->topology_mode = mode;
  switch (mode) {
    case DSDE_TOPOLOGY_DYNAMIC:
      ierr = MatSetOption(de->topo,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);CHKERRQ(ierr);
      break;
    case DSDE_TOPOLOGY_LOCKED:
      ierr = MatSetOption(de->topo,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(de->comm,PETSC_ERR_USER,"Unknown DSDETopologyMode provided");
      break;
  }
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExTopologyReset"
PetscErrorCode DSDataExTopologyReset(DSDataEx de)
{
  PetscMPIInt    commsize;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  
  if (de->communication_status == DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Messages are already in flight. Must call DSDataExEnd() first" );

  if (de->topology_mode == DSDE_TOPOLOGY_LOCKED) SETERRQ(de->comm,PETSC_ERR_USER,"DSDETopology is locked but you are requesting to re-initialize it. Call DSDataExTopologySetMode(de,DSDE_TOPOLOGY_DYNAMIC) first");

  if (de->topo) {
    ierr = MatDestroy(&de->topo);CHKERRQ(ierr);
  }
  ierr = MPI_Comm_size(de->comm,&commsize);CHKERRQ(ierr);
  ierr = MatCreate(de->comm,&de->topo);CHKERRQ(ierr);
  ierr = MatSetSizes(de->topo,PETSC_DECIDE,PETSC_DECIDE,commsize,commsize);CHKERRQ(ierr);
  ierr = MatSetType(de->topo,MATAIJ);CHKERRQ(ierr);
  ierr = MatSetUp(de->topo);CHKERRQ(ierr);
  ierr = MatSetOption(de->topo,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackInitialize"
PetscErrorCode DSDataExPackInitialize(DSDataEx de,size_t unit_message_size)
{
  PetscMPIInt    k,np;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  
  if (de->communication_status == DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Messages are already in flight. Must call DSDataExEnd() first" );
  
  de->unit_message_size = unit_message_size;

  np = de->npacks;
  if (de->pack) {
    for (k=0; k<np; k++) {
      // destroy pack[k]
      ierr = DSDataExPackDestroy(&de->pack[k]);CHKERRQ(ierr);
    }
    free(de->pack);
  }
  de->pack = malloc(sizeof(DSDataExPack));
  de->npacks = 0;
  
  ierr = MatZeroEntries(de->topo);CHKERRQ(ierr);
  
  de->packer_status   = DSDEOBJECT_INITIALIZED;
  de->topology_status = DSDEOBJECT_INITIALIZED;
  de->communication_status = DSDEOBJECT_STATE_UNKNOWN;
  
  PetscFunctionReturn(0);
}

/*
 Makes the communication map symmetric
 */
//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "_DSDataExCompleteCommunicationMap"
PetscErrorCode _DSDataExCompleteCommunicationMap(DSDataEx de)
{
  PetscInt          j,nc;
  const PetscInt    *cols;
  const PetscScalar *vals;
  PetscLogDouble    t0,t1;
  PetscBool         debug = PETSC_FALSE;
  PetscErrorCode    ierr;
  
  
  PetscFunctionBegin;
  PetscTime(&t0);
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-dsde_topo_debug",&debug,NULL);CHKERRQ(ierr);
  if (debug) {
    //ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_INFO);CHKERRQ(ierr);
    ierr = MatView(de->topo,PETSC_VIEWER_STDOUT_(de->comm));CHKERRQ(ierr);
    //ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  
  ierr = MatGetRow( de->topo, (PetscInt)de->rank, &nc, &cols, &vals );CHKERRQ(ierr);
  
  de->nneighbours = (PetscMPIInt)nc;
  
  if (de->neighbour) {
    ierr = PetscFree(de->neighbour);CHKERRQ(ierr);
  }
  ierr = PetscMalloc1(nc+1,&de->neighbour);CHKERRQ(ierr);
  
  for (j=0; j<nc; j++) {
    de->neighbour[j] = (PetscMPIInt)cols[j];
  }
  
  ierr = MatRestoreRow( de->topo, (PetscInt)de->rank, &nc, &cols, &vals );CHKERRQ(ierr);
  
  ierr = MPI_Barrier(de->comm);CHKERRQ(ierr);
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"[DSDataEx] setup communication map: %1.4e (sec) \n",t1-t0);
  
  de->topology_status = DSDEOBJECT_FINALIZED;
  
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "_DSDataExCompleteActiveCommunicationMap"
PetscErrorCode _DSDataExCompleteActiveCommunicationMap(DSDataEx de)
{
  PetscInt          j,nc,nc_active;
  const PetscInt    *cols;
  const PetscScalar *vals;
  PetscLogDouble    t0,t1;
  PetscBool         debug = PETSC_FALSE;
  PetscErrorCode    ierr;
  
  
  PetscFunctionBegin;
  PetscTime(&t0);
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-dsde_topo_debug",&debug,NULL);CHKERRQ(ierr);
  if (debug) {
    //ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_INFO);CHKERRQ(ierr);
    ierr = MatView(de->topo,PETSC_VIEWER_STDOUT_(de->comm));CHKERRQ(ierr);
    //ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  
  ierr = MatGetRow( de->topo, (PetscInt)de->rank, &nc, &cols, &vals );CHKERRQ(ierr);
  
  nc_active = 0;
  for (j=0; j<nc; j++) {
    if (vals[j] > 0.0) {
      nc_active++;
    }
  }
  
  de->nneighbours = (PetscMPIInt)nc_active;
  
  if (de->neighbour) {
    ierr = PetscFree(de->neighbour);CHKERRQ(ierr);
  }
  ierr = PetscMalloc1(nc_active+1,&de->neighbour);CHKERRQ(ierr);
  
  nc_active = 0;
  for (j=0; j<nc; j++) {
    if (vals[j] > 0.0) {
      de->neighbour[nc_active] = (PetscMPIInt)cols[j];
      nc_active++;
    }
  }
  
  ierr = MatRestoreRow( de->topo, (PetscInt)de->rank, &nc, &cols, &vals );CHKERRQ(ierr);
  
  ierr = MPI_Barrier(de->comm);CHKERRQ(ierr);
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"[DSDataEx] setup active communication map: %1.4e (sec) \n",t1-t0);
  
  de->topology_status = DSDEOBJECT_FINALIZED;
  
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExPackFinalize"
PetscErrorCode DSDataExPackFinalize(DSDataEx de)
{
  PetscErrorCode ierr;
  PetscInt i,k;
  
  if (de->packer_status != DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Packer not initialized. Must call DSDataExPackInitialize() first" );
  
  ierr = MatAssemblyBegin(de->topo,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(de->topo,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  /* complete the communication map */
  if (de->use_only_active_neighbours) {
    ierr = _DSDataExCompleteActiveCommunicationMap(de);CHKERRQ(ierr);
  } else {
    ierr = _DSDataExCompleteCommunicationMap(de);CHKERRQ(ierr);
  }
  
  /* create dummy packs */
  for (i=0; i<de->nneighbours; i++) {
    PetscMPIInt dest = de->neighbour[i];
    PetscBool found = PETSC_FALSE;
    
    found  = PETSC_FALSE;
    for (k=0; k<de->npacks; k++) {
      if (de->pack[k]->dest == dest) {
        found  = PETSC_TRUE;
        break;
      }
    }
    
    /* if not found - resize and create one */
    if (!found) {
      DSDataExPack *tmp,p;
      
      ierr = DSDataExPackCreate(&p);CHKERRQ(ierr);
      ierr = DSDataExPackInit(p,de->rank,dest,de->unit_message_size);CHKERRQ(ierr);
      
      tmp = (DSDataExPack*)realloc( de->pack, sizeof(DSDataExPack)*(de->npacks+1) );
      de->pack = tmp;
      de->pack[de->npacks] = p;
      de->npacks++;
    }
  }
  
  /* put the neighbours in the order matching the pack[] */
  for (i=0; i<de->nneighbours; i++) {
    de->neighbour[i] = de->pack[i]->dest;
  }
  
  
  if (de->_stats) {
    free(de->_stats);
    de->_stats = NULL;
  }
  de->_stats = (MPI_Status*)malloc( sizeof(MPI_Status) * (2*de->nneighbours+1) );
  
  if (de->_requests) {
    free(de->_requests);
    de->_requests = NULL;
  }
  de->_requests = (MPI_Request*)malloc( sizeof(MPI_Request) * (2*de->nneighbours+1) );
  
  if (de->message_offsets         )  {  free(de->message_offsets);            de->message_offsets          = NULL;  }
  if (de->messages_to_be_recvieved)  {  free(de->messages_to_be_recvieved);   de->messages_to_be_recvieved = NULL;  }

  de->message_offsets = (PetscInt*)malloc( sizeof(PetscInt) * (de->nneighbours+1) );
  de->messages_to_be_recvieved = (PetscInt*)malloc( sizeof(PetscInt) * (de->nneighbours+1) );
  for (i=0; i<de->nneighbours+1; i++) {
    de->message_offsets[i] = 0;
    de->messages_to_be_recvieved[i] = 0;
  }
  
  if (de->recv_message) {
    free(de->recv_message);
    de->recv_message = NULL;
  }

  de->recv_message_length = 0;
  
  de->packer_status   = DSDEOBJECT_FINALIZED;
  
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExCreate"
PetscErrorCode DSDataExCreate(MPI_Comm comm,DSDataEx *dsde)
{
	DSDataEx       d;
  PetscMPIInt    commsize;
	PetscErrorCode ierr;
	
  ierr = PetscMalloc1(1,&d);CHKERRQ(ierr);
  ierr = PetscMemzero(d,sizeof(struct _p_DSDataEx));CHKERRQ(ierr);
	
  d->commref = comm;
  ierr = PetscCommDuplicate(d->commref,&d->comm,&d->tag);CHKERRQ(ierr);
	ierr = MPI_Comm_rank(d->comm,&d->rank);CHKERRQ(ierr);
	
	d->npacks = 0;
  d->pack   = malloc(sizeof(DSDataExPack));
  
  d->nneighbours = 0;
  d->neighbour = NULL;
  
  d->topology_status        = DSDEOBJECT_STATE_UNKNOWN;
  d->packer_status          = DSDEOBJECT_STATE_UNKNOWN;
  d->communication_status   = DSDEOBJECT_STATE_UNKNOWN;

  d->_stats    = NULL;
  d->_requests = NULL;
  
  d->message_offsets          = NULL;
  d->messages_to_be_recvieved = NULL;

  d->recv_message_length = -1;
  d->recv_message        = NULL;
	
  d->topology_mode = DSDE_TOPOLOGY_DYNAMIC;
  
  ierr = MPI_Comm_size(d->comm,&commsize);CHKERRQ(ierr);
  ierr = MatCreate(d->comm,&d->topo);CHKERRQ(ierr);
  ierr = MatSetSizes(d->topo,PETSC_DECIDE,PETSC_DECIDE,commsize,commsize);CHKERRQ(ierr);
  ierr = MatSetType(d->topo,MATAIJ);CHKERRQ(ierr);
  ierr = MatSetUp(d->topo);CHKERRQ(ierr);
  ierr = MatSetOption(d->topo,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);CHKERRQ(ierr);
  
  d->use_only_active_neighbours = PETSC_FALSE;
  
  *dsde = d;
  
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExDestroy"
PetscErrorCode DSDataExDestroy(DSDataEx *dsde)
{
  PetscErrorCode ierr;
  DSDataEx d;
  PetscInt i;
  
  PetscFunctionBegin;
  
  if (!dsde) PetscFunctionReturn(0);
  d = *dsde;
  if (!d) PetscFunctionReturn(0);
  
  ierr = PetscCommDestroy(&d->comm);CHKERRQ(ierr);

  if (d->topo) {
    ierr = MatDestroy(&d->topo);CHKERRQ(ierr);
  }
  
  if (d->pack) {
    for (i=0; i<d->npacks; i++) {
      ierr = DSDataExPackDestroy(&d->pack[i]);CHKERRQ(ierr);
    }
    free(d->pack);
  }
  
  if (d->neighbour) {
    ierr = PetscFree(d->neighbour);CHKERRQ(ierr);
  }
  
  if (d->message_offsets) {
    free(d->message_offsets);
  }
  
  if (d->messages_to_be_recvieved) {
    free(d->messages_to_be_recvieved);
  }
  
  if (d->recv_message) {
    free(d->recv_message);
  }
  
  if (d->_stats) {
    free(d->_stats);
  }
  
  if (d->_requests) {
    free(d->_requests);
  }
  
  ierr = PetscFree(d);CHKERRQ(ierr);
  *dsde = NULL;
  
  PetscFunctionReturn(0);
}

/* do the actual message passing now */
//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExBegin"
PetscErrorCode DSDataExBegin(DSDataEx de)
{
  PetscMPIInt i,np;
  void       *buffer;
  PetscInt    length;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  if (de->packer_status == DSDEOBJECT_STATE_UNKNOWN) SETERRQ( de->comm, PETSC_ERR_ORDER, "Packer not initialized. Must call DSDataExPackInitialize(), DSDataExPackFinalize() first" );
  if (de->packer_status != DSDEOBJECT_FINALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Packer not finalized. Must call DSDataExPackFinalize() first" );

  if (de->topology_status == DSDEOBJECT_STATE_UNKNOWN) SETERRQ( de->comm, PETSC_ERR_ORDER, "Topology not initialized. Must call DSDataExPackInitialize(), DSDataExPackFinalize() first" );
  if (de->topology_status != DSDEOBJECT_FINALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Topology not finalized. Must call DSDataExPackFinalize() first" );
  
  if (de->communication_status == DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Messages are already in flight. Must call DSDataExEnd() first" );
  
  ierr = PetscLogEventBegin(DSDE_DataExchangerBegin,0,0,0,0);CHKERRQ(ierr);
  np = de->npacks;
  
  ierr = PetscCommGetNewTag(de->comm,&de->tag);CHKERRQ(ierr);
  
  /* == NON BLOCKING == */
  for (i=0; i<np; i++) {
    length = de->pack[i]->unit_message_size * de->pack[i]->nitems;
    buffer = (char*)de->pack[i]->buffer;
    
    //printf("de->pack[%d] %p\n",i,de->pack[i]);
    //printf(" buffer %p : len %d : dest %d : r %p\n",buffer,length,de->pack[i]->dest,&de->_requests[i]);
    
    ierr = MPI_Isend( buffer, length, MPI_CHAR, de->pack[i]->dest, de->tag, de->comm, &de->_requests[i] );
    //printf("ierr = %d : tag %d %d\n",(int)ierr,(int)MPI_ERR_TAG,(int)MPI_ANY_TAG);
    CHKERRQ(ierr);
  }
  
  ierr = PetscLogEventEnd(DSDE_DataExchangerBegin,0,0,0,0);CHKERRQ(ierr);

  de->communication_status = DSDEOBJECT_INITIALIZED;

  PetscFunctionReturn(0);
}

/* do the actual message passing now */
//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExEnd"
PetscErrorCode DSDataExEnd(DSDataEx de)
{
  PetscMPIInt    i,np,total;
  void           *dest_buffer;
  PetscInt       length;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  if (de->communication_status != DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Communication has not been initialized. Must call DSDataExBegin() first." );
  
  ierr = PetscLogEventBegin(DSDE_DataExchangerEnd,0,0,0,0);CHKERRQ(ierr);
  np = de->nneighbours;

  ierr = MPI_Waitall( np, de->_requests, de->_stats );CHKERRQ(ierr);
  
  for (i=0; i<np; i++) {
    MPI_Status status;
    int count;
    
    ierr = MPI_Probe(de->neighbour[i],de->tag,de->comm,&status);CHKERRQ(ierr);
    
    ierr = MPI_Get_count(&status,MPI_CHAR,&count);CHKERRQ(ierr);
    
    de->messages_to_be_recvieved[i] = (PetscInt)( count/((int)de->unit_message_size) );
  }
  
  de->message_offsets[0] = 0;
  total = de->messages_to_be_recvieved[0];
  for (i=1; i<np; i++) {
    de->message_offsets[i] = total;
    total = total + de->messages_to_be_recvieved[i];
  }
  /* set total items to recieve */
  de->recv_message_length = total;

  de->recv_message = (void*)malloc( de->unit_message_size * (total + 1) );
  /* initialize memory */
  memset( de->recv_message, 0, de->unit_message_size * (total + 1) );
  
  
  /* allocate storage for all received data */
  
  
  for (i=0; i<np; i++) {
    length = de->messages_to_be_recvieved[i] * de->unit_message_size;
    dest_buffer = ((char*)de->recv_message) + de->unit_message_size * de->message_offsets[i];
    
    ierr = MPI_Irecv( dest_buffer, length, MPI_CHAR, de->neighbour[i], de->tag, de->comm, &de->_requests[np+i] );CHKERRQ(ierr);
  }
  
  ierr = MPI_Waitall( 2*np, de->_requests, de->_stats );CHKERRQ(ierr);
  
  de->communication_status = DSDEOBJECT_FINALIZED;
  ierr = PetscLogEventEnd(DSDE_DataExchangerEnd,0,0,0,0);CHKERRQ(ierr);
  
  /* flush packed data */
  np = de->npacks;
  for (i=0; i<np; i++) {
    ierr = DSDataExPackDestroy(&de->pack[i]);CHKERRQ(ierr);
  }
  free(de->pack);
  de->pack = malloc(sizeof(DSDataExPack));
  de->npacks = 0;
  de->packer_status = DSDEOBJECT_FLUSHED;

  
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExGetRecvData"
PetscErrorCode DSDataExGetRecvData(DSDataEx de,PetscInt *length,void **recv)
{
  PetscFunctionBegin;
  if (de->communication_status != DSDEOBJECT_FINALIZED) SETERRQ( PETSC_COMM_SELF, PETSC_ERR_ARG_WRONGSTATE, "Data has not finished being sent." );
  
  *length = de->recv_message_length;
  *recv   = de->recv_message;
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExGetRecvDataByRank"
PetscErrorCode DSDataExGetRecvDataByRank(DSDataEx de,PetscMPIInt src,PetscInt *length,void **recv)
{
  PetscInt i,offset;
  PetscBool found;
  
  PetscFunctionBegin;
  if (de->communication_status != DSDEOBJECT_FINALIZED) SETERRQ( PETSC_COMM_SELF, PETSC_ERR_ARG_WRONGSTATE, "Data has not finished being sent." );
  
  *length = 0;
  *recv = NULL;
  found = PETSC_FALSE;
  
  for (i=0; i<de->nneighbours; i++) {
    if (src == de->neighbour[i]) {
      *length = de->messages_to_be_recvieved[i];
      offset = de->message_offsets[i];
      found = PETSC_TRUE;
      break;
    }
  }
  
  if (found) {
    void *buffer;
    
    buffer = ((char*)de->recv_message) + de->unit_message_size * offset;
    
    *recv   = buffer;
  }
  PetscFunctionReturn(0);
}

//
// Not collective
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExTopologyGetNeighbours"
PetscErrorCode DSDataExTopologyGetNeighbours(DSDataEx de,PetscMPIInt *n,PetscInt *neigh[])
{
  PetscFunctionBegin;
  if (de->topology_status != DSDEOBJECT_FINALIZED) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ORDER,"Cannot query neighbours if the topology is not defined");
  
  if (n)     { *n     = de->nneighbours; }
  if (neigh) { *neigh = de->neighbour; }
  PetscFunctionReturn(0);
}

//
// Collective on DSDataEx
//
#undef __FUNCT__
#define __FUNCT__ "DSDataExView"
PetscErrorCode DSDataExView(DSDataEx d)
{
	PetscMPIInt    p;
  PetscErrorCode ierr;
	
	PetscFunctionBegin;
  MPI_Barrier(d->comm);
	PetscPrintf( d->comm, "DataSparseDataEx\n");
	
	PetscPrintf( d->comm, "  topology status:        %s \n", dsde_status_names[d->topology_status]);
	PetscPrintf( d->comm, "  packer status status:   %s \n", dsde_status_names[d->packer_status] );
	PetscPrintf( d->comm, "  communication status:   %s \n", dsde_status_names[d->communication_status] );

  if (d->topology_status == DSDEOBJECT_FINALIZED) {
		PetscPrintf( d->comm, "  Topology:\n");
    if (d->topology_mode == DSDE_TOPOLOGY_LOCKED) {
      PetscPrintf( d->comm, "    mode: locked\n");
    } else {
      PetscPrintf( d->comm, "    mode: dynamic\n");
    }
    if (d->use_only_active_neighbours) {
      PetscPrintf( d->comm, "    communication pattern mode: using only active neighbours in graph\n");
    } else {
      PetscPrintf( d->comm, "    communication pattern mode: using all defined neighbours in graph\n");
    }
		PetscSynchronizedPrintf( d->comm, "    [%d] neighbours: %d \n", (int)d->rank, (int)d->nneighbours );
		for (p=0; p<d->nneighbours; p++) {
			PetscSynchronizedPrintf( d->comm, "    [%d]   neighbour[%D] = %d \n", (int)d->rank, p, (int)d->neighbour[p]);
		}
    PetscSynchronizedFlush(d->comm,PETSC_STDOUT);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_(d->comm));CHKERRQ(ierr);
    ierr = MatView(d->topo,PETSC_VIEWER_STDOUT_(d->comm));CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_(d->comm));CHKERRQ(ierr);
	}
  MPI_Barrier(d->comm);
  
	if (d->communication_status == DSDEOBJECT_FINALIZED) {
		PetscPrintf( d->comm, "  Message lengths:\n");
    PetscPrintf( d->comm, "    using tag: %D\n",(PetscInt)d->tag);
    PetscPrintf( d->comm, "    atomic size: %ld \n", (long int)d->unit_message_size );
    //cannot view if we flush packed data after send
    //for (p=0; p<d->nneighbours; p++) {
    //	PetscSynchronizedPrintf( d->comm, "    [%d] >>>>> ( %D units :: tag = 0 ) >>>>> [%d] \n", (int)d->rank, d->pack[p]->nitems, (int)d->neighbour[p] );
    //}
		for (p=0; p<d->nneighbours; p++) {
			PetscSynchronizedPrintf( d->comm, "    [%d] <<<<< ( %D units) <<<<< [%d] \n", (int)d->rank, d->messages_to_be_recvieved[p], (int)d->neighbour[p] );
		}
    PetscSynchronizedFlush(d->comm,PETSC_STDOUT);
	}
	PetscFunctionReturn(0);
}


#ifdef DSDE_PARTIAL_GATHER
/*
 Really unsafe without extra data structure support.
*/
#undef __FUNCT__
#define __FUNCT__ "DSDataExEndByRank"
PetscErrorCode DSDataExEndByRank(DSDataEx de,PetscBool first_call,PetscBool final_call,PetscMPIInt src)
{
  PetscMPIInt    i,np,total,src_id = -1;
  void           *dest_buffer;
  PetscInt       length;
  PetscMPIInt    commrank;
  int            message_complete;
  MPI_Status     status;
  PetscErrorCode ierr;
  
  
  PetscFunctionBegin;
  if (de->communication_status != DSDEOBJECT_INITIALIZED) SETERRQ( de->comm, PETSC_ERR_ORDER, "Communication has not been initialized. Must call DSDataExBegin() first." );
  ierr = MPI_Comm_rank(de->comm,&commrank);CHKERRQ(ierr);
  
  ierr = PetscLogEventBegin(DSDE_DataExchangerEnd,0,0,0,0);CHKERRQ(ierr);
  
  if (de->recv_message) {
    free(de->recv_message);
    de->recv_message = NULL;
  }
  
  np = de->nneighbours;
  
  if (first_call) {
    ierr = MPI_Waitall( np, de->_requests, de->_stats );CHKERRQ(ierr);
    
    for (i=0; i<np; i++) {
      MPI_Status status;
      int count;
      
      ierr = MPI_Probe(de->neighbour[i],de->tag,de->comm,&status);CHKERRQ(ierr);
      
      ierr = MPI_Get_count(&status,MPI_CHAR,&count);CHKERRQ(ierr);
      
      de->messages_to_be_recvieved[i] = (PetscInt)( count/((int)de->unit_message_size) );
      
      //ierr = MPI_Request_free(&de->_requests[np+i]);CHKERRQ(ierr);
      de->_requests[np+i] = MPI_REQUEST_NULL;
      //de->_stats[np+i] = *MPI_STATUS_IGNORE;
    }
  }
  
  for (i=0; i<np; i++) {
    if (de->neighbour[i] == src) {
      src_id = i;
      total = de->messages_to_be_recvieved[i];
      break;
    }
  }
  if (src_id == -1) {//SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER, "Requested rank is not a topological neighbour");
    de->message_offsets[0] = 0;
    de->recv_message_length = 0;
    de->recv_message = NULL;
    de->communication_status = DSDEOBJECT_FINALIZED;
    PetscFunctionReturn(0);
  }
  
  
  de->message_offsets[0] = 0;

  /* set total items to recieve */
  de->recv_message_length = total;
  
  de->recv_message = (void*)malloc( de->unit_message_size * (total + 1) );
  /* initialize memory */
  memset( de->recv_message, 0, de->unit_message_size * (total + 1) );
  
  
  /* allocate storage for all received data */
  
  
  length = de->messages_to_be_recvieved[ src_id ] * de->unit_message_size;
  dest_buffer = ((char*)de->recv_message) + de->unit_message_size * de->message_offsets[ 0 ];
  
  //message_complete = 0;
  //ierr = MPI_Test(&de->_requests[np + src_id], &message_complete, &status);CHKERRQ(ierr);
  //if (message_complete == 1) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Messages from %D to %D have already been received",src_id,commrank );
  //if (de->_stats[np + src_id] != *MPI_STATUS_IGNORE) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Messages from %D to %D have already been received",src_id,commrank );
  //ierr = MPI_Iprobe(de->neighbour[src_id],de->tag,de->comm,&message_complete,&status);CHKERRQ(ierr);
  //if (message_complete == 0) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Messages from %D to %D have already been received",src_id,commrank );
  
  if (de->_requests[np + src_id] != MPI_REQUEST_NULL) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Messages from %D to %D have already been received",src_id,commrank );
  
  ierr = MPI_Irecv( dest_buffer, length, MPI_CHAR, de->neighbour[ src_id ], de->tag, de->comm, &de->_requests[np + src_id] );CHKERRQ(ierr);
  
  ierr = MPI_Waitall( 1, &de->_requests[np + src_id], &de->_stats[np + src_id] );CHKERRQ(ierr);
  
  de->communication_status = DSDEOBJECT_FINALIZED;
  ierr = PetscLogEventEnd(DSDE_DataExchangerEnd,0,0,0,0);CHKERRQ(ierr);
  
  /* flush packed data */
  if (final_call) {
    np = de->npacks;
    for (i=0; i<np; i++) {
      ierr = DSDataExPackDestroy(&de->pack[i]);CHKERRQ(ierr);
    }
    free(de->pack);
    de->pack = malloc(sizeof(DSDataExPack));
    de->npacks = 0;
    de->packer_status = DSDEOBJECT_FLUSHED;
  }
  
  PetscFunctionReturn(0);
}
#endif

