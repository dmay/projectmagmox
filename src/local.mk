libproj-y.c += $(call thisdir, \
    proj_init_finalize.c \
    dmbcs.c \
    quadrature.c quadrature_iterator.c \
    mpiio_blocking.c \
    data_bucket.c \
    paraview_utils.c \
    data_exchanger.c dsde.c \
)

PROJ_INC += -I$(abspath $(call thisdir,.))

include $(call incsubdirs,dmtet pde coefficient analytics petsc-utils utils tests)
