#
#  Evaluates analytic fields defined in the following paper
#
#  "Simple 2-D models for melt extraction at mid-ocean ridges and island arcs"
#  Marc Spiegelman and Dan McKenzie,
#  EPSL 83 (1987) 137-152
#

from sympy import *

r = Symbol('r')
theta = Symbol('theta')
p = Symbol('p')

def DefinePsiSolid_Ridge():
  A = Symbol('A')
  B = Symbol('B')

  psi_s = r * (A *sin(theta) - B*theta*cos(theta))
  return(psi_s)

def DefinePsiFluid_Ridge():
  w = Symbol('w0')
  U = Symbol('U0')
  B = Symbol('B')
  
  psi_s = DefinePsiSolid_Ridge()
  psi_f = -(w/U) * (2*B/r + r) * sin(theta) + psi_s
  
  return(psi_f)

def DefinePressure_Ridge():
  B = Symbol('B')
  
  P = (-2*B/r + r) * cos(theta)
  
  return(P)


def DefinePsiSolid_Trench():
  C = Symbol('C')
  D = Symbol('D')
  
  psi_s = r * (C * (sin(theta) - theta*cos(theta)) + D*theta*sin(theta))
  return(psi_s)

def DefinePsiFluid_Trench():
  w = Symbol('w0')
  U = Symbol('U0')
  C = Symbol('C')
  D = Symbol('D')
  
  psi_s = DefinePsiSolid_Trench()
  psi_f = -(w/U) * ( (2/r)*( C * sin(theta) + D*cos(theta)  ) - r*cos(theta)) + psi_s
  
  return(psi_f)

#psi = DefinePsiSolid_Ridge()
#psi = DefinePsiFluid_Ridge()
p = DefinePressure_Ridge()
#psi = DefinePsiSolid_Trench()
psi = DefinePsiFluid_Trench()
               
psi_dr = diff(psi,r)
psi_dt = diff(psi,theta)

vx = (1/r) * psi_dt
vz = -psi_dr

c_vx = ccode(vx)
c_vz = ccode(vz)
c_p = ccode(p)

print('vr =',c_vx,';')
print('vt =',c_vz,';')
print('p =',c_p,';')
