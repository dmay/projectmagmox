from sympy import *

x = Symbol('x[0]')
y = Symbol('x[1]')


#u =  sin(5.0*pi*x) * cos(5.0*pi*y)
#v = -cos(5.0*pi*x) * sin(5.0*pi*y)

# Choose u
u =  sin(5.0*pi*x) * cos(3.0*pi*y)

# Create divergence free solution
dudx = diff(u,x)
dvdy = dudx
int_dvdy = integrate(-dvdy,y)
v = int_dvdy

#p = exp(y*y)*sin(4.0*x*pi)*cos(3.0*pi*y+pi*x)
#p = sin(4.0*x*pi)*cos(3.0*pi*y+pi*x)
p = sin(1.5*x*pi)*cos(3.4*pi*y+pi*x)

_int_p_y = integrate(p,(y,0.0,1.0))
int_p_y = _int_p_y.doit()
int_p = integrate(int_p_y,(x,0.0,1.0))
p_avg = int_p.doit()

p = p - p_avg

eta = 1.0e-5*(14.0 + exp(11.0*x)*sin(y) )

exx = diff(u,x)
eyy = diff(v,y)
exy = 0.5 * ( diff(u,y) + diff(v,x) )

txx = 2.0 * eta * exx
tyy = 2.0 * eta * eyy
txy = 2.0 * eta * exy


fu = -(diff(txx,x) + diff(txy,y) - diff(p,x))
fv = -(diff(txy,x) + diff(tyy,y) - diff(p,y))
fp = -(exx + eyy)


#print('fu = ', fu)
#print('fv = ', fv)
#print('fp = ', fp)

print('[ccode] uv[0] =', ccode(u) + ';')
print('[ccode] uv[1] =', ccode(v) + ';')
print('[ccode] p[0] =', ccode(p) + ';')
print('[ccode] eta[0] =', ccode(eta) + ';')

print('[ccode] Fu[0] =', ccode(fu) + ';')
print('[ccode] Fu[1] =', ccode(fv) + ';')
print('[ccode] Fp[0] =', ccode(fp) + ';')

# grad(V)
u_x = diff(u,x)
u_y = diff(u,y)
v_x = diff(v,x)
v_y = diff(v,y)
print('[ccode] grad[0] =', ccode(u_x) + ';')
print('[ccode] grad[1] =', ccode(u_y) + ';')
print('[ccode] grad[2] =', ccode(v_x) + ';')
print('[ccode] grad[3] =', ccode(v_y) + ';')

sxx = txx - p
syy = tyy - p
sxy = txy
print('[ccode] sigma[0] =', ccode(sxx) + ';')
print('[ccode] sigma[1] =', ccode(sxy) + ';')
print('[ccode] sigma[2] = sigma[1];')
print('[ccode] sigma[3] =', ccode(syy) + ';')

_int_p_y = integrate(p,(y,0.0,1.0))
int_p_y = _int_p_y.doit()
int_p = integrate(int_p_y,(x,0.0,1.0))
#print('[ccode] p_avg =', ccode(p_avg) + ';')
print('[ccode] \\int p =', ccode(int_p.doit()) + ';')

#u =  sin(5.0*pi*x) * cos(3.0*pi*y)

#dudx = diff(u,x)
#dvdy = dudx
#int_dvdy = integrate(-dvdy,y)
#v = int_dvdy
#print(v)

#exy = 0.5 * ( diff(u,y) + diff(v,x) )
#print('[ccode] e_xy =', ccode(exy))
#div = 0.5 * ( diff(u,x) + diff(v,y) )
#print('[ccode] div =', ccode(div))

