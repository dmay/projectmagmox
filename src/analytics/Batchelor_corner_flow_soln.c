
#include <stdio.h>
#include <math.h>

void evaluate_BatchelorCornerFlowSoln(double alpha,double origin[],double coor[],double vel[])
{
  double s = sin(alpha);
  double c = cos(alpha);
  double theta = atan2(-(coor[1]-origin[1]),(coor[0]-origin[0]));
  double deno = alpha*alpha - s*s;
  double A = alpha * s / deno;
  double B = 0.0;
  double C = (alpha*c - s)/deno;
  double D = (-alpha*s)/deno;
  double F = A * sin(theta) + B*cos(theta) + C*theta * sin(theta) + D*theta*cos(theta);
  double Fp = (A+D)*cos(theta) + (C-B)*sin(theta) + C*theta*cos(theta) -D*theta*sin(theta);
  double vr = Fp;
  double vtheta = - F;
  double vx_wedge = (vr * cos(theta)-vtheta*sin(theta));
  double vy_wedge = (-vr*sin(theta)-vtheta*cos(theta));
  
  vel[0] = vx_wedge;
  vel[1] = vy_wedge;
}
