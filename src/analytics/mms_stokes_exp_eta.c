
#include <petsc.h>

/* 

 Output of sympy script
 mms_stokes_exp_eta.py

 [ccode] uv[0] = sin(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
 [ccode] uv[1] = -1.66666666666667*sin(3.0*M_PI*x[1])*cos(5.0*M_PI*x[0]);
 [ccode] p[0] = sin(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]) + 0.235294117647059*cos(0.4*M_PI)/pow(M_PI, 2) + 0.235294117647059/pow(M_PI, 2) - 0.352941176470588*sin(1.4*M_PI)/pow(M_PI, 2);
 [ccode] eta[0] = 1.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00014;
 [ccode] Fu[0] = 17.0*pow(M_PI, 2)*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*sin(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - 0.0011*M_PI*exp(11.0*x[0])*sin(x[1])*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - 5.33333333333333e-5*M_PI*exp(11.0*x[0])*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1])*cos(x[1]) - M_PI*sin(1.5*M_PI*x[0])*sin(M_PI*x[0] + 3.4*M_PI*x[1]) + 1.5*M_PI*cos(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]);
 [ccode] Fu[1] = -28.3333333333333*pow(M_PI, 2)*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*sin(3.0*M_PI*x[1])*cos(5.0*M_PI*x[0]) - 0.000586666666666667*M_PI*exp(11.0*x[0])*sin(x[1])*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]) + 0.0001*M_PI*exp(11.0*x[0])*cos(x[1])*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - 3.4*M_PI*sin(1.5*M_PI*x[0])*sin(M_PI*x[0] + 3.4*M_PI*x[1]);
 [ccode] Fp[0] = 0;
 [ccode] grad[0] = 5.0*M_PI*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
 [ccode] grad[1] = -3.0*M_PI*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
 [ccode] grad[2] = 8.33333333333333*M_PI*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
 [ccode] grad[3] = -5.0*M_PI*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
 [ccode] sigma[0] = 5.0*M_PI*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - sin(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]) + 0.352941176470588*sin(1.4*M_PI)/pow(M_PI, 2) - 0.235294117647059/pow(M_PI, 2) - 0.235294117647059*cos(0.4*M_PI)/pow(M_PI, 2);
 [ccode] sigma[1] = 2.66666666666667*M_PI*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
 [ccode] sigma[2] = sigma[1];
 [ccode] sigma[3] = -5.0*M_PI*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - sin(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]) + 0.352941176470588*sin(1.4*M_PI)/pow(M_PI, 2) - 0.235294117647059/pow(M_PI, 2) - 0.235294117647059*cos(0.4*M_PI)/pow(M_PI, 2);
 [ccode] \int p = 0;

 
*/


#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateV"
PetscErrorCode StokesSolution_EvaluateV(PetscReal x[],PetscReal uv[],void *ctx)
{
  uv[0] = sin(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  uv[1] = -1.66666666666667*sin(3.0*M_PI*x[1])*cos(5.0*M_PI*x[0]);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateP"
PetscErrorCode StokesSolution_EvaluateP(PetscReal x[],PetscReal p[],void *ctx)
{
  p[0] = sin(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]) + 0.235294117647059*cos(0.4*M_PI)/pow(M_PI, 2) + 0.235294117647059/pow(M_PI, 2) - 0.352941176470588*sin(1.4*M_PI)/pow(M_PI, 2);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateViscosity"
PetscErrorCode StokesSolution_EvaluateViscosity(PetscReal x[],PetscReal eta[],void *ctx)
{
  eta[0] = 1.0e-5*exp(11.0*x[0])*PetscSinReal(x[1]) + 0.00014;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateGradientV"
PetscErrorCode StokesSolution_EvaluateGradientV(PetscReal x[],PetscReal grad[],void *ctx)
{
  grad[0] = 5.0*M_PI*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  grad[1] = -3.0*M_PI*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
  grad[2] = 8.33333333333333*M_PI*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
  grad[3] = -5.0*M_PI*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateFu"
PetscErrorCode StokesSolution_EvaluateFu(PetscReal x[],PetscReal Fu[],void *ctx)
{
  Fu[0] = 17.0*pow(M_PI, 2)*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*sin(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - 0.0011*M_PI*exp(11.0*x[0])*sin(x[1])*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - 5.33333333333333e-5*M_PI*exp(11.0*x[0])*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1])*cos(x[1]) - M_PI*sin(1.5*M_PI*x[0])*sin(M_PI*x[0] + 3.4*M_PI*x[1]) + 1.5*M_PI*cos(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]);
  Fu[1] = -28.3333333333333*pow(M_PI, 2)*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*sin(3.0*M_PI*x[1])*cos(5.0*M_PI*x[0]) - 0.000586666666666667*M_PI*exp(11.0*x[0])*sin(x[1])*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]) + 0.0001*M_PI*exp(11.0*x[0])*cos(x[1])*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - 3.4*M_PI*sin(1.5*M_PI*x[0])*sin(M_PI*x[0] + 3.4*M_PI*x[1]);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateFp"
PetscErrorCode StokesSolution_EvaluateFp(PetscReal x[],PetscReal Fp[],void *ctx)
{
  Fp[0] = 0.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesSolution_EvaluateStress"
PetscErrorCode StokesSolution_EvaluateStress(PetscReal x[],PetscReal sigma[],void *ctx)
{
  sigma[0] = 5.0*M_PI*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - sin(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]) + 0.352941176470588*sin(1.4*M_PI)/pow(M_PI, 2) - 0.235294117647059/pow(M_PI, 2) - 0.235294117647059*cos(0.4*M_PI)/pow(M_PI, 2);
  sigma[1] = 2.66666666666667*M_PI*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*sin(5.0*M_PI*x[0])*sin(3.0*M_PI*x[1]);
  sigma[2] = sigma[1];
  sigma[3] = -5.0*M_PI*(2.0e-5*exp(11.0*x[0])*sin(x[1]) + 0.00028)*cos(5.0*M_PI*x[0])*cos(3.0*M_PI*x[1]) - sin(1.5*M_PI*x[0])*cos(M_PI*x[0] + 3.4*M_PI*x[1]) + 0.352941176470588*sin(1.4*M_PI)/pow(M_PI, 2) - 0.235294117647059/pow(M_PI, 2) - 0.235294117647059*cos(0.4*M_PI)/pow(M_PI, 2);
  
  PetscFunctionReturn(0);
}


