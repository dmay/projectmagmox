
#include <math.h>
#include <petsc.h>

/*
 
 Evaluates analytic fields defined in the following paper
 
 "Simple 2-D models for melt extraction at mid-ocean ridges and island arcs"
 Marc Spiegelman and Dan McKenzie,
 EPSL 83 (1987) 137-152
 
 Note to the user.
 The coordinate system used in these solutions is
  -----> x
  |
  |
  z

 We assume that all input coor[] are defined with coor[1] being -z.
 Recommonded you define you model domain as follows
 
 y = 0    ---------
         |
         |
         |
 y = -D  |_________

 z will be assumed to be given by abs(coor[1])
 
*/

/*
 V = \nabla x (\psi^s \hat{j})
   = ( (1/r) d/dtheta[psi_s] , -d/dr[psi_s] )
 \psi^s_R = r ( A sin(theta) - B theta cos(theta) )
 
 See equation (22a)
*/
#undef __FUNCT__
#define __FUNCT__ "Spiegelman87Ridge_EvaluateSolidVelocity"
PetscErrorCode Spiegelman87Ridge_EvaluateSolidVelocity(PetscReal coor[],PetscReal _alpha,PetscReal _w0,PetscReal _U0,PetscReal vel[])
{
  double x,z,theta,vr,vt,R[2][2];
  double alpha,U0;
  double fac,A,B;

  x = (double)coor[0];
  z = (double)PetscAbsReal(coor[1]);
  theta = atan2(z,x) - 0.5*M_PI;
  theta = fabs(theta);
  
  alpha = (double)_alpha;
  U0    = (double)_U0;
  
  fac = M_PI - 2.0 * alpha - sin(2.0 * alpha);
  A = 2.0 * sin(alpha) * sin(alpha) / fac;
  B = 2.0 / fac;
  
  vr = A*cos(theta) + B*theta*sin(theta) - B*cos(theta) ;
  vt = -A*sin(theta) + B*theta*cos(theta) ;
  
  R[0][0] = sin(theta);  R[0][1] = cos(theta);
  R[1][0] = -cos(theta); R[1][1] = sin(theta);
  
  vel[0] = (PetscReal)(R[0][0]*vr + R[0][1]*vt);
  vel[1] = (PetscReal)(R[1][0]*vr + R[1][1]*vt);
  
  if (theta > M_PI*0.5 - alpha) {
    vel[0] = U0;
    vel[1] = 0.0;
  }
  if (x < 0.0) {
    vel[0] = -vel[0];
  }
  
  PetscFunctionReturn(0);
}

/*
 v = \nabla x (\psi^f \hat{j})
 \psi^f_R = (-w_0/U_0) ( 2 B / r + r ) sin(theta) + \phi^s_R

 See equation (24)
*/
#undef __FUNCT__
#define __FUNCT__ "Spiegelman87Ridge_EvaluateFluidVelocity"
PetscErrorCode Spiegelman87Ridge_EvaluateFluidVelocity(PetscReal coor[],PetscReal _alpha,PetscReal _w0,PetscReal _U0,PetscReal vel[])
{
  double x,z,r,theta,vr,vt,R[2][2];
  double alpha,w0,U0;
  double fac,A,B;
  
  x = (double)coor[0];
  z = (double)PetscAbsReal(coor[1]);
  r = sqrt(x*x + z*z);
  theta = atan2(z,x) - 0.5*M_PI;
  theta = fabs(theta);

  alpha = (double)_alpha;
  w0    = (double)_w0;
  U0    = (double)_U0;
  fac = M_PI - 2.0 * alpha - sin(2.0 * alpha);
  A = 2.0 * sin(alpha) * sin(alpha) / fac;
  B = 2.0 / fac;
  
  vr = (r*(A*cos(theta) + B*theta*sin(theta) - B*cos(theta)) - w0*(2*B/r + r)*cos(theta)/U0)/r ;
  vt = -A*sin(theta) + B*theta*cos(theta) + w0*(-2*B/pow(r, 2) + 1)*sin(theta)/U0 ;
  
  R[0][0] = sin(theta);  R[0][1] = cos(theta);
  R[1][0] = -cos(theta); R[1][1] = sin(theta);
  
  vel[0] = (PetscReal)(R[0][0]*vr + R[0][1]*vt);
  vel[1] = (PetscReal)(R[1][0]*vr + R[1][1]*vt);
  
  if (theta > M_PI*0.5 - alpha) {
    vel[0] = 0.0;
    vel[1] = 0.0;
  }
  if (x < 0.0) {
    vel[0] = -vel[0];
  }
  PetscFunctionReturn(0);
}

/*
 See equation (30a)
*/
#undef __FUNCT__
#define __FUNCT__ "Spiegelman87Trench_EvaluateSolidVelocity"
PetscErrorCode Spiegelman87Trench_EvaluateSolidVelocity(PetscReal coor[],PetscReal _beta,PetscReal _w0,PetscReal _U0,PetscReal vel[])
{
  double x,z,theta,vr,vt,R[2][2];
  double abs_x;
  double beta,U0;
  double fac,C,D;
  
  x = (double)coor[0];
  z = (double)PetscAbsReal(coor[1]);
  abs_x = fabs(x);
  
  theta = atan2(z,abs_x);
  beta = (double)_beta;
  U0   = (double)_U0;

  fac = beta * beta - sin(beta)*sin(beta);
  C = beta * sin(beta) / fac;
  D = (beta * cos(beta) - sin(beta)) / fac;
  
  vr = C*theta*sin(theta) + D*theta*cos(theta) + D*sin(theta) ;
  vt = -C*(-theta*cos(theta) + sin(theta)) - D*theta*sin(theta) ;
  
  R[0][0] = cos(-theta);  R[0][1] = sin(-theta);
  R[1][0] = -sin(-theta); R[1][1] = cos(-theta);
  
  vel[0] = -(PetscReal)(R[0][0]*vr + R[0][1]*vt);
  vel[1] = -(PetscReal)(R[1][0]*vr + R[1][1]*vt);

  if (theta > beta) {
    vel[0] = -U0*cos(beta);
    vel[1] = -U0*sin(beta);
  }
  if (x > 0) {
    vel[0] = -vel[0];
  }
  
  PetscFunctionReturn(0);
}

/*
 See equation (32)
*/
#undef __FUNCT__
#define __FUNCT__ "Spiegelman87Trench_EvaluateFluidVelocity"
PetscErrorCode Spiegelman87Trench_EvaluateFluidVelocity(PetscReal coor[],PetscReal _beta,PetscReal _w0,PetscReal _U0,PetscReal vel[])
{
  double x,z,r,theta,vr,vt,R[2][2];
  double abs_x;
  double beta,w0,U0;
  double fac,C,D;
  
  x = (double)coor[0];
  z = (double)PetscAbsReal(coor[1]);
  r = sqrt(x*x + z*z);
  abs_x = fabs(x);
  
  theta = atan2(z,abs_x);
  beta = (double)_beta;
  w0   = (double)_w0;
  U0   = (double)_U0;
  
  fac = beta * beta - sin(beta)*sin(beta);
  C = beta * sin(beta) / fac;
  D = (beta * cos(beta) - sin(beta)) / fac;

  vr = (r*(C*theta*sin(theta) + D*theta*cos(theta) + D*sin(theta)) - w0*(r*sin(theta) + 2*(C*cos(theta) - D*sin(theta))/r)/U0)/r ;
  vt = -C*(-theta*cos(theta) + sin(theta)) - D*theta*sin(theta) + w0*(-cos(theta) - 2*(C*sin(theta) + D*cos(theta))/pow(r, 2))/U0 ;

  R[0][0] = cos(-theta);  R[0][1] = sin(-theta);
  R[1][0] = -sin(-theta); R[1][1] = cos(-theta);
  
  vel[0] = -(PetscReal)(R[0][0]*vr + R[0][1]*vt);
  vel[1] = -(PetscReal)(R[1][0]*vr + R[1][1]*vt);
  
  if (theta > beta) {
    vel[0] = 0.0;
    vel[1] = 0.0;
  }
  if (x > 0) {
    vel[0] = -vel[0];
  }
  

  PetscFunctionReturn(0);
}
