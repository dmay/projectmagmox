

#include <petsc.h>
#include <petscdmtet.h>
#include <quadrature.h>
#include <fe_geometry_utils.h>
#include <element_container.h>
#include <inorms.h>


#define MAX_FIELDS 30

#undef __FUNCT__
#define __FUNCT__ "ComputeINorms"
PetscErrorCode ComputeINorms(PetscReal E[],DM dm,Vec x,
                            PetscErrorCode (*eval_solution)(PetscReal*,PetscReal*,void*),
                            PetscErrorCode (*eval_solution_gradient)(PetscReal*,PetscReal*,void*),void *ctx)
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni,**GNixi,**GNieta,**dNdx,**dNdy;
  PetscReal *coords_g,*elcoords,detJ_affine,*elfield;
  PetscInt *element,*element_g,nen,npe,border,d,b;
  PetscInt e,i,q,dim,dof,bs;
  const PetscScalar *LA_x;
  Quadrature quadrature;
  EContainer ele_container,ele_container_g;
  Vec xl;
  PetscReal local[3],global[3];
  
  
  PetscFunctionBegin;
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  if (bs > MAX_FIELDS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for number of fields (block size) <= 30");
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  if (dof != bs) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Vector not compatable with DMTet");
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,border+4);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&ele_container);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dm,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  nbasis = ele_container->nbasis;
  Ni     = ele_container->N;
  GNixi  = ele_container->dNr1;
  GNieta = ele_container->dNr2;
  dNdx   = ele_container->dNx1;
  dNdy   = ele_container->dNx2;
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  ierr = PetscMalloc1(nbasis*bs,&elfield);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,0,0,0);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(xl,&LA_x);CHKERRQ(ierr);
  
  E[Q_VOL] = 0.0; /* vol */
  E[Q_L2] = 0.0; /* L2 */
  E[Q_H1] = 0.0; /* H1 */
  E[Q_H1SEMI] = 0.0; /* H1 semi norm */
  
  for (e=0; e<nen; e++) {
    PetscInt *idx,*idx_g;
    PetscReal vol_cell;
    
    /* get basis dofs */
    idx   = &element[nbasis*e];
    idx_g = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx_g[i];
      
      for (d=0; d<dim; d++) {
        elcoords[dim*i+d] = coords_g[dim*nidx+d];
      }
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      for (b=0; b<bs; b++) {
        elfield[bs*i+b] = LA_x[bs*nidx+b];
      }
    }
    
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis,elcoords,GNixi,GNieta,dNdx,dNdy,&detJ_affine);
    
    vol_cell = 0.0;
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[3];
      PetscReal field_exact[MAX_FIELDS],field_approx[MAX_FIELDS];
      PetscReal grad_field_exact[MAX_FIELDS*3],grad_field_approx[MAX_FIELDS*3];
      PetscReal integrand_L2,integrand_H1s,dJ_q;
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = x_qp[2] = 0.0;
      for (i=0; i<ele_container_g->nbasis; i++) {
        for (d=0; d<dim; d++) {
          x_qp[d] += ele_container_g->N[q][i] * elcoords[dim*i+d];
        }
      }
      
      /* evaluate solution */
      if (eval_solution) {
        ierr = eval_solution(x_qp,field_exact,ctx);CHKERRQ(ierr);

        /* interpolate field to each quadrature point */
        for (b=0; b<bs; b++) {
          field_approx[b] = 0.0;
          for (i=0; i<nbasis; i++) {
            field_approx[b] += Ni[q][i] * elfield[bs*i+b];
          }
        }
      } else {
        for (b=0; b<bs; b++) {
          field_exact[b] = 0.0;
          field_approx[b] = 0.0;
        }
      }
      
      /* 
       Evaluate derivatives of solution
       grad(U) = \partial U_i / \partial x_j
         nrows = dof [i ==> b]
         ncols = dim [j ==> d]
       idx = nrows*i + j
       */
      if (eval_solution_gradient) {
        ierr = eval_solution_gradient(x_qp,grad_field_exact,ctx);CHKERRQ(ierr);
        
        /* interpolate gradient of field to each quadrature point */
        for (b=0; b<bs; b++) {
          PetscReal *dN[3];
          
          dN[0] = dNdx[q];
          dN[1] = dNdy[q];
          dN[2] = NULL;
          
          for (d=0; d<dim; d++) {
            grad_field_approx[bs*b+d] = 0.0;
            for (i=0; i<nbasis; i++) {
              grad_field_approx[bs*b+d] += dN[d][i] * elfield[bs*i+b];
            }
          }
        }
        
      } else {
        for (b=0; b<bs; b++) {
          for (d=0; d<dim; d++) {
            PetscInt ii = bs*b+d;
            grad_field_exact[ii] = 0.0;
            grad_field_approx[ii] = 0.0;
          }
        }
      }
      
      dJ_q = PetscAbsReal(detJ_affine);
      
      vol_cell += w[q] * 1.0 * dJ_q;
      
      E[Q_VOL] += w[q] * 1.0 * dJ_q;
      
      integrand_L2 = 0.0;
      for (b=0; b<bs; b++) {
        integrand_L2 += (field_exact[b] - field_approx[b])*(field_exact[b] - field_approx[b]);
      }
      E[Q_L2] += w[q] * integrand_L2 * dJ_q;
      
      integrand_H1s = 0.0;
      for (b=0; b<bs; b++) {
        for (d=0; d<dim; d++) {
          PetscInt ii = bs*b+d;
          
          integrand_H1s += (grad_field_exact[ii] - grad_field_approx[ii])*(grad_field_exact[ii] - grad_field_approx[ii]);
        }
      }
      E[Q_H1]     += w[q] * (integrand_L2 + integrand_H1s) * dJ_q;
      E[Q_H1SEMI] += w[q] * (integrand_H1s) * dJ_q;
    }
    
  }

  /* tidy up */
  ierr = VecRestoreArrayRead(xl,&LA_x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);

  local[0] = E[Q_L2];
  local[1] = E[Q_H1];
  local[2] = E[Q_H1SEMI];
  ierr = MPI_Allreduce(local,global,3,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  E[Q_L2]     = global[0];
  E[Q_H1]     = global[1];
  E[Q_H1SEMI] = global[2];
  
  E[Q_L2]     = PetscSqrtReal(E[Q_L2]);
  E[Q_H1]     = PetscSqrtReal(E[Q_H1]);
  E[Q_H1SEMI] = PetscSqrtReal(E[Q_H1SEMI]);
  
  ierr = PetscFree(elfield);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "IntegrateField"
PetscErrorCode IntegrateField(PetscReal E[],DM dm,Vec x)
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni;
  PetscReal *coords_g,*elcoords,detJ_affine,*elfield;
  PetscInt *element,*element_g,nen,npe,border,d,b;
  PetscInt e,i,q,dim,dof,bs;
  const PetscScalar *LA_x;
  Quadrature quadrature;
  EContainer ele_container,ele_container_g;
  Vec xl;
  PetscReal local[MAX_FIELDS],global[MAX_FIELDS];
  
  PetscFunctionBegin;
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  if (bs > MAX_FIELDS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for number of fields (block size) <= 30");
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  if (dof != bs) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Vector not compatable with DMTet");
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,border+4);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&ele_container);CHKERRQ(ierr);
  ierr = EContainerGeometryCreate(dm,quadrature,&ele_container_g);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  nbasis = ele_container->nbasis;
  Ni     = ele_container->N;
  
  elcoords = ele_container_g->buf_basis_2vector_a;
  ierr = PetscMalloc1(nbasis*bs,&elfield);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,0,0,0);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(xl,&LA_x);CHKERRQ(ierr);
  
  for (b=0; b<bs; b++) {
    E[b] = 0.0;
  }
  
  for (e=0; e<nen; e++) {
    PetscInt *idx,*idx_g;
    
    /* get basis dofs */
    idx   = &element[nbasis*e];
    idx_g = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx_g[i];
      
      for (d=0; d<dim; d++) {
        elcoords[dim*i+d] = coords_g[dim*nidx+d];
      }
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      for (b=0; b<bs; b++) {
        elfield[bs*i+b] = LA_x[bs*nidx+b];
      }
    }
    
    EvaluateBasisGeometry_Affine(elcoords,&detJ_affine);
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[3];
      PetscReal field_approx[MAX_FIELDS];
      PetscReal dJ_q;
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = x_qp[2] = 0.0;
      for (i=0; i<ele_container_g->nbasis; i++) {
        for (d=0; d<dim; d++) {
          x_qp[d] += ele_container_g->N[q][i] * elcoords[dim*i+d];
        }
      }
      
      /* evaluate solution */
      /* interpolate field to each quadrature point */
      for (b=0; b<bs; b++) {
        field_approx[b] = 0.0;
        for (i=0; i<nbasis; i++) {
          field_approx[b] += Ni[q][i] * elfield[bs*i+b];
        }
      }
      
      dJ_q = PetscAbsReal(detJ_affine);
      
      for (b=0; b<bs; b++) {
        E[b] += w[q] * field_approx[b] * dJ_q;
      }
    }
    
  }
  
  /* tidy up */
  ierr = VecRestoreArrayRead(xl,&LA_x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  
  for (b=0; b<bs; b++) {
    local[b] = E[b];
  }
  ierr = MPI_Allreduce(local,global,bs,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  for (b=0; b<bs; b++) {
    E[b] = global[b];
  }
  
  ierr = PetscFree(elfield);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container_g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 E = [L2]
*/
#undef __FUNCT__
#define __FUNCT__ "IntegrateFieldL2"
PetscErrorCode IntegrateFieldL2(PetscReal E[],DM dm,Vec x)
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni;
  PetscReal *coords_g,*elcoords,detJ_affine,*elfield;
  PetscInt *element,*element_g,nen,npe,border,d,b;
  PetscInt e,i,q,dim,dof,bs;
  const PetscScalar *LA_x;
  Quadrature quadrature;
  EContainer ele_container;
  Vec xl;
  PetscReal local,global;
  
  
  PetscFunctionBegin;
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  if (bs > MAX_FIELDS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for number of fields (block size) <= 30");
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  if (dof != bs) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Vector not compatable with DMTet");
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,border+4);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&ele_container);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  nbasis = ele_container->nbasis;
  Ni     = ele_container->N;
  
  elcoords = ele_container->buf_basis_2vector_a;
  ierr = PetscMalloc1(nbasis*bs,&elfield);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,0,0,0);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(xl,&LA_x);CHKERRQ(ierr);
  
  E[0] = 0.0;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx,*idx_g;
    
    /* get basis dofs */
    idx = &element[nbasis*e];
    idx_g = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx_g[i];
      
      for (d=0; d<dim; d++) {
        elcoords[dim*i+d] = coords_g[dim*nidx+d];
      }
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      for (b=0; b<bs; b++) {
        elfield[bs*i+b] = LA_x[bs*nidx+b];
      }
    }
    
    EvaluateBasisGeometry_Affine(elcoords,&detJ_affine);
    
    for (q=0; q<nqp; q++) {
      PetscReal field_approx[MAX_FIELDS];
      PetscReal dJ_q;
      
      /* evaluate solution */
      /* interpolate field to each quadrature point */
      for (b=0; b<bs; b++) {
        field_approx[b] = 0.0;
        for (i=0; i<nbasis; i++) {
          field_approx[b] += Ni[q][i] * elfield[bs*i+b];
        }
      }
      
      dJ_q = PetscAbsReal(detJ_affine);
      
      for (b=0; b<bs; b++) {
        E[0] += w[q] * (field_approx[b]*field_approx[b]) * dJ_q;
      }
    }
    
  }

  /* tidy up */
  ierr = VecRestoreArrayRead(xl,&LA_x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  
  local = E[0];
  ierr = MPI_Allreduce(&local,&global,1,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  E[0] = global;
  E[0] = PetscSqrtReal(E[0]);
  
  ierr = PetscFree(elfield);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 E = [L2,H1_semi,H1]
*/
#undef __FUNCT__
#define __FUNCT__ "IntegrateFieldH1"
PetscErrorCode IntegrateFieldH1(PetscReal E[],DM dm,Vec x)
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni,**GNixi,**GNieta,**dNdx,**dNdy;
  PetscReal *coords_g,*elcoords,detJ_affine,*elfield;
  PetscInt *element,*element_g,nen,npe,border,d,b;
  PetscInt e,i,q,dim,dof,bs;
  const PetscScalar *LA_x;
  Quadrature quadrature;
  EContainer ele_container;
  Vec xl;
  PetscReal local[3],global[3];
  
  PetscFunctionBegin;
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  if (bs > MAX_FIELDS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for number of fields (block size) <= 30");
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  if (dof != bs) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Vector not compatable with DMTet");
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,border+4);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&ele_container);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  nbasis = ele_container->nbasis;
  Ni     = ele_container->N;
  GNixi  = ele_container->dNr1;
  GNieta = ele_container->dNr2;
  dNdx   = ele_container->dNx1;
  dNdy   = ele_container->dNx2;
  
  elcoords = ele_container->buf_basis_2vector_a;
  ierr = PetscMalloc1(nbasis*bs,&elfield);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,0,0,0);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);

  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(xl,&LA_x);CHKERRQ(ierr);
  
  for (b=0; b<3; b++) {
    E[b] = 0.0;
  }
  
  for (e=0; e<nen; e++) {
    PetscInt *idx,*idx_g;
    
    /* get basis dofs */
    idx   = &element[nbasis*e];
    idx_g = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx_g[i];
      
      for (d=0; d<dim; d++) {
        elcoords[dim*i+d] = coords_g[dim*nidx+d];
      }
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      for (b=0; b<bs; b++) {
        elfield[bs*i+b] = LA_x[bs*nidx+b];
      }
    }
    
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis,elcoords,GNixi,GNieta,dNdx,dNdy,&detJ_affine);
    
    for (q=0; q<nqp; q++) {
      PetscReal field_approx;
      PetscReal field_grad[4]; // du_i/dx_j
      PetscReal dJ_q;
      PetscReal L2,H1s,H1;
      
      /* interpolate field to each quadrature point */
      field_approx = 0.0;
      for (i=0; i<nbasis; i++) {
        for (b=0; b<bs; b++) {
          field_approx += Ni[q][i] * elfield[bs*i+b];
        }
      }
      for (b=0; b<4; b++) {
        field_grad[b] = 0.0;
      }

      for (b=0; b<bs; b++) {
        for (i=0; i<nbasis; i++) {
          field_grad[bs*b+0] += dNdx[q][i] * elfield[bs*i+b];
          field_grad[bs*b+1] += dNdy[q][i] * elfield[bs*i+b];
        }
      }
      
      dJ_q = PetscAbsReal(detJ_affine);
      
      L2 = field_approx * field_approx;
      H1s = 0.0;
      for (b=0; b<bs*2; b++) {
        H1s += field_grad[b] * field_grad[b];
      }
      H1 = L2 + H1s;

      E[0] += w[q] * (L2)  * dJ_q;
      E[1] += w[q] * (H1s) * dJ_q;
      E[2] += w[q] * (H1)  * dJ_q;
    }
    
  }

  /* tidy up */
  ierr = VecRestoreArrayRead(xl,&LA_x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  
  for (b=0; b<3; b++) {
    local[b] = E[b];
  }
  ierr = MPI_Allreduce(local,global,3,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  for (b=0; b<3; b++) {
    E[b] = global[b];
  }
  for (b=0; b<3; b++) {
    E[b] = PetscSqrtReal(E[b]);
  }
  
  ierr = PetscFree(elfield);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 E = [L2,H1_semi,H1,Hdiv,Hdiv_Linf]
 */
#undef __FUNCT__
#define __FUNCT__ "IntegrateFieldHdiv"
PetscErrorCode IntegrateFieldHdiv(PetscReal E[],DM dm,Vec x)
{
  PetscErrorCode ierr;
  PetscInt nqp,nbasis;
  PetscReal *xi,*w,**Ni,**GNixi,**GNieta,**dNdx,**dNdy;
  PetscReal *coords_g,*elcoords,detJ_affine,*elfield;
  PetscInt *element,*element_g,nen,npe,border,d,b;
  PetscInt e,i,q,dim,dof,bs;
  const PetscScalar *LA_x;
  Quadrature quadrature;
  EContainer ele_container;
  PetscReal Hdiv_Linf = PETSC_MIN_REAL;
  Vec xl;
  PetscReal local[5],global[5];
  
  
  PetscFunctionBegin;
  ierr = VecGetBlockSize(x,&bs);CHKERRQ(ierr);
  if (bs > MAX_FIELDS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for number of fields (block size) <= 30");
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  if (dof != bs) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Vector not compatable with DMTet");
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,border+4);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&ele_container);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  nbasis = ele_container->nbasis;
  Ni     = ele_container->N;
  GNixi  = ele_container->dNr1;
  GNieta = ele_container->dNr2;
  dNdx   = ele_container->dNx1;
  dNdy   = ele_container->dNx2;
  
  elcoords = ele_container->buf_basis_2vector_a;
  ierr = PetscMalloc1(nbasis*bs,&elfield);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,0,0,0);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,x,INSERT_VALUES,xl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(xl,&LA_x);CHKERRQ(ierr);
  
  for (b=0; b<5; b++) {
    E[b] = 0.0;
  }
  
  for (e=0; e<nen; e++) {
    PetscInt *idx,*idx_g;
    
    /* get basis dofs */
    idx   = &element[nbasis*e];
    idx_g = &element_g[npe*e];
    
    /* get element coordinates */
    for (i=0; i<npe; i++) {
      PetscInt nidx = idx_g[i];
      
      for (d=0; d<dim; d++) {
        elcoords[dim*i+d] = coords_g[dim*nidx+d];
      }
    }
    
    /* get element solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      for (b=0; b<bs; b++) {
        elfield[bs*i+b] = LA_x[bs*nidx+b];
      }
    }
    
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis,elcoords,GNixi,GNieta,dNdx,dNdy,&detJ_affine);
    
    for (q=0; q<nqp; q++) {
      PetscReal field_approx;
      PetscReal field_grad[4]; // du_i/dx_j
      PetscReal dJ_q;
      PetscReal L2,H1s,H1,div,Hdiv;
      
      /* interpolate field to each quadrature point */
      field_approx = 0.0;
      for (i=0; i<nbasis; i++) {
        for (b=0; b<bs; b++) {
          field_approx += Ni[q][i] * elfield[bs*i+b];
        }
      }
      for (b=0; b<4; b++) {
        field_grad[b] = 0.0;
      }
      
      for (b=0; b<bs; b++) {
        for (i=0; i<nbasis; i++) {
          field_grad[bs*b+0] += dNdx[q][i] * elfield[bs*i+b];
          field_grad[bs*b+1] += dNdy[q][i] * elfield[bs*i+b];
        }
      }
      
      dJ_q = PetscAbsReal(detJ_affine);
      
      L2 = field_approx * field_approx;
      H1s = 0.0;
      for (b=0; b<bs*2; b++) {
        H1s += field_grad[b] * field_grad[b];
      }
      H1 = L2 + H1s;

      div = 0.0;
      for (b=0; b<2; b++) {
        div += field_grad[bs*b+b];
      }
      Hdiv = div * div;

      Hdiv_Linf = PetscMax(Hdiv_Linf,PetscAbsReal(div));

      E[0] += w[q] * (L2)  * dJ_q;
      E[1] += w[q] * (H1s) * dJ_q;
      E[2] += w[q] * (H1)  * dJ_q;
      E[3] += w[q] * (Hdiv)  * dJ_q;
    }
    
  }
  
  /* tidy up */
  ierr = VecRestoreArrayRead(xl,&LA_x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  
  for (b=0; b<5; b++) {
    local[b] = E[b];
  }
  ierr = MPI_Allreduce(local,global,4,MPIU_REAL,MPIU_SUM,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&local[4],&global[4],1,MPIU_REAL,MPIU_MAX,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  for (b=0; b<5; b++) {
    E[b] = global[b];
  }
  
  for (b=0; b<4; b++) {
    E[b] = PetscSqrtReal(E[b]);
  }
  
  ierr = PetscFree(elfield);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
