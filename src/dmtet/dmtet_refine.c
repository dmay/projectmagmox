
#include <khash.h>
#include <petsc.h>
#include <dmtetimpl.h>    /*I   "petscdmtet.h"   I*/
#include <dmtet_common.h>
#include <petscdmtet.h>


/* interpolate stencil_xi[] to x,y */
/* insert stencil_nvert into table */

/* allocate geometry */

/* interpolate stencil_xi[] to x,y */
/* generate key and fetch from table */
/* assign element -> node values */

#define TOL PETSC_MACHINE_EPSILON

KHASH_MAP_INIT_INT64(64, long int)

#undef __FUNCT__
#define __FUNCT__ "_DMTetRefineGeometry"
PetscErrorCode _DMTetRefineGeometry(DMTetMesh *gcoarse,
                                    PetscInt stencil_nvert,
                                    PetscReal stencil_xi[],
                                    PetscInt stencil_ncells,
                                    PetscInt stencil_map[],
                                    DMTetMesh **_grefine)
{
  PetscErrorCode ierr;
  PetscInt e,i,j,order;
  PetscReal Ni[3];
  PetscReal elcoor[2*3];
  PetscReal *xn;
  PetscReal min[2],max[2],mindx,cellmindx,celldx[2];
  long int mx,my,key;
  khash_t(64) *h;
  int ret;
  khiter_t iterator;
  long int unique;
  DMTetMesh *grefine;
  PetscInt cell_count,elocal,*label;
  
  ierr = PetscMalloc1(stencil_nvert*2,&xn);CHKERRQ(ierr);
  ierr = PetscMalloc1(stencil_nvert,&label);CHKERRQ(ierr);
  
  /* generate worst case bounding box for each element */
  order = 1;
  celldx[0] = celldx[1] = PETSC_MAX_REAL;
  mindx = PETSC_MAX_REAL;
  for (e=0; e<gcoarse->nelements; e++) {
    PetscReal cx[2],cy[2];
    
    cx[0] = cy[0] = PETSC_MAX_REAL;
    cx[1] = cy[1] = PETSC_MIN_REAL;
    
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = gcoarse->element[3*e+i];
      
      cx[0] = PetscMin(cx[0],gcoarse->coords[2*idx+0]);
      cy[0] = PetscMin(cy[0],gcoarse->coords[2*idx+1]);
      
      cx[1] = PetscMax(cx[1],gcoarse->coords[2*idx+0]);
      cy[1] = PetscMax(cy[1],gcoarse->coords[2*idx+1]);
    }
    celldx[0] = PetscMin(celldx[0],PetscAbsReal(cx[1]-cx[0]));
    celldx[1] = PetscMin(celldx[1],PetscAbsReal(cy[1]-cy[0]));
  }
  cellmindx = celldx[0];
  cellmindx = PetscMin(cellmindx,celldx[1]);
  
  mindx = cellmindx;
  mindx = 1.0e-1 * mindx / (PetscReal)(order+3);
  
  /* get domain bounding box */
  min[0] = min[1] = PETSC_MAX_REAL;
  max[0] = max[1] = PETSC_MIN_REAL;
  for (i=0; i<gcoarse->nnodes; i++) {
    min[0] = PetscMin(min[0],gcoarse->coords[2*i+0]);
    min[1] = PetscMin(min[1],gcoarse->coords[2*i+1]);
    
    max[0] = PetscMax(max[0],gcoarse->coords[2*i+0]);
    max[1] = PetscMax(max[1],gcoarse->coords[2*i+1]);
  }
  
  mx = (max[0] - min[0])/mindx;
  my = (max[1] - min[1])/mindx;
  
  /* === Table creation and initialisation === */
  h = kh_init(64);
  
  /* insert key into slot 1 */
  iterator = kh_put(64, h, -1, &ret);
  unique = 0;
  
  /* populate table */
  for (e=0; e<gcoarse->nelements; e++) {
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = gcoarse->element[3*e+i];
      elcoor[2*i+0] = gcoarse->coords[2*idx+0];
      elcoor[2*i+1] = gcoarse->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<stencil_nvert; i++) {
      Ni[0] = 1.0 - stencil_xi[2*i+0] - stencil_xi[2*i+1];
      Ni[1] = stencil_xi[2*i+0];
      Ni[2] = stencil_xi[2*i+1];
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    /* generate label */
    for (i=0; i<stencil_nvert; i++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = xn[2*i+0];
      yp = xn[2*i+1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      
      iterator = kh_get(64, h, key);
      if (kh_exist(h, iterator)) {
        //printf("... key %ld exists... don't insert \n",key);
      } else {
        /* set value */
        iterator = kh_put(64, h, key, &ret);
        (kh_value(h, iterator)) = unique;
        
        unique++;
      }
    }
  }

  ierr = _DMTetMeshCreate(&grefine);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(gcoarse->nelements * stencil_ncells,3,grefine);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2((PetscInt)unique,3,2,grefine);CHKERRQ(ierr);

  /* === Table query === */
  cell_count = 0;
  for (e=0; e<gcoarse->nelements; e++) {
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = gcoarse->element[3*e+i];
      elcoor[2*i+0] = gcoarse->coords[2*idx+0];
      elcoor[2*i+1] = gcoarse->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<stencil_nvert; i++) {
      Ni[0] = 1.0 - stencil_xi[2*i+0] - stencil_xi[2*i+1];
      Ni[1] = stencil_xi[2*i+0];
      Ni[2] = stencil_xi[2*i+1];
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    /* generate label */
    for (i=0; i<stencil_nvert; i++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = xn[2*i+0];
      yp = xn[2*i+1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      iterator = kh_get(64, h, key);
      if (!kh_exist(h, iterator)) {
        printf("... key %ld doesn't exist ERROR \n",key);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key generation has failed");
      } else {
        int nodeidx = kh_value(h, iterator);
        label[i] = nodeidx;
      }
    }
    
    /* assign coordinates */
    for (i=0; i<stencil_nvert; i++) {
      PetscInt index = label[i];

      grefine->coords[2*index + 0] = xn[2*i+0];
      grefine->coords[2*index + 1] = xn[2*i+1];
    }
    
    /* assign labels */
    for (elocal=0; elocal<stencil_ncells; elocal++) {
      for (i=0; i<3; i++) {
        PetscInt index = stencil_map[3*elocal + i];
        
        grefine->element[3 * cell_count + i] = label[index];
      }
      cell_count++;
    }
    
  }

  
  *_grefine = grefine;

  ierr = PetscFree(label);CHKERRQ(ierr);
  ierr = PetscFree(xn);CHKERRQ(ierr);
  kh_destroy(64, h);
  

  if (gcoarse->element_tag) {
    for (e=0; e<gcoarse->nelements; e++) {
      PetscInt cell_label = gcoarse->element_tag[e];
      for (elocal=0; elocal<stencil_ncells; elocal++) {
        grefine->element_tag[stencil_ncells*e + elocal] = cell_label;
      }
    }
  }

  PetscFunctionReturn(0);
}

/*

 Reference layout is over [0,1]x[0,1]
 Reference layout for refinement is
 
 2
 |  \
 5  -  4
 |  \  |  \
 0  -  3  -  1
 
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetRefineGeometry_Subdivide"
PetscErrorCode DMTetRefineGeometry_Subdivide(DMTetMesh *gcoarse,DMTetMesh **grefine)
{
  const PetscInt stencil_nvert = 6;
  const PetscInt stencil_ncells = 4;
  PetscInt index;
  PetscReal stencil_xi[2*6];
  PetscInt stencil_map[4*3];
  PetscErrorCode ierr;
  
  index = 0;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 0.0;
  index = 1;  stencil_xi[2*index+0] = 1.0;  stencil_xi[2*index+1] = 0.0;
  index = 2;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 1.0;

  index = 3;  stencil_xi[2*index+0] = 0.5;  stencil_xi[2*index+1] = 0.0;
  index = 4;  stencil_xi[2*index+0] = 0.5;  stencil_xi[2*index+1] = 0.5;
  index = 5;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 0.5;

  stencil_map[ 0] = 0;
  stencil_map[ 1] = 3;
  stencil_map[ 2] = 5;

  stencil_map[ 3] = 1;
  stencil_map[ 4] = 4;
  stencil_map[ 5] = 3;

  stencil_map[ 6] = 2;
  stencil_map[ 7] = 5;
  stencil_map[ 8] = 4;

  stencil_map[ 9] = 3;
  stencil_map[10] = 4;
  stencil_map[11] = 5;

  ierr = _DMTetRefineGeometry(gcoarse,stencil_nvert,stencil_xi,stencil_ncells,stencil_map,grefine);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 
 Reference layout is over [0,1]x[0,1]
 Reference layout for refinement is
 
 2
 | \
 | . \
 |  .  \
 |   .   \
 |    3    \
 |  .   .    \
 | .        .  \
 0  -  -  -  -  1
 
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetRefineGeometry_Barycenter"
PetscErrorCode DMTetRefineGeometry_Barycenter(DMTetMesh *gcoarse,DMTetMesh **grefine)
{
  const PetscInt stencil_nvert = 4;
  const PetscInt stencil_ncells = 3;
  PetscInt i,index;
  PetscReal stencil_xi[2*4];
  PetscInt stencil_map[3*3];
  PetscErrorCode ierr;
  
  index = 0;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 0.0;
  index = 1;  stencil_xi[2*index+0] = 1.0;  stencil_xi[2*index+1] = 0.0;
  index = 2;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 1.0;
  
  index = 3;
  stencil_xi[2*index+0] = 0.0;
  stencil_xi[2*index+1] = 0.0;
  for (i=0; i<3; i++) {
    stencil_xi[2*index+0] += stencil_xi[2*i+0];
    stencil_xi[2*index+1] += stencil_xi[2*i+1];
  }
  stencil_xi[2*index+0] /= 3.0;
  stencil_xi[2*index+1] /= 3.0;
  
  stencil_map[0] = 0;
  stencil_map[1] = 1;
  stencil_map[2] = 3;
  
  stencil_map[3] = 1;
  stencil_map[4] = 2;
  stencil_map[5] = 3;
  
  stencil_map[6] = 2;
  stencil_map[7] = 0;
  stencil_map[8] = 3;
  
  ierr = _DMTetRefineGeometry(gcoarse,stencil_nvert,stencil_xi,stencil_ncells,stencil_map,grefine);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 
 Reference layout is over [0,1]x[0,1]
 Reference layout for refinement is
 
 2
 | \
 |   \
(5)   (4)
 | [6]   \
 |         \
 0----(3)----1
 
 The central node [6] is connected to each mid-side node (3,4,5)
 
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetRefineGeometry_PowellSabin"
PetscErrorCode DMTetRefineGeometry_PowellSabin(DMTetMesh *gcoarse,DMTetMesh **grefine)
{
  const PetscInt stencil_nvert = 7;
  const PetscInt stencil_ncells = 6;
  PetscInt i,index;
  PetscReal stencil_xi[2*7];
  PetscInt stencil_map[6*3];
  PetscErrorCode ierr;
  
  index = 0;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 0.0;
  index = 1;  stencil_xi[2*index+0] = 1.0;  stencil_xi[2*index+1] = 0.0;
  index = 2;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 1.0;
  
  index = 3;  stencil_xi[2*index+0] = 0.5;  stencil_xi[2*index+1] = 0.0;
  index = 4;  stencil_xi[2*index+0] = 0.5;  stencil_xi[2*index+1] = 0.5;
  index = 5;  stencil_xi[2*index+0] = 0.0;  stencil_xi[2*index+1] = 0.5;

  index = 6;
  stencil_xi[2*index+0] = 0.0;
  stencil_xi[2*index+1] = 0.0;
  for (i=0; i<3; i++) {
    stencil_xi[2*index+0] += stencil_xi[2*i+0];
    stencil_xi[2*index+1] += stencil_xi[2*i+1];
  }
  stencil_xi[2*index+0] /= 3.0;
  stencil_xi[2*index+1] /= 3.0;

  stencil_map[ 0] = 0;
  stencil_map[ 1] = 3;
  stencil_map[ 2] = 6;
  
  stencil_map[ 3] = 1;
  stencil_map[ 4] = 6;
  stencil_map[ 5] = 3;
  
  stencil_map[ 6] = 4;
  stencil_map[ 7] = 6;
  stencil_map[ 8] = 1;
  
  stencil_map[ 9] = 2;
  stencil_map[10] = 6;
  stencil_map[11] = 4;

  stencil_map[12] = 5;
  stencil_map[13] = 6;
  stencil_map[14] = 2;

  stencil_map[15] = 0;
  stencil_map[16] = 6;
  stencil_map[17] = 5;

  ierr = _DMTetRefineGeometry(gcoarse,stencil_nvert,stencil_xi,stencil_ncells,stencil_map,grefine);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
