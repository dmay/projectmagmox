
#include <khash.h>
#include <petsc.h>
#include <petscmat.h>
#include <petscksp.h>
#include <dmtetimpl.h>
#include <dmtet_common.h>
#include <dmtetdt.h>


#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d_DG"
PetscErrorCode DMTetGenerateBasis2d_DG(DM dm,DM_TET *tet,PetscInt order)
{
  PetscErrorCode ierr;
  PetscInt e,i,bpe,count;
  PetscReal *xilocal;
  
  
  /* fetch layout of local basis */
  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&bpe,&xilocal);CHKERRQ(ierr);
  
  ierr = _DMTetMeshCreate(&tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(tet->geometry->nelements,bpe,tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(tet->geometry->nnodes,3,2,tet->space);CHKERRQ(ierr);

  /* copy coordinates */
  ierr = PetscMemcpy(tet->space->coords,tet->geometry->coords,tet->geometry->nnodes*sizeof(PetscReal)*2);CHKERRQ(ierr);

  /* populate table */
  count = 0;
  for (e=0; e<tet->space->nelements; e++) {
    for (i=0; i<bpe; i++) {
      tet->space->element[bpe*e+i] = count;
      count++;
    }
  }
  
  ierr = PetscFree(xilocal);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d_DG_MPI"
PetscErrorCode DMTetGenerateBasis2d_DG_MPI(DM dm,DM_TET *tet,PetscInt order)
{
  PetscInt e,i,bpe,nel,nbasis_local,basis_psum,basis_offset,count;
  MPI_Comm comm;
  PetscInt *l2g;
  DMTetPartition p;
  PetscReal *xilocal;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  p = tet->partition;
  
  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&bpe,&xilocal);CHKERRQ(ierr);
  
  ierr = _DMTetMeshCreate(&tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1_with_ghost(p->nelement_local,p->nelement_ghost,bpe,tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2_with_ghost(p->nvert_local,p->nvert_ghost,3,2,tet->space);CHKERRQ(ierr);
  
  /* copy coordinates */
  ierr = PetscMemcpy(tet->space->coords,tet->geometry->coords,(tet->geometry->nnodes+tet->geometry->nghost_nodes)*sizeof(PetscReal)*2);CHKERRQ(ierr);

  /* populate table */
  nel = p->nelement_local + p->nelement_ghost;
  count = 0;
  for (e=0; e<nel; e++) {
    for (i=0; i<bpe; i++) {
      tet->space->element[bpe*e+i] = count;
      count++;
    }
  }

  /* populate l2g */
  nel = p->nelement_local;
  nbasis_local = nel * bpe;
  ierr = MPI_Scan(&nbasis_local,&basis_psum,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  basis_offset = basis_psum - nbasis_local;
  
  ierr = PetscMalloc1(nbasis_local,&l2g);CHKERRQ(ierr);
  
  int rank;
  MPI_Comm_rank(comm,&rank);

  //printf("rank %d: basis_offset %d : basis_psum %d : nbasis_local %d  \n",rank,basis_offset,basis_psum,nbasis_local);
  
  count = 0;
  for (e=0; e<nel; e++) {
    for (i=0; i<bpe; i++) {
      l2g[bpe * e + i] = basis_offset + count;
      //printf("rank %d : e %d %d -> %d\n",rank,e,i,l2g[bpe * e + i]);
      count++;
    }
  }

  tet->mpi->nelements_g = nel;
  ierr = MPI_Allreduce(&tet->mpi->nelements_g,&tet->mpi->nelements,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  
  tet->mpi->nnodes   = tet->mpi->nelements * bpe;
  tet->mpi->nnodes_l = tet->mpi->nelements_g * bpe;
  tet->mpi->nnodes_g = tet->mpi->nelements_g * bpe;
  
  tet->mpi->l2g = l2g;
  ierr = PetscFree(xilocal);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetMeshGenerateBasisCoords2d_DG"
PetscErrorCode DMTetMeshGenerateBasisCoords2d_DG(PetscInt order,PetscInt *_npe,PetscReal **_xi)
{
  PetscErrorCode ierr;
  if (order == 0) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"P0 not supported");
  } else {
    ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,_npe,_xi);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasis2d_DG"
PetscErrorCode DMTetTabulateBasis2d_DG(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_Ni)
{
  PetscErrorCode ierr;
  if (order == 0) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"P0 not supported");
  } else {
    ierr = DMTetTabulateBasis2d_CWARPANDBLEND(npoints,xi,order,_nbasis,_Ni);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasisDerivatives2d_DG"
PetscErrorCode DMTetTabulateBasisDerivatives2d_DG(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_GNix,PetscReal ***_GNiy)
{
  PetscErrorCode ierr;
  if (order == 0) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"P0 not supported");
  } else {
    ierr = DMTetTabulateBasisDerivatives2d_CWARPANDBLEND(npoints,xi,order,_nbasis,_GNix,_GNiy);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}
