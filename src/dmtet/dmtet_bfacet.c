
#include <petsc.h>
#include <dmtetimpl.h>
#include <dmtet_topology.h>
#include <petscdmtet.h>

#undef __FUNCT__
#define __FUNCT__ "BFacetListCreate"
PetscErrorCode BFacetListCreate(BFacetList *_f)
{
  BFacetList f;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&f);CHKERRQ(ierr);
  ierr = PetscMemzero(f,sizeof(struct _p_BFacetList));CHKERRQ(ierr);
  f->setup = PETSC_FALSE;
  f->nfacets = 0;
  f->facet_to_element = NULL;
  f->facet_element_face_id = NULL;
  *_f = f;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListDestroy"
PetscErrorCode BFacetListDestroy(BFacetList *_f)
{
  BFacetList f;
  PetscErrorCode ierr;
  
  if (!_f) PetscFunctionReturn(0);
  f = *_f;

  if (f->facet_to_element) { ierr = PetscFree(f->facet_to_element);CHKERRQ(ierr); }
  if (f->facet_element_face_id) { ierr = PetscFree(f->facet_element_face_id);CHKERRQ(ierr); }
  if (f->face_normal) { ierr = PetscFree(f->face_normal);CHKERRQ(ierr); }
  if (f->face_tangent) { ierr = PetscFree(f->face_tangent);CHKERRQ(ierr); }
  if (f->facet0_eid) { ierr = PetscFree(f->facet0_eid);CHKERRQ(ierr); }
  if (f->facet1_eid) { ierr = PetscFree(f->facet1_eid);CHKERRQ(ierr); }
  if (f->facet2_eid) { ierr = PetscFree(f->facet2_eid);CHKERRQ(ierr); }
  ierr = PetscFree(f);CHKERRQ(ierr);
  *_f = NULL;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetBFacetListSetup_FaceIds"
PetscErrorCode _DMTetBFacetListSetup_FaceIds(DM_TET *dmtetctx)
{
  PetscErrorCode ierr;
  DMTetMesh *tet;
  BFacetList f;
  FEGTopology t;
  VLIndex map;
  PetscInt e,bf,bfacet_cnt,vpe;
  
  tet = dmtetctx->geometry;
  
  f = tet->boundary_facets;
  if (f->setup) PetscFunctionReturn(0);
  
  t = tet->topology;
  
  /* check of topological relationship element-to-element exists */
  if (!t->has_valid_e2e) {
    //PetscPrintf(PETSC_COMM_SELF,"DMTetBFacetListSetup: Generating E2E map\n");
    ierr = FEGTopologySetup_E2E(t,dmtetctx);CHKERRQ(ierr);
  } else {
    //PetscPrintf(PETSC_COMM_SELF,"DMTetBFacetListSetup: Using existing E2E map\n");
  }
  
  /* extract data from element-element map */
  map = t->element_to_element;
  
  vpe = tet->npe; /* vertex per element */
  
  /* For 2d, triangular elements with < 3 neighours are by definition on the boundary */
  bfacet_cnt = 0;
  for (e=0; e<map->L; e++) {
    PetscInt nbf_element = 0;
    
    if (map->iL[e] == 3) { continue; } /* no boundary segements on this elements */

    
    if (map->iL[e] == 0) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Element[%D] has zero neighbouring elements - this should never occur, nor is it allowed",e);
 
    switch (map->iL[e]) {
      case 1: /* one neighbour element implies two boundary segements exist on this element */
        nbf_element = 2;
        break;
      case 2: /* two neighbour element implies one boundary segements exist on this element */
        nbf_element = 1;
        break;
    }
    bfacet_cnt = bfacet_cnt + nbf_element;
  }
  
  ierr = PetscMalloc1(bfacet_cnt,&f->facet_to_element);CHKERRQ(ierr);
  ierr = PetscMalloc1(bfacet_cnt,&f->facet_element_face_id);CHKERRQ(ierr);

  ierr = PetscMemzero(f->facet_to_element,sizeof(PetscInt)*bfacet_cnt);CHKERRQ(ierr);
  ierr = PetscMemzero(f->facet_element_face_id,sizeof(PetscInt)*bfacet_cnt);CHKERRQ(ierr);
  
  f->nfacets = bfacet_cnt;
  
  /* assign facet->element */
  bfacet_cnt = 0;
  for (e=0; e<map->L; e++) {
    PetscInt nbf_element = 0;
    
    if (map->iL[e] == 3) { continue; } /* no boundary segements on this elements */
    
    switch (map->iL[e]) {
      case 1: /* one neighbour element implies two boundary segements exist on this element */
        nbf_element = 2;
        break;
      case 2: /* two neighbour element implies one boundary segements exist on this element */
        nbf_element = 1;
        break;
    }
    for (bf=0; bf<nbf_element; bf++) {
      f->facet_to_element[bfacet_cnt] = e;
      bfacet_cnt++;
    }
  }

  /* determine and assign facet->element->face_id */
  bfacet_cnt = 0;
  for (e=0; e<map->L; e++) {
    PetscInt ne;
    PetscInt element,n_neigh_elements,*neigh_element_list;
    PetscInt element_vid[3],*neigh_element_vid;
    PetscBool face_shared[3];
    
    if (map->iL[e] == 3) { continue; } /* no boundary segements on this elements */

    element = e;

    // ask the map which elements are my neighbours //
    n_neigh_elements   = map->iL[element];
    neigh_element_list = map->value[element];
    
    
    for (bf=0; bf<3; bf++) { /* 3 is the number of faces on a triangle */
      face_shared[bf] = PETSC_FALSE;
    }
    
    for (ne=0; ne<n_neigh_elements; ne++) {
      PetscInt v,vn,face_id = -1;
      
      /* reset vert ids for element */
      element_vid[0] = tet->element[vpe*element+0];
      element_vid[1] = tet->element[vpe*element+1];
      element_vid[2] = tet->element[vpe*element+2];

      /* get vertex ids for neighbour element */
      neigh_element_vid = &tet->element[vpe*neigh_element_list[ne]];
      
      /* compare vert ids between neighbour element and this element - matches are marked with -1 */
      for (v=0; v<vpe; v++) {
        for (vn=0; vn<vpe; vn++) {
          if (neigh_element_vid[vn] == element_vid[v]) {
            element_vid[v] = -1;
          }
        }
      }
      
      /* check cases of -1 to determine face index */
      if ( (element_vid[0] == -1) && (element_vid[1] == -1) ) {
        face_id = 0;
      }
      else if ( (element_vid[1] == -1) && (element_vid[2] == -1) ) {
        face_id = 1;
      }
      else if ( (element_vid[0] == -1) && (element_vid[2] == -1) ) {
        face_id = 2;
      }
      else SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"A valid face index labal could not be determined");
      
      face_shared[face_id] = PETSC_TRUE;
    }

    for (bf=0; bf<3; bf++) { /* 3 is the number of faces on a triangle */
      if (!face_shared[bf]) { /* detected non-shared face, add it to my list */
        f->facet_element_face_id[bfacet_cnt] = bf;
        bfacet_cnt++;
      }
    }
    
  }
  
  /* log */
  //PetscPrintf(PETSC_COMM_SELF,"DMTetBFacetListSetup: Found %D boundary facets\n",f->nfacets);
  for (bf=0; bf<f->nfacets; bf++) {
    //PetscPrintf(PETSC_COMM_SELF,"  [facet %D] --> element %D --> face_index %D\n",bf,f->facet_to_element[bf],f->facet_element_face_id[bf]);
  }
  
  {
    PetscInt order;
    PetscInt i,j,nij,fccnt[]={0,0,0};
    
    order = 1; /* dmtetctx->basis_order; */ /* geometry is a always P1 */
    f->nbasis_facet = order + 1;
    
    ierr = PetscMalloc1(order+1,&f->facet0_eid);CHKERRQ(ierr);
    ierr = PetscMalloc1(order+1,&f->facet1_eid);CHKERRQ(ierr);
    ierr = PetscMalloc1(order+1,&f->facet2_eid);CHKERRQ(ierr);
    
    nij = 0;
    for (j=0; j<=order; j++) {
      for (i=0; i<=order; i++) {
        if (i+j > order) { continue; }
        
        if (j==0) {
          f->facet0_eid[fccnt[0]] = nij;
          fccnt[0]++;
        }
        
        if (i==0) {
          f->facet2_eid[fccnt[2]] = nij;
          fccnt[2]++;
        }
        
        if (i==(order-j)) {
          f->facet1_eid[fccnt[1]] = nij;
          fccnt[1]++;
        }
        
        nij++;
      }
    }
    
    // reverse order of f->facet2_eid[]
    for (i=0; i<=order/2; i++) {
      PetscInt tmp = f->facet2_eid[i];
      
      f->facet2_eid[i] = f->facet2_eid[order-i];
      f->facet2_eid[order-i] = tmp;
    }
  }
  
  /* mark facet list as created */
  f->setup = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetBFacetListSetup_FaceIds_FromGeometry"
PetscErrorCode _DMTetBFacetListSetup_FaceIds_FromGeometry(DM_TET *dmtetctx,BFacetList f)
{
  PetscErrorCode ierr;
  DMTetMesh *geometry;
  BFacetList geom_f;
  FEGTopology t;
  
  if (f->setup) PetscFunctionReturn(0);

  geometry = dmtetctx->geometry;
  t = geometry->topology;
  
  geom_f = geometry->boundary_facets;
  
  
  /* check of topological relationship element-to-element exists */
  if (!t->has_valid_e2e) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot build facet list for basis before the geometry facet list is available\n");
  }
  
  f->nfacets = geom_f->nfacets;
  
  ierr = PetscMalloc1(f->nfacets,&f->facet_to_element);CHKERRQ(ierr);
  ierr = PetscMalloc1(f->nfacets,&f->facet_element_face_id);CHKERRQ(ierr);
  
  ierr = PetscMemcpy(f->facet_to_element     ,geom_f->facet_to_element,     sizeof(PetscInt)*f->nfacets);CHKERRQ(ierr);
  ierr = PetscMemcpy(f->facet_element_face_id,geom_f->facet_element_face_id,sizeof(PetscInt)*f->nfacets);CHKERRQ(ierr);
  
  {
    PetscInt order;
    PetscInt i,j,nij,fccnt[]={0,0,0};
    
    order = dmtetctx->basis_order;
    f->nbasis_facet = order + 1;
    
    ierr = PetscMalloc1(order+1,&f->facet0_eid);CHKERRQ(ierr);
    ierr = PetscMalloc1(order+1,&f->facet1_eid);CHKERRQ(ierr);
    ierr = PetscMalloc1(order+1,&f->facet2_eid);CHKERRQ(ierr);
    
    nij = 0;
    for (j=0; j<=order; j++) {
      for (i=0; i<=order; i++) {
        if (i+j > order) { continue; }
        
        if (j==0) {
          f->facet0_eid[fccnt[0]] = nij;
          fccnt[0]++;
        }
        
        if (i==0) {
          f->facet2_eid[fccnt[2]] = nij;
          fccnt[2]++;
        }
        
        if (i==(order-j)) {
          f->facet1_eid[fccnt[1]] = nij;
          fccnt[1]++;
        }
        
        nij++;
      }
    }
    
    // reverse order of f->facet2_eid[]
    for (i=0; i<=order/2; i++) {
      PetscInt tmp = f->facet2_eid[i];
      
      f->facet2_eid[i] = f->facet2_eid[order-i];
      f->facet2_eid[order-i] = tmp;
    }
  }
  
  /* mark facet list as created */
  f->setup = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

//#define TET_BFACET_ORIENTATION_VIEW

/*
 Facet normals and tangents
 We choose n under the following definition
   \vec n \cross \vec t = \vec \hat{k}
 
 tx, ty are computed directly from the vertex coordinates
 
 The outward pointing normal vector is constructed according to
 nx = ty
 ny = -tx
*/
#undef __FUNCT__
#define __FUNCT__ "_DMTetBFacetListSetup_Tangents"
PetscErrorCode _DMTetBFacetListSetup_Tangents(DM_TET *dmtetctx,BFacetList f)
{
  PetscErrorCode ierr;
  DMTetMesh *geometry;
  PetscInt bf;
  
  geometry = dmtetctx->geometry;

  if (!f->face_tangent) {
    ierr = PetscMalloc1(2*f->nfacets,&f->face_tangent);CHKERRQ(ierr);
  }
  ierr = PetscMemzero(f->face_tangent,sizeof(PetscReal)*f->nfacets*2);CHKERRQ(ierr);

#ifdef TET_BFACET_ORIENTATION_VIEW
  printf("# tangent vectors \n");
#endif
  for (bf=0; bf<f->nfacets; bf++) {
    PetscInt *eln;
    PetscInt eidx,fidx;
    PetscReal *c0,*c1,dx,dy,tangent[2],mag;
    
    eidx = f->facet_to_element[bf];
    fidx = f->facet_element_face_id[bf];
    eln = &geometry->element[3*eidx];
    
    switch (fidx) {
      case 0:
        c0 = &geometry->coords[ 2*eln[0] ];
        c1 = &geometry->coords[ 2*eln[1] ];
        break;

      case 1:
        c0 = &geometry->coords[ 2*eln[1] ];
        c1 = &geometry->coords[ 2*eln[2] ];
        break;
      
      case 2:
        c0 = &geometry->coords[ 2*eln[2] ];
        c1 = &geometry->coords[ 2*eln[0] ];
        break;
        
      default:
        c0 = NULL;
        c1 = NULL;
        SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"fidx can only ever be 0,1,2. Found %D",fidx);
        break;
    }
    
    dx = c1[0]-c0[0];
    dy = c1[1]-c0[1];
    mag = PetscSqrtReal( dx*dx + dy*dy  );
    
    tangent[0] = dx/mag;
    tangent[1] = dy/mag;

    f->face_tangent[2*bf+0] = tangent[0];
    f->face_tangent[2*bf+1] = tangent[1];
    
#ifdef TET_BFACET_ORIENTATION_VIEW
    {
      PetscReal centroid[2];
      centroid[0] = 0.5 * (c0[0] + c1[0]);
      centroid[1] = 0.5 * (c0[1] + c1[1]);
      
      printf("%1.4e %1.4e %1.4e %1.4e\n",centroid[0],centroid[1],tangent[0]*0.05,tangent[1]*0.05);
    }
#endif
  }
  
  
  PetscFunctionReturn(0);
}

/* 
 The output of these guys can be plotted in gnuplot via
 plot "normal" using 1:2:3:4 with vectors head filled lt 4,"tangent" using 1:2:3:4 with vectors head filled lt 3
*/
#undef __FUNCT__
#define __FUNCT__ "_DMTetBFacetListSetup_Normals"
PetscErrorCode _DMTetBFacetListSetup_Normals(DM_TET *dmtetctx,BFacetList f)
{
  PetscErrorCode ierr;
  DMTetMesh *geometry;
  PetscInt bf;
  
  geometry = dmtetctx->geometry;

  if (!f->face_normal) {
    ierr = PetscMalloc1(2*f->nfacets,&f->face_normal);CHKERRQ(ierr);
  }
  ierr = PetscMemzero(f->face_normal,sizeof(PetscReal)*f->nfacets*2);CHKERRQ(ierr);
  
#ifdef TET_BFACET_ORIENTATION_VIEW
  printf("# normal vectors \n");
#endif
  for (bf=0; bf<f->nfacets; bf++) {
    PetscInt *eln;
    PetscInt eidx,fidx;
    PetscReal *c0,*c1,dx,dy,tangent[2],normal[2],mag;
    
    eidx = f->facet_to_element[bf];
    fidx = f->facet_element_face_id[bf];
    eln = &geometry->element[3*eidx];
    
    switch (fidx) {
      case 0:
        c0 = &geometry->coords[ 2*eln[0] ];
        c1 = &geometry->coords[ 2*eln[1] ];
        break;
        
      case 1:
        c0 = &geometry->coords[ 2*eln[1] ];
        c1 = &geometry->coords[ 2*eln[2] ];
        break;
        
      case 2:
        c0 = &geometry->coords[ 2*eln[2] ];
        c1 = &geometry->coords[ 2*eln[0] ];
        break;

      default:
        c0 = NULL;
        c1 = NULL;
        SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"fidx can only ever be 0,1,2. Found %D",fidx);
        break;
    }
    
    dx = c1[0]-c0[0];
    dy = c1[1]-c0[1];
    mag = PetscSqrtReal( dx*dx + dy*dy  );
    
    tangent[0] = dx/mag;
    tangent[1] = dy/mag;
    
    /* nx = ty ; ny =  -tx <for outward pointing normals> */
    normal[0] = tangent[1];
    normal[1] = -tangent[0];

    f->face_normal[2*bf+0] = normal[0];
    f->face_normal[2*bf+1] = normal[1];

#ifdef TET_BFACET_ORIENTATION_VIEW
    {
      PetscReal centroid[2];
      
      centroid[0] = 0.5 * (c0[0] + c1[0]);
      centroid[1] = 0.5 * (c0[1] + c1[1]);
      printf("%1.4e %1.4e %1.4e %1.4e\n",centroid[0],centroid[1],normal[0]*0.05,normal[1]*0.05);
    }
#endif
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBFacetSetup"
PetscErrorCode DMTetBFacetSetup(DM dm)
{
  PetscErrorCode ierr;
  DM_TET *tet;
  PetscBool istet = PETSC_FALSE;
  DMTetBasisType btype;
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype == DMTET_DG) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Bug fix required in QuadratureSetUpFromDM_Facet when using DG spaces");

  tet = (DM_TET*)dm->data;

  ierr = _DMTetBFacetListSetup_FaceIds(tet);CHKERRQ(ierr); /* one time setup */
  ierr = _DMTetBFacetListSetup_Tangents(tet,tet->geometry->boundary_facets);CHKERRQ(ierr);
  ierr = _DMTetBFacetListSetup_Normals(tet,tet->geometry->boundary_facets);CHKERRQ(ierr);

  ierr = _DMTetBFacetListSetup_FaceIds_FromGeometry(tet,tet->space->boundary_facets);CHKERRQ(ierr); /* one time setup */
  ierr = _DMTetBFacetListSetup_Tangents(tet,tet->space->boundary_facets);CHKERRQ(ierr);
  ierr = _DMTetBFacetListSetup_Normals(tet,tet->space->boundary_facets);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetRestrictField"
PetscErrorCode BFacetRestrictField(BFacetList f,PetscInt face_id,PetscInt ndof,PetscReal cell_val[],PetscReal face_val[])
{
  PetscInt *cell_face_nid = NULL,k,d;
  
  switch (face_id) {
    case 0:
      cell_face_nid = f->facet0_eid;
      break;
    case 1:
      cell_face_nid = f->facet1_eid;
      break;
    case 2:
      cell_face_nid = f->facet2_eid;
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Arg 2 is a face indentifier tag which must be [0,1,2]. Found face_id  %D",face_id);
      break;
  }

  for (k=0; k<f->nbasis_facet; k++) {
    for (d=0; d<ndof; d++) {
      face_val[ ndof * k + d ] = cell_val[ ndof * cell_face_nid[k] + d];
    }
  }

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetGeometryBFacetList"
PetscErrorCode DMTetGetGeometryBFacetList(DM dm,BFacetList *f)
{
  DM_TET *tet;
  PetscBool istet = PETSC_FALSE;
  PetscErrorCode ierr;
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  tet = (DM_TET*)dm->data;
  if (f) {
    *f = tet->geometry->boundary_facets;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetSpaceBFacetList"
PetscErrorCode DMTetGetSpaceBFacetList(DM dm,BFacetList *f)
{
  DM_TET *tet;
  PetscBool istet = PETSC_FALSE;
  PetscErrorCode ierr;
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  tet = (DM_TET*)dm->data;
  if (f) {
    *f = tet->space->boundary_facets;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListGetOrientations"
PetscErrorCode BFacetListGetOrientations(BFacetList f,PetscReal **normal,PetscReal **tangent)
{
  if (normal) {
    *normal = f->face_normal;
  }
  if (tangent) {
    *tangent = f->face_tangent;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListGetSizes"
PetscErrorCode BFacetListGetSizes(BFacetList f,PetscInt *nfacets,PetscInt *nbasis_facet)
{
  if (nfacets) {
    *nfacets = f->nfacets;
  }
  if (nbasis_facet) {
    *nbasis_facet = f->nbasis_facet;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListGetCellFaceBasisId"
PetscErrorCode BFacetListGetCellFaceBasisId(BFacetList f,PetscInt face_id,PetscInt **cell_face_nid)
{
  switch (face_id) {
    case 0:
      *cell_face_nid = f->facet0_eid;
      break;
    case 1:
      *cell_face_nid = f->facet1_eid;
      break;
    case 2:
      *cell_face_nid = f->facet2_eid;
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Arg 2 is a face indentifier tag which must be [0,1,2]. Found face_id  %D",face_id);
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListGetFacetElementMap"
PetscErrorCode BFacetListGetFacetElementMap(BFacetList f,PetscInt **facet_to_element)
{
  if (facet_to_element) {
    *facet_to_element = f->facet_to_element;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListGetFacetId"
PetscErrorCode BFacetListGetFacetId(BFacetList f,PetscInt **facet_element_face_id)
{
  if (facet_element_face_id) {
    *facet_element_face_id = f->facet_element_face_id;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BFacetListIsSetup"
PetscErrorCode BFacetListIsSetup(BFacetList f,PetscBool *s)
{
  if (s) {
    *s = f->setup;
  }
  PetscFunctionReturn(0);
}
