
#include <khash.h>
#include <petsc.h>
#include <petscmat.h>
#include <petscksp.h>
#include <dmtetimpl.h>
#include <dmtet_common.h>
#include <dmtetdt.h>

#define TOL PETSC_MACHINE_EPSILON

KHASH_MAP_INIT_INT64(64, long int)

#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d_CWARPANDBLEND"
PetscErrorCode DMTetGenerateBasis2d_CWARPANDBLEND(DM dm,DM_TET *tet,PetscInt order)
{
  PetscErrorCode ierr;
  PetscInt e,i,j,npe;
  PetscReal Ni[3];
  PetscReal *xilocal,elcoor[2*3];
  PetscReal *xn;
  PetscReal min[2],max[2],mindx,cellmindx,celldx[2];
  long int mx,my,key;
  
  khash_t(64) *h;
  int ret;
  khiter_t iterator;
  long int unique;
  
  
  /* fetch layout of local basis */
  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&npe,&xilocal);CHKERRQ(ierr);
  ierr = PetscMalloc(sizeof(PetscReal)*npe*2,&xn);CHKERRQ(ierr);
  
  /* generate worst case bounding box for each element */
  celldx[0] = celldx[1] = PETSC_MAX_REAL;
  mindx = PETSC_MAX_REAL;
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscReal cx[2],cy[2];
    
    cx[0] = cy[0] = PETSC_MAX_REAL;
    cx[1] = cy[1] = PETSC_MIN_REAL;
    
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      
      cx[0] = PetscMin(cx[0],tet->geometry->coords[2*idx+0]);
      cy[0] = PetscMin(cy[0],tet->geometry->coords[2*idx+1]);
      
      cx[1] = PetscMax(cx[1],tet->geometry->coords[2*idx+0]);
      cy[1] = PetscMax(cy[1],tet->geometry->coords[2*idx+1]);
    }
    celldx[0] = PetscMin(celldx[0],PetscAbsReal(cx[1]-cx[0]));
    celldx[1] = PetscMin(celldx[1],PetscAbsReal(cy[1]-cy[0]));
  }
  cellmindx = celldx[0];
  cellmindx = PetscMin(cellmindx,celldx[1]);
  
  mindx = cellmindx;
  mindx = 1.0e-1 * mindx / (PetscReal)(order+3);
  
  /* get domain bounding box */
  min[0] = min[1] = PETSC_MAX_REAL;
  max[0] = max[1] = PETSC_MIN_REAL;
  for (i=0; i<tet->geometry->nnodes; i++) {
    min[0] = PetscMin(min[0],tet->geometry->coords[2*i+0]);
    min[1] = PetscMin(min[1],tet->geometry->coords[2*i+1]);
    
    max[0] = PetscMax(max[0],tet->geometry->coords[2*i+0]);
    max[1] = PetscMax(max[1],tet->geometry->coords[2*i+1]);
  }
  
  mx = (max[0] - min[0])/mindx;
  my = (max[1] - min[1])/mindx;
  
  h = kh_init(64);
  
  /* insert key into slot 1 */
  iterator = kh_put(64, h, -1, &ret);
  unique = 0;
  
  /* populate table */
  for (e=0; e<tet->geometry->nelements; e++) {
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<npe; i++) {
      Ni[0] = 1.0 - 0.5*(1.0+xilocal[2*i+0]) - 0.5*(1.0+xilocal[2*i+1]);
      Ni[1] = 0.5*(1.0 + xilocal[2*i+0]);
      Ni[2] = 0.5*(1.0 + xilocal[2*i+1]);
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    /* generate label */
    for (i=0; i<npe; i++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = xn[2*i+0];
      yp = xn[2*i+1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      //printf("key %ld \n",key);
      
      iterator = kh_get(64, h, key);
      if (kh_exist(h, iterator)) {
        //printf("... key %ld exists... don't insert \n",key);
      } else {
        /* set value */
        iterator = kh_put(64, h, key, &ret);
        (kh_value(h, iterator)) = unique;
        
        unique++;
      }
    }
  }
  
  ierr = _DMTetMeshCreate(&tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(tet->geometry->nelements,npe,tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2((PetscInt)unique,npe,2,tet->space);CHKERRQ(ierr);
  
  if (order == 1) {
    /* copy geometry */
    
    ierr = PetscMemcpy(tet->space->element,tet->geometry->element,tet->geometry->nelements*npe*sizeof(PetscInt));CHKERRQ(ierr);
    ierr = PetscMemcpy(tet->space->coords,tet->geometry->coords,tet->geometry->nnodes*sizeof(PetscReal)*2);CHKERRQ(ierr);
    
    
    ierr = PetscFree(xilocal);CHKERRQ(ierr);
    ierr = PetscFree(xn);CHKERRQ(ierr);
    kh_destroy(64, h);
    
    PetscFunctionReturn(0);
  }
  
  
  /* populate table */
  for (e=0; e<tet->geometry->nelements; e++) {
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<npe; i++) {
      Ni[0] = 1.0 - 0.5*(1.0+xilocal[2*i+0]) - 0.5*(1.0+xilocal[2*i+1]);
      Ni[1] = 0.5*(1.0 + xilocal[2*i+0]);
      Ni[2] = 0.5*(1.0 + xilocal[2*i+1]);
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    /* generate label */
    for (i=0; i<npe; i++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = xn[2*i+0];
      yp = xn[2*i+1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      iterator = kh_get(64, h, key);
      if (!kh_exist(h, iterator)) {
        printf("... key %ld doesn't exist ERROR \n",key);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key generation has failed");
      } else {
        int nodeidx = kh_value(h, iterator);
        
        tet->space->element[npe*e+i] = nodeidx;
        
        tet->space->coords[2*nodeidx + 0] = xn[2*i+0];
        tet->space->coords[2*nodeidx + 1] = xn[2*i+1];
      }
    }
  }
  
  ierr = PetscFree(xilocal);CHKERRQ(ierr);
  ierr = PetscFree(xn);CHKERRQ(ierr);
  kh_destroy(64, h);
  
  PetscFunctionReturn(0);
}


/*
 Look up from precomputed values
 */

#include "generatedcode/warp_basis_p1.dat"
#include "generatedcode/warp_basis_p2.dat"
#include "generatedcode/warp_basis_p3.dat"
#include "generatedcode/warp_basis_p4.dat"
#include "generatedcode/warp_basis_p5.dat"
#include "generatedcode/warp_basis_p6.dat"
#include "generatedcode/warp_basis_p7.dat"
#include "generatedcode/warp_basis_p8.dat"
#include "generatedcode/warp_basis_p9.dat"
#include "generatedcode/warp_basis_p10.dat"
#include "generatedcode/warp_basis_p11.dat"
#include "generatedcode/warp_basis_p12.dat"
#include "generatedcode/warp_basis_p13.dat"
#include "generatedcode/warp_basis_p14.dat"
#include "generatedcode/warp_basis_p15.dat"

#undef __FUNCT__
#define __FUNCT__ "DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND"
PetscErrorCode DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(PetscInt order,PetscInt *_npe,PetscReal **_xi)
{
  PetscErrorCode ierr;
  PetscInt npe;
  PetscReal *xilocal;
  
  if (order <= 0) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Warp-And-Blend basis only supports orders 1<= p <= 15");
  }
  if (order > 15) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Warp-And-Blend basis only supports orders <= 15");
  }
  
  switch (order) {
    case 1:
      npe = warp_basis_nbasis_P1;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P1,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 2:
      npe = warp_basis_nbasis_P2;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P2,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 3:
      npe = warp_basis_nbasis_P3;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P3,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 4:
      npe = warp_basis_nbasis_P4;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P4,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 5:
      npe = warp_basis_nbasis_P5;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P5,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 6:
      npe = warp_basis_nbasis_P6;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P6,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 7:
      npe = warp_basis_nbasis_P7;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P7,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 8:
      npe = warp_basis_nbasis_P8;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P8,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 9:
      npe = warp_basis_nbasis_P9;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P9,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 10:
      npe = warp_basis_nbasis_P10;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P10,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 11:
      npe = warp_basis_nbasis_P11;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P11,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 12:
      npe = warp_basis_nbasis_P12;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P12,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 13:
      npe = warp_basis_nbasis_P13;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P13,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 14:
      npe = warp_basis_nbasis_P14;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P14,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
      
    case 15:
      npe = warp_basis_nbasis_P15;
      ierr = PetscMalloc(sizeof(PetscReal)*2*npe,&xilocal);CHKERRQ(ierr);
      ierr = PetscMemcpy(xilocal,warp_basis_coords_P15,sizeof(PetscReal)*2*npe);CHKERRQ(ierr);
      break;
  }
  
  *_npe = npe;
  *_xi  = xilocal;
  
  PetscFunctionReturn(0);
}



/*
 
 Use basis functions of the form
 psi_ij(r,s) = c_ij P_i^{0,0}[(2.r + s + 1)/(1 - s)] (0.5*(1-s))^i P_j^{2i+1,0}[s]
 
 This is taken from
 "Spectral element methods on unstructured meshes: which interpolation points?"
 Richard Pasquetti & Francesca Rapetti,
 Numerical Algorithms, 2010,
 DOI 10.1007/s11075-010-9390-0
 
 The expression for n_{ij} was taken from
 "An explicit construction of interpolation nodes on the simplex"
 T. Warburton, 2006,
 56:247--262
 Journal of Engineering Mathematics,
 DOI 10.1007/s10665-006-9086-6
 
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasis2d_CWARPANDBLEND"
PetscErrorCode DMTetTabulateBasis2d_CWARPANDBLEND(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_Ni)
{
  PetscErrorCode ierr;
  PetscReal **Ni,*xilocal,**basis_coeff, *monomials;
  PetscInt i,j,k,p;
  PetscInt nbasis;
  PetscReal xil,etal,Aij;
  Mat A;
  Vec x,y;
  KSP ksp;
  PC pc;
  
  
  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&nbasis,&xilocal);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&monomials);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal*)*npoints,&Ni);CHKERRQ(ierr);
  for (i=0; i<npoints; i++) {
    ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&Ni[i]);CHKERRQ(ierr);
  }
  
  ierr = PetscMalloc(sizeof(PetscReal*)*nbasis,&basis_coeff);CHKERRQ(ierr);
  for (i=0; i<nbasis; i++) {
    ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&basis_coeff[i]);CHKERRQ(ierr);
  }
  
  /* generate all the basis coefficients */
  ierr = MatCreateSeqDense(PETSC_COMM_SELF,nbasis,nbasis,NULL,&A);CHKERRQ(ierr);
  for (k=0; k<nbasis; k++) {
    PetscReal cij,P0_i,fac,P2_j,arg;
    PetscInt nij;
    
    xil  = xilocal[2*k];
    etal = xilocal[2*k+1];
    
    for (i=0; i<=order; i++) {
      for (j=0; j<=order; j++) {
        if (i+j > order) { continue; }
        nij = (i+j)*(i+j+1)/2 + j;
        //printf("i,j - %d %d : nij %d \n",i,j,nij);
        
        cij = PetscSqrtReal( (2.0*i + 1.0) * (i + j + 1.0) * 0.5 );
        fac = pow( 0.5*(1.0 - etal), (double)i);
        arg = (2.0 * xil + etal + 1.0);
        if (PetscAbsReal(1.0 - etal)>1.0e-32) {
          arg = arg / (1.0 - etal);
        }
        
        //printf("xi/eta %1.4e %1.4e \n",xil,etal);
        //printf("cij = %1.4e : fac = %1.4e : arg = %1.4e \n",cij,fac,arg);
        
        ierr = _PetscDTComputeJacobi(0,    0,i,arg,  &P0_i);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobi(2*i+1,0,j,etal, &P2_j);CHKERRQ(ierr);
        
        //printf("P0_i = %1.4e : P2_j = %1.4e \n",P0_i,P2_j);
        Aij = cij * P0_i * fac * P2_j;
        
        
        ierr = MatSetValue(A,k,nij,Aij,INSERT_VALUES);CHKERRQ(ierr);
      }
    }
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  {
    PetscReal cond;
    PetscBool compute_vandermonde_condition = PETSC_FALSE;
    
    PetscOptionsGetBool(NULL,NULL,"-compute_vandermonde_condition",&compute_vandermonde_condition,0);
    if (compute_vandermonde_condition) {
      
      PetscPrintf(PETSC_COMM_WORLD,"Computing condition number of Vandermonde matrix\n");
      ierr = MatComputeConditionNumber(A,&cond);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_WORLD,"cond(V) = %1.6e \n",cond);
    }
  }
  
  ierr = MatCreateVecs(A,&x,&y);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"basis_");CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  for (k=0; k<nbasis; k++) {
    const PetscScalar *LA_x;
    
    ierr = VecZeroEntries(y);CHKERRQ(ierr);
    ierr = VecSetValue(y,k,1.0,INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecAssemblyBegin(y);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(y);CHKERRQ(ierr);
    
    ierr = KSPSolve(ksp,y,x);CHKERRQ(ierr);
    
    ierr = VecGetArrayRead(x,&LA_x);
    for (i=0; i<nbasis; i++) {
      basis_coeff[k][i] = LA_x[i];
    }
    ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  }
  
  /* evaluate basis at each xi[] */
  for (p=0; p<npoints; p++) {
    PetscReal cij,P0_i,fac,P2_j,arg;
    PetscInt nij;
    
    /* generate all monomials for point, p */
    xil  = xi[2*p+0];
    etal = xi[2*p+1];
    
    for (i=0; i<=order; i++) {
      for (j=0; j<=order; j++) {
        if (i+j > order) { continue; }
        nij = (i+j)*(i+j+1)/2 + j;
        
        cij = PetscSqrtReal( (2.0*i + 1.0) * (i + j + 1.0) * 0.5 );
        fac = pow( 0.5*(1.0 - etal), (double)i);
        arg = (2.0 * xil + etal + 1.0);
        if (PetscAbsReal(1.0 - etal)>1.0e-32) {
          arg = arg / (1.0 - etal);
        }
        
        ierr = _PetscDTComputeJacobi(0,    0,i,arg,  &P0_i);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobi(2*i+1,0,j,etal, &P2_j);CHKERRQ(ierr);
        Aij = cij * P0_i * fac * P2_j;
        
        monomials[nij] = Aij;
      }
    }
    
    for (i=0; i<nbasis; i++) {
      Ni[p][i] = 0.0;
      
      for (j=0; j<nbasis; j++) {
        Ni[p][i] += basis_coeff[i][j] * monomials[j];
      }
    }
  }
  
  ierr = PetscFree(monomials);CHKERRQ(ierr);
  for (i=0; i<nbasis; i++) {
    ierr = PetscFree(basis_coeff[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree(basis_coeff);CHKERRQ(ierr);
  ierr = PetscFree(xilocal);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  
  *_nbasis = nbasis;
  *_Ni = Ni;
  
  PetscFunctionReturn(0);
}

/*
 
 Use basis functions of the form
 psi_ij(r,s) = c_{ij} P_i^{0,0}[(2.r + s + 1)/(1 - s)] (0.5*(1-s))^i P_j^{2i+1,0}[s]
 
 Derivatives thus look like
 
 d/dr ( psi_ij ) =
 c_{ij} . (0.5*(1-s))^i . d/d(alpha)(P_i^{0,0}[alpha]) . d(alpha)/dr . P_j^{2i+1,0}[s]
 where
 alpha = (2.r + s + 1)/(1 - s)
 
 d/ds ( psi_ij ) =
 c_{ij} . -(1/2) . i . (0.5*(1-s))^{i-1} . P_i^{0,0}[(2.r + s + 1)/(1 - s)] . P_j^{2i+1,0}[s]
 +  c_{ij} . (0.5*(1-s))^i . d/d(alpha)(P_i^{0,0}[alpha]) . d(alpha)/ds . P_j^{2i+1,0}[s]
 +  c_{ij} . (0.5*(1-s))^i . P_i^{0,0}[alpha] . d/ds(P_j^{2i+1,0}[s])
 
 
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasisDerivatives2d_CWARPANDBLEND"
PetscErrorCode DMTetTabulateBasisDerivatives2d_CWARPANDBLEND(PetscInt npoints,PetscReal xi[],PetscInt order,PetscInt *_nbasis,PetscReal ***_GNix,PetscReal ***_GNiy)
{
  PetscErrorCode ierr;
  PetscReal **GNix,**GNiy,*xilocal,**basis_coeff, *monomials;
  PetscInt i,j,k,p;
  PetscInt nbasis;
  PetscReal r,s,Aij;
  Mat A;
  Vec x,y;
  KSP ksp;
  PC pc;
  
  
  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&nbasis,&xilocal);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&monomials);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal*)*npoints,&GNix);CHKERRQ(ierr);
  ierr = PetscMalloc(sizeof(PetscReal*)*npoints,&GNiy);CHKERRQ(ierr);
  for (i=0; i<npoints; i++) {
    ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&GNix[i]);CHKERRQ(ierr);
    ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&GNiy[i]);CHKERRQ(ierr);
  }
  
  ierr = PetscMalloc(sizeof(PetscReal*)*nbasis,&basis_coeff);CHKERRQ(ierr);
  for (i=0; i<nbasis; i++) {
    ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&basis_coeff[i]);CHKERRQ(ierr);
  }
  
  /* generate all the basis coefficients */
  ierr = MatCreateSeqDense(PETSC_COMM_SELF,nbasis,nbasis,NULL,&A);CHKERRQ(ierr);
  for (k=0; k<nbasis; k++) {
    PetscReal cij,P0_i,fac,P2_j,arg;
    PetscInt nij;
    
    r = xilocal[2*k];
    s = xilocal[2*k+1];
    
    for (i=0; i<=order; i++) {
      for (j=0; j<=order; j++) {
        if (i+j > order) { continue; }
        nij = (i+j)*(i+j+1)/2 + j;
        
        cij = PetscSqrtReal( (2.0*i + 1.0) * (i + j + 1.0) * 0.5 );
        fac = pow( 0.5*(1.0 - s), (double)i);
        arg = (2.0 * r + s + 1.0);
        if (PetscAbsReal(1.0 - s) > 1.0e-32) {
          arg = arg / (1.0 - s);
        }
        
        ierr = _PetscDTComputeJacobi(0,    0,i,arg,&P0_i);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobi(2*i+1,0,j,s,  &P2_j);CHKERRQ(ierr);
        
        Aij = cij * P0_i * fac * P2_j;
        
        
        ierr = MatSetValue(A,k,nij,Aij,INSERT_VALUES);CHKERRQ(ierr);
      }
    }
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  
  ierr = MatCreateVecs(A,&x,&y);CHKERRQ(ierr);
  
  ierr = KSPCreate(PETSC_COMM_SELF,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"basis_");CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  for (k=0; k<nbasis; k++) {
    const PetscScalar *LA_x;
    
    ierr = VecZeroEntries(y);CHKERRQ(ierr);
    ierr = VecSetValue(y,k,1.0,INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecAssemblyBegin(y);CHKERRQ(ierr);
    ierr = VecAssemblyEnd(y);CHKERRQ(ierr);
    
    ierr = KSPSolve(ksp,y,x);CHKERRQ(ierr);
    
    ierr = VecGetArrayRead(x,&LA_x);CHKERRQ(ierr);
    for (i=0; i<nbasis; i++) {
      basis_coeff[k][i] = LA_x[i];
    }
    ierr = VecRestoreArrayRead(x,&LA_x);CHKERRQ(ierr);
  }
  
  /* evaluate basis derivative at each xi[] */
  for (p=0; p<npoints; p++) {
    PetscReal cij,fac,P0_i,dP0_i,P2_j,dP2_j,arg,dadr,dads;
    PetscInt nij;
    
    /* generate all monomials derivatives wtr x for point, p */
    /*
     
     d/dr ( psi_ij ) =
     c_{ij} . (0.5*(1-s))^i . d/d(alpha)(P_i^{0,0}[alpha]) . d(alpha)/dr . P_j^{2i+1,0}[s]
     where
     alpha = (2.r + s + 1)/(1 - s)
     
     d(alpha)/dr = 2/(1-s)
     
     */
    r = xi[2*p+0];
    s = xi[2*p+1];
    
    for (i=0; i<=order; i++) {
      for (j=0; j<=order; j++) {
        if (i+j > order) { continue; }
        nij = (i+j)*(i+j+1)/2 + j;
        //printf("i,j - %d %d : nij %d \n",i,j,nij);
        
        cij = PetscSqrtReal( (2.0*i + 1.0) * (i + j + 1.0) * 0.5 );
        
        fac = pow( 0.5*(1.0 - s), (double)i );
        
        arg = (2.0 * r + s + 1.0);
        if (PetscAbsReal(1.0 - s) > 1.0e-32) {
          arg = arg / (1.0 - s);
        }
        
        dadr = 2.0/(1.0 - s);
        
        
        ierr = _PetscDTComputeJacobiDerivative(0,    0,i,arg,&dP0_i);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobi(          2*i+1,0,j,s,  &P2_j);CHKERRQ(ierr);
        
        //printf("r/s %1.4e %1.4e \n",r,s);
        //printf("cij = %1.4e : fac = %1.4e : arg = %1.4e \n",cij,fac,arg);
        //printf("dP0_i = %1.4e : P2_j = %1.4e \n",dP0_i,P2_j);
        //printf("dadr = %1.4e \n",dadr);
        
        Aij = cij * fac * dP0_i * dadr * P2_j;
        
        monomials[nij] = Aij;
        //printf("\n");
      }
    }
    
    for (i=0; i<nbasis; i++) {
      GNix[p][i] = 0.0;
      
      for (j=0; j<nbasis; j++) {
        GNix[p][i] += basis_coeff[i][j] * monomials[j];
      }
    }
    
    
    /* generate all monomials derivatives wtr y for point, p */
    /*
     
     d/ds ( psi_ij ) =
     c_{ij} . -(1/2) . i . (0.5*(1-s))^{i-1} . P_i^{0,0}[(2.r + s + 1)/(1 - s)] . P_j^{2i+1,0}[s]
     +  c_{ij} . (0.5*(1-s))^i . d/d(alpha)(P_i^{0,0}[alpha]) . d(alpha)/ds . P_j^{2i+1,0}[s]
     +  c_{ij} . (0.5*(1-s))^i . P_i^{0,0}[alpha] . d/ds(P_j^{2i+1,0}[s])
     
     alpha = (2.r + s + 1).(1 - s)^{-1}
     
     d(alpha)/ds = (1-s)^{-1} + (2.r + s + 1).(1-s)^{-2}
     
     */
    r = xi[2*p+0];
    s = xi[2*p+1];
    
    for (i=0; i<=order; i++) {
      for (j=0; j<=order; j++) {
        if (i+j > order) { continue; }
        nij = (i+j)*(i+j+1)/2 + j;
        
        cij = PetscSqrtReal( (2.0*i + 1.0) * (i + j + 1.0) * 0.5 );
        
        fac = pow( 0.5*(1.0 - s), (double)i );
        
        arg = (2.0 * r + s + 1.0);
        if (PetscAbsReal(1.0 - s) > 1.0e-32) {
          arg = arg / (1.0 - s);
        }
        
        dads = pow(1.0 - s,-1.0) + (2.0*r + s + 1.0) * pow(1.0 - s,-2.0);
        
        ierr = _PetscDTComputeJacobi(          0,    0,i,arg,&P0_i);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobi(          2*i+1,0,j,s,  &P2_j);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobiDerivative(0,    0,i,arg,&dP0_i);CHKERRQ(ierr);
        ierr = _PetscDTComputeJacobiDerivative(2*i+1,0,j,s,  &dP2_j);CHKERRQ(ierr);
        
        Aij  = cij * (-0.5) * ((PetscReal)i) * pow(0.5*(1.0-s),i-1.0) * P0_i * P2_j;
        Aij += cij * fac * dP0_i * dads * P2_j;
        Aij += cij * fac * P0_i * dP2_j;
        
        monomials[nij] = Aij;
      }
    }
    
    for (i=0; i<nbasis; i++) {
      GNiy[p][i] = 0.0;
      
      for (j=0; j<nbasis; j++) {
        GNiy[p][i] += basis_coeff[i][j] * monomials[j];
      }
    }
  }
  
  ierr = PetscFree(monomials);CHKERRQ(ierr);
  for (i=0; i<nbasis; i++) {
    ierr = PetscFree(basis_coeff[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree(basis_coeff);CHKERRQ(ierr);
  ierr = PetscFree(xilocal);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&y);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  
  *_nbasis = nbasis;
  *_GNix = GNix;
  *_GNiy = GNiy;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetGeometryFieldView2d_WAB"
PetscErrorCode _DMTetGeometryFieldView2d_WAB(DM dm,const char fieldname[],PetscReal field[],const char name[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  FILE *fp;
  PetscInt i,e;
  
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",tet->geometry->nnodes,tet->geometry->nelements);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                 tet->geometry->element[3*e],tet->geometry->element[3*e+1],tet->geometry->element[3*e+2]);
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<tet->geometry->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",tet->geometry->coords[2*i],tet->geometry->coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" format=\"ascii\"> \n",fieldname);
  for (i=0; i<tet->geometry->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.4e ",field[i]);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewBasisFunctions2d_CWARPANDBLEND"
PetscErrorCode DMTetViewBasisFunctions2d_CWARPANDBLEND(DM dm)
{
  PetscErrorCode ierr;
  DM basisdm;
  PetscReal **Ni,*coords;
  PetscInt nbasis,p,pb,border,nen,bpe,*element,npe,nnodes;
  PetscReal *data;
  
  
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  
  ierr = DMTetCreate(PETSC_COMM_SELF,&basisdm);CHKERRQ(ierr);
  ierr = DMSetDimension(basisdm,2);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(basisdm,DMTET_CWARP_AND_BLEND);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(basisdm,1);CHKERRQ(ierr);
  
  ierr = DMTetCreateGeometryFromTriangle(basisdm,"examples/reference_meshes/reference-triangle/ref_triangle_mone2one");CHKERRQ(ierr);
  
  //ierr = DMTetGeometryView2d_VTK(basisdm,"dm_reference_triangle_geom.vtu");CHKERRQ(ierr);
  
  ierr = DMTetGenerateBasis2d(basisdm);CHKERRQ(ierr);
  
  /* tabulate basis functions at nodes in geometry */
  ierr = DMTetGeometryElement(basisdm,&nen,&bpe,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis2d_CWARPANDBLEND(nnodes,coords,border,&nbasis,&Ni);CHKERRQ(ierr);
  ierr = PetscMalloc(sizeof(PetscReal)*nnodes,&data);CHKERRQ(ierr);
  for (pb=0; pb<nbasis; pb++) {
    char bname[PETSC_MAX_PATH_LEN];
    char fname[PETSC_MAX_PATH_LEN];
    
    for (p=0; p<nnodes; p++) {
      data[p] = Ni[p][pb];
    }
    
    PetscSNPrintf(bname,sizeof(bname),"basis");
    PetscSNPrintf(fname,sizeof(fname),"P%D_basis%D.vtu",border,pb);
    
    ierr = _DMTetGeometryFieldView2d_WAB(basisdm,bname,data,fname);CHKERRQ(ierr);
  }
  
  {
    FILE * fp;
    char fname[PETSC_MAX_PATH_LEN];
    char line[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(fname,sizeof(fname),"BasisP%D.pvd",border);
    fp = fopen(fname,"w");
    
    PetscSNPrintf(line,sizeof(line),"<?xml version=\"1.0\"?>"); fprintf(fp,"%s\n",line);
#ifdef WORDSIZE_BIGENDIAN
    PetscSNPrintf(line,sizeof(line),"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"BigEndian\">"); fprintf(fp,"%s\n",line);
#else
    PetscSNPrintf(line,sizeof(line),"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">"); fprintf(fp,"%s\n",line);
#endif
    PetscSNPrintf(line,sizeof(line),"<Collection>"); fprintf(fp,"%s\n",line);
    for (pb=0; pb<nbasis; pb++) {
      PetscSNPrintf(line,sizeof(line),"  <DataSet timestep=\"%D\" file=\"./P%D_basis%D.vtu\"/>",pb,border,pb);
      fprintf(fp,"%s\n",line);
    }
    PetscSNPrintf(line,sizeof(line),"</Collection>"); fprintf(fp,"%s\n",line);
    PetscSNPrintf(line,sizeof(line),"</VTKFile>"); fprintf(fp,"%s\n",line);
    
    fclose(fp);
  }
  
  ierr = PetscFree(data);CHKERRQ(ierr);
  for (p=0; p<nnodes; p++) {
    ierr = PetscFree(Ni[p]);CHKERRQ(ierr);
  }
  ierr = PetscFree(Ni);CHKERRQ(ierr);
  ierr = DMDestroy(&basisdm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

