
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscksp.h>

#include <dmtetimpl.h>    /*I   "petscdmtet.h"   I*/
#include <dmtet_common.h>


#undef __FUNCT__
#define __FUNCT__ "DMTetCreateLocalVector_MPI"
PetscErrorCode DMTetCreateLocalVector_MPI(DM dm, Vec *x)
{
  PetscErrorCode ierr;
  DM_TET         *tet;
  Vec            y;
  
  
  tet = (DM_TET*)dm->data;
  
  ierr = VecCreate(PETSC_COMM_SELF,&y);CHKERRQ(ierr);
  ierr = VecSetSizes(y,tet->mpi->nnodes_l*tet->dof,tet->mpi->nnodes_l*tet->dof);CHKERRQ(ierr);
  if (tet->dof > 1) { ierr = VecSetBlockSize(y,tet->dof);CHKERRQ(ierr); }
  ierr = VecSetType(y,VECSEQ);CHKERRQ(ierr);
  ierr = VecSetDM(y,dm);CHKERRQ(ierr);
  *x = y;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateGlobalVector_MPI"
PetscErrorCode DMTetCreateGlobalVector_MPI(DM dm, Vec *x)
{
  PetscErrorCode ierr;
  MPI_Comm       comm;
  DM_TET         *tet;
  Vec            y;
  ISLocalToGlobalMapping mapping;
  
  
  tet = (DM_TET*)dm->data;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = VecCreate(comm,&y);CHKERRQ(ierr);
  ierr = VecSetSizes(y,tet->mpi->nnodes_g*tet->dof,tet->mpi->nnodes*tet->dof);CHKERRQ(ierr);
  if (tet->dof > 1) { ierr = VecSetBlockSize(y,tet->dof);CHKERRQ(ierr); }
  ierr = VecSetType(y,VECMPI);CHKERRQ(ierr);
  ierr = VecSetDM(y,dm);CHKERRQ(ierr);

  ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm),tet->dof,tet->mpi->nnodes_l,(const PetscInt*)tet->mpi->l2g,PETSC_COPY_VALUES,&mapping);CHKERRQ(ierr);
  ierr = VecSetLocalToGlobalMapping(y,mapping);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingDestroy(&mapping);CHKERRQ(ierr);
  
  *x = y;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetMPI_GlobalToLocalBegin"
PetscErrorCode DMTetMPI_GlobalToLocalBegin(DM dm,Vec g,InsertMode mode,Vec l)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = VecScatterBegin(tet->scat_l2g,g,l,mode,SCATTER_REVERSE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetMPI_GlobalToLocalEnd"
PetscErrorCode DMTetMPI_GlobalToLocalEnd(DM dm,Vec g,InsertMode mode,Vec l)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = VecScatterEnd(tet->scat_l2g,g,l,mode,SCATTER_REVERSE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetMPI_LocalToGlobalBegin"
PetscErrorCode DMTetMPI_LocalToGlobalBegin(DM dm,Vec l,InsertMode mode,Vec g)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = VecScatterBegin(tet->scat_l2g,l,g,mode,SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetMPI_LocalToGlobalEnd"
PetscErrorCode DMTetMPI_LocalToGlobalEnd(DM dm,Vec l,InsertMode mode,Vec g)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = VecScatterEnd(tet->scat_l2g,l,g,mode,SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
