
#include <petsc.h>
#include <dmtetimpl.h>
#include <dmtet_common.h>
#include <petscdmtet.h>
#include <data_exchanger.h>

#undef __FUNCT__
#define __FUNCT__ "DMCreateInjection_SubTet_SEQ"
PetscErrorCode DMCreateInjection_SubTet_SEQ(DM dmTo,DM dmFrom,Mat *inject)
{
  DM parentdm,subdm;
  IS is;
  PetscInt dof;
  PetscInt k,len,cnt;
  PetscInt *p2sub_from,*p2sub_to;
  PetscInt *sub2p_from,*sub2p_to;
  const PetscInt *isidx;
  IS p2sub_from_is,p2sub_to_is;
  IS sub2p_from_is,sub2p_to_is;
  VecScatter scatter;
  Vec vecS,vecP;
  PetscErrorCode ierr;
  
  /* check DM's are compatable for injection */
  parentdm = NULL;
  ierr = PetscObjectQuery((PetscObject)dmTo,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
  if (parentdm) {
    subdm = dmTo;
    if (parentdm != dmFrom) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmTo was not derived from dmFrom>");
  } else {
    ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    if (parentdm) {
      subdm = dmFrom;
      if (parentdm != dmTo) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmFrom was not derived from dmTo>");
    } else {
      SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other");
    }
  }

  ierr = DMTetGetDof(parentdm,&dof);CHKERRQ(ierr);
  
  ierr = PetscObjectQuery((PetscObject)subdm,"ParentDMTet2SubDMTetIS",(PetscObject*)&is);CHKERRQ(ierr);
  //ierr = ISView(is,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = ISGetSize(is,&len);CHKERRQ(ierr);
  ierr = ISGetIndices(is,&isidx);CHKERRQ(ierr);
  
  cnt = 0;
  for (k=0; k<len; k++) {
    if (isidx[k] != -1) {
      cnt++;
    }
  }

  ierr = PetscMalloc1(len,&p2sub_from);CHKERRQ(ierr);
  ierr = PetscMalloc1(len,&p2sub_to);CHKERRQ(ierr);
  cnt = 0;
  for (k=0; k<len; k++) {
    if (isidx[k] != -1) {
      p2sub_from[cnt] = k;
      p2sub_to[cnt]   = isidx[k];
      cnt++;
    }
  }

  ierr = PetscMalloc1(len,&sub2p_from);CHKERRQ(ierr);
  ierr = PetscMalloc1(len,&sub2p_to);CHKERRQ(ierr);
  cnt = 0;
  for (k=0; k<len; k++) {
    if (isidx[k] != -1) {
      sub2p_from[cnt] = isidx[k];
      sub2p_to[cnt]   = k;
      cnt++;
    }
  }
  ierr = ISRestoreIndices(is,&isidx);CHKERRQ(ierr);
  
  /* create parent -> sub */
  ierr = ISCreateBlock(PetscObjectComm((PetscObject)dmTo),dof,cnt,p2sub_from,PETSC_OWN_POINTER,&p2sub_from_is);CHKERRQ(ierr);
  ierr = ISCreateBlock(PetscObjectComm((PetscObject)dmTo),dof,cnt,p2sub_to,PETSC_OWN_POINTER,&p2sub_to_is);CHKERRQ(ierr);

  /* create sub -> parent */
  ierr = ISCreateBlock(PetscObjectComm((PetscObject)dmTo),dof,cnt,sub2p_from,PETSC_OWN_POINTER,&sub2p_from_is);CHKERRQ(ierr);
  ierr = ISCreateBlock(PetscObjectComm((PetscObject)dmTo),dof,cnt,sub2p_to,PETSC_OWN_POINTER,&sub2p_to_is);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(parentdm,&vecP);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(subdm,&vecS);CHKERRQ(ierr);
  
  if (dmFrom == parentdm) {
    ierr = VecScatterCreate(vecP,p2sub_from_is,vecS,p2sub_to_is,&scatter);CHKERRQ(ierr);
  }
  if (dmFrom == subdm) {
    /*
    printf("*** values from subtet\n");
    ierr = ISView(sub2p_from_is,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    printf("*** values to be inserted into parent vec \n");
    ierr = ISView(sub2p_to_is,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    */
    ierr = VecScatterCreate(vecS,sub2p_from_is,vecP,sub2p_to_is,&scatter);CHKERRQ(ierr);
  }
  ierr = MatCreateScatter(PetscObjectComm((PetscObject)scatter),scatter, inject);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&scatter);CHKERRQ(ierr);
  
  ierr = VecDestroy(&vecS);CHKERRQ(ierr);
  ierr = VecDestroy(&vecP);CHKERRQ(ierr);
  ierr = ISDestroy(&p2sub_from_is);CHKERRQ(ierr);
  ierr = ISDestroy(&p2sub_to_is);CHKERRQ(ierr);
  ierr = ISDestroy(&sub2p_from_is);CHKERRQ(ierr);
  ierr = ISDestroy(&sub2p_to_is);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInjection_SubTet_SEQ_SEQ"
PetscErrorCode DMCreateInjection_SubTet_SEQ_SEQ(DM dmTo,DM dmFrom,IS *isFrom,IS *isTo)
{
  DM parentdm,subdm;
  IS is;
  PetscInt k,len,cnt;
  PetscInt *p2sub_from,*p2sub_to;
  PetscInt *sub2p_from,*sub2p_to;
  const PetscInt *isidx;
  IS p2sub_from_is,p2sub_to_is;
  IS sub2p_from_is,sub2p_to_is;
  PetscErrorCode ierr;
  
  /* check DM's are compatable for injection */
  parentdm = NULL;
  ierr = PetscObjectQuery((PetscObject)dmTo,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
  if (parentdm) {
    subdm = dmTo;
    if (parentdm != dmFrom) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmTo was not derived from dmFrom>");
  } else {
    ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    if (parentdm) {
      subdm = dmFrom;
      if (parentdm != dmTo) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmFrom was not derived from dmTo>");
    } else {
      SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other");
    }
  }
  
  ierr = PetscObjectQuery((PetscObject)subdm,"ParentDMTet2SubDMTetIS",(PetscObject*)&is);CHKERRQ(ierr);
  
  ierr = ISGetSize(is,&len);CHKERRQ(ierr);
  ierr = ISGetIndices(is,&isidx);CHKERRQ(ierr);
  
  cnt = 0;
  for (k=0; k<len; k++) {
    if (isidx[k] != -1) {
      cnt++;
    }
  }
  
  ierr = PetscMalloc1(len,&p2sub_from);CHKERRQ(ierr);
  ierr = PetscMalloc1(len,&p2sub_to);CHKERRQ(ierr);
  cnt = 0;
  for (k=0; k<len; k++) {
    if (isidx[k] != -1) {
      p2sub_from[cnt] = k;
      p2sub_to[cnt]   = isidx[k];
      cnt++;
    }
  }
  
  ierr = PetscMalloc1(len,&sub2p_from);CHKERRQ(ierr);
  ierr = PetscMalloc1(len,&sub2p_to);CHKERRQ(ierr);
  cnt = 0;
  for (k=0; k<len; k++) {
    if (isidx[k] != -1) {
      sub2p_from[cnt] = isidx[k];
      sub2p_to[cnt]   = k;
      cnt++;
    }
  }
  ierr = ISRestoreIndices(is,&isidx);CHKERRQ(ierr);
  
  /* create parent -> sub */
  ierr = ISCreateGeneral(PETSC_COMM_SELF,cnt,p2sub_from,PETSC_OWN_POINTER,&p2sub_from_is);CHKERRQ(ierr);
  ierr = ISCreateGeneral(PETSC_COMM_SELF,cnt,p2sub_to,PETSC_OWN_POINTER,&p2sub_to_is);CHKERRQ(ierr);
  
  /* create sub -> parent */
  ierr = ISCreateGeneral(PETSC_COMM_SELF,cnt,sub2p_from,PETSC_OWN_POINTER,&sub2p_from_is);CHKERRQ(ierr);
  ierr = ISCreateGeneral(PETSC_COMM_SELF,cnt,sub2p_to,PETSC_OWN_POINTER,&sub2p_to_is);CHKERRQ(ierr);
  
  if (dmFrom == parentdm) {
    *isFrom = p2sub_from_is;
    *isTo   = p2sub_to_is;
    ierr = ISDestroy(&sub2p_from_is);CHKERRQ(ierr);
    ierr = ISDestroy(&sub2p_to_is);CHKERRQ(ierr);
  }
  if (dmFrom == subdm) {
    *isFrom = sub2p_from_is;
    *isTo   = sub2p_to_is;
    ierr = ISDestroy(&p2sub_from_is);CHKERRQ(ierr);
    ierr = ISDestroy(&p2sub_to_is);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

typedef struct {
  PetscInt e_l_id,e_g_id,e_gsub_id;
  PetscInt cell_basis_id;
  PetscInt basis_l2g;
  PetscMPIInt from_rank,to_rank;
} BasisCommCtx;

#undef __FUNCT__
#define __FUNCT__ "DMCreateInjection_SubTet_MPI_MPI"
PetscErrorCode DMCreateInjection_SubTet_MPI_MPI(DM parent,DM sub,
                                                PetscInt *_nfrom,PetscInt *_from[],PetscInt *_nto,PetscInt *_to[])
{
  DM_TET *tet_p = (DM_TET*)parent->data;
  DM_TET *tet_s = (DM_TET*)sub->data;
  PetscErrorCode ierr;
  MPI_Comm comm;
  PetscMPIInt commrank;
  IS is;
  PetscInt e,nelements,nbasis_parent,nbasis_sub,nbasis_cell_parent,nbasis_cell_sub,k;
  PetscInt *basis_mark,*element_parent,*element_sub;
  const PetscInt *emark;
  BasisCommCtx *commctx,*commctx_recv;
  
  ierr = PetscObjectGetComm((PetscObject)parent,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  
  is = NULL;
  ierr = PetscObjectQuery((PetscObject)sub,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
  if (!is) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"SubDM on rank %D failed to located element map",(PetscInt)commrank);

  ierr = DMTetSpaceElement(parent,0,&nbasis_cell_parent,&element_parent,0,&nbasis_parent,0);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nbasis_parent,&basis_mark);CHKERRQ(ierr);
  for (k=0; k<nbasis_parent; k++) {
    basis_mark[k] = -1;
  }
  ierr = PetscMalloc1(nbasis_parent,&commctx);CHKERRQ(ierr);
  ierr = PetscMemzero(commctx,nbasis_parent*sizeof(BasisCommCtx));CHKERRQ(ierr);
  for (k=0; k<nbasis_parent; k++) {
    commctx[k].e_l_id = -1;
  }
  
  ierr = ISGetIndices(is,&emark);CHKERRQ(ierr);
  
  nelements = tet_p->geometry->nelements;
  for (e=0; e<nelements; e++) {
    PetscInt eidx_g_parent;
    PetscInt eidx_g_sub,rid;

    eidx_g_parent = tet_p->partition->element_gid_local[e];

    if (emark[eidx_g_parent] == -1) { continue; }
    
    if (eidx_g_parent >= tet_p->pinfo->nelements) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"eidx_g_parent (%D) exceeds num. global elements %D",eidx_g_parent,tet_p->pinfo->nelements);
    
    eidx_g_sub    = emark[eidx_g_parent];
    
    if (eidx_g_sub >= tet_p->pinfo->nelements) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"eidx_g_sub (%D) exceeds num. global elements %D",eidx_g_sub,tet_p->pinfo->nelements);
    
    rid           = tet_s->pinfo->epart[eidx_g_sub];

    if (rid >= tet_p->pinfo->npartitions) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"rid (%D) exceeds num. partitions %D",rid,tet_p->pinfo->npartitions);

    /*
    printf("rank %d: parent gid %d --> sub gid %d on rank %d\n",commrank,eidx_g_parent,eidx_g_sub,rid);
    */
     
    for (k=0; k<nbasis_cell_parent; k++) {
      PetscInt nidx = element_parent[e*nbasis_cell_parent + k];
      
      commctx[nidx].e_l_id = e;
      commctx[nidx].e_g_id = eidx_g_parent;
      commctx[nidx].e_gsub_id = eidx_g_sub;
      commctx[nidx].cell_basis_id = k;
      commctx[nidx].basis_l2g = tet_p->mpi->l2g[nidx];
      commctx[nidx].from_rank = commrank;
      commctx[nidx].to_rank = rid;
      basis_mark[nidx] = nidx;
    }
    
  }
  ierr = ISRestoreIndices(is,&emark);CHKERRQ(ierr);
  
  /*
  if (commrank==0) {
    for (k=0; k<nbasis_parent; k++) {
      if (basis_mark[k] == -1) continue;
      printf("commctx[%d]  e(%d,%d,%d) k(%d) g(%d) r(%d,%d)\n",
             k,commctx[k].e_l_id,commctx[k].e_g_id,commctx[k].e_gsub_id,
             commctx[k].cell_basis_id,commctx[k].basis_l2g,commctx[k].from_rank,commctx[k].to_rank);
    }
  }
  */
  
  DataEx            de;
  PetscInt          recv_length;
  PetscInt *from,*to;
  PetscInt from_to_cnt;
  
  de = DataExCreate(comm,0);

  ierr = DataExTopologyInitialize(de);CHKERRQ(ierr);
  for (k=0; k<nbasis_parent; k++) {
    if (basis_mark[k] != -1) {
      if (commctx[k].to_rank == commrank) continue;
      ierr = DataExTopologyAddNeighbour(de,commctx[k].to_rank);CHKERRQ(ierr);
    }
  }
  ierr = DataExTopologyFinalize(de);CHKERRQ(ierr);
  
  ierr = DataExInitializeSendCount(de);CHKERRQ(ierr);
  for (k=0; k<nbasis_parent; k++) {
    if (basis_mark[k] != -1) {
      if (commctx[k].to_rank == commrank) continue;
      ierr = DataExAddToSendCount( de, commctx[k].to_rank, 1 );CHKERRQ(ierr);
    }
  }
  ierr = DataExFinalizeSendCount(de);CHKERRQ(ierr);
  
  ierr = DataExPackInitialize(de,sizeof(BasisCommCtx));CHKERRQ(ierr);

  for (k=0; k<nbasis_parent; k++) {
    if (basis_mark[k] != -1) {
      if (commctx[k].to_rank == commrank) continue;
      ierr = DataExPackData( de, commctx[k].to_rank, 1,(void*)&commctx[k] );CHKERRQ(ierr);
    }
  }
  ierr = DataExPackFinalize(de);CHKERRQ(ierr);

  ierr = DataExBegin(de);CHKERRQ(ierr);
  ierr = DataExEnd(de);CHKERRQ(ierr);
  
  ierr = DataExGetRecvData( de, &recv_length, (void**)&commctx_recv );CHKERRQ(ierr);

  ierr = PetscFree(basis_mark);CHKERRQ(ierr);

  /*
   printf("rank %d : recv length %d\n",commrank,recv_length);
  */
  
  /* now traverse the sub-dm elements */
  nelements = tet_s->geometry->nelements;
  
  ierr = DMTetSpaceElement(sub,0,&nbasis_cell_sub,&element_sub,0,&nbasis_sub,0);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nbasis_sub,&from);CHKERRQ(ierr); /* 2 x nbasis is simply the worst case */
  ierr = PetscMalloc1(2*nbasis_sub,&to);CHKERRQ(ierr);
  for (k=0; k<2*nbasis_sub; k++) {
    from[k] = -1; /* this will be the "from" array */
    to[k]   = -1; /* this will be the "to" array */
  }

  from_to_cnt = 0;

#if 1
  /* do the parent.rank = sub.rank entries first */
  for (k=0; k<nbasis_parent; k++) {
    if (commctx[k].e_l_id != -1) {
      if (commrank == commctx[k].to_rank) {
        PetscBool found;
        PetscInt this_element = -1, e_gsub_id = commctx[k].e_gsub_id;
        
        // find
        found = PETSC_FALSE;
        for (e=0; e<nelements; e++) {
          if (e_gsub_id == tet_s->partition->element_gid_local[e]) {
            this_element = e;
            found = PETSC_TRUE;
            break;
          }
        }
        if (!found) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Local-to-local map failed");
       
        PetscInt *sub_cell_basis = &element_sub[ nbasis_cell_sub * this_element ];
        PetscInt localid = sub_cell_basis[ commctx[k].cell_basis_id ];
        PetscInt l2g = tet_s->mpi->l2g[localid];
        
        if (from_to_cnt == 2*nbasis_sub) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"fromto_cnt == 2*nbasis_sub");
        from[from_to_cnt] = commctx[k].basis_l2g;
        to[from_to_cnt]   = l2g;
        from_to_cnt++;
      }
    }
  }
  
  for (k=0; k<recv_length; k++) {
    if (commctx_recv[k].e_l_id == -1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Recieved data should never have e_l_id = -1");
    if (commrank != commctx_recv[k].to_rank) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Recieved data should always have commrank = commctx_recv[].to_rank");
      
    {
      PetscBool found;
      PetscInt this_element = -1, e_gsub_id = commctx_recv[k].e_gsub_id;
      
      // find
      found = PETSC_FALSE;
      for (e=0; e<nelements; e++) {
        if (e_gsub_id == tet_s->partition->element_gid_local[e]) {
          this_element = e;
          found = PETSC_TRUE;
          break;
        }
      }
      if (!found) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Local-to-local [MPI] map failed");
      
      PetscInt *sub_cell_basis = &element_sub[ nbasis_cell_sub * this_element ];
      PetscInt localid = sub_cell_basis[ commctx_recv[k].cell_basis_id ];
      PetscInt l2g = tet_s->mpi->l2g[localid];
      
      if (from_to_cnt == 2*nbasis_sub) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"fromto_cnt == 2*nbasis_sub");
      from[from_to_cnt] = commctx_recv[k].basis_l2g;
      to[from_to_cnt]   = l2g;
      from_to_cnt++;
    }
  }
#endif

#if 0
  ///
  /* do the parent.rank = sub.rank entries first */
  for (k=0; k<nbasis_parent; k++) {
    if (commctx[k].e_l_id != -1) {
      if (commrank == commctx[k].to_rank) {
        PetscBool found;
        PetscInt this_element = -1, e_gsub_id = commctx[k].e_gsub_id;
        
        // find
        found = PETSC_FALSE;
        for (e=0; e<nelements; e++) {
          if (e_gsub_id == tet_s->partition->element_gid_local[e]) {
            this_element = e;
            found = PETSC_TRUE;
            break;
          }
        }
        if (!found) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Local-to-local map failed");
        
        PetscInt *sub_cell_basis = &element_sub[ nbasis_cell_sub * this_element ];
        PetscInt localid = sub_cell_basis[ commctx[k].cell_basis_id ];
        PetscInt l2g = tet_s->mpi->l2g[localid];
        
        if (from_to_cnt == 2*nbasis_sub) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"fromto_cnt == 2*nbasis_sub");
        from[localid] = commctx[k].basis_l2g;
        to[localid]   = l2g;
        //from_to_cnt++;
      }
    }
  }
  
  for (k=0; k<recv_length; k++) {
    if (commctx_recv[k].e_l_id == -1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Recieved data should never have e_l_id = -1");
    if (commrank != commctx_recv[k].to_rank) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Recieved data should always have commrank = commctx_recv[].to_rank");
    
    {
      PetscBool found;
      PetscInt this_element = -1, e_gsub_id = commctx_recv[k].e_gsub_id;
      
      // find
      found = PETSC_FALSE;
      for (e=0; e<nelements; e++) {
        if (e_gsub_id == tet_s->partition->element_gid_local[e]) {
          this_element = e;
          found = PETSC_TRUE;
          break;
        }
      }
      if (!found) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Local-to-local [MPI] map failed");
      
      PetscInt *sub_cell_basis = &element_sub[ nbasis_cell_sub * this_element ];
      PetscInt localid = sub_cell_basis[ commctx_recv[k].cell_basis_id ];
      PetscInt l2g = tet_s->mpi->l2g[localid];
      
      if (from_to_cnt == 2*nbasis_sub) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"fromto_cnt == 2*nbasis_sub");
      from[localid] = commctx_recv[k].basis_l2g;
      to[localid]   = l2g;
      //from_to_cnt++;
    }
  }
  
  /* flatten */
  
  PetscInt *tmp_f,*tmp_t;

  ierr = PetscMalloc1(2*nbasis_sub,&tmp_f);CHKERRQ(ierr); /* 2 x nbasis is simply the worst case */
  ierr = PetscMalloc1(2*nbasis_sub,&tmp_t);CHKERRQ(ierr);

  from_to_cnt = 0;
  for (k=0; k<nbasis_sub; k++) {
    if (from[k] != -1) {
      tmp_f[from_to_cnt] = from[k];
      tmp_t[from_to_cnt] = to[k];
      from_to_cnt++;
    }
  }
  PetscFree(from);
  PetscFree(to);
  
  from = tmp_f;
  to   = tmp_t;
  
  ///
#endif
  
  /*
  //if (commrank == 0) {
    for (k=0; k<from_to_cnt; k++) {
      printf("[rank %d] : (%d) : from[%d] --> to[%d]\n",commrank,k,from[k],to[k]);
    }
  //}
  */
  
  /*ierr = MPI_Barrier(comm);CHKERRQ(ierr);*/
  /*
  for (k=0; k<nbasis_sub; k++) {
    if (from[k] == -1) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"[rank %D] from[%D] = -1",(PetscInt)commrank,k);
    if (to[k] == -1) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"[rank %D] to[%D] = -1",(PetscInt)commrank,k);
  }
  */
  /*
  for (k=0; k<from_to_cnt; k++) {
    if (from[k] == -1) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"[rank %D] from[%D] = -1",(PetscInt)commrank,k);
    if (to[k] == -1) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"[rank %D] to[%D] = -1",(PetscInt)commrank,k);
  }
  */
   
  *_nfrom = from_to_cnt;
  *_from = from;
  *_nto = from_to_cnt;
  *_to = to;
  
  ierr = DataExDestroy(de);CHKERRQ(ierr);
  ierr = PetscFree(commctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMCreateInjection_SubTet_General"
PetscErrorCode DMCreateInjection_SubTet_General(DM dmTo,DM dmFrom,Mat *inject)
{
  DM parentdm=NULL,subdm=NULL;
  PetscInt dof_p,dof_s;
  DMTetBasisType btype_p,btype_s;
  PetscInt degree_p,degree_s;
  PetscMPIInt commsize_p,commsize_s;
  PetscErrorCode ierr;
  
  /* check DM's are potentially compatable for injection (match up from and to) */
  parentdm = NULL;
  ierr = PetscObjectQuery((PetscObject)dmTo,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
  if (parentdm) {
    subdm = dmTo;
    if (parentdm != dmFrom) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmTo was not derived from dmFrom>");
  } else {
    ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    if (parentdm) {
      subdm = dmFrom;
      if (parentdm != dmTo) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmFrom was not derived from dmTo>");
    } else {
      SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other");
    }
  }
#if 0
  {
    DM_TET *tet_parentdm = NULL;
    DM_TET *tet_dmTo     = (DM_TET*)dmTo->data;
    DM_TET *tet_dmFrom   = (DM_TET*)dmFrom->data;
    
    parentdm = NULL;
    ierr = PetscObjectQuery((PetscObject)dmTo,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    if (parentdm) {
      tet_parentdm = (DM_TET*)parentdm->data;
      printf("[1] found tet_parentdm %p with space %p\n",tet_parentdm,tet_parentdm->space);
      subdm = dmTo;
      printf("[1] matched? : tet_dmFrom %p with space %p\n",tet_dmFrom,tet_dmFrom->space);
      if (tet_parentdm->space != tet_dmFrom->space) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmTo was not derived from dmFrom>");
    } else {
      ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
      if (parentdm) {
        tet_parentdm = (DM_TET*)parentdm->data;
        printf("[2] found tet_parentdm %p with space %p\n",tet_parentdm,tet_parentdm->space);
        subdm = dmFrom;
        printf("[1] matched? : tet_dmTo %p with space %p\n",tet_dmTo,tet_dmTo->space);
        if (tet_parentdm->space != tet_dmTo->space) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmFrom was not derived from dmTo>");
      } else {
        SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other");
      }
    }
  }
#endif
  
  /* check DM's are potentially compatable for injection (check function spaces match) */
  ierr = DMTetGetDof(parentdm,&dof_p);CHKERRQ(ierr);
  ierr = DMTetGetDof(subdm,&dof_s);CHKERRQ(ierr);
  if (dof_p != dof_s) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DOF is inconsistent between \"from\" and \"to\" DMs");

  ierr = DMTetGetBasisType(parentdm,&btype_p);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(subdm,&btype_s);CHKERRQ(ierr);
  if (btype_p != btype_s) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"Basis type is inconsistent between \"from\" and \"to\" DMs");

  ierr = DMTetGetBasisOrder(parentdm,&degree_p);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(subdm,&degree_s);CHKERRQ(ierr);
  if (btype_p != btype_s) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"Basis degree is inconsistent between \"from\" and \"to\" DMs");

  /* check communicators and select appropriate constructor */
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)parentdm),&commsize_p);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)subdm),&commsize_s);CHKERRQ(ierr);
  if ((commsize_p == 1) && (commsize_s == 1)) {
    ierr = DMCreateInjection_SubTet_SEQ(dmTo,dmFrom,inject);CHKERRQ(ierr);
  } else if ((commsize_p != 1) && (commsize_s != 1)) {
    IS is = NULL;
    
    is = NULL;
    ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
    /*if (is) { printf("  --> found ParentElementMarkerIS_DMTetSubDMTet on the \"from\" DM\n"); }*/

    is = NULL;
    ierr = PetscObjectQuery((PetscObject)subdm,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
    /*if (is) { printf("  --> found ParentElementMarkerIS_DMTetSubDMTet on the \"subdm\" DM\n"); }*/

    is = NULL;
    ierr = PetscObjectQuery((PetscObject)dmTo,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
    /*if (is) { printf("  --> found ParentElementMarkerIS_DMTetSubDMTet on the \"to\" DM\n"); }*/

    PetscInt nf,*from,nt,*to;
    
    ierr = DMCreateInjection_SubTet_MPI_MPI(parentdm,subdm,&nf,&from,&nt,&to);CHKERRQ(ierr);
    
    //if (dof_p != 1) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"MPI-MPI requires small code change to work with dof!=1. Use ISCreateBlock (and write a test!)");

    {
      IS isf,ist;
      Vec vecP,vecS;
      VecScatter scatter;
      
      /* create parent -> sub */
      ierr = ISCreateBlock(PETSC_COMM_SELF,dof_p,nf,from,PETSC_COPY_VALUES,&isf);CHKERRQ(ierr);
      ierr = ISCreateBlock(PETSC_COMM_SELF,dof_p,nt,to,PETSC_COPY_VALUES,&ist);CHKERRQ(ierr);
      
      ierr = DMCreateGlobalVector(parentdm,&vecP);CHKERRQ(ierr);
      ierr = DMCreateGlobalVector(subdm,&vecS);CHKERRQ(ierr);
      
      if (dmFrom == parentdm) {
        ierr = VecScatterCreate(vecP,isf,vecS,ist,&scatter);CHKERRQ(ierr);
      }
      if (dmFrom == subdm) {
        //SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"[MPI -> MPI] sub ==> parent yet not tested");
        ierr = VecScatterCreate(vecS,ist,vecP,isf,&scatter);CHKERRQ(ierr);
      }
      ierr = MatCreateScatter(PetscObjectComm((PetscObject)scatter),scatter, inject);CHKERRQ(ierr);
      ierr = VecScatterDestroy(&scatter);CHKERRQ(ierr);
      ierr = ISDestroy(&isf);CHKERRQ(ierr);
      ierr = ISDestroy(&ist);CHKERRQ(ierr);
      ierr = VecDestroy(&vecS);CHKERRQ(ierr);
      ierr = VecDestroy(&vecP);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(to);CHKERRQ(ierr);
    ierr = PetscFree(from);CHKERRQ(ierr);
    
    /*
    ierr = MPI_Barrier(PetscObjectComm((PetscObject)dmTo));CHKERRQ(ierr); // Barrier just inserted to allow printf to write //
    */
    
    //SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"[MPI -> MPI] Unhandled case for DMTetInjection");
  } else if ((commsize_p != 1) && (commsize_s == 1)) {
    SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"[MPI -> SEQ] Unhandled case for DMTetInjection");
  } else if ((commsize_p == 1) && (commsize_s != 1)) {
    SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"[SEQ -> MPI] Unhandled case for DMTetInjection");
  } else SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"Unhandled case for DMTetInjection");
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetMarkElementsByRegions"
PetscErrorCode DMTetMarkElementsByRegions(DM dm,PetscInt nr,PetscInt rlist[],PetscInt *nsub,PetscBool *mark[])
{
  PetscInt nen,*regions,nesub,e,r;
  PetscBool *element_mark;
  PetscErrorCode ierr;
  
  /* scan geomtry regions and check requested regions exist (count as you go) */
  ierr = DMTetGeometryGetAttributes(dm,&nen,&regions,NULL);CHKERRQ(ierr);
  nesub = 0;
  for (e=0; e<nen; e++) {
    for (r=0; r<nr; r++) {
      if (rlist[r] == regions[e]) {
        nesub++;
      }
    }
  }
  
  /* mark all elements, nodes, dofs you want to keep */
  /* element tags */
  ierr = PetscMalloc1(nen,&element_mark);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    element_mark[e] = PETSC_FALSE;
    for (r=0; r<nr; r++) {
      if (rlist[r] == regions[e]) {
        element_mark[e] = PETSC_TRUE;
      }
    }
  }
  if (nsub) { *nsub = nesub; }
  *mark = element_mark;
  
  PetscFunctionReturn(0);
}

PetscErrorCode _DMTetCreateSubmeshGeometry_DuplicateParent_SEQ(DM dm,PetscInt nesub,PetscBool element_mark[],DM sub);
PetscErrorCode _DMTetCreateSubmeshSpace_DuplicateParent_SEQ(DM dm,PetscInt nesub,PetscBool element_mark[],DM sub);

#undef __FUNCT__
#define __FUNCT__ "DMTetConvertCopyAttachElementMarker"
PetscErrorCode DMTetConvertCopyAttachElementMarker(PetscInt len,PetscBool mark[],DM dm)
{
  IS is;
  PetscInt k,*indices;
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(len+1,&indices);CHKERRQ(ierr);
  for (k=0; k<len+1; k++) {
    indices[k] = -1;
  }
  if (mark) {
    PetscInt count = 0;
    for (k=0; k<len; k++) {
      if (mark[k]) {
        indices[k] = count;
        count++;
      }
    }
  }
  
  ierr = ISCreateGeneral(PETSC_COMM_SELF,len,indices,PETSC_COPY_VALUES,&is);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject)dm,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject)is);CHKERRQ(ierr);
  ierr = ISDestroy(&is);CHKERRQ(ierr);
  ierr = PetscFree(indices);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetDistributeElementMarkerIS"
PetscErrorCode DMTetDistributeElementMarkerIS(DM dmseq,DM dm)
{
  IS is,is_seq = NULL;
  PetscInt k,len,*indices;
  const PetscInt *indices_seq;
  MPI_Comm comm;
  PetscMPIInt commsize,commrank;
  PetscErrorCode ierr;

  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);

  if (dmseq) {
    ierr = PetscObjectQuery((PetscObject)dmseq,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is_seq);CHKERRQ(ierr);
    if (commrank != 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected ParentElementMarkerIS_DMTetSubDMTet to be attached to the dm_sequential defined on rank 0");
  }

  len = 0;
  if (is_seq) {
    ierr = ISGetSize(is_seq,&len);CHKERRQ(ierr);
  }
  ierr = MPI_Bcast(&len,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  
  indices_seq = NULL;
  ierr = PetscMalloc1(len,&indices);CHKERRQ(ierr);
  if (is_seq) {
    ierr = ISGetIndices(is_seq,&indices_seq);CHKERRQ(ierr);
    for (k=0; k<len; k++) {
      indices[k] = indices_seq[k];
    }
    ierr = ISRestoreIndices(is_seq,&indices_seq);CHKERRQ(ierr);
  }
  
  ierr = MPI_Bcast(indices,len,MPIU_INT,0,comm);CHKERRQ(ierr);

  ierr = ISCreateGeneral(PETSC_COMM_SELF,len,indices,PETSC_COPY_VALUES,&is);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject)dm,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject)is);CHKERRQ(ierr);
  ierr = ISDestroy(&is);CHKERRQ(ierr);
  
  ierr = PetscFree(indices);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/*
 This default sub-mesh constructor using the function space defined on the parent on the sub-mesh.
 
 By splitting the creation of the geometry and space we have the ability to:
 1) Control if and when the space field is created
 2) Define different spaces on the sub-mesh
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSubmeshByRegionsDefault_SEQ"
PetscErrorCode DMTetCreateSubmeshByRegionsDefault_SEQ(DM dm,PetscInt nr,PetscInt rlist[],DM *_sub)
{
  PetscInt nsub;
  PetscBool *element_mark;
  DM sub;
  PetscInt dim;
  DM_TET *tet;
  DM_TET *subtet;
  PetscErrorCode ierr;
  
  ierr = DMTetMarkElementsByRegions(dm,nr,rlist,&nsub,&element_mark);CHKERRQ(ierr);
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);

  ierr = DMTetCreate(PetscObjectComm((PetscObject)dm),&sub);CHKERRQ(ierr);
  ierr = DMSetDimension(sub,dim);CHKERRQ(ierr);

  tet = (DM_TET*)dm->data;
  subtet = (DM_TET*)sub->data;

  sub->ops->getinjection = DMCreateInjection_SubTet_General;
  dm->ops->getinjection  = DMCreateInjection_SubTet_General;
  
  /* set objects parent / child -- needed for DMInjection */
  ierr = PetscObjectCompose((PetscObject)sub,"ParentDMTet",(PetscObject)dm);CHKERRQ(ierr);

  ierr = DMTetConvertCopyAttachElementMarker(tet->geometry->nelements,element_mark,sub);CHKERRQ(ierr);
  
  ierr = _DMTetCreateSubmeshGeometry_DuplicateParent_SEQ(dm,nsub,element_mark,sub);CHKERRQ(ierr);

  /* get defaults from dm and set into subdm */
  ierr = DMTetGetDof(dm,&subtet->dof);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(dm,&subtet->basis_type);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&subtet->basis_order);CHKERRQ(ierr);

  ierr = _DMTetCreateSubmeshSpace_DuplicateParent_SEQ(dm,nsub,element_mark,sub);CHKERRQ(ierr);
  
  ierr = DMSetUp(sub);CHKERRQ(ierr);
  
  ierr = PetscFree(element_mark);CHKERRQ(ierr);
  
  *_sub = sub;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSubmesh_SEQ"
PetscErrorCode DMTetCreateSubmesh_SEQ(DM dm,PetscInt nsub,PetscBool element_mark[],
                                       PetscInt dof,DMTetBasisType basistype,PetscInt basisorder,DM *_sub)
{
  DM sub;
  PetscInt dim;
  DM_TET *tet;
  PetscBool space_duplicate;
  PetscErrorCode ierr;
  
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetCreate(PetscObjectComm((PetscObject)dm),&sub);CHKERRQ(ierr);
  ierr = DMSetDimension(sub,dim);CHKERRQ(ierr);
  
  tet = (DM_TET*)dm->data;

  /* injection is not valid in general */
  sub->ops->getinjection = DMCreateInjection_SubTet_General;
  dm->ops->getinjection  = DMCreateInjection_SubTet_General;
  
  /* set objects parent / child -- needed for DMInjection */
  ierr = PetscObjectCompose((PetscObject)sub,"ParentDMTet",(PetscObject)dm);CHKERRQ(ierr);
  
  ierr = DMTetConvertCopyAttachElementMarker(tet->geometry->nelements,element_mark,sub);CHKERRQ(ierr);

  ierr = _DMTetCreateSubmeshGeometry_DuplicateParent_SEQ(dm,nsub,element_mark,sub);CHKERRQ(ierr);
  
  /* get defaults from dm and set into subdm */
  ierr = DMTetSetDof(sub,dof);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(sub,basistype);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(sub,basisorder);CHKERRQ(ierr);

  space_duplicate = PETSC_FALSE;
  if ((basistype == tet->basis_type) && (basisorder == tet->basis_order)) {
    space_duplicate = PETSC_TRUE;
  }
  
  if (!space_duplicate) {
    ierr = DMTetGenerateBasis2d(sub);CHKERRQ(ierr);
  } else {
    ierr = _DMTetCreateSubmeshSpace_DuplicateParent_SEQ(dm,nsub,element_mark,sub);CHKERRQ(ierr);
  }

  ierr = DMSetUp(sub);CHKERRQ(ierr);
  
  *_sub = sub;

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSubmeshGeometry_SEQ"
PetscErrorCode DMTetCreateSubmeshGeometry_SEQ(DM dm,PetscInt nsub,PetscBool element_mark[],DM *_sub)
{
  DM sub;
  PetscInt dim;
  DM_TET *tet;
  PetscErrorCode ierr;
  
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetCreate(PetscObjectComm((PetscObject)dm),&sub);CHKERRQ(ierr);
  ierr = DMSetDimension(sub,dim);CHKERRQ(ierr);
  
  tet = (DM_TET*)dm->data;
  
  /* injection is not valid in general */
  sub->ops->getinjection = DMCreateInjection_SubTet_General;
  dm->ops->getinjection  = DMCreateInjection_SubTet_General;
  
  /* set objects parent / child -- needed for DMInjection */
  ierr = PetscObjectCompose((PetscObject)sub,"ParentDMTet",(PetscObject)dm);CHKERRQ(ierr);
  
  ierr = DMTetConvertCopyAttachElementMarker(tet->geometry->nelements,element_mark,sub);CHKERRQ(ierr);

  ierr = _DMTetCreateSubmeshGeometry_DuplicateParent_SEQ(dm,nsub,element_mark,sub);CHKERRQ(ierr);
  
  *_sub = sub;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetCreateSubmeshGeometry_DuplicateParent_SEQ"
PetscErrorCode _DMTetCreateSubmeshGeometry_DuplicateParent_SEQ(DM dm,PetscInt nesub,PetscBool element_mark[],DM sub)
{
  DM_TET *tet = (DM_TET*)dm->data;
  DM_TET *subtet = (DM_TET*)sub->data;
  PetscInt dim,nen,n,e,k,npe,bpe,*element,gncnt,nnodes;
  PetscInt *gnlist;
  PetscBool *gnode_mark;
  PetscErrorCode ierr;
  
  if (nesub == 0) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input tags do not define any elements to define a sub-mesh");
  
  /* geometry node tags */
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,&nen,&bpe,&element,&npe,&nnodes,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes,&gnode_mark);CHKERRQ(ierr);
  for (n=0; n<nnodes ; n++) {
    gnode_mark[n] = PETSC_FALSE;
  }
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        gnode_mark[idx] = PETSC_TRUE;
      }
    }
  }
  ierr = PetscMalloc1(nnodes,&gnlist);CHKERRQ(ierr);
  gncnt = 0;
  for (n=0; n<nnodes; n++) {
    gnlist[n] = -1;
    if (gnode_mark[n]) {
      gnlist[n] = gncnt;
      gncnt++;
    }
  }
  
  ierr = _DMTetMeshCreate(&subtet->geometry);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(nesub,bpe,subtet->geometry);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(gncnt,npe,dim,subtet->geometry);CHKERRQ(ierr);
  subtet->geometry->nactivedof = gncnt;
  
  /* copy coordinates */
  gncnt = 0;
  for (n=0; n<nnodes; n++) {
    if (gnode_mark[n]) {
      ierr = PetscMemcpy(&subtet->geometry->coords[dim*gncnt],&tet->geometry->coords[dim*n],sizeof(PetscReal)*dim);CHKERRQ(ierr);
      gncnt++;
    }
  }
  
  /* copy elements */
  nesub = 0;
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      
      /* copy region */
      subtet->geometry->element_tag[nesub] = tet->geometry->element_tag[e];
      
      /* copy dof indices */
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        subtet->geometry->element[nesub*bpe + k] = gnlist[idx];
      }
      nesub++;
    }
  }
  ierr = PetscFree(gnode_mark);CHKERRQ(ierr);
  ierr = PetscFree(gnlist);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetCreateSubmeshSpace_DuplicateParent_SEQ"
PetscErrorCode _DMTetCreateSubmeshSpace_DuplicateParent_SEQ(DM dm,PetscInt nesub,PetscBool element_mark[],DM sub)
{
  DM_TET *tet = (DM_TET*)dm->data;
  DM_TET *subtet = (DM_TET*)sub->data;
  PetscInt dim,nen,n,e,k,npe,bpe,*element,sncnt,nnodes;
  PetscInt *snlist;
  PetscBool *snode_mark;
  PetscErrorCode ierr;
  
  if (nesub == 0) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input tags do not define any elements to define a sub-mesh");
  
  /* space node tags */
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,&npe,&nnodes,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes,&snode_mark);CHKERRQ(ierr);
  for (n=0; n<nnodes ; n++) {
    snode_mark[n] = PETSC_FALSE;
  }
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        snode_mark[idx] = PETSC_TRUE;
      }
    }
  }
  ierr = PetscMalloc1(nnodes,&snlist);CHKERRQ(ierr);
  sncnt = 0;
  for (n=0; n<nnodes; n++) {
    snlist[n] = -1;
    if (snode_mark[n]) {
      snlist[n] = sncnt;
      sncnt++;
    }
  }
  
  ierr = _DMTetMeshCreate(&subtet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(nesub,bpe,subtet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(sncnt,npe,dim,subtet->space);CHKERRQ(ierr);
  subtet->space->nactivedof = sncnt;
  
  /* copy coordinates */
  sncnt = 0;
  for (n=0; n<nnodes; n++) {
    if (snode_mark[n]) {
      ierr = PetscMemcpy(&subtet->space->coords[dim*sncnt],&tet->space->coords[dim*n],sizeof(PetscReal)*dim);CHKERRQ(ierr);
      sncnt++;
    }
  }
  
  /* copy elements */
  nesub = 0;
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      
      /* copy region */
      subtet->space->element_tag[nesub] = tet->space->element_tag[e];
      
      /* copy dof indices */
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        subtet->space->element[nesub*bpe + k] = snlist[idx];
      }
      nesub++;
    }
  }
  
  {
    IS is;
    
    ierr = ISCreateGeneral(PetscObjectComm((PetscObject)sub),nnodes,snlist,PETSC_COPY_VALUES,&is);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject)sub,"ParentDMTet2SubDMTetIS",(PetscObject)is);CHKERRQ(ierr);
    ierr = ISDestroy(&is);CHKERRQ(ierr);
  }
  
  ierr = PetscFree(snode_mark);CHKERRQ(ierr);
  ierr = PetscFree(snlist);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSubmeshByRegions_SEQ"
PetscErrorCode DMTetCreateSubmeshByRegions_SEQ(DM dm,PetscInt nr,PetscInt rlist[],DM *_sub)
{
  DM_TET *tet = (DM_TET*)dm->data;
  DM_TET *subtet;
  DM sub;
  PetscInt dim,nen,*regions,nesub,n,e,r,k,npe,bpe,*element,gncnt,sncnt,nnodes;
  PetscInt *gnlist,*snlist;
  PetscBool *element_mark,*gnode_mark,*snode_mark;
  PetscErrorCode ierr;

  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  /* scan geomtry regions and check requested regions exist (count as you go) */
  ierr = DMTetGeometryGetAttributes(dm,&nen,&regions,NULL);CHKERRQ(ierr);
  nesub = 0;
  for (e=0; e<nen; e++) {
    for (r=0; r<nr; r++) {
      if (rlist[r] == regions[e]) {
        nesub++;
      }
    }
  }
  if (nesub == 0) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input mesh does not contain elements with requested region labels");

  ierr = DMTetCreate(PetscObjectComm((PetscObject)dm),&sub);CHKERRQ(ierr);
  subtet = (DM_TET*)sub->data;
  ierr = DMSetDimension(sub,dim);CHKERRQ(ierr);
  ierr = DMTetSetDof(sub,tet->dof);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(sub,tet->basis_type);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(sub,tet->basis_order);CHKERRQ(ierr);

  /* mark all elements, nodes, dofs you want to keep */
  /* element tags */
  ierr = PetscMalloc1(nen,&element_mark);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    element_mark[e] = PETSC_FALSE;
    for (r=0; r<nr; r++) {
      if (rlist[r] == regions[e]) {
        element_mark[e] = PETSC_TRUE;
      }
    }
  }
  /* geometry node tags */
  ierr = DMTetGeometryElement(dm,NULL,&bpe,&element,&npe,&nnodes,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes,&gnode_mark);CHKERRQ(ierr);
  for (n=0; n<nnodes ; n++) {
    gnode_mark[n] = PETSC_FALSE;
  }
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        gnode_mark[idx] = PETSC_TRUE;
      }
    }
  }
  ierr = PetscMalloc1(nnodes,&gnlist);CHKERRQ(ierr);
  gncnt = 0;
  for (n=0; n<nnodes; n++) {
    gnlist[n] = -1;
    if (gnode_mark[n]) {
      gnlist[n] = gncnt;
      gncnt++;
    }
  }

  ierr = _DMTetMeshCreate(&subtet->geometry);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(nesub,bpe,subtet->geometry);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(gncnt,npe,dim,subtet->geometry);CHKERRQ(ierr);
  subtet->geometry->nactivedof = gncnt;

  /* copy coordinates */
  gncnt = 0;
  for (n=0; n<nnodes; n++) {
    if (gnode_mark[n]) {
      ierr = PetscMemcpy(&subtet->geometry->coords[dim*gncnt],&tet->geometry->coords[dim*n],sizeof(PetscReal)*dim);CHKERRQ(ierr);
      gncnt++;
    }
  }
  
  /* copy elements */
  nesub = 0;
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      
      /* copy region */
      subtet->geometry->element_tag[nesub] = tet->geometry->element_tag[e];
      
      /* copy dof indices */
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        subtet->geometry->element[nesub*bpe + k] = gnlist[idx];
      }
      
      nesub++;
    }
  }
  
  /* space node tags */
  ierr = DMTetSpaceElement(dm,NULL,&bpe,&element,&npe,&nnodes,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes,&snode_mark);CHKERRQ(ierr);
  for (n=0; n<nnodes ; n++) {
    snode_mark[n] = PETSC_FALSE;
  }
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        snode_mark[idx] = PETSC_TRUE;
      }
    }
  }
  ierr = PetscMalloc1(nnodes,&snlist);CHKERRQ(ierr);
  sncnt = 0;
  for (n=0; n<nnodes; n++) {
    snlist[n] = -1;
    if (snode_mark[n]) {
      snlist[n] = sncnt;
      sncnt++;
    }
  }
  
  ierr = _DMTetMeshCreate(&subtet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(nesub,bpe,subtet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(sncnt,npe,dim,subtet->space);CHKERRQ(ierr);
  subtet->space->nactivedof = sncnt;
  
  /* copy coordinates */
  sncnt = 0;
  for (n=0; n<nnodes; n++) {
    if (snode_mark[n]) {
      ierr = PetscMemcpy(&subtet->space->coords[dim*sncnt],&tet->space->coords[dim*n],sizeof(PetscReal)*dim);CHKERRQ(ierr);
      sncnt++;
    }
  }
  
  /* copy elements */
  nesub = 0;
  for (e=0; e<nen; e++) {
    if (element_mark[e]) {
      
      /* copy region */
      subtet->space->element_tag[nesub] = tet->space->element_tag[e];
      
      /* copy dof indices */
      for (k=0; k<bpe; k++) {
        PetscInt idx;
        
        idx = element[e*bpe+k];
        subtet->space->element[nesub*bpe + k] = snlist[idx];
      }
      nesub++;
    }
  }
  
  ierr = DMSetUp(sub);CHKERRQ(ierr);
  
  sub->ops->getinjection = DMCreateInjection_SubTet_General;
  dm->ops->getinjection  = DMCreateInjection_SubTet_General;

  /* set objects parent / child -- needed for DMInjection */
  ierr = PetscObjectCompose((PetscObject)sub,"ParentDMTet",(PetscObject)dm);CHKERRQ(ierr);
  
  ierr = DMTetConvertCopyAttachElementMarker(tet->geometry->nelements,element_mark,sub);CHKERRQ(ierr);
  
  {
    IS is;
    
    ierr = ISCreateGeneral(PetscObjectComm((PetscObject)sub),nnodes,snlist,PETSC_COPY_VALUES,&is);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject)sub,"ParentDMTet2SubDMTetIS",(PetscObject)is);CHKERRQ(ierr);
    ierr = ISDestroy(&is);CHKERRQ(ierr);
  }
  
  ierr = PetscFree(element_mark);CHKERRQ(ierr);
  ierr = PetscFree(gnode_mark);CHKERRQ(ierr);
  ierr = PetscFree(snode_mark);CHKERRQ(ierr);
  ierr = PetscFree(snlist);CHKERRQ(ierr);
  ierr = PetscFree(gnlist);CHKERRQ(ierr);
  
  *_sub = sub;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSubmeshByRegions"
PetscErrorCode DMTetCreateSubmeshByRegions(DM dm,PetscInt nr,PetscInt rlist[],DM *_sub)
{
  PetscErrorCode ierr;
  MPI_Comm comm;
  PetscMPIInt commsize,commrank;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = DMTetCreateSubmeshByRegions_SEQ(dm,nr,rlist,_sub);CHKERRQ(ierr);
  } else {
    DM_TET *tet = (DM_TET*)dm->data;
    DM dm_seq = NULL;
    DM sub_seq = NULL;
    
    dm_seq = tet->dmtet_sequential;
    
    /*
    if (commrank == 0) {
      ierr = DMTetCreateSubmeshByRegions_SEQ(dm_seq,nr,rlist,&sub_seq);CHKERRQ(ierr);
      ierr = DMSetUp(sub_seq);CHKERRQ(ierr);
    }
    ierr = DMTetCreatePartitioned(comm,sub_seq,_sub);CHKERRQ(ierr);
    ierr = DMDestroy(&sub_seq);CHKERRQ(ierr);
    ierr = DMSetUp(*_sub);CHKERRQ(ierr);
    */
    
    if (commrank == 0) {
      PetscInt nsub = 0;
      PetscBool *mark = NULL;

      ierr = DMTetMarkElementsByRegions(dm_seq,nr,rlist,&nsub,&mark);CHKERRQ(ierr);
      ierr = DMTetCreateSubmeshGeometry_SEQ(dm_seq,nsub,mark,&sub_seq);CHKERRQ(ierr);

      ierr = PetscFree(mark);CHKERRQ(ierr);
    }
    ierr = DMTetCreatePartitioned(comm,sub_seq,tet->dof,tet->basis_type,tet->basis_order,_sub);CHKERRQ(ierr);

    ierr = PetscObjectCompose((PetscObject)(*_sub),"ParentDMTet",(PetscObject)dm);CHKERRQ(ierr);
    ierr = DMTetDistributeElementMarkerIS(sub_seq,*_sub);CHKERRQ(ierr);
    (*_sub)->ops->getinjection = DMCreateInjection_SubTet_General;
    dm->ops->getinjection      = DMCreateInjection_SubTet_General;
    
    ierr = DMDestroy(&sub_seq);CHKERRQ(ierr);
    ierr = DMSetUp(*_sub);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSubmeshByRegions_CreateElementMaps"
PetscErrorCode DMTetSubmeshByRegions_CreateElementMaps(DM dm,PetscInt nr,PetscInt rlist[],PetscInt *L_p2s,PetscInt **_p2s,PetscInt *L_s2p,PetscInt **_s2p)
{
  PetscInt       nen,*regions,nesub,e,r,ecnt;
  PetscInt       *p2s,*s2p;
  PetscBool      *element_mark;
  PetscMPIInt    commsize;
  PetscErrorCode ierr;
  
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Support for comm.size = 1 only");
  
  /* scan geomtry regions and check requested regions exist (count as you go) */
  ierr = DMTetGeometryGetAttributes(dm,&nen,&regions,NULL);CHKERRQ(ierr);

  /* mark all elements you want to keep and count them */
  ierr = PetscMalloc1(nen,&element_mark);CHKERRQ(ierr);
  
  nesub = 0;
  for (e=0; e<nen; e++) {
    PetscInt regions_per_element = 0;
    
    element_mark[e] = PETSC_FALSE;
    for (r=0; r<nr; r++) {
      if (rlist[r] == regions[e]) {
        element_mark[e] = PETSC_TRUE;
        regions_per_element++;
        nesub++;
      }
    }
    if (regions_per_element > 1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"The array rlist[] must not contain duplicate entries");
  }
  if (nesub == 0) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input mesh does not contain elements with requested region labels");
  
  ierr = PetscMalloc1(nen,&p2s);CHKERRQ(ierr);
  ierr = PetscMalloc1(nesub,&s2p);CHKERRQ(ierr);

  /* 
   Define parent mesh element -> sub mesh element map (p2s)
   Define sub mesh element -> parent mesh element map (s2p)
  */
  ecnt = 0;
  for (e=0; e<nen; e++) {
    p2s[e] = -1;
    if (element_mark[e]) {
      p2s[e] = ecnt;
      s2p[ecnt] = e;
      ecnt++;
    }
  }
  ierr = PetscFree(element_mark);CHKERRQ(ierr);
  
  if (L_p2s) { *L_p2s = nen; }
  if (_p2s) {
    *_p2s = p2s;
  } else {
    ierr = PetscFree(p2s);CHKERRQ(ierr);
  }
  if (L_s2p) { *L_s2p = nesub; }
  if (_s2p) {
    *_s2p = s2p;
  } else {
    ierr = PetscFree(s2p);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateInjectionScalarIS"
PetscErrorCode DMTetCreateInjectionScalarIS(DM dmTo,DM dmFrom,IS *isTo,IS *isFrom)
{
  DM parentdm=NULL,subdm=NULL;
  DMTetBasisType btype_p,btype_s;
  PetscInt degree_p,degree_s;
  PetscMPIInt commsize_p,commsize_s;
  PetscErrorCode ierr;
  
  /* check DM's are potentially compatable for injection (match up from and to) */
  parentdm = NULL;
  ierr = PetscObjectQuery((PetscObject)dmTo,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
  if (parentdm) {
    subdm = dmTo;
    if (parentdm != dmFrom) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmTo was not derived from dmFrom>");
  } else {
    ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    if (parentdm) {
      subdm = dmFrom;
      if (parentdm != dmTo) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other <dmFrom was not derived from dmTo>");
    } else {
      SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"DMTets are not compatable with each other");
    }
  }
  
  ierr = DMTetGetBasisType(parentdm,&btype_p);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(subdm,&btype_s);CHKERRQ(ierr);
  if (btype_p != btype_s) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"Basis type is inconsistent between \"from\" and \"to\" DMs");
  
  ierr = DMTetGetBasisOrder(parentdm,&degree_p);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(subdm,&degree_s);CHKERRQ(ierr);
  if (btype_p != btype_s) SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"Basis degree is inconsistent between \"from\" and \"to\" DMs");
  
  /* check communicators and select appropriate constructor */
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)parentdm),&commsize_p);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)subdm),&commsize_s);CHKERRQ(ierr);
  if ((commsize_p == 1) && (commsize_s == 1)) {
    ierr = DMCreateInjection_SubTet_SEQ_SEQ(dmTo,dmFrom,isFrom,isTo);CHKERRQ(ierr);
  } else if ((commsize_p != 1) && (commsize_s != 1)) {
    IS is = NULL;
    PetscInt nf,*from,nt,*to;
    
    is = NULL;
    ierr = PetscObjectQuery((PetscObject)dmFrom,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
    is = NULL;
    ierr = PetscObjectQuery((PetscObject)subdm,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
    is = NULL;
    ierr = PetscObjectQuery((PetscObject)dmTo,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is);CHKERRQ(ierr);
        
    ierr = DMCreateInjection_SubTet_MPI_MPI(parentdm,subdm,&nf,&from,&nt,&to);CHKERRQ(ierr);
    
    {
      IS isf,ist;
      
      /* create parent -> sub */
      ierr = ISCreateGeneral(PETSC_COMM_SELF,nf,from,PETSC_COPY_VALUES,&isf);CHKERRQ(ierr);
      ierr = ISCreateGeneral(PETSC_COMM_SELF,nt,to,PETSC_COPY_VALUES,&ist);CHKERRQ(ierr);
      
      if (dmFrom == parentdm) {
        *isFrom = isf;
        *isTo   = ist;
      }
      if (dmFrom == subdm) {
        *isFrom = ist;
        *isTo   = isf;
      }
    }
    
    ierr = PetscFree(to);CHKERRQ(ierr);
    ierr = PetscFree(from);CHKERRQ(ierr);
  } else if ((commsize_p != 1) && (commsize_s == 1)) {
    SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"[MPI -> SEQ] Unhandled case for DMTetInjection");
  } else if ((commsize_p == 1) && (commsize_s != 1)) {
    SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"[SEQ -> MPI] Unhandled case for DMTetInjection");
  } else SETERRQ(PetscObjectComm((PetscObject)dmTo),PETSC_ERR_SUP,"Unhandled case for DMTetInjection");
  
  PetscFunctionReturn(0);
}
