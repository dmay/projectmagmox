
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>

#undef __FUNCT__
#define __FUNCT__ "DMTetVecTraverseByRegionId"
PetscErrorCode DMTetVecTraverseByRegionId(DM dm,Vec x,PetscInt regionid,PetscInt dof_idx,
                                          PetscErrorCode (*eval)(PetscScalar*,PetscInt,PetscScalar*,void*),
                                          void *ctx)
{
  PetscInt e,k;
  PetscBool istet = PETSC_FALSE;
  PetscInt blocksize;
  PetscScalar *LA_x;
  PetscInt nen,*regionlist,bpe,npe,*element;
  PetscReal *coords;
  DM dm_vec;
  Vec xl;
  DMTetBasisType btype;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"A valid evaluation function must be provided");

  ierr = VecGetDM(x,&dm_vec);CHKERRQ(ierr);
  if (!dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"x->dm = NULL. Expected input vec was created via DMCreateGlobalVector() or DMGetGlobalVector()");
  if (dm != dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input dm does not match x->dm. Expected input DM was used with DMCreateGlobalVector() or DMGetGlobalVector()");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for DMTET");
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for basis type = DMTET_CWARP_AND_BLEND");

  ierr = VecGetBlockSize(x,&blocksize);CHKERRQ(ierr);
  if (dof_idx > 0) {
    if (dof_idx > blocksize) SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"DOF index must be less than %D",blocksize);
  }
  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = VecGetArray(xl,&LA_x);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,NULL,&bpe,&element,&npe,NULL,&coords);CHKERRQ(ierr);
  if (bpe != npe) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"No support for case when basis-per-element does not equal geometry representation (nodes-per-el)");
  ierr = DMTetGeometryGetAttributes(dm,&nen,&regionlist,NULL);CHKERRQ(ierr);

  if (dof_idx > 0) {
    for (e=0; e<nen; e++) {
      PetscInt *eidx;
      
      if (regionlist[e] != regionid) continue;
      eidx = &element[e*npe];
      for (k=0; k<npe; k++) {
        PetscInt loc;
        PetscScalar coor_k[3],value;
        
        loc = blocksize * eidx[k] + dof_idx;
        
        coor_k[0] = coords[2*eidx[k]+0];
        coor_k[1] = coords[2*eidx[k]+1];
        coor_k[2] = 0.0;
        
        ierr = eval(coor_k,dof_idx,&value,ctx);CHKERRQ(ierr);

        LA_x[loc] = value;
      }
    }
  } else {
    /* process all dofs at once */
    for (e=0; e<nen; e++) {
      PetscInt *eidx;
      
      if (regionlist[e] != regionid) continue;
      eidx = &element[e*npe];
      for (k=0; k<npe; k++) {
        PetscInt d,loc;
        PetscScalar coor_k[3],value[10];
        
        
        coor_k[0] = coords[2*eidx[k]+0];
        coor_k[1] = coords[2*eidx[k]+1];
        coor_k[2] = 0.0;
        
        ierr = eval(coor_k,dof_idx,value,ctx);CHKERRQ(ierr);

        for (d=0; d<blocksize; d++) {
          loc = blocksize * eidx[k] + d;
          LA_x[loc] = value[d];
        }
        
      }
    }
  }
  
  ierr = VecRestoreArray(xl,&LA_x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dm,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 This method supports SEQ and MPI Vec's, however it is somewhat inefficient as it
 (1) creates a local vector
 (2) requires a LocalToGlobal call at the end
 (3) traverses element-wise, then eleent->basis, so the user function gets called many extra times,
     e.g. it gets called N times per basis, where N is the number of elements sharing a basis.
*/
#undef __FUNCT__
#define __FUNCT__ "_DMTetVecTraverse_ElementByElement"
PetscErrorCode _DMTetVecTraverse_ElementByElement(DM dm,Vec x,
                                PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),
                                void *ctx)
{
  PetscInt e,k;
  PetscBool istet = PETSC_FALSE;
  PetscInt blocksize;
  PetscScalar *LA_x;
  PetscInt nen,bpe,npe,*element;
  PetscReal *coords;
  DM dm_vec;
  Vec xl;
  DMTetBasisType btype;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"A valid evaluation function must be provided");
  
  ierr = VecGetDM(x,&dm_vec);CHKERRQ(ierr);
  if (!dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"x->dm = NULL. Expected input vec was created via DMCreateGlobalVector() or DMGetGlobalVector()");
  if (dm != dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input dm does not match x->dm. Expected input DM was used with DMCreateGlobalVector() or DMGetGlobalVector()");

  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for DMTET");
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for basis type = DMTET_CWARP_AND_BLEND");
  
  ierr = VecGetBlockSize(x,&blocksize);CHKERRQ(ierr);

  ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
  ierr = VecGetArray(xl,&LA_x);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,&npe,NULL,&coords);CHKERRQ(ierr);
  if (bpe != npe) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"No support for case when basis-per-element does not equal geometry representation (nodes-per-el)");
  
  /* process all dofs at once */
  for (e=0; e<nen; e++) {
    PetscInt *eidx;
    
    eidx = &element[e*npe];
    for (k=0; k<npe; k++) {
      PetscInt d,loc;
      PetscScalar coor_k[3],value[10];
      
      coor_k[0] = coords[2*eidx[k]+0];
      coor_k[1] = coords[2*eidx[k]+1];
      coor_k[2] = 0.0;
      
      ierr = eval(coor_k,value,ctx);CHKERRQ(ierr);
      
      for (d=0; d<blocksize; d++) {
        loc = blocksize * eidx[k] + d;
        LA_x[loc] = value[d];
      }
    }
  }
  
  ierr = VecRestoreArray(xl,&LA_x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(dm,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm,xl,INSERT_VALUES,x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetVecTraverse_MPI"
PetscErrorCode DMTetVecTraverse_MPI(DM dm,Vec x,
                                PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),
                                void *ctx)
{
  DM_TET *tet;
  PetscInt k,d;
  PetscBool istet = PETSC_FALSE;
  PetscInt blocksize,loc;
  PetscScalar *LA_x;
  PetscInt nen,bpe,npe,nvert,Nv,start,end,*element;
  PetscReal *coords;
  DM dm_vec;
  DMTetBasisType btype;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"A valid evaluation function must be provided");
  
  ierr = VecGetDM(x,&dm_vec);CHKERRQ(ierr);
  if (!dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"x->dm = NULL. Expected input vec was created via DMCreateGlobalVector() or DMGetGlobalVector()");
  if (dm != dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input dm does not match x->dm. Expected input DM was used with DMCreateGlobalVector() or DMGetGlobalVector()");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for DMTET");
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for basis type = DMTET_CWARP_AND_BLEND");
  
  tet = (DM_TET*)dm->data;
  
  ierr = VecGetBlockSize(x,&blocksize);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,&npe,&nvert,&coords);CHKERRQ(ierr);
  if (bpe != npe) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"No support for case when basis-per-element does not equal geometry representation (nodes-per-el)");

  ierr = VecGetLocalSize(x,&Nv);CHKERRQ(ierr);
  ierr = VecGetOwnershipRange(x,&start,&end);CHKERRQ(ierr);
  start = start/blocksize;
  end = end/blocksize;
  ierr = VecZeroEntries(x);CHKERRQ(ierr);
  ierr = VecGetArray(x,&LA_x);CHKERRQ(ierr);
  
  if (Nv != tet->mpi->nnodes_g*blocksize) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Global sizes of space vector do not match");
  
  /* process all dofs at once */
  for (k=0; k<nvert; k++) {
    PetscScalar coor_k[3],value[10];
    PetscInt index,l2g;
    
    l2g = tet->mpi->l2g[k];
    if (l2g < start) continue;
    if (l2g >= end) continue;
    
    coor_k[0] = coords[2*k+0];
    coor_k[1] = coords[2*k+1];
    coor_k[2] = 0.0;
    
    ierr = eval(coor_k,value,ctx);CHKERRQ(ierr);
    
    index = l2g - start;
    for (d=0; d<blocksize; d++) {
      loc = blocksize * index + d;
      LA_x[loc] = value[d];
    }
  }
  ierr = VecRestoreArray(x,&LA_x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetVecTraverse_SEQ"
PetscErrorCode DMTetVecTraverse_SEQ(DM dm,Vec x,
                                    PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),
                                    void *ctx)
{
  PetscInt k,d;
  PetscBool istet = PETSC_FALSE;
  PetscInt blocksize,loc;
  PetscScalar *LA_x;
  PetscInt nen,bpe,npe,nvert,Nv,*element;
  PetscReal *coords;
  DM dm_vec;
  DMTetBasisType btype;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!eval) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"A valid evaluation function must be provided");
  
  ierr = VecGetDM(x,&dm_vec);CHKERRQ(ierr);
  if (!dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"x->dm = NULL. Expected input vec was created via DMCreateGlobalVector() or DMGetGlobalVector()");
  if (dm != dm_vec) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input dm does not match x->dm. Expected input DM was used with DMCreateGlobalVector() or DMGetGlobalVector()");
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for DMTET");
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only valid for basis type = DMTET_CWARP_AND_BLEND");
  
  ierr = VecGetBlockSize(x,&blocksize);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,&npe,&nvert,&coords);CHKERRQ(ierr);
  if (bpe != npe) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"No support for case when basis-per-element does not equal geometry representation (nodes-per-el)");
  
  ierr = VecGetLocalSize(x,&Nv);CHKERRQ(ierr);
  ierr = VecZeroEntries(x);CHKERRQ(ierr);
  ierr = VecGetArray(x,&LA_x);CHKERRQ(ierr);
  
  if (Nv != nvert*blocksize) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Global sizes of space vector do not match");
  
  /* process all dofs at once */
  for (k=0; k<nvert; k++) {
    PetscScalar coor_k[3],value[10];
    PetscInt index;
    
    coor_k[0] = coords[2*k+0];
    coor_k[1] = coords[2*k+1];
    coor_k[2] = 0.0;
    
    ierr = eval(coor_k,value,ctx);CHKERRQ(ierr);
    
    index = k;
    for (d=0; d<blocksize; d++) {
      loc = blocksize * index + d;
      LA_x[loc] = value[d];
    }
  }
  ierr = VecRestoreArray(x,&LA_x);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetVecTraverse"
PetscErrorCode DMTetVecTraverse(DM dm,Vec x,
                                    PetscErrorCode (*eval)(PetscScalar*,PetscScalar*,void*),
                                    void *ctx)
{
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = DMTetVecTraverse_SEQ(dm,x,eval,ctx);CHKERRQ(ierr);
  } else {
    ierr = DMTetVecTraverse_MPI(dm,x,eval,ctx);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}
