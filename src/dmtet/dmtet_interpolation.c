
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>
#include <dmtetimpl.h>
#include <petscdmtet.h>
#include <dmtet_topology.h>
#include <kdtree.h>

typedef struct {
  PetscReal x,y;
} Point2d;

#undef __FUNCT__
#define __FUNCT__ "signp2d"
PetscErrorCode signp2d(Point2d p1, Point2d p2, Point2d p3,PetscReal *s)
{
  *s = (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PointInTriangle_v1"
PetscErrorCode PointInTriangle_v1(Point2d pt, Point2d v1, Point2d v2, Point2d v3,PetscBool *v)
{
  PetscReal s1,s2,s3;
  PetscBool b1, b2, b3;
  
  signp2d(pt, v1, v2,&s1); b1 = s1 < 0.0f;
  signp2d(pt, v2, v3,&s2); b2 = s2 < 0.0f;
  signp2d(pt, v3, v1,&s3); b3 = s3 < 0.0f;
  
  *v = ((b1 == b2) && (b2 == b3));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PointInTriangle_v1_debug"
PetscErrorCode PointInTriangle_v1_debug(Point2d pt, Point2d v1, Point2d v2, Point2d v3,PetscBool *v)
{
  PetscReal s1,s2,s3;
  PetscBool b1, b2, b3;
  
  signp2d(pt, v1, v2,&s1); b1 = s1 < 0.0f;  printf("s1 %+1.14e \n",s1);
  signp2d(pt, v2, v3,&s2); b2 = s2 < 0.0f;  printf("s2 %+1.14e \n",s2);
  signp2d(pt, v3, v1,&s3); b3 = s3 < 0.0f;  printf("s3 %+1.14e \n",s3);
  
  *v = ((b1 == b2) && (b2 == b3));
  PetscFunctionReturn(0);
}

/*
 Point location taken from
 http://totologic.blogspot.com/2014/01/accurate-point-in-triangle-test.html
*/
/* This is suitable when using float */
#define EPSILON_F          0.001
#define EPSILON_SQUARE_F   EPSILON_F * EPSILON_F

/* This is suitable when using double */
#define EPSILON_D          1.0e-6
#define EPSILON_SQUARE_D   EPSILON_D * EPSILON_D

/* This is suitable when using long double */
#define EPSILON_LD         1.0e-15
#define EPSILON_SQUARE_LD  EPSILON_LD * EPSILON_LD

typedef long double funit;
#define EPSILON_FUNIT        EPSILON_LD
#define EPSILON_SQUARE_FUNIT EPSILON_FUNIT * EPSILON_FUNIT

#define MATH_MIN(a,b) (((a)<(b))?(a):(b))
#define MATH_MAX(a,b) (((a)>(b))?(a):(b))

static funit side_fu(funit x1,funit y1,funit x2,funit y2,funit x,funit y)
{
  return (y2 - y1)*(x - x1) + (-x2 + x1)*(y - y1);
}

static PetscBool naivePointInTriangle_fu(funit x1,funit y1,funit x2,funit y2,funit x3,funit y3,funit x,funit y)
{
  PetscBool checkSide1 = side_fu(x1,y1,x2,y2,x,y) >= 0;
  PetscBool checkSide2 = side_fu(x2,y2,x3,y3,x,y) >= 0;
  PetscBool checkSide3 = side_fu(x3,y3,x1,y1,x,y) >= 0;
  
  return checkSide1 && checkSide2 && checkSide3;
}

static PetscBool pointInTriangleBoundingBox_fu(funit x1,funit y1,funit x2,funit y2,funit x3,funit y3,funit x,funit y)
{
  funit xMin = MATH_MIN(x1, MATH_MIN(x2, x3)) - EPSILON_FUNIT;
  funit xMax = MATH_MAX(x1, MATH_MAX(x2, x3)) + EPSILON_FUNIT;
  funit yMin = MATH_MIN(y1, MATH_MIN(y2, y3)) - EPSILON_FUNIT;
  funit yMax = MATH_MAX(y1, MATH_MAX(y2, y3)) + EPSILON_FUNIT;
  
  if ( (x < xMin) || (xMax < x) || (y < yMin) || (yMax < y) ) {
    return PETSC_FALSE;
  } else {
    return PETSC_TRUE;
  }
}

static funit distanceSquarePointToSegment_fu(funit x1,funit y1,funit x2,funit y2,funit x,funit y)
{
  funit p1_p2_squareLength = (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1);
  funit dotProduct = ((x - x1)*(x2 - x1) + (y - y1)*(y2 - y1)) / p1_p2_squareLength;
  
  if (dotProduct < 0) {
    return (x - x1)*(x - x1) + (y - y1)*(y - y1);
  } else if (dotProduct <= 1) {
    funit p_p1_squareLength = (x1 - x)*(x1 - x) + (y1 - y)*(y1 - y);
    return p_p1_squareLength - dotProduct * dotProduct * p1_p2_squareLength;
  } else {
    return (x - x2)*(x - x2) + (y - y2)*(y - y2);
  }
}

static PetscBool accuratePointInTriangle_fu(PetscReal _x1,PetscReal _y1,PetscReal _x2,PetscReal _y2,PetscReal _x3,PetscReal _y3,PetscReal _x,PetscReal _y)
{
  funit x1,y1,x2,y2,x3,y3,x,y;
  
  x1 = (funit)_x1;
  x2 = (funit)_x2;
  x3 = (funit)_x3;
  
  y1 = (funit)_y1;
  y2 = (funit)_y2;
  y3 = (funit)_y3;
  
  x = (funit)_x;
  y = (funit)_y;
  
  if (!pointInTriangleBoundingBox_fu(x1,y1,x2,y2,x3,y3,x,y)) {
    return PETSC_FALSE;
  }
  
  if (naivePointInTriangle_fu(x1,y1,x2,y2,x3,y3,x,y)) {
    return PETSC_TRUE;
  }
  
  if (distanceSquarePointToSegment_fu(x1,y1,x2,y2,x,y) <= EPSILON_SQUARE_FUNIT) {
    return PETSC_TRUE;
  }
  
  if (distanceSquarePointToSegment_fu(x2,y2,x3,y3,x,y) <= EPSILON_SQUARE_FUNIT) {
    return PETSC_TRUE;
  }
  
  if (distanceSquarePointToSegment_fu(x3,y3,x1,y1,x,y) <= EPSILON_SQUARE_FUNIT) {
    return PETSC_TRUE;
  }
  
  return PETSC_FALSE;
}

#undef __FUNCT__
#define __FUNCT__ "PointInTriangle_v2"
PetscErrorCode PointInTriangle_v2(Point2d pt, Point2d v1, Point2d v2, Point2d v3,PetscBool *v)
{
  *v = PETSC_FALSE;
  
  *v = accuratePointInTriangle_fu( v1.x,v1.y , v2.x,v2.y , v3.x,v3.y , pt.x,pt.y );
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetComputeLocalCoordinateAffine2d"
PetscErrorCode DMTetComputeLocalCoordinateAffine2d(PetscReal xp[],PetscReal coords[],PetscReal xip[])
{
  PetscReal x1,y1,x2,y2,x3,y3;
  PetscReal c,b[2],A[2][2],inv[2][2],detJ,od;
  
  x1 = coords[2*0+0];
  x2 = coords[2*1+0];
  x3 = coords[2*2+0];
  
  y1 = coords[2*0+1];
  y2 = coords[2*1+1];
  y3 = coords[2*2+1];
  
  c = x1 - 0.5*x1 - 0.5*x1 + 0.5*x2 + 0.5*x3;
  b[0] = xp[0] - c;
  c = y1 - 0.5*y1 - 0.5*y1 + 0.5*y2 + 0.5*y3;
  b[1] = xp[1] - c;
  
  A[0][0] = -0.5*x1 + 0.5*x2;   A[0][1] = -0.5*x1 + 0.5*x3;
  A[1][0] = -0.5*y1 + 0.5*y2;   A[1][1] = -0.5*y1 + 0.5*y3;
  
  detJ = A[0][0]*A[1][1] - A[0][1]*A[1][0];
  od = 1.0/detJ;
  
  inv[0][0] =  A[1][1] * od;
  inv[0][1] = -A[0][1] * od;
  inv[1][0] = -A[1][0] * od;
  inv[1][1] =  A[0][0] * od;
  
  xip[0] = inv[0][0]*b[0] + inv[0][1]*b[1];
  xip[1] = inv[1][0]*b[0] + inv[1][1]*b[1];
  
  PetscFunctionReturn(0);
}

typedef enum { ORIENTATION_COLINEAR = 0, ORIENTATION_CWISE = 1, ORIENTATION_CCWISE = 2 } OrientationType;

// To find orientation of ordered triplet (p1, p2, p3).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
OrientationType TriangleDetermineOrientation(Point2d a, Point2d b, Point2d c)
{
  Point2d p1,p2,p3;
  PetscReal c12,c23,c31;
  
  p1.x = b.x - a.x;  p1.y = b.y - a.y;
  p2.x = c.x - b.x;  p2.y = c.y - b.y;
  p3.x = a.x - c.x;  p3.y = a.y - c.y;
  
  //cross(p1,p2);
  c12 = p1.x*p2.y - p1.y*p2.x;
  //cross(p2,p3);
  c23 = p2.x*p3.y - p2.y*p3.x;
  //cross(p3,p1);
  c31 = p3.x*p1.y - p3.y*p1.x;

  if ( (c12 < 0.0) && (c23 < 0.0) && (c31 < 0.0) ) {
    return ORIENTATION_CCWISE;
  } else if ( (c12 > 0.0) && (c23 > 0.0) && (c31 > 0.0) ) {
    return ORIENTATION_CWISE;
  } else {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Mixed sign cross products detected");
  }

}

#undef __FUNCT__
#define __FUNCT__ "DMTetPointLocationAffine2d"
PetscErrorCode DMTetPointLocationAffine2d(DM dm,const PetscReal gmin[],const PetscReal gmax[],PetscInt npoints,PetscReal xp[],PetscInt econtaining[],PetscReal xip[])
{
  PetscErrorCode ierr;
  PetscInt e,nelements_g,bpe_g,*element_g,npe_g,nnodes_g;
  PetscReal *coords_g,elcoords_g[3*2];
  PetscInt p,i;
  PetscBool point_located,point_in_boundingbox;
  Point2d point,coords[3];
  DMTetBasisType btype;
  //PetscErrorCode (*fp_point_in_triangle)(Point2d,Point2d,Point2d,Point2d,PetscBool*) = PointInTriangle_v1;
  PetscErrorCode (*fp_point_in_triangle)(Point2d,Point2d,Point2d,Point2d,PetscBool*) = PointInTriangle_v2;
  OrientationType orientation;
  
  ierr = DMTetGeometryElement(dm,&nelements_g,&bpe_g,&element_g,&npe_g,&nnodes_g,&coords_g);CHKERRQ(ierr);
  for (p=0; p<npoints; p++) {
    PetscInt fail_counter = 0;
    
    point.x = xp[2*p+0];
    point.y = xp[2*p+1];
    
  restart:
    
    point_in_boundingbox = PETSC_TRUE;
    if (point.x < gmin[0]) { point_in_boundingbox = PETSC_FALSE; }
    if (point.y < gmin[1]) { point_in_boundingbox = PETSC_FALSE; }
    if (point.x > gmax[0]) { point_in_boundingbox = PETSC_FALSE; }
    if (point.y > gmax[1]) { point_in_boundingbox = PETSC_FALSE; }
    
    point_located = PETSC_FALSE;
    if (econtaining[p] != -1) {
      PetscInt *elnidx_g;

      e = econtaining[p];
      /* get element -> node map for the triangle geometry */
      elnidx_g = &element_g[npe_g*e];
      
      /* get element coordinates */
      for (i=0; i<npe_g; i++) {
        PetscInt nidx = elnidx_g[i];
        
        coords[i].x = coords_g[2*nidx  ];
        coords[i].y = coords_g[2*nidx+1];
        
        elcoords_g[2*i+0] = coords_g[2*nidx  ];
        elcoords_g[2*i+1] = coords_g[2*nidx+1];
      }
      
      orientation = TriangleDetermineOrientation(coords[0],coords[1],coords[2]);
      if (orientation == ORIENTATION_CWISE) {
        for (i=0; i<npe_g; i++) {
          PetscInt nidx = elnidx_g[i];
          
          coords[npe_g-1-i].x = coords_g[2*nidx  ];
          coords[npe_g-1-i].y = coords_g[2*nidx+1];
        }
      } else if (orientation == ORIENTATION_COLINEAR) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Triangle %D is colinear",e);
      
      ierr = fp_point_in_triangle(point, coords[0],coords[1],coords[2], &point_located);CHKERRQ(ierr);
    }

    if (!point_located && point_in_boundingbox) {
      for (e=0; e<nelements_g; e++) {
        PetscInt *elnidx_g;
        
        /* get element -> node map for the triangle geometry */
        elnidx_g = &element_g[npe_g*e];
        
        /* get element coordinates */
        for (i=0; i<npe_g; i++) {
          PetscInt nidx = elnidx_g[i];
          
          coords[i].x = coords_g[2*nidx  ];
          coords[i].y = coords_g[2*nidx+1];
          
          elcoords_g[2*i+0] = coords_g[2*nidx  ];
          elcoords_g[2*i+1] = coords_g[2*nidx+1];
        }

        orientation = TriangleDetermineOrientation(coords[0],coords[1],coords[2]);
        if (orientation == ORIENTATION_CWISE) {
          for (i=0; i<npe_g; i++) {
            PetscInt nidx = elnidx_g[i];
            
            coords[npe_g-1-i].x = coords_g[2*nidx  ];
            coords[npe_g-1-i].y = coords_g[2*nidx+1];
          }
        } else if (orientation == ORIENTATION_COLINEAR) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Triangle %D is colinear",e);

        ierr = fp_point_in_triangle(point, coords[0],coords[1],coords[2], &point_located);CHKERRQ(ierr);
        
        if (point_located) { break; }
      }
    }

    if ((!point_located) && (fail_counter <= 10) && point_in_boundingbox) {
      PetscReal shift[2];
      
      fail_counter++;

      shift[0] = (PetscReal)( 2.0 * rand()/((double)RAND_MAX) - 1.0 );
      shift[1] = (PetscReal)( 2.0 * rand()/((double)RAND_MAX) - 1.0 );
      
      PetscPrintf(PETSC_COMM_SELF,"  [%D] Failed to locate point (%1.4e,%1.4e) in local mesh - applying small random shift\n",fail_counter,point.x,point.y);
      
      point.x = xp[2*p+0];
      point.y = xp[2*p+1];

      point.x += 1.0e-14 * shift[0];
      point.y += 1.0e-14 * shift[1];

      goto restart;
    }
    
    if (!point_located) {
      econtaining[p] = -1;
      xip[2*p  ] = NAN;
      xip[2*p+1] = NAN;
      if (point_in_boundingbox) {
        PetscPrintf(PETSC_COMM_SELF,"Failed to locate point (%1.4e,%1.4e) in local mesh \n",point.x,point.y);
      }
    } else {
      /* copy element index */
      econtaining[p] = e;
      
      /* compute local coordinates */
      ierr = DMTetComputeLocalCoordinateAffine2d(&xp[2*p],elcoords_g,&xip[2*p]);CHKERRQ(ierr);
      
      //PetscPrintf(PETSC_COMM_SELF,"[p=%D] x(%+1.4e,%+1.4e) -> mapped to element %D xi(%+1.4e,%+1.4e)\n",p,point.x,point.y,econtaining[p],xip[2*p],xip[2*p+1]);
    }
  }
  
  /* rescale xi if required */
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype == DMTET_CLEGENDRE) {
    PetscPrintf(PETSC_COMM_WORLD,"DMTetPointLocationAffine2d : Rescaling local coords to the domain [0,1]\n");
    for (p=0; p<npoints; p++) {
      if (econtaining[p] != -1) {
        xip[2*p+0] = 0.5*(xip[2*p+0] + 1.0);
        xip[2*p+1] = 0.5*(xip[2*p+1] + 1.0);
      }
    }
  }
  PetscFunctionReturn(0);
}

/*
 dmF == dmFrom
 dmT == dmTo
 
 dmT->vec = II * dmF->vec
 
 II_{ij} = dmF->Ni( dmT->coords_j )
*/
#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_DMTET_SEQ"
PetscErrorCode DMCreateInterpolation_DMTET_DMTET_SEQ(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  DM_TET *tetT;
  PetscBool istet;
  Mat interp;
  PetscInt i,nelF,nelT,bpeF,bpeT,nnodesF,nnodesT,*elementF,*elementT;
  PetscReal *coorF,*coorT;
  PetscReal gmin[3],gmax[3];
  
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must be type DMTET");
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"Arg 2 must be type DMTET");

  tetF = (DM_TET*)dmF->data;
  tetT = (DM_TET*)dmT->data;

  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  if (tetT->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 2 must use a continuous basis");
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dmT),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,PETSC_DECIDE,PETSC_DECIDE,tetT->space->nactivedof,tetF->space->nactivedof);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATSEQAIJ);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nelT,&bpeT,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);
  
  ierr = MatSeqAIJSetPreallocation(interp,bpeF,NULL);CHKERRQ(ierr);
  
#if 0
  for (i=0; i<nnodesT; i++) {
    PetscReal *pos_i,xi[2];
    PetscInt eidF,*elF;
    PetscReal **Ni;
    PetscInt nb;
    
    pos_i = &coorT[2*i];
    
    /* locate element and local coord within dmF */
    ierr = DMTetPointLocationAffine2d(dmF,1,pos_i,&eidF,xi);CHKERRQ(ierr);
    if (eidF == -1) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"The point must be found - extrapolation not supported");
    
    ierr = DMTetTabulateBasis(dmF,1,xi,&nb,&Ni);CHKERRQ(ierr);

    elF = &elementF[bpeF*eidF];
    ierr = MatSetValues(interp,1,&i,nb,elF,Ni[0],INSERT_VALUES);CHKERRQ(ierr);
    
    ierr = PetscFree(Ni[0]);CHKERRQ(ierr);
    ierr = PetscFree(Ni);CHKERRQ(ierr);
  }
#endif
  
  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  eidF,*elF;
    PetscReal **Ni;
    PetscInt  nb;

    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);

    /* compute bounding box */
    ierr = DMTetGetBoundingBox(dmF,gmin,gmax);CHKERRQ(ierr);
    
    eidF = -1;
    for (i=0; i<nnodesT; i++) {
      PetscReal *pos_i,xi[2];
      
      pos_i = &coorT[2*i];
      
      /* locate element and local coord within dmF */
      ierr = DMTetPointLocationAffine2d(dmF,gmin,gmax,1,pos_i,&eidF,xi);CHKERRQ(ierr);
      if (eidF == -1) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"The point must be found - extrapolation not supported");
      
      xilist[2*i]   = xi[0];
      xilist[2*i+1] = xi[1];
      ellist[i] = eidF;
      //printf("[TET-TET] node %d : econtaining %d \n",i,eidF);
    }
    
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
      
    for (i=0; i<nnodesT; i++) {
      elF = &elementF[bpeF*ellist[i]];
      ierr = MatSetValues(interp,1,&i,nb,elF,Ni[i],INSERT_VALUES);CHKERRQ(ierr);

      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_DMTET_SameGeom_SEQ"
PetscErrorCode DMCreateInterpolation_DMTET_DMTET_SameGeom_SEQ(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  DM_TET *tetT;
  PetscBool istet;
  Mat interp;
  PetscInt e,i,nelF,nelT,bpeF,bpeT,nnodesF,nnodesT,*elementF,*elementT;
  PetscInt nel_g,bpe_g,npe_g,nnodes_g,*element_g;
  PetscReal *coorF,*coorT,elcoor_g[2*3],*coor_g;
  
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must be type DMTET");
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"Arg 2 must be type DMTET");
  
  tetF = (DM_TET*)dmF->data;
  tetT = (DM_TET*)dmT->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  if (tetT->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 2 must use a continuous basis");
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dmT),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,PETSC_DECIDE,PETSC_DECIDE,tetT->space->nactivedof,tetF->space->nactivedof);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATSEQAIJ);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nelT,&bpeT,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,&nel_g,&bpe_g,&element_g,&npe_g,&nnodes_g,&coor_g);CHKERRQ(ierr);
  
  ierr = MatSeqAIJSetPreallocation(interp,bpeF,NULL);CHKERRQ(ierr);
  
  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  *elF,*elT,*elg;
    PetscReal **Ni;
    PetscInt  nb;
    PetscBool *done;
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&done);CHKERRQ(ierr);
    for (i=0; i<nnodesT; i++) { done[i] = PETSC_FALSE; }
    
    for (e=0; e<nelT; e++) {
      elT = &elementT[bpeT*e];
      elg = &element_g[bpe_g*e];
      
      for (i=0; i<3; i++) {
        PetscInt gnidx = elg[i];
        
        elcoor_g[2*i+0] = coor_g[2*gnidx  ];
        elcoor_g[2*i+1] = coor_g[2*gnidx+1];
      }
      
      for (i=0; i<bpeT; i++) {
        PetscInt nidx = elT[i];
        PetscReal *pos_i;
        
        pos_i = &coorT[2*nidx];
        if (!done[nidx]) {

          ierr = DMTetComputeLocalCoordinateAffine2d(pos_i,elcoor_g,&xilist[2*nidx]);CHKERRQ(ierr);
          ellist[nidx] = e;
          done[nidx] = PETSC_TRUE;
        }
      }
    }
    
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<nnodesT; i++) {
      elF = &elementF[bpeF*ellist[i]];
      ierr = MatSetValues(interp,1,&i,nb,elF,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(done);CHKERRQ(ierr);
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_DMDA_SEQ"
PetscErrorCode DMCreateInterpolation_DMTET_DMDA_SEQ(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  Mat interp;
  PetscInt i,nelF,bpeF,nnodesF,nnodesT,*elementF,M,N;
  PetscReal *coorF;
  Vec coor;
  const PetscScalar *LA_coorT;
  PetscReal gmin[3],gmax[3];
  
  tetF = (DM_TET*)dmF->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");

  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  ierr = DMDAGetInfo(dmT,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  nnodesT = M * N;

  /* compute bounding box */
  ierr = DMTetGetBoundingBox(dmF,gmin,gmax);CHKERRQ(ierr);

  ierr = MatCreate(PetscObjectComm((PetscObject)dmT),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,PETSC_DECIDE,PETSC_DECIDE,nnodesT,tetF->space->nactivedof);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATSEQAIJ);CHKERRQ(ierr);
  ierr = MatSeqAIJSetPreallocation(interp,bpeF,NULL);CHKERRQ(ierr);
  
  ierr = DMGetCoordinates(dmT,&coor);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coor,&LA_coorT);CHKERRQ(ierr);
  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  eidF,*elF;
    PetscReal **Ni;
    PetscInt  nb;
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    
    eidF = -1;
    for (i=0; i<nnodesT; i++) {
      PetscReal *pos_i,xi[2];
      
      pos_i = (PetscReal*)&LA_coorT[2*i];
      
      /* locate element and local coord within dmF */
      ierr = DMTetPointLocationAffine2d(dmF,gmin,gmax,1,pos_i,&eidF,xi);CHKERRQ(ierr);
      //if (eidF == -1) SETERRQ(PetscObjectComm((PetscObject)interp),PETSC_ERR_SUP,"The point must be found - extrapolation not supported");
      if (eidF == -1) {
        xi[0] = xi[1] = 0.0;
      }
      
      xilist[2*i]   = xi[0];
      xilist[2*i+1] = xi[1];
      ellist[i] = eidF;
      //printf("[TET-DA] node %d : econtaining %d \n",i,eidF);
    }
    ierr = VecRestoreArrayRead(coor,&LA_coorT);CHKERRQ(ierr);
    
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<nnodesT; i++) {
      if (ellist[i] >= 0) {
        elF = &elementF[bpeF*ellist[i]];
        ierr = MatSetValues(interp,1,&i,nb,elF,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      }
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_SEQ"
PetscErrorCode DMCreateInterpolation_DMTET_SEQ(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscBool dmF_istet,dmF_isda;
  PetscBool dmT_istet,dmT_isda;
  PetscLogDouble t0,t1;
  PetscErrorCode ierr;
  
  
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMTET,&dmF_istet);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMDA,&dmF_isda);CHKERRQ(ierr);

  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&dmT_istet);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMDA,&dmT_isda);CHKERRQ(ierr);

  if (dmF_istet && dmT_istet) {
    PetscInt nelF,nelT,*elementF,*elementT,nnodesF,nnodesT;
    PetscReal *coorF,*coorT;
    PetscBool same_geom = PETSC_FALSE;
    
    ierr = DMTetGeometryElement(dmF,&nelF,NULL,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
    ierr = DMTetGeometryElement(dmT,&nelT,NULL,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);

    if (nelF == nelT) {
      PetscInt i,delta,delta_max=-1;
      for (i=0; i<nelF*3; i++) {
        delta = elementF[i] - elementT[i];
        if (delta > delta_max) { delta_max = delta; }
      }
      if (delta_max == 0) {
        same_geom = PETSC_TRUE;
      }
    }
    
    if (same_geom) {
      PetscTime(&t0);
      ierr = DMCreateInterpolation_DMTET_DMTET_SameGeom_SEQ(dmF,dmT,mat,vec);CHKERRQ(ierr);
      PetscTime(&t1);
      PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolation_DMTET_DMTET_SameGeom_SEQ : %1.2e (sec)\n",t1-t0);
    } else {
      PetscTime(&t0);
      ierr = DMCreateInterpolation_DMTET_DMTET_SEQ(dmF,dmT,mat,vec);CHKERRQ(ierr);
      PetscTime(&t1);
      PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolation_DMTET_DMTET_SEQ : %1.2e (sec)\n",t1-t0);
    }
    
  } else if (dmF_istet && dmT_isda) {
    PetscTime(&t0);
    ierr = DMCreateInterpolation_DMTET_DMDA_SEQ(dmF,dmT,mat,vec);CHKERRQ(ierr);
    PetscTime(&t1);
    PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolation_DMTET_DMDA_SEQ : %1.2e (sec)\n",t1-t0);
  } else if (dmF_isda && dmT_istet) { /* not sure this will ever get executed */
    SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must be type DMTET. To interpolate from DA to TET");
  } else if (dmF_isda && dmT_isda) { /* not sure this will ever get executed */
    /* call standard */
    SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 and 2 must be type DMTET or DMDA - both cannot be of type DMDA");
  } else {
    SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must be of type DMTET");
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_DMTET_SameGeom_MPI"
PetscErrorCode DMCreateInterpolation_DMTET_DMTET_SameGeom_MPI(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  DM_TET *tetT;
  PetscBool istet;
  Mat interp;
  PetscInt e,i,nelF,nelT,bpeF,bpeT,nnodesF,nnodesT,*elementF,*elementT;
  PetscInt nel_g,bpe_g,npe_g,nnodes_g,*element_g;
  PetscReal *coorF,*coorT,elcoor_g[2*3],*coor_g;
  PetscInt i_l2g,*elF_l2g;
  
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must be type DMTET");
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"Arg 2 must be type DMTET");
  
  tetF = (DM_TET*)dmF->data;
  tetT = (DM_TET*)dmT->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  if (tetT->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 2 must use a continuous basis");
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dmT),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,tetT->mpi->nnodes_g,tetF->mpi->nnodes_g,tetT->mpi->nnodes,tetF->mpi->nnodes);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATMPIAIJ);CHKERRQ(ierr);
  ierr = MatSetUp(interp);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nelT,&bpeT,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,&nel_g,&bpe_g,&element_g,&npe_g,&nnodes_g,&coor_g);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(bpeF,&elF_l2g);CHKERRQ(ierr);
  
  ierr = MatMPIAIJSetPreallocation(interp,bpeF,NULL,bpeF,NULL);CHKERRQ(ierr);
  ierr = MatSetOption(interp,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);CHKERRQ(ierr);
  
  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  *elF,*elT,*elg;
    PetscReal **Ni;
    PetscInt  nb;
    PetscBool *done;
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&done);CHKERRQ(ierr);
    for (i=0; i<nnodesT; i++) { done[i] = PETSC_FALSE; }
    
    for (e=0; e<nelT; e++) {
      elT = &elementT[bpeT*e];
      elg = &element_g[bpe_g*e];
      
      for (i=0; i<3; i++) {
        PetscInt gnidx = elg[i];
        
        elcoor_g[2*i+0] = coor_g[2*gnidx  ];
        elcoor_g[2*i+1] = coor_g[2*gnidx+1];
      }
      
      for (i=0; i<bpeT; i++) {
        PetscInt nidx = elT[i];
        PetscReal *pos_i;
        
        pos_i = &coorT[2*nidx];
        if (!done[nidx]) {
          
          ierr = DMTetComputeLocalCoordinateAffine2d(pos_i,elcoor_g,&xilist[2*nidx]);CHKERRQ(ierr);
          ellist[nidx] = e;
          done[nidx] = PETSC_TRUE;
        }
      }
    }
    
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<nnodesT; i++) {
      PetscInt k;
      
      i_l2g = tetT->mpi->l2g[ i ];
      
      elF = &elementF[bpeF*ellist[i]];
      for (k=0; k<bpeF; k++) {
        elF_l2g[k] = tetF->mpi->l2g[ elF[k] ];
      }
      
      ierr = MatSetValues(interp,1,&i_l2g,nb,elF_l2g,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(done);CHKERRQ(ierr);
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = PetscFree(elF_l2g);CHKERRQ(ierr);
  
  *mat = interp;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_DMTET_SameGeom_MPI_optimalnnz"
PetscErrorCode DMCreateInterpolation_DMTET_DMTET_SameGeom_MPI_optimalnnz(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  DM_TET *tetT;
  PetscBool istet;
  Mat interp;
  PetscInt e,i,nelF,nelT,bpeF,bpeT,nnodesF,nnodesT,*elementF,*elementT;
  PetscInt nel_g,bpe_g,npe_g,nnodes_g,*element_g;
  PetscReal *coorF,*coorT,elcoor_g[2*3],*coor_g;
  PetscInt i_l2g,*elF_l2g;
  
  PetscPrintf(PetscObjectComm((PetscObject)dmF),"DMCreateInterpolation: Using experimental version with exact preallocation\n");
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must be type DMTET");
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"Arg 2 must be type DMTET");
  
  tetF = (DM_TET*)dmF->data;
  tetT = (DM_TET*)dmT->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  if (tetT->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 2 must use a continuous basis");
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dmT),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,tetT->mpi->nnodes_g,tetF->mpi->nnodes_g,tetT->mpi->nnodes,tetF->mpi->nnodes);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATMPIAIJ);CHKERRQ(ierr);
  ierr = MatSetUp(interp);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmT,&nelT,&bpeT,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmT,&nel_g,&bpe_g,&element_g,&npe_g,&nnodes_g,&coor_g);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(bpeF,&elF_l2g);CHKERRQ(ierr);
  
  ierr = MatSetUp(interp);CHKERRQ(ierr);
  
  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  *elF,*elT,*elg;
    PetscReal **Ni;
    PetscInt  nb;
    PetscBool *done;
    PetscInt start,end;
    Vec x;
    
    DMCreateGlobalVector(dmT,&x);
    VecGetOwnershipRange(x,&start,&end);
    VecDestroy(&x);
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&done);CHKERRQ(ierr);
    for (i=0; i<nnodesT; i++) { done[i] = PETSC_FALSE; }
    
    for (e=0; e<nelT; e++) {
      elT = &elementT[bpeT*e];
      elg = &element_g[bpe_g*e];
      
      for (i=0; i<3; i++) {
        PetscInt gnidx = elg[i];
        
        elcoor_g[2*i+0] = coor_g[2*gnidx  ];
        elcoor_g[2*i+1] = coor_g[2*gnidx+1];
      }
      
      for (i=0; i<bpeT; i++) {
        PetscInt nidx = elT[i];
        PetscReal *pos_i;
        
        pos_i = &coorT[2*nidx];
        if (!done[nidx]) {
          
          ierr = DMTetComputeLocalCoordinateAffine2d(pos_i,elcoor_g,&xilist[2*nidx]);CHKERRQ(ierr);
          ellist[nidx] = e;
          done[nidx] = PETSC_TRUE;
        }
      }
    }
    
    
    
    ierr = FARCMatPreallocationInit(interp);CHKERRQ(ierr);
    for (i=0; i<nnodesT; i++) {
      PetscInt k,i_l2g;
      
      i_l2g = tetT->mpi->l2g[ i ];
      
      if (i_l2g < start) continue;
      if (i_l2g >= end) continue;
      
      elF = &elementF[bpeF*ellist[i]];
      for (k=0; k<bpeF; k++) {
        elF_l2g[k] = tetF->mpi->l2g[ elF[k] ];
      }
      ierr = FARCMatPreallocationSetValues(interp,1,&i_l2g,bpeF,elF_l2g);CHKERRQ(ierr);
    }
    ierr = FARCMatPreallocationFinalize(interp);CHKERRQ(ierr);
    
    
    
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<nnodesT; i++) {
      PetscInt k;
      
      i_l2g = tetT->mpi->l2g[ i ];
      if (i_l2g < start) continue;
      if (i_l2g >= end) continue;
      
      elF = &elementF[bpeF*ellist[i]];
      for (k=0; k<bpeF; k++) {
        elF_l2g[k] = tetF->mpi->l2g[ elF[k] ];
      }
      
      ierr = MatSetValues(interp,1,&i_l2g,nb,elF_l2g,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(done);CHKERRQ(ierr);
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  ierr = PetscFree(elF_l2g);CHKERRQ(ierr);
  
  *mat = interp;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET_MPI"
PetscErrorCode DMCreateInterpolation_DMTET_MPI(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscBool dmF_istet,dmT_istet;
  PetscLogDouble t0,t1,deltat,mint,maxt;
  PetscMPIInt commsize[2];
  PetscErrorCode ierr;
  
  
  /* check both are DMTET */
  ierr = PetscObjectTypeCompare((PetscObject)dmF,DMTET,&dmF_istet);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)dmT,DMTET,&dmT_istet);CHKERRQ(ierr);
  
  if (!dmF_istet) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"The \"from\" DM must be of type DMTET");
  if (!dmT_istet) SETERRQ(PetscObjectComm((PetscObject)dmT),PETSC_ERR_SUP,"The \"to\" DM must be of type DMTET");
  
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dmF),&commsize[0]);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dmT),&commsize[1]);CHKERRQ(ierr);
  if (commsize[0] != commsize[1]) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"Communicators of the \"from\" (%D) and \"to\" (%D) DM must match",(PetscInt)commsize[0],(PetscInt)commsize[1]);
  
  if (dmF_istet && dmT_istet) {
    PetscInt nelF,nelT,*elementF,*elementT,nnodesF,nnodesT;
    PetscReal *coorF,*coorT;
    PetscBool same_geom = PETSC_FALSE;
    PetscInt nelg[2];
    
    
    ierr = DMTetGeometryElement(dmF,&nelF,NULL,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
    ierr = DMTetGeometryElement(dmT,&nelT,NULL,&elementT,NULL,&nnodesT,&coorT);CHKERRQ(ierr);
    
    if (nelF != nelT) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"Local number of elements in the \"from\" (%D) and \"to\" (%D) DM must match",nelF,nelT);

    ierr = MPI_Allreduce(&nelF,&nelg[0],1,MPIU_INT,MPI_SUM,PetscObjectComm((PetscObject)dmF));CHKERRQ(ierr);
    ierr = MPI_Allreduce(&nelT,&nelg[1],1,MPIU_INT,MPI_SUM,PetscObjectComm((PetscObject)dmT));CHKERRQ(ierr);

    if (nelg[0] != nelg[1]) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"Global number of elements in the \"from\" (%D) and \"to\" (%D) DM must match",nelg[0],nelg[1]);

    if (nelF == nelT) {
      PetscInt i,delta,delta_max=-1;
      for (i=0; i<nelF*3; i++) {
        delta = elementF[i] - elementT[i];
        if (delta > delta_max) { delta_max = delta; }
      }
      if (delta_max == 0) {
        same_geom = PETSC_TRUE;
      }
    }
    
    if (same_geom) {
      PetscBool use_optimal = PETSC_FALSE;
      ierr = PetscOptionsGetBool(NULL,NULL,"-dmtet_interpolation_mpi_optimal",&use_optimal,NULL);CHKERRQ(ierr);
      
      PetscTime(&t0);
      if (!use_optimal) {
        ierr = DMCreateInterpolation_DMTET_DMTET_SameGeom_MPI(dmF,dmT,mat,vec);CHKERRQ(ierr);
      } else {
        ierr = DMCreateInterpolation_DMTET_DMTET_SameGeom_MPI_optimalnnz(dmF,dmT,mat,vec);CHKERRQ(ierr);
      }
      PetscTime(&t1);
      deltat = t1 - t0;
      
      ierr = MPI_Allreduce(&deltat,&mint,1,MPIU_PETSCLOGDOUBLE,MPI_MIN,PetscObjectComm((PetscObject)dmF));CHKERRQ(ierr);
      ierr = MPI_Allreduce(&deltat,&maxt,1,MPIU_PETSCLOGDOUBLE,MPI_MAX,PetscObjectComm((PetscObject)dmF));CHKERRQ(ierr);
      
      PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolation_DMTET_DMTET_SameGeom : {rank 0,min,max} %1.2e , %1.2e , %1.2e (sec)\n",deltat,mint,maxt);
    } else {
      SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"The \"from\" and \"to\" DM must have the same geometry layout");
    }
    
  } else {
    SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 and 2 must be of type DMTET");
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolation_DMTET"
PetscErrorCode DMCreateInterpolation_DMTET(DM dmF,DM dmT,Mat *mat,Vec *vec)
{
  PetscErrorCode ierr;
  PetscMPIInt size;
  
  if (!dmF->setupcalled) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_USER,"The \"from\" DM is not setup. Call DMSetUp() first");
  if (!dmT->setupcalled) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_USER,"The \"to\" DM is not setup. Call DMSetUp() first");
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dmF),&size);CHKERRQ(ierr);
  
  if (size == 1) {
    ierr = DMCreateInterpolation_DMTET_SEQ(dmF,dmT,mat,vec);CHKERRQ(ierr);
  } else {
    ierr = DMCreateInterpolation_DMTET_MPI(dmF,dmT,mat,vec);CHKERRQ(ierr);
  }
  
  {
    Mat bmat;
    PetscInt bs;
    
    ierr = DMTetGetDof(dmF,&bs);CHKERRQ(ierr);
    if (bs > 1) {
      ierr = MatCreateMAIJ(*mat,bs,&bmat);CHKERRQ(ierr);
      ierr = MatDestroy(mat);CHKERRQ(ierr);
      *mat = bmat;
    }
  }
  
  if (vec) {
    PetscInt M,N,m,n;
    ierr = MatGetSize(*mat,&M,&N);CHKERRQ(ierr);
    ierr = MatGetLocalSize(*mat,&m,&n);CHKERRQ(ierr);
    ierr = VecCreate(PetscObjectComm((PetscObject)*mat),vec);CHKERRQ(ierr);
    ierr = VecSetSizes(*vec,m,M);CHKERRQ(ierr);
    ierr = VecSetFromOptions(*vec);CHKERRQ(ierr);
    ierr = VecSet(*vec,1.0);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolationFromCoords_bruteforce"
PetscErrorCode DMCreateInterpolationFromCoords_bruteforce(DM dmF,PetscInt npoints,const PetscScalar LA_coorT[],PetscBool error_on_point_not_found,Mat *mat)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  Mat interp;
  PetscInt i,nelF,bpeF,nnodesF,nnodesT,*elementF;
  PetscReal *coorF;
  PetscLogDouble t0,t1;
  PetscReal gmin[3],gmax[3];
  
  PetscTime(&t0);
  tetF = (DM_TET*)dmF->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  nnodesT = npoints;

  /* compute bounding box */
  ierr = DMTetGetBoundingBox(dmF,gmin,gmax);CHKERRQ(ierr);

  ierr = MatCreate(PetscObjectComm((PetscObject)dmF),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,PETSC_DECIDE,PETSC_DECIDE,nnodesT,tetF->space->nactivedof);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATSEQAIJ);CHKERRQ(ierr);
  ierr = MatSeqAIJSetPreallocation(interp,bpeF,NULL);CHKERRQ(ierr);
  ierr = MatSetOptionsPrefix(interp,"Iwc_");CHKERRQ(ierr);
  ierr = MatSetFromOptions(interp);CHKERRQ(ierr);
  
  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  eidF,*elF;
    PetscReal **Ni;
    PetscInt  nb;
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    
    eidF = -1;
    for (i=0; i<nnodesT; i++) {
      PetscReal *pos_i,xi[2];
      
      pos_i = (PetscReal*)&LA_coorT[2*i];
      
      /* locate element and local coord within dmF */
      ierr = DMTetPointLocationAffine2d(dmF,gmin,gmax,1,pos_i,&eidF,xi);CHKERRQ(ierr);
      if (eidF == -1) {
        xi[0] = xi[1] = 0.0;
      }
      
      xilist[2*i]   = xi[0];
      xilist[2*i+1] = xi[1];
      ellist[i] = eidF;
    }
    
    if (error_on_point_not_found) {
      PetscBool any_failures = PETSC_FALSE;
      for (i=0; i<nnodesT; i++) {
        Point2d point;
        
        if (ellist[i] == -1) {
          point.x = LA_coorT[2*i+0];
          point.y = LA_coorT[2*i+1];
          PetscPrintf(PETSC_COMM_SELF,"  [pointlocation error] Failed to locate point (%1.4e,%1.4e) in local mesh \n",point.x,point.y);
          any_failures = PETSC_TRUE;
        }
      }
      if (any_failures) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Failed to locate point at least one point in local mesh");
    }
    
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<nnodesT; i++) {
      if (ellist[i] >= 0) {
        elF = &elementF[bpeF*ellist[i]];
        ierr = MatSetValues(interp,1,&i,nb,elF,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      }
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolationFromCoords[brute force] : %1.2e (sec)\n",t1-t0);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolationFromCoords_kdtree"
PetscErrorCode DMCreateInterpolationFromCoords_kdtree(DM dmF,PetscInt npoints,const PetscScalar LA_coorT[],PetscBool error_on_point_not_found,Mat *mat)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  Mat interp;
  PetscInt i,k,p,nelF,bpeF,nnodesF,nnodesT,*elementF,nnodes_g,*element_g,npe_g;
  PetscReal *coorF,*coords_g;
  PetscLogDouble t0,t1;
  PetscReal gmin[3],gmax[3];
  KDTree kdtree;
  FEGTopology topo;
  DMTetBasisType btype;
  //PetscErrorCode (*fp_point_in_triangle)(Point2d,Point2d,Point2d,Point2d,PetscBool*) = PointInTriangle_v1;
  PetscErrorCode (*fp_point_in_triangle)(Point2d,Point2d,Point2d,Point2d,PetscBool*) = PointInTriangle_v2;
  OrientationType orientation;
  
  PetscTime(&t0);
  tetF = (DM_TET*)dmF->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  
  /* setup up the vertex-to-element map */
  ierr = DMTetTopologySetup(dmF,0);CHKERRQ(ierr);
  ierr = DMTetGetFEGTopology(dmF,&topo);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  nnodesT = npoints;
  
  /* compute bounding box */
  ierr = DMTetGetBoundingBox(dmF,gmin,gmax);CHKERRQ(ierr);
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dmF),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,PETSC_DECIDE,PETSC_DECIDE,nnodesT,tetF->space->nactivedof);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATSEQAIJ);CHKERRQ(ierr);
  ierr = MatSeqAIJSetPreallocation(interp,bpeF,NULL);CHKERRQ(ierr);
  ierr = MatSetOptionsPrefix(interp,"Iwc_");CHKERRQ(ierr);
  ierr = MatSetFromOptions(interp);CHKERRQ(ierr);
  
  /* setup the kdtree */
  ierr = DMTetGeometryElement(dmF,NULL,NULL,&element_g,&npe_g,&nnodes_g,&coords_g);CHKERRQ(ierr);

  ierr = KDTreeCreate(nnodes_g,2,&kdtree);CHKERRQ(ierr);
  for (i=0; i<nnodes_g; i++) {
    ierr = KDTreeInsertPoint(kdtree,i,&coords_g[2*i]);CHKERRQ(ierr);
  }
  ierr = KDTreeSetup(kdtree);CHKERRQ(ierr);
  
  {
    PetscReal *xilist;
    PetscInt  *ellist,*nearest_vertex;
    PetscInt  eidF,*elF;
    PetscReal **Ni;
    PetscInt  nb;
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&nearest_vertex);CHKERRQ(ierr);

    /* do nearest vertex query */
    ierr = KDTreeNDistanceQuery(kdtree,nnodesT,(PetscReal*)LA_coorT,nearest_vertex);

    for (p=0; p<nnodesT; p++) {
      PetscInt  i_closest_v,n_connected_elements,e;
      PetscBool point_located,point_in_boundingbox;
      Point2d   point,coords[3];
      PetscReal elcoords_g[2*3];
      PetscInt  candidate_element_list[100],nc,n_candidate_elements;
      
      point.x = LA_coorT[2*p+0];
      point.y = LA_coorT[2*p+1];

      eidF = -1;

      point_in_boundingbox = PETSC_TRUE;
      if (point.x < gmin[0]) { point_in_boundingbox = PETSC_FALSE; }
      if (point.y < gmin[1]) { point_in_boundingbox = PETSC_FALSE; }
      if (point.x > gmax[0]) { point_in_boundingbox = PETSC_FALSE; }
      if (point.y > gmax[1]) { point_in_boundingbox = PETSC_FALSE; }

      if (!point_in_boundingbox) {
        xilist[2*p  ] = 0.0;
        xilist[2*p+1] = 0.0;
        ellist[p] = eidF;
        continue;
      }
      
      point_located = PETSC_FALSE;
      
      i_closest_v = nearest_vertex[p];
      n_connected_elements = topo->vert_to_element->iL[i_closest_v];

      n_candidate_elements = 0;
      for (e=0; e<n_connected_elements; e++) {
        PetscInt eidx;

        eidx = topo->vert_to_element->value[i_closest_v][e];
        candidate_element_list[n_candidate_elements] = eidx;
        //printf("  candidate[%d] %d\n",e,candidate_element_list[n_candidate_elements]);
        n_candidate_elements++;
      }

      /* build next layer */
      nc = n_candidate_elements;
      for (e=0; e<nc; e++) {
        PetscInt eidx;
        PetscInt *elnidx_g;
        
        eidx = candidate_element_list[e];
        elnidx_g = &element_g[npe_g*eidx];
        
        for (k=0; k<npe_g; k++) {
          PetscInt vidx,ee,eidx_n,l;
          PetscBool exists;

          vidx = elnidx_g[k];
          n_connected_elements = topo->vert_to_element->iL[vidx];
          //printf("  examining element[%d] -> vertex[%d]\n",eidx,vidx);
          
          for (ee=0; ee<n_connected_elements; ee++) {
            eidx_n = topo->vert_to_element->value[vidx][ee];
            //printf("    found element neighbour[%d] \n",eidx_n);
            
            exists = PETSC_FALSE;
            for (l=0; l<n_candidate_elements; l++) {
              if (candidate_element_list[l] == eidx_n) {
                exists = PETSC_TRUE;
                break;
              }
            }
            if (!exists) {
              //printf("    appending element [%d] to list \n",eidx_n);
              candidate_element_list[n_candidate_elements] = eidx_n;
              n_candidate_elements++;
            }
            if (n_candidate_elements >= 100) { SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Candidate element list buffer exceeded"); exit(1); }
          }
        }
      }
      
      //printf("point %d: ( %+1.4e , %+1.4e )\n",p,point.x,point.y);
      //printf("  i_closest_v el %d\n",i_closest_v);
      //printf("  neighbour els %d\n",n_candidate_elements);
      for (e=0; e<n_candidate_elements; e++) {
        PetscInt eidx;
        PetscInt *elnidx_g;
        
        eidx = candidate_element_list[e];
        //printf("    edidx[%d] -> %d\n",e,eidx);
        
        /* get element -> node map for the triangle geometry */
        elnidx_g = &element_g[npe_g*eidx];
        
        /* get element coordinates */
        for (k=0; k<npe_g; k++) {
          PetscInt nidx = elnidx_g[k];
          
          coords[k].x = coords_g[2*nidx  ];
          coords[k].y = coords_g[2*nidx+1];

          elcoords_g[2*k+0] = coords_g[2*nidx  ];
          elcoords_g[2*k+1] = coords_g[2*nidx+1];
          //printf("      vertex[%d] -> [%d] ( %+1.4e , %+1.4e )\n",k,nidx,coords[k].x,coords[k].y);
        }

        orientation = TriangleDetermineOrientation(coords[0],coords[1],coords[2]);
        if (orientation == ORIENTATION_CWISE) {
          for (i=0; i<npe_g; i++) {
            PetscInt nidx = elnidx_g[i];
            
            coords[npe_g-1-i].x = coords_g[2*nidx  ];
            coords[npe_g-1-i].y = coords_g[2*nidx+1];
          }
        } else if (orientation == ORIENTATION_COLINEAR) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Triangle %D is colinear",e);
        
        ierr = fp_point_in_triangle(point, coords[0],coords[1],coords[2], &point_located);CHKERRQ(ierr);
        
        if (point_located) {
          eidF = eidx;
          break;
        }
      }
      //printf("--> eidF %d\n",eidF);

      ellist[p] = eidF;

      if (!point_located) {
        xilist[2*p  ] = 0.0;
        xilist[2*p+1] = 0.0;
      } else {
        /* compute local coordinates */
        ierr = DMTetComputeLocalCoordinateAffine2d((PetscReal*)&LA_coorT[2*p],elcoords_g,&xilist[2*p]);CHKERRQ(ierr);
        //PetscPrintf(PETSC_COMM_SELF,"[p=%D] x(%+1.4e,%+1.4e) -> mapped to element %D xi(%+1.4e,%+1.4e)\n",p,point.x,point.y,ellist[p],xilist[2*p],xilist[2*p+1]);
      }
    }

    if (error_on_point_not_found) {
      PetscBool any_failures = PETSC_FALSE;
      for (p=0; p<nnodesT; p++) {
        Point2d point;
        
        if (ellist[p] == -1) {
          point.x = LA_coorT[2*p+0];
          point.y = LA_coorT[2*p+1];
          PetscPrintf(PETSC_COMM_SELF,"  [pointlocation error] Failed to locate point (%1.4e,%1.4e) in local mesh \n",point.x,point.y);
          any_failures = PETSC_TRUE;
        }
      }
      if (any_failures) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Failed to locate at least one point in the local mesh");
    }
    
    /* rescale xi if required */
    ierr = DMTetGetBasisType(dmF,&btype);CHKERRQ(ierr);
    if (btype == DMTET_CLEGENDRE) {
      PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolationFromCoords[kdtree] : Rescaling local coords to the domain [0,1]\n");
      for (p=0; p<nnodesT; p++) {
        if (ellist[p] != -1) {
          xilist[2*p+0] = 0.5*(xilist[2*p+0] + 1.0);
          xilist[2*p+1] = 0.5*(xilist[2*p+1] + 1.0);
        }
      }
    }
    
/*
    ierr = DMTetTabulateBasis(dmF,nnodesT,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<nnodesT; i++) {
      if (ellist[i] < 0) continue;
      
      elF = &elementF[bpeF*ellist[i]];
      ierr = MatSetValues(interp,1,&i,nb,elF,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(Ni);CHKERRQ(ierr);
*/
/*
    for (i=0; i<nnodesT; i++) {
      if (ellist[i] < 0) continue;

      ierr = DMTetTabulateBasis(dmF,1,&xilist[2*i],&nb,&Ni);CHKERRQ(ierr);

      elF = &elementF[bpeF*ellist[i]];
      ierr = MatSetValues(interp,1,&i,nb,elF,Ni[0],INSERT_VALUES);CHKERRQ(ierr);
      
      ierr = PetscFree(Ni[0]);CHKERRQ(ierr);
      ierr = PetscFree(Ni);CHKERRQ(ierr);
    }
*/
    {
      PetscInt b,kb,nbatches,rem;
      PetscInt batch_size = 10000;
      
      nbatches = nnodesT / batch_size;
      if ( nnodesT < batch_size) {
        batch_size = nnodesT;
        nbatches = 1;
      }
      
      i = 0;
      for (b=0; b<nbatches; b++) {
        ierr = DMTetTabulateBasis(dmF,batch_size,&xilist[2*i],&nb,&Ni);CHKERRQ(ierr);
        for (kb=0; kb<batch_size; kb++) {
          if (ellist[i] >= 0) {
            elF = &elementF[bpeF*ellist[i]];
            ierr = MatSetValues(interp,1,&i,nb,elF,Ni[kb],INSERT_VALUES);CHKERRQ(ierr);
          }
          ierr = PetscFree(Ni[kb]);CHKERRQ(ierr);
          i++;
        }
        ierr = PetscFree(Ni);CHKERRQ(ierr);
      }
      
      rem = nnodesT - nbatches * batch_size;
      if (rem > 0) {
        ierr = DMTetTabulateBasis(dmF,rem,&xilist[2*i],&nb,&Ni);CHKERRQ(ierr);
        for (kb=0; kb<rem; kb++) {
          if (ellist[i] >= 0) {
            elF = &elementF[bpeF*ellist[i]];
            ierr = MatSetValues(interp,1,&i,nb,elF,Ni[kb],INSERT_VALUES);CHKERRQ(ierr);
          }
          ierr = PetscFree(Ni[kb]);CHKERRQ(ierr);
          i++;
        }
        ierr = PetscFree(Ni);CHKERRQ(ierr);
      }
    }

    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
    ierr = PetscFree(nearest_vertex);CHKERRQ(ierr);
  }
  
  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolationFromCoords[kdtree] : %1.2e (sec)\n",t1-t0);
  KDTreeDestroy(&kdtree);
 
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolationFromCoords_bruteforce_MPI"
PetscErrorCode DMCreateInterpolationFromCoords_bruteforce_MPI(DM dmF,PetscInt npoints,const PetscScalar LA_coorT[],PetscBool error_on_point_not_found,Mat *mat)
{
  PetscErrorCode ierr;
  DM_TET *tetF;
  Mat interp;
  PetscInt i,nelF,bpeF,nnodesF,nnodesT,*elementF;
  PetscReal *coorF;
  PetscLogDouble t0,t1;
  PetscReal gmin[3],gmax[3];
  PetscInt partial_sum,offset,m,M;
  ISLocalToGlobalMapping ltog;
  const PetscInt *ltog_indices;
  PetscInt dof;
  PetscInt *elFg;
  
  PetscTime(&t0);
  tetF = (DM_TET*)dmF->data;
  
  /* check both using continuous basis */
  if (tetF->basis_type != DMTET_CWARP_AND_BLEND) SETERRQ(PetscObjectComm((PetscObject)dmF),PETSC_ERR_SUP,"Arg 1 must use a continuous basis");
  
  ierr = MPI_Scan(&npoints,&partial_sum,1,MPIU_INT,MPI_SUM,PetscObjectComm((PetscObject)dmF));CHKERRQ(ierr);
  offset = partial_sum - npoints;
  
  ierr = DMTetSpaceElement(dmF,&nelF,&bpeF,&elementF,NULL,&nnodesF,&coorF);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(bpeF,&elFg);CHKERRQ(ierr);
  
  ierr = MPI_Allreduce(&npoints,&nnodesT,1,MPIU_INT,MPI_SUM,PetscObjectComm((PetscObject)dmF));CHKERRQ(ierr);
  {
    Vec vec;
    PetscInt bs;
    ierr = DMCreateGlobalVector(dmF,&vec);CHKERRQ(ierr);
    ierr = VecGetSize(vec,&M);CHKERRQ(ierr);
    ierr = VecGetLocalSize(vec,&m);CHKERRQ(ierr);
    ierr = VecGetBlockSize(vec,&bs);CHKERRQ(ierr);
    ierr = VecDestroy(&vec);CHKERRQ(ierr);
    m = m / bs;
    M = M / bs;
  }
  
  /* compute bounding box */
  ierr = DMTetGetBoundingBoxLocal(dmF,gmin,gmax);CHKERRQ(ierr);
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dmF),&interp);CHKERRQ(ierr);
  ierr = MatSetSizes(interp,npoints,m,nnodesT,M);CHKERRQ(ierr);
  ierr = MatSetType(interp,MATMPIAIJ);CHKERRQ(ierr);
  ierr = MatMPIAIJSetPreallocation(interp,bpeF,NULL,bpeF,NULL);CHKERRQ(ierr);
  ierr = MatSetOptionsPrefix(interp,"Iwc_");CHKERRQ(ierr);
  ierr = MatSetFromOptions(interp);CHKERRQ(ierr);
  
  ierr = DMTetGetDof(dmF,&dof);CHKERRQ(ierr);
  ierr = DMGetLocalToGlobalMapping(dmF, &ltog);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(ltog, &ltog_indices);CHKERRQ(ierr);

  {
    PetscReal *xilist;
    PetscInt  *ellist;
    PetscInt  eidF,*elF;
    PetscReal **Ni;
    PetscInt  nb;
    
    ierr = PetscMalloc1(nnodesT*2,&xilist);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodesT,&ellist);CHKERRQ(ierr);
    
    eidF = -1;
    for (i=0; i<npoints; i++) {
      PetscReal *pos_i,xi[2];
      
      pos_i = (PetscReal*)&LA_coorT[2*i];
      
      /* locate element and local coord within dmF */
      ierr = DMTetPointLocationAffine2d(dmF,gmin,gmax,1,pos_i,&eidF,xi);CHKERRQ(ierr);
      if (eidF == -1) {
        xi[0] = xi[1] = 0.0;
      }
      
      xilist[2*i]   = xi[0];
      xilist[2*i+1] = xi[1];
      ellist[i] = eidF;
    }
    
    if (error_on_point_not_found) {
      PetscBool any_failures = PETSC_FALSE;
      for (i=0; i<npoints; i++) {
        Point2d point;
        
        if (ellist[i] == -1) {
          point.x = LA_coorT[2*i+0];
          point.y = LA_coorT[2*i+1];
          PetscPrintf(PETSC_COMM_SELF,"  [pointlocation error] Failed to locate point (%1.4e,%1.4e) in local mesh \n",point.x,point.y);
          any_failures = PETSC_TRUE;
        }
      }
      if (any_failures) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Failed to locate point at least one point in local mesh");
    }
    
    ierr = DMTetTabulateBasis(dmF,npoints,xilist,&nb,&Ni);CHKERRQ(ierr);
    
    for (i=0; i<npoints; i++) {
      PetscInt k;
      PetscInt index = i + offset;
      
      if (ellist[i] >= 0) {
        elF = &elementF[bpeF*ellist[i]];
        
        for (k=0; k<nb; k++) {
          PetscInt lindex,gindex;
          
          lindex = dof * elF[k] + 0; /* shift */
          gindex = ltog_indices[lindex] / dof; /* scale by dof to make compatable with a scalar */
          
          elFg[k] = gindex;
        }
        
        ierr = MatSetValues(interp,1,&index,nb,elFg,Ni[i],INSERT_VALUES);CHKERRQ(ierr);
      }
      ierr = PetscFree(Ni[i]);CHKERRQ(ierr);
    }
    
    ierr = PetscFree(Ni);CHKERRQ(ierr);
    ierr = PetscFree(xilist);CHKERRQ(ierr);
    ierr = PetscFree(ellist);CHKERRQ(ierr);
  }
  
  ierr = PetscFree(elFg);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingRestoreIndices(ltog,&ltog_indices);CHKERRQ(ierr);

  ierr = MatAssemblyBegin(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(interp,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  
  *mat = interp;
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"DMCreateInterpolationFromCoords_MPI[brute force] : %1.2e (sec)\n",t1-t0);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMCreateInterpolationFromCoords"
PetscErrorCode DMCreateInterpolationFromCoords(DM dmF,PetscInt bs,PetscInt npoints,const PetscScalar LA_coorT[],PetscBool error_on_point_not_found,Mat *mat)
{
  PetscErrorCode ierr;
  Mat interp_scalar;
  PetscBool use_kdtree = PETSC_FALSE;
  MPI_Comm comm;
  PetscMPIInt commsize;
  
  ierr = PetscObjectGetComm((PetscObject)dmF,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  //if (commsize != 1) SETERRQ(comm,PETSC_ERR_SUP,"General interpolation only valid for sequential \"from\" DM");
  
  if (commsize == 1) {
    ierr = PetscOptionsGetBool(NULL,NULL,"-dmtet.interp_coors_method.kdtree",&use_kdtree,NULL);CHKERRQ(ierr);
    if (!use_kdtree) {
      ierr = DMCreateInterpolationFromCoords_bruteforce(dmF,npoints,LA_coorT,error_on_point_not_found,&interp_scalar);CHKERRQ(ierr);
    } else if (use_kdtree) {
      ierr = DMCreateInterpolationFromCoords_kdtree(dmF,npoints,LA_coorT,error_on_point_not_found,&interp_scalar);CHKERRQ(ierr);
    }
  } else {
    ierr = DMCreateInterpolationFromCoords_bruteforce_MPI(dmF,npoints,LA_coorT,error_on_point_not_found,&interp_scalar);CHKERRQ(ierr);
  }
  
  if (bs > 1) {
    Mat bs_interp;
    
    ierr = MatCreateMAIJ(interp_scalar,bs,&bs_interp);CHKERRQ(ierr);
    ierr = MatDestroy(&interp_scalar);CHKERRQ(ierr);
    *mat = bs_interp;
  } else {
    *mat = interp_scalar;
  }
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "DMCoarsen_Tet"
PetscErrorCode DMCoarsen_Tet(DM dm,MPI_Comm comm,DM *_dm)
{
  DM             dmc;
  DMTetBasisType btype;
  PetscInt       border,dof;
  PetscErrorCode ierr;
  
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  if (border == 1) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only coarsen from p=k to p=1. Input DMTet is using p=1");
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  
  ierr = DMTetCloneGeometry(dm,dof,btype,1,&dmc);CHKERRQ(ierr);
  dmc->fineMesh = dm;
  ierr = PetscObjectReference((PetscObject)dm);CHKERRQ(ierr);
  *_dm = dmc;
  PetscFunctionReturn(0);
}


