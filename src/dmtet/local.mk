libproj-y.c += $(call thisdir, \
    dmtet.c \
    dmtet_2d.c \
    dmtet_basis_cwarpandblend.c \
    dmtet_basis_dg.c \
    dmtet_basis_rt.c \
    dmtetdt.c \
    dmtet_view.c \
    dmtet_geom_utils.c \
    dmtet_interpolation.c \
    dmtet_submesh.c \
    dmtet_iterator.c \
    dmtet_topology.c \
    dmtet_bfacet.c \
    kdtree.c \
    dmtet_refine.c \
    dmtet_matcreate.c \
    dmtet_list.c \
    dmtet_partition.c \
    dmtet_mpi.c \
)

