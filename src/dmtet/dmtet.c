
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petsc/private/dmimpl.h>

#include <dmtetimpl.h>    /*I   "petscdmtet.h"   I*/
#include <dmtet_common.h>
#include <dmtet_topology.h>
#include <dmtet_bfacet.h>

PetscLogEvent DMTET_BasisEvaluate;
PetscLogEvent DMTET_BasisDerivativeEvaluate;
extern PetscLogStage stageLog_DMTET_Basis;


const char *DMTetBasisTypeNames[] = { "UNINIT", "CLEGENDRE", "CFETEKE", "DG", "CWARP_AND_BLEND", "RAVIART_THOMAS", "DMTetBasisType", "DMTET_", 0 };

const char *MeshReaderTypeNames[] = { "triangle", "tetgen", "diffpack", "gambit", "t3d", "generated", "hdf5", "partitionedhdf5", "basic", "MeshReaderType", "MREADER_", 0 };


PetscErrorCode _DMTetMeshMPIDestroy(DMTetMeshMPI **_mpi);

PetscErrorCode DMCreate_Tet(DM dm);
PetscErrorCode DMDestroy_Tet(DM dm);
PetscErrorCode DMSetFromOptions_Tet(PetscOptionItems *PetscOptionsObject,DM dm);

#undef __FUNCT__
#define __FUNCT__ "DMCreate_Tet"
PetscErrorCode DMCreate_Tet(DM dm)
{
  PetscErrorCode ierr;
  DM_TET         *tet;
  
  PetscFunctionBegin;
  PetscValidPointer(dm,1);
  ierr     = PetscNewLog(dm,&tet);CHKERRQ(ierr);
  dm->data = tet;
  
  /* set defaults and init internals for DM_TET */
  ierr = DMSetDimension(dm,-1);CHKERRQ(ierr);
  tet->basis_type = DMTET_UINIT;
  tet->basis_order = -1;
  tet->dof = -1;
  
  tet->is_local = NULL;
  tet->is_global = NULL;
  tet->scat_l2g = NULL;

  /* set operations */
  dm->ops->destroy = DMDestroy_Tet;
  dm->ops->setfromoptions = DMSetFromOptions_Tet;
  dm->ops->setup = DMSetUp_TET;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreate"
PetscErrorCode  DMTetCreate(MPI_Comm comm, DM *dm)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  PetscValidPointer(dm,2);
  ierr = DMCreate(comm,dm);CHKERRQ(ierr);
  ierr = DMSetDimension(*dm,2);CHKERRQ(ierr);
  ierr = DMSetType(*dm,DMTET);CHKERRQ(ierr);
  (*dm)->ops->createinterpolation = DMCreateInterpolation_DMTET;
  (*dm)->ops->coarsen = DMCoarsen_Tet;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDestroy_Tet"
PetscErrorCode DMDestroy_Tet(DM dm)
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  
  /* release internals */
  if (tet->geometry) {
    ierr = _DMTetMeshDestroy(&tet->geometry);CHKERRQ(ierr);
  }
  if (tet->space) {
    ierr = _DMTetMeshDestroy(&tet->space);CHKERRQ(ierr);
  }
  if (tet->mpi) {
    ierr = _DMTetMeshMPIDestroy(&tet->mpi);CHKERRQ(ierr);
  }
  if (tet->partition) {
    ierr = DMTetPartitionDestroy(&tet->partition);CHKERRQ(ierr);
  }
  if (tet->pinfo) {
    ierr = DMTetPartInfoDestroy(&tet->pinfo);CHKERRQ(ierr);
  }
  if (tet->dmtet_sequential) {
    ierr = DMDestroy(&tet->dmtet_sequential);CHKERRQ(ierr);
  }
  
  if (tet->is_local) {
    ierr = ISDestroy(&tet->is_local);CHKERRQ(ierr);
  }
  if (tet->is_global) {
    ierr = ISDestroy(&tet->is_global);CHKERRQ(ierr);
  }
  if (tet->scat_l2g) {
    ierr = VecScatterDestroy(&tet->scat_l2g);CHKERRQ(ierr);
  }

  /* release tet structures */
  ierr = PetscFree(tet);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSetBasisType"
PetscErrorCode DMTetSetBasisType(DM dm,DMTetBasisType type)
{
  DM_TET *tet = (DM_TET*)dm->data;
  if (!dm->setupcalled) {
    tet->basis_type = type;
  } else {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot change basis type once DMSetUp() has been called");
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSetDof"
PetscErrorCode DMTetSetDof(DM dm,PetscInt dof)
{
  DM_TET *tet = (DM_TET*)dm->data;
  if (!dm->setupcalled) {
    tet->dof = dof;
  } else {
    if (dof != tet->dof) {
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot change mesh DOF once DMSetUp() has been called");
    } else {
      tet->dof = dof;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSetBasisOrder"
PetscErrorCode DMTetSetBasisOrder(DM dm,PetscInt order)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt dim;
  PetscErrorCode ierr;
  
  if (dm->setupcalled) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot change basis degree once DMSetUp() has been called");
  }

  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  if (dim == -1) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Must call DMTetSetDim() first");
  }
  
  if (tet->basis_type == DMTET_UINIT) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Must call DMTetSetBasisType() first");
  }
  
  /* check if choices are valid */
  switch (tet->basis_type) {
    case DMTET_CLEGENDRE:
      
      if (order < 1) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Zeroth order basis cannot be defined for contiuous function space");
      break;
      
    case DMTET_CFETEKE:
      if (dim == 2) {
        SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Feteke points (2d) not implemented");
      }
      if (dim == 3) {
        SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Feteke points (3d) not implemented");
      }
      break;
      
    case DMTET_DG:
      break;
      
    case DMTET_CWARP_AND_BLEND:
      if (order < 1) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Zeroth order basis cannot be defined for contiuous function space");
      break;
      
    case DMTET_RAVIART_THOMAS:
      if (order < 0) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"RT basis must have order > 0");
      break;
      
    default:
      break;
  }
  tet->basis_order = order;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshCreate"
PetscErrorCode _DMTetMeshCreate(DMTetMesh **_mesh)
{
  DMTetMesh *m;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&m);CHKERRQ(ierr);
  ierr = PetscMemzero(m,sizeof(DMTetMesh));CHKERRQ(ierr);
  
  m->nelements = -1;
  m->bpe = -1;
  m->nnodes = -1;
  m->npe = -1;
  m->element = NULL;
  m->coords = NULL;
  m->element_tag = NULL;
  /*m->active_element_dof = PETSC_FALSE;*/
  /*m->element_dof = NULL;*/
  ierr = FEGTopologyCreate(&m->topology);CHKERRQ(ierr);
  ierr = BFacetListCreate(&m->boundary_facets);CHKERRQ(ierr);
  m->refcnt = 1;
  *_mesh = m;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshCreate1"
PetscErrorCode _DMTetMeshCreate1(PetscInt nel,PetscInt bpe,DMTetMesh *m)
{
  PetscErrorCode ierr;

  ierr = PetscMalloc(sizeof(PetscInt)*nel*bpe,&m->element);CHKERRQ(ierr);
  ierr = PetscMemzero(m->element,sizeof(PetscInt)*nel*bpe);CHKERRQ(ierr);
  m->nelements = nel;
  m->nghost_elements = 0;
  m->bpe = bpe;
  ierr = PetscMalloc(sizeof(PetscInt)*nel,&m->element_tag);CHKERRQ(ierr);
  ierr = PetscMemzero(m->element_tag,sizeof(PetscInt)*nel);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshCreate2"
PetscErrorCode _DMTetMeshCreate2(PetscInt nnodes,PetscInt npe,PetscInt dim,DMTetMesh *m)
{
  PetscErrorCode ierr;

  ierr = PetscMalloc(sizeof(PetscReal)*nnodes*dim,&m->coords);CHKERRQ(ierr);
  ierr = PetscMemzero(m->coords,sizeof(PetscReal)*nnodes*dim);CHKERRQ(ierr);
  m->nnodes= nnodes;
  m->nghost_nodes = 0;
  m->npe = npe;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshCreate1_with_ghost"
PetscErrorCode _DMTetMeshCreate1_with_ghost(PetscInt nel,PetscInt nel_ghost,PetscInt bpe,DMTetMesh *m)
{
  PetscErrorCode ierr;

  ierr = PetscMalloc(sizeof(PetscInt)*(nel+nel_ghost)*bpe,&m->element);CHKERRQ(ierr);
  ierr = PetscMemzero(m->element,sizeof(PetscInt)*(nel+nel_ghost)*bpe);CHKERRQ(ierr);
  m->nelements = nel;
  m->nghost_elements = nel_ghost;
  m->bpe = bpe;
  ierr = PetscMalloc(sizeof(PetscInt)*(nel+nel_ghost),&m->element_tag);CHKERRQ(ierr);
  ierr = PetscMemzero(m->element_tag,sizeof(PetscInt)*(nel+nel_ghost));CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshCreate2_with_ghost"
PetscErrorCode _DMTetMeshCreate2_with_ghost(PetscInt nnodes,PetscInt nnodes_ghost,PetscInt npe,PetscInt dim,DMTetMesh *m)
{
  PetscErrorCode ierr;
  
  ierr = PetscMalloc(sizeof(PetscReal)*(nnodes+nnodes_ghost)*dim,&m->coords);CHKERRQ(ierr);
  ierr = PetscMemzero(m->coords,sizeof(PetscReal)*(nnodes+nnodes_ghost)*dim);CHKERRQ(ierr);
  m->nnodes = nnodes;
  m->nghost_nodes = nnodes_ghost;
  m->npe = npe;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshDestroy"
PetscErrorCode _DMTetMeshDestroy(DMTetMesh **_mesh)
{
  DMTetMesh *m;
  PetscErrorCode ierr;
  
  if (!_mesh) { PetscFunctionReturn(0); }
  m = *_mesh;
  if ((--m->refcnt) > 0) {
    *_mesh = NULL;
    PetscFunctionReturn(0);
  }
  
  if (m->element) {
    ierr = PetscFree(m->element);CHKERRQ(ierr);
  }
  if (m->coords) {
    ierr = PetscFree(m->coords);CHKERRQ(ierr);
  }
  if (m->element_tag) {
    ierr = PetscFree(m->element_tag);CHKERRQ(ierr);
  }
  /*
  if (m->element_dof) {
    ierr = PetscFree(m->element_dof);CHKERRQ(ierr);
  }
  */
  
  ierr = FEGTopologyDestroy(&m->topology);CHKERRQ(ierr);
  ierr = BFacetListDestroy(&m->boundary_facets);CHKERRQ(ierr);
  
  ierr = PetscFree(m);CHKERRQ(ierr);
  
  *_mesh = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshMPICreate"
PetscErrorCode _DMTetMeshMPICreate(DMTetMeshMPI **_mpi)
{
  DMTetMeshMPI *m;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc(sizeof(DMTetMeshMPI),&m);CHKERRQ(ierr);
  ierr = PetscMemzero(m,sizeof(DMTetMeshMPI));CHKERRQ(ierr);
  
  m->nelements = -1;
  m->nelements_g = -1;
  m->nnodes = -1;
  m->nnodes_l = -1;
  m->nnodes_g = -1;
  m->l2g = NULL;
  m->refcnt = 1;
  *_mpi = m;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMeshMPIDestroy"
PetscErrorCode _DMTetMeshMPIDestroy(DMTetMeshMPI **_mpi)
{
  DMTetMeshMPI *mpi;
  PetscErrorCode ierr;
  
  if (!_mpi) { PetscFunctionReturn(0); }
  mpi = *_mpi;
  if ((--mpi->refcnt) > 0) {
    *_mpi = NULL;
    PetscFunctionReturn(0);
  }
  
  if (mpi->l2g) {
    ierr = PetscFree(mpi->l2g);CHKERRQ(ierr);
  }
  
  ierr = PetscFree(mpi);CHKERRQ(ierr);
  *_mpi = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSetFromOptions_Tet"
PetscErrorCode DMSetFromOptions_Tet(PetscOptionItems *PetscOptionsObject,DM dm)
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  PetscBool flg;
  PetscInt basis_order;
  
  
  ierr = PetscOptionsHead(PetscOptionsObject,"DMTet Options");CHKERRQ(ierr);
  
  ierr = PetscOptionsEnum("-tet_basis_type","Type of basis function","DMTetSetBasisType",DMTetBasisTypeNames,(PetscEnum)tet->basis_type,(PetscEnum*)&tet->basis_type,&flg);CHKERRQ(ierr);
  
  flg = PETSC_FALSE; ierr = PetscOptionsInt("-tet_basis_order","Order of basis function","DMTetSetBasisOrder",tet->basis_order,&basis_order,&flg);CHKERRQ(ierr);
  if (flg) {
    ierr = DMTetSetBasisOrder(dm,basis_order);CHKERRQ(ierr);
  }
  
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetBasisType"
PetscErrorCode DMTetGetBasisType(DM dm,DMTetBasisType *a)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (a) { *a = tet->basis_type; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetBasisOrder"
PetscErrorCode DMTetGetBasisOrder(DM dm,PetscInt *a)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (a) { *a = tet->basis_order; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetDof"
PetscErrorCode DMTetGetDof(DM dm,PetscInt *a)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (a) { *a = tet->dof; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGeometryElement"
PetscErrorCode DMTetGeometryElement(DM dm,PetscInt *nen,PetscInt *bpe,PetscInt **element,PetscInt *npe,PetscInt *nnodes,PetscReal **coords)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (nen)     { *nen = tet->geometry->nelements; }
  if (bpe)     { *bpe = tet->geometry->bpe; }
  if (element) { *element = tet->geometry->element; }
  if (npe)     { *npe = tet->geometry->npe; }
  if (nnodes)     { *nnodes = tet->geometry->nnodes; }
  if (coords)  { *coords = tet->geometry->coords; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGeometryGetAttributes"
PetscErrorCode DMTetGeometryGetAttributes(DM dm,PetscInt *nen,PetscInt **el_tag,PetscInt **node_tag)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (nen)     { *nen = tet->geometry->nelements; }
  if (el_tag)  { *el_tag = tet->geometry->element_tag; }
  //if (node_tag){ *node_tag = tet->geometry->node_tag; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceElement"
PetscErrorCode DMTetSpaceElement(DM dm,PetscInt *nen,PetscInt *bpe,PetscInt **element,PetscInt *npe,PetscInt *nnodes,PetscReal **coords)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (nen)     { *nen = tet->space->nelements; }
  if (bpe)     { *bpe = tet->space->bpe; }
  if (element) { *element = tet->space->element; }
  if (npe)     { *npe = tet->space->npe; }
  if (nnodes)  { *nnodes = tet->space->nnodes; }
  if (coords)  { *coords = tet->space->coords; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceGetElementDOF"
PetscErrorCode DMTetSpaceGetElementDOF(DM dm,PetscInt e,PetscInt **elementdof)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (elementdof) { *elementdof = &tet->space->element[e*tet->space->bpe]; }
  /*
  if (!tet->space->active_element_dof) {
    if (elementdof) { *elementdof = &tet->space->element[e*tet->space->bpe]; }
  } else {
    if (elementdof) { *elementdof = &tet->space->element_dof[e*tet->space->bpe]; }
  }
  */
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceGetAttributes"
PetscErrorCode DMTetSpaceGetAttributes(DM dm,PetscInt *nen,PetscInt **el_tag,PetscInt **node_tag)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (nen)     { *nen = tet->space->nelements; }
  if (el_tag)  { *el_tag = tet->space->element_tag; }
  //if (node_tag){ *node_tag = tet->space->node_tag; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetBoundingBoxLocal"
PetscErrorCode DMTetGetBoundingBoxLocal(DM dm,PetscReal min[],PetscReal max[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal *coords;
  PetscInt i,nnodes,dim;
  PetscErrorCode ierr;
  
  min[0] = min[1] = min[2] = PETSC_MAX_REAL;
  max[0] = max[1] = max[2] = PETSC_MIN_REAL;
  
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  nnodes = tet->geometry->nnodes;
  coords = tet->geometry->coords;
  if (dim == 2) {
    for (i=0; i<nnodes; i++) {
      min[0] = PetscMin(min[0],coords[2*i+0]);
      min[1] = PetscMin(min[1],coords[2*i+1]);
      
      max[0] = PetscMax(max[0],coords[2*i+0]);
      max[1] = PetscMax(max[1],coords[2*i+1]);
    }
  } else if(dim == 3) {
    for (i=0; i<nnodes; i++) {
      min[0] = PetscMin(min[0],coords[3*i+0]);
      min[1] = PetscMin(min[1],coords[3*i+1]);
      min[2] = PetscMin(min[2],coords[3*i+2]);
      
      max[0] = PetscMax(max[0],coords[3*i+0]);
      max[1] = PetscMax(max[1],coords[3*i+1]);
      max[2] = PetscMax(max[2],coords[3*i+2]);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetBoundingBox"
PetscErrorCode DMTetGetBoundingBox(DM dm,PetscReal gmin[],PetscReal gmax[])
{
  DM_TET         *tet = (DM_TET*)dm->data;
  PetscReal      *coords;
  PetscInt       i,nnodes,dim;
  PetscReal      min[3],max[3];
  MPI_Comm       comm;
  PetscErrorCode ierr;
  
  min[0] = min[1] = min[2] = PETSC_MAX_REAL;
  max[0] = max[1] = max[2] = PETSC_MIN_REAL;
  
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  nnodes = tet->geometry->nnodes;
  coords = tet->geometry->coords;
  if (dim == 2) {
    for (i=0; i<nnodes; i++) {
      min[0] = PetscMin(min[0],coords[2*i+0]);
      min[1] = PetscMin(min[1],coords[2*i+1]);
      
      max[0] = PetscMax(max[0],coords[2*i+0]);
      max[1] = PetscMax(max[1],coords[2*i+1]);
    }
  } else if(dim == 3) {
    for (i=0; i<nnodes; i++) {
      min[0] = PetscMin(min[0],coords[3*i+0]);
      min[1] = PetscMin(min[1],coords[3*i+1]);
      min[2] = PetscMin(min[2],coords[3*i+2]);
      
      max[0] = PetscMax(max[0],coords[3*i+0]);
      max[1] = PetscMax(max[1],coords[3*i+1]);
      max[2] = PetscMax(max[2],coords[3*i+2]);
    }
  }
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Allreduce(min,gmin,dim,MPIU_REAL,MPIU_MIN,comm);CHKERRQ(ierr);
  ierr = MPI_Allreduce(max,gmax,dim,MPIU_REAL,MPIU_MAX,comm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetGetMinBasisSeparationLocal"
PetscErrorCode _DMTetGetMinBasisSeparationLocal(DM dm,PetscInt nnodes,PetscReal coords[],PetscReal *gmin)
{
  PetscInt       i,j,d,dim;
  PetscReal      min,sep2;
  PetscErrorCode ierr;
  
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  min = PETSC_MAX_REAL;
  for (i=0; i<nnodes; i++) {
    PetscReal *coord_I;
    
    coord_I = &coords[dim*i];
    
    for (j=i+1; j<nnodes; j++) {
      PetscReal *coord_J;
      
      coord_J = &coords[dim*j];
      
      sep2 = 0.0;
      for (d=0; d<dim; d++) {
        sep2 = sep2 + (coord_I[d] - coord_J[d])*(coord_I[d] - coord_J[d]);
      }
      if (sep2 < min) { min = sep2; }
    }
  }
  *gmin = PetscSqrtReal(min);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetMinBasisSeparationLocal"
PetscErrorCode DMTetGetMinBasisSeparationLocal(DM dm,PetscReal *gmin)
{
  DM_TET         *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = _DMTetGetMinBasisSeparationLocal(dm,tet->space->nnodes,tet->space->coords,gmin);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetMinBasisSeparation"
PetscErrorCode DMTetGetMinBasisSeparation(DM dm,PetscReal *gmin)
{
  DM_TET         *tet = (DM_TET*)dm->data;
  MPI_Comm       comm;
  PetscReal      lmin;
  PetscErrorCode ierr;
  
  ierr = _DMTetGetMinBasisSeparationLocal(dm,tet->space->nnodes,tet->space->coords,&lmin);CHKERRQ(ierr);
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&lmin,gmin,1,MPIU_REAL,MPIU_MIN,comm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetMinCoordSeparationLocal"
PetscErrorCode DMTetGetMinCoordSeparationLocal(DM dm,PetscReal *gmin)
{
  DM_TET         *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = _DMTetGetMinBasisSeparationLocal(dm,tet->geometry->nnodes,tet->geometry->coords,gmin);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetMinCoordSeparation"
PetscErrorCode DMTetGetMinCoordSeparation(DM dm,PetscReal *gmin)
{
  DM_TET         *tet = (DM_TET*)dm->data;
  MPI_Comm       comm;
  PetscReal      lmin;
  PetscErrorCode ierr;
  
  ierr = _DMTetGetMinBasisSeparationLocal(dm,tet->geometry->nnodes,tet->geometry->coords,&lmin);CHKERRQ(ierr);
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&lmin,gmin,1,MPIU_REAL,MPIU_MIN,comm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#if defined(PETSC_HAVE_HDF5)

#undef __FUNCT__
#define __FUNCT__ "DMTetMeshSpaceView_HDF"
PetscErrorCode DMTetMeshSpaceView_HDF(DMTetMesh *space,const PetscInt nsd,const char groupname[],hid_t file_id,hid_t group_id)
{
  hid_t       dataspace_id, attribute_id, dataset_id;  /* identifiers */
  hsize_t     dimension;
  herr_t      status;
  char        name[PETSC_MAX_PATH_LEN];
  
  /* write out the attributes */
  dimension = 1;
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/nelements", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &space->nelements);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/nnodes", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &space->nnodes);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/basis_per_element", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &space->bpe);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/nodes_per_element", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &space->npe);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  
  /* write out the map_el_nd */
  PetscSNPrintf(name,sizeof(name),"%s/map_el_nd",groupname);
  dimension = space->nelements * space->npe;
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  dataset_id = H5Dcreate2(file_id, name, H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
  status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, space->element);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  
  
  /* write out the element_tags */
  PetscSNPrintf(name,sizeof(name),"%s/tags_el",groupname);
  dimension = space->nelements;
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  dataset_id = H5Dcreate2(file_id, name, H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
  status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, space->element_tag);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  
  
  /* write out the node coordinates */
  PetscSNPrintf(name,sizeof(name),"%s/coors",groupname);
  dimension = space->nnodes * nsd;
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  dataset_id = H5Dcreate2(file_id, name, H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
  status = H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, space->coords);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetFieldSpaceView_HDF"
PetscErrorCode DMTetFieldSpaceView_HDF(DM dm,Vec field,const char fieldname[],const char filename[])
{
  hid_t       file_id, dataspace_id, dataset_id;  /* identifiers */
  hsize_t     dimension;
  herr_t      status;
  char        name[PETSC_MAX_PATH_LEN];
  const PetscReal *LA_field;
  PetscInt M,len,dof;
  PetscErrorCode ierr;
  
  file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);

  /* check sizes match */
  ierr = VecGetSize(field,&M);CHKERRQ(ierr);
  ierr = DMTetSpaceGetSize(dm,&len,&dof);CHKERRQ(ierr);
  if (len*dof != M) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Size of Vec does not match size of DMTet->space");
  
  PetscSNPrintf(name,sizeof(name),"/DMTet/Space/Fields/%s",fieldname);
  dimension = M;
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  dataset_id = H5Dcreate2(file_id, name, H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  ierr = VecGetArrayRead(field,&LA_field);CHKERRQ(ierr);
  dataset_id = H5Dopen2(file_id, name, H5P_DEFAULT);
  status = H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, LA_field);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  ierr = VecRestoreArrayRead(field,&LA_field);CHKERRQ(ierr);
  
  status = H5Fclose(file_id);CHKERRQ(status);
  
  PetscFunctionReturn(0);
}

/*
 /DMTet
 /dim
 /dof
 /basis_order
 /basis_type
 
 DMTet/Geometry
 DMTet/Geometry/nelements
 DMTet/Geometry/basis_per_el
 DMTet/Geometry/nodes_per_el
 DMTet/Geometry/nnodes
 DMTet/Geometry/coords
 DMTet/Geometry/map_el_gnd
 DMTet/Geometry/element_tags
 
 DMTet/Space
 ...
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetView_HDF"
PetscErrorCode DMTetView_HDF(DM dm,const char filename[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  hid_t       file_id, dataspace_id, attribute_id, group_id;  /* identifiers */
  hsize_t     dimension;
  herr_t      status;
  PetscInt dim,dofs,order,_btype;
  DMTetBasisType btype;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Currently this is only supported for comm.size = 1 (needs enhancement)");

  /* Create a new file using default properties. */
  file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  
  group_id = H5Gcreate2(file_id, "/DMTet", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  /* Create a dataset attribute. */
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm,&dofs);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  _btype = (PetscInt)btype;
  
  dimension = 1;
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/dim", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &dim);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/dof", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &dofs);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/basis_order", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &order);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);
  
  dataspace_id = H5Screate_simple(1, &dimension, NULL);
  attribute_id = H5Acreate2 (group_id, "/basis_type", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Awrite(attribute_id, H5T_NATIVE_INT, &_btype);CHKERRQ(status);
  status = H5Aclose(attribute_id);status = H5Sclose(dataspace_id);CHKERRQ(status);

  status = H5Gclose(group_id);CHKERRQ(status);

  /* geometry group */
  if (tet->geometry) {
    group_id = H5Gcreate2(file_id, "/DMTet/Geometry", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    ierr = DMTetMeshSpaceView_HDF(tet->geometry,dim,"/DMTet/Geometry",file_id,group_id);CHKERRQ(ierr);
    status = H5Gclose(group_id);CHKERRQ(status);
  }
  
  /* space group */
  if (tet->space) {
    group_id = H5Gcreate2(file_id, "/DMTet/Space", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    ierr = DMTetMeshSpaceView_HDF(tet->space,dim,"/DMTet/Space",file_id,group_id);CHKERRQ(ierr);
    status = H5Gclose(group_id);CHKERRQ(status);
    
    group_id = H5Gcreate2(file_id, "/DMTet/Space/Fields", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Gclose(group_id);CHKERRQ(status);
  }

  status = H5Fclose(file_id);CHKERRQ(status);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceAddGroupNameHDF5"
PetscErrorCode DMTetSpaceAddGroupNameHDF5(const char filename[],const char groupname[])
{
  char           fullgroupname[PETSC_MAX_PATH_LEN];
  hid_t          file_id,group_id;
  herr_t         status;
  
  
  file_id = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);
  if (file_id < 0) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Cannot open HDF5 file %s",filename);

  PetscSNPrintf(fullgroupname,PETSC_MAX_PATH_LEN-1,"/DMTet/Space/Fields/%s",groupname);
  group_id = H5Gcreate2(file_id, fullgroupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  status = H5Gclose(group_id);CHKERRQ(status);
  
  status = H5Fclose(file_id);CHKERRQ(status);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSpaceFromHDF5"
PetscErrorCode DMTetCreateSpaceFromHDF5(DMTetMesh *space,const PetscInt dim,const char filename[],const char groupname[])
{
  PetscInt       _nel,_bpe,_npe,_nnodes;
  char           name[PETSC_MAX_PATH_LEN];
  hid_t          file_id,group_id,attribute_id,dataset_id,dataspace_id,memspace_id;
  herr_t         status;
  hsize_t        dimension;
  PetscErrorCode ierr;
  
  
  file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file_id < 0) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Cannot open HDF5 file %s",filename);
  
  group_id = H5Gopen2(file_id, groupname, H5P_DEFAULT);
  
  attribute_id = H5Aopen_name(group_id, "/nelements");
  status = H5Aread(attribute_id, H5T_NATIVE_INT, &_nel);CHKERRQ(status);
  status = H5Aclose(attribute_id);CHKERRQ(status);
  
  attribute_id = H5Aopen_name(group_id, "/nnodes");
  status = H5Aread(attribute_id, H5T_NATIVE_INT, &_nnodes);CHKERRQ(status);
  status = H5Aclose(attribute_id);CHKERRQ(status);
  
  attribute_id = H5Aopen_name(group_id, "/basis_per_element");
  status = H5Aread(attribute_id, H5T_NATIVE_INT, &_bpe);CHKERRQ(status);
  status = H5Aclose(attribute_id);CHKERRQ(status);
  
  attribute_id = H5Aopen_name(group_id, "/nodes_per_element");
  status = H5Aread(attribute_id, H5T_NATIVE_INT, &_npe);CHKERRQ(status);
  status = H5Aclose(attribute_id);CHKERRQ(status);
  
  /* allocate space */
  ierr = _DMTetMeshCreate1(_nel,_bpe,space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(_nnodes,_npe,dim,space);CHKERRQ(ierr);
  
  /* read form file */
  PetscSNPrintf(name,sizeof(name),"%s/map_el_nd",groupname);
  dataset_id = H5Dopen2(file_id,name,H5P_DEFAULT);
  dataspace_id = H5Dget_space(dataset_id);
  dimension = _nel * _npe;
  memspace_id = H5Screate_simple(1, &dimension, NULL);
  status = H5Dread(dataset_id,H5T_NATIVE_INT,memspace_id,dataspace_id,H5P_DEFAULT,space->element);CHKERRQ(status);
  status = H5Sclose(memspace_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  
  /* write out the element_tags */
  PetscSNPrintf(name,sizeof(name),"%s/tags_el",groupname);
  dataset_id = H5Dopen2(file_id,name,H5P_DEFAULT);
  dataspace_id = H5Dget_space(dataset_id);
  dimension = _nel;
  memspace_id = H5Screate_simple(1, &dimension, NULL);
  status = H5Dread(dataset_id,H5T_NATIVE_INT,memspace_id,dataspace_id,H5P_DEFAULT,space->element_tag);
  status = H5Sclose(memspace_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  
  /* write out the node coordinates */
  PetscSNPrintf(name,sizeof(name),"%s/coors",groupname);
  dataset_id = H5Dopen2(file_id,name,H5P_DEFAULT);
  dataspace_id = H5Dget_space(dataset_id);
  dimension = _nnodes * dim;
  memspace_id = H5Screate_simple(1, &dimension, NULL);
  status = H5Dread(dataset_id,H5T_NATIVE_DOUBLE,memspace_id,dataspace_id,H5P_DEFAULT,space->coords);CHKERRQ(status);
  status = H5Sclose(memspace_id);CHKERRQ(status);
  status = H5Sclose(dataspace_id);CHKERRQ(status);
  status = H5Dclose(dataset_id);CHKERRQ(status);
  
  status = H5Gclose(group_id);CHKERRQ(status);
  status = H5Fclose(file_id);CHKERRQ(status);
  
  PetscFunctionReturn(0);
}

#endif

#if !defined(PETSC_HAVE_HDF5)
#undef __FUNCT__
#define __FUNCT__ "DMTetView_HDF"
PetscErrorCode DMTetView_HDF(DM dm,const char filename[])
{
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requies HDF5 - please recompile PETSc with --download-hdf5=yes or --with-hdf5-dir");
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetFieldSpaceView_HDF"
PetscErrorCode DMTetFieldSpaceView_HDF(DM dm,Vec field,const char fieldname[],const char filename[])
{
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requies HDF5 - please recompile PETSc with --download-hdf5=yes or --with-hdf5-dir");
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSpaceFromHDF5"
PetscErrorCode DMTetCreateSpaceFromHDF5(DMTetMesh *space,const PetscInt dim,const char filename[],const char groupname[])
{
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requies HDF5 - please recompile PETSc with --download-hdf5=yes or --with-hdf5-dir");
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceAddGroupNameHDF5"
PetscErrorCode DMTetSpaceAddGroupNameHDF5(const char filename[],const char groupname[])
{
  SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requies HDF5 - please recompile PETSc with --download-hdf5=yes or --with-hdf5-dir");
  PetscFunctionReturn(0);
}
#endif

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceGetSize"
PetscErrorCode DMTetSpaceGetSize(DM dm,PetscInt *M,PetscInt *dof)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (tet->mpi) {
    *M = tet->mpi->nnodes;
  } else {
    *M = tet->space->nnodes;
  }
  if (dof) { *dof = tet->dof; }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceGetLocalSize"
PetscErrorCode DMTetSpaceGetLocalSize(DM dm,PetscInt *m,PetscInt *dof)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (tet->mpi) {
    *m = tet->mpi->nnodes_g;
  } else {
    *m = tet->space->nnodes;
  }
  if (dof) { *dof = tet->dof; }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSpaceGetSubdomainSize"
PetscErrorCode DMTetSpaceGetSubdomainSize(DM dm,PetscInt *m,PetscInt *dof)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (tet->mpi) {
    *m = tet->mpi->nnodes_l;
  } else {
    *m = tet->space->nnodes;
  }
  if (dof) { *dof = tet->dof; }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasisDerivatives"
PetscErrorCode DMTetTabulateBasisDerivatives(DM dm,PetscInt nqp,PetscReal xi[],
                                             PetscInt *nbasis,PetscReal ***GNix,PetscReal ***GNiy,PetscReal ***GNiz)
{
  PetscInt order;
  DMTetBasisType btype;
  PetscInt dim;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscLogStagePush(stageLog_DMTET_Basis);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(DMTET_BasisDerivativeEvaluate,0,0,0,0);CHKERRQ(ierr);
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  
  switch (dim) {
    case 2:
      
      switch (btype) {
        case DMTET_CWARP_AND_BLEND:
          ierr = DMTetTabulateBasisDerivatives2d_CWARPANDBLEND(nqp,xi,order,nbasis,GNix,GNiy);CHKERRQ(ierr);
          break;

        case DMTET_RAVIART_THOMAS:
          ierr = DMTetTabulateBasisDerivatives2d_RaviartThomas(dm,nqp,xi,nbasis,GNix,GNiy);CHKERRQ(ierr);
          break;

        case DMTET_DG:
          ierr = DMTetTabulateBasisDerivatives2d_DG(nqp,xi,order,nbasis,GNix,GNiy);CHKERRQ(ierr);
          break;
          
        default:
          SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"No basis derivative evaluation support");
          break;
      }
      
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only 2d supported");
      break;
  }
  ierr = PetscLogEventEnd(DMTET_BasisDerivativeEvaluate,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasis"
PetscErrorCode DMTetTabulateBasis(DM dm,PetscInt nqp,PetscReal xi[],
                                  PetscInt *nbasis,PetscReal ***Ni)
{
  PetscInt order;
  DMTetBasisType btype;
  PetscInt dim;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscLogStagePush(stageLog_DMTET_Basis);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(DMTET_BasisEvaluate,0,0,0,0);CHKERRQ(ierr);
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  
  switch (dim) {
    case 2:
      
      switch (btype) {
        case DMTET_CWARP_AND_BLEND:
          ierr = DMTetTabulateBasis2d_CWARPANDBLEND(nqp,xi,order,nbasis,Ni);CHKERRQ(ierr);
          break;
          
        case DMTET_RAVIART_THOMAS:
          ierr = DMTetTabulateBasis2d_RaviartThomas(dm,nqp,xi,nbasis,Ni);CHKERRQ(ierr);
          break;
          
        case DMTET_DG:
          ierr = DMTetTabulateBasis2d_DG(nqp,xi,order,nbasis,Ni);CHKERRQ(ierr);
          break;
          
        default:
          SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"No basis derivative evaluation support");
          break;
      }
      
      break;
    default:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only 2d supported");
      break;
  }
  ierr = PetscLogEventEnd(DMTET_BasisEvaluate,0,0,0,0);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "debug_DMTetCreatePartitioned"
PetscErrorCode debug_DMTetCreatePartitioned(MPI_Comm comm,DM dm,DM *dmp)
{
  PetscErrorCode ierr;
  DMTetPartInfo info;
  PetscMPIInt commrank;

  ierr = DMTetPartInfoCreate(&info);CHKERRQ(ierr);
  ierr = DMTetPartInfoSetup(info,dm,comm);CHKERRQ(ierr);
  
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  if (commrank == 0) {
    ierr = DMTetPartitionView2d_VTK(info->dm,info->epart,"dmtet_partition.vtu");CHKERRQ(ierr);
  }
  
  ierr = DMTetPartInfoDestroy(&info);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMSetUp_TET"
PetscErrorCode DMSetUp_TET(DM dm)
{
  DM_TET *tet = (DM_TET*)dm->data;
  MPI_Comm comm;
  PetscMPIInt commsize;
  DMTetBasisType btype1;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  dm->ops->view = DMTetView_2d;
  
  ierr = DMTetGetBasisType(dm,&btype1);CHKERRQ(ierr);
  if (commsize == 1) {
    dm->ops->createlocalvector  = DMTetCreateGlobalVector_SEQ;
    dm->ops->createglobalvector = DMTetCreateGlobalVector_SEQ;
    dm->ops->globaltolocalbegin = DMTetSEQ_GlobalToLocalBegin;
    dm->ops->globaltolocalend   = DMTetSEQ_GlobalToLocalEnd;
    dm->ops->localtoglobalbegin = DMTetSEQ_LocalToGlobalBegin;
    dm->ops->localtoglobalend   = DMTetSEQ_LocalToGlobalEnd;
    
    switch (btype1) {
      case DMTET_DG:
        dm->ops->creatematrix = DMTetCreateMatrixDiscontinuousSpace_SEQ;
        break;
      case DMTET_CWARP_AND_BLEND:
        dm->ops->creatematrix = DMTetCreateMatrixContinuousSpace_SEQ;
        break;
      case DMTET_RAVIART_THOMAS:
        dm->ops->creatematrix = DMTetCreateMatrixRTSpace_SEQ;
        break;
      default:
        SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Matrix constructor unknown for this DMTET type");
        break;
    }
    
  } else {
    dm->ops->createlocalvector  = DMTetCreateLocalVector_MPI;
    dm->ops->createglobalvector = DMTetCreateGlobalVector_MPI;
    dm->ops->globaltolocalbegin = DMTetMPI_GlobalToLocalBegin;
    dm->ops->globaltolocalend   = DMTetMPI_GlobalToLocalEnd;
    dm->ops->localtoglobalbegin = DMTetMPI_LocalToGlobalBegin;
    dm->ops->localtoglobalend   = DMTetMPI_LocalToGlobalEnd;
    
    switch (btype1) {
      case DMTET_DG:
        /* create the dm local->global map using the exisiting l2g data */
        ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm),tet->dof,tet->mpi->nnodes_l,(const PetscInt*)tet->mpi->l2g,PETSC_COPY_VALUES,&dm->ltogmap);CHKERRQ(ierr);
        
        dm->ops->creatematrix = DMTetCreateMatrixDiscontinuousSpace_MPI;
        break;
        
      case DMTET_CWARP_AND_BLEND:
        /* create the dm local->global map using the exisiting l2g data */
        ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm),tet->dof,tet->mpi->nnodes_l,(const PetscInt*)tet->mpi->l2g,PETSC_COPY_VALUES,&dm->ltogmap);CHKERRQ(ierr);
        
        dm->ops->creatematrix = DMTetCreateMatrixContinuousSpace_MPI;
        break;
        
      case DMTET_RAVIART_THOMAS:
        SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Matrix constructor DMTET RT (MPI) not supported");
        break;
        
      default:
        SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Matrix constructor unknown for this DMTET type");
        break;
    }
  }
  
  if (!tet->space) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Function space on tet is not defined");
  if (!tet->space) { /* generate it now - why not do the basis generation in a lazy fashion? (test later) */
    ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);
  }
  
  /* create the dm local->global map */
  if (commsize == 1) {
    IS is;
    const PetscInt *indices;
    
    if (tet->basis_type == DMTET_RAVIART_THOMAS) {
      ierr = ISCreateStride(PETSC_COMM_SELF,tet->space->nnodes,0,1,&is);CHKERRQ(ierr);
      ierr = ISGetIndices(is,&indices);CHKERRQ(ierr);
      ierr = ISLocalToGlobalMappingCreate(PETSC_COMM_SELF,1,tet->space->nnodes,indices,PETSC_COPY_VALUES,&dm->ltogmap);CHKERRQ(ierr);
      ierr = ISRestoreIndices(is,&indices);CHKERRQ(ierr);
      ierr = ISDestroy(&is);CHKERRQ(ierr);
    } else {
      ierr = ISCreateStride(PETSC_COMM_SELF,tet->space->nactivedof,0,1,&is);CHKERRQ(ierr);
      ierr = ISGetIndices(is,&indices);CHKERRQ(ierr);
      ierr = ISLocalToGlobalMappingCreate(PETSC_COMM_SELF,tet->dof,tet->space->nactivedof,indices,PETSC_COPY_VALUES,&dm->ltogmap);CHKERRQ(ierr);
      ierr = ISRestoreIndices(is,&indices);CHKERRQ(ierr);
      ierr = ISDestroy(&is);CHKERRQ(ierr);
    }
  }
  
  /* create the scatter context using the exisiting l2g data */
  if (commsize > 1) {
    if (tet->mpi) {
      Vec xl,xg;
      
      /* This only makes sense for CG */
      ierr = ISCreateStride(PETSC_COMM_SELF,tet->mpi->nnodes_l*tet->dof,0,1,&tet->is_local);CHKERRQ(ierr);
      ierr = ISCreateBlock(PETSC_COMM_SELF,tet->dof,tet->mpi->nnodes_l,tet->mpi->l2g,PETSC_COPY_VALUES,&tet->is_global);CHKERRQ(ierr);
      
      ierr = DMCreateLocalVector(dm,&xl);CHKERRQ(ierr);
      ierr = DMCreateGlobalVector(dm,&xg);CHKERRQ(ierr);
      
      ierr = VecScatterCreate(xl,tet->is_local,xg,tet->is_global,&tet->scat_l2g);CHKERRQ(ierr);
      
      ierr = VecDestroy(&xl);CHKERRQ(ierr);
      ierr = VecDestroy(&xg);CHKERRQ(ierr);
    } else SETERRQ(comm,PETSC_ERR_USER,"tet->mpi must be non-null");
  }
  
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreatePartitionedFromTriangle"
PetscErrorCode DMTetCreatePartitionedFromTriangle(MPI_Comm comm,const char filename[],PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm)
{
  PetscMPIInt commsize,commrank;
  PetscErrorCode ierr;
  DM dm;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  if (commsize > 1) {
    DM dmsequential = NULL;
    
    if (commrank == 0) {
      ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,filename,dof,btype,border,&dmsequential);CHKERRQ(ierr);
      ierr = DMSetUp(dmsequential);CHKERRQ(ierr);
    }
    ierr = DMTetCreatePartitioned(comm,dmsequential,dof,btype,border,&dm);CHKERRQ(ierr);
    ierr = DMDestroy(&dmsequential);CHKERRQ(ierr);
  } else {
    ierr = DMTetCreate2dFromTriangle(comm,filename,dof,btype,border,&dm);CHKERRQ(ierr);
  }
  ierr = DMSetUp(dm);CHKERRQ(ierr);
  
  *_dm = dm;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreatePartitionedSquareGeometry"
PetscErrorCode DMTetCreatePartitionedSquareGeometry(MPI_Comm comm,
                                                    PetscReal x0,PetscReal x1,PetscReal y0,PetscReal y1,
                                                    PetscInt mx,PetscInt my,
                                                    PetscBool right_oriented,
                                                    PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm)
{
  PetscMPIInt commsize,commrank;
  PetscErrorCode ierr;
  DM dm;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  if (commsize > 1) {
    DM dmsequential = NULL;
    
    if (commrank == 0) {
      ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_SELF,x0,x1,y0,y1,mx,my,right_oriented,dof,btype,border,&dmsequential);CHKERRQ(ierr);
      ierr = DMSetUp(dmsequential);CHKERRQ(ierr);
    }
    ierr = DMTetCreatePartitioned(comm,dmsequential,dof,btype,border,&dm);CHKERRQ(ierr);
    ierr = DMDestroy(&dmsequential);CHKERRQ(ierr);
  } else {
    ierr = DMTetCreate2dSquareGeometry(comm,x0,x1,y0,y1,mx,my,right_oriented,dof,btype,border,&dm);CHKERRQ(ierr);
  }
  ierr = DMSetUp(dm);CHKERRQ(ierr);
  
  *_dm = dm;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateGlobalCoordinateVector"
PetscErrorCode DMTetCreateGlobalCoordinateVector(DM dm,Vec *c)
{
  DM dm_c;
  Vec cl,cg;
  PetscScalar *LA_cl;
  PetscInt nbasis,i;
  PetscReal *coords;
  PetscErrorCode ierr;
  
  ierr = DMTetCreateSharedSpace(dm,2,&dm_c);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_c,&cg);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(dm_c,&cl);CHKERRQ(ierr);
  ierr = VecGetArray(cl,&LA_cl);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_c,NULL,NULL,NULL,NULL,&nbasis,&coords);CHKERRQ(ierr);
  for (i=0; i<nbasis; i++) {
    LA_cl[2*i+0] = coords[2*i+0];
    LA_cl[2*i+1] = coords[2*i+1];
  }
  ierr = VecRestoreArray(cl,&LA_cl);CHKERRQ(ierr);
  
  ierr = DMLocalToGlobalBegin(dm_c,cl,INSERT_VALUES,cg);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(dm_c,cl,INSERT_VALUES,cg);CHKERRQ(ierr);
  
  ierr = VecDestroy(&cl);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_c);CHKERRQ(ierr);
  *c = cg;
  
  PetscFunctionReturn(0);
}

