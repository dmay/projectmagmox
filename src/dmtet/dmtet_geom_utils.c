
#include <petsc.h>
#include <petscmat.h>
#include <petscksp.h>
#include <dmtetimpl.h>
#include <dmtetdt.h>

#undef __FUNCT__
#define __FUNCT__ "GeomHelper_ComputeElememntBoundingBox"
PetscErrorCode GeomHelper_ComputeElememntBoundingBox(DM_TET *tet,PetscReal celldx[])
{
  PetscInt e,i;
  
  PetscFunctionBegin;
  celldx[0] = celldx[1] = PETSC_MAX_REAL;

  for (e=0; e<tet->geometry->nelements; e++) {
    PetscReal cx[2],cy[2];
    
    cx[0] = cy[0] = PETSC_MAX_REAL;
    cx[1] = cy[1] = PETSC_MIN_REAL;
    
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      
      cx[0] = PetscMin(cx[0],tet->geometry->coords[2*idx+0]);
      cy[0] = PetscMin(cy[0],tet->geometry->coords[2*idx+1]);
      
      cx[1] = PetscMax(cx[1],tet->geometry->coords[2*idx+0]);
      cy[1] = PetscMax(cy[1],tet->geometry->coords[2*idx+1]);
    }
    celldx[0] = PetscMin(celldx[0],PetscAbsReal(cx[1]-cx[0]));
    celldx[1] = PetscMin(celldx[1],PetscAbsReal(cy[1]-cy[0]));
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GeomHelper_ComputeDomainBoundingBox"
PetscErrorCode GeomHelper_ComputeDomainBoundingBox(DM_TET *tet,PetscReal min[],PetscReal max[])
{
  PetscInt i;
  
  PetscFunctionBegin;
  min[0] = min[1] = PETSC_MAX_REAL;
  max[0] = max[1] = PETSC_MIN_REAL;
  for (i=0; i<tet->geometry->nnodes; i++) {
    min[0] = PetscMin(min[0],tet->geometry->coords[2*i+0]);
    min[1] = PetscMin(min[1],tet->geometry->coords[2*i+1]);
    
    max[0] = PetscMax(max[0],tet->geometry->coords[2*i+0]);
    max[1] = PetscMax(max[1],tet->geometry->coords[2*i+1]);
  }
  PetscFunctionReturn(0);
}

/*
         2
         |
         |         edge 0
 edge 1  |
         |
         0 ---------- 1
              edge 2
*/
#undef __FUNCT__
#define __FUNCT__ "GeomHelper_ComputeEdgeMidPoints"
PetscErrorCode GeomHelper_ComputeEdgeMidPoints(DM_TET *tet,PetscReal vert[],PetscReal e0[],PetscReal e1[],PetscReal e2[])
{
  PetscInt d;
  PetscReal *v0,*v1,*v2;

  PetscFunctionBegin;
  v0 = &vert[2*0];
  v1 = &vert[2*1];
  v2 = &vert[2*2];

  for (d=0; d<2; d++) {
    e0[d] = 0.5 * (v1[d] + v2[d]);

    e1[d] = 0.5 * (v0[d] + v2[d]);

    e2[d] = 0.5 * (v0[d] + v1[d]);
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "PiolaTransformation_EvaluateJ_DetJ"
PetscErrorCode PiolaTransformation_EvaluateJ_DetJ(PetscReal ecoor[],PetscReal j[],PetscReal *dj)
{
  PetscReal j00,j01,j10,j11;
  
  PetscFunctionBegin;
  j00 = ecoor[2*1+0] - ecoor[2*0+0];
  j01 = ecoor[2*2+0] - ecoor[2*0+0];
  j10 = ecoor[2*1+1] - ecoor[2*0+1];
  j11 = ecoor[2*2+1] - ecoor[2*0+1];
  
  j[0] = j00;
  j[1] = j01;
  j[2] = j10;
  j[3] = j11;
  
  *dj = (j00*j11 - j01*j10);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PiolaTransformation_EvaluateJ_AbsDetJ"
PetscErrorCode PiolaTransformation_EvaluateJ_AbsDetJ(PetscReal ecoor[],PetscReal j[],PetscReal *dj)
{
  PetscFunctionBegin;
  PiolaTransformation_EvaluateJ_DetJ(ecoor,j,dj);
  *dj = PetscAbsReal(*dj);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PiolaTransformation_EvaluateAbsDetJ"
PetscErrorCode PiolaTransformation_EvaluateAbsDetJ(PetscReal ecoor[],PetscReal *dj)
{
  PetscReal j[4];
  
  PetscFunctionBegin;
  PiolaTransformation_EvaluateJ_DetJ(ecoor,j,dj);
  //printf("J = [%1.4e %1.4e ; %1.4e %1.4e]\n",j[0],j[1],j[2],j[3]);
  //printf("dJ %1.4e \n",*dj);
  *dj = PetscAbsReal(*dj);
  PetscFunctionReturn(0);
}

