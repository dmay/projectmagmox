
#include <petsc.h>
#include <data_exchanger.h>
#include <dmtetimpl.h>    /*I   "petscdmtet.h"   I*/
#include <dmtet_common.h>
#include <petscdmtet.h>


#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic_SEQ"
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic_SEQ(DM dm_w,DM dm_u,Mat *_A)
{
  DM_TET *tet_w = (DM_TET*)dm_w->data;
  DM_TET *tet_u = (DM_TET*)dm_u->data;
  PetscInt dof_w,dof_u;
  PetscInt e,k,b,j,jj;
  Mat A;
  MatType type;
  PetscInt *nnz,*nnz_block,**nnmap;
  PetscInt nel,bpe_w,*element_w,bpe_u,*element_u;
  PetscErrorCode ierr;
  
  ierr = DMTetGetDof(dm_w,&dof_w);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm_u,&dof_u);CHKERRQ(ierr);

  ierr = DMTetSpaceElement(dm_w,&nel,&bpe_w,&element_w,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_u,&nel,&bpe_u,&element_u,NULL,NULL,NULL);CHKERRQ(ierr);
  
  /* compute non-zero counts */
  ierr = PetscMalloc1(tet_w->space->nactivedof,&nnz);CHKERRQ(ierr);
  ierr = PetscMalloc1(tet_w->space->nactivedof*dof_w,&nnz_block);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(tet_w->space->nactivedof,&nnmap);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  for (e=0; e<nel; e++) {
    PetscInt *enodes_w,*enodes_u;
    PetscInt nj,njj;
    
    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    for (j=0; j<bpe_w; j++) {
      nj = enodes_w[j];
      
      for (jj=0; jj<bpe_u; jj++) {
        PetscBool exists = PETSC_FALSE;
        
        njj = enodes_u[jj];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[nj]; k++) {
          if (nnmap[nj][k] == njj) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[nj];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[nj]+1));
          nnmap[nj] = tmp;
          nnmap[nj][ nnz[nj] ] = njj;
          nnz[nj]++;
        }
        
      }
      
    }
  }
  
  /* common */
  for (k=0; k<tet_w->space->nactivedof; k++) {
    for (b=0; b<dof_w; b++) {
      nnz_block[dof_w*k + b] = dof_u * nnz[k];
    }
  }

  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm_w),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,
                     tet_w->space->nactivedof*dof_w,
                     tet_u->space->nactivedof*dof_u);CHKERRQ(ierr);
  ierr = DMGetMatType(dm_w,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = MatSetLocalToGlobalMapping(A,dm_w->ltogmap,dm_u->ltogmap);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);

  ierr = MatSeqAIJSetPreallocation(A,0,nnz_block);CHKERRQ(ierr);
  
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt *edof_w,*edof_u;
    
    ierr = PetscMalloc1(bpe_w*dof_w,&edof_w);CHKERRQ(ierr);
    ierr = PetscMalloc1(bpe_u*dof_u,&edof_u);CHKERRQ(ierr);

    ierr = PetscMalloc1(bpe_w*bpe_u*dof_w*dof_u,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,bpe_w*bpe_u*dof_w*dof_u*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nel; e++) {
      PetscInt *enodes_w;
      PetscInt *enodes_u;
      
      enodes_w = &element_w[bpe_w*e];
      for (k=0; k<bpe_w; k++) {
        for (b=0; b<dof_w; b++) {
          edof_w[dof_w*k+b] = dof_w * enodes_w[k] + b;
        }
      }
      
      enodes_u = &element_u[bpe_u*e];
      for (k=0; k<bpe_u; k++) {
        for (b=0; b<dof_u; b++) {
          edof_u[dof_u*k+b] = dof_u * enodes_u[k] + b;
        }
      }

      ierr = MatSetValues(A, bpe_w*dof_w,edof_w , bpe_u*dof_u,edof_u , empty,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(edof_w);CHKERRQ(ierr);
    ierr = PetscFree(edof_u);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }

  *_A = A;
  
  ierr = PetscFree(nnz);CHKERRQ(ierr);
  ierr = PetscFree(nnz_block);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic_MPI"
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic_MPI(DM dm_w,DM dm_u,Mat *_A)
{
  DM_TET *tet_w = (DM_TET*)dm_w->data;
  DM_TET *tet_u = (DM_TET*)dm_u->data;
  PetscInt dof_w,dof_u,e,b,k;
  Mat A;
  MatType type;
  PetscInt nel,bpe_w,*element_w,bpe_u,*element_u,nnodes_w,nnodes_u;
  ISLocalToGlobalMapping r_mapping,c_mapping;
  PetscLogDouble t0,t1,time,time_min,time_max;
  PetscErrorCode ierr;
  

  ierr = DMTetGetDof(dm_w,&dof_w);CHKERRQ(ierr);
  ierr = DMTetGetDof(dm_u,&dof_u);CHKERRQ(ierr);
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm_w),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,tet_w->mpi->nnodes_g*dof_w,tet_u->mpi->nnodes_g*dof_u,
                       tet_w->mpi->nnodes*dof_w,  tet_u->mpi->nnodes*dof_u);CHKERRQ(ierr);
  ierr = DMGetMatType(dm_w,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm_w),dof_w,tet_w->mpi->nnodes_l,(const PetscInt*)tet_w->mpi->l2g,PETSC_COPY_VALUES,&r_mapping);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm_u),dof_u,tet_u->mpi->nnodes_l,(const PetscInt*)tet_u->mpi->l2g,PETSC_COPY_VALUES,&c_mapping);CHKERRQ(ierr);
  ierr = MatSetLocalToGlobalMapping(A,r_mapping,c_mapping);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingDestroy(&r_mapping);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingDestroy(&c_mapping);CHKERRQ(ierr);
  
  ierr = MatSetUp(A);CHKERRQ(ierr);

  /* determine preallocation */
  ierr = DMTetSpaceElement(dm_w,&nel,&bpe_w,&element_w,NULL,&nnodes_w,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_u,&nel,&bpe_u,&element_u,NULL,&nnodes_u,NULL);CHKERRQ(ierr);
  
  /* set preallocation */
  PetscTime(&t0);
  ierr = FARCMatPreallocation(A,dm_w,dm_u);CHKERRQ(ierr);
  PetscTime(&t1);
  time = t1 - t0;
  ierr = MPI_Allreduce(&time,&time_min,1,MPIU_PETSCLOGDOUBLE,MPI_MIN,PetscObjectComm((PetscObject)dm_w));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&time,&time_max,1,MPIU_PETSCLOGDOUBLE,MPI_MAX,PetscObjectComm((PetscObject)dm_w));CHKERRQ(ierr);
  PetscPrintf(PetscObjectComm((PetscObject)dm_w),"DMTet: MPIAIJ preallocation %+1.2e / %+1.2e min/max (sec) \n",time_min,time_max);
  
  //ierr = MatSetOption(A,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);CHKERRQ(ierr);
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt *edof_w,*edof_u;
    
    ierr = PetscMalloc1(bpe_w*dof_w,&edof_w);CHKERRQ(ierr);
    ierr = PetscMalloc1(bpe_u*dof_u,&edof_u);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(bpe_w*bpe_u*dof_w*dof_u,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,bpe_w*bpe_u*dof_w*dof_u*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nel; e++) {
      PetscInt *enodes_w;
      PetscInt *enodes_u;
      
      enodes_w = &element_w[bpe_w*e];
      for (k=0; k<bpe_w; k++) {
        for (b=0; b<dof_w; b++) {
          edof_w[dof_w*k+b] = dof_w * enodes_w[k] + b;
        }
      }
      
      enodes_u = &element_u[bpe_u*e];
      for (k=0; k<bpe_u; k++) {
        for (b=0; b<dof_u; b++) {
          edof_u[dof_u*k+b] = dof_u * enodes_u[k] + b;
        }
      }
      
      ierr = MatSetValuesLocal(A, bpe_w*dof_w,edof_w , bpe_u*dof_u,edof_u , empty,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatSetOption(A,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
    
    ierr = PetscFree(edof_w);CHKERRQ(ierr);
    ierr = PetscFree(edof_u);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }

  *_A = A;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic"
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic(DM dm_w,DM dm_u,Mat *_A)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize;
  
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm_w),&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic_SEQ(dm_w,dm_u,_A);CHKERRQ(ierr);
  } else {
    ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic_MPI(dm_w,dm_u,_A);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_Mixed_Pk1_Pk2"
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_Pk2(DM dm_w,DM dm_u,Mat *_A)
{
  PetscErrorCode ierr;
  //SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Mixed space for Pk x Pk not implemented");
  ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic(dm_w,dm_u,_A);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_Mixed_Pk1_disc_Pk2"
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_disc_Pk2(DM dm_w,DM dm_u,Mat *_A)
{
  PetscErrorCode ierr;
  ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic(dm_w,dm_u,_A);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_Mixed_Pk1_Pk2_disc"
PetscErrorCode DMTetCreateMatrix_Mixed_Pk1_Pk2_disc(DM dm_w,DM dm_u,Mat *_A)
{
  PetscErrorCode ierr;
  ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic(dm_w,dm_u,_A);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMixedSpaceMatrix"
PetscErrorCode DMTetCreateMixedSpaceMatrix(DM dm_w,DM dm_u,Mat *A)
{
  DMTetBasisType bt_1,bt_2;
  PetscErrorCode ierr;
  
  ierr = DMTetGetBasisType(dm_w,&bt_1);CHKERRQ(ierr);
  ierr = DMTetGetBasisType(dm_u,&bt_2);CHKERRQ(ierr);
  if ((bt_1 == DMTET_CWARP_AND_BLEND) && (bt_2 == DMTET_DG)) {
    ierr = DMTetCreateMatrix_Mixed_Pk1_Pk2_disc(dm_w,dm_u,A);CHKERRQ(ierr);
  } else   if ((bt_1 == DMTET_DG) && (bt_2 == DMTET_CWARP_AND_BLEND)) {
    ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2(dm_w,dm_u,A);CHKERRQ(ierr);
  } else   if ((bt_1 == DMTET_CWARP_AND_BLEND) && (bt_2 == DMTET_CWARP_AND_BLEND)) {
    ierr = DMTetCreateMatrix_Mixed_Pk1_Pk2(dm_w,dm_u,A);CHKERRQ(ierr);
  } else   if ((bt_1 == DMTET_DG) && (bt_2 == DMTET_DG)) {
    ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic(dm_w,dm_u,A);CHKERRQ(ierr);
  } else {
    SETERRQ2(PetscObjectComm((PetscObject)dm_w),PETSC_ERR_SUP,"No support for mixed matrix using %s x %s",DMTetBasisTypeNames[(int)bt_1],DMTetBasisTypeNames[(int)bt_2]);
  }
  
  PetscFunctionReturn(0);
}


#if 0
#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrixDiscontinuousSpace_MPI"
PetscErrorCode DMTetCreateMatrixDiscontinuousSpace_MPI(DM dm_w,Mat *A)
{
  PetscErrorCode ierr;
  ierr = DMTetCreateMatrix_Mixed_Pk1_disc_Pk2_generic(dm_w,dm_w,A);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
#endif


#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrixDiscontinuousSpace_MPI"
PetscErrorCode DMTetCreateMatrixDiscontinuousSpace_MPI(DM dm,Mat *_A)
{
  DM_TET *tet = (DM_TET*)dm->data;
  Mat      A;
  PetscInt nen,bpe,dof;
  MatType type;
  PetscErrorCode ierr;
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,&bpe,0,0,0,0); CHKERRQ(ierr);
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,nen*bpe*dof,nen*bpe*dof,tet->mpi->nnodes*dof,tet->mpi->nnodes*dof);CHKERRQ(ierr);
  ierr = DMGetMatType(dm,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = MatSetLocalToGlobalMapping(A,dm->ltogmap,dm->ltogmap);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  ierr = MatMPIAIJSetPreallocation(A,bpe*dof,0,bpe*dof,0);CHKERRQ(ierr);
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt  e,k,b,*edof,*element;
    
    ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = PetscMalloc1(bpe*dof,&edof);CHKERRQ(ierr);
    ierr = PetscMalloc1(bpe*bpe*dof*dof,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,bpe*bpe*dof*dof*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nen; e++) {
      PetscInt *enodes;
      
      enodes = &element[bpe*e];
      for (k=0; k<bpe; k++) {
        for (b=0; b<dof; b++) {
          edof[dof*k+b] = dof * enodes[k] + b;
        }
      }
      ierr = MatSetValuesLocal(A,bpe*dof,edof,bpe*dof,edof,empty,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(edof);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }
  
  *_A = A;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrixDiscontinuousSpace_SEQ"
PetscErrorCode DMTetCreateMatrixDiscontinuousSpace_SEQ(DM dm,Mat *_A)
{
  Mat      A;
  PetscInt nen,bpe,dof;
  MatType type;
  PetscErrorCode ierr;
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,&bpe,0,0,0,0); CHKERRQ(ierr);
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,nen*bpe*dof,nen*bpe*dof);CHKERRQ(ierr);
  ierr = DMGetMatType(dm,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = MatSetLocalToGlobalMapping(A,dm->ltogmap,dm->ltogmap);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);

  ierr = MatSeqAIJSetPreallocation(A,bpe*dof,0);CHKERRQ(ierr);
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt  e,k,b,*edof,*element;
    
    ierr = DMTetSpaceElement(dm,&nen,&bpe,&element,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = PetscMalloc1(bpe*dof,&edof);CHKERRQ(ierr);
    ierr = PetscMalloc1(bpe*bpe*dof*dof,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,bpe*bpe*dof*dof*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nen; e++) {
      PetscInt *enodes;
      
      enodes = &element[bpe*e];
      for (k=0; k<bpe; k++) {
        for (b=0; b<dof; b++) {
          edof[dof*k+b] = dof * enodes[k] + b;
        }
      }
      ierr = MatSetValues(A,bpe*dof,edof,bpe*dof,edof,empty,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(edof);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }
  
  *_A = A;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrixContinuousSpace_SEQ"
PetscErrorCode DMTetCreateMatrixContinuousSpace_SEQ(DM dm,Mat *_A)
{
  Mat A;
  PetscInt e,k,j,jj,b,nnodes,dof,npe,nelements;
  MatType type;
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt *nnz,*nnz_block,**nnmap,*element;
  PetscErrorCode ierr;
  
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,0,&element,&npe,&nnodes,0);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nnodes,&nnz);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes*dof,&nnz_block);CHKERRQ(ierr);
  for (k=0; k<nnodes; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(nnodes,&nnmap);CHKERRQ(ierr);
  for (k=0; k<nnodes; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  for (e=0; e<nelements; e++) {
    PetscInt *enodes;
    PetscInt nj,njj;
    
    enodes = &element[npe*e];
    
    for (j=0; j<npe; j++) {
      nj = enodes[j];
      
      for (jj=0; jj<npe; jj++) {
        PetscBool exists = PETSC_FALSE;
        
        njj = enodes[jj];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[nj]; k++) {
          if (nnmap[nj][k] == njj) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[nj];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[nj]+1));
          nnmap[nj] = tmp;
          nnmap[nj][ nnz[nj] ] = njj;
          nnz[nj]++;
        }
        
      }
      
    }
  }
  for (k=0; k<nnodes; k++) {
    for (b=0; b<dof; b++) {
      nnz_block[dof*k + b] = dof * nnz[k];
    }
  }
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,tet->space->nactivedof*dof,tet->space->nactivedof*dof);CHKERRQ(ierr);
  ierr = DMGetMatType(dm,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = MatSetLocalToGlobalMapping(A,dm->ltogmap,dm->ltogmap);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  ierr = MatSeqAIJSetPreallocation(A,0,nnz_block);CHKERRQ(ierr);
  
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt *edof;
    
    ierr = PetscMalloc1(npe*dof,&edof);CHKERRQ(ierr);
    ierr = PetscMalloc1(npe*npe*dof*dof,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,npe*npe*dof*dof*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nelements; e++) {
      PetscInt *enodes;
      
      enodes = &element[npe*e];
      for (k=0; k<npe; k++) {
        for (b=0; b<dof; b++) {
          edof[dof*k+b] = dof * enodes[k] + b;
        }
      }
      ierr = MatSetValues(A,npe*dof,edof,npe*dof,edof,empty,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(edof);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }
  
  *_A = A;
  
  ierr = PetscFree(nnz);CHKERRQ(ierr);
  ierr = PetscFree(nnz_block);CHKERRQ(ierr);
  for (k=0; k<nnodes; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetCreateMatrixRTSpace_RT0"
PetscErrorCode _DMTetCreateMatrixRTSpace_RT0(DM dm,Mat *_A)
{
  DM_TET *tet = (DM_TET*)dm->data;
  Mat A;
  PetscInt e,k,j,jj,nspacedof,dof,bpe,nelements;
  MatType type;
  PetscInt *nnz,**nnmap,*element,*edof;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,&bpe,&element,NULL,NULL,0);CHKERRQ(ierr);
  nspacedof = tet->space->nactivedof;
  
  ierr = PetscMalloc1(bpe,&edof);CHKERRQ(ierr);
  ierr = PetscMalloc1(nspacedof,&nnz);CHKERRQ(ierr);
  for (k=0; k<nspacedof; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(nspacedof,&nnmap);CHKERRQ(ierr);
  for (k=0; k<nspacedof; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  for (e=0; e<nelements; e++) {
    PetscInt *edof_ref;
    PetscInt nj,njj;
    
    edof_ref = &element[bpe*e];
    for (j=0; j<bpe; j++) {
      edof[j] = edof_ref[j];
      //if (edof[j] < 0) {
      //  edof[j] = -edof_ref[j] - 1;
      //}
      if (edof[j] < 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"negative DOF not allowed");
    }
    
    for (j=0; j<bpe; j++) {
      nj = edof[j];
      
      for (jj=0; jj<bpe; jj++) {
        PetscBool exists = PETSC_FALSE;
        
        njj = edof[jj];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[nj]; k++) {
          if (nnmap[nj][k] == njj) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[nj];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[nj]+1));
          nnmap[nj] = tmp;
          nnmap[nj][ nnz[nj] ] = njj;
          nnz[nj]++;
        }
        
      }
      
    }
  }
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,nspacedof,nspacedof);CHKERRQ(ierr);
  ierr = DMGetMatType(dm,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = MatSeqAIJSetPreallocation(A,0,nnz);CHKERRQ(ierr);
  
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    
    ierr = PetscMalloc1(bpe*bpe,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,bpe*bpe*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nelements; e++) {
      PetscInt *edof_ref;
      
      edof_ref = &element[bpe*e];
      for (j=0; j<bpe; j++) {
        edof[j] = edof_ref[j];
        //if (edof[j] < 0) { /* not needed anymore */
        //  edof[j] = -edof_ref[j] - 1;
        //}
        if (edof[j] < 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"negative DOF not allowed");
      }
      ierr = MatSetValues(A,bpe,edof,bpe,edof,empty,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }
  
  *_A = A;
  
  ierr = PetscFree(edof);CHKERRQ(ierr);
  ierr = PetscFree(nnz);CHKERRQ(ierr);
  for (k=0; k<nspacedof; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrixRTSpace_SEQ"
PetscErrorCode DMTetCreateMatrixRTSpace_SEQ(DM dm,Mat *_A)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  switch (tet->basis_order) {
    case 0:
      ierr = _DMTetCreateMatrixRTSpace_RT0(dm,_A);CHKERRQ(ierr);
      break;
      
    default:
      break;
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrixContinuousSpace_MPI"
PetscErrorCode DMTetCreateMatrixContinuousSpace_MPI(DM dm,Mat *_A)
{
  Mat A;
  PetscInt e,i,k,j,jj,b,nnodes,dof,npe,nelements;
  MatType type;
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt *nnz,*nnz_d,*nnz_od,**nnmap,*element,start,end,cnt;
  ISLocalToGlobalMapping mapping;
  PetscErrorCode ierr;
  
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,0,&element,&npe,&nnodes,0);CHKERRQ(ierr);

  ierr = PetscMalloc1(nnodes,&nnz_d);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes,&nnz_od);CHKERRQ(ierr);
  for (k=0; k<nnodes; k++) {
    nnz_d[k] = 0;
    nnz_od[k] = 0;
  }

  ierr = PetscMalloc1(nnodes+tet->space->nghost_nodes,&nnz);CHKERRQ(ierr);
  for (k=0; k<nnodes+tet->space->nghost_nodes; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(nnodes+tet->space->nghost_nodes,&nnmap);CHKERRQ(ierr);
  for (k=0; k<nnodes+tet->space->nghost_nodes; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  for (e=0; e<nelements+tet->space->nghost_elements; e++) {
    PetscInt *enodes;
    PetscInt nj,njj;
    
    enodes = &element[npe*e];
    
    for (j=0; j<npe; j++) {
      nj = enodes[j];
      
      for (jj=0; jj<npe; jj++) {
        PetscBool exists = PETSC_FALSE;
        
        njj = enodes[jj];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[nj]; k++) {
          if (nnmap[nj][k] == njj) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[nj];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[nj]+1));
          nnmap[nj] = tmp;
          nnmap[nj][ nnz[nj] ] = njj;
          nnz[nj]++;
        }
      }
    }
  }
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,tet->mpi->nnodes_g*dof,tet->mpi->nnodes_g*dof,tet->mpi->nnodes*dof,tet->mpi->nnodes*dof);CHKERRQ(ierr);
  ierr = DMGetMatType(dm,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm),dof,tet->mpi->nnodes_l,(const PetscInt*)tet->mpi->l2g,PETSC_COPY_VALUES,&mapping);CHKERRQ(ierr);
  ierr = MatSetLocalToGlobalMapping(A,mapping,mapping);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingDestroy(&mapping);CHKERRQ(ierr);
  
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  ierr = MatGetOwnershipRange(A,&start,&end);CHKERRQ(ierr);
  start = start / dof;
  end   = end / dof;

  /*
  PetscMPIInt commrank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&commrank);
  
  PetscPrintf(PETSC_COMM_SELF,"[r%d] start %d -- end %d\n",commrank,start,end);
  PetscPrintf(PETSC_COMM_SELF,"[r%d] mpi->nelements %d\n",commrank,tet->mpi->nelements);
  PetscPrintf(PETSC_COMM_SELF,"[r%d] mpi->nelements_g %d\n",commrank,tet->mpi->nelements_g);
  PetscPrintf(PETSC_COMM_SELF,"[r%d] mpi->nnodes %d\n",commrank,tet->mpi->nnodes);
  PetscPrintf(PETSC_COMM_SELF,"[r%d] mpi->nnodes_l %d\n",commrank,tet->mpi->nnodes_l);
  PetscPrintf(PETSC_COMM_SELF,"[r%d] mpi->nnodes_g %d\n",commrank,tet->mpi->nnodes_g);
  if (commrank == 0) {
    for (i=0; i<nnodes; i++) {
      PetscInt gidx,gidx_j,kk;
      
      gidx = tet->mpi->l2g[i];
      PetscPrintf(PETSC_COMM_SELF,"[r%d] map %d (l) to %d (g)\n",commrank,i,gidx);
      
      for (kk=0; kk<nnz[i]; kk++) {
        PetscInt lidx_j;
        
        lidx_j = nnmap[i][kk];
        if (lidx_j < nnodes) {
          gidx_j = tet->mpi->l2g[ lidx_j ];
          PetscPrintf(PETSC_COMM_SELF,"[r%d]   [%d] touches %d (l) to %d (g)\n",commrank,kk,lidx_j,gidx_j);
        } else {
          PetscPrintf(PETSC_COMM_SELF,"[r%d]   [%d] touches %d (l) [assoc. ghost element]\n",commrank,kk,lidx_j);
        }
      }

    }
  }
  */
   
  cnt = 0;
  for (i=0; i<nnodes; i++) {
    PetscInt gidx,gidx_j,kk;
    
    gidx = tet->mpi->l2g[i];
    
    if (gidx >= start && gidx < end) {

      for (kk=0; kk<nnz[i]; kk++) {
        PetscInt lidx_j;
        
        lidx_j = nnmap[i][kk];
        
        if (lidx_j < nnodes) { /* if its inside the local space we can do a local-to-global conversion */
          gidx_j = tet->mpi->l2g[ lidx_j ];

          if (gidx_j >= start && gidx_j < end) {
            nnz_d[cnt]++;
          } else {
            nnz_od[cnt]++;
          }
        } else { /* otherwise we have a basis associated with a ghost element and we know it must be on the off diagonal */
          nnz_od[cnt]++;
        }
      }
      /*
      if (commrank == 0) {
        PetscPrintf(PETSC_COMM_SELF,"[r%d] map %d (l) to %d (g) --> nnz %d %d \n",commrank,i,gidx,nnz_d[cnt],nnz_od[cnt]);
      }
      */
      
      cnt++;
    }
  }

  /* convert to block size != 1 */
  if (dof != 1) {
    PetscInt d,*tmp_d,*tmp_od;
    
    tmp_d  = nnz_d;
    tmp_od = nnz_od;
    
    ierr = PetscMalloc1(nnodes*dof,&nnz_d);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodes*dof,&nnz_od);CHKERRQ(ierr);
    for (k=0; k<nnodes; k++) {
      for (d=0; d<dof; d++) {
        nnz_d[dof*k+d]  = dof * tmp_d[k];
        nnz_od[dof*k+d] = dof * tmp_od[k];
      }
    }

    ierr = PetscFree(tmp_d);CHKERRQ(ierr);
    ierr = PetscFree(tmp_od);CHKERRQ(ierr);
  }
  
  ierr = MatMPIAIJSetPreallocation(A,0,nnz_d,0,nnz_od);CHKERRQ(ierr);

  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt *edof;
    
    ierr = PetscMalloc1(npe*dof,&edof);CHKERRQ(ierr);
    ierr = PetscMalloc1(npe*npe*dof*dof,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,npe*npe*dof*dof*sizeof(PetscReal));CHKERRQ(ierr);
    
      for (e=0; e<nelements; e++) {
        PetscInt *enodes;
        
        enodes = &element[npe*e];
        /*
        for (k=0; k<npe; k++) {
          PetscInt gidx_k = tet->mpi->l2g[ enodes[k] ];
          
          for (b=0; b<dof; b++) {
            edof[dof*k+b] = dof * gidx_k + b;
          }
        }
        ierr = MatSetValues(A,npe*dof,edof,npe*dof,edof,empty,INSERT_VALUES);CHKERRQ(ierr);
        */
        for (k=0; k<npe; k++) {
          
          for (b=0; b<dof; b++) {
            edof[dof*k+b] = dof * enodes[k] + b;
          }
        }
        ierr = MatSetValuesLocal(A,npe*dof,edof,npe*dof,edof,empty,INSERT_VALUES);CHKERRQ(ierr);
      }
    
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(edof);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }
  
  *_A = A;
  
  ierr = PetscFree(nnz_d);CHKERRQ(ierr);
  ierr = PetscFree(nnz_od);CHKERRQ(ierr);

  ierr = PetscFree(nnz);CHKERRQ(ierr);
  for (k=0; k<nnodes+tet->space->nghost_nodes; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_ScalarCoupling_SEQ"
PetscErrorCode DMTetCreateMatrix_ScalarCoupling_SEQ(DM dm_w,Mat *_A)
{
  DM_TET *tet_w = (DM_TET*)dm_w->data;
  PetscInt dof_w;
  PetscInt e,k,b,j,jj;
  Mat A;
  MatType type;
  PetscInt *nnz,*nnz_block,**nnmap;
  PetscInt nel,bpe_w,*element_w;
  PetscErrorCode ierr;
  
  ierr = DMTetGetDof(dm_w,&dof_w);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm_w,&nel,&bpe_w,&element_w,NULL,NULL,NULL);CHKERRQ(ierr);
  
  /* compute non-zero counts */
  ierr = PetscMalloc1(tet_w->space->nactivedof,&nnz);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(tet_w->space->nactivedof,&nnmap);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  for (e=0; e<nel; e++) {
    PetscInt *enodes_w;
    PetscInt nj,njj;
    
    enodes_w = &element_w[bpe_w*e];
    
    for (j=0; j<bpe_w; j++) {
      nj = enodes_w[j];
      
      for (jj=0; jj<bpe_w; jj++) {
        PetscBool exists = PETSC_FALSE;
        
        njj = enodes_w[jj];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[nj]; k++) {
          if (nnmap[nj][k] == njj) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[nj];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[nj]+1));
          nnmap[nj] = tmp;
          nnmap[nj][ nnz[nj] ] = njj;
          nnz[nj]++;
        }
      }
    }
  }
  
  /* common */
  ierr = PetscMalloc1(tet_w->space->nactivedof*dof_w,&nnz_block);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    for (b=0; b<dof_w; b++) {
      nnz_block[dof_w*k + b] = nnz[k];
    }
  }
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm_w),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,PETSC_DECIDE,PETSC_DECIDE,
                     tet_w->space->nactivedof*dof_w,
                     tet_w->space->nactivedof*dof_w);CHKERRQ(ierr);
  ierr = DMGetMatType(dm_w,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = MatSetLocalToGlobalMapping(A,dm_w->ltogmap,dm_w->ltogmap);CHKERRQ(ierr);
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  ierr = MatSeqAIJSetPreallocation(A,0,nnz_block);CHKERRQ(ierr);
  
  /* force zero entries into nnz slots */
  {
    PetscReal *empty;
    PetscInt *edof_w;
    
    ierr = PetscMalloc1(bpe_w*dof_w,&edof_w);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(bpe_w*bpe_w,&empty);CHKERRQ(ierr);
    ierr = PetscMemzero(empty,bpe_w*bpe_w*sizeof(PetscReal));CHKERRQ(ierr);
    
    for (e=0; e<nel; e++) {
      PetscInt *enodes_w;
      
      enodes_w = &element_w[bpe_w*e];
      for (b=0; b<dof_w; b++) {

        for (k=0; k<bpe_w; k++) {
          edof_w[k] = dof_w * enodes_w[k] + b;
        }
        ierr = MatSetValues(A, bpe_w,edof_w , bpe_w,edof_w , empty,INSERT_VALUES);CHKERRQ(ierr);
      }
    }
    ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = PetscFree(edof_w);CHKERRQ(ierr);
    ierr = PetscFree(empty);CHKERRQ(ierr);
  }
  
  *_A = A;
  
  ierr = PetscFree(nnz);CHKERRQ(ierr);
  ierr = PetscFree(nnz_block);CHKERRQ(ierr);
  for (k=0; k<tet_w->space->nactivedof; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_ScalarCoupling_MPI"
PetscErrorCode DMTetCreateMatrix_ScalarCoupling_MPI(DM dm_w,Mat *_A)
{
  DM_TET *tet_w = (DM_TET*)dm_w->data;
  PetscInt dof_w,e,b,k;
  Mat A;
  MatType type;
  PetscInt nel,bpe_w,*element_w,nnodes_w;
  ISLocalToGlobalMapping r_mapping;
  PetscLogDouble t0,t1,time,time_min,time_max;
  PetscErrorCode ierr;
  
  
  ierr = DMTetGetDof(dm_w,&dof_w);CHKERRQ(ierr);
  
  ierr = MatCreate(PetscObjectComm((PetscObject)dm_w),&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,tet_w->mpi->nnodes_g*dof_w,tet_w->mpi->nnodes_g*dof_w,
                     tet_w->mpi->nnodes*dof_w,  tet_w->mpi->nnodes*dof_w);CHKERRQ(ierr);
  ierr = DMGetMatType(dm_w,&type);CHKERRQ(ierr);
  ierr = MatSetType(A,type);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);
  
  ierr = ISLocalToGlobalMappingCreate(PetscObjectComm((PetscObject)dm_w),dof_w,tet_w->mpi->nnodes_l,(const PetscInt*)tet_w->mpi->l2g,PETSC_COPY_VALUES,&r_mapping);CHKERRQ(ierr);
  ierr = MatSetLocalToGlobalMapping(A,r_mapping,r_mapping);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingDestroy(&r_mapping);CHKERRQ(ierr);
  
  ierr = MatSetUp(A);CHKERRQ(ierr);
  
  /* determine preallocation */
  ierr = DMTetSpaceElement(dm_w,&nel,&bpe_w,&element_w,NULL,&nnodes_w,NULL);CHKERRQ(ierr);
  
  /* set preallocation */
  PetscTime(&t0);
  ierr = FARCMatPreallocationInit(A);CHKERRQ(ierr);
  /* mesh traversal */
  {
    PetscInt *edof_w;
    
    ierr = PetscMalloc1(bpe_w,&edof_w);CHKERRQ(ierr);
    for (e=0; e<nel; e++) {
      PetscInt *enodes_w;
      
      enodes_w = &element_w[bpe_w*e];
      for (b=0; b<dof_w; b++) {
        for (k=0; k<bpe_w; k++) {
          edof_w[k] = dof_w * enodes_w[k] + b;
        }
        ierr = FARCMatPreallocationSetValuesLocal(A,bpe_w,edof_w,bpe_w,edof_w);CHKERRQ(ierr);
      }
    }
    ierr = PetscFree(edof_w);CHKERRQ(ierr);
  }
  ierr = FARCMatPreallocationFinalize(A);CHKERRQ(ierr);
  PetscTime(&t1);
  time = t1 - t0;
  ierr = MPI_Allreduce(&time,&time_min,1,MPIU_PETSCLOGDOUBLE,MPI_MIN,PetscObjectComm((PetscObject)dm_w));CHKERRQ(ierr);
  ierr = MPI_Allreduce(&time,&time_max,1,MPIU_PETSCLOGDOUBLE,MPI_MAX,PetscObjectComm((PetscObject)dm_w));CHKERRQ(ierr);
  PetscPrintf(PetscObjectComm((PetscObject)dm_w),"DMTet: MPIAIJ preallocation %+1.2e / %+1.2e min/max (sec) \n",time_min,time_max);
  
  *_A = A;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateMatrix_ScalarCoupling"
PetscErrorCode DMTetCreateMatrix_ScalarCoupling(DM dm_w,Mat *_A)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize;
  
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm_w),&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = DMTetCreateMatrix_ScalarCoupling_SEQ(dm_w,_A);CHKERRQ(ierr);
  } else {
    ierr = DMTetCreateMatrix_ScalarCoupling_MPI(dm_w,_A);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}


//
// farc_table_search() performs a brute force table lookup
//   - nlocal x dof is the size of the table.
//   - l2g[] is of size nlocal and maps the local indices to global indices.
//   - len is the total number of entries to look up.
//   - g_indices[] is the array of len entries we wish to find.
//   + values[] is of size len and holds the index i assocated with a table lookup such as table[i][].
//     For some value g_indices[x], let gidx = g_indices[x].
//     The result of dof * l2g[i] + b = g_idx, where i is the index which can be used to address the table.
//     In the even that the g_indices[] cannot be matched, a -1 is inserted into values[].
//
#undef __FUNCT__
#define __FUNCT__ "farc_table_search"
PetscErrorCode farc_table_search(PetscInt nlocal,const PetscInt l2g[],PetscInt dof,PetscInt len,const PetscInt g_indices[],PetscInt values[])
{
  PetscInt ii,i,b;
  
  for (ii=0; ii<len; ii++) {
    PetscInt lookup_value = g_indices[ii];
    PetscInt found = 0;
    
    values[ii] = -1;

    for (i=0; i<nlocal; i++) {
      PetscInt l2g_i = l2g[i];

      for (b=0; b<dof; b++) {
        PetscInt l2g_dof = dof * l2g_i + b;
        
        if (lookup_value == l2g_dof) {
          values[ii] = dof * i + b;
          goto __done_search;
        }
      }
    }
    if (found == 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Table lookup failed");
    
  __done_search: found = 1;
    
  }
  PetscFunctionReturn(0);
}

//
// Flexible,
// Algebraic,
// Robust,
// Computation
//   of Matix Preallocation
//
// Also the word frequently spoken when folks have to
// preallocate non-trival FE operators in parallel
//
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocation_v0"
PetscErrorCode FARCMatPreallocation_v0(Mat A,DM dmw,DM dmu)
{
  DM_TET *tet_w = (DM_TET*)dmw->data;
  DM_TET *tet_u = (DM_TET*)dmu->data;
  PetscInt dof_w,dof_u,nel,bpe_w,bpe_u,*element_w,*element_u,nnodes_w,nnodes_u;
  PetscInt i,j,k,e,b,ni,nj;
  PetscInt *buffer; /* [dof_u*bpe_u + 1] */
  const PetscInt *row_ranges,*col_ranges;
  PetscInt row_start,row_end,col_start,col_end;
  PetscMPIInt commsize,commrank;
  PetscMPIInt *colrank;
  PetscInt *dnnz,*onnz;
  PetscInt len,*nnz,**nnmap;
  PetscInt *local_w_dof,*local_u_dof;
  DataEx de;
  PetscInt *recv_buffer;
  PetscInt recv_length;
  PetscInt *table_location;
  MPI_Comm comm;
  PetscErrorCode ierr;
  
  
  comm = PetscObjectComm((PetscObject)A);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);

  ierr = MatGetOwnershipRange(A,&row_start,&row_end);CHKERRQ(ierr);
  ierr = MatGetOwnershipRanges(A,&row_ranges);CHKERRQ(ierr);

  ierr = MatGetOwnershipRangeColumn(A,&col_start,&col_end);CHKERRQ(ierr);
  ierr = MatGetOwnershipRangesColumn(A,&col_ranges);CHKERRQ(ierr);
  
  ierr = DMTetGetDof(dmw,&dof_w);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmu,&dof_u);CHKERRQ(ierr);

  ierr = DMTetSpaceElement(dmw,&nel,&bpe_w,&element_w,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmu,&nel,&bpe_u,&element_u,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceGetSubdomainSize(dmw,&nnodes_w,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceGetSubdomainSize(dmu,&nnodes_u,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nnodes_u*dof_u,&colrank);CHKERRQ(ierr);
  for (j=0; j<nnodes_u*dof_u; j++) {
    colrank[j] = commrank;
  }
  
  /* convert column space DOFS into rank */
  for (j=0; j<nnodes_u; j++) {
    PetscInt nj_g,nj_gdof;
    PetscBool requires_search;
    
    nj_g = tet_u->mpi->l2g[j];
    
    for (b=0; b<dof_u; b++) {
      requires_search = PETSC_TRUE;
      nj_gdof = dof_u * nj_g + b;

      /* assume local owenership */
      if ((nj_gdof >= col_start) && (nj_gdof < col_end)) {
        colrank[dof_u*j + b] = commrank;
        requires_search = PETSC_FALSE;
      }
      
      if (requires_search) { /* find rank */
        for (k=0; k<commsize; k++) {
          if ((nj_gdof >= col_ranges[k]) && (nj_gdof < col_ranges[k+1])) {
            colrank[dof_u*j + b] = (PetscMPIInt)k;
            break;
          }
        }
      }
    }
    
  }

  //////////////////////////////////////////////
  /* Build the table for the local sub-domain */
  //////////////////////////////////////////////
  ierr = PetscMalloc1(bpe_w*dof_w,&local_w_dof);CHKERRQ(ierr);
  ierr = PetscMalloc1(bpe_u*dof_u,&local_u_dof);CHKERRQ(ierr);
  
  len = dof_w * (nnodes_w + 0);
  ierr = PetscMalloc1(len,&nnz);CHKERRQ(ierr);
  for (k=0; k<len; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(len,&nnmap);CHKERRQ(ierr);
  for (k=0; k<len; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  for (e=0; e<nel + 0; e++) {
    PetscInt *enodes_w,*enodes_u;
    
    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    /* get local dof w and u */
    for (i=0; i<bpe_w; i++) {
      ni = enodes_w[i];
      for (b=0; b<dof_w; b++) {
        local_w_dof[dof_w * i + b] = dof_w * ni + b;
      }
    }

    for (j=0; j<bpe_u; j++) {
      nj = enodes_u[j];
      for (b=0; b<dof_u; b++) {
        local_u_dof[dof_u * j + b] = dof_u * nj + b;
        
        local_u_dof[dof_u * j + b] = dof_u * tet_u->mpi->l2g[nj] + b;
      }
    }
    
    for (i=0; i<bpe_w*dof_w; i++) {
      PetscInt index_i = local_w_dof[i];
      
      for (j=0; j<bpe_u*dof_u; j++) {
        PetscBool exists = PETSC_FALSE;
        PetscInt index_j;
        
        index_j = local_u_dof[j];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[index_i]; k++) {
          if (nnmap[index_i][k] == index_j) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[index_i];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[index_i]+1));
          nnmap[index_i] = tmp;
          nnmap[index_i][ nnz[index_i] ] = index_j;
          nnz[index_i]++;
        }
      }
    }
  }
  
  
  /////////////////////////////////////////////////
  /* Get the table content from off-rank sources */
  /////////////////////////////////////////////////
  
  de = DataExCreate(comm,0);

  // [topology]
  ierr = DataExTopologyInitialize(de);CHKERRQ(ierr);
  /*
  for (j=0; j<nnodes_u*dof_u; j++) {
    PetscMPIInt rank;
    
    rank = colrank[j];
    if (rank != commrank) {
      ierr = DataExTopologyAddNeighbour(de,rank);CHKERRQ(ierr);
    }
  }
  */
  /* This is a hack and simple done because I was lazy */
  for (j=0; j<commsize; j++) {
    PetscMPIInt rank = (PetscMPIInt)j;
    if (rank != commrank) {
      ierr = DataExTopologyAddNeighbour(de,rank);CHKERRQ(ierr);
    }
  }
  ierr = DataExTopologyFinalize(de);CHKERRQ(ierr);

  
  // [send counts]
  ierr = DataExInitializeSendCount(de);CHKERRQ(ierr);
  
  // examine the local table and see if col indices are off-rank
  /*
  for (i=0; i<len; i++) {
    PetscInt len_i;
    
    len_i = nnz[i];
    for (j=0; klen_i<; k++) {
      PetscInt index_j;
      PetscMPIInt rank_index_j;
      
      index_j = nnmap[i][j];
      
      rank_index_j = colrank[index_j];
      
      if (rank_index_j != commrank) {
        ierr = DataExAddToSendCount( de, rank_index_j, 1 );CHKERRQ(ierr);
      }
      
    }
  }
  */
  
  for (e=0; e<nel + 0; e++) {
    PetscInt *enodes_w,*enodes_u;
    PetscBool this_element_needs_to_be_packed;
    PetscInt *indices,rr=0;
    
    ierr = PetscMalloc1((bpe_w*dof_w + bpe_u*dof_u),&indices);CHKERRQ(ierr);

    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    /* get local dof w and u */
    for (i=0; i<bpe_w; i++) {
      ni = enodes_w[i];
      for (b=0; b<dof_w; b++) {
        local_w_dof[dof_w * i + b] = dof_w * ni + b;
        local_w_dof[dof_w * i + b] = dof_w * tet_w->mpi->l2g[ni] + b;
      }
    }

    for (j=0; j<bpe_u; j++) {
      nj = enodes_u[j];
      for (b=0; b<dof_u; b++) {
        local_u_dof[dof_u * j + b] = dof_u * nj + b;
      }
    }
    
    this_element_needs_to_be_packed = PETSC_FALSE;

    for (i=0; i<bpe_w*dof_w; i++) {
      PetscInt    g2l_i;
      PetscMPIInt rank_index_i = commrank;
      
      g2l_i   = local_w_dof[i];
      for (k=0; k<commsize; k++) {
        if ((g2l_i >= row_ranges[k]) && (g2l_i < row_ranges[k+1])) {
          rank_index_i = (PetscMPIInt)k;
          break;
        }
      }
      
      if (rank_index_i != commrank) {
        this_element_needs_to_be_packed = PETSC_TRUE;
        indices[rr] = (PetscInt)rank_index_i;
        rr++;
      }
    }
    
    for (j=0; j<bpe_u*dof_u; j++) {
      PetscInt index_j;
      PetscMPIInt rank_index_j;

      index_j = local_u_dof[j];
      rank_index_j = colrank[index_j];
      if (rank_index_j != commrank) {
        this_element_needs_to_be_packed = PETSC_TRUE;
        indices[rr] = (PetscInt)rank_index_j;
        rr++;
      }
    }
    
    if (this_element_needs_to_be_packed) {
      PetscInt len;
      PetscInt out;
      
      out = rr;
      ierr = PetscSortRemoveDupsInt(&out,indices);CHKERRQ(ierr);
      //printf("[rank %d] local e (%d) needs packing : in %d : out %d\n",commrank,e,in,out);
      len = out;
    
      for (k=0; k<len; k++) {
        PetscMPIInt rank_index_j;

        rank_index_j = (PetscMPIInt)indices[k];
        ierr = DataExAddToSendCount( de, rank_index_j, 1 );CHKERRQ(ierr);
      }
    }
    ierr = PetscFree(indices);CHKERRQ(ierr);
  }

  ierr = DataExFinalizeSendCount(de);CHKERRQ(ierr);

  
  // [pack]
  ierr = PetscMalloc1(bpe_w*dof_w + bpe_u*dof_u,&buffer);CHKERRQ(ierr);

  ierr = DataExPackInitialize(de,sizeof(PetscInt)*(bpe_w*dof_w + bpe_u*dof_u));CHKERRQ(ierr);

  for (e=0; e<nel + 0; e++) {
    PetscInt  *enodes_w,*enodes_u;
    PetscBool cell_requires_packing;
    PetscInt  *indices,rr=0;
    
    ierr = PetscMalloc1((bpe_w*dof_w + bpe_u*dof_u),&indices);CHKERRQ(ierr);
    
    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    /* get local dof w and u */
    for (i=0; i<bpe_w; i++) {
      ni = enodes_w[i];
      for (b=0; b<dof_w; b++) {
        local_w_dof[dof_w * i + b] = dof_w * ni + b;
        /* convert l2g on w */
        local_w_dof[dof_w * i + b] = dof_w * tet_w->mpi->l2g[ni] + b;
      }
    }
    
    for (j=0; j<bpe_u; j++) {
      nj = enodes_u[j];
      for (b=0; b<dof_u; b++) {
        local_u_dof[dof_u * j + b] = dof_u * nj + b;
      }
    }

    cell_requires_packing = PETSC_FALSE;
    
    for (i=0; i<bpe_w*dof_w; i++) {
      PetscInt    g2l_i;
      PetscMPIInt rank_index_i = commrank;
      
      g2l_i   = local_w_dof[i];
      for (k=0; k<commsize; k++) {
        if ((g2l_i >= row_ranges[k]) && (g2l_i < row_ranges[k+1])) {
          rank_index_i = (PetscMPIInt)k;
          break;
        }
      }
      
      if (rank_index_i != commrank) {
        cell_requires_packing = PETSC_TRUE;
        indices[rr] = (PetscInt)rank_index_i;
        rr++;
      }
    }

    for (j=0; j<bpe_u*dof_u; j++) {
      PetscInt index_j;
      PetscMPIInt rank_index_j;
      
      index_j = local_u_dof[j];
      rank_index_j = colrank[index_j];
      if (rank_index_j != commrank) {
        cell_requires_packing = PETSC_TRUE;
        indices[rr] = (PetscInt)rank_index_j;
        rr++;
      }
    }

    if (cell_requires_packing) {
      PetscInt len;
      PetscInt out;
      
      out = rr;
      ierr = PetscSortRemoveDupsInt(&out,indices);CHKERRQ(ierr);
      len = out;
      
      /* convert l2g on u */
      for (j=0; j<bpe_u; j++) {
        nj = enodes_u[j];
        for (b=0; b<dof_u; b++) {
          local_u_dof[dof_u * j + b] = dof_u * tet_u->mpi->l2g[nj] + b;
        }
      }
      
      ierr = PetscMemcpy(&buffer[0],local_w_dof, sizeof(PetscInt) * (bpe_w * dof_w) );CHKERRQ(ierr);
      ierr = PetscMemcpy(&buffer[bpe_w * dof_w],local_u_dof, sizeof(PetscInt) * (bpe_u * dof_u) );CHKERRQ(ierr);

      for (k=0; k<len; k++) {
        PetscMPIInt rank_index_j;
        
        rank_index_j = (PetscMPIInt)indices[k];
        ierr = DataExPackData( de, rank_index_j, 1,(void*)buffer );CHKERRQ(ierr);
      }
      
    }
    ierr = PetscFree(indices);CHKERRQ(ierr);
  }

  ierr = DataExPackFinalize(de);CHKERRQ(ierr);

  // [send-recv]
  ierr = DataExBegin(de);CHKERRQ(ierr);
  ierr = DataExEnd(de);CHKERRQ(ierr);
  
  
  // [un-pack]
  ierr = DataExGetRecvData( de, &recv_length, (void**)&recv_buffer );CHKERRQ(ierr);

  ierr = PetscMalloc1(bpe_w*dof_w,&table_location);CHKERRQ(ierr);
  
  for (e=0; e<recv_length; e++) {
    const PetscInt *this_data;
    const PetscInt *this_data_w,*this_data_u;
    
    this_data   = &recv_buffer[ (bpe_w*dof_w + bpe_u*dof_u)*e ];
    this_data_w = &this_data[0];
    this_data_u = &this_data[bpe_w*dof_w];

    ierr = farc_table_search(nnodes_w,(const PetscInt*)tet_w->mpi->l2g,dof_w, bpe_w*dof_w ,this_data_w, table_location );CHKERRQ(ierr);
    
    for (i=0; i<bpe_w*dof_w; i++) {
      PetscInt index_i;
      
      if (table_location[i] == -1) continue;
      
      index_i = table_location[i];
      
      for (j=0; j<bpe_u*dof_u; j++) {
        PetscBool exists = PETSC_FALSE;
        PetscInt index_j;
        
        index_j = this_data_u[j];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[index_i]; k++) {
          if (nnmap[index_i][k] == index_j) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[index_i];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[index_i]+1));
          nnmap[index_i] = tmp;
          nnmap[index_i][ nnz[index_i] ] = index_j;
          nnz[index_i]++;
        }
      }
      
    }
  }
  
  /*
  if (commrank == 2)
  {
    for (i=0; i<len; i++) {
      PetscInt len_i;
      
      len_i = nnz[i];
      printf("[i %d ] - ",i);
      for (j=0; j<len_i; j++) {
        PetscInt index_j;
        
        index_j = nnmap[i][j];
        printf(" %d : ",index_j);
      } printf("\n");
    }
  }
  */
  
  ierr = PetscMalloc1(nnodes_w*dof_w,&dnnz);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes_w*dof_w,&onnz);CHKERRQ(ierr);
  for (i=0; i<nnodes_w*dof_w; i++) {
    dnnz[i] = 0;
    onnz[i] = 0;
  }
  
  for (i=0; i<nnodes_w; i++) {
    for (b=0; b<dof_w; b++) {
      PetscInt gidx,index_i,localidx;
      
      index_i = dof_w * i + b;
      gidx = dof_w * tet_w->mpi->l2g[i] + b;
      
      // convert global to local
      localidx = gidx - row_start;
      
      if ((gidx >= row_start) && (gidx < row_end)) { /* in the row range */
        PetscInt diag_cnt = 0;
        PetscInt off_diag_cnt = 0;
        
        for (j=0; j<nnz[index_i]; j++) {
          PetscInt gidx_j;
        
          gidx_j = nnmap[index_i][j];
          if ((gidx_j >= col_start) && (gidx_j < col_end)) { /* in the col range <diag> */
            diag_cnt++;
          } else {
            off_diag_cnt++;
          }
        }
        
        dnnz[localidx] = diag_cnt;
        onnz[localidx] = off_diag_cnt;
      }
      
    }
  }
  
  ierr = MatMPIAIJSetPreallocation(A,0,dnnz,0,onnz);CHKERRQ(ierr);
  
  ierr = PetscFree(dnnz);CHKERRQ(ierr);
  ierr = PetscFree(onnz);CHKERRQ(ierr);
  ierr = PetscFree(table_location);CHKERRQ(ierr);
  ierr = PetscFree(local_w_dof);CHKERRQ(ierr);
  ierr = PetscFree(local_u_dof);CHKERRQ(ierr);
  ierr = PetscFree(colrank);CHKERRQ(ierr);
  ierr = PetscFree(buffer);CHKERRQ(ierr);
  ierr = DataExDestroy(de);CHKERRQ(ierr);

  ierr = PetscFree(nnz);CHKERRQ(ierr);
  for (k=0; k<nnodes_w+0; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 This version is algebraic as possible.
 Simply put, this function uses as little DMTet info as possible.
 It tries to be agnostic wrt the data storage generating the indices.
*/
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocation_v1"
PetscErrorCode FARCMatPreallocation_v1(Mat A,DM dmw,DM dmu)
{
  const PetscInt *row_ranges,*col_ranges;
  PetscInt nrows_w,row_start,row_end,col_start,col_end;
  PetscMPIInt commsize,commrank;
  PetscInt *dnnz,*onnz;
  PetscInt *nnz,**nnmap,len_w,len_u;
  const PetscInt *row_l2g,*col_l2g;
  ISLocalToGlobalMapping row_l2g_map,col_l2g_map;

  PetscInt *buffer; /* [dof_u*bpe_u + 1] */
  PetscInt dof_w,dof_u,nel,bpe_w,bpe_u,*element_w,*element_u;
  PetscInt i,j,k,e,b,ni,nj;
  PetscInt *local_w_dof,*local_u_dof;
  
  DataEx de;
  PetscInt *recv_buffer;
  PetscInt recv_length;
  MPI_Comm comm;
  PetscErrorCode ierr;
  
  
  comm = PetscObjectComm((PetscObject)A);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  
  ierr = MatGetOwnershipRange(A,&row_start,&row_end);CHKERRQ(ierr);
  ierr = MatGetOwnershipRanges(A,&row_ranges);CHKERRQ(ierr);
  
  ierr = MatGetOwnershipRangeColumn(A,&col_start,&col_end);CHKERRQ(ierr);
  ierr = MatGetOwnershipRangesColumn(A,&col_ranges);CHKERRQ(ierr);
  
  nrows_w = row_end - row_start;
  
  ierr = MatGetLocalToGlobalMapping(A,&row_l2g_map,&col_l2g_map);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(row_l2g_map,&row_l2g);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(col_l2g_map,&col_l2g);CHKERRQ(ierr);
  
  //////////////////////////////////////////////
  /* Build the table for the local sub-domain */
  //////////////////////////////////////////////
  
  ierr = PetscMalloc1(nrows_w,&nnz);CHKERRQ(ierr);
  for (k=0; k<nrows_w; k++) {
    nnz[k] = 1;
  }
  
  ierr = PetscMalloc1(nrows_w,&nnmap);CHKERRQ(ierr);
  for (k=0; k<nrows_w; k++) {
    nnmap[k] = malloc(sizeof(PetscInt));
    nnmap[k][0] = k; // node is connected to itself
  }
  
  
  
  ierr = DMTetGetDof(dmw,&dof_w);CHKERRQ(ierr);
  ierr = DMTetGetDof(dmu,&dof_u);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmw,&nel,&bpe_w,&element_w,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmu,&nel,&bpe_u,&element_u,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(bpe_w*dof_w,&local_w_dof);CHKERRQ(ierr);
  ierr = PetscMalloc1(bpe_u*dof_u,&local_u_dof);CHKERRQ(ierr);
  
  len_w = bpe_w*dof_w;
  len_u = bpe_u*dof_u;
  
  for (e=0; e<nel; e++) {
    PetscInt *enodes_w,*enodes_u;
    
    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    /* get local dof w and u */
    for (i=0; i<bpe_w; i++) {
      ni = enodes_w[i];
      for (b=0; b<dof_w; b++) {
        local_w_dof[dof_w * i + b] = dof_w * ni + b;
      }
    }
    
    for (j=0; j<bpe_u; j++) {
      nj = enodes_u[j];
      for (b=0; b<dof_u; b++) {
        local_u_dof[dof_u * j + b] = dof_u * nj + b;
      }
    }

    /* convert w */
    for (i=0; i<len_w; i++) {
      PetscInt lidx,gidx;
      
      lidx = local_w_dof[i];
      gidx = row_l2g[lidx];
      
      local_w_dof[i] = gidx;
    }

    /* convert u */
    for (j=0; j<len_u; j++) {
      PetscInt lidx,gidx;
      
      lidx = local_u_dof[j];
      gidx = col_l2g[lidx];
      
      local_u_dof[j] = gidx;
    }
    
    for (i=0; i<len_w; i++) {
      PetscInt map_idx;
      PetscInt index_i = local_w_dof[i];
      
      if (index_i < row_start) continue;
      if (index_i >= row_end) continue;
      
      map_idx = index_i - row_start;
      
      for (j=0; j<len_u; j++) {
        PetscBool exists = PETSC_FALSE;
        PetscInt index_j;
        
        index_j = local_u_dof[j];
        
        /* check if node index njj is in list - if not, resize and insert */
        for (k=0; k<nnz[map_idx]; k++) {
          if (nnmap[map_idx][k] == index_j) {
            exists = PETSC_TRUE;
            break;
          }
        }
        
        if (!exists) {
          PetscInt *orig;
          PetscInt *tmp;
          
          orig = nnmap[map_idx];
          tmp = realloc(orig,sizeof(PetscInt)*(nnz[map_idx]+1));
          nnmap[map_idx] = tmp;
          nnmap[map_idx][ nnz[map_idx] ] = index_j;
          nnz[map_idx]++;
        }
      }
    }
  }
  
  
  /////////////////////////////////////////////////
  /* Get the table content from off-rank sources */
  /////////////////////////////////////////////////
  
  de = DataExCreate(comm,0);
  
  // [topology]
  ierr = DataExTopologyInitialize(de);CHKERRQ(ierr);

  /* This is a hack and simple done because I was lazy */
  for (j=0; j<commsize; j++) {
    PetscMPIInt rank = (PetscMPIInt)j;
    if (rank != commrank) {
      ierr = DataExTopologyAddNeighbour(de,rank);CHKERRQ(ierr);
    }
  }
  ierr = DataExTopologyFinalize(de);CHKERRQ(ierr);
  
  
  // [send counts]
  ierr = DataExInitializeSendCount(de);CHKERRQ(ierr);
  
  // examine the local table and see if col indices are off-rank
  for (e=0; e<nel; e++) {
    PetscInt *enodes_w,*enodes_u;
    
    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    /* get local dof w and u */
    for (i=0; i<bpe_w; i++) {
      ni = enodes_w[i];
      for (b=0; b<dof_w; b++) {
        local_w_dof[dof_w * i + b] = dof_w * ni + b;
      }
    }
    
    for (j=0; j<bpe_u; j++) {
      nj = enodes_u[j];
      for (b=0; b<dof_u; b++) {
        local_u_dof[dof_u * j + b] = dof_u * nj + b;
      }
    }
    
    /* convert w */
    for (i=0; i<len_w; i++) {
      PetscInt lidx,gidx;
      
      lidx = local_w_dof[i];
      gidx = row_l2g[lidx];
      
      local_w_dof[i] = gidx;
    }
    
    /* convert u */
    for (j=0; j<len_u; j++) {
      PetscInt lidx,gidx;
      
      lidx = local_u_dof[j];
      gidx = col_l2g[lidx];
      
      local_u_dof[j] = gidx;
    }
    
    for (i=0; i<len_w; i++) {
      PetscInt g2l_i;
      PetscMPIInt rank_index_i = commrank;
      
      g2l_i = local_w_dof[i];
      for (k=0; k<commsize; k++) {
        if ((g2l_i >= row_ranges[k]) && (g2l_i < row_ranges[k+1])) {
          rank_index_i = (PetscMPIInt)k;
          break;
        }
      }
      
      if (rank_index_i != commrank) {
        ierr = DataExAddToSendCount( de, rank_index_i, 1 );CHKERRQ(ierr);
      }
    }
  }
  
  ierr = DataExFinalizeSendCount(de);CHKERRQ(ierr);
  
  
  // [pack]
  ierr = PetscMalloc1(1 + len_u,&buffer);CHKERRQ(ierr);
  
  ierr = DataExPackInitialize(de,sizeof(PetscInt)*(1 + len_u));CHKERRQ(ierr);
  
  for (e=0; e<nel + 0; e++) {
    PetscInt  *enodes_w,*enodes_u;
    
    enodes_w = &element_w[bpe_w*e];
    enodes_u = &element_u[bpe_u*e];
    
    
    /* get local dof w and u */
    for (i=0; i<bpe_w; i++) {
      ni = enodes_w[i];
      for (b=0; b<dof_w; b++) {
        local_w_dof[dof_w * i + b] = dof_w * ni + b;
      }
    }
    
    for (j=0; j<bpe_u; j++) {
      nj = enodes_u[j];
      for (b=0; b<dof_u; b++) {
        local_u_dof[dof_u * j + b] = dof_u * nj + b;
      }
    }
    
    /* convert w */
    for (i=0; i<len_w; i++) {
      PetscInt lidx,gidx;
      
      lidx = local_w_dof[i];
      gidx = row_l2g[lidx];
      
      local_w_dof[i] = gidx;
    }
    
    /* convert u */
    for (j=0; j<len_u; j++) {
      PetscInt lidx,gidx;
      
      lidx = local_u_dof[j];
      gidx = col_l2g[lidx];
      
      local_u_dof[j] = gidx;
    }
    
    for (i=0; i<len_w; i++) {
      PetscInt g2l_i;
      PetscMPIInt rank_index_i = commrank;
      
      g2l_i = local_w_dof[i];
      for (k=0; k<commsize; k++) {
        if ((g2l_i >= row_ranges[k]) && (g2l_i < row_ranges[k+1])) {
          rank_index_i = (PetscMPIInt)k;
          break;
        }
      }
      
      if (rank_index_i != commrank) {
        buffer[0] = local_w_dof[i];
        
        ierr = PetscMemcpy(&buffer[1],local_u_dof, sizeof(PetscInt) * (len_u) );CHKERRQ(ierr);
        
        ierr = DataExPackData( de, rank_index_i, 1,(void*)buffer );CHKERRQ(ierr);
      }
    }
  }
  
  ierr = DataExPackFinalize(de);CHKERRQ(ierr);
  
  // [send-recv]
  ierr = DataExBegin(de);CHKERRQ(ierr);
  ierr = DataExEnd(de);CHKERRQ(ierr);
  
  
  // [un-pack]
  ierr = DataExGetRecvData( de, &recv_length, (void**)&recv_buffer );CHKERRQ(ierr);
  
  for (e=0; e<recv_length; e++) {
    const PetscInt *this_data;
    const PetscInt *this_data_w,*this_data_u;
    PetscInt index_i,map_idx;
    
    this_data   = &recv_buffer[ (1 + len_u)*e ];
    this_data_w = &this_data[0];
    this_data_u = &this_data[1];
    
    index_i = this_data_w[0];
    if (index_i < row_start) continue;
    if (index_i >= row_end) continue;
    
    map_idx = index_i - row_start;
    
    for (j=0; j<len_u; j++) {
      PetscBool exists = PETSC_FALSE;
      PetscInt index_j;
      
      index_j = this_data_u[j];
      
      /* check if node index njj is in list - if not, resize and insert */
      for (k=0; k<nnz[map_idx]; k++) {
        if (nnmap[map_idx][k] == index_j) {
          exists = PETSC_TRUE;
          break;
        }
      }
      
      if (!exists) {
        PetscInt *orig;
        PetscInt *tmp;
        
        orig = nnmap[map_idx];
        tmp = realloc(orig,sizeof(PetscInt)*(nnz[map_idx]+1));
        nnmap[map_idx] = tmp;
        nnmap[map_idx][ nnz[map_idx] ] = index_j;
        nnz[map_idx]++;
      }
    }
  }

  ierr = PetscMalloc1(nrows_w,&dnnz);CHKERRQ(ierr);
  ierr = PetscMalloc1(nrows_w,&onnz);CHKERRQ(ierr);
  for (i=0; i<nrows_w; i++) {
    dnnz[i] = 0;
    onnz[i] = 0;
  }
  
  for (i=0; i<nrows_w; i++) {
    PetscInt diag_cnt = 0;
    PetscInt off_diag_cnt = 0;
    
    for (j=0; j<nnz[i]; j++) {
      PetscInt gidx_j;
      
      gidx_j = nnmap[i][j];
      if ((gidx_j >= col_start) && (gidx_j < col_end)) { /* in the col range <diag> */
        diag_cnt++;
      } else {
        off_diag_cnt++;
      }
    }
    
    dnnz[i] = diag_cnt;
    onnz[i] = off_diag_cnt;
  }
  
  ierr = MatMPIAIJSetPreallocation(A,0,dnnz,0,onnz);CHKERRQ(ierr);
  
  ierr = ISLocalToGlobalMappingRestoreIndices(col_l2g_map,&col_l2g);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingRestoreIndices(row_l2g_map,&row_l2g);CHKERRQ(ierr);
  
  ierr = PetscFree(dnnz);CHKERRQ(ierr);
  ierr = PetscFree(onnz);CHKERRQ(ierr);
  ierr = DataExDestroy(de);CHKERRQ(ierr);
  ierr = PetscFree(buffer);CHKERRQ(ierr);
  
  ierr = PetscFree(nnz);CHKERRQ(ierr);
  for (k=0; k<nrows_w; k++) {
    free(nnmap[k]);
  }
  ierr = PetscFree(nnmap);CHKERRQ(ierr);

  ierr = PetscFree(local_w_dof);CHKERRQ(ierr);
  ierr = PetscFree(local_u_dof);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

typedef struct _p_FARC *FARC;
struct _p_FARC {
  PetscInt       len,*nnz,**nnmap;
  const PetscInt *row_ranges,*col_ranges;
  PetscInt       nrows_w,row_start,row_end,col_start,col_end;
  PetscInt       len_w,len_u;
  PetscInt       *global_w_dof,*global_u_dof;
  PetscInt       ncache,*cache,stride;
  DataEx         de;
};

// collective on A->comm
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationInit"
PetscErrorCode FARCMatPreallocationInit(Mat A)
{
  FARC p = NULL;
  PetscInt k,j;
  MPI_Comm comm;
  PetscMPIInt commsize,commrank;
  PetscContainer container = NULL;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&p);CHKERRQ(ierr);
  ierr = PetscMemzero(p,sizeof(struct _p_FARC));CHKERRQ(ierr);
  
  ierr = MatGetOwnershipRange(A,&p->row_start,&p->row_end);CHKERRQ(ierr);
  ierr = MatGetOwnershipRanges(A,&p->row_ranges);CHKERRQ(ierr);
  
  ierr = MatGetOwnershipRangeColumn(A,&p->col_start,&p->col_end);CHKERRQ(ierr);
  ierr = MatGetOwnershipRangesColumn(A,&p->col_ranges);CHKERRQ(ierr);
  
  p->nrows_w = p->row_end - p->row_start;
  
  /////////////////////////
  /* Initalize the table */
  /////////////////////////
  ierr = PetscMalloc1(p->nrows_w,&p->nnz);CHKERRQ(ierr);
  for (k=0; k<p->nrows_w; k++) {
    p->nnz[k] = 0;
  }
  
  ierr = PetscMalloc1(p->nrows_w,&p->nnmap);CHKERRQ(ierr);
  for (k=0; k<p->nrows_w; k++) {
    p->nnmap[k] = malloc(sizeof(PetscInt));
    p->nnmap[k][0] = 0; /* set a default value - it will be clobbered immediately */
  }
  
  comm = PetscObjectComm((PetscObject)A);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  p->de = DataExCreate(comm,0);
  
  // [topology]
  ierr = DataExTopologyInitialize(p->de);CHKERRQ(ierr);
  
  /* This is a hack and simple done because I was lazy */
  for (j=0; j<commsize; j++) {
    PetscMPIInt rank = (PetscMPIInt)j;
    if (rank != commrank) {
      ierr = DataExTopologyAddNeighbour(p->de,rank);CHKERRQ(ierr);
    }
  }
  ierr = DataExTopologyFinalize(p->de);CHKERRQ(ierr);
 
  p->len_w = 0;
  p->len_u = 0;
  
  p->ncache = 0;
  p->stride = 0;
  p->cache = malloc(sizeof(PetscInt));
  p->cache[0] = 0;
  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) {
    ierr = PetscContainerCreate(comm,&container);CHKERRQ(ierr);
    ierr = PetscObjectCompose((PetscObject)A,"__farc_prealloc",(PetscObject)container);CHKERRQ(ierr);
  }
  ierr = PetscContainerSetPointer(container,(void*)p);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

// not collective
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationSetValuesLocal"
PetscErrorCode FARCMatPreallocationSetValuesLocal(Mat A,PetscInt nr,PetscInt rows[],PetscInt nc,PetscInt cols[])
{
  FARC p = NULL;
  PetscContainer container = NULL;
  PetscInt i,j,k;
  const PetscInt *row_l2g,*col_l2g;
  ISLocalToGlobalMapping row_l2g_map,col_l2g_map;
  PetscErrorCode ierr;
  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);
  
  if (p->len_w == 0) {
    p->len_w = nr;
    ierr = PetscMalloc1(p->len_w,&p->global_w_dof);CHKERRQ(ierr);
    p->stride = p->len_w + 1;
  } else {
    if (nr > p->len_w) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot re-size data block <row> being inserted to be larger than that specified on the first call (%D). Call FARCMatPreallocationFlush() first if you want to change the size",p->len_w);
  }
  if (p->len_u == 0) {
    p->len_u = nc;
    ierr = PetscMalloc1(p->len_u,&p->global_u_dof);CHKERRQ(ierr);
  } else {
    if (nc > p->len_u) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot re-size data block <col> being inserted to be larger than that specified on the first call (%D). Call FARCMatPreallocationFlush() first if you want to change the size",p->len_u);
  }
  
  ierr = MatGetLocalToGlobalMapping(A,&row_l2g_map,&col_l2g_map);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(row_l2g_map,&row_l2g);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(col_l2g_map,&col_l2g);CHKERRQ(ierr);
  
  for (i=0; i<p->len_w; i++) { p->global_w_dof[i] = -1; }
  for (i=0; i<p->len_u; i++) { p->global_u_dof[i] = -1; }
  
  /* convert w */
  for (i=0; i<nr; i++) {
    PetscInt lidx,gidx;
    
    lidx = rows[i];
    gidx = row_l2g[lidx];
    p->global_w_dof[i] = gidx;
  }
  
  /* convert u */
  for (j=0; j<nc; j++) {
    PetscInt lidx,gidx;
    
    lidx = cols[j];
    gidx = col_l2g[lidx];
    p->global_u_dof[j] = gidx;
  }
  
  /* try and insert into local table */
  for (i=0; i<nr; i++) {
    PetscInt map_idx;
    PetscInt index_i = p->global_w_dof[i];
    
    if (index_i == -1) continue;
    if (index_i < p->row_start) continue;
    if (index_i >= p->row_end) continue;
    
    map_idx = index_i - p->row_start;
    
    for (j=0; j<nc; j++) {
      PetscBool exists = PETSC_FALSE;
      PetscInt index_j;
      
      index_j = p->global_u_dof[j];
      if (index_j == -1) continue;
      
      /* check if node index njj is in list - if not, resize and insert */
      for (k=0; k<p->nnz[map_idx]; k++) {
        if (p->nnmap[map_idx][k] == index_j) {
          exists = PETSC_TRUE;
          break;
        }
      }
      
      if (!exists) {
        PetscInt *orig;
        PetscInt *tmp;
        
        orig = p->nnmap[map_idx];
        tmp = realloc(orig,sizeof(PetscInt)*(p->nnz[map_idx]+1));
        p->nnmap[map_idx] = tmp;
        p->nnmap[map_idx][ p->nnz[map_idx] ] = index_j;
        p->nnz[map_idx]++;
      }
    }
  }
  
  /* cache everything else */
  for (i=0; i<nr; i++) {
    PetscInt index_i = p->global_w_dof[i];
    PetscInt *orig;
    PetscInt *tmp;
    
    if (index_i == -1) continue;

    if ((index_i >= p->row_start) && (index_i < p->row_end)) continue;
    
    orig = p->cache;
    tmp = realloc(orig,sizeof(PetscInt)*((p->ncache + 1) * p->stride));
    p->cache = tmp;
    
    p->cache[ p->ncache*p->stride + 0 ] = index_i;
    for (j=0; j<p->len_u; j++) { /* note here we use the entire buffer - not just nc values */
      PetscInt index_j;
      
      index_j = p->global_u_dof[j];
      p->cache[ p->ncache*p->stride + 1 + j ] = index_j;
    }
    p->ncache++;
  }

  ierr = ISLocalToGlobalMappingRestoreIndices(col_l2g_map,&col_l2g);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingRestoreIndices(row_l2g_map,&row_l2g);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

// not collective
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationSetValues"
PetscErrorCode FARCMatPreallocationSetValues(Mat A,PetscInt nr,PetscInt rows[],PetscInt nc,PetscInt cols[])
{
  FARC p = NULL;
  PetscContainer container = NULL;
  PetscInt i,j,k;
  PetscErrorCode ierr;
  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);
  
  if (p->len_w == 0) {
    p->len_w = nr;
    ierr = PetscMalloc1(p->len_w,&p->global_w_dof);CHKERRQ(ierr);
    p->stride = p->len_w + 1;
  } else {
    if (nr > p->len_w) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot re-size data block <row> being inserted to be larger than that specified on the first call (%D). Call FARCMatPreallocationFlush() first if you want to change the size",p->len_w);
  }
  if (p->len_u == 0) {
    p->len_u = nc;
    ierr = PetscMalloc1(p->len_u,&p->global_u_dof);CHKERRQ(ierr);
  } else {
    if (nc > p->len_u) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot re-size data block <col> being inserted to be larger than that specified on the first call (%D). Call FARCMatPreallocationFlush() first if you want to change the size",p->len_u);
  }
  
  for (i=0; i<p->len_w; i++) { p->global_w_dof[i] = -1; }
  for (i=0; i<p->len_u; i++) { p->global_u_dof[i] = -1; }
  
  /* copy w */
  for (i=0; i<nr; i++) {
    PetscInt gidx;
    
    gidx = rows[i];
    p->global_w_dof[i] = gidx;
  }
  
  /* copy u */
  for (j=0; j<nc; j++) {
    PetscInt gidx;
    
    gidx = cols[j];
    p->global_u_dof[j] = gidx;
  }
  
  /* try and insert into local table */
  for (i=0; i<nr; i++) {
    PetscInt map_idx;
    PetscInt index_i = p->global_w_dof[i];
    
    if (index_i == -1) continue;
    if (index_i < p->row_start) continue;
    if (index_i >= p->row_end) continue;
    
    map_idx = index_i - p->row_start;
    
    for (j=0; j<nc; j++) {
      PetscBool exists = PETSC_FALSE;
      PetscInt index_j;
      
      index_j = p->global_u_dof[j];
      if (index_j == -1) continue;
      
      /* check if node index njj is in list - if not, resize and insert */
      for (k=0; k<p->nnz[map_idx]; k++) {
        if (p->nnmap[map_idx][k] == index_j) {
          exists = PETSC_TRUE;
          break;
        }
      }
      
      if (!exists) {
        PetscInt *orig;
        PetscInt *tmp;
        
        orig = p->nnmap[map_idx];
        tmp = realloc(orig,sizeof(PetscInt)*(p->nnz[map_idx]+1));
        p->nnmap[map_idx] = tmp;
        p->nnmap[map_idx][ p->nnz[map_idx] ] = index_j;
        p->nnz[map_idx]++;
      }
    }
  }
  
  /* cache everything else */
  for (i=0; i<nr; i++) {
    PetscInt index_i = p->global_w_dof[i];
    PetscInt *orig;
    PetscInt *tmp;
    
    if (index_i == -1) continue;
    
    if ((index_i >= p->row_start) && (index_i < p->row_end)) continue;
    
    orig = p->cache;
    tmp = realloc(orig,sizeof(PetscInt)*((p->ncache + 1) * p->stride));
    p->cache = tmp;
    
    p->cache[ p->ncache*p->stride + 0 ] = index_i;
    for (j=0; j<p->len_u; j++) { /* note here we use the entire buffer - not just nc values */
      PetscInt index_j;
      
      index_j = p->global_u_dof[j];
      p->cache[ p->ncache*p->stride + 1 + j ] = index_j;
    }
    p->ncache++;
  }
  
  PetscFunctionReturn(0);
}

// collective on A->comm
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationFlush_MPI"
PetscErrorCode FARCMatPreallocationFlush_MPI(Mat A)
{
  FARC p = NULL;
  PetscContainer container = NULL;
  MPI_Comm comm;
  PetscMPIInt commsize,commrank;
  PetscInt e,j,k,min;
  PetscInt recv_length,*recv_buffer;
  PetscErrorCode ierr;
  
  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);
  
  comm = PetscObjectComm((PetscObject)A);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  
  /* check that the bounds of len_w and len_u were the same on all ranks */
  ierr = MPI_Allreduce(&p->len_w,&min,1,MPIU_INT,MPI_MIN,comm);CHKERRQ(ierr);
  if (p->len_w != min) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"All ranks must use a row buffer of the same size");
  
  ierr = MPI_Allreduce(&p->len_u,&min,1,MPIU_INT,MPI_MIN,comm);CHKERRQ(ierr);
  if (p->len_u != min) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"All ranks must use a col buffer of the same size");
  
  
  // [send counts]
  ierr = DataExInitializeSendCount(p->de);CHKERRQ(ierr);
  
  // examine the local table and see if col indices are off-rank
  for (e=0; e<p->ncache; e++) {
    PetscInt row;
    PetscMPIInt rank_index_i = commrank;
    
    row  =  p->cache[e*p->stride + 0];
    //cols = &p->cache[e*p->stride + 1];
    
    for (k=0; k<commsize; k++) {
      if ((row >= p->row_ranges[k]) && (row < p->row_ranges[k+1])) {
        rank_index_i = (PetscMPIInt)k;
        break;
      }
    }
    if (rank_index_i != commrank) {
      ierr = DataExAddToSendCount( p->de, rank_index_i, 1 );CHKERRQ(ierr);
    }
  }
  
  ierr = DataExFinalizeSendCount(p->de);CHKERRQ(ierr);
  
  
  // [pack]
  ierr = DataExPackInitialize(p->de,sizeof(PetscInt)*(p->stride));CHKERRQ(ierr);
  
  for (e=0; e<p->ncache; e++) {
    PetscInt row;
    PetscMPIInt rank_index_i = commrank;
    
    row  =  p->cache[e*p->stride + 0];
    //cols = &p->cache[e*p->stride + 1];
    
    for (k=0; k<commsize; k++) {
      if ((row >= p->row_ranges[k]) && (row < p->row_ranges[k+1])) {
        rank_index_i = (PetscMPIInt)k;
        break;
      }
    }
    
    if (rank_index_i != commrank) {
      PetscInt *buffer;
      
      buffer = &p->cache[e * p->stride];
      ierr = DataExPackData( p->de, rank_index_i, 1,(void*)buffer );CHKERRQ(ierr);
    }
  }
  
  ierr = DataExPackFinalize(p->de);CHKERRQ(ierr);
  
  // [send-recv]
  ierr = DataExBegin(p->de);CHKERRQ(ierr);
  ierr = DataExEnd(p->de);CHKERRQ(ierr);
  
  
  // [un-pack]
  ierr = DataExGetRecvData( p->de, &recv_length, (void**)&recv_buffer );CHKERRQ(ierr);
  
  for (e=0; e<recv_length; e++) {
    const PetscInt *this_data;
    const PetscInt *this_data_row,*this_data_col;
    PetscInt index_i,map_idx;
    
    this_data   = &recv_buffer[ p->stride*e ];
    this_data_row = &this_data[0];
    this_data_col = &this_data[1];
    
    index_i = this_data_row[0];
    if (index_i == -1) continue;
    if (index_i < p->row_start) continue;
    if (index_i >= p->row_end) continue;
    
    map_idx = index_i - p->row_start;
    
    for (j=0; j<p->len_u; j++) {
      PetscBool exists = PETSC_FALSE;
      PetscInt index_j;
      
      index_j = this_data_col[j];
      if (index_j == -1) continue;
      
      /* check if node index njj is in list - if not, resize and insert */
      for (k=0; k<p->nnz[map_idx]; k++) {
        if (p->nnmap[map_idx][k] == index_j) {
          exists = PETSC_TRUE;
          break;
        }
      }
      
      if (!exists) {
        PetscInt *orig;
        PetscInt *tmp;
        
        orig = p->nnmap[map_idx];
        tmp = realloc(orig,sizeof(PetscInt)*(p->nnz[map_idx]+1));
        p->nnmap[map_idx] = tmp;
        p->nnmap[map_idx][ p->nnz[map_idx] ] = index_j;
        p->nnz[map_idx]++;
      }
    }
  }
  
  p->len_w = 0;
  p->stride = 0;
  p->len_u = 0;
  ierr = PetscFree(p->global_w_dof);CHKERRQ(ierr);
  ierr = PetscFree(p->global_u_dof);CHKERRQ(ierr);

  if (p->ncache != 0) {
    PetscInt *tmp,*orig;
    
    orig = p->cache;
    tmp = realloc(orig,sizeof(PetscInt));
    p->cache = tmp;
    p->cache[0] = 0;
    p->ncache = 0;
  }

  
  PetscFunctionReturn(0);
}


// collective on A->comm
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationFinalize_MPI"
PetscErrorCode FARCMatPreallocationFinalize_MPI(Mat A)
{
  FARC p = NULL;
  PetscContainer container = NULL;
  PetscInt i,j;
  PetscInt *dnnz,*onnz;
  PetscErrorCode ierr;

  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);
  
  ierr = FARCMatPreallocationFlush_MPI(A);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(p->nrows_w,&dnnz);CHKERRQ(ierr);
  ierr = PetscMalloc1(p->nrows_w,&onnz);CHKERRQ(ierr);
  for (i=0; i<p->nrows_w; i++) {
    dnnz[i] = 0;
    onnz[i] = 0;
  }
  
  for (i=0; i<p->nrows_w; i++) {
    PetscInt diag_cnt = 0;
    PetscInt off_diag_cnt = 0;
    
    for (j=0; j<p->nnz[i]; j++) {
      PetscInt gidx_j;
      
      gidx_j = p->nnmap[i][j];
      if ((gidx_j >= p->col_start) && (gidx_j < p->col_end)) { /* in the col range <diag> */
        diag_cnt++;
      } else {
        off_diag_cnt++;
      }
    }
    
    dnnz[i] = diag_cnt;
    onnz[i] = off_diag_cnt;
  }
  
  ierr = MatMPIAIJSetPreallocation(A,0,dnnz,0,onnz);CHKERRQ(ierr);

  ierr = MatSetOption(A,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);

  ierr = PetscFree(dnnz);CHKERRQ(ierr);
  ierr = PetscFree(onnz);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationFinalize_SEQ"
PetscErrorCode FARCMatPreallocationFinalize_SEQ(Mat A)
{
  FARC p = NULL;
  PetscContainer container = NULL;
  PetscInt i;
  PetscInt *dnnz;
  PetscErrorCode ierr;
  
  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);
  
  /*
  {
    for (i=0; i<p->nrows_w; i++) {
      for (j=0; j<p->nnz[i]; j++) {
        printf("i %d -> j[%d] %d\n",i,j,p->nnmap[i][j]);
      }
    }
  }
  */
   
  ierr = PetscMalloc1(p->nrows_w,&dnnz);CHKERRQ(ierr);
  for (i=0; i<p->nrows_w; i++) {
    dnnz[i] = p->nnz[i];
  }
  
  ierr = MatSeqAIJSetPreallocation(A,0,dnnz);CHKERRQ(ierr);
  ierr = MatSetOption(A,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
  
  ierr = PetscFree(dnnz);CHKERRQ(ierr);
                     
  PetscFunctionReturn(0);
}

// collective on A->comm
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationFlush"
PetscErrorCode FARCMatPreallocationFlush(Mat A)
{
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  comm = PetscObjectComm((PetscObject)A);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    FARC p = NULL;
    PetscContainer container = NULL;

    ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
    if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
    ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);

    p->len_w = 0;
    p->stride = 0;
    p->len_u = 0;
    ierr = PetscFree(p->global_w_dof);CHKERRQ(ierr);
    ierr = PetscFree(p->global_u_dof);CHKERRQ(ierr);
    
    if (p->ncache != 0) {
      PetscInt *orig,*tmp;
      
      orig = p->cache;
      tmp = realloc(orig,sizeof(PetscInt));
      p->cache = tmp;
      p->cache[0] = 0;
      p->ncache = 0;
    }
  } else {
    ierr = FARCMatPreallocationFlush_MPI(A);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationInsertZeros"
PetscErrorCode FARCMatPreallocationInsertZeros(Mat A)
{
  FARC p = NULL;
  PetscContainer container = NULL;
  PetscInt i,start;
  PetscErrorCode ierr;
  
  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);
  
  ierr = MatGetOwnershipRange(A,&start,NULL);CHKERRQ(ierr);
  for (i=0; i<p->nrows_w; i++) {
    PetscScalar *zeros;
    PetscInt g_row;
    const PetscInt *cols;
    
    g_row = i + start;
    ierr = PetscMalloc1(p->nnz[i],&zeros);CHKERRQ(ierr);
    ierr = PetscMemzero(zeros,sizeof(PetscScalar)*p->nnz[i]);CHKERRQ(ierr);
    cols = (const PetscInt*)p->nnmap[i];
    ierr = MatSetValues(A,1,&g_row,p->nnz[i],cols,zeros,INSERT_VALUES);CHKERRQ(ierr);
    ierr = PetscFree(zeros);CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

// collective on A->comm
#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocationFinalize"
PetscErrorCode FARCMatPreallocationFinalize(Mat A)
{
  FARC p = NULL;
  PetscContainer container = NULL;
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  comm = PetscObjectComm((PetscObject)A);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = FARCMatPreallocationFinalize_SEQ(A);CHKERRQ(ierr);
  } else {
    ierr = FARCMatPreallocationFinalize_MPI(A);CHKERRQ(ierr);
  }
  ierr = FARCMatPreallocationInsertZeros(A);CHKERRQ(ierr);

  ierr = PetscObjectQuery((PetscObject)A,"__farc_prealloc",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) SETERRQ(PetscObjectComm((PetscObject)A),PETSC_ERR_USER,"Mat must be composed with \"__farc_prealloc\"\n");
  ierr = PetscContainerGetPointer(container,(void**)&p);CHKERRQ(ierr);

  /* release memory */
  {
    PetscInt i;
    
    ierr = PetscFree(p->nnz);CHKERRQ(ierr);
    for (i=0; i<p->nrows_w; i++) {
      free(p->nnmap[i]);
    }
    ierr = PetscFree(p->nnmap);CHKERRQ(ierr);
    ierr = PetscFree(p->global_w_dof);CHKERRQ(ierr);
    ierr = PetscFree(p->global_u_dof);CHKERRQ(ierr);
    free(p->cache);
    ierr = DataExDestroy(p->de);CHKERRQ(ierr);
    ierr = PetscFree(p);CHKERRQ(ierr);
    p = NULL;
  }
  
  /* nullify container->pointer */
  ierr = PetscContainerSetPointer(container,NULL);CHKERRQ(ierr);
  ierr = PetscContainerDestroy(&container);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject)A,"__farc_prealloc",NULL);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "FARCMatPreallocation"
PetscErrorCode FARCMatPreallocation(Mat A,DM dmw,DM dmu)
{
  PetscErrorCode ierr;
  //ierr = FARCMatPreallocation_v0(A,dmw,dmu);CHKERRQ(ierr);
  ierr = FARCMatPreallocation_v1(A,dmw,dmu);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
