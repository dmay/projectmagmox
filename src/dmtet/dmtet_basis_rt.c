
#include <khash.h>
#include <petsc.h>
#include <petscmat.h>
#include <petscksp.h>
#include <dmtetimpl.h>
#include <dmtet_common.h>
#include <dmtetdt.h>
#include <dmtet_geom_utils.h>

#define TOL PETSC_MACHINE_EPSILON

KHASH_MAP_INIT_INT64(64, long int)

#undef __FUNCT__
#define __FUNCT__ "_DMTetGenerateBasis2d_RT0"
PetscErrorCode _DMTetGenerateBasis2d_RT0(DM dm,DM_TET *tet,PetscInt order)
{
  PetscErrorCode ierr;
  khash_t(64) *h;
  int ret;
  khiter_t iterator;
  long int unique_edges;
  PetscReal min[2],max[2],mindx,cellmindx,celldx[2];
  PetscInt e,i,edg,nodes_per_element,basis_per_edge,basis_per_element;
  long int mx,my,key;
  
  
  PetscFunctionBegin;
  ierr = GeomHelper_ComputeElememntBoundingBox(tet,celldx);CHKERRQ(ierr);
  cellmindx = celldx[0];
  cellmindx = PetscMin(cellmindx,celldx[1]);
  mindx = cellmindx;
  mindx = 1.0e-1 * mindx / (PetscReal)(order+3);
  
  ierr = GeomHelper_ComputeDomainBoundingBox(tet,min,max);CHKERRQ(ierr);
  min[0] -= TOL; min[1] -= TOL;
  max[0] += TOL; max[1] += TOL;
  
  mx = (max[0] - min[0])/mindx;
  my = (max[1] - min[1])/mindx;

  /* Initialize hash table */
  h = kh_init(64);
  iterator = kh_put(64, h, -1, &ret); /* insert key into slot 1 */
  unique_edges = 0; /* set edge counter to zero */
  
  /* populate table */
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscReal elcoor[2*3],e0[2],e1[2],e2[2],*edges[3];
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
      //printf("e %d : i %d (%1.4e,%1.4e)\n",e,i,elcoor[2*i],elcoor[2*i+1]);
    }
    
    /* generate edge coordinates */
    ierr = GeomHelper_ComputeEdgeMidPoints(tet,elcoor,e0,e1,e2);CHKERRQ(ierr);
    edges[0] = e0;
    edges[1] = e1;
    edges[2] = e2;
    
    /* generate edge DOF labels */
    for (edg=0; edg<3; edg++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = edges[edg][0];
      yp = edges[edg][1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      //printf("key %ld \n",key);
      
      iterator = kh_get(64, h, key);
      if (kh_exist(h, iterator)) { /* key exists */
        //printf("common edge found *** e[%d] edge[%d] \n",e,edg);
      } else { /* key doesn't exist - insert it */
        /* set value */
        iterator = kh_put(64, h, key, &ret);
        (kh_value(h, iterator)) = unique_edges;
        
        unique_edges++;
      }
    }
  }
  
  basis_per_edge = 1;
  nodes_per_element = 3;
  basis_per_element = 3 * basis_per_edge;

  ierr = _DMTetMeshCreate(&tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1(tet->geometry->nelements,basis_per_element,tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(tet->geometry->nnodes,nodes_per_element,2,tet->space);CHKERRQ(ierr);
  
  /* extract lables from table */
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscReal elcoor[2*3],e0[2],e1[2],e2[2],*edges[3];
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
    }

    /* copy geometry coordinates into element */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      tet->space->coords[2*i+0] = tet->geometry->coords[2*idx+0];
      tet->space->coords[2*i+1] = tet->geometry->coords[2*idx+1];
    }

    /* generate edge mid-point coordinates */
    ierr = GeomHelper_ComputeEdgeMidPoints(tet,elcoor,e0,e1,e2);CHKERRQ(ierr);
    edges[0] = e0;
    edges[1] = e1;
    edges[2] = e2;
    
    /* get dof label */
    for (edg=0; edg<3; edg++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = edges[edg][0];
      yp = edges[edg][1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      iterator = kh_get(64, h, key);
      if (!kh_exist(h, iterator)) {
        printf("... key %ld doesn't exist ERROR \n",key);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key generation has failed");
      } else {
        int nodeidx = kh_value(h, iterator);

        tet->space->element[basis_per_element*e + edg] = (PetscInt)nodeidx;

        /* flip sign */
        //if (nodeidx >= 0) {
        //  /* set value */
        //  iterator = kh_put(64, h, key, &ret);
        //  (kh_value(h, iterator)) = -(nodeidx + 1);
        //}
      
      }
    }
  }

  tet->space->nactivedof = (PetscInt)unique_edges;
  PetscPrintf(PETSC_COMM_SELF,"Found %D unique edge DOFs\n",tet->space->nactivedof);
  
  /*
   if (nodeidx < 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key generation has failed - sign should only be flipped once. More than two elements are trying to share this DOF");
  */
   
   
   
  
  kh_destroy(64, h); /* destroy hash table */
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d_RaviartThomas"
PetscErrorCode DMTetGenerateBasis2d_RaviartThomas(DM dm,DM_TET *tet,PetscInt order)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (order > 0) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only RT_0 supported");
  
  switch (order) {
    case 0:
      ierr = _DMTetGenerateBasis2d_RT0(dm,tet,order);CHKERRQ(ierr);
      break;
      
    default:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only RT_0 supported");
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetBasisDOF_RT"
PetscErrorCode DMTetGetBasisDOF_RT(DM dm,PetscInt e,PetscInt edof[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt *edof_ref,j,bpe;

  
  PetscFunctionBegin;
  if (tet->basis_type != DMTET_RAVIART_THOMAS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"DM space is not of type RaviartThomas");
  
  bpe = tet->space->bpe;
  edof_ref = &tet->space->element[bpe*e];
  for (j=0; j<bpe; j++) {
    edof[j] = edof_ref[j];
    //if (edof[j] < 0) { /* not needed anymore */
    //  edof[j] = -edof_ref[j] - 1;
    //}
    if (edof[j] < 0) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"negative DOF not allowed");
  }
  
  PetscFunctionReturn(0);
}

void EvaluateLocalBasis_RT0(PetscReal xi[],PetscReal N1[],PetscReal N2[],PetscReal N3[])
{
  const PetscReal sr2 = 1.0;//PetscSqrtReal(2.0);
  
  N1[0] = sr2 * xi[0];
  N1[1] = sr2 * xi[1];
  
  N2[0] = xi[0] - 1.0;
  N2[1] = xi[1];
  
  N3[0] = xi[0];
  N3[1] = xi[1] - 1.0;
}

void EvaluateLocalBasisDerivatives_RT0(
  PetscReal xi[],
  PetscReal dN1_x1[],PetscReal dN2_x1[],PetscReal dN3_x1[],
  PetscReal dN1_x2[],PetscReal dN2_x2[],PetscReal dN3_x2[])
{
  const PetscReal sr2 = 1.0; //PetscSqrtReal(2.0);

  /* derivatives wrt to xi */
  dN1_x1[0] = sr2;
  dN1_x1[1] = 0.0;
  
  dN2_x1[0] = 1.0;
  dN2_x1[1] = 0.0;
  
  dN3_x1[0] = 1.0;
  dN3_x1[1] = 0.0;

  /* derivatives wrt to eta */
  dN1_x2[0] = 0.0;
  dN1_x2[1] = sr2;
  
  dN2_x2[0] = 0.0;
  dN2_x2[1] = 1.0;
  
  dN3_x2[0] = 0.0;
  dN3_x2[1] = 1.0;
}

#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasis2d_RaviartThomas"
PetscErrorCode DMTetTabulateBasis2d_RaviartThomas(
  DM dm, PetscInt npoints,PetscReal xi[],
  PetscInt *_nbasis,PetscReal ***_N)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal **N;
  PetscInt q,bpe;
  const PetscInt dimension = 2;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  bpe = tet->space->bpe;
  ierr = PetscMalloc1(npoints,&N);CHKERRQ(ierr);
  for (q=0; q<npoints; q++) {
    ierr = PetscMalloc1(dimension*bpe,&N[q]);CHKERRQ(ierr);
  }

  for (q=0; q<npoints; q++) {
    switch (tet->basis_order) {
      
      case 0:
        EvaluateLocalBasis_RT0(&xi[2*q],&N[q][dimension*0],&N[q][dimension*1],&N[q][dimension*2]);
        break;
        
      default:
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"No basis evaluation methods for RT_k, k > 0");
        break;
    }
  }
  *_nbasis = bpe;
  *_N = N;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetTabulateBasisDerivatives2d_RaviartThomas"
PetscErrorCode DMTetTabulateBasisDerivatives2d_RaviartThomas(
  DM dm,PetscInt npoints,PetscReal xi[],
  PetscInt *_nbasis,PetscReal ***_GNx,PetscReal ***_GNy)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal **GNx,**GNy;
  PetscInt q,bpe;
  const PetscInt dimension = 2;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  bpe = tet->space->bpe;
  ierr = PetscMalloc1(npoints,&GNx);CHKERRQ(ierr);
  ierr = PetscMalloc1(npoints,&GNy);CHKERRQ(ierr);
  for (q=0; q<npoints; q++) {
    ierr = PetscMalloc1(dimension*bpe,&GNx[q]);CHKERRQ(ierr);
    ierr = PetscMalloc1(dimension*bpe,&GNy[q]);CHKERRQ(ierr);
  }
  
  for (q=0; q<npoints; q++) {
    switch (tet->basis_order) {
        
      case 0:
        EvaluateLocalBasisDerivatives_RT0(&xi[2*q],
          &GNx[q][dimension*0],&GNx[q][dimension*1],&GNx[q][dimension*2],
          &GNy[q][dimension*0],&GNy[q][dimension*1],&GNy[q][dimension*2]);
        break;
        
      default:
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"No basis evaluation methods for RT_k, k > 0");
        break;
    }
  }
  *_nbasis = bpe;
  *_GNx = GNx;
  *_GNy = GNy;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GetBasisSign_RT0"
PetscErrorCode GetBasisSign_RT0(PetscInt gedof[],PetscInt sign[])
{
  /* flip sign as required */
  //PetscInt gedof = &tet->geometry->element[3*e];
  //PetscInt sedof = &tet->space->element[basis_per_element*e];
  
  sign[0] = sign[1] = sign[2] = 1;
  
  // apply flip rule
  if (gedof[1] > gedof[2]) {/*leave*/}
  else { sign[0] = -1; }
  
  if (gedof[2] > gedof[0]) {/*leave*/}
  else { sign[1] = -1; }
  
  if (gedof[0] > gedof[1]) {/*leave*/}
  else { sign[2] = -1; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateBasis_RT0"
PetscErrorCode EvaluateBasis_RT0(DM dm,PetscInt e,PetscReal ecoor[],PetscReal Nlocal[],PetscReal N[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal J[4],detJ;
  PetscInt k;
  const PetscInt dimension = 2;
  PetscInt sign[3];

  
  GetBasisSign_RT0(&tet->geometry->element[3*e],sign);
  PiolaTransformation_EvaluateJ_DetJ(ecoor,J,&detJ);

  /* transform and scale basis */
  for (k=0; k<3; k++) {
    PetscReal *q,*Pq;
    
    q  = &Nlocal[dimension * k];
    Pq = &N[dimension * k];
    
    Pq[0] = J[0]*q[0] + J[1]*q[1];
    Pq[1] = J[2]*q[0] + J[3]*q[1];
    
    Pq[0] = Pq[0] / detJ;
    Pq[1] = Pq[1] / detJ;

    if (sign[k] < 0) {
      Pq[0] = -Pq[0];
      Pq[1] = -Pq[1];
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateSolution_RT0"
PetscErrorCode EvaluateSolution_RT0(DM dm,PetscInt e,PetscReal ecoor[],PetscReal Nlocal[],PetscReal eval[],PetscReal phi[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal J[4],detJ;
  PetscInt k;
  const PetscInt dimension = 2;
  PetscInt sign[3];
  
  
  GetBasisSign_RT0(&tet->geometry->element[3*e],sign);
  PiolaTransformation_EvaluateJ_DetJ(ecoor,J,&detJ);
  
  phi[0] = phi[1] = 0.0;
  /* transform and scale basis */
  for (k=0; k<3; k++) {
    PetscReal *q,Pq[2],BkPq[2];
    
    q  = &Nlocal[dimension * k];

    Pq[0] = q[0] * eval[k];
    Pq[1] = q[1] * eval[k];
    
    BkPq[0] = J[0]*Pq[0] + J[1]*Pq[1];
    BkPq[1] = J[2]*Pq[0] + J[3]*Pq[1];
    
    BkPq[0] = BkPq[0] / detJ;
    BkPq[1] = BkPq[1] / detJ;

    /* apply sign change as required */
    if (sign[k] < 0) {
      BkPq[0] = -BkPq[0];
      BkPq[1] = -BkPq[1];
    }
    
    phi[0] += BkPq[0];
    phi[1] += BkPq[1];
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateBasisDerivative_RT0"
PetscErrorCode EvaluateBasisDerivative_RT0(DM dm,PetscInt e,PetscReal ecoor[],
                                           PetscReal dNlocalx[],PetscReal dNx[],PetscReal dNlocaly[],PetscReal dNy[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal J[4],detJ;
  PetscInt k,d;
  const PetscInt dimension = 2;
  PetscInt sign[3];
  
  
  GetBasisSign_RT0(&tet->geometry->element[3*e],sign);
  PiolaTransformation_EvaluateJ_DetJ(ecoor,J,&detJ);
  
  /* scale basis */
  for (k=0; k<3; k++) {
    PetscReal *qx,*qy;
    PetscReal *Pqx,*Pqy;

    qx  = &dNlocalx[dimension * k];
    qy  = &dNlocaly[dimension * k];
    
    Pqx = &dNx[dimension * k];
    Pqy = &dNy[dimension * k];
    
    for (d=0; d<dimension; d++) {
      Pqx[d] = qx[d] / detJ;
      Pqy[d] = qy[d] / detJ;
    }

    if (sign[k] < 0) {
      for (d=0; d<dimension; d++) {
        Pqx[d] = -Pqx[d];
        Pqy[d] = -Pqy[d];
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateSolutionGradient_RT0"
PetscErrorCode EvaluateSolutionGradient_RT0(DM dm,PetscInt e,PetscReal ecoor[],
                                            PetscReal dNlocalx[],PetscReal dNlocaly[],
                                            PetscReal eval[],PetscReal grad_phi[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscReal J[4],detJ;
  PetscInt k,d;
  const PetscInt dimension = 2;
  PetscInt sign[3];
  
  grad_phi[0] = grad_phi[1] = 0.0;
  GetBasisSign_RT0(&tet->geometry->element[3*e],sign);
  PiolaTransformation_EvaluateJ_DetJ(ecoor,J,&detJ);
  
  /* scale basis */
  for (k=0; k<3; k++) {
    PetscReal *qx,*qy;
    PetscReal Pqx[2],Pqy[2];
    
    qx  = &dNlocalx[dimension * k];
    qy  = &dNlocaly[dimension * k];
    
    for (d=0; d<dimension; d++) {
      Pqx[d] = qx[d] / detJ;
      Pqy[d] = qy[d] / detJ;
    }
    
    if (sign[k] < 0) {
      for (d=0; d<dimension; d++) {
        Pqx[d] = -Pqx[d];
        Pqy[d] = -Pqy[d];
      }
    }
  
    grad_phi[0] += Pqx[0] * eval[k];
    grad_phi[1] += Pqy[1] * eval[k];
  }
  PetscFunctionReturn(0);
}
