
#include <khash.h>
#include <data_exchanger.h>
#include <petsc.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>
#include <dmtet_common.h>

#if defined(PETSC_HAVE_METIS)
  #include <metis.h>
  typedef idx_t  MetisPetscInt;
  typedef real_t MetisScalar;
#endif

#if defined(PETSC_HAVE_PTSCOTCH)
  #include <ptscotch.h>
#endif


KHASH_MAP_INIT_INT64(64, long int)


#undef __FUNCT__
#define __FUNCT__ "DMTetPartInfoCreate"
PetscErrorCode DMTetPartInfoCreate(DMTetPartInfo *info)
{
  DMTetPartInfo ii;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&ii);CHKERRQ(ierr);
  ierr = PetscMemzero(ii,sizeof(struct _p_DMTetPartInfo));CHKERRQ(ierr);
  
  ii->dm = NULL;
  ii->epart = NULL;
  
  ii->node_element_map_size = NULL;
  ii->node_element_map = NULL;
  
  ii->element_element_map_size = NULL;
  ii->element_element_map = NULL;
  ii->refcnt = 1;
  
  *info = ii;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetPartInfoDestroy"
PetscErrorCode DMTetPartInfoDestroy(DMTetPartInfo *info)
{
  PetscErrorCode ierr;
  DMTetPartInfo ii;
  PetscInt k;
  
  if (!info) PetscFunctionReturn(0);
  ii = *info;
  if ((--ii->refcnt) > 0) {
    *info = NULL;
    PetscFunctionReturn(0);
  }
  
  if (ii->epart) {
    ierr = PetscFree(ii->epart);CHKERRQ(ierr);
    ii->epart = NULL;
  }
  
  if (ii->node_element_map) {
    for (k=0; k<ii->nnodes; k++) {
      free(ii->node_element_map[k]);
    }
    free(ii->node_element_map);
  }
  if (ii->node_element_map_size) free(ii->node_element_map_size);
  
  if (ii->element_element_map) {
    for (k=0; k<ii->nelements; k++) {
      free(ii->element_element_map[k]);
    }
    free(ii->element_element_map);
  }
  if (ii->element_element_map_size) free(ii->element_element_map_size);
  
  ierr = PetscFree(ii);CHKERRQ(ierr);
  *info = NULL;
  
  PetscFunctionReturn(0);
}

/* generic */
#undef __FUNCT__
#define __FUNCT__ "DMTetMapCreate_NodeElement"
PetscErrorCode DMTetMapCreate_NodeElement(PetscInt ne,PetscInt npe,PetscInt elndmap[],PetscInt nnodes,PetscInt **_mapcnt,PetscInt ***_map)
{
  PetscInt n,e,ce;
  PetscInt *mapcnt;
  PetscInt *mapcnt_max;
  PetscInt **map;
  
  mapcnt = malloc(sizeof(PetscInt)*nnodes);
  mapcnt_max = malloc(sizeof(PetscInt)*nnodes);
  for (n=0; n<nnodes; n++) {
    mapcnt[n] = 0; /* current count */
    mapcnt_max[n] = 6; /* current allocation */
  }
  
  map = malloc(sizeof(PetscInt*)*nnodes);
  for (n=0; n<nnodes; n++) {
    map[n] = malloc(sizeof(PetscInt)*mapcnt_max[n]);
  }
  
  for (e=0; e<ne; e++) {
    PetscInt *elnodes;
    
    elnodes = &elndmap[e*npe];
    
    for (n=0; n<npe; n++) {
      PetscInt nid,found;
      
      nid = elnodes[n];
      
      /* check if e is in current list for connected elements with node index nid */
      found = 0;
      for (ce=0; ce<mapcnt[nid]; ce++) {
        if (map[nid][ce] == e) {
          found = 1;
          break;
        }
      }
      
      if (found == 0) {
        /* check size and realloc */
        if (mapcnt[nid] >= mapcnt_max[nid]) {
          PetscInt *buffer;
          
          mapcnt_max[nid] = mapcnt_max[nid] + 6;
          buffer = realloc(map[nid],sizeof(PetscInt)*(mapcnt_max[nid]));
          
          map[nid] = buffer;
        }
        
        map[nid][mapcnt[nid]] = e;
        mapcnt[nid]++;
      }
    }
  }
  
  free(mapcnt_max);
  
  /* sanity check */
  for (n=0; n<nnodes; n++) {
    if (mapcnt[n] < 1) PetscPrintf(PETSC_COMM_SELF,"Warning: Node [%D] must be connected to at least 1 element\n",n);
  }
  
  *_mapcnt = mapcnt;
  *_map = map;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetPartInfoSetup_phase1"
PetscErrorCode DMTetPartInfoSetup_phase1(DMTetPartInfo info)
{
  PetscErrorCode ierr;
  PetscInt npe,nnodes,nelements,*elndmap;
  PetscLogDouble t0,t1;
  PetscMPIInt commsize;
  DM dm;
  
  dm = info->dm;
  if (!dm) PetscFunctionReturn(0);
  
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Input DM is expected to be defined on PETSC_COMM_SELF");

  ierr = DMTetGeometryElement(dm,&nelements,&npe,&elndmap,NULL,&nnodes,NULL);CHKERRQ(ierr);
  info->nelements = nelements;
  info->nnodes = nnodes;
  
  PetscTime(&t0);
  ierr = DMTetMapCreate_NodeElement(nelements,npe,elndmap,nnodes,&info->node_element_map_size,&info->node_element_map);CHKERRQ(ierr);
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"DMTetPartInfoSetup [N->E] %1.4e (sec)\n",t1-t0);
  
  PetscFunctionReturn(0);
}

#if defined(PETSC_HAVE_METIS)
#undef __FUNCT__
#define __FUNCT__ "DMTetPartition_Metis"
PetscErrorCode DMTetPartition_Metis(DM dm,PetscInt npartitions,PetscInt element_partitions[])
{
  PetscErrorCode ierr;
  MetisPetscInt options[METIS_NOPTIONS];
  int metis_ierr;
  MetisPetscInt nelements_m,nnodes_m,npe_m;
  MetisPetscInt *eptr_m,*eind_m;
  MetisPetscInt npartitions_m,ncommon_m,objval_m;
  MetisPetscInt *element_partitions_m,*node_partitions_m;
  PetscInt e,n,cnt,nelements,npe,*geom_element,nnodes,border,dim;
  
  
  if (!dm) PetscFunctionReturn(0);
  ierr = DMTetGeometryElement(dm,&nelements,&npe,&geom_element,NULL,&nnodes,NULL);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_SELF,"  nelements   %D\n",nelements);
  PetscPrintf(PETSC_COMM_SELF,"  nnodes      %D\n",nnodes);
  PetscPrintf(PETSC_COMM_SELF,"  npartitions %D\n",npartitions);
  
  if (npartitions > nelements) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Number of partitions must be less than or equal to the number of elements");
  
  metis_ierr = METIS_SetDefaultOptions(options);
  
  options[METIS_OPTION_PTYPE]   = METIS_PTYPE_KWAY; // {METIS_PTYPE_KWAY , METIS_PTYPE_RB}
  options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT; // {METIS_OBJTYPE_CUT , METIS_OBJTYPE_VOL}
  options[METIS_OPTION_CTYPE]   = METIS_CTYPE_SHEM; // {METIS_CTYPE_SHEM, METIS_CTYPE_RM}
  options[METIS_OPTION_IPTYPE]  = METIS_IPTYPE_GROW; // {METIS_IPTYPE_GROW, METIS_IPTYPE_RANDOM, METIS_IPTYPE_EDGE, METIS_IPTYPE_NODE}
  options[METIS_OPTION_RTYPE]   = METIS_RTYPE_FM; // {METIS_RTYPE_FM, METIS_RTYPE_GREEDY, METIS_RTYPE_SEP2SIDED, METIS_RTYPE_SEP1SIDED}
  options[METIS_OPTION_NCUTS]   = 1;
  //options[METIS_OPTION_NSEPS] =
  options[METIS_OPTION_NUMBERING] = 0;
  options[METIS_OPTION_NITER]     = 10;
  options[METIS_OPTION_SEED]      = -1;
  //options[METIS_OPTION_MINCONN] = ;
  //options[METIS_OPTION_NO2HOP] = ;
  options[METIS_OPTION_CONTIG] = 1; /* 1 => force contiguous partitions */
  //options[METIS_OPTION_COMPRESS] = ;
  //options[METIS_OPTION_CCORDER] = ;
  //options[METIS_OPTION_PFACTOR] = ;
  options[METIS_OPTION_UFACTOR] = 30;
  options[METIS_OPTION_DBGLVL] = 0;
  
  nelements_m = (MetisPetscInt)nelements;
  nnodes_m    = (MetisPetscInt)nnodes;
  npe_m       = (MetisPetscInt)npe;
  
  npartitions_m = (MetisPetscInt)npartitions;
  
  //ncommon_m = 3; /* common nodes required to make an edge in the graph */
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&border);CHKERRQ(ierr);
  if (dim == 2) {
    ncommon_m = (border+1); /* number of points on a shared edge (2d) */
  }
  else if (dim == 3) {
    ncommon_m = (border+1)*(border+2)/2; /* number of points on a shared face (3d) */
  }
  
  eptr_m = malloc(sizeof(MetisPetscInt)*(nelements_m+1));
  eind_m = malloc(sizeof(MetisPetscInt)*(nelements_m*npe_m));
  
  cnt = 0;
  for (e=0; e<nelements; e++) {
    
    eptr_m[e] = e*npe;
    
    for(n=0; n<npe; n++) {
      eind_m[cnt] = (MetisPetscInt)geom_element[e*npe + n];
      cnt++;
    }
  }
  eptr_m[nelements] = nelements*npe;
  
  element_partitions_m = malloc(sizeof(MetisPetscInt)*nelements_m);
  node_partitions_m    = malloc(sizeof(MetisPetscInt)*nnodes_m);
  
  metis_ierr = METIS_PartMeshDual(&nelements_m,&nnodes_m, eptr_m, eind_m,
                                  NULL, NULL, &ncommon_m, &npartitions_m,
                                  NULL, options, &objval_m,
                                  element_partitions_m, node_partitions_m);
  
  switch (metis_ierr) {
    case METIS_OK:
      PetscPrintf(PETSC_COMM_SELF,"  status: METIS_OK\n");
      break;
    case METIS_ERROR_INPUT:
      PetscPrintf(PETSC_COMM_SELF,"  status: METIS_ERROR_INPUT\n");
      break;
    case METIS_ERROR_MEMORY:
      PetscPrintf(PETSC_COMM_SELF,"  status: METIS_ERROR_MEMORY\n");
      break;
    case METIS_ERROR:
      PetscPrintf(PETSC_COMM_SELF,"  status: METIS_ERROR\n");
      break;
  }
  
  for (e=0; e<nelements; e++) {
    element_partitions[e] = (PetscInt)element_partitions_m[e];
  }
  
  free(eind_m);
  free(eptr_m);
  free(element_partitions_m);
  free(node_partitions_m);
  
  PetscFunctionReturn(0);
}
#else
#undef __FUNCT__
#define __FUNCT__ "DMTetPartition_Metis"
PetscErrorCode DMTetPartition_Metis(DM dm,PetscInt npartitions,PetscInt element_partitions[])
{
  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"METIS library was not detected - please recompile");
  PetscFunctionReturn(0);
}
#endif

#if defined(PETSC_HAVE_PTSCOTCH)

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateDualMesh_MatchDuplicates"
PetscErrorCode DMTetCreateDualMesh_MatchDuplicates(PetscInt ni,PetscInt nidx[],PetscInt nj,PetscInt njdx[],PetscInt *duplcates)
{
  PetscInt i,j,cnt;
  
  *duplcates = 0;
  
  cnt = 0;
  for (i=0; i<ni; i++) {
    for (j=0; j<nj; j++) {
      if (nidx[i] == njdx[j]) {
        cnt++;
        break;
      }
    }
  }
  *duplcates = cnt;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateDualMesh_CountDuplicates"
PetscErrorCode DMTetCreateDualMesh_CountDuplicates(PetscInt ni,PetscInt nidx[],PetscInt nj,PetscInt njdx[],PetscInt *duplcates)
{
  PetscInt i,j,cnt,last;
  
  *duplcates = 0;
  
  last = 0;
  cnt = 0;
  for (i=0; i<ni; i++) {
    for (j=last; j<nj; j++) {
      if (nidx[i] == njdx[j]) {
        last = j-1;
        cnt++;
        break;
      }
    }
  }
  *duplcates = cnt;
  
  PetscFunctionReturn(0);
}

int qsort_ComparePetscInt(const void * a, const void * b)
{
  return ( *(PetscInt*)a - *(PetscInt*)b );
}

/*
 - Copy element->node map
 - Sort each pair
 - Create node->element map
 
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetCreateDualMesh_2d"
PetscErrorCode DMTetCreateDualMesh_2d(DM dm,PetscInt ncommon,PetscInt **_n_el_neigh,PetscInt ***_el_neigh)
{
  PetscInt e,i,t,nelements,npe,*element,nnodes;
  PetscInt *ne_cnt,**ne_map;
  PetscInt *ee_cnt,**ee_map;
  PetscErrorCode ierr;
  PetscLogDouble t0,t1;
  PetscInt max_ne,max_ee;
  PetscInt *element_sorted;
  
  ierr = DMTetGeometryElement(dm,&nelements,&npe,&element,0,&nnodes,0);CHKERRQ(ierr);
  
  PetscMalloc(sizeof(PetscInt)*nelements*npe,&element_sorted);
  PetscMemcpy(element_sorted,element,sizeof(PetscInt)*nelements*npe);
  for (e=0; e<nelements; e++) {
    PetscInt *idx;
    
    idx = &element[npe*e];
    
    qsort(idx, npe, sizeof(PetscInt), qsort_ComparePetscInt);
  }
  /* create space for node->element map */
  PetscTime(&t0);
  PetscMalloc(sizeof(PetscInt)*nnodes,&ne_cnt);
  PetscMemzero(ne_cnt,sizeof(PetscInt)*nnodes);
  
  ne_map = malloc(sizeof(PetscInt*)*nnodes);
  for (i=0; i<nnodes; i++) {
    ne_map[i] = malloc(sizeof(PetscInt));
    ne_map[i][0] = 0;
  }
  
  /* generate node->element map */
  for (e=0; e<nelements; e++) {
    PetscInt *idx;
    
    idx = &element_sorted[npe*e];
    
    /* for each node - add element index if its not present */
    for (i=0; i<npe; i++) {
      PetscInt nidx_i;
      PetscBool add_value = PETSC_TRUE;
      
      nidx_i = idx[i];
      
      for (t=0; t<ne_cnt[nidx_i]; t++) {
        if (ne_map[nidx_i][t] == e) {
          add_value = PETSC_FALSE;
          break;
        }
      }
      if (add_value) {
        PetscInt *tmp;
        
        tmp = realloc(ne_map[nidx_i],sizeof(PetscInt)*(ne_cnt[nidx_i]+1));
        ne_map[nidx_i] = tmp;
        
        ne_map[nidx_i][ne_cnt[nidx_i]] = e;
        ne_cnt[nidx_i]++;
        
      }
    }
  }
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_SELF,"  Generated node->element map %1.4e (sec)\n",t1-t0);
  
  PetscTime(&t0);
  for (i=0; i<nnodes; i++) {
    qsort(ne_map[i], ne_cnt[i], sizeof(PetscInt), qsort_ComparePetscInt);
  }
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_SELF,"  Sorted node->element map %1.4e (sec)\n",t1-t0);
  
  
  max_ne = 0;
  for (i=0; i<nnodes; i++) {
    if (ne_cnt[i] > max_ne) { max_ne = ne_cnt[i]; }
  }
  
  /* for each element, e
   for each element->node, n
   get all elements connected to node, n->e()
   if a n->e() shares ncommon nodal entries with e, it's an element neighbour
   */
  
  PetscTime(&t0);
  PetscMalloc(sizeof(PetscInt)*nelements,&ee_cnt);
  PetscMemzero(ee_cnt,sizeof(PetscInt)*nelements);
  
  ee_map = malloc(sizeof(PetscInt*)*nelements);
  for (i=0; i<nelements; i++) {
    ee_map[i] = malloc(sizeof(PetscInt));
    ee_map[i][0] = 0;
  }
  
  for (e=0; e<nelements; e++) {
    PetscInt *idx;
    
    idx = &element_sorted[npe*e];
    
    for (i=0; i<npe; i++) {
      PetscInt ti,nidx_i;
      PetscInt *neighbour_candidates;
      
      nidx_i = idx[i];
      
      neighbour_candidates = ne_map[nidx_i];
      
      for (t=0; t<ne_cnt[nidx_i]; t++) {
        PetscInt neighbour_e,duplicates;
        PetscInt *nidx;
        PetscBool add_value = PETSC_TRUE;
        
        /* don't add yourself! */
        if (neighbour_candidates[t] == e) { continue; }
        
        neighbour_e = neighbour_candidates[t];
        nidx = &element_sorted[npe*neighbour_e];
        /* look for ncommon duplicates between idx and nidx */
        
        ierr = DMTetCreateDualMesh_MatchDuplicates(npe,idx,npe,nidx,&duplicates);CHKERRQ(ierr);
        //ierr = DMTetCreateDualMesh_CountDuplicates(npe,idx,npe,nidx,&duplicates);CHKERRQ(ierr);
        if (duplicates >= ncommon) {
          add_value = PETSC_TRUE;
        } else {
          continue;
        }
        
        /* check if it already exists */
        for (ti=0; ti<ee_cnt[e]; ti++) {
          if (ee_map[e][ti] == neighbour_e) {
            add_value = PETSC_FALSE;
            break;
          }
        }
        
        if (add_value) {
          PetscInt *tmp;
          
          tmp = realloc(ee_map[e],sizeof(PetscInt)*(ee_cnt[e]+1));
          ee_map[e] = tmp;
          
          ee_map[e][ee_cnt[e]] = neighbour_e;
          ee_cnt[e]++;
        }
      }
    }
  }
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_SELF,"  Generated element->element map %1.4e (sec)\n",t1-t0);
  max_ee = 0;
  for (i=0; i<nelements; i++) {
    if (ee_cnt[i] > max_ee) { max_ee = ee_cnt[i]; }
  }
  PetscPrintf(PETSC_COMM_SELF,"    max element->element count %D\n",max_ee);
  
  max_ee = 1e6;
  for (i=0; i<nelements; i++) {
    if (ee_cnt[i] < max_ee) { max_ee = ee_cnt[i]; }
  }
  PetscPrintf(PETSC_COMM_SELF,"    min element->element count %D\n",max_ee);
  
  for (i=0; i<nnodes; i++) {
    free(ne_map[i]);
  }
  free(ne_map);
  PetscFree(ne_cnt);
  
  *_n_el_neigh = ee_cnt;
  *_el_neigh   = ee_map;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetDestroyDualMesh"
PetscErrorCode DMTetDestroyDualMesh(DM dm,PetscInt **n_el_neigh,PetscInt ***el_neigh)
{
  PetscErrorCode ierr;
  PetscInt e,nelements;
  PetscInt *ee_cnt = NULL,**ee_map = NULL;
  
  ierr = DMTetGeometryElement(dm,&nelements,0,0,0,0,0);CHKERRQ(ierr);
  
  if (n_el_neigh) { ee_cnt = *n_el_neigh; }
  if (el_neigh)   { ee_map = *el_neigh;   }
  
  if (ee_map) {
    for (e=0; e<nelements; e++) {
      free(ee_map[e]);
    }
    free(ee_map);
  }
  if (ee_cnt) {
    ierr = PetscFree(ee_cnt);CHKERRQ(ierr);
  }
  
  *n_el_neigh = NULL;
  *el_neigh   = NULL;
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetPartition_PTScotch"
PetscErrorCode DMTetPartition_PTScotch(DM dm,PetscInt npartitions,PetscInt element_partitions[])
{
  PetscErrorCode ierr;
  PetscInt e,i,nelements,npe,*geom_element,nnodes;
  PetscInt *ee_cnt,**ee_map,ncommon;
  
  SCOTCH_Graph        grafdat;                    /* Scotch graph object to interface with libScotch */
  SCOTCH_Strat        stradat;
  SCOTCH_Num          baseval;
  /*SCOTCH_Num          vertnbr;*/
  int                 scotch_ierr;
  
  SCOTCH_Num        npartitions_s,nelements_s,nedges_s,cnt,*xadj,*adjncy,*part_s;
  SCOTCH_Num        flagval;
  double            kbalval;
  
  
  if (!dm) PetscFunctionReturn(0);
  PetscPrintf(PETSC_COMM_SELF,"  [%s]:\n",__FUNCTION__);
  ierr = DMTetGeometryElement(dm,&nelements,&npe,&geom_element,0,&nnodes,0);CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_SELF,"    nelements   %D\n",nelements);
  PetscPrintf(PETSC_COMM_SELF,"    nnodes      %D\n",nnodes);
  PetscPrintf(PETSC_COMM_SELF,"    npartitions %D\n",npartitions);
  
  ncommon = 1;  /* 2 vertices => matched edge [2d] : 3 vertices => matched face [3d] */
  ierr = DMTetCreateDualMesh_2d(dm,ncommon,&ee_cnt,&ee_map);CHKERRQ(ierr);
  
  nelements_s = (SCOTCH_Num)nelements;
  npartitions_s = (SCOTCH_Num)npartitions;
  
  
  /* create adj arrays */
  xadj = malloc(sizeof(SCOTCH_Num)*(nelements+1));
  
  cnt = 0;
  for (e=0; e<nelements; e++) {
    xadj[e] = cnt;
    cnt = cnt + ee_cnt[e];
  }
  xadj[nelements] = cnt;
  
  /* create adjncy arrays */
  nedges_s = 0;
  for (e=0; e<nelements; e++) {
    nedges_s = nedges_s + ee_cnt[e];
  }
  adjncy = malloc(sizeof(SCOTCH_Num)*(2*nedges_s));
  
  cnt = 0;
  for (e=0; e<nelements; e++) {
    for (i=0; i<ee_cnt[e]; i++) {
      adjncy[cnt] = (SCOTCH_Num)ee_map[e][i];
      cnt++;
    }
  }
  
  /*
   for (e=0; e<nelements+1; e++) {
   printf("xadj[%d] = %d \n",e,xadj[e]);
   }
   printf("\n");
   for (i=0; i<nedges_s; i++) {
   printf("adjncy[%d] = %d \n",i,adjncy[i]);
   }
   */
  
  /* allocate space for partition */
  part_s = malloc(sizeof(SCOTCH_Num)*(nelements_s));
  
  
  /*
   METISNAMEU (METIS_PartGraphKway) -->
   _SCOTCH_METIS_PartGraph (n, xadj, adjncy, vwgt, adjwgt, wgtflag, numflag, nparts, options, edgecut, part,
   SCOTCH_STRATDEFAULT, 0.01);
   
   
   */
  
  baseval = 0; /* C indices */
  scotch_ierr = SCOTCH_graphBuild(&grafdat,
                                  baseval,
                                  nelements_s, /* vertnbr - number of vertices */
                                  xadj, /* verttab - adj index array - size of vertnbr+1 */
                                  NULL, /* vendtab - the adjacency end index array, of size vertnbr if it is disjoint from verttab */
                                  NULL, /* velotab is the vertex load array, of size vertnbr if it exists */
                                  NULL, /* vlbltab is the vertex label array, of size vertnbr if it exists */
                                  nedges_s, /* edgenbr is the number of arcs (that is, twice the number of edges). */
                                  adjncy, /* edgetab is the adjacency array, of size at least edgenbr (it can be more if the edge array is not compact */
                                  NULL);CHKERRQ(scotch_ierr); /* edlotab is the arc load array, of size edgenbr if it exists. */
  
  scotch_ierr = SCOTCH_graphCheck(&grafdat);CHKERRQ(scotch_ierr);
  
  scotch_ierr = SCOTCH_stratInit(&stradat);CHKERRQ(scotch_ierr);
  
  flagval = SCOTCH_STRATDEFAULT;
  kbalval = 0.01;
  scotch_ierr = SCOTCH_stratGraphMapBuild(&stradat,flagval,npartitions_s,kbalval);CHKERRQ(scotch_ierr);
  
  scotch_ierr = SCOTCH_graphPart(&grafdat, npartitions_s, &stradat, part_s);CHKERRQ(scotch_ierr);
  
  SCOTCH_stratExit(&stradat);
  SCOTCH_graphExit(&grafdat);
  
  for (e=0; e<nelements; e++) {
    element_partitions[e] = (PetscInt)part_s[e];
  }
  
  /*
   PetscPrintf(PETSC_COMM_SELF,"  partition\n");
   for (e=0; e<nelements; e++) {
   PetscPrintf(PETSC_COMM_SELF,"    %D\n",element_partitions[e]);
   }
   */
  
  ierr = DMTetDestroyDualMesh(dm,&ee_cnt,&ee_map);CHKERRQ(ierr);
  free(part_s);
  free(xadj);
  free(adjncy);
  
  PetscFunctionReturn(0);
}
#else
#undef __FUNCT__
#define __FUNCT__ "DMTetPartition_PTScotch"
PetscErrorCode DMTetPartition_PTScotch(DM dm,PetscInt npartitions,PetscInt element_partitions[])
{
  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"PTSCOTCH library was not detected - please recompile");
  PetscFunctionReturn(0);
}
#endif

#undef __FUNCT__
#define __FUNCT__ "DMTetPartInfoSetup"
PetscErrorCode DMTetPartInfoSetup(DMTetPartInfo info,DM dm,MPI_Comm comm)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize,commrank;
  PetscBool found,use_metis = PETSC_TRUE,use_scotch = PETSC_FALSE;
  PetscInt partition_package = 0;
  
  info->dm = dm;

  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  info->npartitions = (PetscInt)commsize;

  // phase 1
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  if (commrank == 0) {
    ierr = DMTetPartInfoSetup_phase1(info);CHKERRQ(ierr);
  }

  // broadcast nelements, nnodes
  ierr = MPI_Bcast(&info->nelements,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  ierr = MPI_Bcast(&info->nnodes,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  
  // allocate epart
  ierr = PetscMalloc1(info->nelements,&info->epart);CHKERRQ(ierr);
  ierr = PetscMemzero(info->epart,sizeof(PetscInt)*info->nelements);CHKERRQ(ierr);

  // define epart
  ierr = PetscOptionsGetBool(NULL,NULL,"-dmtet.partition_type.metis",&use_metis,&found);CHKERRQ(ierr);
  if (use_metis) { partition_package = 0; }
  found = PETSC_FALSE;
  ierr = PetscOptionsGetBool(NULL,NULL,"-dmtet.partition_type.ptscotch",&use_scotch,&found);CHKERRQ(ierr);
  if (use_scotch) { partition_package = 1; }

  if (commrank == 0 && (info->npartitions > 1)) {
    PetscLogDouble t0,t1;

    switch (partition_package) {
      case 0:
        PetscTime(&t0);
        ierr = DMTetPartition_Metis(dm,info->npartitions,info->epart);CHKERRQ(ierr);
        PetscTime(&t1);
        PetscPrintf(PETSC_COMM_SELF,"DMTetPartition[metis]: %1.4e (sec)\n",t1-t0);
        break;

      case 1:
        PetscTime(&t0);
        ierr = DMTetPartition_PTScotch(dm,info->npartitions,info->epart);CHKERRQ(ierr);
        PetscTime(&t1);
        PetscPrintf(PETSC_COMM_SELF,"DMTetPartition[ptscotch]: %1.4e (sec)\n",t1-t0);
        break;

      default:
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Unknown partitioner specified");
        break;
    }
  }
  
  // broadcast epart
  ierr = MPI_Bcast(info->epart,info->nelements,MPIU_INT,0,comm);CHKERRQ(ierr);
 
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetPartitionView2d_VTK"
PetscErrorCode DMTetPartitionView2d_VTK(DM dm,PetscInt epart[],const char name[])
{
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt i,e;
  PetscInt nelements,npe,*element,nnodes,*etags;
  PetscReal *coords;

  
  if (!dm) PetscFunctionReturn(0);
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  ierr = DMTetGeometryElement(dm,&nelements,&npe,&element,0,&nnodes,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dm,NULL,&etags,NULL);CHKERRQ(ierr);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes,nelements);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                 element[3*e],element[3*e+1],element[3*e+2]);
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"partition\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)epart[e]);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"eltag\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)etags[e]);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");

  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e %1.10e ",coords[2*i],coords[2*i+1],0.0);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "DMTetPartitionCreate"
PetscErrorCode DMTetPartitionCreate(MPI_Comm comm,DM dm_sequential,DMTetPartition *p)
{
  PetscMPIInt rank;
  DMTetPartition ii;
  PetscInt dim,npe;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&ii);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
  ii->rank = rank;
  
  npe = 0;
  if (dm_sequential) {
    ierr = DMTetGeometryElement(dm_sequential,0,&npe,0,0,0,0);CHKERRQ(ierr);
  }
  ii->npe = npe;
  ierr = MPI_Bcast(&ii->npe,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  
  dim = 0;
  if (dm_sequential) {
    ierr = DMGetDimension(dm_sequential,&dim);CHKERRQ(ierr);
  }
  ii->dim = dim;
  ierr = MPI_Bcast(&ii->dim,1,MPIU_INT,0,comm);CHKERRQ(ierr);
  ii->refcnt = 1;
  
  *p = ii;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetPartitionDestroy"
PetscErrorCode DMTetPartitionDestroy(DMTetPartition *_p)
{
  DMTetPartition p;
  PetscErrorCode ierr;
  
  if (!_p) PetscFunctionReturn(0);
  p = *_p;
  if ((--p->refcnt) > 0) {
    *_p = NULL;
    PetscFunctionReturn(0);
  }
  
  ierr = PetscFree(p->element_map_local);CHKERRQ(ierr);
  ierr = PetscFree(p->element_map_ghost);CHKERRQ(ierr);
  
  ierr = PetscFree(p->element_rank_ghost);CHKERRQ(ierr);
  
  ierr = PetscFree(p->element_gid_local);CHKERRQ(ierr);
  ierr = PetscFree(p->element_gid_ghost);CHKERRQ(ierr);
  
  ierr = PetscFree(p->vert_coor_local);CHKERRQ(ierr);
  ierr = PetscFree(p->vert_coor_ghost);CHKERRQ(ierr);
  
  ierr = PetscFree(p->vert_gid_local);CHKERRQ(ierr);
  ierr = PetscFree(p->vert_gid_ghost);CHKERRQ(ierr);
  
  ierr = PetscFree(p);CHKERRQ(ierr);
  *_p = NULL;
  PetscFunctionReturn(0);
}

/* methods which compute on rank 0 and scatter to others */
#undef __FUNCT__
#define __FUNCT__ "Extract_SubdomainElements_Seq2MPI"
PetscErrorCode Extract_SubdomainElements_Seq2MPI(MPI_Comm comm,DMTetPartition part,DM dm,PetscInt epart[])
{
  PetscErrorCode ierr;
  PetscInt nelements,e,i,p,max = 0,ecnt,ecnt_p;
  PetscMPIInt csize,crank,dest,source,tag;
  MPI_Status stat;
  
  ierr = MPI_Comm_size(comm,&csize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&crank);CHKERRQ(ierr);
  
  if (crank != 0) {
    source = 0;
    tag = 3*crank;
    ierr = MPI_Recv(&part->nelement_local,1,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
  }
  
  if (crank == 0) {
    ierr = DMTetGeometryElement(dm,&nelements,0,0,0,0,0);CHKERRQ(ierr);
    
    // cnt
    ecnt = 0;
    for (e=0; e<nelements; e++) {
      if (epart[e] == part->rank) {
        ecnt++;
      }
    }
    part->nelement_local = ecnt;
    
    max = 0;
    for (p=1; p<csize; p++) {
      // cnt
      ecnt_p = 0;
      for (e=0; e<nelements; e++) {
        if (epart[e] == p) {
          ecnt_p++;
        }
      }
      if (ecnt_p > max) { max = ecnt_p; }
      dest = p;
      tag = 3*p;
      ierr = MPI_Send(&ecnt_p,1,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
    }
  }
  
  ecnt = part->nelement_local;
  
  ierr = PetscMalloc1(ecnt*part->npe,&part->element_map_local);CHKERRQ(ierr);
  ierr = PetscMalloc1(ecnt,&part->element_gid_local);CHKERRQ(ierr);
  
  if (crank != 0) {
    source = 0;
    tag = 3*crank+1;
    ierr = MPI_Recv(part->element_map_local,ecnt*part->npe,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    tag = 3*crank+2;
    ierr = MPI_Recv(part->element_gid_local,ecnt,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
  }
  
  if (crank == 0) {
    PetscInt npe,*elmap = NULL;
    PetscInt *element_map_local_buf,*element_gid_local_buf;
    
    ierr = DMTetGeometryElement(dm,&nelements,&npe,&elmap,0,0,0);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(max*npe,&element_map_local_buf);CHKERRQ(ierr);
    ierr = PetscMalloc1(max,&element_gid_local_buf);CHKERRQ(ierr);
    
    ecnt = 0;
    for (e=0; e<nelements; e++) {
      if (epart[e] == part->rank) {
        part->element_gid_local[ecnt] = e;
        for (i=0; i<npe; i++) {
          part->element_map_local[npe*ecnt+i] = elmap[npe*e+i];
        }
        ecnt++;
      }
    }
    
    for (p=1; p<csize; p++) {
      ierr = PetscMemzero(element_map_local_buf,max*npe*sizeof(PetscInt));CHKERRQ(ierr);
      ierr = PetscMemzero(element_gid_local_buf,max*sizeof(PetscInt));CHKERRQ(ierr);
      
      ecnt_p = 0;
      for (e=0; e<nelements; e++) {
        if (epart[e] == p) {
          element_gid_local_buf[ecnt_p] = e;
          for (i=0; i<npe; i++) {
            element_map_local_buf[npe*ecnt_p+i] = elmap[npe*e+i];
          }
          ecnt_p++;
        }
      }
      // send
      dest = p;
      tag = 3*p+1;
      ierr = MPI_Send(element_map_local_buf,ecnt_p*npe,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
      tag = 3*p+2;
      ierr = MPI_Send(element_gid_local_buf,ecnt_p,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
    }
    ierr = PetscFree(element_map_local_buf);CHKERRQ(ierr);
    ierr = PetscFree(element_gid_local_buf);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Extract_SubdomainVertices_Seq2MPI"
PetscErrorCode Extract_SubdomainVertices_Seq2MPI(MPI_Comm comm,DMTetPartition part,DM dm,PetscInt epart[])
{
  PetscErrorCode ierr;
  PetscMPIInt csize,crank,dest,source,tag;
  MPI_Status stat;
  PetscInt p,dim,ncnt;
  
  ierr = MPI_Comm_size(comm,&csize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&crank);CHKERRQ(ierr);
  
  dim = part->dim;
  
  if (crank != 0) {
    source = 0;
    // recv lengths
    tag = 3*crank;
    ierr = MPI_Recv(&part->nvert_local,1,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    ncnt = part->nvert_local;
    
    // allocate
    ierr = PetscMalloc1(ncnt,&part->vert_gid_local);CHKERRQ(ierr);
    ierr = PetscMalloc1(ncnt*dim,&part->vert_coor_local);CHKERRQ(ierr);
    
    // recv data
    tag = 3*crank+1;
    ierr = MPI_Recv(part->vert_gid_local,ncnt,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    tag = 3*crank+2;
    ierr = MPI_Recv(part->vert_coor_local,ncnt*dim,MPIU_REAL,source,tag,comm,&stat);CHKERRQ(ierr);
  }
  
  if (crank == 0) {
    PetscInt e,i,d,npe,nnodes,nelements;
    PetscInt *elmap = NULL;
    PetscInt *mark = NULL;
    PetscReal *coors;
    
    ierr = DMTetGeometryElement(dm,&nelements,&npe,&elmap,0,&nnodes,&coors);CHKERRQ(ierr);
    ierr = PetscMalloc1(nnodes,&mark);CHKERRQ(ierr);
    
    for (p=0; p<csize; p++) {
      PetscReal *vert_coor_local_buf;
      PetscInt  *vert_gid_local_buf;
      
      for (i=0; i<nnodes; i++) { mark[i] = -1; }
      
      /* traverse elements and all verts - mark verts touched */
      for (e=0; e<nelements; e++) {
        if (epart[e] == p) {
          for (i=0; i<npe; i++) {
            PetscInt gnid;
            
            gnid = elmap[npe*e+i];
            
            mark[gnid] = gnid;
          }
        }
      }
      
      ncnt = 0;
      for (i=0; i<nnodes; i++) {
        if (mark[i] != -1) {
          ncnt++;
        }
      }
      if (p == 0) {
        part->nvert_local = ncnt;
      } else {
        dest = p;
        // send
        tag = 3*p;
        ierr = MPI_Send(&ncnt,1,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
      }
      
      if (p == 0) {
        ierr = PetscMalloc1(ncnt,&part->vert_gid_local);CHKERRQ(ierr);
        ierr = PetscMalloc1(ncnt*dim,&part->vert_coor_local);CHKERRQ(ierr);
        
        vert_gid_local_buf  = part->vert_gid_local;
        vert_coor_local_buf = part->vert_coor_local;
      } else {
        ierr = PetscMalloc1(ncnt,&vert_gid_local_buf);CHKERRQ(ierr);
        ierr = PetscMalloc1(ncnt*dim,&vert_coor_local_buf);CHKERRQ(ierr);
      }
      
      ncnt = 0;
      for (i=0; i<nnodes; i++) {
        if (mark[i] != -1) {
          
          vert_gid_local_buf[ncnt] = i;
          for (d=0; d<dim; d++) {
            vert_coor_local_buf[dim*ncnt + d] = coors[dim*i + d];
          }
          
          ncnt++;
        }
      }
      
      if (p != 0) {
        dest = p;
        // send
        tag = 3*p+1;
        ierr = MPI_Send(vert_gid_local_buf,ncnt,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
        tag = 3*p+2;
        ierr = MPI_Send(vert_coor_local_buf,ncnt*dim,MPIU_REAL,dest,tag,comm);CHKERRQ(ierr);
        
        ierr = PetscFree(vert_gid_local_buf);CHKERRQ(ierr);
        ierr = PetscFree(vert_coor_local_buf);CHKERRQ(ierr);
      }
      
    }
    ierr = PetscFree(mark);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

/*
 < stage 1 >
 * traverse local elements and all their verts - mark verts touched
 * traverse all marked nodes and their element neighbours - mark non-local neighbours (these are the ghost elements)
 
 < stage 2 >
 * init node_mark[] = -1
 * traverse ghost elements and their nodes, insert 1 into mark[node]
 * traverse local elements and their nodes, insert 0 into mark[node]
 * if node_mark[] = 1, then its a ghost node
 * if node_mark[] = 0, then its a local node
 * if node_mark[] = -1, then its outside the sub-domain and overlap
 
 [Messages being sent : total = 7]
 < stage 1 >
 part->nelement_ghost
 part->element_map_ghost
 part->element_gid_ghost
 part->element_rank_ghost
 
 < stage 2 >
 part->nvert_ghost
 part->vert_coor_ghost
 part->vert_gid_ghost
 
 */
#undef __FUNCT__
#define __FUNCT__ "Extract_GhostElementsVertices_Seq2MPI"
PetscErrorCode Extract_GhostElementsVertices_Seq2MPI(MPI_Comm comm,DMTetPartition part,DMTetPartInfo info,DM dm,PetscInt epart[])
{
  PetscErrorCode ierr;
  PetscMPIInt csize,crank,tag,source,dest;
  MPI_Status stat;
  PetscInt ecnt,ncnt,p,dim;
  
  ierr = MPI_Comm_size(comm,&csize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&crank);CHKERRQ(ierr);
  
  
  dim = part->dim;
  
  if (crank != 0) {
    source = 0;
    
    /* ================================ < stage 1 > ================================ */
    // recv lengths
    tag = 7*crank;
    ierr = MPI_Recv(&part->nelement_ghost,1,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    ecnt = part->nelement_ghost;
    
    // allocate
    ierr = PetscMalloc1(ecnt*part->npe,&part->element_map_ghost);CHKERRQ(ierr);
    ierr = PetscMalloc1(ecnt,&part->element_gid_ghost);CHKERRQ(ierr);
    ierr = PetscMalloc1(ecnt,&part->element_rank_ghost);CHKERRQ(ierr);
    
    // recv data
    tag = 7*crank+1;
    ierr = MPI_Recv(part->element_map_ghost,ecnt*part->npe,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    tag = 7*crank+2;
    ierr = MPI_Recv(part->element_gid_ghost,ecnt,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    tag = 7*crank+3;
    ierr = MPI_Recv(part->element_rank_ghost,ecnt,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    
    /* ================================ < stage 2 > ================================ */
    // recv lengths
    tag = 7*crank+4;
    ierr = MPI_Recv(&part->nvert_ghost,1,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    ncnt = part->nvert_ghost;
    
    // allocate
    ierr = PetscMalloc1(ncnt*dim,&part->vert_coor_ghost);CHKERRQ(ierr);
    ierr = PetscMalloc1(ncnt,&part->vert_gid_ghost);CHKERRQ(ierr);
    
    // recv data
    tag = 7*crank+5;
    ierr = MPI_Recv(part->vert_coor_ghost,ncnt*dim,MPIU_REAL,source,tag,comm,&stat);CHKERRQ(ierr);
    tag = 7*crank+6;
    ierr = MPI_Recv(part->vert_gid_ghost,ncnt,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
  }
  
  if (crank == 0) {
    PetscInt nelements,npe,i,e,d,ne,nnodes;
    PetscInt *emark,*nmark,*elmap;
    PetscReal *coors;
    
    ierr = DMTetGeometryElement(dm,&nelements,&npe,&elmap,0,&nnodes,&coors);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(nnodes,&nmark);CHKERRQ(ierr);
    ierr = PetscMalloc1(nelements,&emark);CHKERRQ(ierr);
    
    for (p=0; p<csize; p++) {
      PetscInt    *element_map_ghost_buf;
      PetscInt    *element_gid_ghost_buf;
      PetscMPIInt *element_rank_ghost_buf;
      PetscReal   *vert_coor_ghost_buf;
      PetscInt    *vert_gid_ghost_buf;
      
      
      for (i=0; i<nnodes; i++) { nmark[i] = -1; }
      for (e=0; e<nelements; e++) { emark[e] = -1; }
      
      /* ================================ < stage 1 > ================================ */
      /* traverse elements and all verts - mark verts touched */
      for (e=0; e<nelements; e++) {
        if (epart[e] == p) {
          for (i=0; i<npe; i++) {
            PetscInt gnid;
            
            gnid = elmap[npe*e+i];
            
            nmark[gnid] = 1;
          }
        }
      }
      
      /* traverse all local nodes and their element neighbours - mark non-local neighbours */
      for (i=0; i<nnodes; i++) {
        PetscInt ngid,neigh_egid;
        
        if (nmark[i] == -1) { continue; }
        
        ngid = i;
        
        /* check neighbours */
        for (ne=0; ne<info->node_element_map_size[ngid]; ne++) {
          neigh_egid = info->node_element_map[ngid][ne];
          
          if (epart[neigh_egid] != p) {
            emark[neigh_egid] = neigh_egid;
          }
        }
      }
      
      ecnt = 0;
      for (e=0; e<nelements; e++) {
        if (emark[e] != -1) {
          ecnt++;
        }
      }
      
      
      if (p == 0) {
        part->nelement_ghost = ecnt;
      } else {
        dest = p;
        // send
        tag = 7*p;
        ierr = MPI_Send(&ecnt,1,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
      }
      
      if (p == 0) {
        ierr = PetscMalloc1(ecnt*part->npe,&part->element_map_ghost);CHKERRQ(ierr);
        ierr = PetscMalloc1(ecnt,&part->element_gid_ghost);CHKERRQ(ierr);
        ierr = PetscMalloc1(ecnt,&part->element_rank_ghost);CHKERRQ(ierr);
        
        element_map_ghost_buf  = part->element_map_ghost;
        element_gid_ghost_buf  = part->element_gid_ghost;
        element_rank_ghost_buf = part->element_rank_ghost;
      } else {
        ierr = PetscMalloc1(ecnt*part->npe,&element_map_ghost_buf);CHKERRQ(ierr);
        ierr = PetscMalloc1(ecnt,&element_gid_ghost_buf);CHKERRQ(ierr);
        ierr = PetscMalloc1(ecnt,&element_rank_ghost_buf);CHKERRQ(ierr);
      }
      
      // fill buffer
      ecnt = 0;
      for (e=0; e<nelements; e++) {
        if (emark[e] != -1) {
          element_gid_ghost_buf[ecnt] = e;
          element_rank_ghost_buf[ecnt] = (PetscMPIInt)epart[e];
          for (i=0; i<npe; i++) {
            element_map_ghost_buf[npe*ecnt+i] = elmap[npe*e+i];
          }
          
          ecnt++;
        }
      }
      
      if (p != 0) {
        dest = p;
        
        // send
        tag = 7*p+1;
        ierr = MPI_Send(element_map_ghost_buf,ecnt*part->npe,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
        tag = 7*p+2;
        ierr = MPI_Send(element_gid_ghost_buf,ecnt,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
        tag = 7*p+3;
        ierr = MPI_Send(element_rank_ghost_buf,ecnt,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
      }
      
      /* ================================ < stage 2 > ================================ */
      /* reset node mark */
      for (i=0; i<nnodes; i++) { nmark[i] = -1; }
      
      /* loop over ghost elements */
      for (e=0; e<ecnt; e++) {
        PetscInt geid,gnid;
        
        geid = element_gid_ghost_buf[e];
        for (i=0; i<npe; i++) {
          gnid = elmap[npe*geid+i];
          
          nmark[gnid] = 1;
        }
      }
      
      /* traverse local elements and all verts - mark verts touched */
      for (e=0; e<nelements; e++) {
        if (epart[e] == p) {
          for (i=0; i<npe; i++) {
            PetscInt gnid;
            
            gnid = elmap[npe*e+i];
            
            nmark[gnid] = 0;
          }
        }
      }
      
      /* count node mark */
      ncnt = 0;
      for (i=0; i<nnodes; i++) {
        if (nmark[i] == 1) { ncnt++; }
      }
      
      
      if (p == 0) {
        part->nvert_ghost = ncnt;
      } else {
        dest = p;
        // send
        tag = 7*p+4;
        ierr = MPI_Send(&ncnt,1,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
      }
      
      if (p == 0) {
        ierr = PetscMalloc1(ncnt*dim,&part->vert_coor_ghost);CHKERRQ(ierr);
        ierr = PetscMalloc1(ncnt,&part->vert_gid_ghost);CHKERRQ(ierr);
        
        vert_coor_ghost_buf  = part->vert_coor_ghost;
        vert_gid_ghost_buf   = part->vert_gid_ghost;
      } else {
        ierr = PetscMalloc1(ncnt*dim,&vert_coor_ghost_buf);CHKERRQ(ierr);
        ierr = PetscMalloc1(ncnt,&vert_gid_ghost_buf);CHKERRQ(ierr);
      }
      
      // fill buffer
      ncnt = 0;
      for (i=0; i<nnodes; i++) {
        if (nmark[i] == 1) {
          
          vert_gid_ghost_buf[ncnt] = i;
          for (d=0; d<dim; d++) {
            vert_coor_ghost_buf[dim*ncnt + d] = coors[dim*i + d];
          }
          ncnt++;
        }
      }
      
      if (p != 0) {
        dest = p;
        
        // send
        tag = 7*p+5;
        ierr = MPI_Send(vert_coor_ghost_buf,ncnt*dim,MPIU_REAL,dest,tag,comm);CHKERRQ(ierr);
        tag = 7*p+6;
        ierr = MPI_Send(vert_gid_ghost_buf,ncnt,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
      }
      
      
      /* free memory buffers */
      if (p != 0) {
        ierr = PetscFree(element_map_ghost_buf);CHKERRQ(ierr);
        ierr = PetscFree(element_gid_ghost_buf);CHKERRQ(ierr);
        ierr = PetscFree(element_rank_ghost_buf);CHKERRQ(ierr);
        
        ierr = PetscFree(vert_coor_ghost_buf);CHKERRQ(ierr);
        ierr = PetscFree(vert_gid_ghost_buf);CHKERRQ(ierr);
      }
      
      
    }
    ierr = PetscFree(nmark);CHKERRQ(ierr);
    ierr = PetscFree(emark);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Distribute_SubdomainElementInts_Seq2MPI"
PetscErrorCode Distribute_SubdomainElementInts_Seq2MPI(DM dm,PetscInt etags[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  PetscInt nelements,e,p,ecnt,ecnt_p,max_length,max_length_g;
  PetscMPIInt csize,crank,dest,source,tag;
  MPI_Status stat;
  DM dm_sequential;
  DMTetPartition part;
  const PetscInt *epart;
  MPI_Comm comm;
  
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&csize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&crank);CHKERRQ(ierr);
  
  epart = tet->pinfo->epart;
  part = tet->partition;
  dm_sequential = tet->dmtet_sequential;
  
  max_length = part->nelement_local + part->nelement_ghost;
  ierr = MPI_Allreduce(&max_length,&max_length_g,1,MPIU_INT,MPI_MAX,comm);CHKERRQ(ierr);
  
  if (crank != 0) {
    source = 0;
    tag = crank;
    ecnt = part->nelement_local + part->nelement_ghost;
    ierr = MPI_Recv(tet->geometry->element_tag,ecnt,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
  }
  
  if (crank == 0) {
    PetscInt *element_local_buf;
    
    ierr = DMTetGeometryElement(dm_sequential,&nelements,0,0,0,0,0);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(max_length_g,&element_local_buf);CHKERRQ(ierr);
    ierr = PetscMemzero(element_local_buf,max_length_g*sizeof(PetscInt));CHKERRQ(ierr);

    /* load tags on rank 0 */
    /*
    ecnt = 0;
    for (e=0; e<nelements; e++) {
      if (epart[e] == part->rank) {
        tet->geometry->element_tag[ecnt] = etags[e];
        ecnt++;
      }
    }
    */
    ecnt = 0;
    for (e=0; e<part->nelement_local; e++) {
      PetscInt eid = part->element_gid_local[e];
      
      tet->geometry->element_tag[e] = etags[eid];
      ecnt++;
    }

    for (e=0; e<part->nelement_ghost; e++) {
      PetscInt eid = part->element_gid_ghost[e];
      
      tet->geometry->element_tag[e+ecnt] = etags[eid];
    }

    for (p=1; p<csize; p++) {
      ierr = PetscMemzero(element_local_buf,max_length_g*sizeof(PetscInt));CHKERRQ(ierr);
      
      ecnt_p = 0;
      for (e=0; e<nelements; e++) {
        if (epart[e] == p) {
          element_local_buf[ecnt_p] = etags[e];
          ecnt_p++;
        }
      }
      
      // send
      dest = p;
      tag = p;
      ierr = MPI_Send(element_local_buf,ecnt_p,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
    }
    ierr = PetscFree(element_local_buf);CHKERRQ(ierr);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Distribute_SubdomainGhostElementInts_Seq2MPI"
PetscErrorCode Distribute_SubdomainGhostElementInts_Seq2MPI(DM dm,PetscInt etags[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  PetscInt e,p,ecnt,max_length,max_length_g;
  PetscMPIInt csize,crank,dest,source,tag;
  MPI_Status stat;
  DMTetPartition part;
  MPI_Comm comm;
  PetscInt *element_local_buf;
  PetscInt *element_id_local_buf;

  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&csize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&crank);CHKERRQ(ierr);
  
  part = tet->partition;
  
  max_length = part->nelement_ghost;
  ierr = MPI_Allreduce(&max_length,&max_length_g,1,MPIU_INT,MPI_MAX,comm);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(max_length_g,&element_local_buf);CHKERRQ(ierr);
  ierr = PetscMemzero(element_local_buf,max_length_g*sizeof(PetscInt));CHKERRQ(ierr);

  ierr = PetscMalloc1(max_length_g,&element_id_local_buf);CHKERRQ(ierr);
  ierr = PetscMemzero(element_id_local_buf,max_length_g*sizeof(PetscInt));CHKERRQ(ierr);

  /* load buffer and send */
  if (crank == 0) {
    for (p=1; p<csize; p++) {
      PetscInt nghost;
      
      ierr = PetscMemzero(element_local_buf,max_length_g*sizeof(PetscInt));CHKERRQ(ierr);
      source = p;
      tag = p;
      ierr = MPI_Recv(&nghost,1,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
      ierr = MPI_Recv(element_id_local_buf,nghost,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
      
      ierr = PetscMemzero(element_local_buf,max_length_g*sizeof(PetscInt));CHKERRQ(ierr);
      for (e=0; e<nghost; e++) {
        PetscInt eid = element_id_local_buf[e];
        
        element_local_buf[e] = etags[eid];
      }

      // send
      dest = p;
      tag = p;
      ierr = MPI_Send(element_local_buf,nghost,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
    }
  }
  
  if (crank != 0) {
    for (e=0; e<part->nelement_ghost; e++) {
      PetscInt eid = part->element_gid_ghost[e];
      
      element_id_local_buf[e] = eid;
    }
    // send
    dest = 0;
    tag = crank;
    ierr = MPI_Send(&part->nelement_ghost,1,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
    ierr = MPI_Send(element_id_local_buf,part->nelement_ghost,MPIU_INT,dest,tag,comm);CHKERRQ(ierr);
  }
  
  if (crank != 0) {
    source = 0;
    tag = crank;
    ierr = MPI_Recv(element_local_buf,part->nelement_ghost,MPIU_INT,source,tag,comm,&stat);CHKERRQ(ierr);
    
    ecnt = part->nelement_local;
    for (e=0; e<part->nelement_ghost; e++) {
      tet->geometry->element_tag[e+ecnt] = element_local_buf[e];
    }
  }

  
  
 
  ierr = PetscFree(element_local_buf);CHKERRQ(ierr);
  ierr = PetscFree(element_id_local_buf);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


/* This will allocate space for ghost elements and vertices in the geometry space */
#undef __FUNCT__
#define __FUNCT__ "DMTetCreateGeometryFromPartition"
PetscErrorCode DMTetCreateGeometryFromPartition(DM dm,DMTetPartition p)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  khash_t(64) *table;
  int         ret;
  khiter_t    iterator;
  long int    unique,key;
  MPI_Comm    comm;
  PetscMPIInt rank;
  PetscInt    i,k,el_map_len,offset;
  
  comm = PetscObjectComm((PetscObject)dm);
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
  
  ierr = _DMTetMeshCreate(&tet->geometry);CHKERRQ(ierr);
  
  ierr = _DMTetMeshCreate1_with_ghost(p->nelement_local,p->nelement_ghost,p->npe,tet->geometry);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2_with_ghost(p->nvert_local,p->nvert_ghost,p->npe,p->dim,tet->geometry);CHKERRQ(ierr);
  
  table = kh_init(64);
  /* insert key into slot 1 */
  iterator = kh_put(64,table,-1,&ret);
  unique = 0;
  
  /* insert local verts into table first */
  for (i=0; i<p->nvert_local; i++) {
    key = (long int)p->vert_gid_local[i];
    iterator = kh_get(64, table, key);
    if (kh_exist(table,iterator)) {
      printf("[rank %d] ... key %ld exists... error! \n",(int)rank,key);
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key generation for local vertices failed when defining DMTet geometry");
    } else {
      /* set value */
      iterator = kh_put(64,table,key,&ret);
      (kh_value(table,iterator)) = unique;
      unique++;
    }
  }
  
  /* insert ghost verts into table */
  for (i=0; i<p->nvert_ghost; i++) {
    key = (long int)p->vert_gid_ghost[i];
    iterator = kh_get(64, table, key);
    if (kh_exist(table,iterator)) {
      PetscPrintf(PETSC_COMM_SELF,"[rank %d] ... key %ld exists... error! \n",(int)rank,key);
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key generation for ghost vertices failed when defining DMTet geometry");
    } else {
      /* set value */
      iterator = kh_put(64,table,key,&ret);
      (kh_value(table,iterator)) = unique;
      unique++;
    }
  }
  
  
  /* copy coordinates */
  offset = 0;
  for (i=0; i<p->nvert_local; i++) {
    PetscReal *coor_i;
    PetscReal *coor_s = &p->vert_coor_local[p->dim*i];
    
    coor_i = &tet->geometry->coords[p->dim * offset];
    ierr = PetscMemcpy(coor_i,coor_s,sizeof(PetscReal)*p->dim);CHKERRQ(ierr);
    
    offset++;
  }
  for (i=0; i<p->nvert_ghost; i++) {
    PetscReal *coor_i;
    PetscReal *coor_s = &p->vert_coor_ghost[p->dim*i];
    
    coor_i = &tet->geometry->coords[p->dim * offset];
    ierr = PetscMemcpy(coor_i,coor_s,sizeof(PetscReal)*p->dim);CHKERRQ(ierr);
    
    offset++;
  }
  
  /* copy entries from partition into elmap (local first then ghost) */
  el_map_len = p->nelement_local * p->npe;
  for (k=0; k<el_map_len; k++) {
    tet->geometry->element[k] = p->element_map_local[k];
  }
  offset = el_map_len;
  
  el_map_len = p->nelement_ghost * p->npe;
  for (k=0; k<el_map_len; k++) {
    tet->geometry->element[k+offset] = p->element_map_ghost[k];
  }
  
  /* traverse geometry element map, set local indices by querying hash table */
  el_map_len = (tet->geometry->nelements + tet->geometry->nghost_elements) * p->npe;
  for (k=0; k<el_map_len; k++) {
    
    key = (long int)tet->geometry->element[k];
    
    iterator = kh_get(64,table,key);
    if (!kh_exist(table,iterator)) {
      PetscPrintf(PETSC_COMM_SELF,"[rank %d] ... key %ld does not exists... error! \n",(int)rank,key);
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key extraction for vertices failed when defining DMTet geometry");
    } else {
      long int value = kh_value(table,iterator);
      
      tet->geometry->element[k] = (PetscInt)value;
    }
  }
  
  kh_destroy(64,table);
  
  PetscFunctionReturn(0);
}

typedef struct {
  PetscReal   coor[2];
  PetscInt    localidx,globalindex;
  PetscMPIInt rank;
} Point2dIdentifier;

PetscErrorCode _DMTetGetMinBasisSeparationLocal(DM dm,PetscInt nnodes,PetscReal coords[],PetscReal *gmin);


#undef __FUNCT__
#define __FUNCT__ "_DMTetGenerateBasis2d_CWARPANDBLEND_MPI"
PetscErrorCode _DMTetGenerateBasis2d_CWARPANDBLEND_MPI(DM dm,PetscInt element_rank_ghost[],DMTetBasisType btype,PetscInt order,DM_TET *tet)
{
  PetscErrorCode ierr;
  PetscInt e,i,j,k,npe,bcnt;
  PetscReal Ni[3];
  PetscReal *xilocal,elcoor[2*3];
  PetscReal *xn;
  //PetscReal min[2],max[2],mindx,cellmindx,celldx[2];
  PetscReal min[2],max[2],mindx,mindx_g,element_ds,min_element_ds,mesh_eps;
  long int mx,my,key;
  PetscMPIInt commsize,rank;
  MPI_Comm comm;
  PetscInt *n2r_size,offset;
  PetscMPIInt **n2r_map;
  
  khash_t(64) *h;
  int ret;
  khiter_t iterator;
  long int unique,unique_local_basis;
  
  comm = PetscObjectComm((PetscObject)dm);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
  
  /*
   Fetch layout of local basis -
   Would be nice to use CLEGENDRE to minimize clustering at end points,
   however would have to ensure the the order of CLegnedre and CWarpAndBlend matched.
   */
  if (btype == DMTET_CWARP_AND_BLEND) {
    ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&npe,&xilocal);CHKERRQ(ierr);
  } else SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot create basis coordinates for type %d",(int)btype);
  
  ierr = PetscMalloc1(npe*2,&xn);CHKERRQ(ierr);
  
  /* generate worst case bounding box for each element (including ghosts) */
  ///-------------------------------------------------------
  /* create basis coordinates  - use to compute min separation */
  min_element_ds = PETSC_MAX_REAL;
  for (e=0; e<(tet->geometry->nelements + tet->geometry->nghost_elements); e++) {
    PetscInt idx;
    
    /* fetch geometry coordinates */
    for (i=0; i<3; i++) {
      idx = tet->geometry->element[3*e+i];
      
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<npe; i++) {
      Ni[0] = 1.0 - 0.5*(1.0+xilocal[2*i+0]) - 0.5*(1.0+xilocal[2*i+1]);
      Ni[1] = 0.5*(1.0 + xilocal[2*i+0]);
      Ni[2] = 0.5*(1.0 + xilocal[2*i+1]);
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    ierr = _DMTetGetMinBasisSeparationLocal(dm,npe,xn,&element_ds);CHKERRQ(ierr);

    if (element_ds < min_element_ds) { min_element_ds = element_ds; }
  }
  
  mindx = 0.25 * min_element_ds;
  ierr = MPI_Allreduce(&mindx,&mindx_g,1,MPIU_REAL,MPIU_MIN,comm);CHKERRQ(ierr);
  mindx = mindx_g;
  
  /* get domain bounding box */
  ierr = DMTetGetBoundingBox(dm,min,max);CHKERRQ(ierr);
  
  /* scale domain bounding box by 1% just to ensure there are no issues with coords landing exactly on the boundary */
  mesh_eps = 0.01 * (max[0] - min[0]);
  mesh_eps = PetscMin(mesh_eps,0.01*(max[1] - min[1]));
  
  max[0] += mesh_eps;
  max[1] += mesh_eps;
  
  min[0] -= mesh_eps;
  min[1] -= mesh_eps;
  
  mx = (max[0] - min[0])/mindx;
  my = (max[1] - min[1])/mindx;
  ///-------------------------------------------------------
  
  /* create and initialize hash table */
  h = kh_init(64);
  iterator = kh_put(64, h, -1, &ret);
  unique = 0;
  unique_local_basis = 0;
  /* populate table with local then ghost element data */
  for (e=0; e<(tet->geometry->nelements + tet->geometry->nghost_elements); e++) {
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<npe; i++) {
      Ni[0] = 1.0 - 0.5*(1.0+xilocal[2*i+0]) - 0.5*(1.0+xilocal[2*i+1]);
      Ni[1] = 0.5*(1.0 + xilocal[2*i+0]);
      Ni[2] = 0.5*(1.0 + xilocal[2*i+1]);
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    /* generate label */
    for (i=0; i<npe; i++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = xn[2*i+0];
      yp = xn[2*i+1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      //printf("key %ld \n",key);
      
      iterator = kh_get(64, h, key);
      if (kh_exist(h, iterator)) {
        //printf("... key %ld exists... don't insert \n",key);
      } else {
        /* set value */
        iterator = kh_put(64, h, key, &ret);
        (kh_value(h, iterator)) = unique;
        
        unique++;
      }
    }
    
    /* keep track of number of strictly local basis */
    if (e < tet->geometry->nelements) {
      unique_local_basis = unique;
    }
  }
  
  ierr = _DMTetMeshCreate(&tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate1_with_ghost(tet->geometry->nelements,tet->geometry->nghost_elements,npe,tet->space);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2_with_ghost((PetscInt)unique_local_basis,(PetscInt)(unique - unique_local_basis),npe,2,tet->space);CHKERRQ(ierr);
  
  
  /* query table */
  for (e=0; e<(tet->geometry->nelements + tet->geometry->nghost_elements); e++) {
    
    /* fetch coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[2*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[2*idx+1];
    }
    
    /* interpolate from local space to global space */
    for (i=0; i<npe; i++) {
      Ni[0] = 1.0 - 0.5*(1.0+xilocal[2*i+0]) - 0.5*(1.0+xilocal[2*i+1]);
      Ni[1] = 0.5*(1.0 + xilocal[2*i+0]);
      Ni[2] = 0.5*(1.0 + xilocal[2*i+1]);
      
      xn[2*i+0] = xn[2*i+1] = 0.0;
      for (j=0; j<3; j++) {
        xn[2*i+0] += Ni[j] * elcoor[2*j+0];
        xn[2*i+1] += Ni[j] * elcoor[2*j+1];
      }
    }
    
    /* fetch label */
    for (i=0; i<npe; i++) {
      PetscReal xp,yp;
      long int ii,jj;
      
      xp = xn[2*i+0];
      yp = xn[2*i+1];
      ii = (xp - min[0])/mindx;
      jj = (yp - min[1])/mindx;
      if (ii == mx) { ii--; }
      if (jj == my) { jj--; }
      
      key = ii + jj * mx;
      iterator = kh_get(64, h, key);
      if (!kh_exist(h, iterator)) {
        PetscPrintf(PETSC_COMM_SELF,"... key %ld doesn't exist ERROR \n",key);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"Key extraction has failed");
      } else {
        long int nodeidx = kh_value(h, iterator);
        
        tet->space->element[npe*e+i] = (PetscInt)nodeidx;
        
        tet->space->coords[2*nodeidx + 0] = xn[2*i+0];
        tet->space->coords[2*nodeidx + 1] = xn[2*i+1];
      }
    }
  }
  
  
  
  /* create node -> rank map */
  n2r_size = malloc(sizeof(PetscInt)*unique);
  for (k=0; k<unique; k++) { n2r_size[k] = 0; }
  
  for (k=0; k<unique_local_basis; k++) {
    n2r_size[k] = 1; /* these are dofs connected to the current rank */
  }
  
  n2r_map = malloc(sizeof(PetscMPIInt*)*unique);
  for (k=0; k<unique; k++) {
    n2r_map[k] = malloc(sizeof(PetscMPIInt)*1);
  }
  
  for (k=0; k<unique_local_basis; k++) {
    n2r_map[k][0] = (PetscInt)rank;
  }
  
  offset = tet->geometry->nelements;
  for (e=0; e<tet->geometry->nghost_elements; e++) {
    PetscMPIInt el_ghost_rank;
    PetscInt eidx,*g_elnap;
    
    el_ghost_rank = element_rank_ghost[e];
    
    if (el_ghost_rank == rank) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"A ghost by definition must have a different rank to the sub-domain");
    
    eidx = e + offset;
    
    g_elnap = &tet->space->element[npe*eidx];
    
    for (i=0; i<npe; i++) {
      PetscInt nidx;
      PetscBool exists;
      
      /* add el_ghost_rank to node if it's not already present */
      nidx = g_elnap[i];
      
      /* check entries in table, break early if found */
      exists = PETSC_FALSE;
      for (k=0; k<n2r_size[nidx]; k++) {
        if (el_ghost_rank == n2r_map[nidx][k]) {
          exists = PETSC_TRUE;
          break;
        }
      }
      
      /* add el_ghost_rank to end of list if it wasn't found */
      if (!exists) {
        PetscMPIInt *buffer;
        
        /* reallocate */
        buffer = realloc(n2r_map[nidx],sizeof(PetscMPIInt)*(n2r_size[nidx]+1));
        n2r_map[nidx] = buffer;
        
        /* insert */
        n2r_map[nidx][n2r_size[nidx]] = el_ghost_rank;
        
        /* increment size */
        n2r_size[nidx]++;
      }
      
    }
    
  }
  
  PetscInt subdomain_nbasis_g,nbasis_g,basis_psum,basis_offset;
  PetscInt *l2g;
  PetscMPIInt *l2g_min_rank;
  
  ierr = PetscMalloc1(unique_local_basis,&l2g);CHKERRQ(ierr);
  ierr = PetscMalloc1(unique_local_basis,&l2g_min_rank);CHKERRQ(ierr);
  for (i=0; i<unique_local_basis; i++) {
    l2g[i] = -1;
    l2g_min_rank[i] = rank;
  }
  
  subdomain_nbasis_g = 0;
  for (i=0; i<unique_local_basis; i++) {
    PetscMPIInt min_rank;
    
    min_rank = commsize + 1;
    for (k=0; k<n2r_size[i]; k++) {
      if (n2r_map[i][k] < min_rank) {
        min_rank = n2r_map[i][k];
      }
    }
    
    if (min_rank == rank) {
      l2g[i] = (PetscInt)rank;
      subdomain_nbasis_g++;
    }
  }
  
  //printf("[r%d] Found %d unique sub-domain basis\n",rank,subdomain_nbasis_g);
  ierr = MPI_Allreduce(&subdomain_nbasis_g,&nbasis_g,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  //printf("[r%d] Compute %d unique domain basis\n",rank,nbasis_g);
  ierr = MPI_Scan(&subdomain_nbasis_g,&basis_psum,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  //printf("[r%d] Computed %d basis_offset sub-domain basis partial sum\n",rank,basis_psum);
  
  basis_offset = basis_psum - subdomain_nbasis_g;
  
  /* assign a consistent global dof to all unique owned points */
  bcnt = 0;
  for (i=0; i<unique_local_basis; i++) {
    if (l2g[i] == (PetscInt)rank) {
      l2g[i] = bcnt + basis_offset;
      bcnt++;
    }
  }
  
  
  
  /* scatter */
  PetscInt          np2scatter,recv_length;
  Point2dIdentifier *pointid,*pointid_recv;
  DataEx            de;
  
  np2scatter = 0;
  for (i=0; i<unique_local_basis; i++) {
    if (n2r_size[i] > 1) {
      np2scatter++;
    }
  }
  
  /* create and fill point identifiers */
  ierr = PetscMalloc1(np2scatter,&pointid);CHKERRQ(ierr);
  ierr = PetscMemzero(pointid,sizeof(Point2dIdentifier)*np2scatter);CHKERRQ(ierr);
  bcnt = 0;
  for (i=0; i<unique_local_basis; i++) {
    if (n2r_size[i] > 1) {
      pointid[bcnt].localidx = i;
      pointid[bcnt].globalindex = l2g[i];
      pointid[bcnt].rank = rank;
      pointid[bcnt].coor[0] = tet->space->coords[2*i+0];
      pointid[bcnt].coor[1] = tet->space->coords[2*i+1];
      bcnt++;
    }
  }
  
  
  
  /* create the data exchanger */
  de = DataExCreate(comm,0);
  ierr = DataExTopologyInitialize(de);CHKERRQ(ierr);
  for (e=0; e<tet->geometry->nghost_elements; e++) {
    PetscMPIInt el_ghost_rank;
    
    el_ghost_rank = element_rank_ghost[e];
    ierr = DataExTopologyAddNeighbour(de,el_ghost_rank);CHKERRQ(ierr);
  }
  ierr = DataExTopologyFinalize(de);CHKERRQ(ierr);
  
  
  ierr = DataExInitializeSendCount(de);CHKERRQ(ierr);
  for (i=0; i<unique_local_basis; i++) {
    if (n2r_size[i] <= 1) continue;
    
    for (k=0; k<n2r_size[i]; k++) {
      PetscMPIInt rank2sendto = n2r_map[i][k];
      
      if (rank2sendto == rank) continue;
      ierr = DataExAddToSendCount( de, rank2sendto, 1 );CHKERRQ(ierr);
    }
  }
  ierr = DataExFinalizeSendCount(de);CHKERRQ(ierr);
  
  
  ierr = DataExPackInitialize(de,sizeof(Point2dIdentifier));CHKERRQ(ierr);
  //for (k=0; k<nneighbour_ranks; k++) {
  //  ierr = DataExPackData( de, neighbour_ranks[k], nbnodes,(void*)pointid );CHKERRQ(ierr);
  //}
  bcnt = 0;
  for (i=0; i<unique_local_basis; i++) {
    if (n2r_size[i] <= 1) continue;
    
    for (k=0; k<n2r_size[i]; k++) {
      PetscMPIInt rank2sendto = n2r_map[i][k];
      
      if (rank2sendto == rank) continue;
      ierr = DataExPackData( de, rank2sendto, 1,(void*)&pointid[bcnt] );CHKERRQ(ierr);
    }
    bcnt++;
  }
  ierr = DataExPackFinalize(de);CHKERRQ(ierr);
  
  //printf("[r%d] np2scatter %d : bcnt %d \n",rank,np2scatter,bcnt);
  
  ierr = DataExBegin(de);CHKERRQ(ierr);
  ierr = DataExEnd(de);CHKERRQ(ierr);
  
  ierr = DataExGetRecvData( de, &recv_length, (void**)&pointid_recv );CHKERRQ(ierr);
  
  
  /* query table */
  for (i=0; i<recv_length; i++) {
    PetscReal xp_recv[2];
    long int ii,jj;
    
    /* fetch coordinates */
    xp_recv[0] = pointid_recv[i].coor[0];
    xp_recv[1] = pointid_recv[i].coor[1];
    
    /* fetch label */
    ii = (xp_recv[0] - min[0])/mindx;
    jj = (xp_recv[1] - min[1])/mindx;
    if (ii == mx) { ii--; }
    if (jj == my) { jj--; }
    
    key = ii + jj * mx;
    iterator = kh_get(64, h, key);
    if (kh_exist(h, iterator)) {
      long int nodeidx = kh_value(h, iterator);
      PetscInt nidx;
      
      nidx = (PetscInt)nodeidx;
      
      /* compare ranks - lowest rank claims ownership */
      if (pointid_recv[i].rank < l2g_min_rank[nidx]) {
        l2g_min_rank[nidx] = pointid_recv[i].rank;
        l2g[nidx] = pointid_recv[i].globalindex;
      }
    } else {
      /* do nothing */
    }
  }
  
  
  for (i=0; i<unique_local_basis; i++) {
    if (l2g[i] < 0) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONGSTATE,"l2g should not contain any negative values. Index %D has value %D",i,l2g[i]);
  }
  
  
  tet->mpi->nelements_g = tet->geometry->nelements;
  ierr = MPI_Allreduce(&tet->mpi->nelements_g,&tet->mpi->nelements,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  
  tet->mpi->nnodes   = nbasis_g;
  tet->mpi->nnodes_l = (PetscInt)unique_local_basis;
  tet->mpi->nnodes_g = subdomain_nbasis_g;
  
  PetscPrintf(comm,"DMTetGenerateBasisFromPartition [space-global] - basis (M) = %D \n",nbasis_g);
  
  
  tet->mpi->l2g = l2g;
  
  ierr = PetscFree(l2g_min_rank);CHKERRQ(ierr);
  ierr = DataExDestroy(de);CHKERRQ(ierr);
  ierr = PetscFree(pointid);CHKERRQ(ierr);
  
  for (k=0; k<unique; k++) {
    free(n2r_map[k]);
  }
  free(n2r_map);
  free(n2r_size);
  
  ierr = PetscFree(xilocal);CHKERRQ(ierr);
  ierr = PetscFree(xn);CHKERRQ(ierr);
  kh_destroy(64, h);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetPartitionViewNd_VTK"
PetscErrorCode DMTetPartitionViewNd_VTK(DM dm,PetscBool with_ghost,PetscInt epart[],PetscInt npart[],PetscInt etypes[],const char name[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt i,e,dim;
  PetscInt nelements,npe,*element,nnodes,*etags;
  PetscReal *coords;
  int vtk_id;
  
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,&nelements,&npe,&element,0,&nnodes,&coords);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dm,NULL,&etags,NULL);CHKERRQ(ierr);
  if (with_ghost) {
    nelements += tet->geometry->nghost_elements;
    nnodes += tet->geometry->nghost_nodes;
  }
  
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes,nelements);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  
  if (dim == 2) {
    for (e=0; e<nelements; e++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                   element[3*e],element[3*e+1],element[3*e+2]);
    }
  }
  if (dim == 3) {
    for (e=0; e<nelements; e++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D %D ",
                   element[4*e],element[4*e+1],element[4*e+2],element[4*e+3]);
    }
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",npe*e+npe);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  
  if (dim == 2) { vtk_id = 5; }
  if (dim == 3) { vtk_id = 10; }
  
  for (e=0; e<nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",vtk_id);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"eltag\" format=\"ascii\"> \n");
    for (e=0; e<nelements; e++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)etags[e]);
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  
  if (epart) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"epartition\" format=\"ascii\"> \n");
    for (e=0; e<nelements; e++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)epart[e]);
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  }
  if (etypes) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"etypes\" format=\"ascii\"> \n");
    for (e=0; e<nelements; e++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)etypes[e]);
    } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  
  if (dim == 2) {
    for (i=0; i<nnodes; i++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",coords[2*i],coords[2*i+1]);
    }
  }
  if (dim == 3) {
    for (i=0; i<nnodes; i++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e %1.10e ",coords[3*i],coords[3*i+1],coords[3*i+2]);
    }
  }
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  
  if (!with_ghost) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"l2g\" NumberOfComponents=\"1\" format=\"ascii\"> \n");
    for (i=0; i<nnodes; i++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)tet->mpi->l2g[i]);
    }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  }
  
  if (npart) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"npartition\" NumberOfComponents=\"1\" format=\"ascii\"> \n");
    for (i=0; i<nnodes; i++) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",(int)npart[i]);
    }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreatePartitioned"
PetscErrorCode DMTetCreatePartitioned(MPI_Comm comm,DM dm,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dmp)
{
  PetscErrorCode ierr;
  DMTetPartInfo info;
  DMTetPartition partition;
  PetscMPIInt commsize,commrank;
  DM dmp;
  PetscLogDouble t0,t1,prof[5];
  DM_TET *tet;
  DM_TET *tet_seq = NULL;
  PetscInt *etags = NULL;
  
  
  //Catch case for commsize = 1 (sequential or commself as arg 1)
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = PetscObjectReference((PetscObject)dm);CHKERRQ(ierr);
    *_dmp = dm;
    PetscFunctionReturn(0);
  }
  

  /* constructor */
  ierr = DMTetCreate(comm,&dmp);CHKERRQ(ierr);
  ierr = DMSetDimension(dmp,2);CHKERRQ(ierr);
  tet = (DM_TET*)dmp->data;
  
  tet->dmtet_sequential = dm;
  if (dm) {
    tet_seq = (DM_TET*)dm->data;
    etags = tet_seq->geometry->element_tag;
    ierr = PetscObjectReference((PetscObject)dm);CHKERRQ(ierr);
  }

  
  //[PartInfo]
  ierr = DMTetPartInfoCreate(&info);CHKERRQ(ierr);
  ierr = DMTetPartInfoSetup(info,dm,comm);CHKERRQ(ierr);
  tet->pinfo = info;
  
  
  //[Partition] create + setup
  PetscTime(&prof[0]);
  ierr = DMTetPartitionCreate(comm,dm,&partition);CHKERRQ(ierr);
  PetscTime(&prof[1]);
  ierr = Extract_SubdomainElements_Seq2MPI(comm,partition,dm,info->epart);CHKERRQ(ierr);
  PetscTime(&prof[2]);
  ierr = Extract_SubdomainVertices_Seq2MPI(comm,partition,dm,info->epart);CHKERRQ(ierr);
  PetscTime(&prof[3]);
  ierr = Extract_GhostElementsVertices_Seq2MPI(comm,partition,info,dm,info->epart);CHKERRQ(ierr);
  PetscTime(&prof[4]);
  tet->partition = partition;
  PetscPrintf(PETSC_COMM_WORLD,"DMTetPartitionSetup-Seq2MPI  : %1.4e (sec)\n",prof[4]-prof[0]);
  PetscPrintf(PETSC_COMM_WORLD,"  DMTetPartitionCreate       :   %1.4e (sec)\n",prof[1]-prof[0]);
  PetscPrintf(PETSC_COMM_WORLD,"  Extract_SubdomainE_Seq2MPI :   %1.4e (sec)\n",prof[2]-prof[1]);
  PetscPrintf(PETSC_COMM_WORLD,"  Extract_SubdomainV_Seq2MPI :   %1.4e (sec)\n",prof[3]-prof[2]);
  PetscPrintf(PETSC_COMM_WORLD,"  Extract_GhostEV_Seq2MPI    :   %1.4e (sec)\n",prof[4]-prof[3]);

  
  PetscTime(&t0);
  //[Geometry] scatter
  ierr = DMTetCreateGeometryFromPartition(dmp,partition);CHKERRQ(ierr);
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"DMTetCreateGeometryFromPartition: %1.4e (sec)\n",t1-t0);
  
  PetscTime(&t0);
  //[Geometry] scatter element tags
  ierr = Distribute_SubdomainElementInts_Seq2MPI(dmp,etags);CHKERRQ(ierr);
  ierr = Distribute_SubdomainGhostElementInts_Seq2MPI(dmp,etags);CHKERRQ(ierr);
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"Distribute_SubdomainElementInts_Seq2MPI: %1.4e (sec)\n",t1-t0);
  
  
  //[DMTetGenerateBasisFromPartition]
  ierr = _DMTetMeshMPICreate(&tet->mpi);CHKERRQ(ierr);

  
  //[Basis configuration]
  ierr = DMTetSetDof(dmp,dof);CHKERRQ(ierr); /* dof must be set before the IS for the VecScatter is created */
  ierr = DMTetSetBasisType(dmp,btype);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dmp,border);CHKERRQ(ierr);
  
  PetscTime(&t0);
  ierr = DMTetGenerateBasis2d_MPI(dmp);CHKERRQ(ierr);
  PetscTime(&t1);
  PetscPrintf(PETSC_COMM_WORLD,"DMTetGenerateBasisFromPartition: %1.4e (sec)\n",t1-t0);

  /* viewers */
  {
    char name[PETSC_MAX_PATH_LEN];
    PetscInt r2v = -1;
    PetscBool rankviewenabled = PETSC_FALSE;
    
    PetscOptionsGetBool(NULL,NULL,"-rank_view_enabled",&rankviewenabled,NULL);
    PetscOptionsGetInt(NULL,NULL,"-rank2view",&r2v,NULL);
    if (rankviewenabled) {
      if (r2v == (PetscInt)commrank) {
        PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"dmtet_part_geom_l-r%d.vtu",commrank);
        ierr = DMTetPartitionViewNd_VTK(dmp,PETSC_FALSE,NULL,NULL,NULL,name);CHKERRQ(ierr);
        
        PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"dmtet_part_geom_g-r%d.vtu",commrank);
        ierr = DMTetPartitionViewNd_VTK(dmp,PETSC_TRUE,NULL,NULL,NULL,name);CHKERRQ(ierr);
      } else if (r2v == -1) {
        PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"dmtet_part_geom_l-r%d.vtu",commrank);
        ierr = DMTetPartitionViewNd_VTK(dmp,PETSC_FALSE,NULL,NULL,NULL,name);CHKERRQ(ierr);
        
        PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"dmtet_part_geom_g-r%d.vtu",commrank);
        ierr = DMTetPartitionViewNd_VTK(dmp,PETSC_TRUE,NULL,NULL,NULL,name);CHKERRQ(ierr);
      }
    }
  }

  if (commrank == 0) {
    char name[PETSC_MAX_PATH_LEN];
    
    PetscSNPrintf(name,PETSC_MAX_PATH_LEN-1,"dmtet_partition_nel%D_basis%D_parts%D.vtu",info->nelements,tet->basis_order,info->npartitions);
    ierr = DMTetPartitionView2d_VTK(info->dm,info->epart,name);CHKERRQ(ierr);
  }
  
  *_dmp = dmp;
  
  PetscFunctionReturn(0);
}
