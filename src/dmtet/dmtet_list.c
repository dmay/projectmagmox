
/*
 Notes:
 1. It might be desirable to allow GLists to be created as the result of calling functions like
    DMTetGListMerge(). Currently the user is required to explicitly call DMGListCreate() prior to
    the assignment functions. Making this change would avoid the need for the compatability check
    which ensures that the DM's match.
 2. The function DMGetDMTetGList() should be renamed to DMTetGetDMTetGList(), since I've adopted the
    convenction that all DMTetxxxGList() methods are private.
 3. The object DMTetGList is currently private - this might be overly restrictive and possibly should
    be relaxed.
 4. Should DMTetBFacetBasisListAssignFromInflowSubset() be renamed to something like
    DMGlistAssign_Edge2Basis_FromInflowSubset().
    The change in name reflects the nature of the primitives expected and generated and is consistent
    with the convention that DMTetXXXGList methods are private.
 5. It would be nice to have a Copy() method like DMGListClone(DM dm1,const char list_name[],DM dm2) which
    would duplicate the Glist named "list_name" from dm1 onto dm2, but this is virtually impossible to make safe.
    Hence it is safer to simply call the assignment method again.
 6. It might be desirable to have a method DMGListDestroy() to remove (for example) intermediate GLists. 
    We would not need this functionality if the DMTetGList object has exposed to the user. 
    This would allow, for example, something like this:
 
    DMTetGListCreate(&set1);
    DMTetGListSetDM(set1,dm);
    DMTetGListSetLableName(set1,"edge:north_face");
    DMTetGListCreate(&set2);
    DMTetGListSetDM(set2,dm);
    DMTetGListSetLableName(set2,"edge:east_face");
 
    DMGListCreate(dm,"edge:merged");
    DMTetGListMergeGList(dm,2,{set1,set2},"edge:merged");
*/

#include <petsc.h>
#include <dmtetimpl.h>
#include <dmtet_common.h>
#include <petscdmtet.h>


#undef __FUNCT__
#define __FUNCT__ "DMTetGListCreate"
PetscErrorCode DMTetGListCreate(DMTetGList *set)
{
  DMTetGList     list;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&list);CHKERRQ(ierr);
  ierr = PetscMemzero(list,sizeof(struct _p_DMTetGList));CHKERRQ(ierr);
  list->dm = NULL;
  list->label_id = -1;
  list->length = 0;
  list->indices = NULL;
  list->issetup = PETSC_FALSE;
  list->bounds[0] = 0;
  list->bounds[1] = 0;
  list->primitive_type = DMTET_UNINITIALIZED;
  *set = list;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListDestroy"
PetscErrorCode DMTetGListDestroy(DMTetGList *set)
{
  DMTetGList     list;
  PetscErrorCode ierr;
  
  if (!set) PetscFunctionReturn(0);
  list = *set;
  if (!list) PetscFunctionReturn(0);
  ierr = PetscFree(list->indices);CHKERRQ(ierr);
  ierr = PetscFree(list);CHKERRQ(ierr);
  *set = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListCreateDefault"
PetscErrorCode DMTetGListCreateDefault(DM dm,PetscInt length,DMTetGList *set)
{
  PetscInt       k;
  DMTetGList     list;
  PetscErrorCode ierr;
  
  ierr = DMTetGListCreate(&list);CHKERRQ(ierr);
  ierr = DMTetGListSetLabelId(list,0);CHKERRQ(ierr);
  ierr = DMTetGListSetLabelName(list,"default");CHKERRQ(ierr);
  ierr = DMTetGListSetDM(list,dm);CHKERRQ(ierr);
  ierr = DMTetGListSetSize(list,length);CHKERRQ(ierr);
  for (k=0; k<length; k++) {
    list->indices[k] = k;
  }
  list->bounds[0] = 0;
  list->bounds[1] = length;
  *set = list;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListCheckCompatibility"
PetscErrorCode DMTetGListCheckCompatibility(DMTetGList list_from,DMTetPrimitiveType expected_primitive,DMTetGList list_to)
{
  PetscErrorCode ierr;

  if (!list_from->issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Input list not setup. Call set DMTetGListSetup() first. Cannot check for compatibility");
  if (!list_from->dm) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Input list must have a DM set. Call set DMTetGListSetDM() first");
  if (!list_to->dm) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Output list must have a DM set. Call set DMTetGListSetDM() first");
  if (list_from->dm != list_to->dm) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Input and output list must refer to the same DM");
  
  if (list_from->primitive_type == DMTET_UNKNOWN_PRIMITIVE) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Input list refers to an unknown primitive type. Compatibility check not possible");
  
  if (list_from->primitive_type == DMTET_UNINITIALIZED) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Input list refers to an un-initialized primitive type. Compatibility check not possible");
  
  if (list_from->primitive_type != expected_primitive) {
    const char *n1,*n2;
    
    ierr = DMTetGListGetPrimitiveTypeAsString(list_from->primitive_type,&n1);CHKERRQ(ierr);
    ierr = DMTetGListGetPrimitiveTypeAsString(expected_primitive,&n2);CHKERRQ(ierr);
    SETERRQ4(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Input list refers to a primitive type \"%s\" (%D). Expected input list must refer to primitive type \"%s\" (%D)",n1,(PetscInt)list_from->primitive_type,n2,(PetscInt)expected_primitive);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListSetup"
PetscErrorCode DMTetGListSetup(DMTetGList list)
{
  PetscInt k;
  
  if (!list->dm) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Must set a valid DM. Call DMTetGListSetDM() first");
  if (list->primitive_type == DMTET_UNINITIALIZED) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Must set a valid primitive type. Call DMTetGListSetPrimitiveType() first");
  list->bounds[0] = PETSC_MAX_INT;
  list->bounds[1] = PETSC_MIN_INT;
  for (k=0; k<list->length; k++) {
    list->bounds[0] = PetscMin(list->bounds[0],list->indices[k]);
    list->bounds[1] = PetscMax(list->bounds[1],list->indices[k]);
  }
  list->issetup = PETSC_TRUE;
  PetscFunctionReturn(0);
}

/*
 Check that the following conditions are satisfied
   bounds[0] >= min
   bounds[1] <= max
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetGListCheckBounds"
PetscErrorCode DMTetGListCheckBounds(DMTetGList list,PetscInt min,PetscInt max)
{
  if (!list->issetup) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"DMTetGList[\"%s\"] is not setup. Cannot check bounds.",list->label_name);
  if (list->bounds[0] < min) {
    SETERRQ4(PETSC_COMM_SELF,PETSC_ERR_USER,"DMTetGList[\"%s\"] expected min (%D) is outside than list bounds [%D,%D]",list->label_name,min,list->bounds[0],list->bounds[1]);
  }
  if (list->bounds[1] > max) {
    SETERRQ4(PETSC_COMM_SELF,PETSC_ERR_USER,"DMTetGList[\"%s\"] expected max (%D) is outside than list bounds [%D,%D]",list->label_name,max,list->bounds[0],list->bounds[1]);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListInit"
PetscErrorCode DMTetGListInit(DMTetGList list)
{
  PetscErrorCode ierr;
  
  ierr = PetscMemzero(list,sizeof(struct _p_DMTetGList));CHKERRQ(ierr);
  list->dm = NULL;
  list->label_id = -1;
  list->length = 0;
  list->indices = NULL;
  list->issetup = PETSC_FALSE;
  list->primitive_type = DMTET_UNINITIALIZED;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListReset"
PetscErrorCode DMTetGListReset(DMTetGList list)
{
  PetscErrorCode ierr;

  if (list->issetup) {
    list->length = 0;
    ierr = PetscFree(list->indices);CHKERRQ(ierr);
    list->indices = NULL;
    list->issetup = PETSC_FALSE;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListSetDM"
PetscErrorCode DMTetGListSetDM(DMTetGList list,DM dm)
{
  list->dm = dm;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListSetPrimitiveType"
PetscErrorCode DMTetGListSetPrimitiveType(DMTetGList list,DMTetPrimitiveType type)
{
  list->primitive_type = type;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListGetPrimitiveType"
PetscErrorCode DMTetGListGetPrimitiveType(DMTetGList list,DMTetPrimitiveType *type)
{
  if (type) { *type = list->primitive_type; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListGetPrimitiveTypeAsString"
PetscErrorCode DMTetGListGetPrimitiveTypeAsString(DMTetPrimitiveType primitive_type,const char *name[])
{
  static const char uninit[] = "un-initialized";
  static const char cell[] = "cells";
  static const char edge[] = "edge";
  static const char vertex[] = "vertex";
  static const char basis[] = "basis";
  static const char unknown[] = "unknown";
  
  switch (primitive_type) {
    case DMTET_UNINITIALIZED:
    *name = uninit;
    break;
    case DMTET_CELL:
    *name = cell;
    break;
    case DMTET_EDGE:
    *name = edge;
    break;
    case DMTET_VERTEX:
    *name = vertex;
    break;
    case DMTET_BASIS:
    *name = basis;
    break;
    case DMTET_UNKNOWN_PRIMITIVE:
    *name = unknown;
    break;
    default:
    *name = uninit;
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot convert primitive type to a string");
    break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListSetSize"
PetscErrorCode DMTetGListSetSize(DMTetGList list,PetscInt length)
{
  PetscErrorCode ierr;
  
  if (list->issetup) {
    SETERRQ(PetscObjectComm((PetscObject)list->dm),PETSC_ERR_USER,"List already setup. Must call DMTetGListReset() before DMTetGListSetSize()");
  } else {
    list->length = length;
    ierr = PetscMalloc1(list->length+1,&list->indices);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListSetLabelId"
PetscErrorCode DMTetGListSetLabelId(DMTetGList list,PetscInt id)
{
  list->label_id = id;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListSetLabelName"
PetscErrorCode DMTetGListSetLabelName(DMTetGList list,const char name[])
{
  PetscErrorCode ierr;
  ierr = PetscSNPrintf(list->label_name,PETSC_MAX_PATH_LEN-1,"%s",name);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListGetIndices"
PetscErrorCode DMTetGListGetIndices(DMTetGList list,PetscInt *length,const PetscInt *indices[])
{
  if (length) { *length = list->length; }
  if (indices) { *indices = (const PetscInt*)list->indices; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGListGetPrimitiveType"
PetscErrorCode DMGListGetPrimitiveType(DM dm,const char labelName[],DMTetPrimitiveType *type)
{
  DMTetGList     list;
  PetscErrorCode ierr;
  
  ierr = DMGetDMTetGList(dm,labelName,&list);CHKERRQ(ierr);
  ierr = DMTetGListGetPrimitiveType(list,type);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGListGetIsSetUp"
PetscErrorCode DMGListGetIsSetUp(DM dm,const char labelName[],PetscBool *issetup)
{
  DMTetGList     list = NULL;
  PetscErrorCode ierr;
  
  ierr = DMGetDMTetGList(dm,labelName,&list);CHKERRQ(ierr);
  if (list) {
    if (issetup) {
      *issetup = list->issetup;
    }
  }
  PetscFunctionReturn(0);
}

/* create a new set within the DM and assign it a label */
#undef __FUNCT__
#define __FUNCT__ "DMGListCreate"
PetscErrorCode DMGListCreate(DM dm,PetscInt id,const char labelName[])
{
  DM_TET         *tet;
  PetscBool      istet = PETSC_FALSE;
  DMTetGList     *list,set;
  DataField      gfield;
  PetscErrorCode ierr;

  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  tet = (DM_TET*)dm->data;

  if (!tet->section) {
    DataBucketCreate(&tet->section);
    DataBucketRegisterField(tet->section,labelName,sizeof(struct _p_DMTetGList),NULL);
    DataBucketFinalize(tet->section);
    DataBucketSetInitialSizes(tet->section,1,0);
  } else {
    DataBucketRegisterField(tet->section,labelName,sizeof(struct _p_DMTetGList),NULL);
  }
  
  /* get list */
  DataBucketGetDataFieldByName(tet->section,labelName,&gfield);
  DataFieldGetEntries(gfield,(void**)&list);
  
  ierr = DMTetGListCreate(&set);CHKERRQ(ierr);
  ierr = DMTetGListInit(set);CHKERRQ(ierr);
  ierr = DMTetGListSetDM(set,dm);CHKERRQ(ierr);
  ierr = DMTetGListSetLabelId(set,id);CHKERRQ(ierr);
  ierr = DMTetGListSetLabelName(set,labelName);CHKERRQ(ierr);
  list[0] = set;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGListReset"
PetscErrorCode DMGListReset(DM dm,const char labelName[])
{
  DMTetGList     list;
  PetscErrorCode ierr;

  ierr = DMGetDMTetGList(dm,labelName,&list);CHKERRQ(ierr);
  ierr = DMTetGListReset(list);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGListView"
PetscErrorCode DMGListView(DM dm)
{
  DM_TET         *tet;
  PetscBool      istet = PETSC_FALSE;
  DMTetGList     *set;
  DataField      *df_sets;
  int            f,nsets;
  PetscErrorCode ierr;
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  tet = (DM_TET*)dm->data;
  
  PetscPrintf(PetscObjectComm((PetscObject)dm),"[DMTet Generic List View]\n");
  if (!tet->section) {
    PetscPrintf(PetscObjectComm((PetscObject)dm),"  + No lists have been created\n");
    PetscFunctionReturn(0);
  }

  DataBucketGetDataFields(tet->section,&nsets,&df_sets);
  for (f=0; f<nsets; f++) {
    DataFieldGetEntries(df_sets[f],(void**)&set);
    if (set[0]->issetup) {
      PetscPrintf(PetscObjectComm((PetscObject)dm),"  + name \"%s\" : id \"%D\" : <setup> : #entries %D\n",set[0]->label_name,set[0]->label_id,set[0]->length);
    } else {
      PetscPrintf(PetscObjectComm((PetscObject)dm),"  + name \"%s\" : id \"%D\" : <no-yet-setup>\n",set[0]->label_name,set[0]->label_id);
    }
    PetscPrintf(PetscObjectComm((PetscObject)dm),"    using DM %p\n",set[0]->dm);
    {
      const char *ptype;
      ierr = DMTetGListGetPrimitiveTypeAsString(set[0]->primitive_type,&ptype);CHKERRQ(ierr);
      PetscPrintf(PetscObjectComm((PetscObject)dm),"    primitive type \"%s\"\n",ptype);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGetDMTetGList"
PetscErrorCode DMGetDMTetGList(DM dm,const char labelName[],DMTetGList *set)
{
  DM_TET         *tet;
  PetscBool      istet = PETSC_FALSE;
  DMTetGList     *list;
  DataField      gfield;
  PetscErrorCode ierr;
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  tet = (DM_TET*)dm->data;
  
  if (!tet->section) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"No lists have been created");

  /* get list */
  DataBucketGetDataFieldByName(tet->section,labelName,&gfield);
  DataFieldGetEntries(gfield,(void**)&list);
  *set = list[0];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGListGetIndices"
PetscErrorCode DMGListGetIndices(DM dm,const char labelName[],PetscInt *length,const PetscInt *indices[])
{
  DMTetGList     list;
  PetscErrorCode ierr;
  
  ierr = DMGetDMTetGList(dm,labelName,&list);CHKERRQ(ierr);
  ierr = DMTetGListGetIndices(list,length,indices);CHKERRQ(ierr);
  if (!list->issetup) {
    PetscPrintf(PetscObjectComm((PetscObject)dm),"DMGListGetIndices[\"%s\"] list is not setup - returning NULL\n",list->label_name);
  }
  PetscFunctionReturn(0);
}

/* Set assignment method */

/* 
 Puts all facets associated with the geometry into a list
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetBFacetListAssign_Default"
PetscErrorCode DMTetBFacetListAssign_Default(DM dm,const char set_name[])
{
  DMTetGList     set;
  BFacetList     f;
  PetscBool      issetup;
  PetscInt       nfacets,nbasis_face,k;
  PetscErrorCode ierr;
  
  /* get the set from the name */
  ierr = DMGetDMTetGList(dm,set_name,&set);CHKERRQ(ierr);
  
  ierr = DMTetGetGeometryBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* assignment - simply mark all facets */
  ierr = DMTetGListSetSize(set,nfacets);CHKERRQ(ierr);
  for (k=0; k<nfacets; k++) {
    set->indices[k] = k;
  }
  ierr = DMTetGListSetPrimitiveType(set,DMTET_EDGE);CHKERRQ(ierr);
  ierr = DMTetGListSetup(set);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 User function mark() returns true/false indicating whether a boundary facet should be included in the list
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetBFacetListAssign_User"
PetscErrorCode DMTetBFacetListAssign_User(DM dm,const char set_name[],PetscErrorCode (*mark)(DMTetFacet,void*,PetscBool*),void *ctx)
{
  DMTetGList     set;
  BFacetList     f;
  PetscBool      issetup;
  PetscInt       nfacets,nbasis_face,k,count;
  PetscBool      *tag;
  struct _p_DMTetFacet facet;
  DMTetFacet     facet_k;
  PetscReal      *normal,*tangent,*cell_coords;
  PetscInt       *facet_to_element,*facet_element_face_id,*element,bpe;
  PetscErrorCode ierr;
  
  /* get the set from the name */
  ierr = DMGetDMTetGList(dm,set_name,&set);CHKERRQ(ierr);
  
  ierr = DMTetGetGeometryBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);

  ierr = DMTetGeometryElement(dm,NULL,&bpe,&element,NULL,NULL,&cell_coords);CHKERRQ(ierr);

  ierr = PetscMalloc1(nfacets,&tag);CHKERRQ(ierr);
  facet_k = &facet;

  /* prepare a facet primitive */
  facet_k->dm = dm;
  ierr = BFacetListGetOrientations(f,&normal,&tangent);CHKERRQ(ierr);
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);

  count = 0;
  for (k=0; k<nfacets; k++) {
    PetscInt e,face_id,*eidx_face,vid[2];
    
    tag[k] = PETSC_FALSE;
    
    /* prepare a facet primitive */
    facet_k->lives_on_boundary = PETSC_TRUE;
    facet_k->normal[0] = normal[2*k+0];
    facet_k->normal[1] = normal[2*k+1];
    facet_k->tangent[0] = tangent[2*k+0];
    facet_k->tangent[1] = tangent[2*k+1];
    
    e = facet_to_element[k];
    face_id = facet_element_face_id[k];
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    
    vid[0] = element[bpe*e + eidx_face[0]];
    facet_k->x0[0] = cell_coords[2*vid[0]+0];
    facet_k->x0[1] = cell_coords[2*vid[0]+1];
    
    vid[1] = element[bpe*e + eidx_face[nbasis_face-1]];
    facet_k->x1[0] = cell_coords[2*vid[1]+0];
    facet_k->x1[1] = cell_coords[2*vid[1]+1];

    /*
    printf("e %d eidx_face %d %d\n",e,eidx_face[0],eidx_face[1]);
    for (PetscInt ii=0; ii<bpe; ii++) {
      printf("  bid %d\n",element[bpe*e + ii]);
    }
    for (PetscInt ii=0; ii<nbasis_face; ii++) {
      printf("  vid %d\n",element[bpe*e + eidx_face[ii]]);
    }
    */
    
    facet_k->facet_index = k;
    
    /* call assignment function */
    ierr = mark(facet_k,ctx,&tag[k]);CHKERRQ(ierr);
    if (tag[k]) { count++; }
  }

  ierr = DMTetGListSetSize(set,count);CHKERRQ(ierr);
  count = 0;
  for (k=0; k<nfacets; k++) {
    if (tag[k]) {
      set->indices[count] = k;
      count++;
    }
  }
  ierr = PetscFree(tag);CHKERRQ(ierr);
  
  ierr = DMTetGListSetPrimitiveType(set,DMTET_EDGE);CHKERRQ(ierr);
  ierr = DMTetGListSetup(set);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBFacetBasisListAssign"
PetscErrorCode DMTetBFacetBasisListAssign(DM dm,const char facet_set_name[],const char basis_facet_set_name[])
{
  DMTetGList     facet_set,set;
  BFacetList     f;
  PetscBool      issetup,*tag;
  PetscInt       nfacets,nbasis_face,k,j,mesh_nbasis,count;
  PetscInt       *facet_to_element,*facet_element_face_id,bpe,*element;
  PetscErrorCode ierr;
  
  /* get the set from the name */
  ierr = DMGetDMTetGList(dm,facet_set_name,&facet_set);CHKERRQ(ierr);
  if (!facet_set->issetup) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires input DMTetGList[\"%s\"] to be setup",facet_set->label_name);
  
  ierr = DMGetDMTetGList(dm,basis_facet_set_name,&set);CHKERRQ(ierr);
  ierr = DMTetGListCheckCompatibility(facet_set,DMTET_EDGE,set);CHKERRQ(ierr);
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,NULL,&bpe,&element,NULL,&mesh_nbasis,NULL);CHKERRQ(ierr);
  ierr = PetscMalloc1(mesh_nbasis,&tag);CHKERRQ(ierr);
  for (k=0; k<mesh_nbasis; k++) {
    tag[k] = PETSC_FALSE;
  }
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  ierr = DMTetGListCheckBounds(facet_set,0,nfacets);CHKERRQ(ierr);

  /* loop through boundary facets */
  /* mark and count basis attached to the facet */
  for (k=0; k<facet_set->length; k++) {
    PetscInt facet_index;
    PetscInt e,face_id,*eidx_face;
    
    facet_index = facet_set->indices[k];
    
    e = facet_to_element[facet_index];
    face_id = facet_element_face_id[facet_index];
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    
    for (j=0; j<nbasis_face; j++) {
      PetscInt basis_idx;
      
      basis_idx = element[bpe*e + eidx_face[j]];
      tag[basis_idx] = PETSC_TRUE;
    }
  }
  
  count = 0;
  for (k=0; k<mesh_nbasis; k++) {
    if (tag[k]) { count++; }
  }

  ierr = DMTetGListSetSize(set,count);CHKERRQ(ierr);
  count = 0;
  for (k=0; k<mesh_nbasis; k++) {
    if (tag[k]) {
      set->indices[count] = k;
      count++;
    }
  }
  ierr = PetscFree(tag);CHKERRQ(ierr);
  
  ierr = DMTetGListSetPrimitiveType(set,DMTET_BASIS);CHKERRQ(ierr);
  ierr = DMTetGListSetup(set);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "_DMTetBFacetBasisListAssignFromInflow"
PetscErrorCode _DMTetBFacetBasisListAssignFromInflow(DM dm,DMTetGList from,DMTetGList to,DM dm_velocity,Vec velocity)
{
  BFacetList     f;
  PetscBool      is_inflow,issetup,*tag;
  PetscInt       nfacets,from_nfacets,nbasis_face,k,j,mesh_nbasis,count,_bpe,order,nel,nel_v;
  PetscInt       *facet_to_element,*facet_element_face_id,bpe,*element,bpe_v,*element_v;
  PetscReal      *normal,*tangent;
  const PetscReal *LA_velocity;
  const PetscInt *from_indices;
  PetscReal      *xilocal;
  Vec            velocity_local;
  PetscErrorCode ierr;
  

  if (!from->issetup) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires input DMTetGList[\"%s\"] to be setup",from->label_name);
  ierr = DMTetGListCheckCompatibility(from,DMTET_EDGE,to);CHKERRQ(ierr);
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nel,&bpe,&element,NULL,&mesh_nbasis,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm_velocity,&nel_v,&bpe_v,&element_v,NULL,NULL,NULL);CHKERRQ(ierr);
  
  if (nel != nel_v) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Input space and velocity space do not define the same number of elements");
  
  ierr = PetscMalloc1(mesh_nbasis,&tag);CHKERRQ(ierr);
  for (k=0; k<mesh_nbasis; k++) {
    tag[k] = PETSC_FALSE;
  }
  
  ierr = DMGetLocalVector(dm_velocity,&velocity_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm_velocity,velocity,INSERT_VALUES,velocity_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm_velocity,velocity,INSERT_VALUES,velocity_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(velocity_local,&LA_velocity);CHKERRQ(ierr);
  
  /* fetch layout of local basis */
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  ierr = DMTetMeshGenerateBasisCoords2d_CWARPANDBLEND(order,&_bpe,&xilocal);CHKERRQ(ierr);
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  ierr = BFacetListGetOrientations(f,&normal,&tangent);CHKERRQ(ierr);
  
  ierr = DMTetGListCheckBounds(from,0,nfacets);CHKERRQ(ierr);
  
  /* loop through boundary facets */
  /* mark and count basis attached to the facet */
  ierr = DMTetGListGetIndices(from,&from_nfacets,&from_indices);CHKERRQ(ierr);
  for (k=0; k<from_nfacets; k++) {
    PetscInt facet_index;
    PetscInt e,face_id,*eidx_face;
    PetscReal *normal_k;
    
    facet_index = from_indices[k];
    e = facet_to_element[facet_index];
    face_id = facet_element_face_id[facet_index];
    ierr = BFacetListGetCellFaceBasisId(f,face_id,&eidx_face);CHKERRQ(ierr);
    normal_k = &normal[2*k];
    
    for (j=0; j<nbasis_face; j++) {
      PetscInt basis_idx,nb,k,local_basis_idx;
      PetscReal **N,v_sp[2],v_dot_n;
      
      local_basis_idx = eidx_face[j];
      basis_idx = element[bpe*e + local_basis_idx];
      
      /* interpolate velocity to point */
      ierr = DMTetTabulateBasis(dm_velocity,1,&xilocal[2*local_basis_idx],&nb,&N);CHKERRQ(ierr);
      
      v_sp[0] = v_sp[1] = 0.0;
      for (k=0; k<bpe_v; k++) {
        v_sp[0] += N[0][k] * LA_velocity[ 2* element_v[bpe_v*e + k] + 0 ];
        v_sp[1] += N[0][k] * LA_velocity[ 2* element_v[bpe_v*e + k] + 1 ];
      }
      
      /* compute dot(v,n) */
      v_dot_n = v_sp[0] * normal_k[0] + v_sp[1] * normal_k[1];
      
      /* mark inflow */
      is_inflow = PETSC_FALSE;
      if (v_dot_n < 0.0) {
        is_inflow = PETSC_TRUE;
      }
      if (is_inflow) {
        tag[basis_idx] = PETSC_TRUE;
      }
      
      ierr = PetscFree(N[0]);CHKERRQ(ierr);
      ierr = PetscFree(N);CHKERRQ(ierr);
    }
  }
  
  count = 0;
  for (k=0; k<mesh_nbasis; k++) {
    if (tag[k]) { count++; }
  }
  
  ierr = DMTetGListSetSize(to,count);CHKERRQ(ierr);
  count = 0;
  for (k=0; k<mesh_nbasis; k++) {
    if (tag[k]) {
      to->indices[count] = k;
      count++;
    }
  }
  
  ierr = VecRestoreArrayRead(velocity_local,&LA_velocity);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm_velocity,&velocity_local);CHKERRQ(ierr);
  ierr = PetscFree(tag);CHKERRQ(ierr);
  ierr = PetscFree(xilocal);CHKERRQ(ierr);

  ierr = DMTetGListSetPrimitiveType(to,DMTET_BASIS);CHKERRQ(ierr);
  ierr = DMTetGListSetup(to);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBFacetBasisListAssignFromInflow"
PetscErrorCode DMTetBFacetBasisListAssignFromInflow(DM dm,const char set_name[],DM dm_velocity,Vec velocity)
{
  DMTetGList     facet_set,default_set;
  BFacetList     f;
  PetscBool      issetup;
  PetscInt       nfacets,nbasis_face;
  PetscErrorCode ierr;
  
  /* get the set from the name */
  ierr = DMGetDMTetGList(dm,set_name,&facet_set);CHKERRQ(ierr);
  
  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  ierr = DMTetGListCreateDefault(dm,nfacets,&default_set);CHKERRQ(ierr);
  ierr = DMTetGListSetPrimitiveType(default_set,DMTET_EDGE);CHKERRQ(ierr);
  ierr = DMTetGListSetup(default_set);CHKERRQ(ierr);
  ierr = _DMTetBFacetBasisListAssignFromInflow(dm,default_set,facet_set,dm_velocity,velocity);CHKERRQ(ierr);
  ierr = DMTetGListDestroy(&default_set);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBFacetBasisListAssignFromInflowSubset"
PetscErrorCode DMTetBFacetBasisListAssignFromInflowSubset(DM dm,const char name_from[],const char name_to[],DM dm_velocity,Vec velocity)
{
  DMTetGList     facet_set_from,facet_set_to;
  PetscErrorCode ierr;
  
  /* get the set from the name */
  ierr = DMGetDMTetGList(dm,name_from,&facet_set_from);CHKERRQ(ierr);
  if (!facet_set_from->issetup) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires input DMTetGList[\"%s\"] to be setup",facet_set_from->label_name);
  
  ierr = DMGetDMTetGList(dm,name_to,&facet_set_to);CHKERRQ(ierr);
  ierr = _DMTetBFacetBasisListAssignFromInflow(dm,facet_set_from,facet_set_to,dm_velocity,velocity);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGListMerge"
PetscErrorCode DMTetGListMerge(DM dm,PetscInt nfrom,const char *name_from[],const char name_to[])
{
  DMTetGList         *facet_set_from_l,facet_set_from,facet_set_to;
  DMTetPrimitiveType type0,type;
  PetscInt           k,i,count,length;
  PetscErrorCode     ierr;
  
  /* get the set from the name */
  ierr = PetscMalloc1(nfrom,&facet_set_from_l);CHKERRQ(ierr);
  for (k=0; k<nfrom; k++) {
    ierr = DMGetDMTetGList(dm,name_from[k],&facet_set_from);CHKERRQ(ierr);
    if (!facet_set_from->issetup) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires input DMTetGList[\"%s\"] to be setup",facet_set_from->label_name);
    facet_set_from_l[k] = facet_set_from;
  }
  
  ierr = DMTetGListGetPrimitiveType(facet_set_from_l[0],&type0);CHKERRQ(ierr);
  for (k=1; k<nfrom; k++) {
    ierr = DMTetGListGetPrimitiveType(facet_set_from_l[k],&type);CHKERRQ(ierr);
    if (type0 != type) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMTetGList to be merged must refer to same primitive type. list[0] -> %D whilst list[%D] -> %D",(PetscInt)type0,(PetscInt)type);
  }
  count = 0;
  for (k=0; k<nfrom; k++) {
    ierr = DMTetGListGetIndices(facet_set_from_l[k],&length,NULL);CHKERRQ(ierr);
    count += length;
  }
  
  ierr = DMGetDMTetGList(dm,name_to,&facet_set_to);CHKERRQ(ierr);
  ierr = DMTetGListSetSize(facet_set_to,count);CHKERRQ(ierr);
  count = 0;
  for (k=0; k<nfrom; k++) {
    const PetscInt *indices;
    
    ierr = DMTetGListGetIndices(facet_set_from_l[k],&length,&indices);CHKERRQ(ierr);
    for (i=0; i<length; i++) {
      facet_set_to->indices[count] = indices[i];
      count++;
    }
  }
  ierr = DMTetGListSetPrimitiveType(facet_set_to,type0);CHKERRQ(ierr);
  ierr = DMTetGListSetup(facet_set_to);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
