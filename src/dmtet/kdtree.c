/*
 
 K-d tree
 Copyright (C) 2012 RosettaCode User:Ledrug.
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 
 C source from:
 https://rosettacode.org/wiki/K-d_tree
 
 Page revision information:
 14:20, 8 April 2017‎ Trizen (Talk | contribs)‎ m . . (73,308 bytes) (0)‎ . . (→‎{{header|Sidef}}:  updated code) (undo)
 
 Notes regarding usage can be found under "C Entry":
 https://rosettacode.org/wiki/Talk:K-d_tree#C_Entry
 
 Caution / implementation limitation:
 - The method does not behave correctly if two input nodes (different pointer but identical x[] values)
   are placed within the kdtree.

 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 
 Changes made:
 + Added static keyword to inline functions
 
 + Renamed 
     struct kd_node_t 
  to 
     struct _p_kd_node_t
 
 + Added typedef struct _p_kd_node_t* kd_node (and updated functions accordingly)
 
 + Added the member 
     int index 
   into the definition of struct kd_node_t

 + Added the "namespace" straing kdtr_ to the original methods
     inline double dist()  --> double kdtr_dist()
     inline void swap()    --> kdtr_swap()
     kd_node find_median() --> kdtr_find_median()
     kd_node make_tree()   --> kdtr_make_tree()
     void nearest()        --> kdtr_nearest()
 
 + Renamed the global variable
     int visited
   to 
    int kdtr_visited
 
 + Added the object KDTree and helpers to generalize the implementation
 
*/

#include <petsc.h>
#include <kdtree.h>

#define MAX_DIM 2

struct _p_kd_node_t {
  PetscInt  index;
  PetscReal x[MAX_DIM];
  kd_node   left, right;
};

/* prototypes */
static inline PetscReal kdtr_dist(kd_node a, kd_node b, PetscInt dim);
static inline void      kdtr_swap(kd_node x, kd_node y);
kd_node                 kdtr_find_median(kd_node start, kd_node end, PetscInt idx);
kd_node                 kdtr_make_tree(kd_node t, PetscInt len, PetscInt i, PetscInt dim);
void                    kdtr_nearest(kd_node root, kd_node nd, PetscInt i, PetscInt dim, kd_node *best, PetscReal *best_dist);

/* global variable, so sue me */
PetscInt kdtr_visited;


#undef __FUNCT__
#define __FUNCT__ "KDTreeCreate"
PetscErrorCode KDTreeCreate(PetscInt npoints,PetscInt dim,KDTree *_k)
{
  KDTree kt;
  PetscInt k;
  
  kt = (KDTree)malloc(sizeof(struct _p_KDTree));
  kt->root = NULL;
  kt->point = NULL;
  kt->npoints = npoints;
  kt->dim = dim;
  kt->visited = 0;
  kt->setup = 0;
  kt->point =(kd_node)calloc(kt->npoints, sizeof(struct _p_kd_node_t));
  for (k=0; k<kt->npoints; k++) {
    kt->point[k].index = -1;
    kt->point[k].left = NULL;
    kt->point[k].right = NULL;
  }
  *_k = kt;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeDestroy"
PetscErrorCode KDTreeDestroy(KDTree *_k)
{
  KDTree k;
  
  k = *_k;
  free(k->point);
  free(k);

  *_k = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeInsertPoint"
PetscErrorCode KDTreeInsertPoint(KDTree k,PetscInt index,PetscReal coor[])
{
  PetscInt d;
  
  if (k->setup == 1) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"[kdtree error] KDTree already setup. Cannot call KDTreeInsertPoint() after KDTreeSetup() has been called\n");
  }
  k->point[index].index = index;
  for (d=0; d<k->dim; d++) {
    k->point[index].x[d] = coor[d];
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeSetup"
PetscErrorCode KDTreeSetup(KDTree kt)
{
  PetscInt k;
  
  if (kt->setup == 1) PetscFunctionReturn(0);
  
  // check all points were inserted
  for (k=0; k<kt->npoints; k++) {
    if (kt->point[k].index == -1) {
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"[kdtree error] Point %d has not been initialized\n",k);
    }
  }
  
  // make_tree
  if (!kt->root) {
    PetscLogDouble t0,t1;
    
    PetscTime(&t0);
    kt->root = kdtr_make_tree(&kt->point[0], kt->npoints, 0, kt->dim);
    PetscTime(&t1);
    PetscPrintf(PETSC_COMM_SELF,"[KDTreeSetup]: npoints %D : time %+1.3e (sec)\n",kt->npoints,t1-t0);
  }
  kt->setup = 1;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeReset"
PetscErrorCode KDTreeReset(KDTree kt)
{
  PetscInt k;
  
  // reset tree
  for (k=0; k<kt->npoints; k++) {
    kt->point[k].left = NULL;
    kt->point[k].right = NULL;
  }
  kt->root = NULL;

  kt->setup = 0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeGetPoint"
PetscErrorCode KDTreeGetPoint(KDTree k,PetscInt index,kd_node *node)
{
  if (k->setup == 0) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"[kdtree error] KDTree not setup. Must call KDTreeSetup() before KDTreeGetPoint()\n");
  }
  *node = &k->point[index];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeDistanceQuery"
PetscErrorCode KDTreeDistanceQuery(KDTree k,PetscReal coor[],PetscInt *nearest,PetscReal *sep)
{
  struct _p_kd_node_t testNode;
  kd_node found = NULL;
  PetscReal best_dist = 1.0e32;
  PetscInt d;
  PetscErrorCode ierr;
  
  /*
  if (k->setup == 0) {
    printf("[kdtree error] KDTree not setup. Must call KDTreeSetup() before KDTreeDistanceQuery().\n");
    exit(1);
  }
  */
  if (k->setup == 0) {
    ierr = KDTreeSetup(k);CHKERRQ(ierr);
  }
  
  kdtr_visited = 0;
  
  testNode.index = 0;
  for (d=0; d<k->dim; d++) {
    testNode.x[d] = coor[d];
  }
  
  kdtr_nearest(k->root, &testNode, 0, k->dim, &found, &best_dist);

  k->visited = kdtr_visited;
  *nearest = found->index;
  if (sep) { *sep = best_dist; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "KDTreeNDistanceQuery"
PetscErrorCode KDTreeNDistanceQuery(KDTree k,PetscInt npoints,PetscReal coor[],PetscInt nearest[])
{
  struct _p_kd_node_t testNode;
  kd_node found = NULL;
  PetscReal best_dist = 1.0e32;
  PetscInt d,p;
  PetscLogDouble t0,t1;
  PetscErrorCode ierr;
  
  if (k->setup == 0) {
    ierr = KDTreeSetup(k);CHKERRQ(ierr);
  }
  
  kdtr_visited = 0;
  PetscTime(&t0);
  for (p=0; p<npoints; p++) {
    PetscReal *coor_p;
    
    coor_p = &coor[k->dim*p];
    
    testNode.index = 0;
    for (d=0; d<k->dim; d++) {
      testNode.x[d] = coor_p[d];
    }
    
    found = NULL;
    best_dist = 1.0e32;

    kdtr_nearest(k->root, &testNode, 0, k->dim, &found, &best_dist);
    
    nearest[p] = found->index;
    //{
    //  printf("p [%d] ( %+1.4e %+1.4e ) : closest %d ( %+1.4e %+1.4e ) : sep2 %+1.4e\n",
    //         p,coor_p[0],coor_p[1],found->index,found->x[0],found->x[1],best_dist);
    //}
  }
  PetscTime(&t1);
  k->visited = kdtr_visited;
  PetscPrintf(PETSC_COMM_SELF,"[KDTreeNDistanceQuery]: nqueries %D : time %+1.3e (sec) : visits %D\n",npoints,t1-t0,k->visited);
  
  PetscFunctionReturn(0);
}


static inline PetscReal kdtr_dist(kd_node a, kd_node b, PetscInt dim)
{
  PetscReal t, d = 0;
  while (dim--) {
    t = a->x[dim] - b->x[dim];
    d += t * t;
  }
  return d;
}

static inline void kdtr_swap(kd_node x, kd_node y)
{
  PetscInt   tmp_i;
  PetscReal tmp[MAX_DIM];

  memcpy(tmp,  x->x, sizeof(tmp));
  tmp_i = x->index;

  memcpy(x->x, y->x, sizeof(tmp));
  x->index = y->index;
  
  memcpy(y->x, tmp,  sizeof(tmp));
  y->index = tmp_i;
}


/* see quickselect method */
kd_node kdtr_find_median(kd_node start, kd_node end, PetscInt idx)
{
  kd_node p,store,md ;
  PetscReal pivot;

  if (end <= start) return NULL;
  if (end == start + 1)
    return start;
  
  md = start + (end - start) / 2;

  while (1) {
    pivot = md->x[idx];
    
    kdtr_swap(md, end - 1);
    for (store = p = start; p < end; p++) {
      if (p->x[idx] < pivot) {
        if (p != store)
          kdtr_swap(p, store);
        store++;
      }
    }
    kdtr_swap(store, end - 1);
    
    /* median has duplicate values */
    if (store->x[idx] == md->x[idx])
      return md;
    
    if (store > md) end = store;
    else        start = store;
  }
}

kd_node kdtr_make_tree(kd_node t, PetscInt len, PetscInt i, PetscInt dim)
{
  kd_node n;
  
  if (!len) return 0;
  
  if ((n = kdtr_find_median(t, t + len, i))) {
    i = (i + 1) % dim;
    n->left  = kdtr_make_tree(t, n - t, i, dim);
    n->right = kdtr_make_tree(n + 1, t + len - (n + 1), i, dim);
  }
  return n;
}


void kdtr_nearest(kd_node root, kd_node nd, PetscInt i, PetscInt dim,
             kd_node *best, PetscReal *best_dist)
{
  PetscReal d, dx, dx2;
  
  if (!root) return;
  d = kdtr_dist(root, nd, dim);
  dx = root->x[i] - nd->x[i];
  dx2 = dx * dx;
  
  kdtr_visited ++;
  
  if (!*best || d < *best_dist) {
    *best_dist = d;
    *best = root;
  }
  
  /* if chance of exact match is high */
  if (!*best_dist) return;
  
  if (++i >= dim) i = 0;
  
  kdtr_nearest(dx > 0 ? root->left : root->right, nd, i, dim, best, best_dist);
  if (dx2 >= *best_dist) return;
  kdtr_nearest(dx > 0 ? root->right : root->left, nd, i, dim, best, best_dist);
}

#if 0

#define N 1000000
#define rand1() (rand() / (double)RAND_MAX)
#define rand_pt(v) { v.x[0] = rand1(); v.x[1] = rand1(); v.x[2] = rand1(); }

int ex1(void)
{
  int i;
  struct _p_kd_node_t wp[] = {
    {0,{2, 3}}, {1,{5, 4}}, {2,{9, 6}}, {3,{4, 7}}, {4,{8, 1}}, {5,{7, 2}}
  };
  struct _p_kd_node_t testNode = {0,{9, 2}};
  kd_node root, found, million;
  double best_dist;
  
  root = kdtr_make_tree(wp, sizeof(wp) / sizeof(wp[1]), 0, 2);
  
  kdtr_visited = 0;
  found = 0;
  kdtr_nearest(root, &testNode, 0, 2, &found, &best_dist);
  
  printf(">> WP tree\nsearching for (%g, %g)\n"
         "found [%d](%g, %g) dist %g\nseen %d nodes\n\n",
         testNode.x[0], testNode.x[1],
         found->index,found->x[0], found->x[1], sqrt(best_dist), kdtr_visited);
  
  million =(kd_node) calloc(N, sizeof(struct _p_kd_node_t));

  srand(0);
  for (i = 0; i < N; i++) {
    million[i].index = i;
    rand_pt(million[i]);
  }
  
  root = kdtr_make_tree(million, N, 0, 3);
  rand_pt(testNode);
  
  kdtr_visited = 0;
  found = 0;
  kdtr_nearest(root, &testNode, 0, 3, &found, &best_dist);
  
  printf(">> Million tree\nsearching for (%g, %g, %g)\n"
         "found [%d](%g, %g, %g) dist %g\nseen %d nodes\n",
         testNode.x[0], testNode.x[1], testNode.x[2],
         found->index,found->x[0], found->x[1], found->x[2],
         sqrt(best_dist), kdtr_visited);
  
  /* search many random points in million tree to see average behavior.
   tree size vs avg nodes visited:
   10      ~  7
   100     ~ 16.5
   1000        ~ 25.5
   10000       ~ 32.8
   100000      ~ 38.3
   1000000     ~ 42.6
   10000000    ~ 46.7              */
  int sum = 0, test_runs = 100000;
  for (i = 0; i < test_runs; i++) {
    found = 0;
    kdtr_visited = 0;
    rand_pt(testNode);
    kdtr_nearest(root, &testNode, 0, 3, &found, &best_dist);
    sum += kdtr_visited;
  }
  printf("\n>> Million tree\n"
         "visited %d nodes for %d random findings (%f per lookup)\n",
         sum, test_runs, sum/(double)test_runs);
  
  // free(million);
  
  return 0;
}

void ex2(void)
{
  
  int npoints;
  int dim;
  KDTree k;
  int i,nearest;
  struct _p_kd_node_t wp[] = {
    {0,{2, 3}}, {1,{5, 4}}, {2,{9, 6}}, {3,{4, 7}}, {4,{8, 1}}, {5,{7, 2}}
  };
  struct _p_kd_node_t testNode = {0,{9, 2}};
  double sep2;
  
  
  dim = 2;
  npoints = sizeof(wp) / sizeof(wp[1]);
  
  KDTreeCreate(npoints,dim,&k);
  
  for (i=0; i<npoints; i++) {
    KDTreeInsertPoint(k,i,wp[i].x);
  }
  KDTreeSetup(k);

  KDTreeDistanceQuery(k,testNode.x,&nearest,&sep2);
  
  printf(">> KDTree \nsearching for (%g, %g)\n"
         "found [%d](%g, %g) dist %g\nseen %d nodes\n\n",
         testNode.x[0], testNode.x[1],
         nearest,k->point[nearest].x[0], k->point[nearest].x[1], sqrt(sep2), kdtr_visited);
  
  KDTreeDestroy(&k);
}

void ex3(void)
{
  
  int npoints;
  int dim;
  KDTree k;
  int i,nearest;
  struct _p_kd_node_t wp;
  struct _p_kd_node_t testNode = {0,{9, 2}};
  double sep2;
  
  
  dim = 3;
  npoints = N;
  
  KDTreeCreate(npoints,dim,&k);

  srand(0);
  for (i=0; i<npoints; i++) {
    rand_pt(wp);
    KDTreeInsertPoint(k,i,wp.x);
  }
  KDTreeSetup(k);
  
  rand_pt(testNode);
  KDTreeDistanceQuery(k,testNode.x,&nearest,&sep2);

  printf(">> Million tree\nsearching for (%g, %g, %g)\n"
         "found [%d](%g, %g, %g) dist %g\nseen %d nodes\n",
         testNode.x[0], testNode.x[1], testNode.x[2],
         nearest,k->point[nearest].x[0], k->point[nearest].x[1], k->point[nearest].x[2],
         sqrt(sep2), kdtr_visited);

  KDTreeDestroy(&k);
}

int main(void)
{
  ex1();

  //ex2();
  
  //for (int k=0; k<100000; k++) {
  ex3();
  //}
  return(0);
}

#endif
