
#include <petsc.h>
#include <dmtetimpl.h>
#include <petscdmtet.h>
#include <dmtet_topology.h>


/* helpers for VLIndex */
#undef __FUNCT__
#define __FUNCT__ "VLIndexCreate"
PetscErrorCode VLIndexCreate(VLIndex *_vl)
{
  VLIndex vl;
  PetscErrorCode ierr;

  ierr = PetscMalloc1(1,&vl);CHKERRQ(ierr);
  ierr = PetscMemzero(vl,sizeof(struct _p_VLIndex));CHKERRQ(ierr);
  vl->L = 0;
  vl->iL = NULL;
  vl->value = NULL;
  *_vl = vl;
  
  PetscFunctionReturn(0);
}

/* Destroys contents of VLIndex */
#undef __FUNCT__
#define __FUNCT__ "VLIndexReset"
PetscErrorCode VLIndexReset(VLIndex vl)
{
  PetscInt k;
  PetscErrorCode ierr;

  if (!vl) PetscFunctionReturn(0);
  if (vl->value) {
    for (k=0; k<vl->L; k++) {
      if (vl->value[k]) {
        free(vl->value[k]);
        vl->value[k] = NULL;
      }
    }
    ierr = PetscFree(vl->value);CHKERRQ(ierr);
    vl->value = NULL;
  }

  if (vl->iL) {
    ierr = PetscFree(vl->iL);CHKERRQ(ierr);
    vl->iL = NULL;
  }
  
  vl->L = 0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VLIndexDestroy"
PetscErrorCode VLIndexDestroy(VLIndex *_vl)
{
  VLIndex vl;
  PetscErrorCode ierr;

  if (!_vl) PetscFunctionReturn(0);
  vl = *_vl;
  ierr = VLIndexReset(vl);CHKERRQ(ierr);
  ierr = PetscFree(vl);CHKERRQ(ierr);
  *_vl = NULL;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FEGTopologyCreate"
PetscErrorCode FEGTopologyCreate(FEGTopology *_t)
{
  FEGTopology t;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&t);CHKERRQ(ierr);
  ierr = PetscMemzero(t,sizeof(struct _p_FEGTopology));CHKERRQ(ierr);
  t->has_valid_e2e = PETSC_FALSE;
  t->has_valid_v2e = PETSC_FALSE;
  t->has_valid_v2v = PETSC_FALSE;
  ierr = VLIndexCreate(&t->element_to_element);CHKERRQ(ierr);
  ierr = VLIndexCreate(&t->vert_to_element);CHKERRQ(ierr);
  ierr = VLIndexCreate(&t->vert_to_vert);CHKERRQ(ierr);
  *_t = t;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FEGTopologyDestroy"
PetscErrorCode FEGTopologyDestroy(FEGTopology *_t)
{
  FEGTopology t;
  PetscErrorCode ierr;

  if (!_t) PetscFunctionReturn(0);
  t = *_t;
  ierr = VLIndexDestroy(&t->element_to_element);CHKERRQ(ierr);
  ierr = VLIndexDestroy(&t->vert_to_element);CHKERRQ(ierr);
  ierr = VLIndexDestroy(&t->vert_to_vert);CHKERRQ(ierr);
  ierr = PetscFree(t);CHKERRQ(ierr);
  *_t = NULL;

  PetscFunctionReturn(0);
}


/* implementations to generate maps */
PetscErrorCode _DMTetMapCreate_Vertex2Element(PetscInt ne,PetscInt npe,PetscInt elndmap[],PetscInt nnodes,
                                   VLIndex vlarray);

PetscErrorCode _DMTetMapCreate_Element2Element_fromV2E(PetscInt nelements,PetscInt nnodes,
                                                       PetscInt npe,PetscInt elnd_map[],
                                                       VLIndex v2e_map,
                                                       PetscInt ncommon,VLIndex vlarray);

#undef __FUNCT__
#define __FUNCT__ "FEGTopologySetup_E2E"
PetscErrorCode FEGTopologySetup_E2E(FEGTopology t,DM_TET *dmtetctx)
{
  PetscErrorCode ierr;
  DMTetMesh *tet;
  
  if (t->has_valid_e2e) PetscFunctionReturn(0);
  
  /* Can build this efficiently if other maps are available */
  ierr = FEGTopologySetup_V2E(t,dmtetctx);CHKERRQ(ierr);
  
  tet = dmtetctx->geometry;
  ierr = _DMTetMapCreate_Element2Element_fromV2E(tet->nelements,tet->nnodes,tet->npe,tet->element,
                                                 t->vert_to_element,2, /* a neighbour elements is defined if two vertices are shared */
                                                 t->element_to_element);CHKERRQ(ierr);
  t->has_valid_e2e = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FEGTopologySetup_V2V"
PetscErrorCode FEGTopologySetup_V2V(FEGTopology t,DM_TET *dmtetctx)
{
  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Vertex-to-vertex topological relationship is not supported");
  if (t->has_valid_v2v) PetscFunctionReturn(0);
  
  /* Can build this efficiently if other maps are available */
  
  t->has_valid_v2v = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FEGTopologySetup_V2E"
PetscErrorCode FEGTopologySetup_V2E(FEGTopology t,DM_TET *dmtetctx)
{
  PetscErrorCode ierr;
  DMTetMesh *tet;

  if (t->has_valid_v2e) PetscFunctionReturn(0);
  
  tet = dmtetctx->geometry;
  ierr = _DMTetMapCreate_Vertex2Element(tet->nelements+tet->nghost_elements,tet->npe,tet->element,tet->nnodes+tet->nghost_nodes,t->vert_to_element);CHKERRQ(ierr);
  t->vert_to_element->L = tet->nnodes; /* exclude ghost nodes from being accessed */
  t->has_valid_v2e = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

/*
 No dependence on other topological relations beside the default element->node map
*/
#undef __FUNCT__
#define __FUNCT__ "_DMTetMapCreate_Vertex2Element"
PetscErrorCode _DMTetMapCreate_Vertex2Element(PetscInt ne,PetscInt npe,PetscInt elndmap[],PetscInt nnodes,
                                   VLIndex vlarray)
{
  PetscInt n,e,ce;
  PetscInt *mapcnt;
  PetscInt *mapcnt_max;
  PetscInt **map;
  PetscLogDouble t0,t1;
  PetscErrorCode ierr;
  
  
  PetscTime(&t0);
  ierr = PetscMalloc1(nnodes,&mapcnt);CHKERRQ(ierr);
  ierr = PetscMalloc1(nnodes,&mapcnt_max);CHKERRQ(ierr);
  for (n=0; n<nnodes; n++) {
    mapcnt[n] = 0; /* current count */
    mapcnt_max[n] = 6; /* current allocation */
  }
  
  ierr = PetscMalloc1(nnodes,&map);CHKERRQ(ierr);
  for (n=0; n<nnodes; n++) {
    map[n] = malloc(sizeof(PetscInt)*mapcnt_max[n]);
  }
  
  for (e=0; e<ne; e++) {
    PetscInt *elnodes;
    
    elnodes = &elndmap[e*npe];
    
    for (n=0; n<npe; n++) {
      PetscInt nid,found;
      
      nid = elnodes[n];
      
      /* check if e is in current list for connected elements with node index nid */
      found = 0;
      for (ce=0; ce<mapcnt[nid]; ce++) {
        if (map[nid][ce] == e) {
          found = 1;
          break;
        }
      }
      
      if (found == 0) {
        /* check size and realloc */
        if (mapcnt[nid] >= mapcnt_max[nid]) {
          PetscInt *buffer;
          
          mapcnt_max[nid] = mapcnt_max[nid] + 6;
          buffer = realloc(map[nid],sizeof(PetscInt)*(mapcnt_max[nid]));
          
          map[nid] = buffer;
        }
        
        map[nid][mapcnt[nid]] = e;
        mapcnt[nid]++;
      }
    }
  }
  
  ierr = PetscFree(mapcnt_max);CHKERRQ(ierr);
  
  /* sanity check */
  for (n=0; n<nnodes; n++) {
    if (mapcnt[n] < 1) PetscPrintf(PETSC_COMM_SELF,"Warning: Node [%D] must be connected to at least 1 element\n",n);
  }
  
  PetscTime(&t1);
  //PetscPrintf(PETSC_COMM_WORLD,"DMTetTopologySetup [V->E] %1.4e (sec)\n",t1-t0);
  
  /*
   for (e=0; e<ne; e++) {
   PetscInt *e_nodes;
   
   e_nodes = &elndmap[e*npe];
   printf("e=%.4d: nodes = %.4d %.4d %.4d %.4d\n",e,e_nodes[0],e_nodes[1],e_nodes[2],e_nodes[3]);
   }
   printf("_____\n");
   
   for (n=0; n<nnodes; n++) {
   PetscInt kk;
   printf("n=%.4d: neigh_el = %.4d\n",n,mapcnt[n]);
   for (kk=0; kk<mapcnt[n]; kk++) {
   printf("  --> %.4d\n",map[n][kk]);
   }
   }
   printf("_____\n");
   */
  
  vlarray->L     = nnodes;
  vlarray->iL    = mapcnt;
  vlarray->value = map;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_ElementElementCompare_single"
PetscInt _ElementElementCompare_single(PetscInt npe,PetscInt lista[],PetscInt listb[])
{
  PetscInt i,j;
  
  for (i=0; i<npe; i++) {
    for (j=0; j<npe; j++) {
      if (lista[i] == listb[j]) {
        return(1);
      }
    }
  }
  return(0);
}

#undef __FUNCT__
#define __FUNCT__ "_ElementElementCompare_many"
PetscInt _ElementElementCompare_many(PetscInt npe,PetscInt ncommon,PetscInt lista[],PetscInt listb[])
{
  PetscInt i,j;
  PetscInt neighbour = 0;
  PetscInt cnt = 0;
  
  for (i=0; i<npe; i++) {
    for (j=0; j<npe; j++) {
      if (lista[i] == listb[j]) {
        cnt++;
        break;
      }
    }
  }
  if (cnt >= ncommon) { neighbour = 1; }
  
  return(neighbour);
}

#undef __FUNCT__
#define __FUNCT__ "_ElementElementCompare"
PetscInt _ElementElementCompare(PetscInt npe,PetscInt ncommon,PetscInt lista[],PetscInt listb[])
{
  PetscInt neighbour;
  
  if (ncommon == 1) {
    neighbour = _ElementElementCompare_single(npe,lista,listb);
  } else {
    neighbour = _ElementElementCompare_many(npe,ncommon,lista,listb);
  }
  
  return(neighbour);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetMapCreate_Element2Element_fromV2E"
PetscErrorCode _DMTetMapCreate_Element2Element_fromV2E(PetscInt nelements,PetscInt nnodes,
                                             PetscInt npe,PetscInt elnd_map[],
                                             VLIndex v2e_map,
                                             PetscInt ncommon,VLIndex vlarray)
{
  PetscInt n,e,k,ce;
  PetscInt *mapcnt;
  PetscInt *mapcnt_max;
  PetscInt **map;
  PetscLogDouble t0,t1;
  PetscInt *ndel_mapcnt,**ndel_map;
  PetscErrorCode ierr;
  
  ndel_mapcnt = v2e_map->iL;
  ndel_map    = v2e_map->value;
  
  PetscTime(&t0);
  ierr = PetscMalloc1(nelements,&mapcnt);CHKERRQ(ierr);
  ierr = PetscMalloc1(nelements,&mapcnt_max);CHKERRQ(ierr);
  for (e=0; e<nelements; e++) {
    mapcnt[e] = 0; /* current count */
    mapcnt_max[e] = 6; /* current allocation */
  }
  
  ierr = PetscMalloc1(nelements,&map);CHKERRQ(ierr);
  for (e=0; e<nelements; e++) {
    map[e] = malloc(sizeof(PetscInt)*mapcnt_max[e]);
  }
  
  for (e=0; e<nelements; e++) {
    PetscInt *e_nodes;
    PetscInt *e_neigh_nodes;
    PetscInt found;
    
    e_nodes = &elnd_map[e*npe];
    
    for (n=0; n<npe; n++) {
      PetscInt nid;
      
      nid = e_nodes[n];
      
      /* loop over connected element of each node */
      for (ce=0; ce<ndel_mapcnt[nid]; ce++) {
        PetscInt e_neigh;
        PetscInt neighbour_element;
        
        e_neigh = ndel_map[nid][ce];
        
        /* We don't consider the trival case of an element connected to itself */
        if (e == e_neigh) { continue; }
        
        e_neigh_nodes = &elnd_map[e_neigh*npe];
        
        /* check if there are ncommon nodes */
        neighbour_element = _ElementElementCompare(npe,ncommon,e_nodes,e_neigh_nodes);
        
        if (neighbour_element == 0) { continue; }
        
        /* check if e is in current list for node index nid */
        found = 0;
        for (k=0; k<mapcnt[e]; k++) {
          if (map[e][k] == e_neigh) {
            found = 1;
            break;
          }
        }
        if (found == 0) {
          /* check size and realloc */
          if (mapcnt[e] >= mapcnt_max[e]) {
            PetscInt *buffer;
            
            mapcnt_max[e] = mapcnt_max[e] + 6;
            buffer = realloc(map[e],sizeof(PetscInt)*(mapcnt_max[e]));
            
            map[e] = buffer;
          }
          
          map[e][mapcnt[e]] = e_neigh;
          mapcnt[e]++;
        }
        
      }
    }
  }
  
  ierr = PetscFree(mapcnt_max);CHKERRQ(ierr);
  
  /* sanity check */
  for (e=0; e<nelements; e++) {
    if (mapcnt[e] < 1) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Element must be connected to at least 1 other element");
  }
  
  PetscTime(&t1);
  //PetscPrintf(PETSC_COMM_WORLD,"DMTetTopologySetup [E->E] %1.4e (sec)\n",t1-t0);
  
  /*
   for (e=0; e<nelements; e++) {
   PetscInt kk;
   printf("e=%.4d: neigh_els = %.4d\n",e,mapcnt[e]);
   for (kk=0; kk<mapcnt[e]; kk++) {
   printf("  --> %.4d\n",map[e][kk]);
   }
   }
   printf("_____\n");
   */
  
  vlarray->L     = nelements;
  vlarray->iL    = mapcnt;
  vlarray->value = map;
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetTopologySetup"
PetscErrorCode DMTetTopologySetup(DM dm,PetscInt ttype)
{
  PetscErrorCode ierr;
  DM_TET *tet;
  DMTetMesh *geometry;
  FEGTopology t;
  PetscBool istet = PETSC_FALSE;

  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");

  tet = (DM_TET*)dm->data;
  geometry = tet->geometry;
  t = geometry->topology;
  
  switch (ttype) {
    case 0:
      ierr = FEGTopologySetup_V2E(t,tet);CHKERRQ(ierr);
      break;
    
    case 1:
      ierr = FEGTopologySetup_E2E(t,tet);CHKERRQ(ierr);
      break;

    case 2:
      ierr = FEGTopologySetup_V2V(t,tet);CHKERRQ(ierr);
      break;
      
    default:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Unknown topological relationship requested. Arg 2 must be [0,1,2] for [V2E,E2E,V2V]");
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGetFEGTopology"
PetscErrorCode DMTetGetFEGTopology(DM dm,FEGTopology *topo)
{
  PetscErrorCode ierr;
  DM_TET *tet;
  DMTetMesh *geometry;
  FEGTopology t;
  PetscBool istet = PETSC_FALSE;
  
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_ARG_WRONG,"Arg 1 must be type DMTET");
  
  tet = (DM_TET*)dm->data;
  geometry = tet->geometry;
  t = geometry->topology;
  *topo = t;
  PetscFunctionReturn(0);
}
