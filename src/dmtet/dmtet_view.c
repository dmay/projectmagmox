

#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <dmtetimpl.h>
#include <mpiio_blocking.h>

#undef __FUNCT__
#define __FUNCT__ "sumdec_WAB"
static PetscInt sumdec_WAB(PetscInt a)
{
  PetscInt res,ac;
  
  res = a;
  ac = a;
  while (ac != 0) {
    ac--;
    res = res + ac;
  }
  return(res);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTWriteFieldsCG_AsciiVTU"
PetscErrorCode _DMTWriteFieldsCG_AsciiVTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char filename[])
{
  /*DM_TET *tet = (DM_TET*)dm->data;*/
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt f,i,j,e,dof;
  const PetscScalar *LA_field;
  PetscInt nelements,npe,nnodes,*element,basis_order;
  PetscReal *coords;
  DMTetBasisType btype;
  PetscInt nspilt,nfull,nsubel,nel_l;
  
  PetscFunctionBegin;
  fp = fopen(filename,"w");
  if (fp == NULL) {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Cannot open file %s",filename);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render continuous basis (DMTET_CWARP_AND_BLEND)");
  }
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements * nsubel;
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes,nel_l);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &element[npe*e];
    PetscInt map[30][30],np,cnt;
    
    /* init map */
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        map[i][j] = 0;
      }
    }
    
    np = basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        
        if (i == np-j-2) {
          /* last triangle - located on side */
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
        } else {
          /* two triangles to define */
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
        }
      }
    }
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",coords[2*i],coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  /*
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"l2g\" NumberOfComponents=\"1\" format=\"ascii\"> \n");
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%d ",tet->mpi->l2g[i]);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  */
  
  for (f=0; f<nf; f++) {
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    ierr = VecGetArrayRead(field[f],&LA_field);CHKERRQ(ierr);
    if (dof == 2) {
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"3\" format=\"ascii\"> \n",fname[f]);
      for (i=0; i<nnodes; i++) {
        PetscReal u1,u2;
        
        u1 = LA_field[2*i];    if (PetscAbsReal(u1) < 1.0e-12) { u1 = 0.0; }
        u2 = LA_field[2*i+1];  if (PetscAbsReal(u2) < 1.0e-12) { u2 = 0.0; }
        PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e 0.0 ",u1,u2);
      } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
      PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
      
    } else if (dof == 1) {
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"1\" format=\"ascii\"> \n",fname[f]);
      for (i=0; i<nnodes; i++) {
        PetscReal scalar;
        
        scalar = LA_field[i]; if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
        PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",scalar);
      } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
      PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
      
    } else {
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"%D\" format=\"ascii\"> \n",fname[f],dof);
      for (i=0; i<nnodes; i++) {
        PetscReal scalar;
        PetscInt d;
        
        for (d=0; d<dof; d++) {
          scalar = LA_field[dof*i+d];
          if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
          PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",scalar);
        }
      } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
      PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
      
    }
    ierr = VecRestoreArrayRead(field[f],&LA_field);CHKERRQ(ierr);
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

PetscErrorCode DMTetMeshGenerateBasisCoords2d_DG(PetscInt order,PetscInt *_npe,PetscReal **_xi);

#undef __FUNCT__
#define __FUNCT__ "_DMTWriteFieldsDG_AsciiVTU"
PetscErrorCode _DMTWriteFieldsDG_AsciiVTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char filename[])
{
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt f,i,j,e,dof;
  const PetscScalar *LA_field;
  PetscInt nelements,nbasis,npe_g,nnodes,*element,*element_g,basis_order;
  PetscReal *coords_g;
  DMTetBasisType btype;
  PetscInt nspilt,nfull,nsubel,nel_l,nnodes_l;
  PetscInt npoints;
  PetscReal *xi,**N;
  
  PetscFunctionBegin;
  fp = fopen(filename,"w");
  if (fp == NULL) {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Cannot open file %s",filename);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_DG) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render discontinuous basis (DMTET_DG)");
  }
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetMeshGenerateBasisCoords2d_DG(basis_order,&npoints,&xi);
  {
    DM_TET *tet = (DM_TET*)dm->data;
    PetscInt basis_order_ref,nb;
    DMTetBasisType basis_type_ref;
    
    /* temporarily over-ride basis definition */
    basis_type_ref = tet->basis_type;
    basis_order_ref = tet->basis_order;
    tet->basis_type = DMTET_CWARP_AND_BLEND;
    tet->basis_order = 1;
    
    ierr = DMTetTabulateBasis(dm,npoints,xi,&nb,&N);CHKERRQ(ierr);
    
    /* reset basis definition */
    tet->basis_type = basis_type_ref;
    tet->basis_order = basis_order_ref;
  }
  ierr = DMTetGeometryElement(dm,NULL,NULL,&element_g,&npe_g,NULL,&coords_g);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,&nbasis,&element,NULL,&nnodes,NULL);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements * nsubel;
  nnodes_l = nelements * nbasis;
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes_l,nel_l);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &element[nbasis*e];
    PetscInt map[30][30],np,cnt;
    
    /* init map */
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        map[i][j] = 0;
      }
    }
    
    np = basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        
        if (i == np-j-2) {
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
        } else {
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          
          PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                       map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
        }
      }
    }
    
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<nel_l; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (e=0; e<nelements; e++) {
    PetscInt *idx_g = &element_g[npe_g*e];
    
    /* interpolate coordinates */
    for (i=0; i<nbasis; i++) {
      PetscReal coor[2];
      
      coor[0] = coor[1] = 0.0;
      for (j=0; j<npe_g; j++) {
        coor[0] += N[i][j] * coords_g[ 2*idx_g[j] + 0 ];
        coor[1] += N[i][j] * coords_g[ 2*idx_g[j] + 1 ];
      }
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",coor[0],coor[1]);
    }
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  for (f=0; f<nf; f++) {
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    ierr = VecGetArrayRead(field[f],&LA_field);CHKERRQ(ierr);

    if (dof == 2) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"3\" format=\"ascii\"> \n",fname[f]);
      for (e=0; e<nelements; e++) {
        PetscInt *idx = &element[nbasis*e];
        
        for (i=0; i<nbasis; i++) {
          PetscReal u1,u2;
          
          u1 = LA_field[2*idx[i]];    if (PetscAbsReal(u1) < 1.0e-12) { u1 = 0.0; }
          u2 = LA_field[2*idx[i]+1];  if (PetscAbsReal(u2) < 1.0e-12) { u2 = 0.0; }
          PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e 0.0 ",u1,u2);
        }
      } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
      PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
    } else if (dof == 1) {
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"1\" format=\"ascii\"> \n",fname[f]);
      for (e=0; e<nelements; e++) {
        PetscInt *idx = &element[nbasis*e];
        
        for (i=0; i<nbasis; i++) {
          PetscReal scalar;
          
          scalar = LA_field[idx[i]]; if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
          PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",scalar);
        }
      } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
      PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
    } else {
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"1\" format=\"ascii\"> \n",fname[f]);
      for (e=0; e<nelements; e++) {
        PetscInt *idx = &element[nbasis*e];
        
        for (i=0; i<nbasis; i++) {
          PetscInt d;
          
          for (d=0; d<dof; d++) {
            PetscReal scalar_d;
            
            scalar_d = LA_field[dof*idx[i] + d]; if (PetscAbsReal(scalar_d) < 1.0e-12) { scalar_d = 0.0; }
            PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",scalar_d);
          }
        }
      } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
      PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
    }
    ierr = VecRestoreArrayRead(field[f],&LA_field);CHKERRQ(ierr);
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  ierr = PetscFree(xi);CHKERRQ(ierr);
  for (i=0; i<npoints; i++) {
    ierr = PetscFree(N[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree(N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetViewFields_VTU"
PetscErrorCode _DMTetViewFields_VTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char filename[])
{
  PetscErrorCode ierr;
  Vec *floc;
  PetscInt f,bs0,bs;
  DMTetBasisType btype;
  
  PetscFunctionBegin;
  ierr = DMTetGetDof(dm,&bs0);CHKERRQ(ierr);
  ierr = PetscMalloc1(nf,&floc);CHKERRQ(ierr);
  for (f=0; f<nf; f++) {
    DM dm_f;
    
    ierr = VecGetBlockSize(field[f],&bs);CHKERRQ(ierr);
    
    ierr = DMTetCreateSharedSpace(dm,bs,&dm_f);CHKERRQ(ierr);
    ierr = DMSetUp(dm_f);CHKERRQ(ierr);
    
    ierr = DMCreateLocalVector(dm_f,&floc[f]);CHKERRQ(ierr);
    
    ierr = DMGlobalToLocalBegin(dm_f,field[f],INSERT_VALUES,floc[f]);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(dm_f,field[f],INSERT_VALUES,floc[f]);CHKERRQ(ierr);
    
    ierr = DMDestroy(&dm_f);CHKERRQ(ierr);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype == DMTET_CWARP_AND_BLEND) {
    ierr = _DMTWriteFieldsCG_AsciiVTU(dm,nf,floc,fname,filename);CHKERRQ(ierr);
  } else if (btype == DMTET_DG) {
    ierr = _DMTWriteFieldsDG_AsciiVTU(dm,nf,floc,fname,filename);CHKERRQ(ierr);
  } else {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render basis of type {DMTET_CWARP_AND_BLEND , DMTET_DG }");
  }
  
  for (f=0; f<nf; f++) {
    ierr = VecDestroy(&floc[f]);CHKERRQ(ierr);
  }
  ierr = PetscFree(floc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFields_VTU"
PetscErrorCode DMTetViewFields_VTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  const char *option;
  PetscMPIInt size,rank;
  char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN];
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm),&rank);CHKERRQ(ierr);
  ierr = PetscObjectGetOptionsPrefix((PetscObject)dm,&option);CHKERRQ(ierr);
  header[0] = '\0';
  str[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (option) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s_",option);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  if (size > 1) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet-r%.4d.vtu",(int)rank);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet.vtu");CHKERRQ(ierr);
  }
  ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(filename,str);CHKERRQ(ierr);

  ierr = _DMTetViewFields_VTU(dm,nf,field,fname,filename);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetViewFields_PVTU"
PetscErrorCode _DMTetViewFields_PVTU(DM dm,PetscInt nf,Vec field[],const char *fieldName[],
                                     const char pvtuFileName[],const char pathToSouce[],const char vtuPrefix[])
{
  PetscErrorCode ierr;
  PetscMPIInt commsize,commrank;
  PetscInt f,k,bs;
  FILE *fp = NULL;
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm),&commrank);CHKERRQ(ierr);
  
  ierr = DMTetGetDof(dm,&bs);CHKERRQ(ierr);
  
  if (commrank == 0) {
    char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN];
    
    fp = fopen(pvtuFileName,"w");
    if (fp == NULL) {
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Cannot open file %s",pvtuFileName);
    }
    
    PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
    PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
    PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
    PetscFPrintf(PETSC_COMM_SELF,fp,"<PUnstructuredGrid GhostLevel=\"0\"> \n");
    
    PetscFPrintf(PETSC_COMM_SELF,fp,"  <PCells> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"    <PDataArray type=\"Int32\" Name=\"connectivity\" NumberOfComponents=\"1\"/> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"    <PDataArray type=\"Int32\" Name=\"offsets\"      NumberOfComponents=\"1\"/> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"    <PDataArray type=\"UInt8\" Name=\"types\"        NumberOfComponents=\"1\"/> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"  </PCells> \n");
    
    PetscFPrintf(PETSC_COMM_SELF,fp,"  <PPoints> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"    <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"  </PPoints> \n");
    
    PetscFPrintf(PETSC_COMM_SELF,fp,"  <PCellData> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"  </PCellData> \n");
    
    PetscFPrintf(PETSC_COMM_SELF,fp,"  <PPointData> \n");
    for (f=0; f<nf; f++) {
      PetscInt dof;
      
      ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
      if (dof == 2) {
        PetscFPrintf(PETSC_COMM_SELF,fp,"    <PDataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"3\"/> \n",fieldName[f]);
      } else {
        PetscFPrintf(PETSC_COMM_SELF,fp,"    <PDataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"%D\"/> \n",fieldName[f],dof);
      }
    }
    PetscFPrintf(PETSC_COMM_SELF,fp,"  </PPointData> \n");
    
    for (k=0; k<commsize; k++) {
      char filename[PETSC_MAX_PATH_LEN];
      
      header[0] = '\0';
      if (pathToSouce) {
        ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s/",pathToSouce);CHKERRQ(ierr);
        ierr = PetscStrcat(header,str);CHKERRQ(ierr);
      }
      if (vtuPrefix) {
        ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",vtuPrefix);CHKERRQ(ierr);
        ierr = PetscStrcat(header,str);CHKERRQ(ierr);
      }
      
      if (commsize > 1) {
        ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet-r%.4d.vtu",(int)k);CHKERRQ(ierr);
      } else {
        ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet.vtu");CHKERRQ(ierr);
      }
      
      ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
      ierr = PetscStrcat(filename,str);CHKERRQ(ierr);
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"  <Piece Source=\"%s\"/> \n",filename);
    }
    PetscFPrintf(PETSC_COMM_SELF,fp,"</PUnstructuredGrid> \n");
    PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
    
    fclose(fp);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFields_PVTU"
PetscErrorCode DMTetViewFields_PVTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  PetscMPIInt commsize,commrank;
  const char *option;
  char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN],ptvu_filename[PETSC_MAX_PATH_LEN];

  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm),&commrank);CHKERRQ(ierr);

  ierr = PetscObjectGetOptionsPrefix((PetscObject)dm,&option);CHKERRQ(ierr);
  header[0] = '\0';
  str[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (option) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s_",option);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet.pvtu");CHKERRQ(ierr);
  
  ierr = PetscStrcpy(ptvu_filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(ptvu_filename,str);CHKERRQ(ierr);

  header[0] = '\0';
  str[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (option) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s_",option);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }

  ierr = _DMTetViewFields_PVTU(dm,nf,field,fname,ptvu_filename,NULL,header);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFields_PVTU2"
PetscErrorCode DMTetViewFields_PVTU2(DM dm,PetscInt nf,Vec field[],const char *fname[],const char path[],const char prefix[],const char relPathToSource[])
{
  PetscErrorCode ierr;
  PetscMPIInt commsize,commrank;
  char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN],ptvu_filename[PETSC_MAX_PATH_LEN];
  
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm),&commrank);CHKERRQ(ierr);
  
  header[0] = '\0';
  str[0] = '\0';
  if (path) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s/",path);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet.pvtu");CHKERRQ(ierr);
  
  ierr = PetscStrcpy(ptvu_filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(ptvu_filename,str);CHKERRQ(ierr);
  
  header[0] = '\0';
  str[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  ierr = _DMTetViewFields_PVTU(dm,nf,field,fname,ptvu_filename,relPathToSource,header);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTWriteFieldsCG_BinaryVTU"
PetscErrorCode _DMTWriteFieldsCG_BinaryVTU(DM dm,PetscInt nf,Vec field[],const char *fname[],const char filename[])
{
  /*DM_TET *tet = (DM_TET*)dm->data;*/
  PetscErrorCode ierr;
  FILE *fp;
  PetscInt f,i,j,e,dof;
  const PetscScalar *LA_field;
  PetscInt nelements,npe,nnodes,*element,basis_order;
  PetscReal *coords;
  DMTetBasisType btype;
  PetscInt nspilt,nfull,nsubel,nel_l;
  int bytes,byte_offset = 0;
  
  PetscFunctionBegin;
  fp = fopen(filename,"w");
  if (fp == NULL) {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_FILE_OPEN,"Cannot open file %s",filename);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only render continuous basis (DMTET_CWARP_AND_BLEND)");
  }
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements * nsubel;
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",nnodes,nel_l);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\"/> \n",byte_offset);
  byte_offset += sizeof(int);
  byte_offset += nelements * nsubel * 3 * sizeof(int);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\"/> \n",byte_offset);
  byte_offset += sizeof(int);
  byte_offset += nel_l * sizeof(int);

  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\"/> \n",byte_offset);
  byte_offset += sizeof(int);
  byte_offset += nel_l * sizeof(unsigned char);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\"/> \n",byte_offset);
  byte_offset += sizeof(int);
  byte_offset += nnodes * 3 * sizeof(double);

  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  
  for (f=0; f<nf; f++) {
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    if ((dof == 2) || (dof == 3)) {
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\"/> \n",fname[f],byte_offset);
      byte_offset += sizeof(int);
      byte_offset += nnodes * 3 * sizeof(double);
      
    } else {
      
      PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"%D\" format=\"appended\" offset=\"%d\"/> \n",fname[f],dof,byte_offset);
      byte_offset += sizeof(int);
      byte_offset += nnodes * dof * sizeof(double);
    }
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  
  /* write tag */
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <AppendedData encoding=\"raw\">\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"_");
  
  /* write connectivity --------------------------------------------------- */
  bytes = sizeof(int)*3*nelements*nsubel;
  fwrite(&bytes,sizeof(int),1,fp);
  
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &element[npe*e];
    PetscInt map[30][30],np,cnt;
    int flat[3];
    
    /* init map */
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        map[i][j] = 0;
      }
    }
    
    np = basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        
        if (i == np-j-2) {
          /* last triangle - located on side */
          //PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          flat[0] = map[i  ][j  ];
          flat[1] = map[i+1][j  ];
          flat[2] = map[i  ][j+1];
          fwrite(flat,sizeof(int),3,fp);
          
        } else {
          /* two triangles to define */
          //PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          flat[0] = map[i  ][j  ];
          flat[1] = map[i+1][j  ];
          flat[2] = map[i  ][j+1];
          fwrite(flat,sizeof(int),3,fp);
          //PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
          flat[0] = map[i+1][j  ];
          flat[1] = map[i+1][j+1];
          flat[2] = map[i  ][j+1];
          fwrite(flat,sizeof(int),3,fp);
        }
      }
    }
  }
  
  /* write offsets --------------------------------------------------- */
  bytes = sizeof(int)*nelements*nsubel;
  fwrite(&bytes,sizeof(int),1,fp);
  
  for (e=0; e<nelements*nsubel; e++) {
    int offset_e;
    
    offset_e = 3 * e + 3;
    fwrite(&offset_e,sizeof(int),1,fp);
  }
  
  /* write types --------------------------------------------------- */
  bytes = sizeof(unsigned char)*nelements*nsubel;
  fwrite(&bytes,sizeof(int),1,fp);
  
  for (e=0; e<nelements*nsubel; e++) {
    unsigned char type_e = 5; /* VTK_TRI */
    
    fwrite(&type_e,sizeof(unsigned char),1,fp);
  }
  
  /* write coords[3] --------------------------------------------------- */
  bytes = sizeof(double)*3*nnodes;
  fwrite(&bytes,sizeof(int),1,fp);

  for (i=0; i<nnodes; i++) {
    double pos[3];
    
    pos[0] = coords[2*i];
    pos[1] = coords[2*i+1];
    pos[2] = 0.0;
    fwrite(pos,sizeof(double),3,fp);
  }
  
  /* write field --------------------------------------------------- */
  for (f=0; f<nf; f++) {
    
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    
    ierr = VecGetArrayRead(field[f],&LA_field);CHKERRQ(ierr);
    if (dof == 2) {

      bytes = sizeof(double)*3*nnodes;
      fwrite(&bytes,sizeof(int),1,fp);

      for (i=0; i<nnodes; i++) {
        double u[3];

        u[0] = LA_field[2*i];    if (PetscAbsReal(u[0]) < 1.0e-12) { u[0] = 0.0; }
        u[1] = LA_field[2*i+1];  if (PetscAbsReal(u[1]) < 1.0e-12) { u[1] = 0.0; }
        u[2] = 0.0;
        //PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e 0.0 ",u1,u2);
        fwrite(u,sizeof(double),3,fp);
      }
      
      
    } else {
      PetscInt d;

      bytes = sizeof(double)*dof*nnodes;
      fwrite(&bytes,sizeof(int),1,fp);

      for (i=0; i<nnodes; i++) {
        for (d=0; d<dof; d++) {
          double scalar = LA_field[dof*i+d];
          if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
          fwrite(&scalar,sizeof(double),1,fp);
        }
      }
      
    }
    ierr = VecRestoreArrayRead(field[f],&LA_field);CHKERRQ(ierr);
  }
  
  /* close tags and vtk format */
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </AppendedData>\n");
  
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFields_VTU_b"
PetscErrorCode DMTetViewFields_VTU_b(DM dm,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  const char *option;
  PetscMPIInt size,rank;
  Vec *floc;
  char str[PETSC_MAX_PATH_LEN],header[PETSC_MAX_PATH_LEN];
  PetscInt f,bs0,bs;
  DMTetBasisType btype;
  
  PetscFunctionBegin;
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&size);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm),&rank);CHKERRQ(ierr);
  ierr = PetscObjectGetOptionsPrefix((PetscObject)dm,&option);CHKERRQ(ierr);
  header[0] = '\0';
  str[0] = '\0';
  if (prefix) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  if (option) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"%s_",option);CHKERRQ(ierr);
    ierr = PetscStrcat(header,str);CHKERRQ(ierr);
  }
  
  if (size > 1) {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet-r%.4d.vtu",(int)rank);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(str,PETSC_MAX_PATH_LEN-1,"dmtet.vtu");CHKERRQ(ierr);
  }
  ierr = PetscStrcpy(filename,header);CHKERRQ(ierr);
  ierr = PetscStrcat(filename,str);CHKERRQ(ierr);
  
  ierr = DMTetGetDof(dm,&bs0);CHKERRQ(ierr);
  ierr = PetscMalloc1(nf,&floc);CHKERRQ(ierr);
  for (f=0; f<nf; f++) {
    DM dm_f;
    
    ierr = VecGetBlockSize(field[f],&bs);CHKERRQ(ierr);
    
    ierr = DMTetCreateSharedSpace(dm,bs,&dm_f);CHKERRQ(ierr);
    ierr = DMSetUp(dm_f);CHKERRQ(ierr);
    
    ierr = DMCreateLocalVector(dm_f,&floc[f]);CHKERRQ(ierr);
    
    ierr = DMGlobalToLocalBegin(dm_f,field[f],INSERT_VALUES,floc[f]);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(dm_f,field[f],INSERT_VALUES,floc[f]);CHKERRQ(ierr);
    
    ierr = DMDestroy(&dm_f);CHKERRQ(ierr);
  }
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype == DMTET_CWARP_AND_BLEND) {
    ierr = _DMTWriteFieldsCG_BinaryVTU(dm,nf,floc,fname,filename);CHKERRQ(ierr);
  } else if (btype == DMTET_DG) {
    //ierr = _DMTWriteFieldsDG_BinaryVTU(dm,nf,floc,fname,filename);CHKERRQ(ierr);
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only emit binary PV data for spaces of type { DMTET_CWARP_AND_BLEND }. DG not supported <todo>");
  } else {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Can only emit VP data for spaces of type { DMTET_CWARP_AND_BLEND , DMTET_DG }");
  }
  
  for (f=0; f<nf; f++) {
    ierr = VecDestroy(&floc[f]);CHKERRQ(ierr);
  }
  ierr = PetscFree(floc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsPV"
PetscErrorCode DMTetViewFieldsPV(DM dm,PetscInt nf,Vec field[],PetscBool binary,const char *fname[],const char prefix[])
{
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  if (binary) {
    ierr = DMTetViewFields_PVTU(dm,nf,field,fname,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU_b(dm,nf,field,fname,prefix);CHKERRQ(ierr);
  } else {
    ierr = DMTetViewFields_PVTU(dm,nf,field,fname,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm,nf,field,fname,prefix);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMF_SEQ"
PetscErrorCode DMTetViewFieldsXDMF_SEQ(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  FILE *fp = NULL;
  PetscInt nelements,npe,nnodes,*element,basis_order,e,i,j;
  PetscReal *coords;
  PetscInt nspilt,nfull,nsubel,nel_l;
  PetscInt f,dof;
  const PetscScalar *LA_field;
  
  
  if (prefix != NULL) {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"%sdmtet.xmf",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"dmtet.xmf",prefix);CHKERRQ(ierr);
  }
  
  ierr = PetscFOpen(PetscObjectComm((PetscObject)dm),filename,"w",&fp);CHKERRQ(ierr);

  fprintf(fp,"<?xml version=\"1.0\"?>\n");
  fprintf(fp,"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  fprintf(fp,"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
  fprintf(fp,"<Domain>\n");
  fprintf(fp,"  <Grid Name=\"DMTet\" GridType=\"Uniform\">\n");
  fprintf(fp,"  <Time Value=\"%1.8e\"/>\n",time);

  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements * nsubel;

  /* write Topology */
  fprintf(fp,"  <Topology TopologyType=\"Triangle\" NumberOfElements=\"%d\">\n",(int)nel_l);
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Format=\"XML\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
  /////
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &element[npe*e];
    PetscInt map[30][30],np,cnt;
    
    /* init map */
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        map[i][j] = 0;
      }
    }
    
    np = basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        if (i == np-j-2) {
          /* last triangle - located on side */
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
        } else {
          /* two triangles to define */
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
        }
      }
    }
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
  /////
  fprintf(fp,"  </Topology>\n");
  
  /* write Geometry */
  fprintf(fp,"  <Geometry GeometryType=\"XY\">\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"Coordinates\" Format=\"XML\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",nnodes,2);
  for (i=0; i<nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e %+1.8e\n",coords[2*i],coords[2*i+1]);
  }
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
  /////
  /////
  fprintf(fp,"  </Geometry>\n");
  
  /* write Attributes <node> */
  for (f=0; f<nf; f++) {
    PetscBool is_std_vector = PETSC_FALSE,is_scalar = PETSC_FALSE,is_other = PETSC_FALSE;
    
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    ierr = VecGetArrayRead(field[f],&LA_field);CHKERRQ(ierr);

    if (dof == 1) {
      is_scalar = PETSC_TRUE;
    } else if (dof == 2) {
      is_std_vector = PETSC_TRUE;
    } else {
      is_other = PETSC_TRUE;
    }
    
    if (is_std_vector) {
      /* paraview requires vectors in 2D be written out with 3 components */
      fprintf(fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Vector\">\n",fname[f]);
      PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 3\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],nnodes);
      
      for (i=0; i<nnodes; i++) {
        PetscReal scalar[] = { 0.0, 0.0, 0.0 };
        PetscInt d;
        
        for (d=0; d<dof; d++) {
          scalar[d] = LA_field[dof*i+d];
          if (PetscAbsReal(scalar[d]) < 1.0e-12) { scalar[d] = 0.0; }
        }
        PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e %+1.8e %+1.8e\n",scalar[0],scalar[1],scalar[2]);
      }
      PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
      fprintf(fp,"  </Attribute>\n");
      
    } else if (is_scalar) {
      fprintf(fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Scalar\">\n",fname[f]);
      PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],nnodes);
     
      for (i=0; i<nnodes; i++) {
        PetscReal scalar;
        
        scalar = LA_field[i];
        if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
        PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e\n",scalar);
      }
      PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
      fprintf(fp,"  </Attribute>\n");

    } else if (is_other) {
      PetscInt d;
      
      for (d=0; d<dof; d++) {
        PetscFPrintf(PETSC_COMM_SELF,fp,"  <Attribute Center=\"Node\" Name=\"%s_%D\" AttributeType=\"Scalar\">\n",fname[f],d);
        PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],nnodes);

        for (i=0; i<nnodes; i++) {
          PetscReal scalar;
          
          scalar = LA_field[dof*i+d];
          if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e\n",scalar);
        }
        PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
        fprintf(fp,"  </Attribute>\n");
      }
    }
    
    ierr = VecRestoreArrayRead(field[f],&LA_field);CHKERRQ(ierr);
  }

  fprintf(fp,"  </Grid>\n");
  fprintf(fp,"</Domain>\n");
  fprintf(fp,"</Xdmf>\n");

  ierr = PetscFClose(PetscObjectComm((PetscObject)dm),fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* 
 Note: This method currently uses non-scalable IO in the sense that the entire
 data is gathered on rank 0 and then written out.
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMF_MPI_non_scalable"
PetscErrorCode DMTetViewFieldsXDMF_MPI_non_scalable(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  FILE *fp = NULL;
  PetscInt nelements,nelements_g,npe,nnodes,*element,*element_gidx,*element_gidx_root,basis_order,e,i,j;
  PetscReal *coords;
  PetscInt nspilt,nfull,nsubel,nel_l;
  PetscInt f,dof;
  const PetscScalar *LA_field;
  MPI_Comm comm;
  PetscMPIInt commrank,commsize;
  
  if (prefix != NULL) {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"%sdmtet.xmf",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"dmtet.xmf",prefix);CHKERRQ(ierr);
  }
  comm = PetscObjectComm((PetscObject)dm);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);

  ierr = PetscFOpen(comm,filename,"w",&fp);CHKERRQ(ierr);
  
  PetscFPrintf(comm,fp,"<?xml version=\"1.0\"?>\n");
  PetscFPrintf(comm,fp,"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  PetscFPrintf(comm,fp,"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
  PetscFPrintf(comm,fp,"<Domain>\n");
  PetscFPrintf(comm,fp,"  <Grid Name=\"DMTet\" GridType=\"Uniform\">\n");
  PetscFPrintf(comm,fp,"  <Time Value=\"%1.8e\"/>\n",time);
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&nelements,&nelements_g,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements_g * nsubel;
  
  /* write Topology */
  PetscFPrintf(comm,fp,"  <Topology TopologyType=\"Triangle\" NumberOfElements=\"%d\">\n",(int)nel_l);
  PetscFPrintf(comm,fp,"    <DataItem Format=\"XML\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
  /////
  
  /* copy and convert local indices into global indices */
  DM                     dm_scalar;
  ISLocalToGlobalMapping ltog;
  const PetscInt         *g_idx;
  PetscInt *rcnt = NULL,*displ = NULL,sendL;

  ierr = DMTetCreateSharedSpace(dm,1,&dm_scalar);CHKERRQ(ierr);

  ierr = PetscMalloc1(nelements*npe,&element_gidx);CHKERRQ(ierr);
  ierr = DMGetLocalToGlobalMapping(dm_scalar,&ltog);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(ltog,&g_idx);CHKERRQ(ierr);
  for (i=0; i<nelements * npe; i++) {
    PetscInt l_idx,l2g_idx;
    
    l_idx = element[i];
    l2g_idx = g_idx[ l_idx ];
    element_gidx[i] = l2g_idx;
  }
  ierr = ISLocalToGlobalMappingRestoreIndices(ltog,&g_idx);CHKERRQ(ierr);
  
  
  element_gidx_root = NULL;
  if (commrank == 0) {
    ierr = PetscMalloc1(nelements_g*npe,&element_gidx_root);CHKERRQ(ierr);
    ierr = PetscMalloc1(commsize,&rcnt);CHKERRQ(ierr);
    ierr = PetscMalloc1(commsize,&displ);CHKERRQ(ierr);
    
  }

  sendL = nelements * npe;
  ierr = MPI_Gather(&sendL, 1, MPIU_INT, rcnt, 1, MPIU_INT, 0, comm);CHKERRQ(ierr);
  if (commrank == 0) {
    PetscInt cnt = 0;
    
    displ[0] = cnt;
    for (j=1; j<commsize; j++) {
      cnt = cnt + rcnt[j-1];
      displ[j] = cnt;
    }
  }
  
  ierr = MPI_Gatherv(element_gidx, nelements * npe, MPIU_INT, element_gidx_root, rcnt, displ, MPIU_INT, 0, comm);CHKERRQ(ierr);
  
  /* write out the gathered (local) data on rank 0 */
  if (commrank == 0) {
    for (e=0; e<nelements_g; e++) {
      PetscInt *idx = &element_gidx_root[npe*e];
      PetscInt map[30][30],np,cnt;
      
      /* init map */
      for (i=0; i<30; i++) {
        for (j=0; j<30; j++) {
          map[i][j] = 0;
        }
      }
      
      np = basis_order + 1;
      cnt = 0;
      for (j=0; j<np; j++) {
        for (i=0; i<np-j; i++) {
          map[i][j] = idx[cnt];
          cnt++;
        }
      }
      
      /* triangle sweep */
      for (j=0; j<np-1; j++) {
        for (i=0; i<np-j-1; i++) {
          if (i == np-j-2) {
            /* last triangle - located on side */
            PetscFPrintf(comm,fp,"    %D %D %D\n",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
          } else {
            /* two triangles to define */
            PetscFPrintf(comm,fp,"    %D %D %D\n",map[i  ][j  ],map[i+1][j  ],map[i  ][j+1]);
            PetscFPrintf(comm,fp,"    %D %D %D\n",map[i+1][j  ],map[i+1][j+1],map[i  ][j+1]);
          }
        }
      }
    }
  }
  
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  /////
  PetscFPrintf(comm,fp,"  </Topology>\n");
  
  ierr = PetscFree(element_gidx);CHKERRQ(ierr);
  if (commrank == 0) {
    ierr = PetscFree(element_gidx_root);CHKERRQ(ierr);
    ierr = PetscFree(rcnt);CHKERRQ(ierr);
    ierr = PetscFree(displ);CHKERRQ(ierr);
  }
  ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
  
  /* write Geometry */
  Vec coords_global,coords_global_root = NULL;
  PetscInt coords_len;
  VecScatter ctx;
  
  ierr = DMTetCreateGlobalCoordinateVector(dm,&coords_global);CHKERRQ(ierr);
  VecScatterCreateToZero(coords_global,&ctx,&coords_global_root);
  VecGetSize(coords_global,&coords_len);
  coords_len = coords_len / 2;
  // scatter as many times as you need
  VecScatterBegin(ctx,coords_global,coords_global_root,INSERT_VALUES,SCATTER_FORWARD);
  VecScatterEnd(ctx,coords_global,coords_global_root,INSERT_VALUES,SCATTER_FORWARD);
  // destroy scatter context and local vector when no longer needed
  VecScatterDestroy(&ctx);
  
  if (commrank == 0) {
    
    ierr = VecGetArrayRead(coords_global_root,&LA_field);CHKERRQ(ierr);

    PetscFPrintf(comm,fp,"  <Geometry GeometryType=\"XY\">\n");
    PetscFPrintf(comm,fp,"    <DataItem Name=\"Coordinates\" Format=\"XML\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",coords_len,2);
    for (i=0; i<coords_len; i++) {
      PetscFPrintf(comm,fp,"    %+1.8e %+1.8e\n",LA_field[2*i],LA_field[2*i+1]);
    }
    PetscFPrintf(comm,fp,"    </DataItem>\n");
    /////
    /////
    PetscFPrintf(comm,fp,"  </Geometry>\n");
    
    ierr = VecRestoreArrayRead(coords_global_root,&LA_field);CHKERRQ(ierr);
  }
  
  VecDestroy(&coords_global_root);
  
  /* write Attributes <node> per input vector */
  for (f=0; f<nf; f++) {
    VecScatter ctx;
    PetscInt len;
    Vec floc_f = NULL;
    PetscBool is_std_vector = PETSC_FALSE,is_scalar = PETSC_FALSE,is_other = PETSC_FALSE;

    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    
    if (dof == 1) {
      is_scalar = PETSC_TRUE;
    } else if (dof == 2) {
      is_std_vector = PETSC_TRUE;
    } else {
      is_other = PETSC_TRUE;
    }

    /* gather */
    VecScatterCreateToZero(field[f],&ctx,&floc_f);
    VecGetSize(field[f],&len);
    len = len / dof;
    // scatter as many times as you need
    VecScatterBegin(ctx,field[f],floc_f,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(ctx,field[f],floc_f,INSERT_VALUES,SCATTER_FORWARD);
    // destroy scatter context and local vector when no longer needed
    VecScatterDestroy(&ctx);
    
    /* get values */
    if (commrank == 0) {
      ierr = VecGetArrayRead(floc_f,&LA_field);CHKERRQ(ierr);
      /* write Attributes <node> */
      
      if (is_std_vector) {
        /* paraview requires vectors in 2D be written out with 3 components */
        PetscFPrintf(PETSC_COMM_SELF,fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Vector\">\n",fname[f]);
        PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 3\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len);
        
        for (i=0; i<len; i++) {
          PetscReal scalar[] = { 0.0, 0.0, 0.0 };
          PetscInt d;
          
          for (d=0; d<dof; d++) {
            scalar[d] = LA_field[dof*i+d];
            if (PetscAbsReal(scalar[d]) < 1.0e-12) { scalar[d] = 0.0; }
          }
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e %+1.8e %+1.8e\n",scalar[0],scalar[1],scalar[2]);
        }
        PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
        PetscFPrintf(PETSC_COMM_SELF,fp,"  </Attribute>\n");
        
      } else if (is_scalar) {
        PetscFPrintf(PETSC_COMM_SELF,fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Scalar\">\n",fname[f]);
        PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len);
        
        for (i=0; i<len; i++) {
          PetscReal scalar;
          
          scalar = LA_field[i];
          if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e\n",scalar);
        }
        PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
        PetscFPrintf(PETSC_COMM_SELF,fp,"  </Attribute>\n");
        
      } else if (is_other) {
        PetscInt d;
        
        for (d=0; d<dof; d++) {
          PetscFPrintf(PETSC_COMM_SELF,fp,"  <Attribute Center=\"Node\" Name=\"%s_%D\" AttributeType=\"Scalar\">\n",fname[f],d);
          PetscFPrintf(PETSC_COMM_SELF,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len);
          
          for (i=0; i<len; i++) {
            PetscReal scalar;
            
            scalar = LA_field[dof*i+d];
            if (PetscAbsReal(scalar) < 1.0e-12) { scalar = 0.0; }
            PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e\n",scalar);
          }
          PetscFPrintf(PETSC_COMM_SELF,fp,"    </DataItem>\n");
          PetscFPrintf(PETSC_COMM_SELF,fp,"  </Attribute>\n");
        }
      }
      
      
      ierr = VecRestoreArrayRead(floc_f,&LA_field);CHKERRQ(ierr);
    }
    ierr = VecDestroy(&floc_f);CHKERRQ(ierr);
  }

  PetscFPrintf(comm,fp,"  </Grid>\n");
  PetscFPrintf(comm,fp,"</Domain>\n");
  PetscFPrintf(comm,fp,"</Xdmf>\n");
  
  ierr = PetscFClose(comm,fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

static PetscErrorCode user_writer_ascii_vec2(FILE *fp,PetscReal data[],PetscInt N,void *ctx)
{
  PetscInt k;
  for (k=0; k<N/2; k++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e %+1.8e\n",data[2*k],data[2*k+1]);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode user_writer_binary_vec2(FILE *fp,PetscReal data[],PetscInt N,void *ctx)
{
  PetscInt k;
  for (k=0; k<N/2; k++) {
    fwrite(&data[2*k],sizeof(PetscReal),2,fp);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode user_writer_ascii_vec3(FILE *fp,PetscReal data[],PetscInt N,void *ctx)
{
  PetscInt k;
  for (k=0; k<N/3; k++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"    %+1.8e %+1.8e %+1.8e\n",data[3*k],data[3*k+1],data[3*k+2]);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode user_writer_binary_vec3(FILE *fp,PetscReal data[],PetscInt N,void *ctx)
{
  PetscInt k;
  for (k=0; k<N/3; k++) {
    fwrite(&data[3*k],sizeof(PetscReal),3,fp);
  }
  PetscFunctionReturn(0);
}

typedef struct {
  PetscInt map[30][30];
  PetscInt nbasis_per_element,basis_order;
} CellWriterCtx;

static PetscErrorCode user_writer_ascii_cells(FILE *fp,PetscInt data[],PetscInt N,void *ctx)
{
  CellWriterCtx *helper = (CellWriterCtx*)ctx;
  PetscInt nelements,e,npe,i,j;
  
  nelements = N / helper->nbasis_per_element;
  npe = helper->nbasis_per_element;

  for (e=0; e<nelements; e++) {
    PetscInt *idx = &data[npe*e];
    PetscInt np,cnt;
    
    np = helper->basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        helper->map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        if (i == np-j-2) {
          /* last triangle - located on side */
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",helper->map[i  ][j  ],helper->map[i+1][j  ],helper->map[i  ][j+1]);
        } else {
          /* two triangles to define */
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",helper->map[i  ][j  ],helper->map[i+1][j  ],helper->map[i  ][j+1]);
          PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",helper->map[i+1][j  ],helper->map[i+1][j+1],helper->map[i  ][j+1]);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode user_writer_binary_cells(FILE *fp,PetscInt data[],PetscInt N,void *ctx)
{
  CellWriterCtx *helper = (CellWriterCtx*)ctx;
  PetscInt nelements,e,npe,i,j;
  int subcellid[3];
  
  nelements = N / helper->nbasis_per_element;
  npe = helper->nbasis_per_element;
  
  for (e=0; e<nelements; e++) {
    PetscInt *idx = &data[npe*e];
    PetscInt np,cnt;
    
    np = helper->basis_order + 1;
    cnt = 0;
    for (j=0; j<np; j++) {
      for (i=0; i<np-j; i++) {
        helper->map[i][j] = idx[cnt];
        cnt++;
      }
    }
    
    /* triangle sweep */
    for (j=0; j<np-1; j++) {
      for (i=0; i<np-j-1; i++) {
        if (i == np-j-2) {
          /* last triangle - located on side */
          //PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",helper->map[i  ][j  ],helper->map[i+1][j  ],helper->map[i  ][j+1]);
          subcellid[0] = (int)helper->map[i  ][j  ];
          subcellid[1] = (int)helper->map[i+1][j  ];
          subcellid[2] = (int)helper->map[i  ][j+1];
          fwrite(subcellid,sizeof(int),3,fp);

        } else {
          /* two triangles to define */
          //PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",helper->map[i  ][j  ],helper->map[i+1][j  ],helper->map[i  ][j+1]);
          subcellid[0] = (int)helper->map[i  ][j  ];
          subcellid[1] = (int)helper->map[i+1][j  ];
          subcellid[2] = (int)helper->map[i  ][j+1];
          fwrite(subcellid,sizeof(int),3,fp);

          //PetscFPrintf(PETSC_COMM_SELF,fp,"    %D %D %D\n",helper->map[i+1][j  ],helper->map[i+1][j+1],helper->map[i  ][j+1]);
          subcellid[0] = (int)helper->map[i+1][j  ];
          subcellid[1] = (int)helper->map[i+1][j+1];
          subcellid[2] = (int)helper->map[i  ][j+1];
          fwrite(subcellid,sizeof(int),3,fp);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMF_MPI_mem_scalable"
PetscErrorCode DMTetViewFieldsXDMF_MPI_mem_scalable(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  FILE *fp = NULL;
  PetscInt nelements,nelements_g,npe,nnodes,*element,*element_gidx,basis_order,i,j;
  PetscReal *coords;
  PetscInt nspilt,nfull,nsubel,nel_l;
  PetscInt f,dof;
  const PetscScalar *LA_field;
  MPI_Comm comm;
  PetscMPIInt commrank,commsize;
  Vec coords_global;
  PetscInt coords_len;
  CellWriterCtx *cell_context;
  
  if (prefix != NULL) {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"%sdmtet.xmf",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"dmtet.xmf",prefix);CHKERRQ(ierr);
  }
  comm = PetscObjectComm((PetscObject)dm);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  ierr = PetscFOpen(comm,filename,"w",&fp);CHKERRQ(ierr);
  
  PetscFPrintf(comm,fp,"<?xml version=\"1.0\"?>\n");
  PetscFPrintf(comm,fp,"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  PetscFPrintf(comm,fp,"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
  PetscFPrintf(comm,fp,"<Domain>\n");
  PetscFPrintf(comm,fp,"  <Grid Name=\"DMTet\" GridType=\"Uniform\">\n");
  PetscFPrintf(comm,fp,"  <Time Value=\"%1.8e\"/>\n",time);
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&nelements,&nelements_g,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements_g * nsubel;
  
  /* write Topology */
  PetscFPrintf(comm,fp,"  <Topology TopologyType=\"Triangle\" NumberOfElements=\"%d\">\n",(int)nel_l);
  PetscFPrintf(comm,fp,"    <DataItem Format=\"XML\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
  /////
  
  /* copy and convert local indices into global indices */
  DM                     dm_scalar;
  ISLocalToGlobalMapping ltog;
  const PetscInt         *g_idx;
  
  ierr = DMTetCreateSharedSpace(dm,1,&dm_scalar);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(nelements*npe,&element_gidx);CHKERRQ(ierr);
  ierr = DMGetLocalToGlobalMapping(dm_scalar,&ltog);CHKERRQ(ierr);
  ierr = ISLocalToGlobalMappingGetIndices(ltog,&g_idx);CHKERRQ(ierr);
  for (i=0; i<nelements * npe; i++) {
    PetscInt l_idx,l2g_idx;
    
    l_idx = element[i];
    l2g_idx = g_idx[ l_idx ];
    element_gidx[i] = l2g_idx;
  }
  ierr = ISLocalToGlobalMappingRestoreIndices(ltog,&g_idx);CHKERRQ(ierr);
  
  
  ierr = PetscMalloc1(1,&cell_context);CHKERRQ(ierr);
  /* init map */
  cell_context->nbasis_per_element = npe;
  cell_context->basis_order = basis_order;
  for (i=0; i<30; i++) {
    for (j=0; j<30; j++) {
      cell_context->map[i][j] = 0;
    }
  }

  ierr = MPIWriteUser_Blocking_PetscInt(fp,element_gidx,nelements * npe,0,PETSC_TRUE,PETSC_FALSE,comm,user_writer_ascii_cells,(void*)cell_context);CHKERRQ(ierr);
  
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  /////
  PetscFPrintf(comm,fp,"  </Topology>\n");
  
  ierr = PetscFree(cell_context);CHKERRQ(ierr);
  ierr = PetscFree(element_gidx);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
  
  /* write Geometry */
  ierr = DMTetCreateGlobalCoordinateVector(dm,&coords_global);CHKERRQ(ierr);
  ierr = VecGetSize(coords_global,&coords_len);CHKERRQ(ierr);
  coords_len = coords_len / 2;
  
  /////
  PetscFPrintf(comm,fp,"  <Geometry GeometryType=\"XY\">\n");
  PetscFPrintf(comm,fp,"    <DataItem Name=\"Coordinates\" Format=\"XML\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",coords_len,2);
  
  ierr = VecGetLocalSize(coords_global,&coords_len);CHKERRQ(ierr);
  coords_len = coords_len / 2;
  
  ierr = VecGetArrayRead(coords_global,&LA_field);CHKERRQ(ierr);
  
  ierr = MPIWriteUser_Blocking_PetscReal(fp,(PetscReal*)LA_field,2*coords_len,0,PETSC_TRUE,PETSC_FALSE,comm,user_writer_ascii_vec2,NULL);CHKERRQ(ierr);
  
  ierr = VecRestoreArrayRead(coords_global,&LA_field);CHKERRQ(ierr);
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  PetscFPrintf(comm,fp,"  </Geometry>\n");
  /////
  
  ierr = VecDestroy(&coords_global);CHKERRQ(ierr);
  
  
  /* write Attributes <node> per input vector */
  for (f=0; f<nf; f++) {
    PetscInt len,len_local;
    PetscBool is_std_vector = PETSC_FALSE,is_scalar = PETSC_FALSE,is_other = PETSC_FALSE;
    
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    
    if (dof == 1) {
      is_scalar = PETSC_TRUE;
    } else if (dof == 2) {
      is_std_vector = PETSC_TRUE;
    } else {
      is_other = PETSC_TRUE;
    }

    ierr = VecGetSize(field[f],&len);CHKERRQ(ierr);
    ierr = VecGetLocalSize(field[f],&len_local);CHKERRQ(ierr);
    /* get values */
    ierr = VecGetArrayRead(field[f],&LA_field);CHKERRQ(ierr);
    
    /* write Attributes <node> */
    
    if (is_std_vector) {
      PetscReal *buffer;
      PetscInt k;
      
      /* paraview requires vectors in 2D be written out with 3 components */
      PetscFPrintf(comm,fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Vector\">\n",fname[f]);
      PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len/2,3);
      
      /* How should this work with the stupid padding? - only option is to copy and pad - lame lame ParaView */
      ierr = PetscMalloc1(3*(len_local/2),&buffer);CHKERRQ(ierr);
      
      for (k=0; k<len_local/2; k++) {
        buffer[3*k]   = LA_field[2*k  ];
        buffer[3*k+1] = LA_field[2*k+1];
        buffer[3*k+2] = 0.0;
      }

      ierr = MPIWriteUser_Blocking_PetscReal(fp,buffer,3*(len_local/2),0,PETSC_TRUE,PETSC_FALSE,comm,user_writer_ascii_vec3,NULL);CHKERRQ(ierr);
      
      ierr = PetscFree(buffer);CHKERRQ(ierr);
      
      PetscFPrintf(comm,fp,"    </DataItem>\n");
      PetscFPrintf(comm,fp,"  </Attribute>\n");
      
    } else if (is_scalar) {
      PetscFPrintf(comm,fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Scalar\">\n",fname[f]);
      PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len);
      
      ierr = MPIWrite_Blocking_PetscReal(fp,(PetscReal*)LA_field,len_local,0,PETSC_TRUE,PETSC_FALSE,comm);CHKERRQ(ierr);
      
      PetscFPrintf(comm,fp,"    </DataItem>\n");
      PetscFPrintf(comm,fp,"  </Attribute>\n");
      
    } else if (is_other) {
      PetscInt d,k;
      PetscReal *buffer;
      
      ierr = PetscMalloc1(len_local/dof,&buffer);CHKERRQ(ierr);

      for (d=0; d<dof; d++) {
        PetscFPrintf(comm,fp,"  <Attribute Center=\"Node\" Name=\"%s_%D\" AttributeType=\"Scalar\">\n",fname[f],d);
        PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"XML\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len/dof);
        
        for (k=0; k<len_local/dof; k++) {
          buffer[k]   = LA_field[dof*k + d];
        }
        ierr = MPIWrite_Blocking_PetscReal(fp,buffer,len_local/dof,0,PETSC_TRUE,PETSC_FALSE,comm);CHKERRQ(ierr);

        PetscFPrintf(comm,fp,"    </DataItem>\n");
        PetscFPrintf(comm,fp,"  </Attribute>\n");
      }
      ierr = PetscFree(buffer);CHKERRQ(ierr);
    }
    
    ierr = VecRestoreArrayRead(field[f],&LA_field);CHKERRQ(ierr);

  }
  
  PetscFPrintf(comm,fp,"  </Grid>\n");
  PetscFPrintf(comm,fp,"</Domain>\n");
  PetscFPrintf(comm,fp,"</Xdmf>\n");
  
  ierr = PetscFClose(comm,fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMF_MPI"
PetscErrorCode DMTetViewFieldsXDMF_MPI(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  PetscErrorCode ierr;
  //ierr = DMTetViewFieldsXDMF_MPI_non_scalable(dm,time,nf,field,fname,prefix);CHKERRQ(ierr);
  ierr = DMTetViewFieldsXDMF_MPI_mem_scalable(dm,time,nf,field,fname,prefix);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* Ascii writer only */
#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMF"
PetscErrorCode DMTetViewFieldsXDMF(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char prefix[])
{
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  DMTetBasisType btype;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"XDMF reader Can only render continuous basis (DMTET_CWARP_AND_BLEND)");
  }

  if (commsize == 1) {
    ierr = DMTetViewFieldsXDMF_SEQ(dm,time,nf,field,fname,prefix);CHKERRQ(ierr);
  } else {
    ierr = DMTetViewFieldsXDMF_MPI(dm,time,nf,field,fname,prefix);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMF_b_MPI_mem_scalable"
PetscErrorCode DMTetViewFieldsXDMF_b_MPI_mem_scalable(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],const char path[],const char prefix[],
                                                      PetscBool use_common_mesh_file)
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  FILE *fp = NULL;
  PetscInt nelements,nelements_g,npe,nnodes,*element,basis_order,i,j;
  PetscReal *coords;
  PetscInt nspilt,nfull,nsubel,nel_l;
  PetscInt f,dof;
  const PetscScalar *LA_field;
  MPI_Comm comm;
  PetscMPIInt commrank,commsize;
  Vec coords_global;
  PetscInt coords_len;
  CellWriterCtx *cell_context;
  FILE *fp_heavydata = NULL;
  char fname_heavydata[PETSC_MAX_PATH_LEN];
  char path_to_file[PETSC_MAX_PATH_LEN];
  
  if (prefix != NULL) {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"%sdmtet.xmf",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"dmtet.xmf",prefix);CHKERRQ(ierr);
  }
  comm = PetscObjectComm((PetscObject)dm);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,filename);CHKERRQ(ierr);
  } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",filename);CHKERRQ(ierr); }
  ierr = PetscFOpen(comm,path_to_file,"w",&fp);CHKERRQ(ierr);
  
  PetscFPrintf(comm,fp,"<?xml version=\"1.0\"?>\n");
  PetscFPrintf(comm,fp,"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  PetscFPrintf(comm,fp,"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
  PetscFPrintf(comm,fp,"<Domain>\n");
  PetscFPrintf(comm,fp,"  <Grid Name=\"DMTet\" GridType=\"Uniform\">\n");
  PetscFPrintf(comm,fp,"  <Time Value=\"%1.8e\"/>\n",time);
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&nelements,&nelements_g,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements_g * nsubel;
  
  /* write Topology */
  if (use_common_mesh_file) {
    ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"common/DMTetXDMFTopology.bin");CHKERRQ(ierr);
  } else {
    if (prefix != NULL) {
      ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sDMTetXDMFTopology.bin",prefix);CHKERRQ(ierr);
    } else {
      ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"DMTetXDMFTopology.bin");CHKERRQ(ierr);
    }
  }
  
  PetscFPrintf(comm,fp,"  <Topology TopologyType=\"Triangle\" NumberOfElements=\"%d\">\n",(int)nel_l);
  
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(comm,fp,"    <DataItem Format=\"Binary\" Endian=\"Big\" Seek=\"0\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
#else
  PetscFPrintf(comm,fp,"    <DataItem Format=\"Binary\" Endian=\"Little\" Seek=\"0\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
#endif
  
  if (!use_common_mesh_file) {
    DM                     dm_scalar;
    ISLocalToGlobalMapping ltog;
    const PetscInt         *g_idx;
    PetscInt               *element_gidx;
    
    if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
    } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }

    ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);

    ierr = DMTetCreateSharedSpace(dm,1,&dm_scalar);CHKERRQ(ierr);

    /* copy and convert local indices into global indices */
    ierr = PetscMalloc1(nelements*npe,&element_gidx);CHKERRQ(ierr);
    ierr = DMGetLocalToGlobalMapping(dm_scalar,&ltog);CHKERRQ(ierr);
    ierr = ISLocalToGlobalMappingGetIndices(ltog,&g_idx);CHKERRQ(ierr);
    for (i=0; i<nelements * npe; i++) {
      PetscInt l_idx,l2g_idx;
      
      l_idx = element[i];
      l2g_idx = g_idx[ l_idx ];
      element_gidx[i] = l2g_idx;
    }
    ierr = ISLocalToGlobalMappingRestoreIndices(ltog,&g_idx);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(1,&cell_context);CHKERRQ(ierr);
    /* init map */
    cell_context->nbasis_per_element = npe;
    cell_context->basis_order = basis_order;
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        cell_context->map[i][j] = 0;
      }
    }
  
    ierr = MPIWriteUser_Blocking_PetscInt(fp_heavydata,element_gidx,nelements * npe,0,PETSC_TRUE,PETSC_TRUE,comm,user_writer_binary_cells,(void*)cell_context);CHKERRQ(ierr);
    
    ierr = PetscFree(cell_context);CHKERRQ(ierr);
    ierr = PetscFree(element_gidx);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
    ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
    fp_heavydata = NULL;
  }
  
  PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
  
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  /////
  PetscFPrintf(comm,fp,"  </Topology>\n");
  
  
  
  /* write Geometry */
  if (use_common_mesh_file) {
    ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"common/DMTetXDMFGeometry.bin");CHKERRQ(ierr);
  } else {
    if (prefix != NULL) {
      ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sDMTetXDMFGeometry.bin",prefix);CHKERRQ(ierr);
    } else {
      ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"DMTetXDMFGeometry.bin");CHKERRQ(ierr);
    }
  }
  
  ierr = DMTetCreateGlobalCoordinateVector(dm,&coords_global);CHKERRQ(ierr);
  ierr = VecGetSize(coords_global,&coords_len);CHKERRQ(ierr);
  coords_len = coords_len / 2;
  
  /////
  PetscFPrintf(comm,fp,"  <Geometry GeometryType=\"XY\">\n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(comm,fp,"    <DataItem Name=\"Coordinates\" Format=\"Binary\" Endian=\"Big\" Seek=\"0\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",coords_len,2);
#else
  PetscFPrintf(comm,fp,"    <DataItem Name=\"Coordinates\" Format=\"Binary\" Endian=\"Little\" Seek=\"0\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",coords_len,2);
#endif
  
  if (!use_common_mesh_file) {
    
    if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
    } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }

    ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);
    
    ierr = VecGetLocalSize(coords_global,&coords_len);CHKERRQ(ierr);
    coords_len = coords_len / 2;
    
    ierr = VecGetArrayRead(coords_global,&LA_field);CHKERRQ(ierr);
    
    ierr = MPIWriteUser_Blocking_PetscReal(fp_heavydata,(PetscReal*)LA_field,2*coords_len,0,PETSC_TRUE,PETSC_TRUE,comm,user_writer_binary_vec2,NULL);CHKERRQ(ierr);
    
    ierr = VecRestoreArrayRead(coords_global,&LA_field);CHKERRQ(ierr);
    
    ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
    fp_heavydata = NULL;
  }
  PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  PetscFPrintf(comm,fp,"  </Geometry>\n");
  
  ierr = VecDestroy(&coords_global);CHKERRQ(ierr);

  
  /* write Attributes <node> per input vector */
  for (f=0; f<nf; f++) {
    PetscInt len,len_local;
    PetscBool is_std_vector = PETSC_FALSE,is_scalar = PETSC_FALSE,is_other = PETSC_FALSE;
    
    ierr = VecGetBlockSize(field[f],&dof);CHKERRQ(ierr);
    
    if (dof == 1) {
      is_scalar = PETSC_TRUE;
    } else if (dof == 2) {
      is_std_vector = PETSC_TRUE;
    } else {
      is_other = PETSC_TRUE;
    }
    
    ierr = VecGetSize(field[f],&len);CHKERRQ(ierr);
    ierr = VecGetLocalSize(field[f],&len_local);CHKERRQ(ierr);
    /* get values */
    ierr = VecGetArrayRead(field[f],&LA_field);CHKERRQ(ierr);
    
    /* write Attributes <node> */
    
    if (is_std_vector) {
      PetscReal *buffer;
      PetscInt k;
      
      /* paraview requires vectors in 2D be written out with 3 components */
      PetscFPrintf(comm,fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Vector\">\n",fname[f]);
#ifdef WORDSIZE_BIGENDIAN
      PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"Binary\" Endian=\"Big\" Seek=\"0\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len/2,3);
#else
      PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"Binary\" Endian=\"Little\" Seek=\"0\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len/2,3);
#endif
      
      if (prefix != NULL) {
        ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sdmtet_vec3_%s.bin",prefix,fname[f]);CHKERRQ(ierr);
      } else {
        ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"dmtet_vec3_%s.bin",fname[f]);CHKERRQ(ierr);
      }
      
      if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
      } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }

      ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);

      /* How should this work with the stupid padding? - only option is to copy and pad - lame lame ParaView */
      ierr = PetscMalloc1(3*(len_local/2),&buffer);CHKERRQ(ierr);
      
      for (k=0; k<len_local/2; k++) {
        buffer[3*k]   = LA_field[2*k  ];
        buffer[3*k+1] = LA_field[2*k+1];
        buffer[3*k+2] = 0.0;
      }
      
      ierr = MPIWriteUser_Blocking_PetscReal(fp_heavydata,buffer,3*(len_local/2),0,PETSC_TRUE,PETSC_TRUE,comm,user_writer_binary_vec3,NULL);CHKERRQ(ierr);
      
      ierr = PetscFree(buffer);CHKERRQ(ierr);
      ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
      fp_heavydata = NULL;
      
      PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
      PetscFPrintf(comm,fp,"    </DataItem>\n");
      PetscFPrintf(comm,fp,"  </Attribute>\n");
      
    } else if (is_scalar) {
      PetscFPrintf(comm,fp,"  <Attribute Center=\"Node\" Name=\"%s\" AttributeType=\"Scalar\">\n",fname[f]);
#ifdef WORDSIZE_BIGENDIAN
      PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"Binary\" Endian=\"Big\" Seek=\"0\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len);
#else
      PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"Binary\" Endian=\"Little\" Seek=\"0\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],len);
#endif
      
      if (prefix != NULL) {
        ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sdmtet_scalar_%s.bin",prefix,fname[f]);CHKERRQ(ierr);
      } else {
        ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"dmtet_scalar_%s.bin",fname[f]);CHKERRQ(ierr);
      }
      
      if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
      } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }
      
      ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);

      ierr = MPIWrite_Blocking_PetscReal(fp_heavydata,(PetscReal*)LA_field,len_local,0,PETSC_TRUE,PETSC_TRUE,comm);CHKERRQ(ierr);
      
      ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
      fp_heavydata = NULL;

      PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
      PetscFPrintf(comm,fp,"    </DataItem>\n");
      PetscFPrintf(comm,fp,"  </Attribute>\n");
      
    } else if (is_other) {
      PetscInt d,k;
      PetscReal *buffer;
      size_t byte_offset = 0;
      
      ierr = PetscMalloc1(len_local/dof,&buffer);CHKERRQ(ierr);

      if (prefix != NULL) {
        ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sdmtet_array_%s.bin",prefix,fname[f]);CHKERRQ(ierr);
      } else {
        ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"dmtet_array_%s.bin",fname[f]);CHKERRQ(ierr);
      }
      
      if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
      } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }
      
      ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);

      for (d=0; d<dof; d++) {
        PetscFPrintf(comm,fp,"  <Attribute Center=\"Node\" Name=\"%s_%D\" AttributeType=\"Scalar\">\n",fname[f],d);
#ifdef WORDSIZE_BIGENDIAN
        PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"Binary\" Endian=\"Big\" Seek=\"%u\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],byte_offset,len/dof);
#else
        PetscFPrintf(comm,fp,"    <DataItem Name=\"%s\" Format=\"Binary\" Endian=\"Little\" Seek=\"%u\" Dimensions=\"%D 1\" NumberType=\"Float\" Precision=\"8\">\n",fname[f],byte_offset,len/dof);
#endif

        for (k=0; k<len_local/dof; k++) {
          buffer[k]   = LA_field[dof*k + d];
        }
        ierr = MPIWrite_Blocking_PetscReal(fp_heavydata,buffer,len_local/dof,0,PETSC_TRUE,PETSC_TRUE,comm);CHKERRQ(ierr);
        byte_offset = byte_offset + sizeof(PetscReal) * (len/dof);
        
        PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
        PetscFPrintf(comm,fp,"    </DataItem>\n");
        PetscFPrintf(comm,fp,"  </Attribute>\n");
      }
      ierr = PetscFree(buffer);CHKERRQ(ierr);
      ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
      fp_heavydata = NULL;
    }
    
    ierr = VecRestoreArrayRead(field[f],&LA_field);CHKERRQ(ierr);
    
  }
  
  PetscFPrintf(comm,fp,"  </Grid>\n");
  PetscFPrintf(comm,fp,"</Domain>\n");
  PetscFPrintf(comm,fp,"</Xdmf>\n");
  
  ierr = PetscFClose(comm,fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*
 If shared_coord_file[] == NULL, always write coordinates
 If shared_coord_file[] != NULL, write coordinate file only if write_coord_file = True,
 Full path to file name will look like:
   path / prefix_dmtet.xmf
 Each file prefix_dmtet.xmf, will potentially contain a reference to a geometry/topology file
 with the fullpath given by
   path / common / DMTetXDMFGeometry.bin
   path / common / DMTetXDMFTopology.bin
 Each file will simply contain a reference to common / DMTetXDMFGeometry.bin and common / DMTetXDMFTopology.bin
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetViewFieldsXDMFBinary"
PetscErrorCode DMTetViewFieldsXDMFBinary(DM dm,PetscReal time,PetscInt nf,Vec field[],const char *fname[],
                                         const char path[],const char prefix[],
                                         PetscBool use_common_mesh_file)
{
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  DMTetBasisType btype;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"XDMF reader Can only render continuous basis (DMTET_CWARP_AND_BLEND)");
  }

  /*
  pathprefix[0] = '\0';
  if (path) {
    ierr = PetscSNPrintf(pathprefix,PETSC_MAX_PATH_LEN-1,"%s",path);CHKERRQ(ierr);
    if (prefix) {
      ierr = PetscSNPrintf(pathprefix,PETSC_MAX_PATH_LEN-1,"%s/%s",path,prefix);CHKERRQ(ierr);
    }
  } else {
    if (prefix) {
      ierr = PetscSNPrintf(pathprefix,PETSC_MAX_PATH_LEN-1,"%s",prefix);CHKERRQ(ierr);
    }
  }
  */
  ierr = DMTetViewFieldsXDMF_b_MPI_mem_scalable(dm,time,nf,field,fname,path,prefix,use_common_mesh_file);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewMeshXDMF_b_MPI_mem_scalable"
PetscErrorCode DMTetViewMeshXDMF_b_MPI_mem_scalable(DM dm,PetscReal time,const char path[],const char prefix[])
{
  PetscErrorCode ierr;
  char filename[PETSC_MAX_PATH_LEN];
  FILE *fp = NULL;
  PetscInt nelements,nelements_g,npe,nnodes,*element,basis_order,i,j;
  PetscReal *coords;
  PetscInt nspilt,nfull,nsubel,nel_l;
  const PetscScalar *LA_field;
  MPI_Comm comm;
  PetscMPIInt commrank,commsize;
  Vec coords_global;
  PetscInt coords_len;
  CellWriterCtx *cell_context;
  FILE *fp_heavydata = NULL;
  char fname_heavydata[PETSC_MAX_PATH_LEN];
  char path_to_file[PETSC_MAX_PATH_LEN];
  
  if (prefix != NULL) {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"%sdmtet.xmf",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(filename,PETSC_MAX_PATH_LEN-1,"dmtet.xmf",prefix);CHKERRQ(ierr);
  }
  comm = PetscObjectComm((PetscObject)dm);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,filename);CHKERRQ(ierr);
  } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",filename);CHKERRQ(ierr); }
  ierr = PetscFOpen(comm,path_to_file,"w",&fp);CHKERRQ(ierr);
  
  PetscFPrintf(comm,fp,"<?xml version=\"1.0\"?>\n");
  PetscFPrintf(comm,fp,"<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n");
  PetscFPrintf(comm,fp,"<Xdmf xmlns:xi=\"http://www.w3.org/2003/XInclude\" Version=\"2.2\">\n");
  PetscFPrintf(comm,fp,"<Domain>\n");
  PetscFPrintf(comm,fp,"  <Grid Name=\"DMTet\" GridType=\"Uniform\">\n");
  PetscFPrintf(comm,fp,"  <Time Value=\"%1.8e\"/>\n",time);
  
  ierr = DMTetGetBasisOrder(dm,&basis_order);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nelements,NULL,&element,&npe,&nnodes,&coords);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&nelements,&nelements_g,1,MPIU_INT,MPI_SUM,comm);CHKERRQ(ierr);
  
  /* number of elements is given by high order elements * num sub elements */
  nspilt = basis_order;
  nfull  = sumdec_WAB(basis_order-1);
  nsubel = ( nspilt + 2*nfull );
  nel_l = nelements_g * nsubel;
  
  /* write Topology */
  if (prefix != NULL) {
    ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sDMTetXDMFTopology.bin",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"DMTetXDMFTopology.bin");CHKERRQ(ierr);
  }
  
  PetscFPrintf(comm,fp,"  <Topology TopologyType=\"Triangle\" NumberOfElements=\"%d\">\n",(int)nel_l);
  
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(comm,fp,"    <DataItem Format=\"Binary\" Endian=\"Big\" Seek=\"0\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
#else
  PetscFPrintf(comm,fp,"    <DataItem Format=\"Binary\" Endian=\"Little\" Seek=\"0\" DataType=\"Int\" Dimensions=\"%D %D\">\n",nel_l,3);
#endif
  
  {
    DM                     dm_scalar;
    ISLocalToGlobalMapping ltog;
    const PetscInt         *g_idx;
    PetscInt               *element_gidx;

    if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
    } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }

    ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);
    
    ierr = DMTetCreateSharedSpace(dm,1,&dm_scalar);CHKERRQ(ierr);
    
    /* copy and convert local indices into global indices */
    ierr = PetscMalloc1(nelements*npe,&element_gidx);CHKERRQ(ierr);
    ierr = DMGetLocalToGlobalMapping(dm_scalar,&ltog);CHKERRQ(ierr);
    ierr = ISLocalToGlobalMappingGetIndices(ltog,&g_idx);CHKERRQ(ierr);
    for (i=0; i<nelements * npe; i++) {
      PetscInt l_idx,l2g_idx;
      
      l_idx = element[i];
      l2g_idx = g_idx[ l_idx ];
      element_gidx[i] = l2g_idx;
    }
    ierr = ISLocalToGlobalMappingRestoreIndices(ltog,&g_idx);CHKERRQ(ierr);
    
    ierr = PetscMalloc1(1,&cell_context);CHKERRQ(ierr);
    /* init map */
    cell_context->nbasis_per_element = npe;
    cell_context->basis_order = basis_order;
    for (i=0; i<30; i++) {
      for (j=0; j<30; j++) {
        cell_context->map[i][j] = 0;
      }
    }
    
    ierr = MPIWriteUser_Blocking_PetscInt(fp_heavydata,element_gidx,nelements * npe,0,PETSC_TRUE,PETSC_TRUE,comm,user_writer_binary_cells,(void*)cell_context);CHKERRQ(ierr);
    
    ierr = PetscFree(cell_context);CHKERRQ(ierr);
    ierr = PetscFree(element_gidx);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
    ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
    fp_heavydata = NULL;
  }
  
  PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
  
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  /////
  PetscFPrintf(comm,fp,"  </Topology>\n");
  
  /* write Geometry */
  if (prefix != NULL) {
    ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"%sDMTetXDMFGeometry.bin",prefix);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(fname_heavydata,PETSC_MAX_PATH_LEN-1,"DMTetXDMFGeometry.bin");CHKERRQ(ierr);
  }
  
  ierr = DMTetCreateGlobalCoordinateVector(dm,&coords_global);CHKERRQ(ierr);
  ierr = VecGetSize(coords_global,&coords_len);CHKERRQ(ierr);
  coords_len = coords_len / 2;
  
  /////
  PetscFPrintf(comm,fp,"  <Geometry GeometryType=\"XY\">\n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(comm,fp,"    <DataItem Name=\"Coordinates\" Format=\"Binary\" Endian=\"Big\" Seek=\"0\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",coords_len,2);
#else
  PetscFPrintf(comm,fp,"    <DataItem Name=\"Coordinates\" Format=\"Binary\" Endian=\"Little\" Seek=\"0\" Dimensions=\"%D %D\" NumberType=\"Float\" Precision=\"8\">\n",coords_len,2);
#endif
  
  {
    if (path) { ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s/%s",path,fname_heavydata);CHKERRQ(ierr);
    } else {    ierr = PetscSNPrintf(path_to_file,PETSC_MAX_PATH_LEN-1,"%s",fname_heavydata);CHKERRQ(ierr); }
    
    ierr = PetscFOpen(comm,path_to_file,"w",&fp_heavydata);CHKERRQ(ierr);
    
    ierr = VecGetLocalSize(coords_global,&coords_len);CHKERRQ(ierr);
    coords_len = coords_len / 2;
    
    ierr = VecGetArrayRead(coords_global,&LA_field);CHKERRQ(ierr);
    
    ierr = MPIWriteUser_Blocking_PetscReal(fp_heavydata,(PetscReal*)LA_field,2*coords_len,0,PETSC_TRUE,PETSC_TRUE,comm,user_writer_binary_vec2,NULL);CHKERRQ(ierr);
    
    ierr = VecRestoreArrayRead(coords_global,&LA_field);CHKERRQ(ierr);
    
    ierr = PetscFClose(comm,fp_heavydata);CHKERRQ(ierr);
    fp_heavydata = NULL;
  }
  PetscFPrintf(comm,fp,"    %s\n",fname_heavydata);
  PetscFPrintf(comm,fp,"    </DataItem>\n");
  PetscFPrintf(comm,fp,"  </Geometry>\n");
  
  ierr = VecDestroy(&coords_global);CHKERRQ(ierr);
  
  PetscFPrintf(comm,fp,"  </Grid>\n");
  PetscFPrintf(comm,fp,"</Domain>\n");
  PetscFPrintf(comm,fp,"</Xdmf>\n");
  
  ierr = PetscFClose(comm,fp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewMeshTopologyXDMFBinary"
PetscErrorCode DMTetViewMeshTopologyXDMFBinary(DM dm,PetscReal time,const char path[],const char prefix[])
{
  MPI_Comm comm;
  PetscMPIInt commsize;
  PetscErrorCode ierr;
  DMTetBasisType btype;
  
  ierr = PetscObjectGetComm((PetscObject)dm,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  
  ierr = DMTetGetBasisType(dm,&btype);CHKERRQ(ierr);
  if (btype != DMTET_CWARP_AND_BLEND) {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"XDMF reader Can only render continuous basis (DMTET_CWARP_AND_BLEND)");
  }
  
  ierr = DMTetViewMeshXDMF_b_MPI_mem_scalable(dm,time,path,prefix);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetViewMeshTopologyCommonXDMFBinary"
PetscErrorCode DMTetViewMeshTopologyCommonXDMFBinary(DM dm,const char path[])
{
  char path2common[PETSC_MAX_PATH_LEN];
  PetscErrorCode ierr;
  
  ierr = PetscSNPrintf(path2common,PETSC_MAX_PATH_LEN-1,"%s/common",path);CHKERRQ(ierr);
  ierr = DMTetViewMeshTopologyXDMFBinary(dm,0.0,path2common,NULL);
  
  PetscFunctionReturn(0);
}


