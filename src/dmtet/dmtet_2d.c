
#include <petsc.h>
#include <dmtetimpl.h>    /*I   "petscdmtet.h"   I*/
#include <dmtet_common.h>
#include <petscdmtet.h>


#undef __FUNCT__
#define __FUNCT__ "_DMTetCreateGeometryTriangle_ReadEleFile"
PetscErrorCode _DMTetCreateGeometryTriangle_ReadEleFile(DM_TET *tet,FILE *fp)
{
  PetscErrorCode ierr;
  int nel,npe,nattributes;
  int index,n0,n1,n2,tag,cnt,shift;
  PetscInt _nel,_npe;
  
  fscanf(fp,"%d %d %d",&nel,&npe,&nattributes);
  PetscMPIIntCast(nel,&_nel);
  PetscMPIIntCast(npe,&_npe);

  if (npe != 3) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for reading 3 node elements");
  }
  if (nattributes > 1) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for reading 1 element attribute");
  }
  
  /* allocate space */
  ierr = _DMTetMeshCreate1(_nel,3,tet->geometry);CHKERRQ(ierr);
  
  /* scan and load */
  cnt = 0;
  shift = 0;
  while (cnt != nel) {
    PetscInt _n0,_n1,_n2,_tag;
    if (nattributes == 0) {
      fscanf(fp,"%d %d %d %d",&index,&n0,&n1,&n2);
    } else if (nattributes == 1) {
      fscanf(fp,"%d %d %d %d %d",&index,&n0,&n1,&n2,&tag);
    }
    
    if (cnt == 0) {
      if (index == 1) shift = 1;
    }
    
    n0 = n0 - shift;
    n1 = n1 - shift;
    n2 = n2 - shift;
    
    PetscMPIIntCast(n0,&_n0);
    PetscMPIIntCast(n1,&_n1);
    PetscMPIIntCast(n2,&_n2);
    
    tet->geometry->element[3*cnt+0] = _n0;
    tet->geometry->element[3*cnt+1] = _n1;
    tet->geometry->element[3*cnt+2] = _n2;
    
    tet->geometry->element_tag[cnt] = 0;
    if (nattributes == 1) {
      PetscMPIIntCast(tag,&_tag);
      tet->geometry->element_tag[cnt] = _tag;
    }
    
    cnt++;
  }
  tet->geometry->nelements = _nel;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMTetCreateGeometryTriangle_ReadNodeFile"
PetscErrorCode _DMTetCreateGeometryTriangle_ReadNodeFile(DM_TET *tet,FILE *fp)
{
  PetscErrorCode ierr;
  int nn,dim,nattr,boundarymarkers,bmarker;
  int index,cnt;
  double x,y,dummy_attr;
  PetscInt _nn;
  
  fscanf(fp,"%d %d %d %d",&nn,&dim,&nattr,&boundarymarkers);
  PetscMPIIntCast(nn,&_nn);
  
  if (nattr == 1) {
    PetscPrintf(PETSC_COMM_SELF,"Warning: Detected one node attributes, however these will not be stored\n");
  }
  
  if (nattr > 1) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only support for reading 0 or 1 node attribute\n");
  }
  
  /* allocate space */
  ierr = _DMTetMeshCreate2(_nn,3,2,tet->geometry);CHKERRQ(ierr);
  
  /* scan and load */
  cnt = 0;
  while (cnt != nn) {
    if (nattr == 0) {
      fscanf(fp,"%d %lf %lf %d",&index,&x,&y,&bmarker);
    } else if (nattr == 1) {
      fscanf(fp,"%d %lf %lf %lf %d",&index,&x,&y,&dummy_attr,&bmarker);
    }
    
    tet->geometry->coords[2*cnt+0] = (PetscReal)x;
    tet->geometry->coords[2*cnt+1] = (PetscReal)y;
    
    cnt++;
  }
  tet->geometry->nnodes = _nn;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateGeometryFromTriangle"
PetscErrorCode DMTetCreateGeometryFromTriangle(DM dm,const char name[])
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  FILE *fp;
  char elefile[PETSC_MAX_PATH_LEN];
  char nodefile[PETSC_MAX_PATH_LEN];
  
  
  ierr = _DMTetMeshCreate(&tet->geometry);CHKERRQ(ierr);
  
  PetscSNPrintf(elefile,sizeof(elefile),"%s.ele",name);
  fp = fopen(elefile,"r");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",elefile);
  }
  ierr = _DMTetCreateGeometryTriangle_ReadEleFile(tet,fp);CHKERRQ(ierr);
  fclose(fp);
  
  PetscSNPrintf(nodefile,sizeof(nodefile),"%s.node",name);
  fp = fopen(nodefile,"r");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",nodefile);
  }
  ierr = _DMTetCreateGeometryTriangle_ReadNodeFile(tet,fp);CHKERRQ(ierr);
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGeometryView2d_VTK"
PetscErrorCode DMTetGeometryView2d_VTK(DM dm,const char name[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  FILE *fp;
  PetscInt i,e;
  
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",tet->geometry->nnodes,tet->geometry->nelements);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D %D %D ",
                 tet->geometry->element[3*e],tet->geometry->element[3*e+1],tet->geometry->element[3*e+2]);
    
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",3*e+3);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"5 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"region_idx\" format=\"ascii\"> \n");
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",tet->geometry->element_tag[e]);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<tet->geometry->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",tet->geometry->coords[2*i],tet->geometry->coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"bndy_tag\" format=\"ascii\"> \n");
  for (i=0; i<tet->geometry->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"1 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBasisView2d_Continuous_VTK"
PetscErrorCode DMTetBasisView2d_Continuous_VTK(DM dm,const char name[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  FILE *fp;
  PetscInt i,e;
  
  fp = fopen(name,"w");
  if (fp == NULL) {
    SETERRQ1(PetscObjectComm((PetscObject)dm),PETSC_ERR_FILE_OPEN,"Cannot open file %s",name);
  }
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"<?xml version=\"1.0\"?> \n");
#ifdef WORDSIZE_BIGENDIAN
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\"> \n");
#else
  PetscFPrintf(PETSC_COMM_SELF,fp,"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\"> \n");
#endif
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"  <UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Piece NumberOfPoints=\"%D\" NumberOfCells=\"%D\">\" \n",tet->space->nnodes,tet->space->nnodes);
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Cells> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",e);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%D ",e+1);
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\"> \n");
  for (e=0; e<tet->space->nnodes; e++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"1 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Cells> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <CellData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </CellData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <Points> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\"> \n");
  for (i=0; i<tet->space->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"%1.10e %1.10e 0.0 ",tet->space->coords[2*i],tet->space->coords[2*i+1]);
  }     PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Points> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    <PointData> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      <DataArray type=\"Float64\" Name=\"one\" format=\"ascii\"> \n");
  for (i=0; i<tet->space->nnodes; i++) {
    PetscFPrintf(PETSC_COMM_SELF,fp,"1.0 ");
  } PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"      </DataArray> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </PointData> \n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"\n");
  
  PetscFPrintf(PETSC_COMM_SELF,fp,"    </Piece> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"  </UnstructuredGrid> \n");
  PetscFPrintf(PETSC_COMM_SELF,fp,"</VTKFile>");
  
  fclose(fp);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetBasisView2d_VTK"
PetscErrorCode DMTetBasisView2d_VTK(DM dm,const char name[])
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  
  switch (tet->basis_type) {
      
    case DMTET_CLEGENDRE:
      ierr = DMTetBasisView2d_Continuous_VTK(dm,name);CHKERRQ(ierr);
      break;
      
    case DMTET_CFETEKE:
      ierr = DMTetBasisView2d_Continuous_VTK(dm,name);CHKERRQ(ierr);
      break;
      
    case DMTET_DG:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot generate VTK output for DMTET_DG basis");
      break;
      
    case DMTET_CWARP_AND_BLEND:
      ierr = DMTetBasisView2d_Continuous_VTK(dm,name);CHKERRQ(ierr);
      break;
      
    default:
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d_SEQ"
PetscErrorCode DMTetGenerateBasis2d_SEQ(DM dm)
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  
  switch (tet->basis_type) {
      
    case DMTET_CLEGENDRE:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Do not use DMTET_CLEGENDRE basis - always use DMTET_CWARP_AND_BLEND");
      break;
      
    case DMTET_CFETEKE:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot generate DMDTET_CFETEKE basis");
      break;
      
    case DMTET_DG:
      ierr = DMTetGenerateBasis2d_DG(dm,tet,tet->basis_order);CHKERRQ(ierr);
      tet->space->nactivedof = tet->space->bpe * tet->space->nelements;
      break;
      
    case DMTET_CWARP_AND_BLEND:
      ierr = DMTetGenerateBasis2d_CWARPANDBLEND(dm,tet,tet->basis_order);CHKERRQ(ierr);
      tet->space->nactivedof = tet->space->nnodes;
      break;

    case DMTET_RAVIART_THOMAS:
      if (tet->dof != 2) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"RT element requires user set dof = 2");
      ierr = DMTetGenerateBasis2d_RaviartThomas(dm,tet,tet->basis_order);CHKERRQ(ierr);
      /* nactive dof specified inside this function */
      break;

    default:
      break;
  }
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d_MPI"
PetscErrorCode DMTetGenerateBasis2d_MPI(DM dm)
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (!tet->pinfo) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"tet->pinfo has not been allocated, or set");
  if (!tet->partition) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"tet->partition has not been allocated, or set");
  if (!tet->mpi) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"tet->mpi has not been allocated, or set");
  
  switch (tet->basis_type) {
      
    case DMTET_CLEGENDRE:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Do not use DMTET_CLEGENDRE basis - always use DMTET_CWARP_AND_BLEND");
      break;
      
    case DMTET_CFETEKE:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot generate DMDTET_CFETEKE basis");
      break;
      
    case DMTET_DG:
      ierr = DMTetGenerateBasis2d_DG_MPI(dm,tet,tet->basis_order);CHKERRQ(ierr);
      tet->space->nactivedof = -1;
      break;
      
    case DMTET_CWARP_AND_BLEND:
      ierr = _DMTetGenerateBasis2d_CWARPANDBLEND_MPI(dm,tet->partition->element_rank_ghost,tet->basis_type,tet->basis_order,tet);CHKERRQ(ierr);
      tet->space->nactivedof = -1;
      break;
      
    case DMTET_RAVIART_THOMAS:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot generate DMDTET_RT basis in parallal");
      /* nactive dof specified inside this function */
      break;
      
    default:
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetGenerateBasis2d"
PetscErrorCode DMTetGenerateBasis2d(DM dm)
{
  PetscErrorCode ierr;
  PetscMPIInt commsize;
  DM_TET *tet = (DM_TET*)dm->data;
  
  if (dm->setupcalled) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMSetUp already called - why is DMTetGenerateBasis2d() being called again??");
  if (tet->space) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMTet already called has a space defined - why is DMTetGenerateBasis2d() being called again??");
  ierr = MPI_Comm_size(PetscObjectComm((PetscObject)dm),&commsize);CHKERRQ(ierr);
  if (commsize == 1) {
    ierr = DMTetGenerateBasis2d_SEQ(dm);CHKERRQ(ierr);
  } else {
    ierr = DMTetGenerateBasis2d_MPI(dm);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetView_2d"
PetscErrorCode DMTetView_2d(DM dm,PetscViewer viewer)
{
  PetscErrorCode ierr;
  PetscMPIInt    rank;
  PetscBool      iascii,isbinary;
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt       dim;
  
  ierr = MPI_Comm_rank(PetscObjectComm((PetscObject)dm),&rank);CHKERRQ(ierr);
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERASCII,&iascii);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERBINARY,&isbinary);CHKERRQ(ierr);
  
  if (iascii) {
    PetscViewerFormat format;
    
    ierr = PetscViewerGetFormat(viewer, &format);CHKERRQ(ierr);
    if (format != PETSC_VIEWER_ASCII_VTK) {
      
      ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(viewer,"DMTetView:\n");CHKERRQ(ierr);

      ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
      if (!dm->setupcalled) {
        ierr = PetscViewerASCIIPrintf(viewer,"DM not yet setup\n");CHKERRQ(ierr);
      }
      ierr = PetscViewerASCIIPrintf(viewer,"dim: %D\n",dim);CHKERRQ(ierr);
      if (tet->pinfo) {
        ierr = PetscViewerASCIIPrintf(viewer,"elements: %D\n",tet->pinfo->nelements);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"partitions: %D\n",tet->pinfo->npartitions);CHKERRQ(ierr);
      } else {
        ierr = PetscViewerASCIIPrintf(viewer,"elements: %D\n",tet->geometry->nelements);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"partitions: 1\n");CHKERRQ(ierr);
      }
      
      
      ierr = PetscViewerASCIIPrintf(viewer,"[GeometryFunctionSpace]\n");CHKERRQ(ierr);
      ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
      if (!tet->geometry) {
        ierr = PetscViewerASCIIPrintf(viewer,"geometry not yet setup\n");CHKERRQ(ierr);
      } else {
        ierr = PetscViewerASCIIPrintf(viewer,"basis type: %s\n",DMTetBasisTypeNames[(int)(DMTET_CWARP_AND_BLEND)]);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"basis degree: %D\n",1);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"basis per element: %D\n",tet->geometry->npe);CHKERRQ(ierr);
      }
      ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
      
      ierr = PetscViewerASCIIPrintf(viewer,"[FunctionSpace]\n");CHKERRQ(ierr);
      ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
      if (!tet->space) {
        ierr = PetscViewerASCIIPrintf(viewer,"function is not yet setup\n");CHKERRQ(ierr);
      } else {
        PetscInt nbasis_domain;

        ierr = DMTetSpaceGetSize(dm,&nbasis_domain,NULL);CHKERRQ(ierr);

        ierr = PetscViewerASCIIPrintf(viewer,"basis type: %s\n",DMTetBasisTypeNames[(int)(tet->basis_type)]);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"basis degree: %D\n",tet->basis_order);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"dof: %D\n",tet->dof);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"basis per element: %D\n",tet->space->bpe);CHKERRQ(ierr);
        ierr = PetscViewerASCIIPrintf(viewer,"basis (total): %D\n",nbasis_domain);CHKERRQ(ierr);
      }
      ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);

      
      ierr = PetscViewerASCIIPrintf(viewer,"[Sub-domains]\n");CHKERRQ(ierr);
      ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPushSynchronized(viewer);CHKERRQ(ierr);
      if (tet->geometry) {
        ierr = PetscViewerASCIISynchronizedPrintf(viewer,"[r%d] geometry { nelements %D , basis %D }\n",rank,tet->geometry->nelements,tet->geometry->nnodes);CHKERRQ(ierr);
      }
      ierr = PetscViewerFlush(viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPopSynchronized(viewer);CHKERRQ(ierr);

      ierr = PetscViewerASCIIPushSynchronized(viewer);CHKERRQ(ierr);
      if (tet->space) {
        PetscInt nbasis_subdomain;
        
        ierr = DMTetSpaceGetSubdomainSize(dm,&nbasis_subdomain,NULL);CHKERRQ(ierr);
        ierr = PetscViewerASCIISynchronizedPrintf(viewer,"[r%d] space { nelements %D , basis %D }\n",rank,tet->space->nelements,nbasis_subdomain);CHKERRQ(ierr);
      }
      ierr = PetscViewerFlush(viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPopSynchronized(viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
      
      ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
    } else {
      //ierr = DMTetBasisView2d_VTK(da,viewer);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateGlobalVector_SEQ"
PetscErrorCode DMTetCreateGlobalVector_SEQ(DM dm,Vec *_x)
{
  Vec      x;
  PetscInt npe,nnodes,dof;
  DM_TET *tet = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  ierr = DMTetGetDof(dm,&dof);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,0,0,0,&npe,&nnodes,0); CHKERRQ(ierr);
  
  ierr = VecCreate(PetscObjectComm((PetscObject)dm),&x);CHKERRQ(ierr);
  ierr = VecSetSizes(x,PETSC_DECIDE,tet->space->nactivedof*dof);CHKERRQ(ierr);
  ierr = VecSetBlockSize(x,dof);CHKERRQ(ierr);
  ierr = VecSetType(x,VECSEQ);CHKERRQ(ierr);
  ierr = VecSetDM(x,dm);CHKERRQ(ierr);
  ierr = VecSetLocalToGlobalMapping(x,dm->ltogmap);CHKERRQ(ierr);
  
  *_x = x;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSEQ_GlobalToLocalBegin"
PetscErrorCode DMTetSEQ_GlobalToLocalBegin(DM dm,Vec g,InsertMode mode,Vec l)
{
  PetscErrorCode ierr;
  
  switch (mode) {
    case INSERT_VALUES:
      ierr = VecCopy(g,l);CHKERRQ(ierr);
      break;
    case ADD_VALUES:
      ierr = VecAXPY(l,1.0,g);CHKERRQ(ierr); /* l = l + g */
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unsuppored insert mode prescribed");
      break;
  }
  PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMTetSEQ_GlobalToLocalEnd"
PetscErrorCode DMTetSEQ_GlobalToLocalEnd(DM dm,Vec g,InsertMode mode,Vec l)
{
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetSEQ_LocalToGlobalBegin"
PetscErrorCode DMTetSEQ_LocalToGlobalBegin(DM dm,Vec l,InsertMode mode,Vec g)
{
  PetscErrorCode ierr;
  
  switch (mode) {
    case INSERT_VALUES:
      ierr = VecCopy(l,g);CHKERRQ(ierr);
      break;
    case ADD_VALUES:
      ierr = VecAXPY(g,1.0,l);CHKERRQ(ierr); /* g = g + l */
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unsuppored insert mode prescribed");
      break;
  }
  PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMTetSEQ_LocalToGlobalEnd"
PetscErrorCode DMTetSEQ_LocalToGlobalEnd(DM dm,Vec l,InsertMode mode,Vec g)
{
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreate2dFromTriangle"
PetscErrorCode DMTetCreate2dFromTriangle(MPI_Comm comm,const char filename[],PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm)
{
  DM             dm;
  PetscMPIInt    commsize;
  PetscErrorCode ierr;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize > 1) SETERRQ(comm,PETSC_ERR_SUP,"Expected commsize = 1. Call DMTetCreatePartitionedFromTriangle() instead");
  
  ierr = DMTetCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,2);CHKERRQ(ierr);
  ierr = DMTetSetDof(dm,dof);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(dm,btype);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dm,border);CHKERRQ(ierr);
  
  ierr = DMTetCreateGeometryFromTriangle(dm,filename);CHKERRQ(ierr);
  
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);
  
  ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);
  
  ierr = DMViewFromOptions(dm,NULL,"-dm_view");CHKERRQ(ierr);
  
  ierr = DMSetUp(dm);CHKERRQ(ierr);

  *_dm = dm;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSquareGeometry"
PetscErrorCode DMTetCreateSquareGeometry(DM dm,
                                         PetscReal x0,PetscReal x1,PetscReal y0,PetscReal y1,
                                         PetscInt mx,PetscInt my,PetscBool right_oriented)
{
  PetscErrorCode ierr;
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt i,j,nx,ny;
  PetscReal dx,dy,ds,perturb;
  PetscInt nel,nnodes,elcnt;
  PetscBool hasperturb = PETSC_FALSE;
  PetscBool no_central_node_perturb = PETSC_TRUE;
  PetscInt nidx;
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-dmtet_square_no_central_node_perturb",&no_central_node_perturb,NULL);CHKERRQ(ierr);
  
  perturb = 0.0;
  ierr = PetscOptionsGetReal(NULL,NULL,"-dmtet_square_perturb",&perturb,&hasperturb);CHKERRQ(ierr);
  if (perturb > 1.0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"-dmtet_square_perturb must be less than 1.0");
  
  ierr = _DMTetMeshCreate(&tet->geometry);CHKERRQ(ierr);
  
  nel = mx * my * 2;
  nnodes = (mx+1)*(my+1);
  
  ierr = _DMTetMeshCreate1(nel,3,tet->geometry);CHKERRQ(ierr);
  ierr = _DMTetMeshCreate2(nnodes,3,2,tet->geometry);CHKERRQ(ierr);
  
  dx = (x1-x0)/((PetscReal)mx);
  dy = (y1-y0)/((PetscReal)my);
  ds = dx; if (dy < dx) ds = dy;
  ds = 0.45 * ds * perturb;
  
  /* coordinates */
  nx = mx + 1;
  ny = my + 1;
  for (j=0; j<ny; j++) {
    for (i=0; i<nx; i++) {
      PetscReal xn[2];
      
      xn[0] = x0 + i * dx;
      xn[1] = y0 + j * dy;
      
      nidx = i + j*nx;
      
      tet->geometry->coords[2*nidx+0] = xn[0];
      tet->geometry->coords[2*nidx+1] = xn[1];
    }
  }
  
  if (hasperturb) {
    srand(0);
    
    for (j=1; j<ny-1; j++) {
      for (i=1; i<nx-1; i++) {
        PetscReal rx[2];
        
        nidx = i + j*nx;
        rx[0] = -1.0 + 2.0 *rand()/((double)RAND_MAX);
        rx[1] = -1.0 + 2.0 *rand()/((double)RAND_MAX);
        tet->geometry->coords[2*nidx+0] += ds*rx[0];
        tet->geometry->coords[2*nidx+1] += ds*rx[1];
      }
    }
  }
  
  /* place central node */
  if (no_central_node_perturb) {
    if (mx%2 == 0) {
      if (my%2 == 0) {
        PetscReal xn[2];
        
        i = mx/2;
        j = my/2;
        nidx = i + j*nx;
        xn[0] = x0 + i * dx;
        xn[1] = y0 + j * dy;
        tet->geometry->coords[2*nidx+0] = xn[0];
        tet->geometry->coords[2*nidx+1] = xn[1];
      }
    }
  }
  
  /* connectivity */
  elcnt = 0;
  for (j=0; j<my; j++) {
    for (i=0; i<mx; i++) {
      PetscInt refsquare[4],triidx[3];
      PetscInt ni;
      
      /*
       2----3
       |    |
       |    |
       0----1
       */
      refsquare[0] = (i)   + (j)*nx;
      refsquare[1] = (i+1) + (j)*nx;
      refsquare[2] = (i)   + (j+1)*nx;
      refsquare[3] = (i+1) + (j+1)*nx;
      
      /*
       Vertices are number in an anti-clockwise manner
       The first triangle is always that which is lower (e.g. contains the edge 0 --> 1
       */
      /* first */
      if (right_oriented) {
        triidx[0] = refsquare[0];
        triidx[1] = refsquare[1];
        triidx[2] = refsquare[3];
      } else {
        triidx[0] = refsquare[0];
        triidx[1] = refsquare[1];
        triidx[2] = refsquare[2];
      }
      for (ni=0; ni<3; ni++) {
        tet->geometry->element[ 3*elcnt + ni ] = triidx[ni];
      }
      elcnt++;
      
      /* second */
      if (right_oriented) {
        triidx[0] = refsquare[0];
        triidx[1] = refsquare[3];
        triidx[2] = refsquare[2];
      } else {
        triidx[0] = refsquare[1];
        triidx[1] = refsquare[3];
        triidx[2] = refsquare[2];
      }
      for (ni=0; ni<3; ni++) {
        tet->geometry->element[ 3*elcnt + ni ] = triidx[ni];
      }
      elcnt++;
    }
  }
  
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreate2dSquareGeometry"
PetscErrorCode DMTetCreate2dSquareGeometry(MPI_Comm comm,
                                           PetscReal x0,PetscReal x1,PetscReal y0,PetscReal y1,
                                           PetscInt mx,PetscInt my,
                                           PetscBool right_oriented,
                                           PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm)
{
  DM             dm;
  PetscInt       _mx,_my;
  PetscBool      flg,_right_oriented;
  PetscReal      _x0[2],_x1[2];
  PetscMPIInt    commsize;
  PetscErrorCode ierr;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize > 1) SETERRQ(comm,PETSC_ERR_SUP,"Expected commsize = 1. Call DMTetCreatePartitionedSquareGeometry() instead");
  
  _right_oriented = right_oriented;
  _mx = mx;
  _my = my;
  _x0[0] = x0;  _x0[1] = y0;
  _x1[0] = x1;  _x1[1] = y1;
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-dmtet_square_mx",&_mx,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-dmtet_square_my",&_my,&flg);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetReal(NULL,NULL,"-dmtet_square_x0",&_x0[0],&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dmtet_square_y0",&_x0[1],&flg);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetReal(NULL,NULL,"-dmtet_square_x1",&_x1[0],&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dmtet_square_y1",&_x1[1],&flg);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetBool(NULL,NULL,"-dmtet_square_right_oriented",&_right_oriented,&flg);CHKERRQ(ierr);
  
  
  ierr = DMTetCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,2);CHKERRQ(ierr);
  ierr = DMTetSetDof(dm,dof);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(dm,btype);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dm,border);CHKERRQ(ierr);
  
  ierr = DMTetCreateSquareGeometry(dm,_x0[0],_x1[0],_x0[1],_x1[1],_mx,_my,_right_oriented);CHKERRQ(ierr);
  
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);
  
  ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);
  
  ierr = DMViewFromOptions(dm,NULL,"-dm_view");CHKERRQ(ierr);
  
  ierr = DMSetUp(dm);CHKERRQ(ierr);

  *_dm = dm;
  
  PetscFunctionReturn(0);
}


PetscErrorCode DMTetCreate2dLowOrderSpace_CLEGENDRE(DM dm,DM *_dml);
PetscErrorCode DMTetCreate2dLowOrderSpace_CWARPANDBLEND(DM dm,DM *_dml);

#undef __FUNCT__
#define __FUNCT__ "DMTetCreate2dLowOrderSpace"
PetscErrorCode DMTetCreate2dLowOrderSpace(DM dm,DM *dml)
{
  DM_TET *tet = (DM_TET*)dm->data;
  
  switch (tet->basis_type) {
      
    case DMTET_CLEGENDRE:
      //ierr = DMTetCreate2dLowOrderSpace_CLEGENDRE(dm,dml);CHKERRQ(ierr);
      break;
      
    case DMTET_CFETEKE:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot generate low order space for DMDTET_CFETEKE basis");
      break;
      
    case DMTET_DG:
      SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Cannot generate low order space for DMTET_DG basis");
      break;
      
    case DMTET_CWARP_AND_BLEND:
      //ierr = DMTetCreate2dLowOrderSpace_CWARPANDBLEND(dm,dml);CHKERRQ(ierr);
      break;
      
    default:
      break;
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetScaleCoordinates2d"
PetscErrorCode DMTetScaleCoordinates2d(DM dm,PetscReal xfac,PetscReal yfac)
{
  DM_TET *tet = (DM_TET*)dm->data;
  PetscInt i,nnodes;
  
  if (tet->geometry) {
    DMTetMesh *m =tet->geometry;
    
    nnodes = m->nnodes;
    for (i=0; i<nnodes; i++) {
      m->coords[2*i+0] = m->coords[2*i+0] * xfac;
      m->coords[2*i+1] = m->coords[2*i+1] * yfac;
    }
  }
  if (tet->space) {
    DMTetMesh *m =tet->space;
    
    nnodes = m->nnodes;
    for (i=0; i<nnodes; i++) {
      m->coords[2*i+0] = m->coords[2*i+0] * xfac;
      m->coords[2*i+1] = m->coords[2*i+1] * yfac;
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetComputeApproxCentroid2d"
PetscErrorCode DMTetComputeApproxCentroid2d(PetscReal coor[],PetscReal cx[])
{
  PetscInt i,nnodes;
  PetscReal ncx[] = { 0.0, 0.0 };
  
  nnodes = 3;
  for (i=0; i<nnodes; i++) {
    ncx[0] += coor[2*i+0];
    ncx[1] += coor[2*i+1];
  }
  ncx[0] = ncx[0] / ((PetscReal)nnodes);
  ncx[1] = ncx[1] / ((PetscReal)nnodes);
  
  cx[0] = ncx[0];
  cx[1] = ncx[1];
  PetscFunctionReturn(0);
}

/*
 This returns the radius of two circles:
 - the min rad defines the min seperation (taken over all elements)
 beteween the element centroid and a vertex
 - the max rad defines the max separation between the element centroid and a vertex
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetGeometryComputeElementDiameterBounds2d"
PetscErrorCode DMTetGeometryComputeElementDiameterBounds2d(DM dm,PetscReal *_hmin,PetscReal *_hmax)
{
  DM_TET *tet = (DM_TET*)dm->data;
  const PetscInt nsd = 2;
  PetscInt e,i;
  PetscReal elcoor[2*3],ds_min,ds_max;
  PetscErrorCode ierr;
  
  *_hmin = PETSC_MIN_REAL;
  *_hmax = PETSC_MAX_REAL;
  
  ds_min = PETSC_MAX_REAL;
  ds_max = PETSC_MIN_REAL;
  
  /* traverse geometry, examine min/max h */
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscReal cx[2];
    
    /* get element coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[nsd*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[nsd*idx+1];
    }
    
    /* fetch centroid */
    ierr = DMTetComputeApproxCentroid2d(elcoor,cx);CHKERRQ(ierr);
    
    /* compare vertex lengths relative to centroid */
    for (i=0; i<3; i++) {
      PetscReal dx[2],sep;
      
      dx[0] = PetscAbsReal(elcoor[2*i+0]-cx[0]);
      dx[1] = PetscAbsReal(elcoor[2*i+1]-cx[1]);
      
      sep = PetscSqrtReal( dx[0]*dx[0] + dx[1]*dx[1] );
      
      ds_min = PetscMin(ds_min,sep);
      ds_max = PetscMax(ds_max,sep);
    }
  }
  
  ds_min = 2.0 * ds_min;
  ds_max = 2.0 * ds_max;
  
  *_hmin = ds_min;
  *_hmax = ds_max;
  
  PetscFunctionReturn(0);
}

/*
 Computes the min/max separation between the element centroid and the mid-side coordinate within each element
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetGeometryComputeEnclosedSphereBounds2d"
PetscErrorCode DMTetGeometryComputeEnclosedSphereBounds2d(DM dm,PetscReal hbounds[])
{
  DM_TET *tet = (DM_TET*)dm->data;
  const PetscInt nsd = 2;
  PetscInt e,i;
  PetscReal elcoor[2*3],ds_min,ds_max;
  PetscErrorCode ierr;
  
  hbounds[0] = PETSC_MIN_REAL;
  hbounds[1] = PETSC_MAX_REAL;
  
  ds_min = PETSC_MAX_REAL;
  ds_max = PETSC_MIN_REAL;
  
  /* traverse geometry, examine min/max h */
  for (e=0; e<tet->geometry->nelements; e++) {
    PetscReal cx[2];
    PetscReal c01[2],c02[2],c12[2];
    PetscInt ii,jj;
    
    /* get element coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[nsd*i+0] = tet->geometry->coords[nsd*idx+0];
      elcoor[nsd*i+1] = tet->geometry->coords[nsd*idx+1];
    }
    
    /* fetch centroid */
    ierr = DMTetComputeApproxCentroid2d(elcoor,cx);CHKERRQ(ierr);
    
    /* compute coordinates of side lengths */
    ii = 0;      jj = 1;
    c01[0] = 0.5 * ( elcoor[nsd*ii+0] + elcoor[nsd*jj+0] );
    c01[1] = 0.5 * ( elcoor[nsd*ii+1] + elcoor[nsd*jj+1] );
    
    ii = 0;      jj = 2;
    c02[0] = 0.5 * ( elcoor[nsd*ii+0] + elcoor[nsd*jj+0] );
    c02[1] = 0.5 * ( elcoor[nsd*ii+1] + elcoor[nsd*jj+1] );
    
    ii = 1;      jj = 2;
    c12[0] = 0.5 * ( elcoor[nsd*ii+0] + elcoor[nsd*jj+0] );
    c12[1] = 0.5 * ( elcoor[nsd*ii+1] + elcoor[nsd*jj+1] );
    
    
    /* compare lengths relative to centroid */
    {
      PetscReal dx[2],sep;
      
      dx[0] = PetscAbsReal(c01[0]-cx[0]);
      dx[1] = PetscAbsReal(c01[1]-cx[1]);
      sep = PetscSqrtReal( dx[0]*dx[0] + dx[1]*dx[1] );
      ds_min = PetscMin(ds_min,sep);
      ds_max = PetscMax(ds_max,sep);
      
      dx[0] = PetscAbsReal(c02[0]-cx[0]);
      dx[1] = PetscAbsReal(c02[1]-cx[1]);
      sep = PetscSqrtReal( dx[0]*dx[0] + dx[1]*dx[1] );
      ds_min = PetscMin(ds_min,sep);
      ds_max = PetscMax(ds_max,sep);
      
      dx[0] = PetscAbsReal(c12[0]-cx[0]);
      dx[1] = PetscAbsReal(c12[1]-cx[1]);
      sep = PetscSqrtReal( dx[0]*dx[0] + dx[1]*dx[1] );
      ds_min = PetscMin(ds_min,sep);
      ds_max = PetscMax(ds_max,sep);
    }
  }
  
  hbounds[0] = ds_min;
  hbounds[1] = ds_max;
  
  PetscFunctionReturn(0);
}

/*
 This returns the radius of two circles:
 - the min rad defines the min seperation (taken over all elements)
 beteween the element centroid and a vertex
 - the max rad defines the max separation between the element centroid and a vertex
 */
#undef __FUNCT__
#define __FUNCT__ "DMTetGeometryComputeElementDiameter2d"
PetscErrorCode DMTetGeometryComputeElementDiameter2d(DM dm,PetscInt e,PetscReal *_hmin,PetscReal *_hmax)
{
  DM_TET *tet = (DM_TET*)dm->data;
  const PetscInt nsd = 2;
  PetscInt i;
  PetscReal elcoor[2*3],ds_min,ds_max;
  PetscErrorCode ierr;
  
  *_hmin = PETSC_MIN_REAL;
  *_hmax = PETSC_MAX_REAL;
  
  ds_min = PETSC_MAX_REAL;
  ds_max = PETSC_MIN_REAL;
  
  /* examine single element, examine min/max h */
  {
    PetscReal cx[2];
    
    /* get element coordinates */
    for (i=0; i<3; i++) {
      PetscInt idx;
      
      idx = tet->geometry->element[3*e+i];
      elcoor[2*i+0] = tet->geometry->coords[nsd*idx+0];
      elcoor[2*i+1] = tet->geometry->coords[nsd*idx+1];
    }
    
    /* fetch centroid */
    ierr = DMTetComputeApproxCentroid2d(elcoor,cx);CHKERRQ(ierr);
    
    /* compare vertex lengths relative to centroid */
    for (i=0; i<3; i++) {
      PetscReal dx[2],sep;
      
      dx[0] = PetscAbsReal(elcoor[2*i+0]-cx[0]);
      dx[1] = PetscAbsReal(elcoor[2*i+1]-cx[1]);
      
      sep = PetscSqrtReal( dx[0]*dx[0] + dx[1]*dx[1] );
      
      ds_min = PetscMin(ds_min,sep);
      ds_max = PetscMax(ds_max,sep);
    }
  }
  
  ds_min = 2.0 * ds_min;
  ds_max = 2.0 * ds_max;
  
  *_hmin = ds_min;
  *_hmax = ds_max;
  
  PetscFunctionReturn(0);
}

PetscErrorCode DMTetRefineGeometry_Subdivide(DMTetMesh *gcoarse,DMTetMesh **grefine);
PetscErrorCode DMTetRefineGeometry_Barycenter(DMTetMesh *gcoarse,DMTetMesh **grefine);
PetscErrorCode DMTetRefineGeometry_PowellSabin(DMTetMesh *gcoarse,DMTetMesh **grefine);

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateRefinement"
PetscErrorCode DMTetCreateRefinement(DM dmc,PetscInt ref_type,DM *_dmf)
{
  DM_TET *dmc_tri = (DM_TET*)dmc->data;
  DM_TET *dmf_tri;
  DM             dm;
  MPI_Comm       comm;
  PetscInt       dim;
  PetscMPIInt    commsize;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dmc,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  if (commsize != 1) SETERRQ(comm,PETSC_ERR_SUP,"Refinement can only be performed when comm.size = 1");
  ierr = DMGetDimension(dmc,&dim);CHKERRQ(ierr);
  ierr = DMTetCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,dim);CHKERRQ(ierr);
  ierr = DMTetSetDof(dm,dmc_tri->dof);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(dm,dmc_tri->basis_type);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dm,dmc_tri->basis_order);CHKERRQ(ierr);
  
  dmf_tri = (DM_TET*)dm->data;
  switch (ref_type) {
    case 0:
      ierr = DMTetRefineGeometry_Subdivide(dmc_tri->geometry,&dmf_tri->geometry);CHKERRQ(ierr);
      break;
    case 1:
      ierr = DMTetRefineGeometry_Barycenter(dmc_tri->geometry,&dmf_tri->geometry);CHKERRQ(ierr);
      break;
    case 2:
      ierr = DMTetRefineGeometry_PowellSabin(dmc_tri->geometry,&dmf_tri->geometry);CHKERRQ(ierr);
      break;
      
    default:
      ierr = DMTetRefineGeometry_Subdivide(dmc_tri->geometry,&dmf_tri->geometry);CHKERRQ(ierr);
      break;
  }
  
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);
  
  ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);
  
  ierr = DMViewFromOptions(dm,NULL,"-dm_view");CHKERRQ(ierr);
  
  ierr = DMSetUp(dm);CHKERRQ(ierr);
  
  *_dmf = dm;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCloneGeometry"
PetscErrorCode DMTetCloneGeometry(DM dmc,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dmf)
{
  DM_TET         *dmc_tri = (DM_TET*)dmc->data;
  DM             dm;
  MPI_Comm       comm;
  PetscInt       dim;
  PetscErrorCode ierr;
  
  ierr = PetscObjectGetComm((PetscObject)dmc,&comm);CHKERRQ(ierr);
  ierr = DMGetDimension(dmc,&dim);CHKERRQ(ierr);
  ierr = DMTetCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,dim);CHKERRQ(ierr);

  ierr = DMTetShallowCopyGeometry(dmc,dm);CHKERRQ(ierr);
  
  if ((btype == dmc_tri->basis_type) && (border == dmc_tri->basis_order)) {
    ierr = DMTetShallowCopySpace(dmc,dm);CHKERRQ(ierr);
    ierr = DMTetSetDof(dm,dof);CHKERRQ(ierr);
  } else {
    DM_TET *dmf_tri;
    
    dmf_tri = (DM_TET*)dm->data;
    ierr = _DMTetMeshMPICreate(&dmf_tri->mpi);CHKERRQ(ierr);
    
    ierr = DMTetSetDof(dm,dof);CHKERRQ(ierr);
    ierr = DMTetSetBasisType(dm,btype);CHKERRQ(ierr);
    ierr = DMTetSetBasisOrder(dm,border);CHKERRQ(ierr);
    ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);
  }
  ierr = DMSetUp(dm);CHKERRQ(ierr);

  *_dmf = dm;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetShallowCopyGeometry"
PetscErrorCode DMTetShallowCopyGeometry(DM dmc,DM dm)
{
  DM_TET         *dmc_tri = (DM_TET*)dmc->data;
  DM_TET         *dmf_tri = (DM_TET*)dm->data;
  PetscInt       dim;
  PetscErrorCode ierr;

  if (!dmc_tri->geometry) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Not valid if input DM does not have any geometry defined");
  if (dm->setupcalled) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Not valid if output DM has had DMSetUp() called");
  
  ierr = DMGetDimension(dmc,&dim);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,dim);CHKERRQ(ierr);

  dmf_tri->geometry = dmc_tri->geometry;
  dmc_tri->geometry->refcnt++;

  /* this encodes the coordinates of the function space and the space itself - not geometry */
  //if (dmc_tri->mpi) {
  //  dmf_tri->mpi = dmc_tri->mpi;
  //  dmc_tri->mpi->refcnt++;
  //}
  
  if (dmc_tri->pinfo) {
    dmf_tri->pinfo = dmc_tri->pinfo;
    dmc_tri->pinfo->refcnt++;
  }
  if (dmc_tri->partition) {
    dmf_tri->partition = dmc_tri->partition;
    dmc_tri->partition->refcnt++;
  }

  if (dmc_tri->dmtet_sequential) {
    ierr = PetscObjectReference((PetscObject)dmc_tri->dmtet_sequential);CHKERRQ(ierr);
    dmf_tri->dmtet_sequential = dmc_tri->dmtet_sequential;
  }
  
  /* submesh attached ISs */
  {
    DM parentdm = NULL;
    IS is_seq = NULL, is = NULL;
    
    ierr = PetscObjectQuery((PetscObject)dmc,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    ierr = PetscObjectQuery((PetscObject)dmc,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject*)&is_seq);CHKERRQ(ierr);
    ierr = PetscObjectQuery((PetscObject)dmc,"ParentDMTet2SubDMTetIS",(PetscObject*)&is);CHKERRQ(ierr);

    if (parentdm) {
      ierr = PetscObjectCompose((PetscObject)dm,"ParentDMTet",(PetscObject)parentdm);CHKERRQ(ierr);
    }
    if (is_seq) {
      ierr = PetscObjectCompose((PetscObject)dm,"ParentElementMarkerIS_DMTetSubDMTet",(PetscObject)is_seq);CHKERRQ(ierr);
    }
    if (is) {
      ierr = PetscObjectCompose((PetscObject)dm,"ParentDMTet2SubDMTetIS",(PetscObject)is);CHKERRQ(ierr);
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSharedGeometry"
PetscErrorCode DMTetCreateSharedGeometry(DM dmc,PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dmf)
{
  DM_TET         *dmf_tri;
  MPI_Comm       comm;
  DM             dm;
  PetscErrorCode ierr;
 
  comm = PetscObjectComm((PetscObject)dmc);
 
  ierr = DMTetCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMTetShallowCopyGeometry(dmc,dm);CHKERRQ(ierr);

  dmf_tri = (DM_TET*)dm->data;
  ierr = _DMTetMeshMPICreate(&dmf_tri->mpi);CHKERRQ(ierr);

  ierr = DMTetSetDof(dm,dof);CHKERRQ(ierr);
  ierr = DMTetSetBasisType(dm,btype);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dm,border);CHKERRQ(ierr);
  ierr = DMTetGenerateBasis2d(dm);CHKERRQ(ierr);
  ierr = DMSetUp(dm);CHKERRQ(ierr);
  
  *_dmf = dm;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetShallowCopySpace"
PetscErrorCode DMTetShallowCopySpace(DM dmc,DM dm)
{
  DM_TET         *dmc_tri = (DM_TET*)dmc->data;
  DM_TET         *dmf_tri = (DM_TET*)dm->data;
  PetscErrorCode ierr;
  
  if (!dmc->setupcalled) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Not valid if input DM has not been setup. Call DMSetUp() first");
  if (dm->setupcalled) SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_USER,"Not valid if output DM has had DMSetUp() called");
  
  ierr = DMTetSetBasisType(dm,dmc_tri->basis_type);CHKERRQ(ierr);
  ierr = DMTetSetBasisOrder(dm,dmc_tri->basis_order);CHKERRQ(ierr);

  if (dmc_tri->mpi) {
    dmf_tri->mpi = dmc_tri->mpi;
    dmc_tri->mpi->refcnt++;
  }
  
  dmf_tri->space = dmc_tri->space;
  dmc_tri->space->refcnt++;

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreateSharedSpace"
PetscErrorCode DMTetCreateSharedSpace(DM dmc,PetscInt dof,DM *_dmf)
{
  MPI_Comm       comm;
  DM             dm;
  PetscErrorCode ierr;
  
  comm = PetscObjectComm((PetscObject)dmc);
  
  ierr = DMTetCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMTetShallowCopyGeometry(dmc,dm);CHKERRQ(ierr);
  ierr = DMTetShallowCopySpace(dmc,dm);CHKERRQ(ierr);

  ierr = DMTetSetDof(dm,dof);CHKERRQ(ierr);
  ierr = DMSetUp(dm);CHKERRQ(ierr);

  /* This is incredibly unsafe - do not allow injection between shared objects */
#if 0
  /* do something special to support injection */
  dm->ops->getinjection  = dmc->ops->getinjection;
  {
    DM parentdm = NULL;
    IS is = NULL;
    
    ierr = PetscObjectQuery((PetscObject)dm,"ParentDMTet",(PetscObject*)&parentdm);CHKERRQ(ierr);
    ierr = PetscObjectQuery((PetscObject)dm,"ParentDMTet2SubDMTetIS",(PetscObject*)&is);CHKERRQ(ierr);

    if (parentdm) {
      ierr = PetscObjectCompose((PetscObject)dm,"ParentDMTet",(PetscObject)parentdm);CHKERRQ(ierr);
    }
    if (is) {
      ierr = PetscObjectCompose((PetscObject)dm,"ParentDMTet2SubDMTetIS",(PetscObject)is);CHKERRQ(ierr);
    }
  }
#endif
  *_dmf = dm;
  
  PetscFunctionReturn(0);
}
