

#include <petscdmtet.h>
#include <dmtetdt.h>
#include <dmtetimpl.h>
#include <data_bucket.h>
#include <quadratureimpl.h>
#include <quadrature.h>


#undef __FUNCT__
#define __FUNCT__ "QuadratureCreate"
PetscErrorCode QuadratureCreate(Quadrature *_q)
{
  PetscErrorCode ierr;
  Quadrature q;

  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&q);CHKERRQ(ierr);
  ierr = PetscMemzero(q,sizeof(struct _p_Quadrature));CHKERRQ(ierr);
  q->dim = -1;
  q->setup = PETSC_FALSE;
  q->data = NULL;
  q->nelements = -1;
  q->properties = NULL;
  q->gtype = QUADRATURE_UNDEFINED;
  *_q = q;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureDestroy"
PetscErrorCode QuadratureDestroy(Quadrature *_q)
{
  Quadrature q;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (!_q) PetscFunctionReturn(0);
  if (!*_q) PetscFunctionReturn(0);
  q = *_q;
  
  if (q->q_xi_coor) { ierr = PetscFree(q->q_xi_coor);CHKERRQ(ierr); }
  if (q->q_weight) { ierr = PetscFree(q->q_weight);CHKERRQ(ierr); }
  if (q->properties) {
    DataBucketDestroy(&q->properties);
  }
  ierr = PetscFree(q);CHKERRQ(ierr);
  *_q = NULL;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSetParameters"
PetscErrorCode QuadratureSetParameters(Quadrature q,PetscInt dim,QuadratureGeometryType gtype,QuadratureType qtype,PetscInt order)
{
  PetscFunctionBegin;
  q->dim = dim;
  q->gtype = gtype;
  q->type = qtype;
  q->order = order;
  PetscFunctionReturn(0);
}

/*
 Reference triangle is [-1,1]
*/
#undef __FUNCT__
#define __FUNCT__ "DMTetQuadratureCreate_2d"
PetscErrorCode DMTetQuadratureCreate_2d(DM dm,PetscInt *_nqp,PetscReal **_w,PetscReal **_xi)
{
  PetscErrorCode ierr;
  PetscReal *weights;
  PetscReal *points;
  PetscInt qpoints,order;
  DMTetBasisType basis;
  
  PetscFunctionBegin;
  ierr = DMTetGetBasisType(dm,&basis);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
  qpoints = order + 1;
  if (basis == DMTET_RAVIART_THOMAS) {
    qpoints = order + 2;
  }
  
  ierr = _PetscDTGaussJacobiQuadrature(2,qpoints,-1.0,1.0,&points,&weights);CHKERRQ(ierr);
  
  if (_nqp) {
    *_nqp = qpoints * qpoints;
  }
  
  if (_w) {
    *_w   = weights;
  } else { PetscFree(weights); }
  
  if (_xi) {
    *_xi  = points;
  } else { PetscFree(points); }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetQuadratureRescaleToUnitTriangle"
PetscErrorCode DMTetQuadratureRescaleToUnitTriangle(DM dm,PetscInt nqp,PetscReal w[],PetscReal xi[])
{
  PetscInt p;
  
  PetscFunctionBegin;
  if (xi) {
    for (p=0; p<nqp; p++) {
      xi[2*p+0] = 0.5*(xi[2*p+0] + 1.0);
      xi[2*p+1] = 0.5*(xi[2*p+1] + 1.0);
    }
  }
  if (w) {
    for (p=0; p<nqp; p++) {
      w[p] = 0.25 * w[p];
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSetUp"
PetscErrorCode QuadratureSetUp(Quadrature q)
{
  PetscInt qpoints;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (q->setup) PetscFunctionReturn(0);
  switch (q->dim) {
    case 2:
      switch (q->gtype) {
          
        case QUADRATURE_SURFACE:
          qpoints = q->order + 1;
          ierr = _PetscDTGaussJacobiQuadrature(q->dim-1,qpoints,-1.0,1.0,&q->q_xi_coor,&q->q_weight);CHKERRQ(ierr);
          q->npoints = qpoints;
          break;
          
        case QUADRATURE_VOLUME:
          
          switch (q->type) {
            case GAUSS_LEGENDRE_WARPED_TENSOR:
              qpoints = q->order + 1;
              ierr = _PetscDTGaussJacobiQuadrature(q->dim,qpoints,-1.0,1.0,&q->q_xi_coor,&q->q_weight);CHKERRQ(ierr);
                  q->npoints = qpoints * qpoints;
              break;
            default:
              SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only warped Gauss-Legendre quadrature supported");
              break;
          }
          
          break;
        default:
          SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only surface, volume quadrature supported");
          break;
      }
      break;
      
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only 2D quadrature rules supported");
      break;
  }
  q->setup = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSetUpFromDM"
PetscErrorCode QuadratureSetUpFromDM(Quadrature q,DM dm)
{
  PetscBool istet = PETSC_FALSE;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (istet) {
    PetscInt dim,order,qpoints;
    
    ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
    ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
    ierr = QuadratureSetParameters(q,dim,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,order);CHKERRQ(ierr);
    
    qpoints = order + 1;
    ierr = _PetscDTGaussJacobiQuadrature(dim,qpoints,-1.0,1.0,&q->q_xi_coor,&q->q_weight);CHKERRQ(ierr);
    switch (dim) {
      case 2:
        q->npoints = qpoints * qpoints;
        break;
      case 3:
        q->npoints = qpoints * qpoints * qpoints;
        break;
    }

  } else {
    SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only know how to automate quadrature selection for DMTET");
  }
  q->setup = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSetProperty"
PetscErrorCode QuadratureSetProperty(Quadrature q,PetscInt nelements,const char qfieldname[],PetscInt ncomponents)
{
 
  if (!q->properties) {
    q->nelements = nelements;
    /* Create data bucket */
    DataBucketCreate(&q->properties);

    /* Insert field into data bucket */
    DataBucketRegisterField(q->properties,qfieldname,sizeof(PetscReal)*ncomponents,NULL);
    DataBucketFinalize(q->properties);
    DataBucketSetInitialSizes(q->properties,q->npoints*q->nelements,1);

  } else {
    /* check for conforming sizes */
    if (nelements != q->nelements) {
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Only valid using %D elements",q->nelements);
    }

    DataBucketRegisterField(q->properties,qfieldname,sizeof(PetscReal)*ncomponents,NULL);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureGetProperty"
PetscErrorCode QuadratureGetProperty(Quadrature q,const char qfieldname[],PetscInt *nelements,PetscInt *nqp,PetscInt *ncomponents,PetscReal **data)
{
  DataField gfield;
  PetscReal *coeff;
  size_t    asize;
  BTruth    found;
  
  PetscFunctionBegin;
  if (!q->properties) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"No quadrature point properties have been declared");

  DataBucketQueryDataFieldByName(q->properties,qfieldname,&found);
  if (!found) {
    if (q->nelements == 0) {
      PetscPrintf(PETSC_COMM_SELF,"QuadratureGetProperty: Requested a property however 0 elements have been defined\n");
      if (q->gtype == QUADRATURE_SURFACE) {
        PetscPrintf(PETSC_COMM_SELF,"QuadratureGetProperty: Requested a property defined on the boundary of the domain.\n");
        PetscPrintf(PETSC_COMM_SELF,"QuadratureGetProperty: A non-zero number of boundary elements requires DMTetBFacetSetup() has been called.\n");
      }
    }
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"No quadrature property field named \"%s\" was found",qfieldname);
  }
  DataBucketGetDataFieldByName(q->properties,qfieldname,&gfield);
  DataFieldGetEntries(gfield,(void**)&coeff);
 
  if (nqp) *nqp = q->npoints;
  if (nelements) *nelements = q->nelements;
  if (ncomponents) {
    DataFieldGetAtomicSize(gfield,&asize);
    *ncomponents = asize/sizeof(PetscReal);
  }
  *data = coeff;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureGetRule"
PetscErrorCode QuadratureGetRule(Quadrature q,PetscInt *n,PetscReal *xi[],PetscReal *w[])
{
  PetscFunctionBegin;
  if (n) *n = q->npoints;
  if (xi) *xi = q->q_xi_coor;
  if (w) *w = q->q_weight;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureGetElementValues"
PetscErrorCode QuadratureGetElementValues(Quadrature q,PetscInt e,PetscInt ncomponents,PetscReal *qdata,PetscReal **qdata_e)
{
  *qdata_e = qdata + e * q->npoints * ncomponents;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSetValues"
PetscErrorCode QuadratureSetValues(Quadrature q,PetscInt ncomponents,PetscReal vals[],PetscReal *qdata)
{
  PetscInt e,qp,c;
  PetscReal *qdata_e;
  PetscErrorCode ierr;
  
  for (e=0; e<q->nelements; e++) {
    ierr = QuadratureGetElementValues(q,e,ncomponents,qdata,&qdata_e);CHKERRQ(ierr);
    for (qp=0; qp<q->npoints; qp++) {
      for (c=0; c<ncomponents; c++) {
        qdata_e[ncomponents*qp + c] = vals[c];
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureGetSizes"
PetscErrorCode QuadratureGetSizes(Quadrature q,PetscInt *d,PetscInt *o,PetscInt *n,PetscInt *e)
{
  PetscFunctionBegin;
  if (d) *n = q->dim;
  if (o) *o = q->order;
  if (n) *n = q->npoints;
  if (e) *e = q->nelements;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSetUpFromDM_Facet"
PetscErrorCode QuadratureSetUpFromDM_Facet(Quadrature q,DM dm)
{
  PetscBool istet = PETSC_FALSE;
  PetscErrorCode ierr;
  BFacetList bfacets;
  PetscInt nfacets;
  
  PetscFunctionBegin;
  ierr = PetscObjectTypeCompare((PetscObject)dm,DMTET,&istet);CHKERRQ(ierr);
  if (!istet)     SETERRQ(PetscObjectComm((PetscObject)dm),PETSC_ERR_SUP,"Only know how to automate quadrature selection for DMTET");

  {
    PetscInt dim,order,qpoints;
    
    ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
    ierr = DMTetGetBasisOrder(dm,&order);CHKERRQ(ierr);
    ierr = QuadratureSetParameters(q,dim,QUADRATURE_SURFACE,GAUSS_LEGENDRE_TENSOR,order);CHKERRQ(ierr);
    
    qpoints = order + 1;
    ierr = _PetscDTGaussJacobiQuadrature(dim-1,qpoints,-1.0,1.0,&q->q_xi_coor,&q->q_weight);CHKERRQ(ierr);
    switch (dim) {
      case 2:
        q->npoints = qpoints;
        break;
      case 3:
        q->npoints = qpoints * qpoints;
        break;
    }
  }
  ierr = DMTetGetGeometryBFacetList(dm,&bfacets);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(bfacets,&nfacets,NULL);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(q,nfacets,"coor",2);CHKERRQ(ierr);
  if (nfacets > 0) {
    ierr = QuadratureComputeCoordinates(q,dm);CHKERRQ(ierr);
  }
  
  q->setup = PETSC_TRUE;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureGetGeometryType"
PetscErrorCode QuadratureGetGeometryType(Quadrature q,QuadratureGeometryType *type)
{
  PetscFunctionBegin;
  if (type) *type = q->gtype;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_QuadratureSurface_EvalCoor"
PetscErrorCode _QuadratureSurface_EvalCoor(Quadrature quadrature,DM dm,PetscReal q_coor[])
{
  PetscErrorCode ierr;
  BFacetList f;
  PetscInt nfacets,q,i,e,bf,nb;
  PetscInt nelements,nbasis,npe,npe_face,*element,nqp;
  PetscReal *coords,*el_coor,*el_coor_face;
  PetscReal *xi_face,*xi_s;
  PetscReal **N_face0,**N_face1,**N_face2,*N_s;
  
  PetscFunctionBegin;

  ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&npe_face);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nelements,&nbasis,&element,&npe,NULL,&coords);CHKERRQ(ierr);
  ierr = PetscMalloc1(npe*2,&el_coor);CHKERRQ(ierr);
  ierr = PetscMalloc1(npe_face*2,&el_coor_face);CHKERRQ(ierr);
  ierr = PetscMalloc1(npe_face*2,&xi_face);CHKERRQ(ierr);
  ierr = PetscMalloc1(npe_face,&N_s);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi_s,NULL);CHKERRQ(ierr);

  for (bf=0; bf<3; bf++) {
    PetscInt face_id = bf;
    
    switch (face_id) {
      case 0:
        for (q=0; q<nqp; q++) {
          xi_face[2*q+0] = xi_s[q];
          xi_face[2*q+1] = -1.0;
        }
        ierr = DMTetTabulateBasis(dm,nqp,xi_face,&nb,&N_face0);CHKERRQ(ierr);
        break;
        
      case 1:
        for (q=0; q<nqp; q++) {
          xi_face[2*q+1] = xi_s[q];
          xi_face[2*q+0] = -xi_s[q];
        }
        ierr = DMTetTabulateBasis(dm,nqp,xi_face,&nb,&N_face1);CHKERRQ(ierr);
        break;
        
      case 2:
        for (q=0; q<nqp; q++) {
          xi_face[2*q+0] = -1.0;
          xi_face[2*q+1] = xi_s[q];
        }
        ierr = DMTetTabulateBasis(dm,nqp,xi_face,&nb,&N_face2);CHKERRQ(ierr);
        break;
    }
  }

  
  for (bf=0; bf<nfacets; bf++) {
    PetscInt *eidx,face_id;
    
    e = f->facet_to_element[bf];
    face_id = f->facet_element_face_id[bf];
    
    eidx = &element[npe*e];
    for (i=0; i<npe; i++) {
      el_coor[2*i+0] = coords[2*eidx[i]+0];
      el_coor[2*i+1] = coords[2*eidx[i]+1];
    }

    ierr = BFacetRestrictField(f,face_id,2,el_coor,el_coor_face);CHKERRQ(ierr);
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      PetscReal *N_face_q = NULL;
      
      switch (face_id) {
        case 0:
          N_face_q = N_face0[q];
          break;
        case 1:
          N_face_q = N_face1[q];
          break;
        case 2:
          N_face_q = N_face2[q];
          break;
      }
      ierr = BFacetRestrictField(f,face_id,1,N_face_q,N_s);CHKERRQ(ierr);
      

      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<npe_face; i++) {
        x_qp[0] += N_s[i] * el_coor_face[2*i+0];
        x_qp[1] += N_s[i] * el_coor_face[2*i+1];
      }
      q_coor[bf*nqp*2 + 2*q+0] = x_qp[0];
      q_coor[bf*nqp*2 + 2*q+1] = x_qp[1];
      
    }
    
    
  }

  ierr = PetscFree(N_s);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(N_face0[q]);CHKERRQ(ierr);
    ierr = PetscFree(N_face1[q]);CHKERRQ(ierr);
    ierr = PetscFree(N_face2[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(N_face0);CHKERRQ(ierr);
  ierr = PetscFree(N_face1);CHKERRQ(ierr);
  ierr = PetscFree(N_face2);CHKERRQ(ierr);
  
  ierr = PetscFree(xi_face);CHKERRQ(ierr);
  ierr = PetscFree(el_coor_face);CHKERRQ(ierr);
  ierr = PetscFree(el_coor);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureComputeCoordinates"
PetscErrorCode QuadratureComputeCoordinates(Quadrature q,DM dm)
{
  PetscErrorCode ierr;
  PetscInt nelements,nqp,ncomponents;
  PetscReal *q_coor;
  
  PetscFunctionBegin;
  
  ierr = QuadratureGetProperty(q,"coor",&nelements,&nqp,&ncomponents,&q_coor);CHKERRQ(ierr);
  switch (q->gtype) {
      
    case QUADRATURE_SURFACE:
      ierr = _QuadratureSurface_EvalCoor(q,dm,q_coor);CHKERRQ(ierr);
      break;
      
    default:
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Only supporing the evaluation of surface quadrature points");
      break;
  }
  PetscFunctionReturn(0);
}
