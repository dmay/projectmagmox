
#include <stdio.h>
#include <string.h>
#include <math.h>

void write_poly(double r0,double r1,int nx0,int nx1,double dr0,double dr1)
{
  FILE *fp;
  double dt;
  int i;
  
  fp = fopen("ann.poly","w");
  
  fprintf(fp,"%d 2 0\n",nx0+nx1);

  dt = 2.0 * M_PI / ((double)nx0);
  for (i=0; i<nx0; i++) {
    double pos[2];
    
    pos[0] = r0 * cos(dt*i);
    pos[1] = r0 * sin(dt*i);
    fprintf(fp,"%d %f %f\n",i+1,pos[0],pos[1]);
  }
  dt = 2.0 * M_PI / ((double)nx1);
  for (i=0; i<nx1; i++) {
    double pos[2];
    
    pos[0] = r1 * cos(dt*i);
    pos[1] = r1 * sin(dt*i);
    fprintf(fp,"%d %f %f\n",nx0+i+1,pos[0],pos[1]);
  }

  // segs
  fprintf(fp,"%d\n",nx0+nx1);
  for (i=0; i<nx0; i++) {
    int last = (i+1)%nx0;
    fprintf(fp,"%d %d %d\n",i+1,i+1,last+1);
  }
  for (i=0; i<nx1; i++) {
    int last = (i+1)%nx1;
    fprintf(fp,"%d %d %d\n",nx0+i+1,nx0+i+1,nx0+last+1);
  }

  // holes
  fprintf(fp,"1\n");
  fprintf(fp,"1 0.0 0.0\n");

  
  fclose(fp);
}


int main(void)
{
  double r0,r1,c0,c1,dr0,dr1;
  int nx0,nx1;
  
  r0 = 0.33;
  r1 = 1.0;

  nx0 = 40;
  c0 = 2.0 * M_PI * r0;
  c1 = 2.0 * M_PI * r1;
  
  dr0 = c0 / ((double)nx0);
  
  nx1 = (int)(c1 / dr0);
  nx1++;
  
  dr1 = c1 / ((double)nx1);
  
  write_poly(r0,r1,nx0,nx1,dr0,dr1);
  
  return(0);
}
