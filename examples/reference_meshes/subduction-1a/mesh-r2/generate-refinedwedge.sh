
# Script used to generate wedge.2 data

../../../../external-packages/triangle/trianglecustom -jprq32.0u ../mesh-r1/wedge.1

# Note triangle will write output relative to the location of the input file, so better move it to here
mv ../mesh-r1/wedge.2* .
