[1] To generate a refined mesh, you will need to use a custom build of triangle which understands how to 
    perform local mesh refinement
   * Please follow the instructions in the file
       external-packages/triangle/README.txt
     regarding how to build your custom version of triangle

[2] To generate the mesh, please execute the following script
    > sh generate-refinedwedge.sh 


