![FoaLab Logo](docs/images/foalab-logo.png)

# MagmOx Project

The intention of this software project is to enable the simulation of two-phase dynamics in a range of geological scenarios, for example within a subduction zone. 

We have addressed this requirement by developing a finite element library employing unstructured triangles. This functionality is expressed with the sub-project PETGS (Portable Extensible Toolkit for GeoScience). On top of PETGS, we built subduction specific applications - these apps are referred to as SubMaFEC (Subduction Magma dynamics Finite Element Code). 


## Software requirements ##

1. PETSc version 3.7 (essential)
2. METIS (essential if you wish to run parallel jobs)
3. HDF5 (optional)

## Installation

1. Follow the [installation](docs/GettingStarted.md) instructions for building the core FE library (PETGS). Further information about the major components of the library is described [here](docs/PETGS.md).


## SubMaFEC models
Several subduction related applications exist. Refer [here](subfusc/README.md) for a summary and assistance getting them up and running.


## License

MIT License

Copyright (c) 2018 Dave A. May

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



## Support

We acknowledge financial support from the Alfred P. Sloan Foundation through the Deep Carbon Observatory (DCO) Modeling and Visualisation Forum and through the UK's Natural Environment Research Council through Volatiles Consortium grant NE/M000427/1.
