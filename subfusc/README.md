# SubMaFEC
**Subduction Magma dynamics Finite Element Code**

The applications residing within this directory are developed by members of the FoaLab team

- David Rees Jones
- Meng Tian
- Richard F. Katz
- Dave A. May


## Wedge

* Synposis: Two phase flow within the mantle wedge. Currently only an idealized thermodynamic model is used.
* Please refer to the [project launch page](models/wedge-0/README.md) and [user guide](models/wedge-0/doc/user_guide.md) for further information.


* Primary developers
	* Dave May
	* David Rees Jones 

## Slab
* Synposis: Kinematic model of flow within the subduction channel.

* Primary developers
	* Meng Tian 
