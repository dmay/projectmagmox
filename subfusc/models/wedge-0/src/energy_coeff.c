
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdehelmholtz.h>
#include <quadrature.h>
#include <coefficient.h>
#include <element_container.h>
#include <fe_geometry_utils.h>
#include <tpctx.h>
#include <mec.h>



#undef __FUNCT__
#define __FUNCT__ "EnergySetCoefficientEvaluators_SteadyState"
PetscErrorCode EnergySetCoefficientEvaluators_SteadyState(ECtx ctx,TPCtx tp)
{
  Coefficient k,alpha,g;
  PDE pde;
  PetscErrorCode ierr;
  
  pde = ctx->pde;

  ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(k,tp->kappa);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(alpha,0.0);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadratureEmpty(g);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Energy_BDF1_RHS"
PetscErrorCode Energy_BDF1_RHS(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elX,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff;
  PetscInt nqp;
  PetscReal *xi,*w;
  Vec Xold;
  ECtx data;
  Vec Xold_local;
  const PetscScalar *LA_Xold;
  
  
  PetscFunctionBegin;
  data = (ECtx)user_context;
  dm   = data->dm_e;
  Xold = data->X_prev;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc(sizeof(PetscReal)*nbasis,&elX);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Xold_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,Xold,INSERT_VALUES,Xold_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,Xold,INSERT_VALUES,Xold_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Xold_local,&LA_Xold);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elX[i] = LA_Xold[nidx];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal Xold_q;
      
      /* get x,y coords at each quadrature point */
      Xold_q = 0.0;
      for (i=0; i<nbasis; i++) {
        Xold_q += tab_N[q][i] * elX[i];
      }
      
      coeff[e*nqp + q] = Xold_q;
    }
  }
  ierr = VecRestoreArrayRead(Xold_local,&LA_Xold);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Xold_local);CHKERRQ(ierr);
  
  ierr = PetscFree(elX);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergySetCoefficientEvaluators_TimeDependent"
PetscErrorCode EnergySetCoefficientEvaluators_TimeDependent(ECtx ctx,TPCtx tp)
{
  Coefficient k,alpha,g;
  PDE pde;
  PetscErrorCode ierr;
  
  pde = ctx->pde;
  
  ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(k,ctx->delta_t*tp->kappa);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(alpha,1.0);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadrature(g,Energy_BDF1_RHS,NULL,(void*)ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EnergySource_BDF1_RHS"
PetscErrorCode _EnergySource_BDF1_RHS(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  DM dm;
  PetscInt q,e;
  PetscInt *element;
  PetscInt nqp,nen,npe;
  ECtx data;
  PetscReal *coeff,*coeff_gamma;
  PetscReal dt,L_Cp;
  
  
  PetscFunctionBegin;
  data  = (ECtx)user_context;
  dm    = data->dm_e;
  dt    = data->delta_t;
  L_Cp  = data->L_on_Cp;
  
  if (!eregion) SETERRQ(PetscObjectComm((PetscObject)c),PETSC_ERR_USER,"Element region tag cannot be NULL");
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"gamma",NULL,NULL,NULL,&coeff_gamma);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,NULL,&element,&npe,NULL,NULL);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt region_idx;
    
    region_idx = eregion[e];
    /*
     skip element contribution from melting term
       L/Cp [ dphi/dt - div((1-phi)vs) ]
     if we are not in the wedge (porosity is zero outside the wedge)
    */
    if (region_idx != data->wedge_region_id) continue;
    
    /* note here we sum into an existing value of coeff[] rather than simply assign a value */
    for (q=0; q<nqp; q++) {
      PetscReal gamma_rho = coeff_gamma[e*nqp + q];
      coeff[e*nqp + q] -= dt * L_Cp * ( gamma_rho );
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergySource_BDF1_RHS"
PetscErrorCode EnergySource_BDF1_RHS(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode ierr;
  
  ierr = Energy_BDF1_RHS(c,quadrature,eregion,user_context);CHKERRQ(ierr);
  ierr = _EnergySource_BDF1_RHS(c,quadrature,eregion,user_context);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergySetCoefficientEvaluators_TimeDependentSource"
PetscErrorCode EnergySetCoefficientEvaluators_TimeDependentSource(ECtx ctx,TPCtx tp)
{
  Coefficient k,alpha,g;
  PDE pde;
  PetscErrorCode ierr;
  
  pde = ctx->pde;
  
  ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(k,ctx->delta_t*tp->kappa);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(alpha,1.0);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadrature(g,EnergySource_BDF1_RHS,NULL,(void*)ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
