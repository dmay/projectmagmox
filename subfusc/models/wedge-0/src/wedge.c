
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <petscutils.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <pdestokesdarcy2field.h>
#include <dmbcs.h>
#include <quadrature.h>
#include <coefficient.h>
#include <element_container.h>
#include <paraview_utils.h>
#include <pointwise_mech_coeff.h>
#include <inorms.h>

#include <tpctx.h>
#include <mec.h>
#include <darcyctx.h>



#undef __FUNCT__
#define __FUNCT__ "MechanicsComputeLiquidVelocity_Omega"
PetscErrorCode MechanicsComputeLiquidVelocity_Omega(MCtx mechanics,PetscInt plate_region_id,PetscInt slab_region_id)
{
  Vec vl_wedge_p = NULL;
  Vec vl_omega_p;
  Coefficient bform_mechanics;
  PetscReal *coeff_k,*coeff_phi;
  PetscInt nen,nqp,e,q;
  Quadrature quadrature;
  DM dm_p_omega,dm_p_omega_wedge;
  Mat interp_pk_to_pkp1;
  DM dm_vec_omega_wedge,dm_vec_omega;
  PetscErrorCode ierr;
  
  
  // v_l in omega_wedge (p basis)
  ierr = PDEStokesDarcy2FieldGetCoefficients(mechanics->pde,&bform_mechanics,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(bform_mechanics,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"k*",&nen,&nqp,NULL,&coeff_k);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",&nen,&nqp,NULL,&coeff_phi);CHKERRQ(ierr);

  /* modify stored values of k* to k^star/phi taking care of division by zero */
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      if (coeff_phi[e*nqp + q] < 1.0e-32) {
        printf("  [cell %d][q=%d] chopping k found value %+1.4e\n",e,q,coeff_phi[e*nqp + q]);
        coeff_k[e*nqp + q] = 0.0;
      } else {
        coeff_k[e*nqp + q] = coeff_k[e*nqp + q] / coeff_phi[e*nqp + q];
      }
    }
  }
  
  ierr = PDEStokesDarcy2FieldComputeFluidVelocityP(mechanics->pde,&mechanics->v_l_hat);CHKERRQ(ierr);
  vl_wedge_p = mechanics->v_l_hat;
  dm_p_omega_wedge = mechanics->dm_p_hat;

  
  /* reset stored values of k* to k^star/phi taking care of division by zero */
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) {
      if (coeff_phi[e*nqp + q] > 1.0e-32) {
        coeff_k[e*nqp + q] = coeff_k[e*nqp + q] * coeff_phi[e*nqp + q];
      }
    }
  }
    
  // v_l in omega (p basis)
  dm_p_omega = mechanics->dm_p;
  
  ierr = DMTetCreateSharedSpace(dm_p_omega,2,&dm_vec_omega);CHKERRQ(ierr);
  ierr = DMTetCreateSharedSpace(dm_p_omega_wedge,2,&dm_vec_omega_wedge);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(dm_vec_omega,&vl_omega_p);CHKERRQ(ierr);
  
  ierr = DMTetVecTraverseByRegionId(dm_vec_omega,vl_omega_p,plate_region_id,-6,zero_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(dm_vec_omega,vl_omega_p,slab_region_id,-6,zero_uv,NULL);CHKERRQ(ierr);
  
  /* inject wedge v_l into omega (p basis) */
  //ierr = DMCreateInjection(dm_p_omega,dm_p_omega_wedge,&I_v_p);CHKERRQ(ierr);
  
  /* re-size scatter context */
  {
    IS is_omega,is_wedge;
    IS is_omegaV,is_wedgeV;
    PetscInt L;
    const PetscInt *indices;
    VecScatter scat;
    
    ierr = DMTetCreateInjectionScalarIS(dm_p_omega,dm_p_omega_wedge,&is_omega,&is_wedge);CHKERRQ(ierr);
    ierr = ISGetLocalSize(is_omega,&L);CHKERRQ(ierr);
    ierr = ISGetIndices(is_omega,&indices);CHKERRQ(ierr);
    ierr = ISCreateBlock(PETSC_COMM_SELF,2,L,indices,PETSC_COPY_VALUES,&is_omegaV);CHKERRQ(ierr);

    ierr = ISGetLocalSize(is_wedge,&L);CHKERRQ(ierr);
    ierr = ISGetIndices(is_wedge,&indices);CHKERRQ(ierr);
    ierr = ISCreateBlock(PETSC_COMM_SELF,2,L,indices,PETSC_COPY_VALUES,&is_wedgeV);CHKERRQ(ierr);

    ierr = VecScatterCreate(vl_wedge_p,is_wedgeV,vl_omega_p,is_omegaV,&scat);CHKERRQ(ierr);
    ierr = VecScatterBegin(scat,vl_wedge_p,vl_omega_p,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,vl_wedge_p,vl_omega_p,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
    
    ierr = ISDestroy(&is_omegaV);CHKERRQ(ierr);
    ierr = ISDestroy(&is_wedgeV);CHKERRQ(ierr);
    ierr = ISDestroy(&is_omega);CHKERRQ(ierr);
    ierr = ISDestroy(&is_wedge);CHKERRQ(ierr);
  }
  
  /* interpolate from p basis to v basis (omega) */
  ierr = DMCreateInterpolation(dm_vec_omega,mechanics->dm_v,&interp_pk_to_pkp1,NULL);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(mechanics->v_l_star);CHKERRQ(ierr);
  ierr = MatMult(interp_pk_to_pkp1,vl_omega_p,mechanics->v_l_star);CHKERRQ(ierr);
  
  /* debug output */
  //ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_star_omega_v_",ctx->output_path,tk,field_vl[0]);CHKERRQ(ierr);
  //ierr = DMTetViewFields_VTU(mechanics->dm_v,1,&mechanics->v_l_star,field_vl,prefix);CHKERRQ(ierr);
  
  ierr = DMDestroy(&dm_vec_omega_wedge);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_vec_omega);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_pk_to_pkp1);CHKERRQ(ierr);
  ierr = VecDestroy(&vl_omega_p);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsComputeBulkVelocity_Omega"
PetscErrorCode MechanicsComputeBulkVelocity_Omega(MCtx mechanics,Mat I_v,Vec phi_nodal_v_wedge,PetscInt plate_region_id,PetscInt slab_region_id,PetscBool use_vs)
{
  VecScatter v_scat;
  Vec v_s_hat,p_hat;
  Vec v_tmp_wedge,v_bar_tmp_wedge;
  Vec phi_tmp_wedge;
  PetscErrorCode ierr;

  /* construct v_bar on omega (v basis) */
  
  // inject the boundary conditions for vs in regions 3 and 1
  ierr = VecZeroEntries(mechanics->v_bar);CHKERRQ(ierr);
  
  ierr = DMTetVecTraverseByRegionId(mechanics->dm_v,mechanics->v_bar,plate_region_id,-6,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(mechanics->dm_v,mechanics->v_bar,slab_region_id,-6,slab_uv,NULL);CHKERRQ(ierr);
  
  ierr = VecDuplicate(phi_nodal_v_wedge,&phi_tmp_wedge);CHKERRQ(ierr);
  
  ierr = DMCompositeGetAccess(mechanics->dm_m_hat,mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  ierr = VecDuplicate(v_s_hat,&v_tmp_wedge);CHKERRQ(ierr);
  ierr = VecDuplicate(v_s_hat,&v_bar_tmp_wedge);CHKERRQ(ierr);
  
  // inject vl within just the wedge region
  ierr = MatScatterGetVecScatter(I_v,&v_scat);CHKERRQ(ierr);
  ierr = VecScatterBegin(v_scat, mechanics->v_l_star, v_tmp_wedge, INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd  (v_scat, mechanics->v_l_star, v_tmp_wedge, INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  
  // v_temp = phi.vl
  ierr = VecPointwiseMultBS(v_tmp_wedge,phi_nodal_v_wedge,v_tmp_wedge);CHKERRQ(ierr);
  
  // phi = 1.0 - phi
  ierr = VecCopy(phi_nodal_v_wedge,phi_tmp_wedge);CHKERRQ(ierr);
  ierr = VecScale(phi_tmp_wedge,-1.0);CHKERRQ(ierr);
  ierr = VecShift(phi_tmp_wedge,1.0);CHKERRQ(ierr);
  
  // v_temp = v_temp + phi*v_temp
  ierr = VecPointwiseMultBS(v_bar_tmp_wedge,v_s_hat,phi_tmp_wedge);CHKERRQ(ierr);
  ierr = VecAXPY(v_bar_tmp_wedge,1.0,v_tmp_wedge);CHKERRQ(ierr);
  
  if (use_vs) {
    ierr = VecScatterBegin(v_scat, v_s_hat, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd  (v_scat, v_s_hat, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  } else {
    ierr = VecScatterBegin(v_scat, v_bar_tmp_wedge, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd  (v_scat, v_bar_tmp_wedge, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  }
  
  ierr = DMCompositeRestoreAccess(mechanics->dm_m_hat,mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  
  ierr = VecDestroy(&v_tmp_wedge);CHKERRQ(ierr);
  ierr = VecDestroy(&v_bar_tmp_wedge);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_tmp_wedge);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TCVPSequentialSolve_Compat"
PetscErrorCode TCVPSequentialSolve_Compat(TPCtx ctx,PetscReal time,PetscReal dt,PetscInt tk,PetscBool *pass)
{
  PetscErrorCode ierr;
  DarcyCtx       darcy;
  PetscBool      pass_darcy;
  KSP            ksp;
  KSPConvergedReason reason;

  *pass = PETSC_TRUE;
  darcy = ctx->darcy;
  


  //// SOLVES
  /* o ------------------------------------------------------------------ o */
  //// ENERGY
  /* o ------------------------------------------------------------------ o */
  PetscPrintf(PETSC_COMM_WORLD,"[Energy]\n");
  {
    ierr = EnergySetTimestep(ctx->energy,dt);CHKERRQ(ierr);
    ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_s_bc,ctx->energy->v_s_e);CHKERRQ(ierr);
    
    ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_bar,ctx->energy->v_bar_e);CHKERRQ(ierr);
    ierr = PDEUpdateLocalSolution(ctx->energy->pde);CHKERRQ(ierr);
    
    ierr = VecScale(ctx->energy->v_bar_e,ctx->energy->delta_t);CHKERRQ(ierr);
    ierr = PDESNESSolve(ctx->energy->pde,ctx->energy->snes);CHKERRQ(ierr);
    ierr = VecScale(ctx->energy->v_bar_e,1.0/ctx->energy->delta_t);CHKERRQ(ierr);

    ierr = SNESGetKSP(ctx->energy->snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
    if ( (PetscInt)reason < 0) {
      PetscPrintf(PETSC_COMM_WORLD,"Energy solve failed to converge: KSPConvergedReason %D. See http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPConvergedReason.html to understand value of KSPConvergedReason",(PetscInt)reason);
      *pass = PETSC_FALSE;
      PetscFunctionReturn(0);
    }
    
  }
  
  //// COMPONENTS
  /* o ------------------------------------------------------------------ o */
  PetscPrintf(PETSC_COMM_WORLD,"[Components (cl_bulk,cs_bulk)]\n");

  // coeff
  {
    Coefficient k,alpha;
    PetscReal kappa_s,kappa_l,target_pe = 2.0;
    PetscBool use_adaptive_kappa = PETSC_FALSE;
    
    ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.adaptive.target_pe",&target_pe,NULL);CHKERRQ(ierr);
    ierr = SDAdvection_ComputeCellDiffusion(darcy->pde_cl,darcy->vl,target_pe,&kappa_l);CHKERRQ(ierr);
    ierr = SDAdvection_ComputeCellDiffusion(darcy->pde_cs,darcy->vs,target_pe,&kappa_s);CHKERRQ(ierr);
    
    ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.cl",&darcy->kappa_cl,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.cs",&darcy->kappa_cs,NULL);CHKERRQ(ierr);
    darcy->kappa_phi = darcy->kappa_cl;
    ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.phi",&darcy->kappa_phi,NULL);CHKERRQ(ierr);

    PetscPrintf(PETSC_COMM_WORLD,"Using kappa_{s,l} = %+1.4e %+1.4e\n",darcy->kappa_cs,darcy->kappa_cl);
    PetscPrintf(PETSC_COMM_WORLD,"Target Pe is %+1.2e : use kappa_{s,l} = %+1.4e %+1.4e\n",target_pe,kappa_s,kappa_l);
    PetscPrintf(PETSC_COMM_WORLD,"Target Pe is %+1.2e : use kappa_{s,l} = %+1.4e %+1.4e for scaled dt\n",target_pe,kappa_s/dt,kappa_l/dt);
    
    ierr = PetscOptionsGetBool(NULL,NULL,"-darcy.adv.kappa.adaptive",&use_adaptive_kappa,NULL);CHKERRQ(ierr);
    if (use_adaptive_kappa) {
      
      darcy->kappa_cs  = PetscMax(darcy->kappa_cs,kappa_s);
      darcy->kappa_cl  = PetscMax(darcy->kappa_cl,kappa_l);
      darcy->kappa_phi = PetscMax(darcy->kappa_cl,kappa_l);
      PetscPrintf(PETSC_COMM_WORLD,"  ** Using adaptive kappa_{s,l} = %+1.4e %+1.4e\n",darcy->kappa_cs,darcy->kappa_cl);
    }
    
    ierr = PDEHelmholtzGetCoefficient_k(darcy->pde_cl,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,dt * darcy->kappa_cl);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_alpha(darcy->pde_cl,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,1.0);CHKERRQ(ierr);
    
    
    ierr = PDEHelmholtzGetCoefficient_k(darcy->pde_cs,&k);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(k,dt * darcy->kappa_cs);CHKERRQ(ierr);
    
    ierr = PDEHelmholtzGetCoefficient_alpha(darcy->pde_cs,&alpha);CHKERRQ(ierr);
    ierr = CoefficientSetDomainConstant(alpha,1.0);CHKERRQ(ierr);
  }
  
  /*
  {
    const char *fields[] = { "cs" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"cRF_field_ic_");CHKERRQ(ierr);
    vecs[0] = darcy->cs;
    ierr = DMTetViewFields_VTU(darcy->dm_c_phi_T,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  */

  /* --- cl --- */
  ierr = PDEUpdateLocalSolution(darcy->pde_cl);CHKERRQ(ierr);
  
  // bc
  ierr = SDAdvection_BoundaryConditionSetup_Cl(darcy);CHKERRQ(ierr);
  ierr = BCListInsert(darcy->bc_cl,darcy->cl);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vl,dt);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(darcy->pde_cl,darcy->snes_adv);CHKERRQ(ierr);

  ierr = VecScale(darcy->vl,1.0/dt);CHKERRQ(ierr);

  ierr = SNESGetKSP(darcy->snes_adv,&ksp);CHKERRQ(ierr);
  ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
  if ((PetscInt)reason < 0) {
    PetscPrintf(PETSC_COMM_WORLD,"SDAdv.Liquid solve failed to converge: KSPConvergedReason %D. See http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPConvergedReason.html to understand value of KSPConvergedReason",(PetscInt)reason);
    *pass = PETSC_FALSE;
    PetscFunctionReturn(0);
  }
  
  
  /* --- cs --- */
  ierr = PDEUpdateLocalSolution(darcy->pde_cs);CHKERRQ(ierr);

  // bc
  ierr = SDAdvection_BoundaryConditionSetup_Cs(darcy);CHKERRQ(ierr);
  ierr = BCListInsert(darcy->bc_cs,darcy->cs);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vs,dt);CHKERRQ(ierr);

  ierr = PDESNESSolve(darcy->pde_cs,darcy->snes_adv_cs);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vs,1.0/dt);CHKERRQ(ierr);

  ierr = SNESGetKSP(darcy->snes_adv_cs,&ksp);CHKERRQ(ierr);
  ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
  if ((PetscInt)reason < 0) {
    PetscPrintf(PETSC_COMM_WORLD,"SDAdv.Solid solve failed to converge: KSPConvergedReason %D. See http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPConvergedReason.html to understand value of KSPConvergedReason",(PetscInt)reason);
    *pass = PETSC_FALSE;
    PetscFunctionReturn(0);
  }

  
  ierr = DarcyCtx_PoroDistF(darcy,darcy->dm_c_phi_T,darcy->cl,darcy->cs,darcy->T,darcy->phi);CHKERRQ(ierr);

  //
  // evaluate darcy->phi
  // project darcy->phi darcy->dm(p = pk_t)
  // inject phi
  //
  {
    PetscInt nregions = 1;
    PetscInt regionlist[] = {2};
    DM  dm_e_hat;
    Vec phi_hat = NULL;
    Mat I_v;
    VecScatter scat;
    
    
    ierr = DMTetCreateSubmeshByRegions(ctx->energy->dm_e,nregions,regionlist,&dm_e_hat);CHKERRQ(ierr);
    ierr = DMTetProjectField(darcy->dm_c_phi_T,darcy->phi,PRJTYPE_NATIVE,dm_e_hat,&phi_hat);CHKERRQ(ierr);
  
    ierr = DMCreateInjection(ctx->energy->dm_e,dm_e_hat,&I_v);CHKERRQ(ierr);
    ierr = MatScatterGetVecScatter(I_v,&scat);CHKERRQ(ierr);

    ierr = VecCopy(ctx->energy->phi,ctx->energy->phi_prev);CHKERRQ(ierr);

    ierr = VecZeroEntries(ctx->energy->phi);CHKERRQ(ierr);
    
    ierr = VecScatterBegin(scat,phi_hat,ctx->energy->phi,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,phi_hat,ctx->energy->phi,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);

    //{
    //  const char *fields[] = { "phi" };
    //  ierr = DMTetViewFields_VTU(dm_e_hat,1,&phi_hat,fields,"phi_hat_e_");CHKERRQ(ierr);
    //  ierr = DMTetViewFields_VTU(ctx->energy->dm_e,1,&ctx->energy->phi,fields,"phi_e_");CHKERRQ(ierr);
    //}
    
    ierr = MatDestroy(&I_v);CHKERRQ(ierr);
    ierr = VecDestroy(&phi_hat);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_e_hat);CHKERRQ(ierr);
  }
  
  /* compute gamma */
  ierr = DarcyReconstructGamma(darcy,darcy->kappa_phi,dt);CHKERRQ(ierr);
  

  
  /////////////////////////////////
  //// MECHANICS
  /* o ------------------------------------------------------------------ o */
  PetscPrintf(PETSC_COMM_WORLD,"[Mechanics]\n");
  {
    Coefficient  bform;
    PhiCoeffType phi_c_type_reference;
    Vec          v_s_hat,p_hat;
    
    /* Interpolate temperature onto the tau_m(wedge) */
    ierr = InterpT_FromTauEToTauM(ctx,ctx->energy->X,ctx->mechanics->T,ctx->mechanics->T_hat);CHKERRQ(ierr);
    
    /* evaluate phi (quadrature and nodes) */
    phi_c_type_reference = ctx->phi_c_type;
    ierr = PDEStokesDarcy2FieldGetCoefficients(ctx->mechanics->pde,&bform,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientEvaluate(bform);CHKERRQ(ierr);
    ctx->phi_c_type = PHI_NONE;
    
    /* solve the mechanical problem */
    ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
    ctx->phi_c_type = phi_c_type_reference;
    
    ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_s_bc);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    
    //ierr = MechanicsComputeLiquidVelocity_Omega(ctx->mechanics,ctx->plate_region_id,ctx->slab_region_id);CHKERRQ(ierr);
    ierr = VecZeroEntries(ctx->mechanics->v_l_hat);CHKERRQ(ierr);
  }
  
  //// Darcy
  /* o ------------------------------------------------------------------ o */
  PetscPrintf(PETSC_COMM_WORLD,"[Darcy]\n");
  
  ierr = DarcyCtxSolve(darcy,&pass_darcy);CHKERRQ(ierr);
  if (!pass_darcy) {
    *pass = PETSC_FALSE;
    PetscFunctionReturn(0);
  }
  
  ierr = VecZeroEntries(ctx->mechanics->v_bar);CHKERRQ(ierr);
  ierr = DarcyComputeBulkVelocity_OmegaM(ctx->darcy,ctx->mechanics,ctx->I_v,ctx->plate_region_id,ctx->slab_region_id,PETSC_FALSE);CHKERRQ(ierr);
  /////////////////////////////////
  
  // If all solves for all systems pass - accept the computed melting rate and update state variables
  {
    Coefficient g;
    Quadrature  quadrature;
    
    ierr = PDEHelmholtzGetCoefficient_g(ctx->energy->pde,&g);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureEvaluator_MeltingRate_Darcy(quadrature,ctx->energy->dm_e,ctx);CHKERRQ(ierr);
  }
  

  
  //// o ------------------------------------------------------------------ o ////
  //// o     STATE UPDATES                                                  o ////
  //// o ------------------------------------------------------------------ o ////
  //// energy
  ierr = VecCopy(ctx->energy->X,ctx->energy->X_prev);CHKERRQ(ierr); /* x --> x_old */

  //// components
  {
    Vec X_prev;
    
    X_prev = NULL;
    ierr = PetscObjectQuery((PetscObject)darcy->pde_cl,"X_k_scalar",(PetscObject*)&X_prev);CHKERRQ(ierr);
    if (!X_prev) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state from pde_cl");
    ierr = VecCopy(darcy->cl,X_prev);CHKERRQ(ierr); /* x --> x_old */
    
    X_prev = NULL;
    ierr = PetscObjectQuery((PetscObject)darcy->pde_cs,"X_k_scalar",(PetscObject*)&X_prev);CHKERRQ(ierr);
    if (!X_prev) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state from pde_cs");
    ierr = VecCopy(darcy->cs,X_prev);CHKERRQ(ierr); /* x --> x_old */
  }
  
  //// darcy->porosity
  ierr = VecCopy(darcy->phi,darcy->phi0);CHKERRQ(ierr); /* x --> x_old */
  
  

  /*
  // view
  {
    const char *fields[] = { "cs" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"cRF_field_step%D_",step);CHKERRQ(ierr);
    vecs[0] = darcy->cs;
    ierr = DMTetViewFields_VTU(darcy->dm_c_phi_T,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  */

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "execute_wedge_model"
PetscErrorCode execute_wedge_model(void)
{
  PetscErrorCode ierr;
  TPCtx ctx;
  PetscInt tk;
  PetscReal dt,cfl_liquid,dt_cfl,dt_user,dt_adaptive,dt_min,dt_min_kyr;
  char h5filename[PETSC_MAX_PATH_LEN];
  char h5filename_e[PETSC_MAX_PATH_LEN];
  char h5filename_d[PETSC_MAX_PATH_LEN];
  PetscReal value_geo_unit;
  PetscReal eta0,zeta0;
  PetscBool found;
  PetscInt nt,output_freq;
  const PetscReal si2kyr = 1.0e-3 / (365.0 * 24.0 * 60.0 * 60.0);
  PetscBool skip_continuation = PETSC_FALSE;
  PetscReal t_kyr,t_nd;
  char output_path_stage[PETSC_MAX_PATH_LEN];
  char meshfile[PETSC_MAX_PATH_LEN];
  size_t meshfile_len = PETSC_MAX_PATH_LEN-1;
  char limiter_type[PETSC_MAX_PATH_LEN];
  PetscBool step_passed;
  
  
  ierr = TPContextCreate(&ctx);CHKERRQ(ierr);

  ierr = PetscSNPrintf(meshfile,PETSC_MAX_PATH_LEN-1,"meshes/examples/ref.1");CHKERRQ(ierr);
  ierr = PetscOptionsGetString(NULL,NULL,"-subfusc_mesh_file",meshfile,meshfile_len,NULL);CHKERRQ(ierr);
  ierr = TPSetUp_Default(ctx,meshfile,4,4);CHKERRQ(ierr);
  
  ctx->slab_region_id = 1;
  ctx->wedge_region_id = 2;
  ctx->plate_region_id = 3;
  ctx->energy->wedge_region_id = 2;

  /* constant material properties */
  ierr = TPCtxSetDimensionalScales_ELV(ctx,ctx->E,ctx->L,ctx->V);CHKERRQ(ierr);
  eta0  = 1.0e21;
  zeta0 = 10.0 * 1.0e21;
  ctx->kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  ctx->rho_s  = 3300.0;
  ctx->rho_l  = 2800.0;
  ctx->g[0] = 0.0;
  ctx->g[1] = 9.81;
  ctx->rho_plate = 3100.0;
  ctx->energy->L_on_Cp = 400.0;
  ctx->Vdarcy = 0.0;
  ctx->gaussian_depth = 120.0e3;
  ctx->gaussian_width = 20.0e3;


  ctx->mechanics->plate_velocity = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0);
  ctx->mechanics->slab_decoupling_depth    = 80.0e3;
  ctx->mechanics->slab_decoupling_distance = 5.0e3;
  
  ierr = PetscOptionsGetReal(NULL,"mechanics.slab_bc.","-plate_velocity",&ctx->mechanics->plate_velocity,NULL);CHKERRQ(ierr);
  ierr = PetscOptionGetReal_GeoVel("mechanics.slab_bc.","-plate_velocity",&value_geo_unit,&ctx->mechanics->plate_velocity,&found);CHKERRQ(ierr);
  
  ierr = PetscOptionsGetReal(NULL,NULL,"-kappa",&ctx->kappa,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-rho_s",&ctx->rho_s,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-rho_l",&ctx->rho_l,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-rho_plate",&ctx->rho_plate,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-L_on_Cp",&ctx->energy->L_on_Cp,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-output_hdf",&ctx->output_hdf,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-output_hdf",&ctx->energy->output_hdf,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-output_hdf",&ctx->darcy->output_hdf,NULL);CHKERRQ(ierr);

  ierr = TPCtxApplyScaling(ctx);CHKERRQ(ierr);
  
  /* open hdf files if desired */
  if (ctx->output_hdf) {
    if (ctx->output_path) {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"%s/wedge.h5",ctx->output_path);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_e,PETSC_MAX_PATH_LEN-1,"%s/energy.h5",ctx->output_path);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_d,PETSC_MAX_PATH_LEN-1,"%s/darcy.h5",ctx->output_path);CHKERRQ(ierr);
      
    } else {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"wedge.h5");CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_e,PETSC_MAX_PATH_LEN-1,"energy.h5",ctx->output_path);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_d,PETSC_MAX_PATH_LEN-1,"%s/darcy.h5",ctx->output_path);CHKERRQ(ierr);

    }
    ierr = DMTetView_HDF(ctx->mechanics->dm_p_hat,h5filename);CHKERRQ(ierr);
    ierr = DMTetView_HDF(ctx->energy->dm_e,h5filename_e);CHKERRQ(ierr);
    ierr = DMTetView_HDF(ctx->darcy->dm_p,h5filename_d);CHKERRQ(ierr);
  }
  
  /* set boundary conditions */
  ierr = MechanicsSetDirichletBC(ctx->mechanics,ctx);CHKERRQ(ierr);
  ierr = EnergySetDirichletBC(ctx->energy,ctx);CHKERRQ(ierr);

  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_s_bc,ctx->plate_region_id,-6,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_s_bc,ctx->slab_region_id,-6,slab_uv,NULL);CHKERRQ(ierr);
  
  /* o --------------------------------------------------------------- o */
  /* Initial condition defined by an incompressible steady state problem */
  ctx->eta_c_type  = ETA_CONST;
  ctx->zeta_c_type = ZETA_CONST;
  ctx->khat_c_type = K_CONST;
  ctx->phi_c_type  = PHI_CONST;
  ctx->phi = 0.0;
  
  /* explicitly require k = 0 in mechanics */
  ierr = MechanicsSetCoefficientEvaluators(ctx->mechanics,ctx);CHKERRQ(ierr);
  ierr = EnergySetCoefficientEvaluators_SteadyState(ctx->energy,ctx);CHKERRQ(ierr);

  /* explicitly enforce: constant viscosity; k* = 0 */
  ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.Constant",NULL);CHKERRQ(ierr);
  ierr = EtaConstantSetParams(ctx->mechanics->coeff_container->pwp_eta,eta0);CHKERRQ(ierr);
  
  ierr = MechanicsCoeffContainerSetupZeta(ctx->mechanics->coeff_container,"zeta.Constant",NULL);CHKERRQ(ierr);
  ierr = ZetaConstantSetParams(ctx->mechanics->coeff_container->pwp_zeta,zeta0);CHKERRQ(ierr);
  
  ierr = MechanicsCoeffContainerSetupEffectivePermability(ctx->mechanics->coeff_container,"k.Constant",NULL);CHKERRQ(ierr);
  ierr = kConstantSetParams(ctx->mechanics->coeff_container->pwp_k_on_mu,0.0);CHKERRQ(ierr);
  
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_eta,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_zeta,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_k_on_mu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  /* solve the mechanical problem */
  ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
  
  /* in the abscence of porosity, \bar{v} = v_s */
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,ctx->plate_region_id,-6,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,ctx->slab_region_id,-6,slab_uv,NULL);CHKERRQ(ierr);
  {
    Vec v_s_hat,p_hat;
    
    /* Note here that v_bar = v_s since porosity was forced to be zero for the steady state IC */
    ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_bar);CHKERRQ(ierr);
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_s_bc);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  }
  ierr = VecZeroEntries(ctx->mechanics->v_l_star);CHKERRQ(ierr);

  /* interpolate solid velocity from mechanical mesh onto energy mesh */
  ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_bar,ctx->energy->v_s_e);CHKERRQ(ierr);
  /* in the abscence of porosity, \bar{v} = v_s */
  ierr = VecCopy(ctx->energy->v_s_e,ctx->energy->v_bar_e);CHKERRQ(ierr);
  
  /* solve thermal problem */
  ierr = EnergySetTimestep(ctx->energy,1.0);CHKERRQ(ierr);
  ierr = PDESNESSolve(ctx->energy->pde,ctx->energy->snes);CHKERRQ(ierr);

  /* write iso-viscous initial condition into a separate directory */
  ierr = PetscSNPrintf(output_path_stage,PETSC_MAX_PATH_LEN-1,"%s/ic-isoviscous",ctx->output_path);CHKERRQ(ierr);
  ierr = projCreateDirectory(output_path_stage);CHKERRQ(ierr);
  if (ctx->output_hdf) {
    if (ctx->output_path) {
      ierr = PetscSNPrintf(h5filename_e,PETSC_MAX_PATH_LEN-1,"%s/energy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_d,PETSC_MAX_PATH_LEN-1,"%s/darcy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"%s/wedge.h5",output_path_stage);CHKERRQ(ierr);
      
    } else {
      ierr = PetscSNPrintf(h5filename_e,PETSC_MAX_PATH_LEN-1,"energy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_d,PETSC_MAX_PATH_LEN-1,"darcy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"wedge.h5",output_path_stage);CHKERRQ(ierr);
    }
    ierr = DMTetView_HDF(ctx->energy->dm_e,h5filename_e);CHKERRQ(ierr);
    ierr = DMTetView_HDF(ctx->darcy->dm_p,h5filename_d);CHKERRQ(ierr);
    ierr = DMTetView_HDF(ctx->mechanics->dm_p_hat,h5filename);CHKERRQ(ierr);
  }
  ierr = MechanicsView(ctx->mechanics,output_path_stage,0);CHKERRQ(ierr);
  ierr = EnergyView(ctx->energy,output_path_stage,0);CHKERRQ(ierr);
  ierr = TPView(ctx,0.0,0,output_path_stage);CHKERRQ(ierr);
  ierr = SubFUScGlobalFieldMetricMonitor(ctx,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  {
    PetscInt nk;
    Vec v_s_hat,p_hat;
    PetscReal vtol = 1.0e-4,ttol=1.0e-1;
    Vec vp_last,t_last,delta_vp,delta_t;
    PetscReal norm_dv,norm_dp,norm_dt;
    PetscInt max_continuation_steps = 4;
    PetscReal user_eta_min,user_eta_max;
    
    ierr = PetscOptionsGetInt(NULL,NULL,"-subfusc.max_continuation_steps",&max_continuation_steps,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetEnum(NULL,NULL,"-mechanics.eta.evaluator",EtaCoeffTypeNames,(PetscEnum*)&ctx->eta_c_type,NULL);CHKERRQ(ierr);

    ierr = PetscSNPrintf(limiter_type,PETSC_MAX_PATH_LEN-1,"%s","limiter.MinMax");CHKERRQ(ierr);
    ierr = PetscOptionsGetString(NULL,NULL,"-mechanics.eta.limiter",limiter_type,PETSC_MAX_PATH_LEN,&found);CHKERRQ(ierr);
    
    switch (ctx->eta_c_type) {
      case ETA_CONST:
        ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.Constant",limiter_type);CHKERRQ(ierr);
        ierr = EtaConstantSetParams(ctx->mechanics->coeff_container->pwp_eta,eta0);CHKERRQ(ierr);
        
        ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
        skip_continuation = PETSC_TRUE;
        break;
      case ETA_DIFF:
        ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.DiffusionCreep.PvKPEPI2008",limiter_type);CHKERRQ(ierr);
        ierr = EtaDiffusionCreepSetParams(ctx->mechanics->coeff_container->pwp_eta,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
        
        ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
        break;
      case ETA_DISC:
        ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.DislocationCreep.PvKPEPI2008",limiter_type);CHKERRQ(ierr);
        ierr = EtaDislocationCreepSetParams(ctx->mechanics->coeff_container->pwp_eta,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
        ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
        break;
      case ETA_COMPOSITE:
        ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.CompositeDiffDisl.PvKPEPI2008",limiter_type);CHKERRQ(ierr);
        ierr = EtaCompositeDiffDislCreepSetParams(ctx->mechanics->coeff_container->pwp_eta,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
        ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
        break;
      default:
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown shear viscosity evaluator requested");
        break;
    }
    ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_eta);CHKERRQ(ierr);
    ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_eta_limiter);CHKERRQ(ierr);
    ierr = LimiterMinMaxGetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,&user_eta_min,&user_eta_max);CHKERRQ(ierr);
    
    ierr = MechanicsCoeffContainerSetupZeta(ctx->mechanics->coeff_container,"zeta.Constant","limiter.MinMax");CHKERRQ(ierr);
    ierr = ZetaConstantSetParams(ctx->mechanics->coeff_container->pwp_zeta,0.0);CHKERRQ(ierr);
    ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_zeta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);

    ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_zeta);CHKERRQ(ierr);
    ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_zeta_limiter);CHKERRQ(ierr);
    
    /* explicitly enforce: k* = 0 */
    ierr = MechanicsCoeffContainerSetupEffectivePermability(ctx->mechanics->coeff_container,"k.Constant",NULL);CHKERRQ(ierr);
    ierr = kConstantSetParams(ctx->mechanics->coeff_container->pwp_k_on_mu,0.0);CHKERRQ(ierr);
    
    ctx->phi_c_type = PHI_CONST;
    ctx->phi = 0.0;

    ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_eta,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_eta_limiter,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_zeta,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_zeta_limiter,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_k_on_mu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    
    ierr = DMCreateGlobalVector(ctx->mechanics->dm_m_hat,&vp_last);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(ctx->energy->dm_e,&t_last);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(ctx->mechanics->dm_m_hat,&delta_vp);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(ctx->energy->dm_e,&delta_t);CHKERRQ(ierr);
    ierr = EnergySetTimestep(ctx->energy,1.0);CHKERRQ(ierr);

    if (skip_continuation) {
      max_continuation_steps = 0;
    }
    for (nk=0; nk<max_continuation_steps; nk++) {
      PWPhysics limiter = ctx->mechanics->coeff_container->pwp_eta_limiter;
      PetscInt nstage = 1;
      PetscReal eta_min = 1.0e17,eta_max = 1.0e26;
      PetscInt MU,MP,MT;
      
      if (nk < nstage) {
        eta_min = 1.0e17;
        eta_max = PetscMin(1.0e22,user_eta_max);
      }
      if (nk >= nstage && nk < 2*nstage) {
        eta_min = 1.0e17;
        eta_max = PetscMin(1.0e23,user_eta_max);
      }
      if (nk >= 2*nstage && nk < 3*nstage) {
        eta_min = 1.0e17;
        eta_max = 1.0e24;
      }
      if (nk >= 3*nstage && nk < 4*nstage) {
        eta_min = 1.0e17;
        eta_max = PetscMin(1.0e25,user_eta_max);
      }
      if (nk >= 4*nstage) {
        eta_min = 1.0e17;
        eta_max = PetscMin(1.0e26,user_eta_max);
      }
      if (limiter->use_si) {
        ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,eta_min,eta_max);CHKERRQ(ierr);
      } else {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"SubFUSc only supports flows defined in SI units");
      }
      
      PetscPrintf(PETSC_COMM_WORLD,"***************************************\n");
      PetscPrintf(PETSC_COMM_WORLD,"*************************************** Continuation step %D : eta range[%+1.2e,%+1.2e]\n",nk,eta_min,eta_max);
      PetscPrintf(PETSC_COMM_WORLD,"***************************************\n");
      
      ierr = VecCopy(ctx->mechanics->X_hat,vp_last);CHKERRQ(ierr);
      ierr = VecCopy(ctx->energy->X,t_last);CHKERRQ(ierr);
      
      /* Interpolate temperature onto the tau_m(wedge) */
      ierr = InterpT_FromTauEToTauM(ctx,ctx->energy->X,ctx->mechanics->T,ctx->mechanics->T_hat);CHKERRQ(ierr);
      
      /* solve the mechanical problem */
      ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
      
      /* in the abscence of porosity, \bar{v} = v_s */
      ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,ctx->plate_region_id,-6,plate_uv,NULL);CHKERRQ(ierr);
      ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,ctx->slab_region_id,-6,slab_uv,NULL);CHKERRQ(ierr);

      /* Note here that v_bar = v_s since porosity was forced to be zero for the steady state IC */
      ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
      ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_bar);CHKERRQ(ierr);
      ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_s_bc);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
      
      /* interpolate solid velocity from mechanical mesh onto energy mesh */
      ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_bar,ctx->energy->v_s_e);CHKERRQ(ierr);
      /* in the abscence of porosity, \bar{v} = v_s */
      ierr = VecCopy(ctx->energy->v_s_e,ctx->energy->v_bar_e);CHKERRQ(ierr);
      
      /* solve thermal problem */
      ierr = EnergySetDirichletBC(ctx->energy,ctx);CHKERRQ(ierr);
      ierr = PDESNESSolve(ctx->energy->pde,ctx->energy->snes);CHKERRQ(ierr);

      /* debug viz for stage solves */
      /*
      ierr = MechanicsView(ctx->mechanics,NULL,nk);CHKERRQ(ierr);
      ierr = EnergyView(ctx->energy,NULL,nk);CHKERRQ(ierr);
      */
      
      /* compute consistent residuals for Fu,Fp,FT */
      ierr = InterpT_FromTauEToTauM(ctx,ctx->energy->X,ctx->mechanics->T,ctx->mechanics->T_hat);CHKERRQ(ierr);

      ierr = SNESComputeFunction(ctx->mechanics->snes,ctx->mechanics->X_hat,ctx->mechanics->F_hat);CHKERRQ(ierr);
      ierr = SNESComputeFunction(ctx->energy->snes,ctx->energy->X,ctx->energy->F);CHKERRQ(ierr);

      ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->F_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
      ierr = VecNorm(v_s_hat,NORM_2,&norm_dv);CHKERRQ(ierr);
      ierr = VecGetSize(v_s_hat,&MU);CHKERRQ(ierr);
      ierr = VecNorm(p_hat,NORM_2,&norm_dp);CHKERRQ(ierr);
      ierr = VecGetSize(p_hat,&MP);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->F_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);

      ierr = VecNorm(ctx->energy->F,NORM_2,&norm_dt);CHKERRQ(ierr);
      ierr = VecGetSize(ctx->energy->F,&MT);CHKERRQ(ierr);
      
      PetscPrintf(PETSC_COMM_WORLD,"    step %D : norm(Fu)_2   %1.4e : norm(Fp)_2   %1.4e : norm(FT)_2   %1.4e\n",nk,norm_dv,norm_dp,norm_dt);
      PetscPrintf(PETSC_COMM_WORLD,"    step %D : norm(Fu)_2*  %1.4e : norm(Fp)_2*  %1.4e : norm(FT)_2*  %1.4e\n",nk,norm_dv/PetscSqrtReal(MU),norm_dp/PetscSqrtReal(MP),norm_dt/PetscSqrtReal(MT));
      
      /* update deltas and compute infinity norms */
      ierr = VecCopy(ctx->mechanics->X_hat,delta_vp);CHKERRQ(ierr);
      ierr = VecCopy(ctx->energy->X,delta_t);CHKERRQ(ierr);
      
      ierr = VecAXPY(delta_vp,-1.0,vp_last);CHKERRQ(ierr);
      ierr = VecAXPY(delta_t,-1.0,t_last);CHKERRQ(ierr);
      
      ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,delta_vp,&v_s_hat,&p_hat);CHKERRQ(ierr);
      ierr = VecNorm(v_s_hat,NORM_INFINITY,&norm_dv);CHKERRQ(ierr);
      ierr = VecNorm(p_hat,NORM_INFINITY,&norm_dp);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,delta_vp,&v_s_hat,&p_hat);CHKERRQ(ierr);
      ierr = VecNorm(delta_t,NORM_INFINITY,&norm_dt);CHKERRQ(ierr);
      
      PetscPrintf(PETSC_COMM_WORLD,"    step %D : norm(du)_inf %1.4e : norm(dp)_inf %1.4e : norm(dT)_inf %1.4e\n",nk,norm_dv,norm_dp,norm_dt);
      {
        if (norm_dt < ttol) {
          PetscPrintf(PETSC_COMM_WORLD,"    [Converged] gracefully terminating as max point-wise difference in T < %1.2e\n",ttol);
          break;
        }
        if (norm_dv < vtol) {
          PetscPrintf(PETSC_COMM_WORLD,"    [Converged] gracefully terminating as max point-wise difference in V < %1.2e\n",vtol);
          break;
        }
      }
    }
    ierr = VecDestroy(&vp_last);CHKERRQ(ierr);
    ierr = VecDestroy(&t_last);CHKERRQ(ierr);
    ierr = VecDestroy(&delta_vp);CHKERRQ(ierr);
    ierr = VecDestroy(&delta_t);CHKERRQ(ierr);
  }

  /* write initial condition into a separate directory */
  ierr = PetscSNPrintf(output_path_stage,PETSC_MAX_PATH_LEN-1,"%s/ic",ctx->output_path);CHKERRQ(ierr);
  ierr = projCreateDirectory(output_path_stage);CHKERRQ(ierr);
  if (ctx->output_hdf) {
    if (ctx->output_path) {
      ierr = PetscSNPrintf(h5filename_e,PETSC_MAX_PATH_LEN-1,"%s/energy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_d,PETSC_MAX_PATH_LEN-1,"%s/darcy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"%s/wedge.h5",output_path_stage);CHKERRQ(ierr);
      
    } else {
      ierr = PetscSNPrintf(h5filename_e,PETSC_MAX_PATH_LEN-1,"energy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename_d,PETSC_MAX_PATH_LEN-1,"darcy.h5",output_path_stage);CHKERRQ(ierr);
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"wedge.h5",output_path_stage);CHKERRQ(ierr);
    }
    ierr = DMTetView_HDF(ctx->energy->dm_e,h5filename_e);CHKERRQ(ierr);
    ierr = DMTetView_HDF(ctx->darcy->dm_p,h5filename_d);CHKERRQ(ierr);
    ierr = DMTetView_HDF(ctx->mechanics->dm_p_hat,h5filename);CHKERRQ(ierr);
  }

  /* group the viewers */
  ierr = MechanicsView(ctx->mechanics,output_path_stage,0);CHKERRQ(ierr);
  ierr = EnergyView(ctx->energy,output_path_stage,0);CHKERRQ(ierr);
  ierr = TPView(ctx,0.0,0,output_path_stage);CHKERRQ(ierr);

  ierr = SubFUScGlobalFieldMetricMonitor(ctx,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  /* Configure time dependent problem */
  ierr = PetscOptionsGetEnum(NULL,NULL,"-mechanics.eta.evaluator",EtaCoeffTypeNames,(PetscEnum*)&ctx->eta_c_type,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetEnum(NULL,NULL,"-mechanics.zeta.evaluator",ZetaCoeffTypeNames,(PetscEnum*)&ctx->zeta_c_type,NULL);CHKERRQ(ierr);
  ctx->khat_c_type = K_PHI_DEP;
  ierr = PetscOptionsGetEnum(NULL,NULL,"-mechanics.k_on_mu.evaluator",KCoeffTypeNames,(PetscEnum*)&ctx->khat_c_type,NULL);CHKERRQ(ierr);
  ctx->phi_c_type = PHI_DISTF;
  ierr = PetscOptionsGetEnum(NULL,NULL,"-mechanics.phi.evaluator",PhiCoeffTypeNames,(PetscEnum*)&ctx->phi_c_type,NULL);CHKERRQ(ierr);
  
  ierr = PetscSNPrintf(limiter_type,PETSC_MAX_PATH_LEN-1,"%s","limiter.MinMax");CHKERRQ(ierr);
  ierr = PetscOptionsGetString(NULL,NULL,"-mechanics.eta.limiter",limiter_type,PETSC_MAX_PATH_LEN,&found);CHKERRQ(ierr);

  switch (ctx->eta_c_type) {
      
    case ETA_CONST:
      ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.Constant",limiter_type);CHKERRQ(ierr);
      ierr = EtaConstantSetParams(ctx->mechanics->coeff_container->pwp_eta,eta0);CHKERRQ(ierr);
      
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
      break;
    case ETA_DIFF:
      ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.DiffusionCreep.PvKPEPI2008",limiter_type);CHKERRQ(ierr);
      ierr = EtaDiffusionCreepSetParams(ctx->mechanics->coeff_container->pwp_eta,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
      break;
    case ETA_DISC:
      ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.DislocationCreep.PvKPEPI2008",limiter_type);CHKERRQ(ierr);
      ierr = EtaDislocationCreepSetParams(ctx->mechanics->coeff_container->pwp_eta,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      break;
    case ETA_COMPOSITE:
      ierr = MechanicsCoeffContainerSetupEta(ctx->mechanics->coeff_container,"eta.CompositeDiffDisl.PvKPEPI2008",limiter_type);CHKERRQ(ierr);
      ierr = EtaCompositeDiffDislCreepSetParams(ctx->mechanics->coeff_container->pwp_eta,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_eta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown shear viscosity evaluator requested");
      break;
  }
  ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_eta);CHKERRQ(ierr);
  ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_eta_limiter);CHKERRQ(ierr);

#if 0
  {
    PWPhysics limiter_composite;
    PWPhysics limiter_minmax = ctx->mechanics->coeff_container->pwp_eta_limiter;
    PWPhysics limiter_radial;
    PetscReal origin[2];
    PetscBool use_si;
    const PetscReal *buffer;
    
    ierr = PWPhysicsGetUseSI(ctx->mechanics->coeff_container->pwp_eta_limiter,&use_si);CHKERRQ(ierr);
    ierr = MechanicsCoeffContainerGetValidCellBuffer(ctx->mechanics->coeff_container,MCBUFFER_COOR,use_si,&buffer);CHKERRQ(ierr);

    ierr = LimiterRadialCreate(PETSC_TRUE,0,"limiter",&limiter_radial);CHKERRQ(ierr);
    ierr = PWPhysicsSetAuxiliaryDataBuffer(limiter_radial,0,(PetscReal*)buffer);CHKERRQ(ierr);
    origin[0] =  0.25 * ctx->L;
    origin[1] = -0.25 * ctx->L;
    ierr = LimiterRadialSetParams(limiter_radial,"shearViscosity",10.0,1.0e-3,0.3 * ctx->L,origin);CHKERRQ(ierr);
    ierr = PWPhysicsSetFromOptions(limiter_radial);CHKERRQ(ierr);

    ierr = LimiterCompositeCreate(use_si,0,"eta::wedge",&limiter_composite);CHKERRQ(ierr);
    ierr = LimiterCompositeSetLaw(limiter_composite,limiter_minmax);CHKERRQ(ierr);
    ierr = LimiterCompositeSetLaw(limiter_composite,limiter_radial);CHKERRQ(ierr);
    
    ctx->mechanics->coeff_container->pwp_eta_limiter = limiter_composite;
  }
#endif
  
  ierr = PetscSNPrintf(limiter_type,PETSC_MAX_PATH_LEN-1,"%s","limiter.MinMax");CHKERRQ(ierr);
  ierr = PetscOptionsGetString(NULL,NULL,"-mechanics.zeta.limiter",limiter_type,PETSC_MAX_PATH_LEN,&found);CHKERRQ(ierr);

  switch (ctx->zeta_c_type) {
    case ZETA_CONST:
      ierr = MechanicsCoeffContainerSetupZeta(ctx->mechanics->coeff_container,"zeta.Constant",limiter_type);CHKERRQ(ierr);
      ierr = ZetaConstantSetParams(ctx->mechanics->coeff_container->pwp_zeta,zeta0);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_zeta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
      break;
    case ZETA_RATIO:
      ierr = MechanicsCoeffContainerSetupZeta(ctx->mechanics->coeff_container,"zeta.Ratio",limiter_type);CHKERRQ(ierr);
      ierr = ZetaRatioSetParams(ctx->mechanics->coeff_container->pwp_zeta,100.0,ctx->mechanics->coeff_container->pwp_eta);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_zeta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
      break;
    case ZETA_DIFF:
      ierr = MechanicsCoeffContainerSetupZeta(ctx->mechanics->coeff_container,"zeta.DiffusionCreep",limiter_type);CHKERRQ(ierr);
      ierr = ZetaDiffusionCreepSetParams(ctx->mechanics->coeff_container->pwp_zeta,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_zeta_limiter,NULL,1.0e17,1.0e26);CHKERRQ(ierr);
      break;
    case ZETA_DISC:
      ierr = MechanicsCoeffContainerSetupZeta(ctx->mechanics->coeff_container,"zeta.DislocationCreep",limiter_type);CHKERRQ(ierr);
      ierr = ZetaDislocationCreepSetParams(ctx->mechanics->coeff_container->pwp_zeta,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown bulk viscosity evaluator requested");
      break;
  }

#if 0
  {
    PWPhysics limiter_composite;
    PWPhysics limiter_minmax = ctx->mechanics->coeff_container->pwp_zeta_limiter;
    PWPhysics limiter_radial;
    PetscReal origin[2];
    PetscBool use_si;
    const PetscReal *buffer;
    
    ierr = PWPhysicsGetUseSI(ctx->mechanics->coeff_container->pwp_zeta_limiter,&use_si);CHKERRQ(ierr);
    ierr = MechanicsCoeffContainerGetValidCellBuffer(ctx->mechanics->coeff_container,MCBUFFER_COOR,use_si,&buffer);CHKERRQ(ierr);
    
    ierr = LimiterRadialCreate(PETSC_TRUE,0,"limiter",&limiter_radial);CHKERRQ(ierr);
    ierr = PWPhysicsSetAuxiliaryDataBuffer(limiter_radial,0,(PetscReal*)buffer);CHKERRQ(ierr);
    origin[0] =  0.25 * ctx->L;
    origin[1] = -0.25 * ctx->L;
    ierr = LimiterRadialSetParams(limiter_radial,"bulkViscosity",10.0,1.0e-3,0.3 * ctx->L,origin);CHKERRQ(ierr);
    ierr = PWPhysicsSetFromOptions(limiter_radial);CHKERRQ(ierr);
    
    ierr = LimiterCompositeCreate(use_si,0,"zeta::wedge",&limiter_composite);CHKERRQ(ierr);
    ierr = LimiterCompositeSetLaw(limiter_composite,limiter_minmax);CHKERRQ(ierr);
    ierr = LimiterCompositeSetLaw(limiter_composite,limiter_radial);CHKERRQ(ierr);
    
    ctx->mechanics->coeff_container->pwp_zeta_limiter = limiter_composite;
  }
#endif
  
  ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_zeta);CHKERRQ(ierr);
  ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_zeta_limiter);CHKERRQ(ierr);
  
  ierr = PetscSNPrintf(limiter_type,PETSC_MAX_PATH_LEN-1,"%s","limiter.MinMax");CHKERRQ(ierr);
  ierr = PetscOptionsGetString(NULL,NULL,"-mechanics.k_on_mu.limiter",limiter_type,PETSC_MAX_PATH_LEN,&found);CHKERRQ(ierr);
  
  switch (ctx->khat_c_type) {
    case K_CONST:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"<deprecated> One should never define k indpendently of phi");
      break;
    case K_PHI_DEP:
      ierr = MechanicsCoeffContainerSetupEffectivePermability(ctx->mechanics->coeff_container,"k.PorosityDependent",limiter_type);CHKERRQ(ierr);
      ierr = kPorosityDependentSetParams(ctx->mechanics->coeff_container->pwp_k_on_mu,1.0e-13,3.0);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(ctx->mechanics->coeff_container->pwp_k_on_mu_limiter,NULL,0.0,PETSC_MAX_REAL);CHKERRQ(ierr);
      ierr = LimiterSpatialXSetParams(ctx->mechanics->coeff_container->pwp_k_on_mu_limiter,"EffectivePermeability",0.7*660.0e3);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Unknown k_on_mu (mobility) evaluator requested");
      break;
  }
  ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_k_on_mu);CHKERRQ(ierr);
  ierr = PWPhysicsSetFromOptions(ctx->mechanics->coeff_container->pwp_k_on_mu_limiter);CHKERRQ(ierr);
  
  /* impose Darcy flux*/
  ierr = MechanicsSetNeumannBC_DarcyFlux(ctx->mechanics,ctx);CHKERRQ(ierr);

  /* report */
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_eta,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_eta_limiter,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_zeta,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_zeta_limiter,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_k_on_mu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsView(ctx->mechanics->coeff_container->pwp_k_on_mu_limiter,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,NULL,"-subfusc.global_field_monitor",&ctx->global_field_monitor,NULL);CHKERRQ(ierr);
  
  output_freq = 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-subfusc.output_frequency",&output_freq,NULL);CHKERRQ(ierr);
  {
    PetscBool found = PETSC_FALSE;
    ierr = PetscOptionsGetInt(NULL,NULL,"-io_freq",&output_freq,&found);CHKERRQ(ierr);
    if (found) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Deprecated option detected: -io_freq. Replace with -subfusc.output_frequency");
    }
  }
  nt = 30;
  ierr = PetscOptionsGetInt(NULL,NULL,"-nt",&nt,NULL);CHKERRQ(ierr);
  dt_user = 0.001;
  dt_min_kyr = 0.1e-3;
  dt_min = dt_min_kyr / si2kyr / ctx->T;
  {
    PetscReal dt_kyr,dt_si;
    
    dt_si = dt_user * ctx->T;
    dt_kyr = dt_si * si2kyr;
    found = PETSC_FALSE;
    ierr = PetscOptionsGetReal(NULL,NULL,"-dt_kyr",&dt_kyr,&found);CHKERRQ(ierr);
    if (found) {
      dt_si = dt_kyr / si2kyr;
      dt_user = dt_si / ctx->T;
    }
    PetscPrintf(PETSC_COMM_WORLD,"[Mechanics, Energy, Components (solid)] : dt_user = %1.2e (kyr) : %+1.12e (non-dim)\n",dt_kyr,dt_user);
    
    found = PETSC_FALSE;
    ierr = PetscOptionsGetReal(NULL,NULL,"-dt_min_kyr",&dt_min_kyr,&found);CHKERRQ(ierr);
    if (found) {
      dt_min = dt_min_kyr / si2kyr / ctx->T;
    }
    PetscPrintf(PETSC_COMM_WORLD,"[Mechanics, Energy, Components (solid)] : dt_min = %1.2e (kyr) : %+1.12e (non-dim)\n",dt_min_kyr,dt_min);
    
    cfl_liquid = 1.0;
    ierr = PetscOptionsGetReal(NULL,NULL,"-cfl_liquid",&cfl_liquid,NULL);CHKERRQ(ierr);
  }
  
  
  /* o ------------------------------------------------------------------ o */
  /* Initialize for time dependent solve */
  /* o ------------------------------------------------------------------ o */
  /* Define initial concentrations */
  ierr = SDAdvection_SetInitialCondition_Cl(ctx->darcy);CHKERRQ(ierr);
  ierr = SDAdvection_SetInitialCondition_Cs(ctx->darcy);CHKERRQ(ierr);
  //// components
  {
    Vec X_prev;
    
    X_prev = NULL;
    ierr = PetscObjectQuery((PetscObject)ctx->darcy->pde_cl,"X_k_scalar",(PetscObject*)&X_prev);CHKERRQ(ierr);
    ierr = VecCopy(ctx->darcy->cl,X_prev);CHKERRQ(ierr); /* x --> x_old */
    
    X_prev = NULL;
    ierr = PetscObjectQuery((PetscObject)ctx->darcy->pde_cs,"X_k_scalar",(PetscObject*)&X_prev);CHKERRQ(ierr);
    ierr = VecCopy(ctx->darcy->cs,X_prev);CHKERRQ(ierr); /* x --> x_old */
  }
  
  /* solve a two phase system */
  {
    PhiCoeffType phi_c_type_reference;
    Coefficient bform;
    DM dm_v_omega_wedge,dm_scalar;
    Vec v_s_hat,p_hat;
    Vec phi_nodal_v_wedge;
    
    dm_v_omega_wedge = ctx->mechanics->dm_v_hat;
    
    /* a) Interpolate temperature onto the tau_m(wedge) */
    ierr = InterpT_FromTauEToTauM(ctx,ctx->energy->X,ctx->mechanics->T,ctx->mechanics->T_hat);CHKERRQ(ierr);
    /* b) evaluate phi (quadrature) */
    phi_c_type_reference = ctx->phi_c_type;
    ierr = PDEStokesDarcy2FieldGetCoefficients(ctx->mechanics->pde,&bform,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientEvaluate(bform);CHKERRQ(ierr);
    ctx->phi_c_type = PHI_NONE;
    
    ierr = DMTetCreateSharedSpace(dm_v_omega_wedge,1,&dm_scalar);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(dm_scalar,&phi_nodal_v_wedge);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
    
    
    /* c) solve vs,pf */
    ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
    ctx->phi_c_type = phi_c_type_reference;
    
    ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_s_bc);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);

    ierr = MechanicsComputeLiquidVelocity_Omega(ctx->mechanics,ctx->plate_region_id,ctx->slab_region_id);CHKERRQ(ierr);

    ierr = VecDestroy(&phi_nodal_v_wedge);CHKERRQ(ierr);
    
    /* d) darcy solve (vD,pf,vl,bar(v)) */
    ierr = DarcyCtxSolve(ctx->darcy,&step_passed);CHKERRQ(ierr);
    
    ierr = VecZeroEntries(ctx->mechanics->v_bar);CHKERRQ(ierr);
    ierr = DarcyComputeBulkVelocity_OmegaM(ctx->darcy,ctx->mechanics,ctx->I_v,ctx->plate_region_id,ctx->slab_region_id,PETSC_TRUE);CHKERRQ(ierr);
  }
  

  /* define the time step */
  ierr = DarcyComputeSolidLiquidVelocityCFLTimestep(ctx->darcy,cfl_liquid,&dt_cfl);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"[ic] dt_user %+1.4e : cfl %+1.4e : cfl dt %+1.4e\n",dt_user,cfl_liquid,dt_cfl);
  dt = dt_user;
  if (dt_cfl < dt) {
    dt = dt_cfl;
  }
  PetscPrintf(PETSC_COMM_WORLD,"[Mechanics, Energy, Components (solid)] : min(dt_user,dt_cfl) = %1.2e (kyr) : %+1.12e (non-dim)\n",dt * ctx->T * si2kyr,dt);
  
  /* Define initial concentrations */
  ierr = VecZeroEntries(ctx->energy->phi);CHKERRQ(ierr);
  ierr = VecCopy(ctx->energy->phi,ctx->energy->phi_prev);CHKERRQ(ierr);

  ierr = EnergySetTimestep(ctx->energy,dt);CHKERRQ(ierr);
  ierr = EnergySetCoefficientEvaluators_TimeDependentSource(ctx->energy,ctx);CHKERRQ(ierr);
  ierr = VecCopy(ctx->energy->X,ctx->energy->X_prev);CHKERRQ(ierr); /* x --> x_old */
  
  /* initialize Darcy T, phi */
  {
    DM dm_scalar;
    
    ierr = DMTetCreateSharedSpace(ctx->mechanics->dm_v_hat,1,&dm_scalar);CHKERRQ(ierr);
    ierr = DMTetProjectField(dm_scalar,ctx->mechanics->T_hat,PRJTYPE_NATIVE,ctx->darcy->dm_c_phi_T,&ctx->darcy->T);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);
  }

  ierr = DarcyCtx_PoroDistF(ctx->darcy,ctx->darcy->dm_c_phi_T,ctx->darcy->cl,ctx->darcy->cs,ctx->darcy->T,ctx->darcy->phi0);CHKERRQ(ierr);
  ierr = DarcyCtx_PoroDistF(ctx->darcy,ctx->darcy->dm_c_phi_T,ctx->darcy->cl,ctx->darcy->cs,ctx->darcy->T,ctx->darcy->phi);CHKERRQ(ierr);
  
  tk = 0;
  t_kyr = 0.0;
  t_nd = 0.0;
  {
    ierr = EnergyView(ctx->energy,ctx->output_path,tk);CHKERRQ(ierr);
    ierr = MechanicsView(ctx->mechanics,ctx->output_path,tk);CHKERRQ(ierr);
    ierr = DarcyCtxView(ctx->darcy,ctx->output_path,tk);CHKERRQ(ierr);
    ierr = DivergenceView(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,ctx->output_path,tk);CHKERRQ(ierr);
    
    ierr = TPView(ctx,t_kyr,tk,ctx->output_path);CHKERRQ(ierr);
    ierr = TPContextPVDAppend(ctx,t_kyr,tk);CHKERRQ(ierr);
  }
  
  
  dt_adaptive = dt;
  
  for (tk=1; tk<=nt; tk++) {
    PetscReal dt_bound;
    PetscReal dt_fac_reduce = 1.0/1.50,dt_fac_grow = 1.05; /* hard coded factors controlling rate time step is reduced and increased */
    PetscInt subk;
    
    ctx->step = tk;
    
    /* dt represents the upper bound allowable */
    dt_bound = dt_user;
    if (dt_cfl < dt_bound) {
      dt_bound = dt_cfl;
    }

    if (dt_fac_reduce > 1.0) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"dt_fac_reduce must be < 1.0. Found %+1.2e",dt_fac_reduce);
    if (dt_fac_grow < 1.0) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"dt_fac_grow must be > 1.0. Found %+1.2e",dt_fac_grow);
    
    /* Allow time step to grow on subsequent steps, but bound the growth by dt_user and dt_cfl */
    dt_adaptive = PetscMin(dt_adaptive * dt_fac_grow, dt_bound);


    
    PetscPrintf(PETSC_COMM_WORLD,"*************************************** ********************************* ***************************************\n");
    PetscPrintf(PETSC_COMM_WORLD,"***************************************  time = %1.2e kyr : step %.4D  ***************************************\n",t_kyr,tk);
    PetscPrintf(PETSC_COMM_WORLD,"*************************************** ********************************* ***************************************\n");
    
    //ierr = TCVPSequentialSolve(ctx,t_nd,dt_nd_restricted,tk);CHKERRQ(ierr);
    //ierr = TCVPCoupledSolve(ctx,t_nd,dt_nd_restricted,tk);CHKERRQ(ierr);
    //ierr = TCVPCoupledSolvePicard(ctx,t_nd,dt_nd_restricted,tk);CHKERRQ(ierr);
    //ierr = TCVPCoupledSolvePicardNestedVl(ctx,t_nd,dt_nd_restricted,tk);CHKERRQ(ierr);
    //ierr = TCVPCoupledSolveNestedVl(ctx,t_nd,dt_nd_restricted,tk);CHKERRQ(ierr);
    //ierr = TCVPCoupledSolveSubC(ctx,t_nd,dt_nd_restricted,tk);CHKERRQ(ierr);

    subk = 1;
    step_passed = PETSC_FALSE;
    while (step_passed != PETSC_TRUE) {
      dt_adaptive = PetscMax(dt_adaptive , dt_min);
      PetscPrintf(PETSC_COMM_WORLD,"  [adaptive time step - iter. #%d] trial time step %+1.2e : %+1.2e kyr\n",subk,dt_adaptive,dt_adaptive * ctx->T * si2kyr);
      ierr = TCVPSequentialSolve_Compat(ctx , t_nd + dt_adaptive , dt_adaptive, tk , &step_passed);CHKERRQ(ierr);
      if (!step_passed) {
        if (subk == 20) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"20 timestep reductions have been performed and solve still fails. Giving up...");
        if (dt_adaptive <= dt_min + 1.0e-12) SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_USER,"Solve still fails even with minimum timestep %+1.2e : %+1.2e kyr. Giving up...",dt_min,dt_min_kyr);
        dt_adaptive = dt_adaptive * dt_fac_reduce;
        PetscPrintf(PETSC_COMM_WORLD,"  [adaptive time step - iter. #%d] --> step rejected\n",subk);
      } else {
        PetscPrintf(PETSC_COMM_WORLD,"  [adaptive time step - iter. #%d] --> step accepted\n",subk);
      }
      subk++;
    }

    t_nd += dt_adaptive;
    t_kyr += dt_adaptive * ctx->T * si2kyr;
    ierr = TPContexSetupTemporalParameters(ctx,t_nd,dt_adaptive,tk);CHKERRQ(ierr);

    
    /* output */
    if (ctx->global_field_monitor) {
      ierr = SubFUScGlobalFieldMetricMonitor(ctx,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    }
    
    if (tk%output_freq == 0) {
      ierr = EnergyView(ctx->energy,ctx->output_path,tk);CHKERRQ(ierr);
      ierr = MechanicsView(ctx->mechanics,ctx->output_path,tk);CHKERRQ(ierr);
      ierr = DarcyCtxView(ctx->darcy,ctx->output_path,tk);CHKERRQ(ierr);
      /*
      ierr = DivergenceView(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,ctx->output_path,tk);CHKERRQ(ierr);
      */
      ierr = TPView(ctx,t_kyr,tk,ctx->output_path);CHKERRQ(ierr);
      ierr = TPContextPVDAppend(ctx,t_kyr,tk);CHKERRQ(ierr);
    }
    
    /* debug output */
    if (tk%output_freq == 0) {
#ifdef SUBFUSC_DEBUG_OUTPUT
      char prefix[PETSC_MAX_PATH_LEN];
      const char *field_T[] = { "T" };
      const char *field_vl[] = { "vl" };
      const char *field_vbar[] = { "vbar" };

      ierr = DMTetSetDof(dm_v_omega_wedge,1);CHKERRQ(ierr);
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_wedge_v_",ctx->output_path,tk,field_T[0]);CHKERRQ(ierr);
      ierr = DMTetViewFields_VTU(dm_v_omega_wedge,1,&ctx->mechanics->T_hat,field_T,prefix);CHKERRQ(ierr);
      ierr = DMTetSetDof(dm_v_omega_wedge,2);CHKERRQ(ierr);

      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_star_omega_v_",ctx->output_path,tk,field_vl[0]);CHKERRQ(ierr);
      ierr = DMTetViewFields_VTU(ctx->mechanics->dm_v,1,&ctx->mechanics->v_l_star,field_vl,prefix);CHKERRQ(ierr);
      
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_omega_",ctx->output_path,tk,field_vbar[0]);CHKERRQ(ierr);
      ierr = DMTetViewFields_VTU(ctx->mechanics->dm_v,1,&ctx->mechanics->v_bar,field_vbar,prefix);CHKERRQ(ierr);
#endif
    }
    
    ierr = DarcyComputeSolidLiquidVelocityCFLTimestep(ctx->darcy,cfl_liquid,&dt_cfl);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"[step %d] dt_user %+1.4e : cfl %+1.4e : cfl dt %+1.4e\n",tk,dt_user,cfl_liquid,dt_cfl);
    dt = dt_user;
    if (dt_cfl < dt) {
      dt = dt_cfl;
    }
  }
  
  ierr = TPContextDestroy(&ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = execute_wedge_model();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

