
#if !defined(__TPCTX_H)
#define __TPCTX_H

#include <petsc.h>
#include <petscdm.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscsnes.h>

#include <pde.h>
#include <dmbcs.h>
#include <multicomponent.h>
#include <quadrature.h>
#include <quadratureimpl.h>
#include <coefficient.h>
#include <element_container.h>
#include <pointwise_physics.h>

#include <distf.h>
#include <cpart.h>
#include <nonidealpart.h>

//#define SUBFUSC_DEBUG_OUTPUT
#define   SUBFUSC_USE_BINARY_PV

/*
 The methods X_NONE simply do not update the specified quantity X
 In this case, the user has control over when quantities are updated.
*/
typedef enum { PHI_NONE = 0, PHI_CONST , PHI_DISTF } PhiCoeffType;
typedef enum { K_NONE = 0, K_CONST, K_PHI_DEP } KCoeffType;
typedef enum { ETA_NONE = 0, ETA_CONST, ETA_DIFF, ETA_DISC, ETA_COMPOSITE } EtaCoeffType;
typedef enum { ZETA_NONE = 0, ZETA_CONST, ZETA_RATIO, ZETA_DIFF, ZETA_DISC } ZetaCoeffType;

extern PetscReal PLATE_THICKNESS;
extern PetscReal SLAB_DIP;
extern PetscReal DOMAIN_XMAX;
extern PetscReal DOMAIN_YMIN;

extern const char *PhiCoeffTypeNames[];
extern const char *KCoeffTypeNames[];
extern const char *EtaCoeffTypeNames[];
extern const char *ZetaCoeffTypeNames[];

extern PetscClassId TWOPHASE_CLASSID;
extern PetscClassId MECHANICS_CLASSID;
extern PetscClassId ENERGY_CLASSID;
extern PetscClassId COMPONENTS_CLASSID;

typedef struct _p_TPCtx *TPCtx;
typedef struct _p_MCtx *MCtx;
typedef struct _p_ECtx *ECtx;
typedef struct _p_CCtx *CCtx;
typedef struct _p_MechanicsCoeffContainer *MechanicsCoeffContainer;
typedef struct _p_DarcyCtx *DarcyCtx;

#define SFUSCSetClassId(a,id) \
  (a)->class_id = id; \

#define SFUSCCheckPointer(a,arg) \
  if (!(a)) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_NULL,"Null Object: Parameter # %d",arg); \

#define SFUSCValidType(a,ck,arg) \
do { \
  SFUSCCheckPointer(a,arg); \
  if ((a)->class_id != ck) { \
    const char *classname; \
    PetscQueryClassName(ck,&classname); \
    SETERRQ4(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Parameter # %d of wrong type: Expected \"%s\" (ClassID %d) but found ClassId %d",arg,classname,ck,(a)->class_id); \
  } \
} while (0)


struct _p_TPCtx {
  PetscClassId class_id;
  const char *output_path;
  MCtx mechanics;
  ECtx energy;
  DarcyCtx darcy;
  
  /* temporary vectors */

  /* injection */
  Mat I_v; /* inject dm_v_hat into dm_v */
  //VecScatter I_e; /* inject dm_ve into dm_ve_hat */
  Mat I_e;
  
  /* interpolation */
  Mat W_ve; /* interpolates from dm_v to dm_ev */
  Mat Q_ev; /* interpolates from dm_e to dm_ve */

  
  /* projected data */
  
  /* other data */
  DistF distfunc; /* distribution function */
  CPart cpart; /* Constant partition thermo-dynamic model */
  NonIdealPart nipart;
  PetscReal E,L,V,T,Ka,K,R,A,P; /* dimensional scaling paramaters */
  char reference_mesh_filename[PETSC_MAX_PATH_LEN];

  // material properties place holder //
  PetscReal kappa;             // thermal diffusivity
  PetscReal phi;               // porosity
  PetscReal rho_s,rho_l;       // solid, liquid density [\rho_s , \rho_f]
  PetscReal rho_plate;

  // boundary condition data //
  PetscReal Vdarcy;            // Darcy flux on the slab-wedge interface
  PetscReal gaussian_depth;            // Darcy flux on the slab-wedge interface
  PetscReal gaussian_width;            // Darcy flux on the slab-wedge interface

  // domain constants //
  PetscReal g[2];              // gravity term
  
  // coefficient evaluator types //
  PhiCoeffType phi_c_type;
  KCoeffType khat_c_type;
  EtaCoeffType eta_c_type;
  ZetaCoeffType zeta_c_type;
  PetscInt slab_region_id,plate_region_id,wedge_region_id;
  // output data types
  PetscBool output_hdf;

  PetscBool global_field_monitor;
  /* time step parameters */
  PetscReal time,dt;
  PetscInt step;
  PetscBool viewer_active;
};

/* context for mechanical (two-phase) problem */
struct _p_MCtx {
  PetscClassId class_id;
  /* DMs over domain \Omega */
  DM dm_v,dm_p,dm_m;
  DM dm_ve; /* copy of dm_v with blocksize = 1 */
  /* DMs over wedge domain \Omega_w */
  DM dm_v_hat,dm_p_hat,dm_m_hat;
  DM dm_ve_hat; /* copy of dm_v_hat with blocksize = 1 */

  BCList bc_v_hat;
  
  /* solution vectors */
  Vec F_hat,X_hat;
  Mat J_hat;
  
  /* liquid on \Omega_w */
  Vec v_l_hat;
  
  /* liquid, bulk velocity on \Omega */
  Vec v_l_star,v_bar;
  
  /* boundary condition vector on \Omega */
  Vec v_s_bc;
  
  /* interpolated temperature */
  Vec T,T_hat;
  
  MechanicsCoeffContainer coeff_container;
  PDE pde;
  SNES snes;
  
  PetscReal plate_velocity;    // velocity of the subducting plate  
  PetscReal slab_decoupling_depth,slab_decoupling_distance;
  PetscBool viewer_active;
};

struct _p_ECtx {
  PetscClassId class_id;
  /* DMs over domain \Omega */
  DM dm_e;
  DM dm_ev;
  
  BCList bc_T;

  /* solution vectors */
  Vec F,X,X_prev;
  Mat J;
  
  /* work vecs */
  Vec v_bar_e,v_s_e;
  Vec phi,phi_prev;
  
  PDE pde;
  SNES snes;
  PetscReal delta_t;
  PetscReal L_on_Cp; /* [K] */
  PetscInt wedge_region_id;
  
  // output data types
  PetscBool output_hdf;
  PetscBool viewer_active;
};

PetscErrorCode PetscQueryClassName(PetscClassId classid,const char *cname[]);

PetscErrorCode TPContextCreate(TPCtx *_ctx);
PetscErrorCode TPContextDestroy(TPCtx *_ctx);
PetscErrorCode TPSetUp_Default(TPCtx ctx,const char meshfilename[],PetscInt pk,PetscInt pke);
PetscErrorCode InterpT_FromTauEToTauM(TPCtx c,Vec T_tau_e,Vec T,Vec T_hat);
PetscErrorCode InjectV_FromOmegaWedgeToOmega(TPCtx c,Vec v_hat,Vec v);
PetscErrorCode InterpV_FromTauMOmegaToTauEOmega(TPCtx c,Vec v_tau_m,Vec v_tau_e);
PetscErrorCode InterpT_FromTauEOmegaToTauMOmega(TPCtx c,Vec T_tau_e,Vec T_tau_m);

PetscErrorCode TPCtxSetDimensionalScales_ELV(TPCtx ctx,PetscReal E,PetscReal L,PetscReal V);
PetscErrorCode TPCtxApplyScaling(TPCtx ctx);
PetscErrorCode TPContextPVDInit(TPCtx c);
PetscErrorCode TPContextPVDAppend(TPCtx c,PetscReal time,PetscInt step);
PetscErrorCode TPView(TPCtx c,PetscReal time,PetscInt step,const char path[]);

PetscErrorCode PetscOptionGetReal_GeoVel(const char pre[],const char opt[],PetscReal *v,PetscReal *v_si,PetscBool *f);
PetscErrorCode PetscOptionGetReal_GeoLength(const char pre[],const char opt[],PetscReal *v,PetscReal *v_si,PetscBool *f);
PetscErrorCode SubFUScGlobalFieldMetricMonitor(TPCtx c,PetscViewer viewer);
PetscErrorCode TPContexSetupTemporalParameters(TPCtx ctx,PetscReal time,PetscReal dt,PetscInt step);
PetscErrorCode DMTetCreatePartitionedFromTriangleWithRef(MPI_Comm comm,const char filename[],PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm);

struct _p_MechanicsCoeffContainer {
  PWPhysics pwp_eta,pwp_eta_limiter;
  PWPhysics pwp_zeta,pwp_zeta_limiter;
  PWPhysics pwp_k_on_mu, pwp_k_on_mu_limiter;
  PWPhysics pwp_rho;
  PetscBool requires_coordinates;
  PetscBool requires_pressure;
  PetscBool requires_temperature;
  PetscBool requires_porosity;
  PetscBool requires_components;
  PetscBool requires_strain_rate_inv;
  EContainer space_v,space_p,space_T;
  PetscReal *cell_qp_coor,*cell_qp_coor_si;
  PetscReal *cell_qp_p,*cell_qp_p_si;
  PetscReal *cell_qp_T,*cell_qp_T_si;
  PetscReal *cell_qp_phi,*cell_qp_phi_si;
  PetscReal *cell_qp_Crf,*cell_qp_CH2O,*cell_qp_Crf_si,*cell_qp_CH2O_si;
  PetscReal *cell_qp_e2,*cell_qp_e2_si;
  PetscBool debug;
};

#endif
