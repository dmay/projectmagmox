
#include <petsc.h>
#include <petsc/private/logimpl.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petscsnes.h>

#include <proj_init_finalize.h>
#include <petscdmtet.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <pdestokes.h>
#include <pdestokesdarcy2field.h>
#include <dmbcs.h>
#include <paraview_utils.h>

#include <tpctx.h>
#include <mec.h>
#include <distf.h>
#include <darcyctx.h>

PetscReal PLATE_THICKNESS;
PetscReal SLAB_DIP;
PetscReal DOMAIN_XMAX;
PetscReal DOMAIN_YMIN;

const char *PhiCoeffTypeNames[] = { "none", "const", "distribution_function", "PhiCoeffType", "QEVALUATOR_PHI", 0 };
const char *KCoeffTypeNames[] = { "none", "const", "phi_dep", "KCoeffType", "QEVALUATOR_K", 0 };
const char *EtaCoeffTypeNames[] = { "none", "const", "diffusion_creep", "dislocation_creep", "composite","EtaCoeffType", "QEVALUATOR_ETA", 0 };
const char *ZetaCoeffTypeNames[] = { "none", "const", "ratio", "diffusion_creep", "dislocation_creep", "ZetaCoeffType", "QEVALUATOR_ZETA", 0 };

PetscClassId TWOPHASE_CLASSID;
PetscClassId MECHANICS_CLASSID;
PetscClassId ENERGY_CLASSID;
PetscClassId COMPONENTS_CLASSID;


#undef __FUNCT__
#define __FUNCT__ "PetscQueryClassName"
PetscErrorCode PetscQueryClassName(PetscClassId classid,const char *cname[])
{
  PetscStageLog  stageLog;
  int            index;
  PetscErrorCode ierr;
  
  ierr = PetscLogGetStageLog(&stageLog);CHKERRQ(ierr);
  ierr = PetscClassRegLogGetClass(stageLog->classLog,classid,&index);CHKERRQ(ierr);
  //printf("oclass %d: name -> %s\n",index,stageLog->classLog->classInfo[index].name);
  if (cname) {
    *cname = stageLog->classLog->classInfo[index].name;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPContextCreate"
PetscErrorCode TPContextCreate(TPCtx *_ctx)
{
  TPCtx ctx;
  static PetscBool registered = PETSC_FALSE;
  PetscErrorCode ierr;
  
  if (!registered) {
    ierr = PetscClassIdRegister("Two-phase flow context (TPCtx)",&TWOPHASE_CLASSID);CHKERRQ(ierr);
    ierr = PetscClassIdRegister("Mechanics context (MCtx)",&MECHANICS_CLASSID);CHKERRQ(ierr);
    ierr = PetscClassIdRegister("Energy context (ECtx)",&ENERGY_CLASSID);CHKERRQ(ierr);
    ierr = PetscClassIdRegister("Components context (CCtx)",&COMPONENTS_CLASSID);CHKERRQ(ierr);
    registered = PETSC_TRUE;
    
    //const char *c;
    //PetscQueryClassName(TWOPHASE_CLASSID,&c);
    //printf("c %s \n",c);
  }
  
  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_TPCtx));CHKERRQ(ierr);
  SFUSCSetClassId(ctx,TWOPHASE_CLASSID);
  
  ierr = projGetOutputPath(&ctx->output_path);CHKERRQ(ierr);
  
  PLATE_THICKNESS = 0.0;
  ierr = MechanicsContextCreate(&ctx->mechanics);CHKERRQ(ierr);
  ierr = EnergyContextCreate(&ctx->energy);CHKERRQ(ierr);
  ierr = DarcyCtxCreate(&ctx->darcy);CHKERRQ(ierr);
  
  ctx->phi_c_type = PHI_CONST;
  ctx->khat_c_type = K_CONST;
  ctx->eta_c_type = ETA_CONST;
  ctx->zeta_c_type = ZETA_CONST;
  
  ctx->output_hdf = PETSC_FALSE;
  ctx->global_field_monitor = PETSC_FALSE;
  
  ctx->time = 0.0;
  ctx->dt = 0.0;
  ctx->step = 0;
  ctx->viewer_active = PETSC_TRUE;
  
  *_ctx = ctx;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPContextDestroy"
PetscErrorCode TPContextDestroy(TPCtx *_ctx)
{
  TPCtx ctx;
  PetscErrorCode ierr;
  if (!_ctx) PetscFunctionReturn(0);
  ctx = *_ctx;

  ierr = MechanicsContextDestroy(&ctx->mechanics);CHKERRQ(ierr);
  ierr = EnergyContextDestroy(&ctx->energy);CHKERRQ(ierr);
  ierr = DarcyCtxDestroy(&ctx->darcy);CHKERRQ(ierr);
  
  ierr = DistFDestroy(&ctx->distfunc);CHKERRQ(ierr);
  ierr = CPartDestroy(&ctx->cpart);CHKERRQ(ierr);
  ierr = NonIdealPartDestroy(&ctx->nipart);CHKERRQ(ierr);
  
  /* auxillary data */
  ierr = MatDestroy(&ctx->I_v);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->W_ve);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->Q_ev);CHKERRQ(ierr);
  //ierr = VecScatterDestroy(&ctx->I_e);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->I_e);CHKERRQ(ierr);
  
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  *_ctx = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_TPSetUp_MEC"
PetscErrorCode _TPSetUp_MEC(TPCtx ctx,const char meshfilename[],PetscInt pk,PetscInt pke)
{
  PetscErrorCode ierr;
  PetscReal gmin[2],gmax[2];
  PetscReal gmin_w[2],gmax_w[2];
  PetscBool flag;
  
  ierr = PetscSNPrintf(ctx->reference_mesh_filename,PETSC_MAX_PATH_LEN-1,"%s",meshfilename);CHKERRQ(ierr);
  
  ierr = MechanicsSetUp(ctx->mechanics,ctx->reference_mesh_filename,pk);CHKERRQ(ierr);
  ierr = EnergySetUp(ctx->energy,ctx->reference_mesh_filename,pke);CHKERRQ(ierr);

  ierr = DMTetGetBoundingBox(ctx->mechanics->dm_v_hat,gmin_w,gmax_w);CHKERRQ(ierr);
  ierr = DMTetGetBoundingBox(ctx->mechanics->dm_v,gmin,gmax);CHKERRQ(ierr);
  
  /* This assumes the following
   - plate has a constant thickness
   - slab is straight (it has a constant dip)
  */
  PLATE_THICKNESS = gmax[1] - gmax_w[1];
  if (PLATE_THICKNESS < 0.0) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"The determined plate thickness (%+1.4e) is < 0",PLATE_THICKNESS);
  if (PLATE_THICKNESS > (gmax[1] - gmin[1])) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"The determined plate thickness (%+1.4e) is larger than the entire model domain",PLATE_THICKNESS);

  DOMAIN_XMAX = gmax[0] - gmin[0];
  if (gmin[0] > 1.0e-8) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"It is assumed that the left side of the domain is located at x = 0. Found x = %+1.4e",gmin[0]);

  if (gmax[1] > 1.0e-8) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"It is assumed that the top of the domain is located at y = 0. Found y = %+1.4e",gmax[1]);
  DOMAIN_YMIN = gmin[1];
  
  SLAB_DIP = 45;
  ierr = PetscOptionsGetReal(NULL,NULL,"-subfusc.slab_dip",&SLAB_DIP,&flag); CHKERRQ(ierr);
  if (!flag) { ierr = PetscPrintf(PETSC_COMM_WORLD,"Warning: the slab dip was not set. Using 45 degrees. (use -subfusc.slab_dip <degrees>) \n");CHKERRQ(ierr); }
  
  ierr = DistFCreate(&ctx->distfunc);CHKERRQ(ierr);
  ierr = DistFInit(ctx->distfunc);CHKERRQ(ierr);
  ierr = CPartCreate(&ctx->cpart);CHKERRQ(ierr);
  ierr = NonIdealPartCreate(1,&ctx->nipart);CHKERRQ(ierr);

  ierr = DarcyCtxSetUp(ctx->darcy,ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_TPSetUp_DomainFieldTransfers"
PetscErrorCode _TPSetUp_DomainFieldTransfers(TPCtx ctx)
{
  PetscErrorCode ierr;

  /* I_v : v_s = I_v v_s_hat */
  ierr = DMCreateInjection(ctx->mechanics->dm_v,ctx->mechanics->dm_v_hat,&ctx->I_v);CHKERRQ(ierr);

  /* W_ve : interpolates from velocity defined on dm_v to dm_ev */
  {
    Mat interp_tet2tet;

    ierr = DMCreateInterpolation(ctx->mechanics->dm_ve,ctx->energy->dm_ev,&interp_tet2tet,NULL);CHKERRQ(ierr);
    ierr = MatCreateMAIJ(interp_tet2tet,2,&ctx->W_ve);CHKERRQ(ierr);
    ierr = MatDestroy(&interp_tet2tet);CHKERRQ(ierr);
  }

  /* Q_ev : interpolates temperature defined on dm_e to dm_v (omega) */
  ierr = DMCreateInterpolation(ctx->energy->dm_e,ctx->mechanics->dm_ve,&ctx->Q_ev,NULL);CHKERRQ(ierr);

#if 0
  /* I_e : inject from dm_ve to dm_ve_hat. We use the layout of the velocity space and exact scalar ISs */
  {
    IS isFrom,isTo;
    
    ierr = DMTetCreateInjectionScalarIS(ctx->mechanics->dm_v,ctx->mechanics->dm_v_hat,&isTo,&isFrom);CHKERRQ(ierr);
    /*
    printf("to\n");
    ISView(isTo,PETSC_VIEWER_STDOUT_SELF);
    printf("from\n");
    ISView(isFrom,PETSC_VIEWER_STDOUT_SELF);
    */
    ierr = VecScatterCreate(ctx->mechanics->T,isTo,ctx->mechanics->T_hat,isFrom,&ctx->I_e);CHKERRQ(ierr);
    
    ierr = ISDestroy(&isTo);CHKERRQ(ierr);
    ierr = ISDestroy(&isFrom);CHKERRQ(ierr);
  }
#endif
  ierr = DMCreateInjection(ctx->mechanics->dm_ve_hat,ctx->mechanics->dm_ve,&ctx->I_e);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPSetUp_Default"
PetscErrorCode TPSetUp_Default(TPCtx ctx,const char meshfilename[],PetscInt pk,PetscInt pke)
{
  PetscErrorCode ierr;
  PetscInt pk_v_opts,pk_e_opts;
  PetscBool flag;
  
  ctx->E = 1.0e26; /* viscosity scale */
  ctx->L = 600.0 * 1.0e3; /* length scale 600 km */
  ctx->V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */

  ierr = PetscOptionsGetReal(NULL,NULL,"-subfusc.length_scale",&ctx->L,&flag); CHKERRQ(ierr);
  if (!flag) {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"Warning: the characteristic length scale was not set. Using %1.4e (use -subfusc.length_scale <value_in_meters>) \n",ctx->L);CHKERRQ(ierr);
  }

  pk_v_opts = pk;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk_v",&pk_v_opts,NULL);CHKERRQ(ierr);

  pk_e_opts = pke;
  ierr = PetscOptionsGetInt(NULL,NULL,"-pk_t",&pk_e_opts,NULL);CHKERRQ(ierr);

  ierr = _TPSetUp_MEC(ctx,meshfilename,pk_v_opts,pk_e_opts);CHKERRQ(ierr);
  ierr = _TPSetUp_DomainFieldTransfers(ctx);CHKERRQ(ierr);
  
  ierr = TPContextPVDInit(ctx);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* multi-stage operation */
/* interpolate T from E to M */
/* inject from T(tauM(omega)) to T(tauM(wedge)) */
#undef __FUNCT__
#define __FUNCT__ "InterpT_FromTauEToTauM"
PetscErrorCode InterpT_FromTauEToTauM(TPCtx c,Vec T_tau_e,Vec T,Vec T_hat)
{
  PetscErrorCode ierr;
  VecScatter scatter;

  ierr = MatMult(c->Q_ev,T_tau_e,T);CHKERRQ(ierr);
  //ierr = VecScatterBegin(c->I_e,T,T_hat,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  //ierr = VecScatterEnd(c->I_e,T,T_hat,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = MatScatterGetVecScatter(c->I_e,&scatter);CHKERRQ(ierr);
  ierr = VecScatterBegin(scatter,T,T_hat,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter,T,T_hat,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);

  
  PetscFunctionReturn(0);
}

/* I_v */
#undef __FUNCT__
#define __FUNCT__ "InjectV_FromOmegaWedgeToOmega"
PetscErrorCode InjectV_FromOmegaWedgeToOmega(TPCtx c,Vec v_hat,Vec v)
{
  PetscErrorCode ierr;
  VecScatter scat;
  
  ierr = MatScatterGetVecScatter(c->I_v,&scat);CHKERRQ(ierr);
  
  ierr = VecScatterBegin(scat,v_hat,v,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scat,v_hat,v,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* W_ve */
#undef __FUNCT__
#define __FUNCT__ "InterpV_FromTauMOmegaToTauEOmega"
PetscErrorCode InterpV_FromTauMOmegaToTauEOmega(TPCtx c,Vec v_tau_m,Vec v_tau_e)
{
  PetscErrorCode ierr;

  ierr = MatMult(c->W_ve,v_tau_m,v_tau_e);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* Q_ev */
#undef __FUNCT__
#define __FUNCT__ "InterpT_FromTauEOmegaToTauMOmega"
PetscErrorCode InterpT_FromTauEOmegaToTauMOmega(TPCtx c,Vec T_tau_e,Vec T_tau_m)
{
  PetscErrorCode ierr;
  
  ierr = MatMult(c->Q_ev,T_tau_e,T_tau_m);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPContextPVDInit"
PetscErrorCode TPContextPVDInit(TPCtx c)
{
  PetscErrorCode ierr;
  char pvdfile[PETSC_MAX_PATH_LEN];
  
  if (c->mechanics->viewer_active) {
    ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/vs_wedge.pvd",c->output_path);CHKERRQ(ierr);
    ierr = ParaviewPVDOpen(pvdfile);CHKERRQ(ierr);
  }
  
#ifdef SUBFUSC_DEBUG_OUTPUT
  ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/phi_wedge.pvd",c->output_path);CHKERRQ(ierr);
  ierr = ParaviewPVDOpen(pvdfile);CHKERRQ(ierr);
#endif
  
  if (c->energy->viewer_active) {
    ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/energy_omega.pvd",c->output_path);CHKERRQ(ierr);
    ierr = ParaviewPVDOpen(pvdfile);CHKERRQ(ierr);
  }

  ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/wedge.pvd",c->output_path);CHKERRQ(ierr);
  ierr = ParaviewPVDOpen(pvdfile);CHKERRQ(ierr);

  ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/darcy.pvd",c->output_path);CHKERRQ(ierr);
  ierr = ParaviewPVDOpen(pvdfile);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPContextPVDAppend"
PetscErrorCode TPContextPVDAppend(TPCtx c,PetscReal time,PetscInt step)
{
  PetscErrorCode ierr;
  char pvdfile[PETSC_MAX_PATH_LEN];
  char fname[PETSC_MAX_PATH_LEN];

  if (c->mechanics->viewer_active) {
    ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/vs_wedge.pvd",c->output_path);CHKERRQ(ierr);
    ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"step-%.4D_vs_dmtet.pvtu",step);CHKERRQ(ierr);
    ierr = ParaviewPVDAppend(pvdfile,time,fname,NULL);CHKERRQ(ierr);
  }
  
#ifdef SUBFUSC_DEBUG_OUTPUT
  ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/phi_wedge.pvd",c->output_path);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"step-%.4D_phi_wedge_v_dmtet.pvtu",step);CHKERRQ(ierr);
  ierr = ParaviewPVDAppend(pvdfile,time,fname,NULL);CHKERRQ(ierr);
#endif
  
  if (c->energy->viewer_active) {
    ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/energy_omega.pvd",c->output_path);CHKERRQ(ierr);
    ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"step-%.4D_energy_dmtet.pvtu",step);CHKERRQ(ierr);
    ierr = ParaviewPVDAppend(pvdfile,time,fname,NULL);CHKERRQ(ierr);
  }

  ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/wedge.pvd",c->output_path);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"step-%.4D_wedge_dmtet.pvtu",step);CHKERRQ(ierr);
  ierr = ParaviewPVDAppend(pvdfile,time,fname,NULL);CHKERRQ(ierr);

  ierr = PetscSNPrintf(pvdfile,PETSC_MAX_PATH_LEN-1,"%s/darcy.pvd",c->output_path);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname,PETSC_MAX_PATH_LEN-1,"step-%.4D_darcy_dmtet.pvtu",step);CHKERRQ(ierr);
  ierr = ParaviewPVDAppend(pvdfile,time,fname,NULL);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPView"
PetscErrorCode TPView(TPCtx c,PetscReal time,PetscInt step,const char path[])
{
  Vec eta = NULL,zeta = NULL,phi = NULL,k_on_mu = NULL;
  Vec vs = NULL,pf = NULL,T = NULL,vl = NULL;
  DM dm_wedge,dm_tmp;
  Coefficient bform;
  Quadrature quadrature;
  /* 
   It is required to place scalar fields first, followed by vector fields.
   The last entry in the list must be NULL (or 0).
   If you change this list, please adjust the const variables named
       nfields_scalar
       nfields_vector
   accordingly
  */
  const char *field_names[] = { "eta", "zeta", "k*", "phi", "pf", "T", "vs", "vl", 0 };
  const PetscInt nfields_scalar = 6;
  const PetscInt nfields_vector = 2;
  Vec *Xp;
  char prefix[PETSC_MAX_PATH_LEN];
  char h5filename[PETSC_MAX_PATH_LEN];
  char stepname[PETSC_MAX_PATH_LEN];
  char stepfieldname[PETSC_MAX_PATH_LEN];
  PetscInt f,id,nfields;
  PetscErrorCode ierr;
  
  nfields = 0;
  while (field_names[nfields] != NULL) {
    nfields++;
  }
  if (nfields != (nfields_scalar+nfields_vector)) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Any inconsistent number of scalar/vector fields was detected");
  ierr = PetscMalloc1(nfields,&Xp);CHKERRQ(ierr);
  for (f=0; f<nfields; f++) {
    Xp[f] = NULL;
  }
  
  dm_wedge = c->mechanics->dm_p_hat; /* might change this to be a reference P1 dm */
  //dm_omega = c->mechanics->dm_p; /* might change this to be a reference P1 dm */
  
  /* mechanics : project quadrature coefficients */
  ierr = PDEStokesGetCoefficients(c->mechanics->pde,&bform,NULL,NULL);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(bform,&quadrature);CHKERRQ(ierr);
  
  ierr = DMTetProjectQuadratureField(quadrature,"eta",  dm_wedge,&eta);CHKERRQ(ierr);
  ierr = DMTetProjectQuadratureField(quadrature,"zeta*",dm_wedge,&zeta);CHKERRQ(ierr);
  ierr = DMTetProjectQuadratureField(quadrature,"k*",   dm_wedge,&k_on_mu);CHKERRQ(ierr);
  ierr = DMTetProjectQuadratureField(quadrature,"phi",  dm_wedge,&phi);CHKERRQ(ierr);
  
  /* project fields */
  /* -- velocity_solid, pressure_fluid -- */
  {
    Vec Xu,Xp;
    
    ierr = DMCompositeGetAccess(c->mechanics->dm_m,c->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  
    ierr = DMTetCreateSharedSpace(dm_wedge,2,&dm_tmp);CHKERRQ(ierr);
    ierr = DMTetProjectField(c->mechanics->dm_v_hat,Xu,PRJTYPE_NATIVE,dm_tmp,&vs);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);
    
    //ierr = DMTetProjectField(c->mechanics->dm_p_hat,Xp,PRJTYPE_NATIVE,dm,&pf);CHKERRQ(ierr);
    ierr = VecDuplicate(Xp,&pf);CHKERRQ(ierr);
    ierr = VecCopy(Xp,pf);CHKERRQ(ierr);
    
    ierr = DMCompositeRestoreAccess(c->mechanics->dm_m,c->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  }
  
  /* -- temperature -- */
  /* Interpolate temperature onto the tau_m(wedge) */
  ierr = InterpT_FromTauEToTauM(c,c->energy->X,c->mechanics->T,c->mechanics->T_hat);CHKERRQ(ierr);
  ierr = DMTetCreateSharedSpace(c->mechanics->dm_v_hat,1,&dm_tmp);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm_tmp,c->mechanics->T_hat,PRJTYPE_NATIVE,dm_wedge,&T);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);

  /* -- velocity_liquid -- */
  //ierr = DMTetCreateSharedSpace(dm,2,&dm_tmp);CHKERRQ(ierr);
  //ierr = DMTetProjectField(c->mechanics->dm_v_hat,Xu,PRJTYPE_NATIVE,dm_tmp,&vl);CHKERRQ(ierr);
  //ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);

  ierr = VecDuplicate(c->mechanics->v_l_hat,&vl);CHKERRQ(ierr);
  ierr = VecCopy(c->mechanics->v_l_hat,vl);CHKERRQ(ierr);

  id = 0;
  Xp[id] = eta;     id++;
  Xp[id] = zeta;     id++;
  Xp[id] = k_on_mu;  id++;
  Xp[id] = phi;      id++;
  
  Xp[id] = pf;       id++;
  Xp[id] = T;        id++;
  
  Xp[id] = vs;  id++;
  Xp[id] = vl;  id++;


  if (path) {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_wedge_",path,step);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_wedge_",step);CHKERRQ(ierr);
  }
  
  if (c->output_hdf) {
    PetscInt j;
    
    if (path) {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"%s/wedge.h5",path);CHKERRQ(ierr);
    } else {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"wedge.h5");CHKERRQ(ierr);
    }
    ierr = PetscSNPrintf(stepname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D",step);CHKERRQ(ierr);
    ierr = DMTetSpaceAddGroupNameHDF5(h5filename,stepname);CHKERRQ(ierr);
    
    for (j=0; j<nfields_scalar; j++) {
      ierr = PetscSNPrintf(stepfieldname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D/%s",step,field_names[j]);CHKERRQ(ierr);
      ierr = DMTetFieldSpaceView_HDF(dm_wedge,Xp[j],stepfieldname,h5filename);CHKERRQ(ierr);
    }
    
    ierr = DMTetCreateSharedSpace(dm_wedge,2,&dm_tmp);CHKERRQ(ierr);
    for (j=nfields_scalar; j<nfields; j++) {
      ierr = PetscSNPrintf(stepfieldname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D/%s",step,field_names[j]);CHKERRQ(ierr);
      ierr = DMTetFieldSpaceView_HDF(dm_tmp,Xp[j],stepfieldname,h5filename);CHKERRQ(ierr);
    }
    ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);
  } else {
#ifdef SUBFUSC_USE_BINARY_PV
    ierr = DMTetViewFields_VTU_b(dm_wedge,nfields,Xp,field_names,prefix);CHKERRQ(ierr);
#elif
    ierr = DMTetViewFields_VTU(dm_wedge,nfields,Xp,field_names,prefix);CHKERRQ(ierr);
#endif
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_wedge_",step);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU2(dm_wedge,nfields,Xp,field_names,path,prefix,NULL);CHKERRQ(ierr);
  }
  
  ierr = VecDestroy(&eta);CHKERRQ(ierr);
  ierr = VecDestroy(&zeta);CHKERRQ(ierr);
  ierr = VecDestroy(&k_on_mu);CHKERRQ(ierr);
  ierr = VecDestroy(&phi);CHKERRQ(ierr);

  ierr = VecDestroy(&vs);CHKERRQ(ierr);
  ierr = VecDestroy(&pf);CHKERRQ(ierr);
  ierr = VecDestroy(&T);CHKERRQ(ierr);
  ierr = VecDestroy(&vl);CHKERRQ(ierr);

  ierr = PetscFree(Xp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PetscOptionGetReal_GeoVel"
PetscErrorCode PetscOptionGetReal_GeoVel(const char pre[],const char opt[],PetscReal *v,PetscReal *v_si,PetscBool *f)
{
  PetscErrorCode ierr;
  char opt_unit[PETSC_MAX_PATH_LEN];
  PetscBool found;
  
  PetscSNPrintf(opt_unit,PETSC_MAX_PATH_LEN-1,"%s::[cm/yr]",opt);
  ierr = PetscOptionsGetReal(NULL,pre,opt_unit,v,&found);CHKERRQ(ierr);
  if (f) { *f = found; }
  
  if (found) {
    if (v_si) {
      *v_si = *v * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PetscOptionGetReal_GeoLength"
PetscErrorCode PetscOptionGetReal_GeoLength(const char pre[],const char opt[],PetscReal *v,PetscReal *v_si,PetscBool *f)
{
  PetscErrorCode ierr;
  char opt_unit[PETSC_MAX_PATH_LEN];
  PetscBool found;
  
  PetscSNPrintf(opt_unit,PETSC_MAX_PATH_LEN-1,"%s::[km]",opt);
  ierr = PetscOptionsGetReal(NULL,pre,opt_unit,v,&found);CHKERRQ(ierr);
  if (f) { *f = found; }
  
  if (found) {
    if (v_si) {
      *v_si = *v * 1.0e3;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SubFUScGlobalFieldMetricMonitor"
PetscErrorCode SubFUScGlobalFieldMetricMonitor(TPCtx c,PetscViewer viewer)
{
  PetscErrorCode ierr;
  PetscReal min,max,l2;
  PetscReal min_up[2],max_up[2],l2_up[2];
  Vec x;
  
  /*
   Formatted output
   [mec][velocity]     : min +1.22222e : max +1.22222e : l2 +1.33333e
   [mec][sub][field]   : min +1.22222e : max +1.22222e : l2 +1.33333e
  */
  
  /* mechanics */
  {
    Vec Xu,Xp;
    
    ierr = DMCompositeGetAccess(c->mechanics->dm_m,c->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
    ierr = VecMin(Xu,NULL,&min_up[0]);CHKERRQ(ierr);
    ierr = VecMax(Xu,NULL,&max_up[0]);CHKERRQ(ierr);
    ierr = VecNorm(Xu,NORM_2,&l2_up[0]);CHKERRQ(ierr);

    ierr = VecMin(Xp,NULL,&min_up[1]);CHKERRQ(ierr);
    ierr = VecMax(Xp,NULL,&max_up[1]);CHKERRQ(ierr);
    ierr = VecNorm(Xp,NORM_2,&l2_up[1]);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(c->mechanics->dm_m,c->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  }

  PetscViewerASCIIPrintf(viewer,"[mech][%17.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","velocity",min_up[0],max_up[0],l2_up[0]);
  PetscViewerASCIIPrintf(viewer,"[mech][%17.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","pressure",min_up[1],max_up[1],l2_up[1]);
  //PetscViewerASCIIPrintf(viewer,"[mech][sub][%12.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","some other thing",min_up[1],max_up[1],l2_up[1]);

  x = c->mechanics->v_bar;
  ierr = VecMin(x,NULL,&min);CHKERRQ(ierr);
  ierr = VecMax(x,NULL,&max);CHKERRQ(ierr);
  ierr = VecNorm(x,NORM_2,&l2);CHKERRQ(ierr);
  PetscViewerASCIIPrintf(viewer,"[mech][sub][%12.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","v_bar(omega)",min,max,l2);

  x = c->mechanics->v_l_hat;
  ierr = VecMin(x,NULL,&min);CHKERRQ(ierr);
  ierr = VecMax(x,NULL,&max);CHKERRQ(ierr);
  ierr = VecNorm(x,NORM_2,&l2);CHKERRQ(ierr);
  PetscViewerASCIIPrintf(viewer,"[mech][sub][%12.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","v_l(wedge)",min,max,l2);

  /* energy */
  x = c->energy->X;
  ierr = VecMin(x,NULL,&min);CHKERRQ(ierr);
  ierr = VecMax(x,NULL,&max);CHKERRQ(ierr);
  ierr = VecNorm(x,NORM_2,&l2);CHKERRQ(ierr);
  PetscViewerASCIIPrintf(viewer,"[engy][%17.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","temperature",min,max,l2);

  x = c->energy->v_bar_e;
  ierr = VecMin(x,NULL,&min);CHKERRQ(ierr);
  ierr = VecMax(x,NULL,&max);CHKERRQ(ierr);
  ierr = VecNorm(x,NORM_2,&l2);CHKERRQ(ierr);
  PetscViewerASCIIPrintf(viewer,"[engy][sub][%12.12s] : min %+1.12e : max %+1.12e : l2 %+1.12e\n","v_bar(omega)",min,max,l2);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPContexSetupTemporalParameters"
PetscErrorCode TPContexSetupTemporalParameters(TPCtx ctx,PetscReal time,PetscReal dt,PetscInt step)
{
  ctx->time = time;
  ctx->dt = dt;
  ctx->step = step;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMTetCreatePartitionedFromTriangleWithRef"
PetscErrorCode DMTetCreatePartitionedFromTriangleWithRef(MPI_Comm comm,const char filename[],PetscInt dof,DMTetBasisType btype,PetscInt border,DM *_dm)
{
  PetscMPIInt commsize,commrank;
  PetscErrorCode ierr;
  DM dm,dmref;
  PetscInt r;
  PetscInt nrefinements = 0;
  
  ierr = MPI_Comm_size(comm,&commsize);CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm,&commrank);CHKERRQ(ierr);
  
  PetscOptionsGetInt(NULL,NULL,"-nref",&nrefinements,NULL);
  if (nrefinements == 0) {
    ierr = DMTetCreatePartitionedFromTriangle(comm,filename,dof,btype,border,&dm);CHKERRQ(ierr);
  } else {
    
    if (commsize > 1) {
      DM dmsequential = NULL;
      
      if (commrank == 0) {
        ierr = DMTetCreate2dFromTriangle(PETSC_COMM_SELF,filename,1,btype,1,&dmsequential);CHKERRQ(ierr);
        ierr = DMSetUp(dmsequential);CHKERRQ(ierr);
        
        for (r=1; r<nrefinements; r++) {
          ierr = DMTetCreateRefinement(dmsequential,0,&dmref);CHKERRQ(ierr);
          ierr = DMDestroy(&dmsequential);CHKERRQ(ierr);
          dmsequential = dmref;
          dmref = NULL;
        }
        
        ierr = DMTetCloneGeometry(dmsequential,dof,btype,border,&dmref);CHKERRQ(ierr);
        ierr = DMDestroy(&dmsequential);CHKERRQ(ierr);
        dmsequential = dmref;
        dmref = NULL;
      }
      ierr = DMTetCreatePartitioned(comm,dmsequential,dof,btype,border,&dm);CHKERRQ(ierr);
      ierr = DMDestroy(&dmsequential);CHKERRQ(ierr);
    } else {
      ierr = DMTetCreate2dFromTriangle(comm,filename,dof,btype,border,&dm);CHKERRQ(ierr);
      ierr = DMSetUp(dm);CHKERRQ(ierr);
      
      for (r=1; r<nrefinements; r++) {
        ierr = DMTetCreateRefinement(dm,0,&dmref);CHKERRQ(ierr);
        ierr = DMDestroy(&dm);CHKERRQ(ierr);
        dm = dmref;
        dmref = NULL;
      }
      
      ierr = DMTetCloneGeometry(dm,dof,btype,border,&dmref);CHKERRQ(ierr);
      ierr = DMDestroy(&dm);CHKERRQ(ierr);
      dm = dmref;
      dmref = NULL;
    }
  }
  ierr = DMSetUp(dm);CHKERRQ(ierr);
  
  *_dm = dm;
  PetscFunctionReturn(0);
}



