
#include <petsc.h>
#include <petscdm.h>
#include <petscsnes.h>
#include <petscdmtet.h>
#include <proj_init_finalize.h>
#include <petscutils.h>
#include <pde.h>
#include <pdedarcy.h>
#include <pdehelmholtz.h>
#include <dmbcs.h>
#include <fe_geometry_utils.h>
#include <element_container.h>
#include <index.h>

#include <tpctx.h>
#include <darcyctx.h>
#include <mec.h>
#include <pointwise_mech_coeff.h>

#define POROSITY_EPSILON 1.0e-30

static PetscErrorCode darcy_a_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);
static PetscErrorCode darcy_f_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);
static PetscErrorCode darcy_g_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);
static PetscErrorCode darcy_p_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);
static PetscErrorCode darcy_u_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);
static PetscErrorCode darcy_u_neumann_gaussian_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);

#undef __FUNCT__
#define __FUNCT__ "DarcyCtxCreate"
PetscErrorCode DarcyCtxCreate(DarcyCtx *ctx)
{
  PetscErrorCode ierr;
  DarcyCtx d;
  
  PetscFunctionBegin;
  ierr = PetscMalloc1(1,&d);CHKERRQ(ierr);
  ierr = PetscMemzero(d,sizeof(struct _p_DarcyCtx));CHKERRQ(ierr);
  d->issetup = PETSC_FALSE;
  d->output_hdf = PETSC_FALSE;
  *ctx = d;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtxDestroy"
PetscErrorCode DarcyCtxDestroy(DarcyCtx *ctx)
{
  PetscErrorCode ierr;
  DarcyCtx d;
  
  PetscFunctionBegin;
  
  if (!ctx) PetscFunctionReturn(0);
  d = *ctx;
  ierr = DMDestroy(&d->dm_ref);CHKERRQ(ierr);
  ierr = DMDestroy(&d->dm_v);CHKERRQ(ierr);
  ierr = DMDestroy(&d->dm_p);CHKERRQ(ierr);
  ierr = DMDestroy(&d->dm_c_phi_T);CHKERRQ(ierr);
  ierr = DMDestroy(&d->dm_adv_v);CHKERRQ(ierr);

  if (d->bc_v) { ierr = BCListDestroy(&d->bc_v);CHKERRQ(ierr); }
  if (d->bc_p) { ierr = BCListDestroy(&d->bc_p);CHKERRQ(ierr); }
  
  ierr = PDEDestroy(&d->pdedarcy);CHKERRQ(ierr);
  ierr = PDEDestroy(&d->pde_cl);CHKERRQ(ierr);
  ierr = PDEDestroy(&d->pde_cs);CHKERRQ(ierr);

  if (d->bc_cl) { ierr = BCListDestroy(&d->bc_cl);CHKERRQ(ierr); }
  if (d->bc_cs) { ierr = BCListDestroy(&d->bc_cs);CHKERRQ(ierr); }

  ierr = SNESDestroy(&d->snesdarcy);CHKERRQ(ierr);
  ierr = MatDestroy(&d->J);CHKERRQ(ierr);
  ierr = VecDestroy(&d->X);CHKERRQ(ierr);
  ierr = VecDestroy(&d->F);CHKERRQ(ierr);

  ierr = SNESDestroy(&d->snes_adv);CHKERRQ(ierr);
  ierr = SNESDestroy(&d->snes_adv_cs);CHKERRQ(ierr);
  ierr = MatDestroy(&d->J_adv);CHKERRQ(ierr);
  ierr = VecDestroy(&d->F_adv);CHKERRQ(ierr);

  ierr = VecDestroy(&d->cl);CHKERRQ(ierr);
  ierr = VecDestroy(&d->cs);CHKERRQ(ierr);
  ierr = VecDestroy(&d->T);CHKERRQ(ierr);
  ierr = VecDestroy(&d->phi);CHKERRQ(ierr);
  ierr = VecDestroy(&d->phi0);CHKERRQ(ierr);
  ierr = VecDestroy(&d->vl);CHKERRQ(ierr);
  ierr = VecDestroy(&d->vbulk);CHKERRQ(ierr);
  ierr = VecDestroy(&d->gamma);CHKERRQ(ierr);
  ierr = VecDestroy(&d->vs);CHKERRQ(ierr);

  ierr = MatDestroy(&d->Ip_m2d);CHKERRQ(ierr);
  
  ierr = MatDestroy(&d->M_dg);CHKERRQ(ierr);
  ierr = KSPDestroy(&d->proj_dg);CHKERRQ(ierr);
  ierr = DMDestroy(&d->dm_dg);CHKERRQ(ierr);
  
  ierr = PetscFree(d);CHKERRQ(ierr);
  *ctx = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "impose_const"
PetscErrorCode impose_const(PetscScalar coor[],PetscInt dof,
                                       PetscScalar *val,PetscBool *constrain,void *data)
{
  PetscReal *constant;

  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type PetscReal* as last argument");
  constant = (PetscReal*)data;
  *constrain = PETSC_TRUE;
  *val = *constant;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "impose_gaussian"
PetscErrorCode impose_gaussian(PetscScalar coor[],PetscInt dof,
                            PetscScalar *val,PetscBool *constrain,void *data)
{
  PetscReal *params,bcvalue;
  PetscReal origin[2],refstate,amp,fac,x,y;
  
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type PetscReal*[5] as last argument");
  params = (PetscReal*)data;
  
  origin[0] = params[0];
  origin[1] = params[1];
  refstate  = params[2];
  amp       = params[3];
  fac       = params[4];
  
  *constrain = PETSC_TRUE;
  
  x = coor[0] - origin[0];
  y = coor[1] - origin[1];
  
  bcvalue = refstate + amp * PetscExpReal(-0.5 * (x*x + y*y)/(fac*fac) );
  
  *val = bcvalue;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SDAdvection_SetInitialCondition_Cl"
PetscErrorCode SDAdvection_SetInitialCondition_Cl(DarcyCtx d)
{
  PetscErrorCode ierr;
  PetscInt ic_type;
  
  /* This should be moved to a SetIC function */
  ic_type = 0;
  ierr = PetscOptionsGetInt(NULL,"component.CH2O.","-ic_type",&ic_type,NULL);CHKERRQ(ierr);
  switch (ic_type) {
    case 0: {
      PetscReal value = 1.0e-3;
      
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-ic.const",&value,NULL);CHKERRQ(ierr);
      
      // CH2O bulk
      ierr = VecSet(d->cl,value);CHKERRQ(ierr);
    }
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only -component.CH2O.ic_type 0 supported");
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SDAdvection_SetInitialCondition_Cs"
PetscErrorCode SDAdvection_SetInitialCondition_Cs(DarcyCtx d)
{
  PetscErrorCode ierr;
  PetscInt ic_type;
  
  ic_type = 0;
  ierr = PetscOptionsGetInt(NULL,"component.CRF.","-ic_type",&ic_type,NULL);CHKERRQ(ierr);
  switch (ic_type) {
    case 0: {
      PetscReal value = 0.8;
      
      ierr = PetscOptionsGetReal(NULL,"component.CRF.","-ic.const",&value,NULL);CHKERRQ(ierr);
      ierr = VecSet(d->cs,value);CHKERRQ(ierr);
    }
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only -component.CRF.ic_type 0 supported");
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SDAdvection_BoundaryConditionSetup_Cl"
PetscErrorCode SDAdvection_BoundaryConditionSetup_Cl(DarcyCtx darcy)
{
  PetscErrorCode ierr;
  PetscReal value1 = 2.0e-3;
  PetscReal value2 = 2.0e-3;
  //PetscReal value3 = 2.0e-3;
  PetscReal CH2O_const_bkg = 2.0e-3;
  PetscInt  bc_type;
  PetscReal params[5];
  
  bc_type = 0;
  ierr = PetscOptionsGetInt(NULL,"component.CH2O.","-bc_type",&bc_type,NULL);CHKERRQ(ierr);
  switch (bc_type) {
    case 0: {
      PetscReal CH2O_slab;
      
      CH2O_slab = 1.0e-3;
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-slab_bc.const",&CH2O_slab,NULL);CHKERRQ(ierr);
      value2 = CH2O_slab;
    }
      break;

    case 1: {

      params[0] = 0.5;  // x,y coordinates
      params[1] = -0.5;
      params[2] = 1.0e-3; // reference value far from the gaussian
      params[3] = 1.0e-3; // amplitude of the gaussian
      params[4] = 0.05;    // parameter controlling the spread of the Gaussian (smaller value yields sharper Gaussian)
      
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-slab_bc.gaussian.origin_x",&params[0],NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-slab_bc.gaussian.origin_y",&params[1],NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-slab_bc.gaussian.refstate",&params[2],NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-slab_bc.gaussian.amp",&params[3],NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-slab_bc.gaussian.sigma",&params[4],NULL);CHKERRQ(ierr);
    }
      break;

    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Valid values for -component.CH2O.bc_type are {0,1}");
      break;
  }
  
  /* A good default would be to use the constant IC value (if that IC was chosen) */
  ierr = PetscOptionsGetReal(NULL,"component.CH2O.","-bc.bkgnd",&CH2O_const_bkg,NULL);CHKERRQ(ierr);
  value1 = CH2O_const_bkg;
  //value3 = CH2O_const_bkg;
  
  ierr = DMGListReset(darcy->dm_c_phi_T,"basis:inflow");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssignFromInflowSubset(darcy->dm_c_phi_T,"edge:inflow_candidate","basis:inflow",darcy->dm_adv_v,darcy->vl);CHKERRQ(ierr);
  //ierr = DMGListView(darcy->dm_c_phi_T);CHKERRQ(ierr);

  ierr = BCListReset(darcy->bc_cl);CHKERRQ(ierr);
  /* 
   With the L2-H1 form, u.n is only imposed weakly, thus even on the slab-wedge boundary, outflow might occur.
   For this reason we avoid imposing Dirichlet values on this interface via the inflow condition
  */

  if (bc_type == 0) {
    ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cl,0,"basis:SlabWedge",impose_const,(void*)&value2);CHKERRQ(ierr);
  } else if (bc_type == 1) {
    ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cl,0,"basis:SlabWedge",impose_gaussian,(void*)params);CHKERRQ(ierr);
  }
  
  ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cl,0,"basis:MantleWedge",impose_const,(void*)&value1);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cl,0,"basis:inflow",impose_const,(void*)&value3);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SDAdvection_BoundaryConditionSetup_Cs"
PetscErrorCode SDAdvection_BoundaryConditionSetup_Cs(DarcyCtx darcy)
{
  PetscErrorCode ierr;
  PetscReal value1 = 0.8;
  PetscReal value2 = 0.8;
  //PetscReal value3 = 0.8;
  PetscReal CRF_const_bkg = 0.8;
  PetscInt  bc_type;
  
  
  bc_type = 0;
  ierr = PetscOptionsGetInt(NULL,"component.CRF.","-bc_type",&bc_type,NULL);CHKERRQ(ierr);
  switch (bc_type) {
    case 0: {
      PetscReal CRF_slab;
      
      CRF_slab = 0.8;
      ierr = PetscOptionsGetReal(NULL,"component.CRF.","-slab_bc.const",&CRF_slab,NULL);CHKERRQ(ierr);
      value2 = CRF_slab;
    }
      break;
      
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Valid values for -component.CRF.bc_type are {0}");
      break;
  }
  
  /* A good default would be to use the constant IC value (if that IC was chosen) */
  ierr = PetscOptionsGetReal(NULL,"component.CRF.","-bc.bkgnd",&CRF_const_bkg,NULL);CHKERRQ(ierr);
  value1 = CRF_const_bkg;
  //value3 = CRF_const_bkg;
  
  ierr = DMGListReset(darcy->dm_c_phi_T,"basis:inflow");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssignFromInflowSubset(darcy->dm_c_phi_T,"edge:inflow_candidate","basis:inflow",darcy->dm_adv_v,darcy->vs);CHKERRQ(ierr);
  //ierr = DMGListView(darcy->dm_c_phi_T);CHKERRQ(ierr);
  
  ierr = BCListReset(darcy->bc_cs);CHKERRQ(ierr);
  /*
   With the L2-H1 form, u.n is only imposed weakly, thus even on the slab-wedge boundary, outflow might occur.
   For this reason we avoid imposing Dirichlet values on this interface via the inflow condition
   */
  ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cs,0,"basis:SlabWedge",impose_const,(void*)&value2);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cs,0,"basis:PlateWedge",impose_const,(void*)&value1);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cs,0,"basis:MantleWedge",impose_const,(void*)&value1);CHKERRQ(ierr);
  //ierr = DMTetBCListTraverseDMTetGList(darcy->bc_cs,0,"basis:inflow",impose_const,(void*)&value3);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtxSetUp"
PetscErrorCode DarcyCtxSetUp(DarcyCtx d,TPCtx tpctx)
{
  PetscErrorCode ierr;
  DM             dmS,dm_vs_m;
  Coefficient    a,f,g,uN,pN;
  PetscInt       pk_v,pk_p;
  //Mat            interp_tet2tet;
  
  
  PetscFunctionBegin;
  if (d->issetup) PetscFunctionReturn(0);
  
  d->tpctx = tpctx;
  
  dm_vs_m = tpctx->mechanics->dm_v_hat;
  //dm_pf_m = tpctx->mechanics->dm_p_hat;
  
  d->kappa_cl = 1.0e-1;
  d->kappa_cs = 1.0e-1;
  d->kappa_phi = 1.0e-1;
  ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.cl",&d->kappa_cl,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.cs",&d->kappa_cs,NULL);CHKERRQ(ierr);
  d->kappa_phi = d->kappa_cl;
  ierr = PetscOptionsGetReal(NULL,NULL,"-darcy.adv.kappa.phi",&d->kappa_phi,NULL);CHKERRQ(ierr);
  
  {
    //DM dm;
    
    //ierr = DMTetCloneGeometry(dm_vs_m,1,DMTET_CWARP_AND_BLEND,1,&dm);CHKERRQ(ierr);
    //ierr = DMTetCreateRefinement(dm,1,&d->dm_ref);CHKERRQ(ierr);
    //ierr = DMDestroy(&dm);CHKERRQ(ierr);
    
    ierr = DMTetCloneGeometry(dm_vs_m,1,DMTET_CWARP_AND_BLEND,1,&d->dm_ref);CHKERRQ(ierr);
  }
  
  /* create the dms for v,p */
  pk_v = 2;
  ierr = PetscOptionsGetInt(NULL,NULL,"-dcy_pk_v",&pk_v,NULL);CHKERRQ(ierr);
  pk_p = pk_v - 1;
  ierr = PetscOptionsGetInt(NULL,NULL,"-dcy_pk_p",&pk_p,NULL);CHKERRQ(ierr);
  //ierr = DMTetCloneGeometry(d->dm_ref,2,DMTET_CWARP_AND_BLEND,pk_v,&d->dm_v);CHKERRQ(ierr); //Darcy-Hdiv
  //ierr = DMTetCloneGeometry(d->dm_ref,1,DMTET_DG,pk_p,&d->dm_p);CHKERRQ(ierr); //Darcy-Hdiv
  ierr = DMTetCloneGeometry(d->dm_ref,2,DMTET_DG,pk_v,&d->dm_v);CHKERRQ(ierr); //Darcy-L2
  ierr = DMTetCloneGeometry(d->dm_ref,1,DMTET_CWARP_AND_BLEND,pk_p,&d->dm_p);CHKERRQ(ierr); //Darcy-L2
  
  //ierr = DMTetBFacetSetup(d->dm_v);CHKERRQ(ierr); /* required for surface integration p.n */ //Darcy-Hdiv
  ierr = DMTetBFacetSetup(d->dm_p);CHKERRQ(ierr); /* required for surface integration U.n */ //Darcy-L2

  ierr = DMConfigureWedgeBoundaries_Default(d->dm_p);CHKERRQ(ierr);
  
  ierr = DMBCListCreate(d->dm_v,&d->bc_v);CHKERRQ(ierr);
  d->bc_p = NULL;
  ierr = DMBCListCreate(d->dm_p,&d->bc_p);CHKERRQ(ierr);
  
  //ierr = PDECreateDarcy(PETSC_COMM_WORLD,d->dm_v,d->dm_p,d->bc_v,NULL,DARCY_BFORM_HDIVL2,&d->pdedarcy);CHKERRQ(ierr); //Darcy-Hdiv
  ierr = PDECreateDarcy(PETSC_COMM_WORLD,d->dm_v,d->dm_p,NULL,NULL,DARCY_BFORM_L2H1,&d->pdedarcy);CHKERRQ(ierr); //Darcy-L2
  ierr = PDESetOptionsPrefix(d->pdedarcy,"dcy_");CHKERRQ(ierr);
  
  /* inter-grid transfer operators */
  //ierr = DMCreateInterpolation(dm_vs_m,d->dm_v,&interp_tet2tet,NULL);CHKERRQ(ierr); //Darcy-Hdiv
  //ierr = MatCreateMAIJ(interp_tet2tet,2,&d->Iv_m2d);CHKERRQ(ierr); //Darcy-Hdiv
  //ierr = MatDestroy(&interp_tet2tet);CHKERRQ(ierr); //Darcy-Hdiv
  
  /* // Darcy-Hdiv
  {
    PetscReal  *q_coor;
    PetscInt   nfacets,nqp;
    Quadrature quadrature_s;

    ierr = PDEDarcyGetSCoefficients(d->pdedarcy,NULL,&pN);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(pN,&quadrature_s);CHKERRQ(ierr);
    ierr = QuadratureGetRule(quadrature_s,&nqp,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature_s,"coor",&nfacets,&nqp,NULL,&q_coor);CHKERRQ(ierr);

    ierr = DMCreateInterpolationFromCoords(dm_pf_m,1,nfacets*nqp,(const PetscScalar*)q_coor,PETSC_TRUE,&d->Ip_m2d);CHKERRQ(ierr);
  }
  */

  // Darcy-L2
  {
    PetscReal  *q_coor;
    PetscInt   nfacets,nqp;
    Quadrature quadrature_s;
    
    ierr = PDEDarcyGetSCoefficients(d->pdedarcy,&uN,NULL);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(uN,&quadrature_s);CHKERRQ(ierr);
    ierr = QuadratureGetRule(quadrature_s,&nqp,NULL,NULL);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature_s,"coor",&nfacets,&nqp,NULL,&q_coor);CHKERRQ(ierr);
    
    ierr = DMCreateInterpolationFromCoords(dm_vs_m,2,nfacets*nqp,(const PetscScalar*)q_coor,PETSC_FALSE,&d->Ip_m2d);CHKERRQ(ierr);
  }

  ierr = PDEDarcyGetDM(d->pdedarcy,&dmS);CHKERRQ(ierr);
  ierr = DMCreateMatrix(dmS,&d->J);CHKERRQ(ierr);
  ierr = MatCreateVecs(d->J,&d->X,&d->F);CHKERRQ(ierr);
  
  ierr = PDESetSolution(d->pdedarcy,d->X);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(d->dm_v,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(d->dm_p,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(d->pdedarcy,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }

  /* volume coefficients */
  ierr = PDEDarcyGetVCoefficients(d->pdedarcy,&a,&f,&g);CHKERRQ(ierr);
  {
    Quadrature quadrature;
    PetscInt nelements;
    
    ierr = CoefficientGetQuadrature(a,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureGetSizes(quadrature,NULL,NULL,NULL,&nelements);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"phi",1);CHKERRQ(ierr);
  }
  
  /* surface coefficients */
  ierr = PDEDarcyGetSCoefficients(d->pdedarcy,&uN,NULL);CHKERRQ(ierr);
  {
    Quadrature quadrature;
    PetscInt nelements;
    
    ierr = CoefficientGetQuadrature(uN,&quadrature);CHKERRQ(ierr);
    ierr = QuadratureGetSizes(quadrature,NULL,NULL,NULL,&nelements);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements,"phi",1);CHKERRQ(ierr);
  }

  
  /* inverse permability */
  ierr = CoefficientSetComputeQuadrature(a,darcy_a_evaluate_quadrature,NULL,(void*)d);CHKERRQ(ierr);
  /* u-momentum right hand side */
  ierr = CoefficientSetComputeQuadrature(f,darcy_f_evaluate_quadrature,NULL,(void*)d);CHKERRQ(ierr);
  /* p-continuity right hand side */
  ierr = CoefficientSetComputeQuadrature(g,darcy_g_evaluate_quadrature,NULL,(void*)d);CHKERRQ(ierr);
  /* surface coefficients */
  ierr = PDEDarcyGetSCoefficients(d->pdedarcy,&uN,&pN);CHKERRQ(ierr);
  {
    PetscInt bc_type = 1;
    
    ierr = PetscOptionsGetInt(NULL,NULL,"-mechanics.darcy_flux.bc_type",&bc_type,NULL);CHKERRQ(ierr);
    
    //ierr = CoefficientSetComputeQuadrature(pN,darcy_p_neumann_evaluate_quadrature,NULL,(void*)d);CHKERRQ(ierr); //Darcy-Hdiv
    if (bc_type == 1) {
      ierr = CoefficientSetComputeQuadrature(uN,darcy_u_neumann_evaluate_quadrature,NULL,(void*)d);CHKERRQ(ierr); //Darcy-L2
    } else if (bc_type == 2) {
      ierr = CoefficientSetComputeQuadrature(uN,darcy_u_neumann_gaussian_evaluate_quadrature,NULL,(void*)d);CHKERRQ(ierr); //Darcy-L2
    }
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&d->snesdarcy);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(d->pdedarcy,d->snesdarcy,d->F,d->J,d->J);CHKERRQ(ierr);
  ierr = SNESSetType(d->snesdarcy,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(d->snesdarcy);CHKERRQ(ierr);
  
  ierr = SaddlePointSetConstantNullSpace_SNES(d->snesdarcy);CHKERRQ(ierr);
  
  ierr = DMTetCloneGeometry(d->dm_ref,1,DMTET_CWARP_AND_BLEND,pk_v,&d->dm_c_phi_T);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(d->dm_c_phi_T);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_c_phi_T,&d->cl);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_c_phi_T,&d->cs);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_c_phi_T,&d->phi);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_c_phi_T,&d->phi0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_c_phi_T,&d->T);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_c_phi_T,&d->gamma);CHKERRQ(ierr);

  ierr = DMTetCloneGeometry(d->dm_ref,2,DMTET_CWARP_AND_BLEND,pk_v,&d->dm_adv_v);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_adv_v,&d->vl);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_adv_v,&d->vbulk);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(d->dm_adv_v,&d->vs);CHKERRQ(ierr);
  
  //ierr = DMCreateInterpolation(tpctx->mechanics->dm_v_hat,d->dm_v,&d->IT_e2d,NULL);CHKERRQ(ierr);

/*
#if 1
  PetscReal value1 = 1.4;
  PetscReal value2 = 2.4;
  PetscReal value3 = 3.4;
  PetscReal value4 = 4.4;
  BCList bcH2O;

  ierr = DMConfigureWedgeBoundaries(d->dm_c_phi_T);CHKERRQ(ierr);
  ierr = DMBCListCreate(d->dm_c_phi_T,&bcH2O);CHKERRQ(ierr);
  
  ierr = DMTetBCListTraverseDMTetGList(bcH2O,0,"basis:SlabWedge",impose_const,(void*)&value1);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseDMTetGList(bcH2O,0,"basis:MantleWedge",impose_const,(void*)&value4);CHKERRQ(ierr);

  ierr = BCListInsert(bcH2O,d->cl);CHKERRQ(ierr);
  {
    const char *fields[] = { "C_H2O_bulk" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"cH2O_field_");CHKERRQ(ierr);
    vecs[0] = d->cl;
    ierr = DMTetViewFields_VTU(d->dm_c_phi_T,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  exit(0);
#endif
*/
  
  ierr = DMConfigureWedgeBoundaries(d->dm_c_phi_T);CHKERRQ(ierr);
  ierr = DMBCListCreate(d->dm_c_phi_T,&d->bc_cl);CHKERRQ(ierr);
  ierr = DMBCListCreate(d->dm_c_phi_T,&d->bc_cs);CHKERRQ(ierr);

  ierr = SDAdvectionPDECreate("adv_",d->dm_c_phi_T,d->bc_cl,d->dm_adv_v,d->vl,1.0e-2,d->kappa_cl,&d->pde_cl);CHKERRQ(ierr);
  ierr = SDAdvectionPDECreate("adv_",d->dm_c_phi_T,d->bc_cs,d->dm_adv_v,d->vs,1.0e-2,d->kappa_cs,&d->pde_cs);CHKERRQ(ierr);

  ierr = DMCreateMatrix(d->dm_c_phi_T,&d->J_adv);CHKERRQ(ierr);
  ierr = MatCreateVecs(d->J_adv,NULL,&d->F_adv);CHKERRQ(ierr);
  ierr = PDESetSolution(d->pde_cl,d->cl);CHKERRQ(ierr);
  ierr = PDESetSolution(d->pde_cs,d->cs);CHKERRQ(ierr);

  {
    Vec x;
    ierr = DMCreateLocalVector(d->dm_c_phi_T,&x);CHKERRQ(ierr);
    ierr = PDESetLocalSolution(d->pde_cl,x);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
    
    ierr = DMCreateLocalVector(d->dm_c_phi_T,&x);CHKERRQ(ierr);
    ierr = PDESetLocalSolution(d->pde_cs,x);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  

  ierr = SNESCreate(PETSC_COMM_WORLD,&d->snes_adv);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(d->pde_cl,d->snes_adv,d->F_adv,d->J_adv,d->J_adv);CHKERRQ(ierr);
  ierr = SNESSetType(d->snes_adv,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(d->snes_adv);CHKERRQ(ierr);

  ierr = SNESCreate(PETSC_COMM_WORLD,&d->snes_adv_cs);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(d->pde_cs,d->snes_adv_cs,d->F_adv,d->J_adv,d->J_adv);CHKERRQ(ierr);
  ierr = SNESSetType(d->snes_adv_cs,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(d->snes_adv_cs);CHKERRQ(ierr);

  /* setup for gamma reconstruction */
  ierr = DMTetCloneGeometry(d->dm_c_phi_T,1,DMTET_DG,pk_v,&d->dm_dg);CHKERRQ(ierr);

  ierr = AssembleBForm_VectorMassMatrix(d->dm_dg,1,MAT_INITIAL_MATRIX,&d->M_dg);CHKERRQ(ierr);
  ierr = KSPCreate(PETSC_COMM_WORLD,&d->proj_dg);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(d->proj_dg,"proj_f_");CHKERRQ(ierr);
  ierr = KSPSetOperators(d->proj_dg,d->M_dg,d->M_dg);CHKERRQ(ierr);
  {
    PC pc;
    
    ierr = KSPGetPC(d->proj_dg,&pc);CHKERRQ(ierr);
    ierr = KSPSetType(d->proj_dg,KSPPREONLY);CHKERRQ(ierr);
    ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
  }
  ierr = KSPSetFromOptions(d->proj_dg);CHKERRQ(ierr);

  
  d->issetup = PETSC_TRUE;

  PetscFunctionReturn(0);
}

// a_evaluate_quadrature()
// - evaluate the permability
//perm:     k[p]  = c->k0_ * PetscPowReal((aux_phi[p]/0.01),c->n_);
//inv perm: ik[p] = (1.0/c->k0_) * PetscPowReal((aux_phi[p]/0.01),-c->n_);
#undef __FUNCT__
#define __FUNCT__ "darcy_a_evaluate_quadrature"
static PetscErrorCode darcy_a_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt       q,e,nen,nqp;
  PetscReal      *coeff,*coor;
  PetscReal      scale_fac;
  DarcyCtx       darcyctx;
  TPCtx          tpctx;
  PetscReal      phi_q,k_q;
  const PetscScalar *LA_phi;
  PetscScalar    *coeff_phi;
  EContainer     ele_container;
  PetscInt       *element,nbasis;
  PetscReal      k0_,n_;
  PetscBool      using_siunits;
  PetscReal      phi_range[2],k_range[2],a_range[2];
  PetscReal      vals[3],min_g[3],max_g[3];
  Vec            phi_local;
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  tpctx = darcyctx->tpctx;
  
  ierr = kPorosityDependentGetParams(tpctx->mechanics->coeff_container->pwp_k_on_mu,&k0_,&n_);CHKERRQ(ierr);
  using_siunits = PETSC_FALSE;
  ierr = PWPhysicsGetUseSI(tpctx->mechanics->coeff_container->pwp_k_on_mu,&using_siunits);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"phi",&nen,&nqp,NULL,&coeff_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"a",&nen,&nqp,NULL,&coeff);CHKERRQ(ierr);
  ierr = EContainerCreate(darcyctx->dm_c_phi_T,quadrature,&ele_container);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(darcyctx->dm_c_phi_T,&nen,&nbasis,&element,0,0,&coor);CHKERRQ(ierr);

  ierr = DMGetLocalVector(darcyctx->dm_c_phi_T,&phi_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcyctx->dm_c_phi_T,darcyctx->phi,INSERT_VALUES,phi_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcyctx->dm_c_phi_T,  darcyctx->phi,INSERT_VALUES,phi_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(phi_local,&LA_phi);CHKERRQ(ierr);

  phi_range[0] = 1.0e32;
  phi_range[1] = -1.0e32;
  for (q=0; q<nen*nqp; q++) {
    phi_range[0] = PetscMin(phi_range[0],coeff_phi[q]);
    phi_range[1] = PetscMax(phi_range[1],coeff_phi[q]);
  }
  for (q=0; q<2; q++) {
    k_range[q] = k0_ * PetscPowReal((phi_range[q]/0.01),n_); // assumed that we are using si units
    if (using_siunits) {
      k_range[q] /= tpctx->K;
    }
  }
  a_range[0] = 1.0/k_range[1];
  a_range[1] = 1.0/k_range[0];

  vals[0] = phi_range[0];
  vals[1] = k_range[0];
  vals[2] = a_range[0];
  ierr = MPI_Allreduce(vals,min_g,3,MPIU_REAL,MPIU_MIN,PETSC_COMM_WORLD);CHKERRQ(ierr);
  vals[0] = phi_range[1];
  vals[1] = k_range[1];
  vals[2] = a_range[1];
  ierr = MPI_Allreduce(vals,max_g,3,MPIU_REAL,MPIU_MAX,PETSC_COMM_WORLD);CHKERRQ(ierr);
  
  phi_range[0] = min_g[0];
  k_range[0]   = min_g[1];
  a_range[0]   = min_g[2];
  phi_range[1] = max_g[0];
  k_range[1]   = max_g[1];
  a_range[1]   = max_g[2];
  
  PetscPrintf(PETSC_COMM_WORLD,"phi_{min,max} %+1.4e : %+1.4e\n",phi_range[0],phi_range[1]);
  PetscPrintf(PETSC_COMM_WORLD,"k*_{min,max}  %+1.4e : %+1.4e\n",k_range[0],k_range[1]);
  PetscPrintf(PETSC_COMM_WORLD,"a_{min,max}   %+1.4e : %+1.4e\n",a_range[0],a_range[1]);
  if (phi_range[0] < POROSITY_EPSILON) {
    a_range[1] = 1.0e8 * a_range[0]; /* 1e8 is an arbitrary choice but one I think the solver will cope with */
    PetscPrintf(PETSC_COMM_WORLD,"a_{min,max*}   %+1.4e : %+1.4e\n",a_range[0],a_range[1]);
  }
  
  
  for (e=0; e<nen; e++) {
    
    for (q=0; q<nqp; q++) {

      coeff[e*nqp + q] = 0.0;
      
      //scale_fac = 0.5*(1.0 + PetscTanhReal(-(avg_c[0]-0.8)/1.3e-2));
      scale_fac = 1.0;
      
      phi_q = coeff_phi[e*nqp+q];
      if (phi_q < 0.0) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Negative porosity is not dealt with - aborting due to a lack of regularisation");
      }
      //phi_q = phi_q * scale_fac;
      
      k_q = k0_ * PetscPowReal((phi_q/0.01),n_); // assumed that we are using si units
      
      k_q = k_q * scale_fac;
      
      if (using_siunits) {
        k_q /= tpctx->K;
      }
      
      //coeff[e*nqp + q] = 1.0/( k_q + 1.0/500.0 );
      if (phi_q < POROSITY_EPSILON) { /* this would imply k* = 0 thus we cannot take a reciprocal */
        coeff[e*nqp + q] = a_range[1];
      } else {
        coeff[e*nqp + q] = 1.0/k_q;
      }
    }
  }
  ierr = VecRestoreArrayRead(phi_local,&LA_phi);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcyctx->dm_c_phi_T,&phi_local);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


// darcy_f_evaluate_quadrature()
#undef __FUNCT__
#define __FUNCT__ "darcy_f_evaluate_quadrature"
static PetscErrorCode darcy_f_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PDE            pde;
  Coefficient    a;
  Quadrature     quadrature_a;
  PetscInt       q,e,nen,nqp;
  PetscReal      *coeff,*coeff_a,*coeff_phi;
  PetscReal      delta_rho;
  DarcyCtx       darcyctx;
  TPCtx          tpctx;
  PetscReal      k0_,n_;
  PetscBool      using_siunits;
  
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  tpctx = darcyctx->tpctx;

  delta_rho = tpctx->rho_s - tpctx->rho_l;

  ierr = QuadratureGetProperty(quadrature,"f",&nen,&nqp,NULL,&coeff);CHKERRQ(ierr);
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEDarcyGetVCoefficients(pde,&a,NULL,NULL);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(a,&quadrature_a);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_a,"a",&nen,&nqp,NULL,&coeff_a);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature_a,"phi",&nen,&nqp,NULL,&coeff_phi);CHKERRQ(ierr);

  ierr = kPorosityDependentGetParams(tpctx->mechanics->coeff_container->pwp_k_on_mu,&k0_,&n_);CHKERRQ(ierr);
  using_siunits = PETSC_FALSE;
  ierr = PWPhysicsGetUseSI(tpctx->mechanics->coeff_container->pwp_k_on_mu,&using_siunits);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    for (q=0; q<nqp; q++) { /* requires a k/mu factor out the front */
      PetscReal phi_q,kstar;
      
      phi_q = coeff_phi[e*nqp+q];
      if (phi_q < 0.0) {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Negative porosity is not dealt with - aborting due to a lack of regularisation");
      }
      
      kstar = k0_ * PetscPowReal((phi_q/0.01),n_); // assumed that we are using si units
      if (using_siunits) {
        kstar /= tpctx->K;
      }
      
      /*kstar = 1.0/coeff_a[e*nqp + q];*/
      coeff[2*(e*nqp + q)+0] = -kstar * delta_rho * tpctx->g[0];
      coeff[2*(e*nqp + q)+1] = -kstar * delta_rho * tpctx->g[1];
    }
  }
  PetscFunctionReturn(0);
}

// darcy_g_evaluate_quadrature()
// - need something div(vs)
#undef __FUNCT__
#define __FUNCT__ "darcy_g_evaluate_quadrature"
static PetscErrorCode darcy_g_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode  ierr;
  PetscInt        q,e,i,nqp;
  PetscReal       *coeff,g,*coords_g,*elcoords_g,*elvel;
  PetscInt        *element,*element_g,nen,nbasis,nbasis_g;
  PetscReal       **GNxi,**GNeta,**GNx,**GNy,detJ_affine;
  EContainer      ele_container;
  const PetscReal *LA_vs;
  Vec             Xu,Xp,vs,vs_local;
  DM              dm;
  DarcyCtx        darcyctx;
  TPCtx           tpctx;
  
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  tpctx = darcyctx->tpctx;
  dm = darcyctx->dm_v;
  
  ierr = QuadratureGetProperty(quadrature,"g",&nen,&nqp,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,0,0,0);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dm,NULL,&nbasis_g,&element_g,NULL,NULL,&coords_g);CHKERRQ(ierr);

  ierr = EContainerCreate(dm,quadrature,&ele_container);CHKERRQ(ierr);
  GNxi  = ele_container->dNr1;
  GNeta = ele_container->dNr2;
  GNx   = ele_container->dNx1;
  GNy   = ele_container->dNx2;

  ierr = PetscMalloc1(2*nbasis,&elvel);CHKERRQ(ierr);
  ierr = PetscMalloc1(2*nbasis_g,&elcoords_g);CHKERRQ(ierr);
  
  //ierr = MatCreateVecs(darcyctx->Iv_m2d,NULL,&vs);CHKERRQ(ierr);  //Darcy-Hdiv
  
  ierr = DMCompositeGetAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  //ierr = MatMult(darcyctx->Iv_m2d,Xu,vs);CHKERRQ(ierr);  //Darcy-Hdiv
  vs = NULL;
  ierr = DMTetProjectField(tpctx->mechanics->dm_v_hat,Xu,PRJTYPE_NATIVE,dm,&vs);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&vs_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,vs,INSERT_VALUES,vs_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,  vs,INSERT_VALUES,vs_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(vs_local,&LA_vs);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[nbasis*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elvel[2*i+0] = LA_vs[2*nidx+0];
      elvel[2*i+1] = LA_vs[2*nidx+1];
    }

    /* get coordinates */
    for (i=0; i<nbasis_g; i++) {
      PetscInt nidx = element_g[nbasis_g*e+i];
      
      elcoords_g[2*i + 0] = coords_g[2*nidx+0];
      elcoords_g[2*i + 1] = coords_g[2*nidx+1];
    }
    
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis,elcoords_g,GNxi,GNeta,GNx,GNy,&detJ_affine);

    for (q=0; q<nqp; q++) {
      PetscReal dv_qp[2];
      
      /* get x,y coords at each quadrature point */
      dv_qp[0] = dv_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        dv_qp[0] += GNx[q][i] * elvel[2*i+0];
        dv_qp[1] += GNy[q][i] * elvel[2*i+1];
      }
      g = (dv_qp[0] + dv_qp[1]);
      //printf("I[div(u)] = %+1.8e\n",g);
      
      coeff[e*nqp + q] = g;
    }
  }
  ierr = VecRestoreArrayRead(vs_local,&LA_vs);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&vs_local);CHKERRQ(ierr);
  ierr = VecDestroy(&vs);CHKERRQ(ierr);
  ierr = PetscFree(elvel);CHKERRQ(ierr);
  ierr = PetscFree(elcoords_g);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

//<MPI - unused - ignore>
// darcy_p_neumann_evaluate_quadrature()
// - interpolate pf onto surface quadrature points
#undef __FUNCT__
#define __FUNCT__ "darcy_p_neumann_evaluate_quadrature"
static PetscErrorCode darcy_p_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt       q,e,pcnt,ncomp_q;
  PetscReal      *q_coeff;
  PetscInt       nfacets,nqp;
  DarcyCtx       darcyctx;
  TPCtx          tpctx;
  Vec            psurf,Xu,Xp;
  const PetscScalar *LA_psurf;
  
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  tpctx = darcyctx->tpctx;
  
  ierr = MatCreateVecs(darcyctx->Ip_m2d,NULL,&psurf);CHKERRQ(ierr);
  
  ierr = DMCompositeGetAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  ierr = MatMult(darcyctx->Ip_m2d,Xp,psurf);CHKERRQ(ierr);
  //VecView(Xp,PETSC_VIEWER_STDOUT_WORLD);
  ierr = DMCompositeRestoreAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"pN",&nfacets,&nqp,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = VecGetArrayRead(psurf,&LA_psurf);CHKERRQ(ierr);
  
  pcnt = 0;
  for (e=0; e<nfacets; e++) {
    for (q=0; q<nqp; q++) {
      PetscReal pressure;
      
      pressure = LA_psurf[pcnt];
      q_coeff[e*nqp + q] = pressure;
      //printf("R{I[p]} = %+1.8e\n",pressure);
      pcnt++;
    }
  }
  ierr = VecRestoreArrayRead(psurf,&LA_psurf);CHKERRQ(ierr);
  ierr = VecDestroy(&psurf);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

//<MPI - looks fine>
// darcy_u_neumann_evaluate_quadrature()
// - interpolate us onto surface quadrature points
#undef __FUNCT__
#define __FUNCT__ "darcy_u_neumann_evaluate_quadrature"
static PetscErrorCode darcy_u_neumann_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt       q,e,pcnt,ncomp_q;
  PetscReal      *q_coeff,*q_coor,*q_coeff_phi;
  PetscInt       nfacets,nqp;
  DarcyCtx       darcyctx;
  TPCtx          tpctx;
  Vec            usurf,Xu,Xp;
  const PetscScalar *LA_usurf;
  PetscReal      delta_rho;
  PetscReal      k0_,n_;
  PetscBool      using_siunits,issetup;
  PetscReal      *normal,*tangent;
  BFacetList     facets;
  DM             dm;
  PetscReal      phi_range[2],k_range[2],vals[2],min_g[2],max_g[2];

  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  tpctx = darcyctx->tpctx;
  
  ierr = MatCreateVecs(darcyctx->Ip_m2d,NULL,&usurf);CHKERRQ(ierr);
  
  ierr = DMCompositeGetAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  ierr = MatMult(darcyctx->Ip_m2d,Xu,usurf);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"uN",&nfacets,&nqp,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",NULL,NULL,NULL,&q_coor);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",NULL,NULL,NULL,&q_coeff_phi);CHKERRQ(ierr);
  ierr = VecGetArrayRead(usurf,&LA_usurf);CHKERRQ(ierr);
  
  dm = darcyctx->dm_p;
  ierr = DMTetGetSpaceBFacetList(dm,&facets);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(facets,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetOrientations(facets,&normal,&tangent);CHKERRQ(ierr);
  
  delta_rho = tpctx->rho_s - tpctx->rho_l;

  ierr = kPorosityDependentGetParams(tpctx->mechanics->coeff_container->pwp_k_on_mu,&k0_,&n_);CHKERRQ(ierr);
  using_siunits = PETSC_FALSE;
  ierr = PWPhysicsGetUseSI(tpctx->mechanics->coeff_container->pwp_k_on_mu,&using_siunits);CHKERRQ(ierr);

  phi_range[0] = 1.0e32;
  phi_range[1] = -1.0e32;
  for (q=0; q<nfacets*nqp; q++) {
    phi_range[0] = PetscMin(phi_range[0],q_coeff_phi[q]);
    phi_range[1] = PetscMax(phi_range[1],q_coeff_phi[q]);
  }
  for (q=0; q<2; q++) {
    k_range[q] = k0_ * PetscPowReal((phi_range[q]/0.01),n_); // assumed that we are using si units
    if (using_siunits) {
      k_range[q] /= tpctx->K;
    }
  }
  
  vals[0] = phi_range[0];
  vals[1] = k_range[0];
  ierr = MPI_Allreduce(vals,min_g,2,MPIU_REAL,MPIU_MIN,PETSC_COMM_WORLD);CHKERRQ(ierr);
  vals[0] = phi_range[1];
  vals[1] = k_range[1];
  ierr = MPI_Allreduce(vals,max_g,2,MPIU_REAL,MPIU_MAX,PETSC_COMM_WORLD);CHKERRQ(ierr);
  
  phi_range[0] = min_g[0];
  k_range[0]   = min_g[1];
  phi_range[1] = max_g[0];
  k_range[1]   = max_g[1];

  PetscPrintf(PETSC_COMM_WORLD,"[surf] phi_{min,max} %+1.4e : %+1.4e\n",phi_range[0],phi_range[1]);
  PetscPrintf(PETSC_COMM_WORLD,"[surf] k*_{min,max}  %+1.4e : %+1.4e\n",k_range[0],k_range[1]);

  pcnt = 0;
  for (e=0; e<nfacets; e++) {
    for (q=0; q<nqp; q++) {
      PetscReal *x_q,phi_q,k_q,scale_fac;
      
      x_q = &q_coor[2*(e*nqp + q)];
      //printf("%+1.6e %+1.6e - boundary\n",x_q[0],x_q[1]);

      //scale_fac = 0.5*(1.0 + PetscTanhReal(-(x_q[0]-0.6)/1.3e-2));
      scale_fac = 1.0;

      phi_q = q_coeff_phi[e*nqp + q];
      
      k_q = k0_ * PetscPowReal((phi_q/0.01),n_); // assumed that we are using si units
      if (using_siunits) {
        k_q /= tpctx->K;
      }
      
      q_coeff[2*(e*nqp + q) + 0] = 0.0;
      q_coeff[2*(e*nqp + q) + 1] = 0.0;
      
      /* compute v_D = k* delta_rho g */
      if (-x_q[1]-1.0e-6 < PLATE_THICKNESS) {
        PetscReal vD[2];

        vD[0] = -k_q * delta_rho * (- tpctx->g[0]);
        vD[1] = -k_q * delta_rho * (- tpctx->g[1]);
        
        //q_coeff[2*(e*nqp + q) + 0] = 0.0 * scale_fac;
        //q_coeff[2*(e*nqp + q) + 1] = +2.5097e-03 * scale_fac; /* positive for outflow */
        
        q_coeff[2*(e*nqp + q) + 0] = vD[0] * scale_fac;
        q_coeff[2*(e*nqp + q) + 1] = vD[1] * scale_fac;
        
        //printf("vd[top] %+1.4e %+1.4e : q_coeff_vd %+1.4e %+1.4e\n",vD[0],vD[1],q_coeff[2*(e*nqp + q) + 0],q_coeff[2*(e*nqp + q) + 1]);
        continue;
      }
      
      PetscReal depth,slab_dip;
      slab_dip = SLAB_DIP * PETSC_PI / 180.0;
      depth = -x_q[1];
      
      if (depth > (x_q[0]*PetscTanReal(slab_dip) - 1.0e-6)) { /* slab-wedge */
        PetscReal vD[2],alpha;
        
        alpha = tpctx->Vdarcy / (-normal[2*e+0] - normal[2*e+1]);
        vD[0] = alpha;
        vD[1] = alpha;
        
        //q_coeff[2*(e*nqp + q) + 0] = +1.4142e-02*scale_fac;
        //q_coeff[2*(e*nqp + q) + 1] = +1.4142e-02*scale_fac; /* positive for inflow */
        
        q_coeff[2*(e*nqp + q) + 0] = vD[0] * scale_fac;
        q_coeff[2*(e*nqp + q) + 1] = vD[1] * scale_fac;
        //printf("vd %+1.4e %+1.4e : q_coeff_vd %+1.4e %+1.4e\n",vD[0],vD[1],q_coeff[2*(e*nqp + q) + 0],q_coeff[2*(e*nqp + q) + 1]);
      }
      
      pcnt++;
    }
  }
  ierr = VecRestoreArrayRead(usurf,&LA_usurf);CHKERRQ(ierr);
  ierr = VecDestroy(&usurf);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

//<MPI - looks fine>
#undef __FUNCT__
#define __FUNCT__ "darcy_u_neumann_gaussian_evaluate_quadrature"
static PetscErrorCode darcy_u_neumann_gaussian_evaluate_quadrature(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscInt       q,e,k,ncomp_q;
  PetscReal      *q_coeff,*q_coor,*q_coeff_phi;
  PetscInt       nfacets,nqp,nfacets_region;
  DarcyCtx       darcyctx;
  TPCtx          tpctx;
  Vec            usurf,Xu,Xp;
  const PetscScalar *LA_usurf;
  PetscReal      delta_rho;
  PetscReal      k0_,n_;
  PetscBool      using_siunits,issetup;
  PetscReal      *normal,*tangent;
  BFacetList     facets;
  DM             dm;
  PetscReal      phi_range[2],k_range[2],vals[2],min_g[2],max_g[2];
  const PetscInt *facet_indices_region;
  
  PetscFunctionBegin;
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  tpctx = darcyctx->tpctx;
  
  ierr = MatCreateVecs(darcyctx->Ip_m2d,NULL,&usurf);CHKERRQ(ierr);
  
  ierr = DMCompositeGetAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  ierr = MatMult(darcyctx->Ip_m2d,Xu,usurf);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(tpctx->mechanics->dm_m_hat,tpctx->mechanics->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  
  ierr = QuadratureGetProperty(quadrature,"uN",&nfacets,&nqp,&ncomp_q,&q_coeff);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",NULL,NULL,NULL,&q_coor);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",NULL,NULL,NULL,&q_coeff_phi);CHKERRQ(ierr);
  ierr = VecGetArrayRead(usurf,&LA_usurf);CHKERRQ(ierr);
  
  dm = darcyctx->dm_p;
  ierr = DMTetGetSpaceBFacetList(dm,&facets);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(facets,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetOrientations(facets,&normal,&tangent);CHKERRQ(ierr);
  
  delta_rho = tpctx->rho_s - tpctx->rho_l;
  
  ierr = kPorosityDependentGetParams(tpctx->mechanics->coeff_container->pwp_k_on_mu,&k0_,&n_);CHKERRQ(ierr);
  using_siunits = PETSC_FALSE;
  ierr = PWPhysicsGetUseSI(tpctx->mechanics->coeff_container->pwp_k_on_mu,&using_siunits);CHKERRQ(ierr);
  
  phi_range[0] = 1.0e32;
  phi_range[1] = -1.0e32;
  for (q=0; q<nfacets*nqp; q++) {
    phi_range[0] = PetscMin(phi_range[0],q_coeff_phi[q]);
    phi_range[1] = PetscMax(phi_range[1],q_coeff_phi[q]);
  }
  for (q=0; q<2; q++) {
    k_range[q] = k0_ * PetscPowReal((phi_range[q]/0.01),n_); // assumed that we are using si units
    if (using_siunits) {
      k_range[q] /= tpctx->K;
    }
  }
  
  vals[0] = phi_range[0];
  vals[1] = k_range[0];
  ierr = MPI_Allreduce(vals,min_g,2,MPIU_REAL,MPIU_MIN,PETSC_COMM_WORLD);CHKERRQ(ierr);
  vals[0] = phi_range[1];
  vals[1] = k_range[1];
  ierr = MPI_Allreduce(vals,max_g,2,MPIU_REAL,MPIU_MAX,PETSC_COMM_WORLD);CHKERRQ(ierr);
  
  phi_range[0] = min_g[0];
  k_range[0]   = min_g[1];
  phi_range[1] = max_g[0];
  k_range[1]   = max_g[1];

  PetscPrintf(PETSC_COMM_WORLD,"[surf] phi_{min,max} %+1.4e : %+1.4e\n",phi_range[0],phi_range[1]);
  PetscPrintf(PETSC_COMM_WORLD,"[surf] k*_{min,max}  %+1.4e : %+1.4e\n",k_range[0],k_range[1]);
  
  /* face by face traversal */
  
  /* [0] default - everything is zero */
  for (e=0; e<nfacets; e++) {
    for (q=0; q<nqp; q++) {
      q_coeff[2*(e*nqp + q) + 0] = 0.0;
      q_coeff[2*(e*nqp + q) + 1] = 0.0;
    }
  }
  
  /* [1] plate-wedge */
  ierr = DMGListGetIndices(dm,"edge:PlateWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  
  for (k=0; k<nfacets_region; k++) {
    e = facet_indices_region[k];
    
    for (q=0; q<nqp; q++) {
      PetscReal phi_q,k_q,vD[2];
      
      phi_q = q_coeff_phi[e*nqp + q];
      
      k_q = k0_ * PetscPowReal((phi_q/0.01),n_); // assumed that we are using si units
      if (using_siunits) {
        k_q /= tpctx->K;
      }
      
      /* compute v_D = k* delta_rho g */
      vD[0] = -k_q * delta_rho * (- tpctx->g[0]);
      vD[1] = -k_q * delta_rho * (- tpctx->g[1]);
      
      q_coeff[2*(e*nqp + q) + 0] = vD[0];
      q_coeff[2*(e*nqp + q) + 1] = vD[1];
    }
  }
  
  /* [2] mantle-wedge */
  ierr = DMGListGetIndices(dm,"edge:MantleWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  for (k=0; k<nfacets_region; k++) {
    e = facet_indices_region[k];
    
    for (q=0; q<nqp; q++) {
      q_coeff[2*(e*nqp + q) + 0] = 0.0;
      q_coeff[2*(e*nqp + q) + 1] = 0.0;
    }
  }

  /* [3] mantle */
  ierr = DMGListGetIndices(dm,"edge:Mantle",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  for (k=0; k<nfacets_region; k++) {
    e = facet_indices_region[k];
    
    for (q=0; q<nqp; q++) {
      q_coeff[2*(e*nqp + q) + 0] = 0.0;
      q_coeff[2*(e*nqp + q) + 1] = 0.0;
    }
  }

  /* [4] slab-wedge */
  ierr = DMGListGetIndices(dm,"edge:SlabWedge",&nfacets_region,&facet_indices_region);CHKERRQ(ierr);
  for (k=0; k<nfacets_region; k++) {
    e = facet_indices_region[k];
    
    for (q=0; q<nqp; q++) {
      PetscReal vD[2],alpha,gaussian_weight;
      PetscReal *x_q;
      
      x_q = &q_coor[2*(e*nqp + q)];
      
      alpha = tpctx->Vdarcy / (-normal[2*e+0] - normal[2*e+1]);
      vD[0] = alpha;
      vD[1] = alpha;
      
      gaussian_weight = PetscExpReal(-(-x_q[1]-tpctx->gaussian_depth)*(-x_q[1]-tpctx->gaussian_depth)/(tpctx->gaussian_width * tpctx->gaussian_width));
      
      q_coeff[2*(e*nqp + q) + 0] = vD[0] * gaussian_weight;
      q_coeff[2*(e*nqp + q) + 1] = vD[1] * gaussian_weight;
    }
  }

  ierr = VecRestoreArrayRead(usurf,&LA_usurf);CHKERRQ(ierr);
  ierr = VecDestroy(&usurf);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_darcy_uv"
PetscErrorCode slab_boundary_darcy_uv(PetscScalar coor[],PetscInt dof,
                                      PetscScalar *val,PetscBool *constrain,void *data)
{
  DarcyCtx darcyctx;
  PetscReal vel[2],depth,slab_dip;
  
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  darcyctx = (DarcyCtx)data;
  
  *constrain = PETSC_FALSE;
  
  slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  
  vel[0] = darcyctx->tpctx->Vdarcy * PetscTanReal(slab_dip - 0.5 * PETSC_PI);
  vel[1] = darcyctx->tpctx->Vdarcy * PetscTanReal(slab_dip - 0.5 * PETSC_PI);

  
  depth = -coor[1];
  if (depth > (coor[0]*PetscTanReal(slab_dip) - 1.0e-6)) { /* slab-wedge */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
  }

  if (*constrain) {
    printf("%+1.4e %+1.4e s \n",coor[0],coor[1]);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "domain_boundary_darcy_un"
PetscErrorCode domain_boundary_darcy_un(PetscScalar coor[],PetscInt dof,
                                      PetscScalar *val,PetscBool *constrain,void *data)
{
  //DarcyCtx darcyctx;
  PetscReal depth,slab_dip;
  
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  //darcyctx = (DarcyCtx)data;
  
  *constrain = PETSC_FALSE;
  
  depth = -coor[1];
  slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  
  //right
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) {
    if (dof == 0) {
      //*constrain = PETSC_TRUE;
      //*val = 0.0;
    }
  }
  
  if (coor[1] < (DOMAIN_YMIN+1.0e-10)) {
   if (coor[0] > PetscAbsReal(DOMAIN_YMIN)/PetscTanReal(slab_dip)) { // bottom boundary
      if (dof == 1) {
        *constrain = PETSC_TRUE;
        *val = 0.0;
      }
    }
  }
  
  // top
  if (depth < (PLATE_THICKNESS+1.0e-10)) {
    if (dof == 1) {
      *constrain = PETSC_TRUE;
      *val = 0.0;
    }
  }
  
  if (*constrain) {
    printf("%+1.4e %+1.4e v \n",coor[0],coor[1]);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "domain_boundary_darcy_p"
PetscErrorCode domain_boundary_darcy_p(PetscScalar coor[],PetscInt dof,
                                        PetscScalar *val,PetscBool *constrain,void *data)
{
  //DarcyCtx darcyctx;
  
  if (!data) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected non-NULL pointer of type DarcyCtx as last argument");
  //darcyctx = (DarcyCtx)data;
  
  *constrain = PETSC_FALSE;
#if 0
  //right
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) {
    //if (-coor[1] - 1.0e-6 < PLATE_THICKNESS) {
      if (dof == 0) {
        *constrain = PETSC_TRUE;
        *val = 3.0e-6;
      }
    //}
  }
#endif
  //left
  if (coor[0]-1.0e-6 < 0.08333333333333) {
    if (-coor[1] - 1.0e-6 < PLATE_THICKNESS) {
      *constrain = PETSC_TRUE;
      *val = 0.0;
      printf("================= pin point p =================\n");
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtxSolve"
PetscErrorCode DarcyCtxSolve(DarcyCtx d,PetscBool *pass)
{
  PetscErrorCode ierr;
  Vec Xv,Xp,vs,vd = NULL;
  DM  dmS,dm_scalar;
  
  
  PetscFunctionBegin;
  *pass = PETSC_TRUE;
  
  // interpolate vs // this is done within the relveant quadrature evaluate
  // interpolate pf // this is done within the relveant quadrature evaluate

  //ierr = MatMult(d->IT_e2d,d->tpctx->mechanics->T_hat,d->T);CHKERRQ(ierr);
  ierr = DMTetCreateSharedSpace(d->tpctx->mechanics->dm_v_hat,1,&dm_scalar);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm_scalar,d->tpctx->mechanics->T_hat,PRJTYPE_NATIVE,d->dm_c_phi_T,&d->T);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);

  ierr = DarcyCtx_PoroDistF(d,d->dm_c_phi_T,d->cl,d->cs,d->T,d->phi);CHKERRQ(ierr);
  {
    Coefficient a,uN;
    
    ierr = PDEDarcyGetVCoefficients(d->pdedarcy,&a,NULL,NULL);CHKERRQ(ierr);
    ierr = DarcyCtx_PoroDistF_VQuadrature(d,d->dm_c_phi_T,d->cl,d->cs,d->T,a);CHKERRQ(ierr);

    ierr = PDEDarcyGetSCoefficients(d->pdedarcy,&uN,NULL);CHKERRQ(ierr);
    ierr = DarcyCtx_PoroDistF_SQuadrature(d,d->dm_c_phi_T,d->cl,d->cs,d->T,uN);CHKERRQ(ierr);
  }
  
  // evaluate Dirichlet bcs for Darcy velocity
  /*
  if (d->bc_v) {
    ierr = DMTetBCListTraverse(d->bc_v,0,domain_boundary_darcy_un,(void*)d);CHKERRQ(ierr);
    ierr = DMTetBCListTraverse(d->bc_v,1,domain_boundary_darcy_un,(void*)d);CHKERRQ(ierr);

    ierr = DMTetBCListTraverse(d->bc_v,0,slab_boundary_darcy_uv,(void*)d);CHKERRQ(ierr);
    ierr = DMTetBCListTraverse(d->bc_v,1,slab_boundary_darcy_uv,(void*)d);CHKERRQ(ierr);
  }
  */
  /*
   // L2-H1 form: We only prescribe u.n //
  if (d->bc_p) {
    ierr = DMTetBCListTraverse(d->bc_p,0,domain_boundary_darcy_p,(void*)d);CHKERRQ(ierr);
  }
   */
  
  // insert bcs for V darcy
  ierr = PDEDarcyGetDM(d->pdedarcy,&dmS);CHKERRQ(ierr);
  if (d->bc_v) {
    ierr = DMCompositeGetAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
    ierr = BCListInsert(d->bc_v,Xv);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
  }
  // snes solve
  ierr = PDESNESSolve(d->pdedarcy,d->snesdarcy);CHKERRQ(ierr);

  {
    KSPConvergedReason reason;
    KSP ksp;
    
    ierr = SNESGetKSP(d->snesdarcy,&ksp);CHKERRQ(ierr);
    ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
    if ((PetscInt)reason < 0) {
      PetscViewer viewer;
      Mat J,Jij;
      Vec Fu,Fp;
      IS *is;
      DM pack;
      Coefficient a,f,g;
      
      ierr = PDEDarcyGetDM(d->pdedarcy,&pack);CHKERRQ(ierr);
      ierr = DMCompositeGetGlobalISs(pack,&is);CHKERRQ(ierr);

      ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"PDEDarcy.J.mat",&viewer);CHKERRQ(ierr);
      ierr = KSPGetOperators(ksp,&J,NULL);CHKERRQ(ierr);
      ierr = MatGetSubMatrix(J,is[0],is[0],MAT_INITIAL_MATRIX,&Jij);CHKERRQ(ierr);
      ierr = MatView(Jij,viewer);CHKERRQ(ierr);
      ierr = MatDestroy(&Jij);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      
      ierr = DMCompositeGetAccess(pack,d->F,&Fu,&Fp);CHKERRQ(ierr);
      ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"PDEDarcy.F.mat",&viewer);CHKERRQ(ierr);
      ierr = VecView(Fu,viewer);CHKERRQ(ierr);
      ierr = VecView(Fp,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      ierr = DMCompositeRestoreAccess(pack,d->F,&Fu,&Fp);CHKERRQ(ierr);

      ierr = PDEDarcyGetVCoefficients(d->pdedarcy,&a,&f,&g);CHKERRQ(ierr);
      ierr = CoefficientView_GP(a,d->dm_p,"PDEDarcy-a");CHKERRQ(ierr);
      ierr = CoefficientView_GP(f,d->dm_p,"PDEDarcy-f");CHKERRQ(ierr);
      ierr = CoefficientView_GP(g,d->dm_p,"PDEDarcy-g");CHKERRQ(ierr);
      
      ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
      ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
      ierr = PetscFree(is);CHKERRQ(ierr);
    }
    
    if ((PetscInt)reason < 0) {
      PetscPrintf(PETSC_COMM_WORLD,"Darcy solve failed to converge: KSPConvergedReason %D. See http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPConvergedReason.html to understand value of KSPConvergedReason",(PetscInt)reason);
      *pass = PETSC_FALSE;
      
      PetscFunctionReturn(0);
    }
  }

  /* If the solver passed, compute new liquid and bulk velocities */
  {
    const PetscReal *LA_phi,*LA_vs,*LA_vd;
    PetscReal *LA_vl,*LA_vbulk,min,max;
    PetscInt i,N;
    DM dm_vector;
    
    //ierr = MatCreateVecs(d->Iv_m2d,NULL,&vs);CHKERRQ(ierr); // Darcy-Hdiv

    ierr = DMTetCreateSharedSpace(d->dm_c_phi_T,2,&dm_vector);CHKERRQ(ierr);
    ierr = DMCompositeGetAccess(d->tpctx->mechanics->dm_m_hat,d->tpctx->mechanics->X_hat,&Xv,&Xp);CHKERRQ(ierr);
    //ierr = MatMult(d->Iv_m2d,Xv,vs);CHKERRQ(ierr);  // Darcy-Hdiv
    vs = d->vs;
    ierr = DMTetProjectField(d->tpctx->mechanics->dm_v_hat,Xv,PRJTYPE_NATIVE,dm_vector,&vs);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(d->tpctx->mechanics->dm_m_hat,d->tpctx->mechanics->X_hat,&Xv,&Xp);CHKERRQ(ierr);
    
    ierr = DMCompositeGetAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
    vd = NULL;
    ierr = DMTetProjectField(d->dm_v,Xv,PRJTYPE_NATIVE,dm_vector,&vd);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);

    ierr = VecGetArrayRead(d->phi,&LA_phi);CHKERRQ(ierr);
    ierr = VecGetArrayRead(vs,&LA_vs);CHKERRQ(ierr);
    ierr = VecGetArrayRead(vd,&LA_vd);CHKERRQ(ierr);
    ierr = VecGetArray(d->vl,&LA_vl);CHKERRQ(ierr);
    ierr = VecGetArray(d->vbulk,&LA_vbulk);CHKERRQ(ierr);
    ierr = VecGetLocalSize(vd,&N);CHKERRQ(ierr);
    N = N / 2;
    for (i=0; i<N; i++) {
      if (LA_phi[i] > POROSITY_EPSILON) {
        LA_vl[2*i+0] = LA_vd[2*i+0]/LA_phi[i] + LA_vs[2*i+0];
        LA_vl[2*i+1] = LA_vd[2*i+1]/LA_phi[i] + LA_vs[2*i+1];

        LA_vbulk[2*i+0] = LA_vd[2*i+0] + LA_vs[2*i+0];
        LA_vbulk[2*i+1] = LA_vd[2*i+1] + LA_vs[2*i+1];
      } else {
        LA_vl[2*i+0] = LA_vs[2*i+0];
        LA_vl[2*i+1] = LA_vs[2*i+1];
        
        LA_vbulk[2*i+0] = LA_vs[2*i+0];
        LA_vbulk[2*i+1] = LA_vs[2*i+1];
      }
      //printf("%+1.6e  %+1.6e %+1.6e %+1.6e\n",LA_vl[i],LA_vd[i],LA_phi[i],LA_vs[i]);

    }
    ierr = VecRestoreArray(d->vbulk,&LA_vbulk);CHKERRQ(ierr);
    ierr = VecRestoreArray(d->vl,&LA_vl);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(vd,&LA_vd);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(vs,&LA_vs);CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(d->phi,&LA_phi);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_vector);CHKERRQ(ierr);
    
    ierr = VecMin(vd,NULL,&min);CHKERRQ(ierr);
    ierr = VecMax(vd,NULL,&max);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"  vD    [min/max] %+1.4e %+1.4e\n",min,max);

    ierr = VecMin(d->vl,NULL,&min);CHKERRQ(ierr);
    ierr = VecMax(d->vl,NULL,&max);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"  vl    [min/max] %+1.4e %+1.4e\n",min,max);

    ierr = VecMin(d->vbulk,NULL,&min);CHKERRQ(ierr);
    ierr = VecMax(d->vbulk,NULL,&max);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"  vbulk [min/max] %+1.4e %+1.4e\n",min,max);
  }

  /*
  ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step%D_",count);CHKERRQ(ierr);
  ierr = PDEDarcyFieldView_AsciiVTU(d->pdedarcy,prefix);CHKERRQ(ierr);
  */
  /*
  {
    const char *fields[] = { "vd", "T", "C_H2O_bulk", "C_RF_bulk", "phi", "vl", "vs" };
    Vec        vecs[10];
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step%D_ctfield_",0);CHKERRQ(ierr);
    ierr = DMCompositeGetAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
    vecs[0] = vd;
    vecs[1] = d->T;
    vecs[2] = d->cl;
    vecs[3] = d->cs;
    vecs[4] = d->phi;
    vecs[5] = d->vl;
    vecs[6] = vs;
    ierr = DMTetViewFields_VTU(d->dm_c_phi_T,7,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(d->dm_c_phi_T,7,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
  }
  */
  /*
  if (count%1 == 0) {
    const char *fields[] = { "vd", "C_H2O_bulk", "phi", "vl" };
    Vec        vecs[10];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step%D_ctfield_",count);CHKERRQ(ierr);
    ierr = DMCompositeGetAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
    vecs[0] = vd;
    vecs[1] = d->cl;
    vecs[2] = d->phi;
    vecs[3] = d->vl;
    ierr = DMTetViewFields_VTU(d->dm_c_phi_T,4,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
  }
   */
  ierr = VecDestroy(&vd);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtxView"
PetscErrorCode DarcyCtxView(DarcyCtx d,const char dir[],PetscInt step)
{
  const char *fields[] = {  "T", "C_H2O_bulk", "C_RF_bulk", "phi", "gamma", "pf", "vd", "vl", "vbar", "vs", 0 };
  const PetscInt nfields_scalar = 6;
  const PetscInt nfields_vector = 4;
  PetscInt nfields;
  Vec        vecs[10],Xv,Xp,vd,pf;
  DM         dmS,dm_vector;
  char       prefix[PETSC_MAX_PATH_LEN];
  char       h5filename[PETSC_MAX_PATH_LEN];
  char       stepname[PETSC_MAX_PATH_LEN];
  char       stepfieldname[PETSC_MAX_PATH_LEN];
  
  PetscErrorCode ierr;
  
  nfields = 0;
  while (fields[nfields] != NULL) {
    nfields++;
  }

  if (nfields != (nfields_scalar+nfields_vector)) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Any inconsistent number of scalar/vector fields was detected");

  ierr = PDEDarcyGetDM(d->pdedarcy,&dmS);CHKERRQ(ierr);
  ierr = DMTetCreateSharedSpace(d->dm_c_phi_T,2,&dm_vector);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
  
  pf = NULL;
  ierr = DMTetProjectField(d->dm_p,Xp,PRJTYPE_NATIVE,d->dm_c_phi_T,&pf);CHKERRQ(ierr);

  vd = NULL;
  ierr = DMTetProjectField(d->dm_v,Xv,PRJTYPE_NATIVE,dm_vector,&vd);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreAccess(dmS,d->X,&Xv,&Xp);CHKERRQ(ierr);
  
  /*
  // point wise evaluation of dphi/dt + div(vl.phi)
  {
    Vec phi_vl,div_phivl;

    ierr = DMCreateGlobalVector(d->dm_c_phi_T,&gamma_pw);CHKERRQ(ierr);
    ierr = VecDuplicate(d->vs,&phi_vl);CHKERRQ(ierr);
    ierr = VecPointwiseMultBS(phi_vl,d->vl,d->phi);CHKERRQ(ierr);
    div_phivl = NULL;
    ierr = DMTetProjectField(dm_vector,phi_vl,PRJTYPE_DIVERGENCE,d->dm_c_phi_T,&div_phivl);CHKERRQ(ierr);
    ierr = VecCopy(d->phi,gamma_pw);CHKERRQ(ierr);
    ierr = VecAXPY(gamma_pw,-1.0,d->phi0);CHKERRQ(ierr);
    ierr = VecScale(gamma_pw,1.0/d->tpctx->dt);CHKERRQ(ierr);
    ierr = VecAXPY(gamma_pw,1.0,div_phivl);CHKERRQ(ierr);
    
    ierr = VecDestroy(&phi_vl);CHKERRQ(ierr);
    ierr = VecDestroy(&div_phivl);CHKERRQ(ierr);
  }
  */
  ierr = DMDestroy(&dm_vector);CHKERRQ(ierr);
  
  vecs[0] = d->T;
  vecs[1] = d->cl;
  vecs[2] = d->cs;
  vecs[3] = d->phi;
  vecs[4] = d->gamma;
  vecs[5] = pf;
  vecs[6] = vd;
  vecs[7] = d->vl;
  vecs[8] = d->vbulk;
  vecs[9] = d->vs;
  
  ierr = DMTetCreateSharedSpace(d->dm_p,2,&dm_vector);CHKERRQ(ierr);
  if (d->output_hdf) {
    if (dir) {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"%s/darcy.h5",dir);CHKERRQ(ierr);
    } else {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"darcy.h5");CHKERRQ(ierr);
    }
    ierr = PetscSNPrintf(stepname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D",step);CHKERRQ(ierr);
    ierr = DMTetSpaceAddGroupNameHDF5(h5filename,stepname);CHKERRQ(ierr);
    
    PetscInt j;
    for (j=0; j<nfields_scalar; j++) {
      ierr = PetscSNPrintf(stepfieldname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D/%s",step,fields[j]);CHKERRQ(ierr);
      ierr = DMTetFieldSpaceView_HDF(d->dm_p,vecs[j],stepfieldname,h5filename);CHKERRQ(ierr);
    }
    
    for (j=nfields_scalar; j<nfields; j++) {
      ierr = PetscSNPrintf(stepfieldname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D/%s",step,fields[j]);CHKERRQ(ierr);
      ierr = DMTetFieldSpaceView_HDF(dm_vector,vecs[j],stepfieldname,h5filename);CHKERRQ(ierr);
    }

  } else {
    if (dir) {
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_darcy_",dir,step);CHKERRQ(ierr);
    } else {
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_darcy_",step);CHKERRQ(ierr);
    }

#ifdef SUBFUSC_USE_BINARY_PV
    ierr = DMTetViewFields_VTU_b(d->dm_c_phi_T,10,vecs,fields,prefix);CHKERRQ(ierr);
#elif
    ierr = DMTetViewFields_VTU(d->dm_c_phi_T,10,vecs,fields,prefix);CHKERRQ(ierr);
#endif
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_darcy_",step);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU2(d->dm_c_phi_T,10,vecs,fields,dir,prefix,NULL);CHKERRQ(ierr);
  }
  ierr = DMDestroy(&dm_vector);CHKERRQ(ierr);
  ierr = VecDestroy(&pf);CHKERRQ(ierr);
  ierr = VecDestroy(&vd);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtx_PoroDistF"
PetscErrorCode DarcyCtx_PoroDistF(DarcyCtx darcy,DM dm,Vec cl,Vec cs,Vec T,Vec phi)
{
  PetscErrorCode ierr;
  PetscInt i,nnodes;
  PetscReal cl_n,cs_n,T_n,P_n,phi_n,depth_n,*x_n;
  PetscReal *LA_coor;
  const PetscScalar *LA_T;
  const PetscScalar *LA_cl;
  const PetscScalar *LA_cs;
  PetscScalar *LA_phi;
  PetscReal gmin[3],gmax[3],z_ref,P_ref;
  TPCtx ctx;
  Vec T_local,cl_local,cs_local,phi_local;

  ctx = darcy->tpctx;
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  z_ref = PetscAbsReal(gmax[1]);
  P_ref = ctx->rho_plate * ctx->g[1] * z_ref; /* all quantities should be non-dimensional */

  ierr = DMTetSpaceElement(dm,NULL,NULL,NULL,0,&nnodes,&LA_coor);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&T_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&cl_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&cs_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&phi_local);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,T,INSERT_VALUES,T_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  T,INSERT_VALUES,T_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,cl,INSERT_VALUES,cl_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  cl,INSERT_VALUES,cl_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,cs,INSERT_VALUES,cs_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  cs,INSERT_VALUES,cs_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,phi,INSERT_VALUES,phi_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  phi,INSERT_VALUES,phi_local);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(T_local,&LA_T);CHKERRQ(ierr);
  ierr = VecGetArrayRead(cl_local,&LA_cl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(cs_local,&LA_cs);CHKERRQ(ierr);
  ierr = VecGetArray(phi_local,&LA_phi);CHKERRQ(ierr);
  
  for (i=0; i<nnodes; i++) {
    PetscBool isvalid;
    
    x_n     = &LA_coor[2*i];
    T_n     = LA_T[i];
    cs_n    = LA_cs[i];
    cl_n    = LA_cl[i];
    depth_n = PetscAbsReal(x_n[1]);
 
    if (cs_n < 0.0) cs_n = 0.0;
    if (cl_n < 0.0) cl_n = 0.0;
    
    /* convert temperature into degree C */
    T_n = T_n - 273.0;
    if (T_n < 0.0) T_n = 0.0;

    /*
    Compute P_qp = P_ref + rho.g.(z-z_ref),
    where z_ref is plate thickess, P_ref is pressure at plate-wedge interface
    */
    P_n = 0.0;
    if (depth_n <= z_ref) {
      P_n = ctx->rho_plate * ctx->g[1] * depth_n;
    } else {
      P_n = P_ref + ctx->rho_s * ctx->g[1] * (depth_n - z_ref);
    }
    /* convert P to GPa */
    P_n = P_n * ctx->P * 1.0e-9;
    
    /* call parameterized dist func */
    phi_n = 0.0;
    ierr = DistFComputePorosity(ctx->distfunc,T_n,P_n,cl_n,cs_n,&phi_n,&isvalid);CHKERRQ(ierr);

    //    if (phi_n < 4.160167646104e-04) {
    //  phi_n = 4.160167646104e-04;
    //}
    //if (phi_n < 1.0e-4) {
    //  phi_n = 1.0e-4;
    //}
    
    //if (x_n[0] > 0.8) { phi_n = 2.2e-3; }

    if (!isvalid) {
      printf("*** [DarcyCtx_PoroDistF] A valid porosity was not computed\n");
      printf("***   x[] %+1.6e %+1.6e\n",x_n[0],x_n[1]);
      printf("***   P,T %+1.4e %+1.4e\n",P_n,T_n);
      printf("***   Cl,Cs %+1.4e %+1.4e\n",cl_n,cs_n);
      printf("***   phi %+1.4e\n",phi_n);
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"A valid porosity was not computed");
    }
    
    /* set value */
    LA_phi[i] = phi_n;
  }
  ierr = VecRestoreArray(phi_local,&LA_phi);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(cs_local,&LA_cs);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(cl_local,&LA_cl);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T_local,&LA_T);CHKERRQ(ierr);

  ierr = VecZeroEntries(phi);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(darcy->dm_c_phi_T,phi_local,INSERT_VALUES,phi);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(darcy->dm_c_phi_T,  phi_local,INSERT_VALUES,phi);CHKERRQ(ierr);

  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&phi_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&cl_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&cs_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&T_local);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtx_PoroDistF_VQuadrature"
PetscErrorCode DarcyCtx_PoroDistF_VQuadrature(DarcyCtx darcy,DM dm,Vec cl,Vec cs,Vec T,Coefficient c)
{
  PetscErrorCode ierr;
  PetscInt i,nbasis,e,nen,*element,q,nqp;
  PetscReal cl_q,cs_q,T_q,P_q,phi_q,depth_q,x_q[2];
  PetscReal *LA_coor,*coeff_phi;
  const PetscScalar *LA_T;
  const PetscScalar *LA_cl;
  const PetscScalar *LA_cs;
  PetscReal gmin[3],gmax[3],z_ref,P_ref;
  TPCtx ctx;
  EContainer space;
  Quadrature quadrature;
  Vec T_local,cl_local,cs_local;
  
  ctx = darcy->tpctx;
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  z_ref = PetscAbsReal(gmax[1]);
  P_ref = ctx->rho_plate * ctx->g[1] * z_ref; /* all quantities should be non-dimensional */
  
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,NULL,NULL,&LA_coor);CHKERRQ(ierr);

  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&T_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&cl_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&cs_local);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,T,INSERT_VALUES,T_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  T,INSERT_VALUES,T_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,cl,INSERT_VALUES,cl_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  cl,INSERT_VALUES,cl_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,cs,INSERT_VALUES,cs_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  cs,INSERT_VALUES,cs_local);CHKERRQ(ierr);

  ierr = VecGetArrayRead(T_local,&LA_T);CHKERRQ(ierr);
  ierr = VecGetArrayRead(cl_local,&LA_cl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(cs_local,&LA_cs);CHKERRQ(ierr);

  ierr = CoefficientGetQuadrature(c,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",&nen,&nqp,NULL,&coeff_phi);CHKERRQ(ierr);

  ierr = EContainerCreate(dm,quadrature,&space);CHKERRQ(ierr);

  for (e=0; e<nen; e++) {
    PetscBool isvalid;
    PetscInt *cell_basis;
    
    cell_basis = &element[nbasis*e];
    
    for (q=0; q<nqp; q++) {
      
      x_q[0] = x_q[1] = 0.0;
      T_q = 0.0;
      cs_q = 0.0;
      cl_q = 0.0;
      for (i=0; i<nbasis; i++) {
        PetscInt basis_i = cell_basis[i];
        
        /* get global coordinates */
        x_q[0] += space->N[q][i] * LA_coor[2*basis_i+0];
        x_q[1] += space->N[q][i] * LA_coor[2*basis_i+1];
        
        /* interpolate to obtain T at quadrature point*/
        T_q += space->N[q][i] * LA_T[basis_i];

        /* interpolate cs, cl */
        cs_q += space->N[q][i] * LA_cs[basis_i];
        cl_q += space->N[q][i] * LA_cl[basis_i];
      }
      
      /* depth at quadrature point */
      depth_q = PetscAbsReal(x_q[1]);
      
      if (cs_q < 0.0) cs_q = 0.0;
      if (cl_q < 0.0) cl_q = 0.0;
    
      /* convert temperature into degree C */
      T_q = T_q - 273.0;
      if (T_q < 0.0) T_q = 0.0;
    
      /*
       Compute P_qp = P_ref + rho.g.(z-z_ref),
       where z_ref is plate thickess, P_ref is pressure at plate-wedge interface
       */
      P_q = 0.0;
      if (depth_q <= z_ref) {
        P_q = ctx->rho_plate * ctx->g[1] * depth_q;
      } else {
        P_q = P_ref + ctx->rho_s * ctx->g[1] * (depth_q - z_ref);
      }
      /* convert P to GPa */
      P_q = P_q * ctx->P * 1.0e-9;
    
      /* call parameterized dist func */
      phi_q = 0.0;
      ierr = DistFComputePorosity(ctx->distfunc,T_q,P_q,cl_q,cs_q,&phi_q,&isvalid);CHKERRQ(ierr);
      
      if (!isvalid) {
        printf("*** [DarcyCtx_PoroDistF_VQuadrature] A valid porosity was not computed\n");
        printf("***   x[] %+1.6e %+1.6e\n",x_q[0],x_q[1]);
        printf("***   P,T %+1.4e %+1.4e\n",P_q,T_q);
        printf("***   Cl,Cs %+1.4e %+1.4e\n",cl_q,cs_q);
        printf("***   phi %+1.4e\n",phi_q);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"A valid porosity was not computed");
      }
      
      /* set value */
      coeff_phi[e*nqp + q] = phi_q;
    }
  }
  ierr = VecRestoreArrayRead(cs_local,&LA_cs);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(cl_local,&LA_cl);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T_local,&LA_T);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&cl_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&cs_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&T_local);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&space);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyCtx_PoroDistF_SQuadrature"
PetscErrorCode DarcyCtx_PoroDistF_SQuadrature(DarcyCtx darcy,DM dm,Vec cl,Vec cs,Vec T,Coefficient c)
{
  PetscErrorCode ierr;
  PetscInt i,nbasis,f,nef,*element,q,nqp;
  PetscReal cl_q,cs_q,T_q,P_q,phi_q,depth_q,x_q[2];
  PetscReal *LA_coor,*coeff_phi;
  const PetscScalar *LA_T;
  const PetscScalar *LA_cl;
  const PetscScalar *LA_cs;
  PetscReal gmin[3],gmax[3],z_ref,P_ref;
  TPCtx ctx;
  EContainer space;
  Quadrature quadrature;
  PetscInt *facet_to_element;
  BFacetList     facets;
  PetscBool issetup;
  Vec T_local,cl_local,cs_local;
  
  ctx = darcy->tpctx;
  
  ierr = DMTetGetBoundingBox(dm,gmin,gmax);CHKERRQ(ierr);
  z_ref = PetscAbsReal(gmax[1]);
  P_ref = ctx->rho_plate * ctx->g[1] * z_ref; /* all quantities should be non-dimensional */
  
  ierr = DMTetSpaceElement(dm,NULL,&nbasis,&element,NULL,NULL,&LA_coor);CHKERRQ(ierr);
  
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&T_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&cl_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_c_phi_T,&cs_local);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,T,INSERT_VALUES,T_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  T,INSERT_VALUES,T_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,cl,INSERT_VALUES,cl_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  cl,INSERT_VALUES,cl_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(darcy->dm_c_phi_T,cs,INSERT_VALUES,cs_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_c_phi_T,  cs,INSERT_VALUES,cs_local);CHKERRQ(ierr);
  
  ierr = VecGetArrayRead(T_local,&LA_T);CHKERRQ(ierr);
  ierr = VecGetArrayRead(cl_local,&LA_cl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(cs_local,&LA_cs);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(c,&quadrature);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",&nef,&nqp,NULL,&coeff_phi);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dm,quadrature,&space);CHKERRQ(ierr);
  
  ierr = DMTetGetSpaceBFacetList(dm,&facets);CHKERRQ(ierr);
  ierr = BFacetListIsSetup(facets,&issetup);CHKERRQ(ierr);
  if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup. Call DMTetBFacetSetup() first");
  ierr = BFacetListGetFacetElementMap(facets,&facet_to_element);CHKERRQ(ierr);

  for (f=0; f<nef; f++) {
    PetscBool isvalid;
    PetscInt eidx,*cell_basis;
    
    eidx = facet_to_element[f];
    cell_basis = &element[nbasis*eidx];
    
    for (q=0; q<nqp; q++) {
      
      x_q[0] = x_q[1] = 0.0;
      T_q = 0.0;
      cs_q = 0.0;
      cl_q = 0.0;
      for (i=0; i<nbasis; i++) {
        PetscInt basis_i = cell_basis[i];
        
        /* get global coordinates */
        x_q[0] += space->N[q][i] * LA_coor[2*basis_i+0];
        x_q[1] += space->N[q][i] * LA_coor[2*basis_i+1];
        
        /* interpolate to obtain T at quadrature point*/
        T_q += space->N[q][i] * LA_T[basis_i];
        
        /* interpolate cs, cl */
        cs_q += space->N[q][i] * LA_cs[basis_i];
        cl_q += space->N[q][i] * LA_cl[basis_i];
      }
      
      /* depth at quadrature point */
      depth_q = PetscAbsReal(x_q[1]);
      
      if (cs_q < 0.0) cs_q = 0.0;
      if (cl_q < 0.0) cl_q = 0.0;
      
      /* convert temperature into degree C */
      T_q = T_q - 273.0;
      if (T_q < 0.0) T_q = 0.0;
      
      /*
       Compute P_qp = P_ref + rho.g.(z-z_ref),
       where z_ref is plate thickess, P_ref is pressure at plate-wedge interface
       */
      P_q = 0.0;
      if (depth_q <= z_ref) {
        P_q = ctx->rho_plate * ctx->g[1] * depth_q;
      } else {
        P_q = P_ref + ctx->rho_s * ctx->g[1] * (depth_q - z_ref);
      }
      /* convert P to GPa */
      P_q = P_q * ctx->P * 1.0e-9;
      
      /* call parameterized dist func */
      phi_q = 0.0;
      ierr = DistFComputePorosity(ctx->distfunc,T_q,P_q,cl_q,cs_q,&phi_q,&isvalid);CHKERRQ(ierr);
      
      if (!isvalid) {
        printf("*** [DarcyCtx_PoroDistF_SQuadrature] A valid porosity was not computed\n");
        printf("***   x[] %+1.6e %+1.6e\n",x_q[0],x_q[1]);
        printf("***   P,T %+1.4e %+1.4e\n",P_q,T_q);
        printf("***   Cl,Cs %+1.4e %+1.4e\n",cl_q,cs_q);
        printf("***   phi %+1.4e\n",phi_q);
        SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"A valid porosity was not computed");
      }
      
      /* set value */
      coeff_phi[f*nqp + q] = phi_q;
    }
  }
  ierr = VecRestoreArrayRead(cs_local,&LA_cs);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(cl_local,&LA_cl);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T_local,&LA_T);CHKERRQ(ierr);
  
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&cl_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&cs_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_c_phi_T,&T_local);CHKERRQ(ierr);
  ierr = EContainerDestroy(&space);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* --- */
#undef __FUNCT__
#define __FUNCT__ "Advection_BDF1_RHS"
PetscErrorCode Advection_BDF1_RHS(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *user_context)
{
  PetscErrorCode    ierr;
  DM                dm;
  PDE               pde;
  PetscInt          i,q,e,*element,nen,nbasis;
  PetscReal         *elX,*coeff;
  Vec               Xold = NULL,Xold_local;
  const PetscScalar *LA_Xold;
  EContainer        ec;
  
  
  PetscFunctionBegin;
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEHelmholtzGetDM(pde,&dm);CHKERRQ(ierr);
  ierr = PetscObjectQuery((PetscObject)pde,"X_k_scalar",(PetscObject*)&Xold);CHKERRQ(ierr);
  if (!Xold) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state");

  ierr = EContainerCreate(dm,quadrature,&ec);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_g0",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Xold_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,Xold,INSERT_VALUES,Xold_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm,  Xold,INSERT_VALUES,Xold_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(Xold_local,&LA_Xold);CHKERRQ(ierr);
  elX = ec->buf_basis_2vector_a;
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    idx = &element[nbasis*e];
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      elX[i] = LA_Xold[nidx];
    }
    for (q=0; q<ec->nqp; q++) {
      PetscReal Xold_q = 0.0;
      for (i=0; i<nbasis; i++) {
        Xold_q += ec->N[q][i] * elX[i];
      }
      coeff[e*ec->nqp + q] = Xold_q;
    }
  }
  ierr = VecRestoreArrayRead(Xold_local,&LA_Xold);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Xold_local);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


static PetscErrorCode SelectBoundary_SlabWedge(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  PetscReal depth,slab_dip;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};
  
  slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  depth = -coor[1];
  if (depth > (coor[0]*PetscTanReal(slab_dip) - 1.0e-6)) { /* slab-wedge */
    if ((facet->x1[0] - 1.0e-6 < 1.0) && (facet->x0[0] - 1.0e-6 < 1.0)) { /* hard-coded for 45 degrees */
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_PlateWedge(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  PetscReal depth;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};
  
  depth = -coor[1];
  // top
  if (depth < (PLATE_THICKNESS+1.0e-10)) {
    *tag = PETSC_TRUE;
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_Mantle(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  //PetscReal depth,slab_dip;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};

  //depth = -coor[1];
  //slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  
  //right
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) {
    *tag = PETSC_TRUE;
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode SelectBoundary_MantleWedge(DMTetFacet facet,void *ctx,PetscBool *tag)
{
  //PetscReal depth,slab_dip;
  PetscReal coor[] = {facet->x0[0],facet->x0[1]};

  //depth = -coor[1];
  //slab_dip = SLAB_DIP * PETSC_PI / 180.0;
  
  if (coor[1] < (DOMAIN_YMIN+1.0e-10)) {
    if ((facet->x1[0] >= 1.0-1.0e-6) && (facet->x0[0] >= 1.0-1.0e-6)) { /* hard-coded for 45 degrees */
      *tag = PETSC_TRUE;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMConfigureWedgeBoundaries_Default"
PetscErrorCode DMConfigureWedgeBoundaries_Default(DM dm)
{
  PetscErrorCode ierr;
  
  ierr = DMGListCreate(dm,0,"edge:SlabWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,1,"edge:PlateWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,2,"edge:Mantle");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,3,"edge:MantleWedge");CHKERRQ(ierr);
  
  ierr = DMTetBFacetListAssign_User(dm,"edge:SlabWedge",  SelectBoundary_SlabWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dm,"edge:PlateWedge", SelectBoundary_PlateWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dm,"edge:Mantle",     SelectBoundary_Mantle,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dm,"edge:MantleWedge",SelectBoundary_MantleWedge,NULL);CHKERRQ(ierr);

  PetscPrintf(PetscObjectComm((PetscObject)dm),"DMConfigureWedgeBoundaries_Default\n");
  ierr = DMGListView(dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMConfigureWedgeBoundaries"
PetscErrorCode DMConfigureWedgeBoundaries(DM dm)
{
  PetscErrorCode ierr;
  
  ierr = DMGListCreate(dm,0,"edge:SlabWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,1,"edge:PlateWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,2,"edge:Mantle");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,3,"edge:MantleWedge");CHKERRQ(ierr);
  ierr = DMGListCreate(dm,4,"edge:inflow_candidate");CHKERRQ(ierr);

  ierr = DMGListCreate(dm,5,"basis:SlabWedge");CHKERRQ(ierr); /* always v.n <= 0 */
  ierr = DMGListCreate(dm,6,"basis:MantleWedge");CHKERRQ(ierr); /* always set v.n = 0 */
  ierr = DMGListCreate(dm,8,"basis:PlateWedge");CHKERRQ(ierr);
  //ierr = DMGListCreate(dm,8,"basis_inflow:Mantle");CHKERRQ(ierr); /* require inflow/outflow detection */
  ierr = DMGListCreate(dm,7,"basis:inflow");CHKERRQ(ierr); /* require inflow/outflow detection */
  ierr = DMGListCreate(dm,9,"basis:Mantle");CHKERRQ(ierr);

  ierr = DMTetBFacetListAssign_User(dm,"edge:SlabWedge",  SelectBoundary_SlabWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dm,"edge:PlateWedge", SelectBoundary_PlateWedge,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dm,"edge:Mantle",     SelectBoundary_Mantle,NULL);CHKERRQ(ierr);
  ierr = DMTetBFacetListAssign_User(dm,"edge:MantleWedge",SelectBoundary_MantleWedge,NULL);CHKERRQ(ierr);
  {
    //const char *names[] = { "edge:PlateWedge", "edge:MantleWedge" };
    const char *names[] = { "edge:PlateWedge", "edge:MantleWedge", "edge:SlabWedge" };
    
    ierr = DMTetGListMerge(dm,3,names,"edge:inflow_candidate");CHKERRQ(ierr);
  }
  
  ierr = DMTetBFacetBasisListAssign(dm,"edge:PlateWedge", "basis:PlateWedge");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssign(dm,"edge:SlabWedge",  "basis:SlabWedge");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssign(dm,"edge:MantleWedge","basis:MantleWedge");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssign(dm,"edge:Mantle",      "basis:Mantle");CHKERRQ(ierr);

  //ierr = DMTetBFacetBasisListAssignFromInflowSubset(dm,"edge:inflow_candidate","basis:inflow",dm_v,velocity);CHKERRQ(ierr);

  PetscPrintf(PetscObjectComm((PetscObject)dm),"DMConfigureWedgeBoundaries\n");
  ierr = DMGListView(dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SDAdvectionPDECreate"
PetscErrorCode SDAdvectionPDECreate(const char name[],DM dm,BCList bclist,DM dm_v,Vec velocity,
                            PetscReal dt,PetscReal kappa,PDE *_pde)
{
  PetscErrorCode ierr;
  Coefficient    k,alpha,g;
  PDE  pde;
  Vec  X_prev;
  
  ierr = PDECreate(PETSC_COMM_WORLD,&pde);CHKERRQ(ierr);
  ierr = PDESetType(pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(pde,name);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(pde,dm,bclist);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_k(pde,&k);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(k,dt * kappa);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_alpha(pde,&alpha);CHKERRQ(ierr);
  ierr = CoefficientSetDomainConstant(alpha,1.0);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_g(pde,&g);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadrature(g,Advection_BDF1_RHS,NULL,NULL);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzSetVelocity(pde,dm_v,velocity);CHKERRQ(ierr);
  
  /* auxillary */
  ierr = DMCreateGlobalVector(dm,&X_prev);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject)pde,"X_k_scalar",(PetscObject)X_prev);CHKERRQ(ierr);
  ierr = VecDestroy(&X_prev);CHKERRQ(ierr);
  
  /* Create and configure the two-phase flow solver */
  ierr = PDESetOptionsPrefix(pde,name);CHKERRQ(ierr);
  *_pde = pde;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyComputeBulkVelocity_OmegaM"
PetscErrorCode DarcyComputeBulkVelocity_OmegaM(DarcyCtx darcy,MCtx mechanics,Mat I_v,PetscInt plate_region_id,PetscInt slab_region_id,PetscBool use_vs)
{
  VecScatter     v_scat;
  Vec            v_s_hat,p_hat;
  PetscErrorCode ierr;
  
  /* construct v_bar on omega (v basis) */
  
  // inject the boundary conditions for vs in regions 3 and 1
  ierr = VecZeroEntries(mechanics->v_bar);CHKERRQ(ierr);
  
  ierr = DMTetVecTraverseByRegionId(mechanics->dm_v,mechanics->v_bar,plate_region_id,-6,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(mechanics->dm_v,mechanics->v_bar,slab_region_id,-6,slab_uv,NULL);CHKERRQ(ierr);

  ierr = DMCompositeGetAccess(mechanics->dm_m_hat,mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);

  ierr = MatScatterGetVecScatter(I_v,&v_scat);CHKERRQ(ierr);
  if (use_vs) {
    ierr = VecScatterBegin(v_scat, v_s_hat, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd  (v_scat, v_s_hat, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  } else {
    Vec vbulk_hat = NULL;
    DM dm_vector;
    
    ierr = DMTetCreateSharedSpace(darcy->dm_c_phi_T,2,&dm_vector);CHKERRQ(ierr);
    ierr = DMTetProjectField(dm_vector,darcy->vbulk,PRJTYPE_NATIVE,mechanics->dm_v_hat,&vbulk_hat);CHKERRQ(ierr);
    ierr = DMDestroy(&dm_vector);CHKERRQ(ierr);

    ierr = VecScatterBegin(v_scat, vbulk_hat, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd  (v_scat, vbulk_hat, mechanics->v_bar, INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);

    ierr = VecDestroy(&vbulk_hat);CHKERRQ(ierr);
  }
  
  ierr = DMCompositeRestoreAccess(mechanics->dm_m_hat,mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

//<MPI?>
#undef __FUNCT__
#define __FUNCT__ "SDAdvection_BoundaryConditionSetup_phi"
PetscErrorCode SDAdvection_BoundaryConditionSetup_phi(DarcyCtx darcy,Vec phi,BCList bc_phi)
{
  PetscErrorCode ierr;
  PetscReal value_phi_dummy = 0.0;
  PetscInt k,L;
  PetscReal *vals;
  const PetscReal *LA_phi;
  
  ierr = DMGListReset(darcy->dm_c_phi_T,"basis:inflow");CHKERRQ(ierr);
  ierr = DMTetBFacetBasisListAssignFromInflowSubset(darcy->dm_c_phi_T,"edge:inflow_candidate","basis:inflow",darcy->dm_adv_v,darcy->vl);CHKERRQ(ierr);
  
  ierr = BCListReset(bc_phi);CHKERRQ(ierr);
  
  ierr = DMTetBCListTraverseDMTetGList(bc_phi,0,"basis:SlabWedge",impose_const,(void*)&value_phi_dummy);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseDMTetGList(bc_phi,0,"basis:MantleWedge",impose_const,(void*)&value_phi_dummy);CHKERRQ(ierr);

  ierr = DMTetBCListTraverseDMTetGList(bc_phi,0,"basis:Mantle",impose_const,(void*)&value_phi_dummy);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseDMTetGList(bc_phi,0,"basis:PlateWedge",impose_const,(void*)&value_phi_dummy);CHKERRQ(ierr);

  //ierr = DMTetBCListTraverseDMTetGList(bc_phi,0,"basis:inflow",impose_const,(void*)&value_phi_dummy);CHKERRQ(ierr);

  /* Insert values from the provided vector phi and insert them into the values stored with the BCList */
  /* Only the values inserted into list->values[] which correspond to Dirichlet DOFs are actually used */
  ierr = VecGetArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = BCListGetGlobalValues(bc_phi,&L,&vals);CHKERRQ(ierr);
  for (k=0; k<L; k++) {
    vals[k] = LA_phi[k];
  }
  ierr = VecRestoreArrayRead(phi,&LA_phi);CHKERRQ(ierr);
  ierr = BCListGlobalToLocal(bc_phi);CHKERRQ(ierr);

  
  PetscFunctionReturn(0);
}

/*
 DarcyReconstructGamma_SNESSolve() performs operator splitting to compute gamma.
 The operator splitting is given by
   /frac{\partial phi}{\partial dt} = - div(vl phi)
   /frac{\partial phi}{\partial dt} = \Gamma
 
 That is, we first solve
   (phi^* - phi^k)/dt + div(vl phi^*) = 0
 Followed by the ODE solve
   (phi^k+1 - phi^*)/dt = \Gamma
 using initial / boundary data from phi^*
*/
#undef __FUNCT__
#define __FUNCT__ "DarcyReconstructGamma_SNESSolve"
PetscErrorCode DarcyReconstructGamma_SNESSolve(DarcyCtx darcy,PetscReal kappa_phi,PetscReal dt)
{
  PetscErrorCode ierr;
  DM dm;
  PDE pde_phi;
  Vec phistar,gamma,phi_p,phi0_p;
  SNES snes;
  BCList bc_phi;
  
  dm = darcy->dm_c_phi_T;
  
  ierr = DMCreateGlobalVector(dm,&gamma);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&phistar);CHKERRQ(ierr);
  
  phi0_p = NULL;
  phi_p = NULL;
  ierr = DMTetProjectField(dm,darcy->phi0,PRJTYPE_NATIVE,dm,&phi0_p);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm,darcy->phi,PRJTYPE_NATIVE,dm,&phi_p);CHKERRQ(ierr);
  /*
  {
    // Previous behaviour - e.g. no projection
    phi0_p = darcy->phi0;  ierr = PetscObjectReference((PetscObject)darcy->phi0);CHKERRQ(ierr);
    phi_p  = darcy->phi;   ierr = PetscObjectReference((PetscObject)darcy->phi);CHKERRQ(ierr);
  }
  */
  
  
  ierr = DMBCListCreate(dm,&bc_phi);CHKERRQ(ierr);
  ierr = SDAdvectionPDECreate("adv_",dm,bc_phi,darcy->dm_adv_v,darcy->vl,dt,kappa_phi,&pde_phi);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde_phi,phistar);CHKERRQ(ierr);
  {
    Vec x;
    
    ierr = DMCreateLocalVector(dm,&x);CHKERRQ(ierr);
    ierr = PDESetLocalSolution(pde_phi,x);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_phi,snes,darcy->F_adv,darcy->J_adv,darcy->J_adv);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  {
    Vec X_prev_phi = NULL;
    
    ierr = PetscObjectQuery((PetscObject)pde_phi,"X_k_scalar",(PetscObject*)&X_prev_phi);CHKERRQ(ierr);
    if (!X_prev_phi) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state from pde_phi");
    
    ierr = VecCopy(phi0_p,X_prev_phi);CHKERRQ(ierr); /* x --> x_old */
  }

  ierr = PDEUpdateLocalSolution(pde_phi);CHKERRQ(ierr);
  /* Use the value of phi^k+1 to define the Dirichlet boundary values */
  /* We use phi^{k+1} as this is consistent with a Backward Euler integrator (which is used here for phi) */
  ierr = SDAdvection_BoundaryConditionSetup_phi(darcy,phi_p,bc_phi);CHKERRQ(ierr);
  ierr = BCListInsert(bc_phi,phistar);CHKERRQ(ierr);
  
  /* [split 1] Solve \partial(phi)/\partial t + grad(phi vl) = 0 for phi^* */
  ierr = VecScale(darcy->vl,dt);CHKERRQ(ierr);

  ierr = PDESNESSolve(pde_phi,snes);CHKERRQ(ierr);
  {
    KSPConvergedReason reason;
    KSP ksp;
    
    ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
    ierr = KSPGetConvergedReason(ksp,&reason);CHKERRQ(ierr);
    if ((PetscInt)reason < 0) {
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"SDAdv.Liquid.porosity solve failed to converge: KSPConvergedReason %D",(PetscInt)reason);
    }
  }

  ierr = VecScale(darcy->vl,1.0/dt);CHKERRQ(ierr);
  
  /* [split 2] Solve the ODE \partial(phi)/\partial t = Gamma (for gamma) */
  ierr = VecCopy(phi_p,gamma);CHKERRQ(ierr);
  ierr = VecAXPY(gamma,-1.0,phistar);CHKERRQ(ierr);
  ierr = VecScale(gamma,1.0/dt);CHKERRQ(ierr);
  
  /*
  {
    PetscReal nrm;
    VecNorm(gamma,NORM_INFINITY,&nrm);
    printf("=== norm(gamma) %+1.12e\n",nrm);
  }
  */
  
  /* save result */
  ierr = VecCopy(gamma,darcy->gamma);CHKERRQ(ierr);
  //
  {
    const char *fields[] = { "gamma","phi1","phi*","phi0" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    PetscBool  dump = PETSC_FALSE;
    
    ierr = PetscOptionsGetBool(NULL,NULL,"-darcy.gamma.debug",&dump,NULL);CHKERRQ(ierr);
    if (dump) {
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_gamma_",darcy->tpctx->output_path,darcy->tpctx->step);CHKERRQ(ierr);
      vecs[0] = gamma;
      vecs[1] = phi_p;
      vecs[2] = phistar;
      vecs[3] = phi0_p;
      ierr = DMTetViewFields_VTU(darcy->dm_c_phi_T,4,vecs,fields,prefix);CHKERRQ(ierr);
      ierr = DMTetViewFields_PVTU(darcy->dm_c_phi_T,4,vecs,fields,prefix);CHKERRQ(ierr);
    }
  }
  //
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_phi);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde_phi);CHKERRQ(ierr);
  ierr = VecDestroy(&phistar);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_p);CHKERRQ(ierr);
  ierr = VecDestroy(&phi0_p);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 DarcyReconstructGamma_SNESFormF() computes the gamma required to move porosity from phi^k -> phi^k+1.
 This is achieved by considering the discrete system
   M phi^{k+1} - M phi^{k} + dt A(vl) phi^{k+1} = dt kappa K phi^{k+1} + dt M \hat{gamma}
 The residual of this discrete problem is
   F = M phi^{k+1} - M phi^{k} + dt A(vl) phi^{k+1} - dt kappa K phi^{k+1} - dt M \hat{gamma}
 
 We use a PDEHelmholtz object which defines
   F_s = M phi^{k+1} - M phi^{k} + dt A(vl) phi^{k+1} - dt kappa K phi^{k+1}
 Hence knowing phi^{k+1} and phi^{k} we have that
   F = F_s - dt M \hat{gamma}
 or assuming that we have a solution to the nonlinear problem, e.g. F = 0
   dt M \hat{gamma} = F_s
 Thus we obtain the nodal melting rate (\hat{gamma}) by solving the projection problem above
 where the RHS is simply the nonlinear residual of the source-less advection-diffusion problem.
*/
#undef __FUNCT__
#define __FUNCT__ "DarcyReconstructGamma_SNESFormF"
PetscErrorCode DarcyReconstructGamma_SNESFormF(DarcyCtx darcy,PetscReal kappa_phi,PetscReal dt)
{
  PetscErrorCode ierr;
  DM dm;
  PDE pde_phi;
  Vec phistar,gamma,phi_p,phi0_p,F;
  SNES snes;
  BCList bc_phi;
  Mat M;
  KSP ksp;
  PC pc;
  
  dm = darcy->dm_c_phi_T;
  
  ierr = DMCreateGlobalVector(dm,&gamma);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&phistar);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm,&F);CHKERRQ(ierr);
  
  phi0_p = NULL;
  phi_p = NULL;
  ierr = DMTetProjectField(dm,darcy->phi0,PRJTYPE_NATIVE,dm,&phi0_p);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm,darcy->phi,PRJTYPE_NATIVE,dm,&phi_p);CHKERRQ(ierr);
  /*
   {
   // Previous behaviour - e.g. no projection
   phi0_p = darcy->phi0;  ierr = PetscObjectReference((PetscObject)darcy->phi0);CHKERRQ(ierr);
   phi_p  = darcy->phi;   ierr = PetscObjectReference((PetscObject)darcy->phi);CHKERRQ(ierr);
   }
   */
  
  
  ierr = DMBCListCreate(dm,&bc_phi);CHKERRQ(ierr);
  ierr = SDAdvectionPDECreate("adv_",dm,bc_phi,darcy->dm_adv_v,darcy->vl,dt,kappa_phi,&pde_phi);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde_phi,phistar);CHKERRQ(ierr);
  {
    Vec x;
    
    ierr = DMCreateLocalVector(dm,&x);CHKERRQ(ierr);
    ierr = PDESetLocalSolution(pde_phi,x);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_phi,snes,darcy->F_adv,darcy->J_adv,darcy->J_adv);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  {
    Vec X_prev_phi = NULL;
    
    ierr = PetscObjectQuery((PetscObject)pde_phi,"X_k_scalar",(PetscObject*)&X_prev_phi);CHKERRQ(ierr);
    if (!X_prev_phi) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state from pde_phi");
    
    ierr = VecCopy(phi0_p,X_prev_phi);CHKERRQ(ierr); /* x --> x_old */
  }

  ierr = VecCopy(phi_p,phistar);CHKERRQ(ierr); /* x --> x_old */

  
  ierr = PDEUpdateLocalSolution(pde_phi);CHKERRQ(ierr);
  /* Use the value of phi^k+1 to define the Dirichlet boundary values */
  /* We use phi^{k+1} as this is consistent with a Backward Euler integrator (which is used here for phi) */
  ierr = SDAdvection_BoundaryConditionSetup_phi(darcy,phi_p,bc_phi);CHKERRQ(ierr);
  ierr = BCListInsert(bc_phi,phistar);CHKERRQ(ierr);
  
  /* [split 1] Solve \partial(phi)/\partial t + grad(phi vl) = 0 for phi^* */
  ierr = VecScale(darcy->vl,dt);CHKERRQ(ierr);

  ierr = SNESComputeFunction(snes,phistar,F);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vl,1.0/dt);CHKERRQ(ierr);
  
  ierr = AssembleBForm_VectorMassMatrix(dm,1,MAT_INITIAL_MATRIX,&M);CHKERRQ(ierr);
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"proj_f_");CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,M,M);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPPREONLY);CHKERRQ(ierr);
  ierr = PCSetType(pc,PCLU);CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  
  ierr = KSPSolve(ksp,F,gamma);CHKERRQ(ierr);
  
  ierr = VecScale(gamma,1.0/dt);CHKERRQ(ierr);
  
  /* save result */
  ierr = VecCopy(gamma,darcy->gamma);CHKERRQ(ierr);
  //
  {
    const char *fields[] = { "gamma","phi1","phi*","phi0" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    PetscBool  dump = PETSC_FALSE;
    
    ierr = PetscOptionsGetBool(NULL,NULL,"-darcy.gamma.debug",&dump,NULL);CHKERRQ(ierr);
    if (dump) {
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_gamma_",darcy->tpctx->output_path,darcy->tpctx->step);CHKERRQ(ierr);
      vecs[0] = gamma;
      vecs[1] = phi_p;
      vecs[2] = phistar;
      vecs[3] = phi0_p;
      ierr = DMTetViewFields_VTU(darcy->dm_c_phi_T,4,vecs,fields,prefix);CHKERRQ(ierr);
    }
  }
  //
  ierr = MatDestroy(&M);CHKERRQ(ierr);
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_phi);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde_phi);CHKERRQ(ierr);
  ierr = VecDestroy(&phistar);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_p);CHKERRQ(ierr);
  ierr = VecDestroy(&phi0_p);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyReconstructGamma_SNESFormF_PetrovGalerkin"
PetscErrorCode DarcyReconstructGamma_SNESFormF_PetrovGalerkin(DarcyCtx darcy,PetscReal kappa_phi,PetscReal dt)
{
  PetscErrorCode ierr;
  DM dm,dm_dg;
  PDE pde_phi;
  Vec phistar,gamma,phi_p,phi0_p,F;
  SNES snes;
  BCList bc_phi;
  KSP ksp;
  
  dm = darcy->dm_c_phi_T;
  dm_dg = darcy->dm_dg;
  ksp = darcy->proj_dg;
  
  ierr = DMCreateGlobalVector(dm,&phistar);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_dg,&gamma);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_dg,&F);CHKERRQ(ierr);

  /* project the input phi and phi0 */
  phi0_p = NULL;
  phi_p = NULL;
  ierr = DMTetProjectField(dm,darcy->phi0,PRJTYPE_NATIVE,dm,&phi0_p);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm,darcy->phi,PRJTYPE_NATIVE,dm,&phi_p);CHKERRQ(ierr);
  
  ierr = DMBCListCreate(dm_dg,&bc_phi);CHKERRQ(ierr);
  ierr = SDAdvectionPDECreate("adv_",dm,bc_phi,darcy->dm_adv_v,darcy->vl,dt,kappa_phi,&pde_phi);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde_phi,phistar);CHKERRQ(ierr);
  {
    Vec x;
    
    ierr = DMCreateLocalVector(dm,&x);CHKERRQ(ierr);
    ierr = PDESetLocalSolution(pde_phi,x);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_phi,snes,darcy->F_adv,darcy->J_adv,darcy->J_adv);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  {
    Vec X_prev_phi = NULL;
    
    ierr = PetscObjectQuery((PetscObject)pde_phi,"X_k_scalar",(PetscObject*)&X_prev_phi);CHKERRQ(ierr);
    if (!X_prev_phi) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state from pde_phi");
    
    ierr = VecCopy(phi0_p,X_prev_phi);CHKERRQ(ierr); /* x --> x_old */
  }
  
  ierr = VecCopy(phi_p,phistar);CHKERRQ(ierr); /* x --> x_old */
  
  
  ierr = PDEUpdateLocalSolution(pde_phi);CHKERRQ(ierr);

  /* Do not impose any Diriclet boundary conditions on phi */
  //ierr = SDAdvection_BoundaryConditionSetup_phi(darcy,phi_p,bc_phi);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vl,dt);CHKERRQ(ierr);
  
  //ierr = SNESComputeFunction(snes,phistar,F);CHKERRQ(ierr);
  ierr = FormFunction_PDEHelmholtz_PetrovGalerkin(snes,phistar,F,dm_dg);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vl,1.0/dt);CHKERRQ(ierr);
  
  ierr = KSPSolve(ksp,F,gamma);CHKERRQ(ierr);
  
  ierr = VecScale(gamma,1.0/dt);CHKERRQ(ierr);
  
  /* project and save result */
  ierr = DMTetProjectField(dm_dg,gamma,PRJTYPE_NATIVE,dm,&darcy->gamma);CHKERRQ(ierr);

  {
    const char *fields[] = { "gamma","phi1","phi*","phi0" };
    const char *fields_dg[] = { "gamma", "F" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    PetscBool  dump = PETSC_FALSE;
    
    ierr = PetscOptionsGetBool(NULL,NULL,"-darcy.gamma.debug",&dump,NULL);CHKERRQ(ierr);
    if (dump) {
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_gamma_dg_",darcy->tpctx->output_path,darcy->tpctx->step);CHKERRQ(ierr);
      vecs[0] = gamma;
      vecs[1] = F;
      ierr = DMTetViewFields_VTU(dm_dg,2,vecs,fields_dg,prefix);CHKERRQ(ierr);
      
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_gamma_",darcy->tpctx->output_path,darcy->tpctx->step);CHKERRQ(ierr);
      vecs[0] = darcy->gamma;
      vecs[1] = phi_p;
      vecs[2] = phistar;
      vecs[3] = phi0_p;
      ierr = DMTetViewFields_VTU(darcy->dm_c_phi_T,4,vecs,fields,prefix);CHKERRQ(ierr);
    }
  }
  //
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_phi);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde_phi);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&phistar);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_p);CHKERRQ(ierr);
  ierr = VecDestroy(&phi0_p);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyReconstructGamma_SNESFormF_PetrovGalerkin_vs"
PetscErrorCode DarcyReconstructGamma_SNESFormF_PetrovGalerkin_vs(DarcyCtx darcy,PetscReal kappa_phi,PetscReal dt)
{
  PetscErrorCode ierr;
  DM dm,dm_dg;
  PDE pde_phi;
  Vec phistar,gamma,phi_p,phi0_p,F;
  SNES snes;
  BCList bc_phi;
  KSP ksp;
  
  dm = darcy->dm_c_phi_T;
  dm_dg = darcy->dm_dg;
  ksp = darcy->proj_dg;
  
  ierr = DMCreateGlobalVector(dm,&phistar);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_dg,&gamma);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_dg,&F);CHKERRQ(ierr);
  
  /* project the input phi and phi0 */
  phi0_p = NULL;
  phi_p = NULL;
  ierr = DMTetProjectField(dm,darcy->phi0,PRJTYPE_NATIVE,dm,&phi0_p);CHKERRQ(ierr);
  ierr = DMTetProjectField(dm,darcy->phi,PRJTYPE_NATIVE,dm,&phi_p);CHKERRQ(ierr);
  
  ierr = VecScale(phi0_p,-1.0);CHKERRQ(ierr);
  ierr = VecShift(phi0_p,1.0);CHKERRQ(ierr);

  ierr = VecScale(phi_p,-1.0);CHKERRQ(ierr);
  ierr = VecShift(phi_p,1.0);CHKERRQ(ierr);
  
  ierr = DMBCListCreate(dm_dg,&bc_phi);CHKERRQ(ierr);
  ierr = SDAdvectionPDECreate("adv_",dm,bc_phi,darcy->dm_adv_v,darcy->vs,dt,kappa_phi,&pde_phi);CHKERRQ(ierr);
  
  ierr = PDESetSolution(pde_phi,phistar);CHKERRQ(ierr);
  {
    Vec x;
    
    ierr = DMCreateLocalVector(dm,&x);CHKERRQ(ierr);
    ierr = PDESetLocalSolution(pde_phi,x);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  
  ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(pde_phi,snes,darcy->F_adv,darcy->J_adv,darcy->J_adv);CHKERRQ(ierr);
  ierr = SNESSetType(snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  
  {
    Vec X_prev_phi = NULL;
    
    ierr = PetscObjectQuery((PetscObject)pde_phi,"X_k_scalar",(PetscObject*)&X_prev_phi);CHKERRQ(ierr);
    if (!X_prev_phi) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Failed to retreive the old solution state from pde_phi");
    
    ierr = VecCopy(phi0_p,X_prev_phi);CHKERRQ(ierr); /* x --> x_old */
  }
  
  ierr = VecCopy(phi_p,phistar);CHKERRQ(ierr); /* x --> x_old */
  
  
  ierr = PDEUpdateLocalSolution(pde_phi);CHKERRQ(ierr);
  
  /* Do not impose any Diriclet boundary conditions on phi */
  //ierr = SDAdvection_BoundaryConditionSetup_phi(darcy,phi_p,bc_phi);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vs,dt);CHKERRQ(ierr);
  
  ierr = FormFunction_PDEHelmholtz_PetrovGalerkin(snes,phistar,F,dm_dg);CHKERRQ(ierr);
  
  ierr = VecScale(darcy->vs,1.0/dt);CHKERRQ(ierr);
  
  ierr = KSPSolve(ksp,F,gamma);CHKERRQ(ierr);
  
  ierr = VecScale(gamma,-1.0/dt);CHKERRQ(ierr);
  
  /* project and save result */
  ierr = DMTetProjectField(dm_dg,gamma,PRJTYPE_NATIVE,dm,&darcy->gamma);CHKERRQ(ierr);
  
  {
    const char *fields[] = { "gamma","phi1","phi*","phi0" };
    const char *fields_dg[] = { "gamma", "F" };
    char       prefix[PETSC_MAX_PATH_LEN];
    Vec        vecs[10];
    PetscBool  dump = PETSC_FALSE;
    
    ierr = PetscOptionsGetBool(NULL,NULL,"-darcy.gamma.debug",&dump,NULL);CHKERRQ(ierr);
    if (dump) {
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_gamma_dg_",darcy->tpctx->output_path,darcy->tpctx->step);CHKERRQ(ierr);
      vecs[0] = gamma;
      vecs[1] = F;
      ierr = DMTetViewFields_VTU(dm_dg,2,vecs,fields_dg,prefix);CHKERRQ(ierr);
      
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_gamma_",darcy->tpctx->output_path,darcy->tpctx->step);CHKERRQ(ierr);
      vecs[0] = darcy->gamma;
      vecs[1] = phi_p;
      vecs[2] = phistar;
      vecs[3] = phi0_p;
      ierr = DMTetViewFields_VTU(darcy->dm_c_phi_T,4,vecs,fields,prefix);CHKERRQ(ierr);
    }
  }

  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = BCListDestroy(&bc_phi);CHKERRQ(ierr);
  ierr = PDEDestroy(&pde_phi);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
  ierr = VecDestroy(&phistar);CHKERRQ(ierr);
  ierr = VecDestroy(&phi_p);CHKERRQ(ierr);
  ierr = VecDestroy(&phi0_p);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyReconstructGamma"
PetscErrorCode DarcyReconstructGamma(DarcyCtx darcy,PetscReal kappa_phi,PetscReal dt)
{
  PetscErrorCode ierr;
  //ierr = DarcyReconstructGamma_SNESSolve(darcy,kappa_phi,dt);CHKERRQ(ierr);
  //ierr = DarcyReconstructGamma_SNESFormF(darcy,kappa_phi,dt);CHKERRQ(ierr);
  
  /* Note here we explicitly enforce that kappa_phi = 0.0 */
  //ierr = DarcyReconstructGamma_SNESFormF_PetrovGalerkin(darcy,0.0,dt);CHKERRQ(ierr);
  ierr = DarcyReconstructGamma_SNESFormF_PetrovGalerkin_vs(darcy,0.0,dt);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureEvaluator_MeltingRate_Darcy_SEQ"
PetscErrorCode QuadratureEvaluator_MeltingRate_Darcy_SEQ(Quadrature quadrature,DM dmT,TPCtx ctx)
{
  PetscErrorCode ierr;
  PetscInt e,e_sub,k,q,nbasis,nqp,nen,nen_sub,nbasis_sub;
  EContainer space_sub;
  PetscReal **_basis;
  PetscInt *element,*element_sub;
  PetscReal *coeff;
  PetscInt *p2s;
  Vec gamma;
  const PetscReal *LA_gamma;
  PetscInt nregions = 1;
  PetscInt regionlist[] = {2};
  Vec gamma_local;

  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"gamma",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmT,&nen,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(ctx->darcy->dm_c_phi_T,&nen_sub,&nbasis_sub,&element_sub,NULL,NULL,NULL);CHKERRQ(ierr);
  
  ierr = EContainerCreate(ctx->darcy->dm_c_phi_T,quadrature,&space_sub);CHKERRQ(ierr);
  nbasis_sub = space_sub->nbasis;
  _basis     = space_sub->N;
  
  ierr = DMTetSubmeshByRegions_CreateElementMaps(dmT,nregions,regionlist,NULL,&p2s,NULL,NULL);CHKERRQ(ierr);
  
  gamma = ctx->darcy->gamma;
  ierr = DMGetLocalVector(ctx->darcy->dm_c_phi_T,&gamma_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(ctx->darcy->dm_c_phi_T,gamma,INSERT_VALUES,gamma_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(ctx->darcy->dm_c_phi_T,  gamma,INSERT_VALUES,gamma_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(gamma_local,&LA_gamma);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    PetscInt *subcell_idx;
    
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
    }
    
    e_sub = p2s[e];
    if (e_sub == -1) continue;
   
    subcell_idx = &element_sub[nbasis_sub*e_sub];
    
    for (q=0; q<nqp; q++) {
      PetscReal gamma_qp;
      
      gamma_qp = 0.0;
      for (k=0; k<nbasis_sub; k++) {
        PetscInt subcell_basis_idx = subcell_idx[k];
        
        gamma_qp += _basis[q][k] * LA_gamma[subcell_basis_idx];
      }
      
      coeff[e*nqp + q] = gamma_qp;
    }
  }
  
  ierr = VecRestoreArrayRead(gamma_local,&LA_gamma);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(ctx->darcy->dm_c_phi_T,&gamma_local);CHKERRQ(ierr);
  ierr = PetscFree(p2s);CHKERRQ(ierr);
  ierr = EContainerDestroy(&space_sub);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureEvaluator_MeltingRate_Darcy"
PetscErrorCode QuadratureEvaluator_MeltingRate_Darcy(Quadrature quadrature,DM dmT,TPCtx ctx)
{
  PetscErrorCode ierr;
  PetscInt e,k,q,nbasis,nqp,nen,nen_g;
  EContainer space;
  PetscReal **_basis;
  PetscInt *element,*eregion;
  PetscReal *coeff;
  const PetscReal *LA_gamma;
  PetscInt regionlist[] = {2};
  Vec gamma,gamma_local;
  Vec gamma_m,gamma_hat_m = NULL;
  Vec gamma_e;
  Mat interp_tet2tet,I_e;
  VecScatter scatter;
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"gamma",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmT,&nen,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryGetAttributes(dmT,&nen_g,&eregion,NULL);CHKERRQ(ierr);
  if (nen != nen_g) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Number of elements does not match number of geometry attribute tags");
  
  ierr = EContainerCreate(dmT,quadrature,&space);CHKERRQ(ierr);
  nbasis = space->nbasis;
  _basis = space->N;
  
  gamma = ctx->darcy->gamma;
  
  /* view raw gamma */
  /*
  {
    const char *fields[] = { "gamma" };
    Vec vecs[] = {gamma};
    char prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"gamma_darcy_");CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->darcy->dm_c_phi_T,1,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(ctx->darcy->dm_c_phi_T,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  */
  
  ierr = DMTetProjectField(ctx->darcy->dm_c_phi_T,gamma,PRJTYPE_NATIVE,ctx->mechanics->dm_ve_hat,&gamma_hat_m);CHKERRQ(ierr);
  /*
  {
    const char *fields[] = { "gamma" };
    Vec vecs[] = {gamma_hat_m};
    char prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"gamma_hat_m_");CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->mechanics->dm_ve_hat,1,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(ctx->mechanics->dm_ve_hat,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  */
   
  // move gamma(wedge) to gamma(omega)
  ierr = DMCreateGlobalVector(ctx->mechanics->dm_ve,&gamma_m);CHKERRQ(ierr);
  ierr = DMCreateInjection(ctx->mechanics->dm_ve,ctx->mechanics->dm_ve_hat,&I_e);CHKERRQ(ierr);
  ierr = MatScatterGetVecScatter(I_e,&scatter);CHKERRQ(ierr);
  ierr = VecScatterBegin(scatter,gamma_hat_m,gamma_m,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter,gamma_hat_m,gamma_m,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  /*
  {
    const char *fields[] = { "gamma" };
    Vec vecs[] = {gamma_m};
    char prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"gamma_m_");CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->mechanics->dm_ve,1,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(ctx->mechanics->dm_ve,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  */

  // move gamma from m to e
  ierr = DMCreateGlobalVector(dmT,&gamma_e);CHKERRQ(ierr);
  ierr = DMCreateInterpolation(ctx->mechanics->dm_ve,dmT,&interp_tet2tet,NULL);CHKERRQ(ierr);
  ierr = MatMult(interp_tet2tet,gamma_m,gamma_e);CHKERRQ(ierr);
  /*
  {
    const char *fields[] = { "gamma" };
    Vec vecs[] = {gamma_e};
    char prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"gamma_e_");CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dmT,1,vecs,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dmT,1,vecs,fields,prefix);CHKERRQ(ierr);
  }
  */
   
  ierr = DMGetLocalVector(dmT,&gamma_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmT,gamma_e,INSERT_VALUES,gamma_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmT,  gamma_e,INSERT_VALUES,gamma_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(gamma_local,&LA_gamma);CHKERRQ(ierr);
  
  for (e=0; e<nen; e++) {
    /* initialize to zero */
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
    }

    if (eregion[e] != regionlist[0]) continue;
    
    for (q=0; q<nqp; q++) {
      PetscReal gamma_qp = 0.0;
      
      for (k=0; k<nbasis; k++) {
        PetscInt basis_idx = element[nbasis*e + k];
        gamma_qp += _basis[q][k] * LA_gamma[basis_idx];
      }
      coeff[e*nqp + q] = gamma_qp;
    }
  }
  
  ierr = VecRestoreArrayRead(gamma_local,&LA_gamma);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmT,&gamma_local);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma_e);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma_m);CHKERRQ(ierr);
  ierr = VecDestroy(&gamma_hat_m);CHKERRQ(ierr);
  ierr = EContainerDestroy(&space);CHKERRQ(ierr);
  ierr = MatDestroy(&interp_tet2tet);CHKERRQ(ierr);
  ierr = MatDestroy(&I_e);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "SDAdvection_ComputeCellDiffusion"
PetscErrorCode SDAdvection_ComputeCellDiffusion(PDE pde,Vec vel,PetscReal target_cell_Pe,PetscReal *_kappa)
{
  PetscErrorCode ierr;
  PetscReal kappa_g,cell_kappa_max = 0.0;
  const PetscReal *LA_vel;
  DM dm,dmv = NULL;
  PetscInt e,k,nen,nbasis,*element;
  Vec vel_local;
  
  ierr = PDEHelmholtzGetDM(pde,&dm);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nen,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = VecGetDM(vel,&dmv);CHKERRQ(ierr);
  if (!dmv) SETERRQ(PetscObjectComm((PetscObject)pde),PETSC_ERR_USER,"No DM composed with velocity");
  ierr = DMGetLocalVector(dmv,&vel_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmv,vel,INSERT_VALUES,vel_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmv,vel,INSERT_VALUES,vel_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(vel_local,&LA_vel);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscReal vel_mag,vel_mag_max = 0.0;
    PetscReal cell_kappa,cell_h[2],cell_h_max,vel_b[2];
    const PetscInt *cell_basis;
    
    cell_basis = &element[e*nbasis];
    
    /* get cell h */
    ierr = DMTetGeometryComputeElementDiameter2d(dm,e,&cell_h[0],&cell_h[1]);CHKERRQ(ierr);
    cell_h_max = cell_h[1];
    
    for (k=0; k<nbasis; k++) {
      vel_b[0] = LA_vel[2*cell_basis[k] + 0];
      vel_b[1] = LA_vel[2*cell_basis[k] + 1];
      vel_mag = PetscSqrtReal(vel_b[0]*vel_b[0] + vel_b[1]*vel_b[1]);
      vel_mag_max = PetscMax(vel_mag_max,vel_mag);
    }
    
    cell_kappa = vel_mag_max * cell_h_max / target_cell_Pe;
    cell_kappa_max = PetscMax(cell_kappa_max,cell_kappa);
  }
  ierr = VecRestoreArrayRead(vel_local,&LA_vel);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmv,&vel_local);CHKERRQ(ierr);
  ierr = MPI_Allreduce(&cell_kappa_max,&kappa_g,1,MPIU_REAL,MPIU_MAX,PETSC_COMM_WORLD);CHKERRQ(ierr);
  *_kappa = kappa_g;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DivergenceView"
PetscErrorCode DivergenceView(DM dmpack,Vec X,const char dir[],PetscInt step)
{
  PetscErrorCode ierr;
  Vec Xu,Xp;
  PetscInt pk;
  DM dmu,dmp,dm_divu;
  Vec div_u = NULL,Xu_local;
  Quadrature quadrature;
  EContainer ele_container;
  PetscInt e,i,q,nqp,nbasis,nbasis_g,nen;
  PetscInt *element,*element_g;
  PetscReal *coeff,*coords_g;
  PetscReal **GNxi,**GNeta,**GNx,**GNy,detJ_affine;
  const PetscReal *LA_Xu;
  PetscReal *elvel,*elcoords_g;
  char prefix[PETSC_MAX_PATH_LEN];
  const char *fields[] = { "div_vs" };

  
  ierr = DMCompositeGetEntries(dmpack,&dmu,&dmp);CHKERRQ(ierr);
  ierr = DMTetGetBasisOrder(dmu,&pk);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmu,&nen,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = DMTetGeometryElement(dmu,NULL,&nbasis_g,&element_g,NULL,NULL,&coords_g);CHKERRQ(ierr);
  
  ierr = DMTetCloneGeometry(dmu,1,DMTET_DG,pk-1,&dm_divu);CHKERRQ(ierr);

  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  ierr = QuadratureSetUpFromDM(quadrature,dm_divu);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature,nen,"div_u",1);CHKERRQ(ierr);
  
  ierr = EContainerCreate(dmu,quadrature,&ele_container);CHKERRQ(ierr);
  GNxi  = ele_container->dNr1;
  GNeta = ele_container->dNr2;
  GNx   = ele_container->dNx1;
  GNy   = ele_container->dNx2;
  elvel = ele_container->buf_basis_2vector_a;
  elcoords_g = ele_container->buf_basis_2vector_b;
  
  ierr = QuadratureGetProperty(quadrature,"div_u",&nen,&nqp,NULL,&coeff);CHKERRQ(ierr);
  
  ierr = DMCompositeGetAccess(dmpack,X,&Xu,&Xp);CHKERRQ(ierr);

  ierr = DMGetLocalVector(dmu,&Xu_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmu,Xu,INSERT_VALUES,Xu_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmu,  Xu,INSERT_VALUES,Xu_local);CHKERRQ(ierr);

  ierr = VecGetArrayRead(Xu_local,&LA_Xu);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = element[nbasis*e + i];
      elvel[2*i+0] = LA_Xu[2*nidx+0];
      elvel[2*i+1] = LA_Xu[2*nidx+1];
    }
    
    /* get coordinates */
    for (i=0; i<nbasis_g; i++) {
      PetscInt nidx = element_g[nbasis_g*e + i];
      elcoords_g[2*i + 0] = coords_g[2*nidx+0];
      elcoords_g[2*i + 1] = coords_g[2*nidx+1];
    }
    
    EvaluateBasisGeometryDerivatives_Affine(nqp,nbasis,elcoords_g,GNxi,GNeta,GNx,GNy,&detJ_affine);
    
    for (q=0; q<nqp; q++) {
      coeff[e*nqp + q] = 0.0;
      for (i=0; i<nbasis; i++) {
        coeff[e*nqp + q] += GNx[q][i] * elvel[2*i+0];
        coeff[e*nqp + q] += GNy[q][i] * elvel[2*i+1];
      }
    }
  }
  ierr = VecRestoreArrayRead(Xu_local,&LA_Xu);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmu,&Xu_local);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(dmpack,X,&Xu,&Xp);CHKERRQ(ierr);

  
  ierr = DMTetProjectQuadratureField(quadrature,"div_u",dm_divu,&div_u);CHKERRQ(ierr);
  
  if (dir) {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_div_vs_",dir,step);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_div_vs_",step);CHKERRQ(ierr);
  }
  //#ifdef SUBFUSC_USE_BINARY_PV
  //ierr = DMTetViewFields_VTU_b(dm_divu,1,&div_u,fields,prefix);CHKERRQ(ierr);
  //#elif
  ierr = DMTetViewFields_VTU(dm_divu,1,&div_u,fields,prefix);CHKERRQ(ierr);
  //#endif
  ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_div_vs_",step);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU2(dm_divu,1,&div_u,fields,dir,prefix,NULL);CHKERRQ(ierr);
  
  ierr = VecDestroy(&div_u);CHKERRQ(ierr);
  ierr = QuadratureDestroy(&quadrature);CHKERRQ(ierr);
  ierr = EContainerDestroy(&ele_container);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_divu);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyComputeSolidLiquidVelocityCFLTimestep"
PetscErrorCode DarcyComputeSolidLiquidVelocityCFLTimestep(DarcyCtx darcy,PetscReal target_cfl,PetscReal *_dt)
{
  PetscReal range_h[2],dt_min;
  PetscInt e,nel,k,nbasis;
  const PetscReal *LA_vl,*LA_vs;
  PetscInt *element;
  DM dm;
  Vec vel_l,vel_s,vel_l_local,vel_s_local;
  PetscReal dt_l,dt_g;
  PetscErrorCode ierr;
  
  dm = darcy->dm_c_phi_T;
  vel_l = darcy->vl;
  vel_s = darcy->vs;
  
  ierr = DMGetLocalVector(darcy->dm_adv_v,&vel_l_local);CHKERRQ(ierr);
  ierr = DMGetLocalVector(darcy->dm_adv_v,&vel_s_local);CHKERRQ(ierr);
  
  ierr = DMGlobalToLocalBegin(darcy->dm_adv_v,vel_l,INSERT_VALUES,vel_l_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_adv_v,  vel_l,INSERT_VALUES,vel_l_local);CHKERRQ(ierr);

  ierr = DMGlobalToLocalBegin(darcy->dm_adv_v,vel_s,INSERT_VALUES,vel_s_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(darcy->dm_adv_v,  vel_s,INSERT_VALUES,vel_s_local);CHKERRQ(ierr);

  ierr = VecGetArrayRead(vel_l_local,&LA_vl);CHKERRQ(ierr);
  ierr = VecGetArrayRead(vel_s_local,&LA_vs);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dm,&nel,&nbasis,&element,NULL,NULL,NULL);CHKERRQ(ierr);
  
  dt_min = 1.0e32;
  for (e=0; e<nel; e++) {
    PetscReal cell_v,cell_max_v,cell_dt;
    
    ierr = DMTetGeometryComputeElementDiameter2d(dm,e,&range_h[0],&range_h[1]);CHKERRQ(ierr);
    
    cell_max_v = 0.0;
    for (k=0; k<nbasis; k++) {
      PetscInt bidx = element[nbasis*e + k];
      cell_v = PetscSqrtReal(LA_vl[2*bidx+0]*LA_vl[2*bidx+0] + LA_vl[2*bidx+1]*LA_vl[2*bidx+1]);
      cell_max_v = PetscMax(cell_max_v,cell_v);
      cell_v = PetscSqrtReal(LA_vs[2*bidx+0]*LA_vs[2*bidx+0] + LA_vs[2*bidx+1]*LA_vs[2*bidx+1]);
      cell_max_v = PetscMax(cell_max_v,cell_v);
    }
    cell_dt = range_h[0] / cell_max_v;
    dt_min = PetscMin(dt_min,cell_dt);
  }
  dt_l = target_cfl * dt_min;
  
  ierr = MPI_Allreduce(&dt_l,&dt_g,1,MPIU_REAL,MPIU_MIN,PETSC_COMM_WORLD);CHKERRQ(ierr);
  *_dt = dt_g;
  
  ierr = VecRestoreArrayRead(vel_s_local,&LA_vs);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(vel_l_local,&LA_vl);CHKERRQ(ierr);

  ierr = DMRestoreLocalVector(darcy->dm_adv_v,&vel_s_local);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(darcy->dm_adv_v,&vel_l_local);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
