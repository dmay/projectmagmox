
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petscsnes.h>

#include <tpctx.h>
#include <mec.h>


#undef __FUNCT__
#define __FUNCT__ "SetIC_H2O_blob"
PetscErrorCode SetIC_H2O_blob(PetscScalar coor[],PetscScalar val[],void *data)
{
  TPCtx tp;
  PetscReal depth,slab_dip,plate_thickness,r2;

  tp = (TPCtx)data;
  SFUSCValidType(tp,TWOPHASE_CLASSID,3);
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  PLATE_THICKNESS; // normalized
  slab_dip = SLAB_DIP*PETSC_PI/180.0;
  
  r2 = (coor[0]-0.8)*(coor[0]-0.8) + (depth-0.2)*(depth-0.2);
  
  val[0] = 1.0e-5 * PetscExpReal(-330.0 * r2);
  val[0] += 1.0e-10;
  
  if (depth < plate_thickness+1.0e-10) { /* bottom of plate boundary */
    val[0] = 1.0e-10;
  }

  if (depth > coor[0]*tan(slab_dip) + 1.0e-10) { /* slab */
    val[0] = 1.0e-10;
  }
  
  PetscFunctionReturn(0);
}

