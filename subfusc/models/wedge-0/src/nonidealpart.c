
#include <petsc.h>
#include <nonidealpart.h>

# define GAS_CONSTANT 8.314   // gas constant

#define H2O_INDEX 0
#define CO2_INDEX 1
#define RS_INDEX  2 /* Rock silicate */

const char *nipart_component_names[] = { "H2O", "CO2", "RS" };

typedef struct{
  PetscReal c_blkh2o;
  PetscReal c_blkco2;
  PetscReal Kh2o;
  PetscReal Kco2;
  PetscReal Wmh;
  PetscReal Wmc;
  PetscReal T,P;
} ThermoParameters;


#undef __FUNCT__
#define __FUNCT__ "FormResidual_2c"
PetscErrorCode FormResidual_2c(const PetscReal fc_array[], PetscReal f_array[], ThermoParameters *p)
{
  PetscFunctionBegin;
  f_array[0] = p->c_blkh2o - fc_array[0]*fc_array[1] - (1.0 - fc_array[0])*fc_array[1]*p->Kh2o*PetscExpReal((p->Wmh*((1.0 - fc_array[1])*(1.0 - fc_array[1])))/(GAS_CONSTANT*p->T));
  f_array[1] = p->c_blkco2 - fc_array[0]*(1.0 - fc_array[1]) - (1.0 - fc_array[0])*(1.0 - fc_array[1])*p->Kco2*PetscExpReal((p->Wmc*(fc_array[1]*fc_array[1]))/(GAS_CONSTANT*p->T));
  PetscFunctionReturn(0);
  
}

#undef __FUNCT__
#define __FUNCT__ "FormJacobian_2c"
PetscErrorCode FormJacobian_2c(const PetscReal xx[], PetscReal *A, ThermoParameters *p)
{
  PetscFunctionBegin;
  A[0] = -xx[1] + xx[1]*p->Kh2o*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[1] = -xx[0] - (1.0 - xx[0])*p->Kh2o*(1.0 - 2.0*xx[1]*(1.0 - xx[0])*p->Wmh/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[2] = -(1.0 - xx[1]) + (1.0 - xx[1])*p->Kco2*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));
  A[3] =  xx[0] - (1.0 - xx[0])*p->Kco2*(-1.0 + 2.0*xx[1]*(1.0 - xx[1])*p->Wmc/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SolveJacobian_2c"
PetscErrorCode SolveJacobian_2c(const PetscReal A[], const PetscReal f_array[], PetscReal dx[])
{
  PetscReal det, inv_det;

  PetscFunctionBegin;
  det     = A[0]*A[3] - A[1]*A[2];
  inv_det = 1.0/det;
  dx[0]   = inv_det * ( A[3] * f_array[0] - A[1] * f_array[1]);
  dx[1]   = inv_det * (-A[2] * f_array[0] + A[0] * f_array[1]);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NewtonSolver_2c"
PetscErrorCode NewtonSolver_2c(ThermoParameters *p, PetscReal atol, PetscReal rtol, PetscReal stol, PetscInt maxit,
                            PetscReal fc_array[], PetscInt *converged_reason)
{
  PetscReal       J[4],F[2];
  PetscReal       dX[2];
  PetscInt        its = 0;
  PetscReal       a0,a1,r,d;
  PetscErrorCode  ierr;
  
  PetscFunctionBegin;
  *converged_reason = 0; // iterating
  d    = 1.0; // norm of solution change
  r    = 1.0; // ratio of residual norm between current and last iterations
  
  ierr = FormResidual_2c(fc_array, F, p);CHKERRQ(ierr);
  a0   = PetscSqrtReal(F[0]*F[0] + F[1]*F[1]);
  /*printf("%d:  ||F|| % 1.12e \n", its, a0);*/
  a1 = a0;
  
  if (a1 < atol) { *converged_reason = -1; goto converged; }
  
  while (its <= maxit) {
    
    its++;
    
    ierr  = FormJacobian_2c(fc_array, J, p);CHKERRQ(ierr);
    ierr  = SolveJacobian_2c(J, F, dX);CHKERRQ(ierr);
    
    fc_array[0] = fc_array[0] - dX[0];
    fc_array[1] = fc_array[1] - dX[1];
    
    ierr = FormResidual_2c(fc_array, F, p);CHKERRQ(ierr);
    a1   = PetscSqrtReal(F[0]*F[0] + F[1]*F[1]);
    r    = a1/a0;
    d    = PetscSqrtReal(dX[0]*dX[0] + dX[1]*dX[1]);
    /*printf("%d:  ||F|| % 1.12e \n", its, a1);*/
  
    if (a1 < atol) {    *converged_reason = -1; goto converged; }
    if (r < rtol) {     *converged_reason = -2; goto converged; }
    
    if (d < stol) {     *converged_reason = 1; goto converged; }
    if (its >= maxit) { *converged_reason = 2; goto converged; }
  }
  
  converged:
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "equilibrium_calc"
PetscErrorCode equilibrium_calc(ThermoParameters *params,
                                PetscReal *fc_guess,
                                PetscReal *phi,
                                PetscReal composition_s[],
                                PetscReal composition_l[],
                                PetscInt *converged_reason,PetscBool *validtype)
{
  PetscReal      fc_array[2];
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  
  fc_array[0] = fc_guess[0];
  fc_array[1] = fc_guess[1];
  
  *validtype = PETSC_TRUE;
  ierr = NewtonSolver_2c(params, 1e-12, 1e-24, 1e-12, 100, fc_array, converged_reason);CHKERRQ(ierr);
  if (*converged_reason >= 0) {
    *validtype = PETSC_FALSE;
  }
  
  if (*validtype) {
    if ((fc_array[0] > 0.0) && (fc_array[0] < 1.0) && (fc_array[1] > 0.0) && (fc_array[1] < 1.0)){
      *phi = fc_array[0];
      composition_l[0] = fc_array[1];
      composition_l[1] = 1.0 - fc_array[1];
      composition_l[2] = 1.0 - composition_l[0] - composition_l[1]; /* unity sum applied to eliminated component */
      
      composition_s[0] = params->Kh2o * PetscExpReal(params->Wmh * composition_l[1]*composition_l[1]/(GAS_CONSTANT*params->T)) * composition_l[0];
      composition_s[1] = params->Kco2 * PetscExpReal(params->Wmc * composition_l[0]*composition_l[0]/(GAS_CONSTANT*params->T)) * composition_l[1];
      composition_s[2] = 1.0 - composition_s[0] - composition_s[1]; /* unity sum applied to eliminated component */
    } else {
      *phi = 0.0;
      composition_l[0] = 0.0;
      composition_l[1] = 0.0;
      composition_l[2] = 0.0;
      
      composition_s[0] = params->c_blkh2o;
      composition_s[1] = params->c_blkco2;
      composition_s[2] = 1.0 - params->c_blkh2o - params->c_blkco2; /* unity sum applied to eliminated component */
    }
  }
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "NonIdealPartEvaluate_sediment"
PetscErrorCode NonIdealPartEvaluate_sediment(
                              const PetscReal P[], const PetscReal T[],
                              const PetscInt ncomponents,
                              const PetscReal C_bulk[],
                              PetscReal C_liquid[],PetscReal C_solid[],
                              PetscReal *phi,
                              PetscBool *isValid,
                              void *ctx)
{
  NonIdealPart     data = (NonIdealPart)ctx;
  SedData          sed = data->sediment;
  ThermoParameters params;
  PetscReal        fc_guess[2];
  PetscReal        cmaxh2o, Lrh2o, Tmh2o, cmaxco2, Lrco2, Tmco2, deltaTco2;
  PetscReal        *cmaxh, *logLrh, *Tmh, *logLrc, *Tmc, *deltaTc;
  PetscReal        cmaxc, P2,P3,P4,P5,logP;
  PetscInt         converged_reason;
  PetscErrorCode   ierr;
  
  // pack
  params.c_blkh2o = C_bulk[H2O_INDEX];
  params.c_blkco2 = C_bulk[CO2_INDEX];
  params.T        = T[0];
  params.P        = P[0];

  P2 = params.P * params.P;
  P3 = P2 * params.P;
  P4 = P3 * params.P;
  P5 = P4 * params.P;

  logP = PetscLogReal(params.P);

  // init sediment
  cmaxh    = sed->cmaxh;
  logLrh   = sed->logLrh;
  Tmh      = sed->Tmh;
  cmaxc    = sed->cmaxc;
  logLrc   = sed->logLrc;
  Tmc      = sed->Tmc;
  deltaTc  = sed->deltaTc;
  params.Wmh  = sed->Wmh;
  params.Wmc  = sed->Wmc;
  
  cmaxh2o  = PetscExpReal(cmaxh[0]*(logP*logP) + cmaxh[1]*logP + cmaxh[2]);

  Lrh2o    = PetscExpReal(
                logLrh[0]/P5
              + logLrh[1]/P4
              + logLrh[2]/P3
              + logLrh[3]/P2
              + logLrh[4]/params.P
              + logLrh[5]);
  
  Tmh2o    = Tmh[0]*P5
              + Tmh[1]*P4
              + Tmh[2]*P3
              + Tmh[3]*P2
              + Tmh[4]*params.P
              + Tmh[5];
  
  cmaxco2  = cmaxc;
  
  Lrco2    = PetscExpReal(
                logLrc[0]/P5
              + logLrc[1]/P4
              + logLrc[2]/P3
              + logLrc[3]/P2
              + logLrc[4]/params.P
              + logLrc[5]);
  
  Tmco2    = Tmc[0]*P2 + Tmc[1]*params.P + Tmc[2];
  
  deltaTco2 = deltaTc[0]*P2 + deltaTc[1]*params.P + deltaTc[2];
  
  Tmco2     = Tmco2 - deltaTco2;

  params.Kh2o   = cmaxh2o*PetscExpReal(Lrh2o*(1.0/params.T - 1.0/Tmh2o))/100.0;
  
  params.Kco2   = cmaxco2*PetscExpReal(Lrco2*(1.0/params.T - 1.0/Tmco2))/100.0;

  // solve
  fc_guess[0] = 1e-6;
  fc_guess[1] = 7.5e-1;
  
  ierr = equilibrium_calc(&params,fc_guess,
                          phi,C_solid,C_liquid,&converged_reason,isValid);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartEvaluate_MORB"
PetscErrorCode NonIdealPartEvaluate_MORB(
                                             const PetscReal P[], const PetscReal T[],
                                             const PetscInt ncomponents,
                                             const PetscReal C_bulk[],
                                             PetscReal C_liquid[],PetscReal C_solid[],
                                             PetscReal *phi,
                                             PetscBool *isValid,
                                             void *ctx)
{
  NonIdealPart     data = (NonIdealPart)ctx;
  MORBData         morb = data->morb;
  ThermoParameters params;
  PetscReal        fc_guess[2];
  PetscReal        cmaxh2o, Lrh2o, Tmh2o, cmaxco2, Lrco2, Tmco2, deltaTco2;
  PetscReal        *cmaxh, *logLrh, *Tmh, *logLrc, *Tmc, *deltaTc;
  PetscReal        cmaxc, P2,P3,P4,P5;
  PetscInt         converged_reason;
  PetscErrorCode   ierr;
  
  // pack
  params.c_blkh2o = C_bulk[H2O_INDEX];
  params.c_blkco2 = C_bulk[CO2_INDEX];
  params.T        = T[0];
  params.P        = P[0];
  
  P2 = params.P * params.P;
  P3 = P2 * params.P;
  P4 = P3 * params.P;
  P5 = P4 * params.P;

  // init MORB
  cmaxh      = morb->cmaxh;
  logLrh     = morb->logLrh;
  Tmh        = morb->Tmh;
  cmaxc      = morb->cmaxc;
  logLrc     = morb->logLrc;
  Tmc        = morb->Tmc;
  deltaTc    = morb->deltaTc;
  params.Wmh = morb->Wmh;
  params.Wmc = morb->Wmc;
  
  cmaxh2o  = PetscExpReal(
                cmaxh[0]*P3
              + cmaxh[1]*P2
              + cmaxh[2]*params.P
              + cmaxh[3]);
  
  Lrh2o    = PetscExpReal(
                logLrh[0]/P3
              + logLrh[1]/P2
              + logLrh[2]/params.P
              + logLrh[3]);
  
  Tmh2o    = Tmh[0]*P5
              + Tmh[1]*P4
              + Tmh[2]*P3
              + Tmh[3]*P2
              + Tmh[4]*params.P
              + Tmh[5];
  
  cmaxco2  = cmaxc;
  Lrco2    = PetscExpReal(
                logLrc[0]/P5
              + logLrc[1]/P4
              + logLrc[2]/P3
              + logLrc[3]/P2
              + logLrc[4]/params.P
              + logLrc[5]);
  
  Tmco2    = Tmc[0]*P2 + Tmc[1]*params.P + Tmc[2];
  
  deltaTco2 = deltaTc[0]*P2 + deltaTc[1]*params.P + deltaTc[2];
  
  Tmco2     = Tmco2 - deltaTco2;

  params.Kh2o   = cmaxh2o*PetscExpReal(Lrh2o*(1.0/params.T - 1.0/Tmh2o))/100.0;
  
  params.Kco2   = cmaxco2*PetscExpReal(Lrco2*(1.0/params.T - 1.0/Tmco2))/100.0;

  // solve
  fc_guess[0] = 1e-6;
  fc_guess[1] = 7.5e-1;
  
  ierr = equilibrium_calc(&params,fc_guess,
                          phi,C_solid,C_liquid,&converged_reason,isValid);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartEvaluate_Gabbro"
PetscErrorCode NonIdealPartEvaluate_Gabbro(
                                         const PetscReal P[], const PetscReal T[],
                                         const PetscInt ncomponents,
                                         const PetscReal C_bulk[],
                                         PetscReal C_liquid[],PetscReal C_solid[],
                                         PetscReal *phi,
                                         PetscBool *isValid,
                                         void *ctx)
{
  NonIdealPart     data = (NonIdealPart)ctx;
  GabbroData       gabbro = data->gabbro;
  ThermoParameters params;
  PetscReal        fc_guess[2];
  PetscReal        cmaxh2o, Lrh2o, Tmh2o, cmaxco2, Lrco2, Tmco2, deltaTco2;
  PetscReal        *cmaxh, *logLrh, *Tmh, *logLrc, *Tmc, *deltaTc;
  PetscReal        cmaxc, P2,P3,P4,logP;
  PetscInt         converged_reason;
  PetscErrorCode   ierr;
  
  // pack
  params.c_blkh2o = C_bulk[H2O_INDEX];
  params.c_blkco2 = C_bulk[CO2_INDEX];
  params.T        = T[0];
  params.P        = P[0];
  
  P2 = params.P * params.P;
  P3 = P2 * params.P;
  P4 = P3 * params.P;

  logP = PetscLogReal(params.P);
  
  // init Gabbro
  cmaxh      = gabbro->cmaxh;
  logLrh     = gabbro->logLrh;
  Tmh        = gabbro->Tmh;
  cmaxc      = gabbro->cmaxc;
  logLrc     = gabbro->logLrc;
  Tmc        = gabbro->Tmc;
  deltaTc    = gabbro->deltaTc;
  params.Wmh = gabbro->Wmh;
  params.Wmc = gabbro->Wmc;
  
  cmaxh2o  = PetscExpReal(
                cmaxh[0]*(logP*logP)
              + cmaxh[1]*logP
              + cmaxh[2]);
  
  Lrh2o    = PetscExpReal(
                logLrh[0]/P4
              + logLrh[1]/P3
              + logLrh[2]/P2
              + logLrh[3]/params.P
              + logLrh[4]);
  
  Tmh2o    = Tmh[0]*P4
              + Tmh[1]*P3
              + Tmh[2]*P2
              + Tmh[3]*params.P
              + Tmh[4];
  
  cmaxco2  = cmaxc;
  
  Lrco2    = PetscExpReal(logLrc[0]/P3
              + logLrc[1]/P2
              + logLrc[2]/params.P
              + logLrc[3]);
  
  Tmco2    = Tmc[0]*P2 + Tmc[1]*params.P + Tmc[2];
  
  deltaTco2 = deltaTc[0]*P2 + deltaTc[1]*params.P + deltaTc[2];

  Tmco2     = Tmco2 - deltaTco2;
  
  params.Kh2o   = cmaxh2o*PetscExpReal(Lrh2o*(1.0/params.T - 1.0/Tmh2o))/100.0;
  
  params.Kco2   = cmaxco2*PetscExpReal(Lrco2*(1.0/params.T - 1.0/Tmco2))/100.0;

  // solve
  fc_guess[0] = 1e-6;
  fc_guess[1] = 7.5e-1;
  
  ierr = equilibrium_calc(&params,fc_guess,
                          phi,C_solid,C_liquid,&converged_reason,isValid);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartEvaluate_PUM"
PetscErrorCode NonIdealPartEvaluate_PUM(
                                           const PetscReal P[], const PetscReal T[],
                                           const PetscInt ncomponents,
                                           const PetscReal C_bulk[],
                                           PetscReal C_liquid[],PetscReal C_solid[],
                                           PetscReal *phi,
                                           PetscBool *isValid,
                                           void *ctx)
{
  NonIdealPart     data = (NonIdealPart)ctx;
  PUMData          pum = data->pum;
  ThermoParameters params;
  PetscReal        fc_guess[2];
  PetscReal        cmaxh2o, Lrh2o, Tmh2o;
  PetscReal        *cmaxh, *logLrh, *Tmh;
  PetscReal        P2,P3,P4,P5,P6,P7,P8;
  PetscReal        T1, T2, T3;
  PetscInt         converged_reason;
  PetscErrorCode   ierr;
  
  // pack
  params.c_blkh2o = C_bulk[H2O_INDEX];
  params.c_blkco2 = C_bulk[CO2_INDEX];
  params.T        = T[0];
  params.P        = P[0];
  
  P2 = params.P * params.P;
  P3 = P2 * params.P;
  P4 = P3 * params.P;
  P5 = P4 * params.P;
  P6 = P5 * params.P;
  P7 = P6 * params.P;
  P8 = P7 * params.P;

  // init PUM
  cmaxh      = pum->cmaxh;
  logLrh     = pum->logLrh;
  Tmh        = pum->Tmh;
  params.Wmh = pum->Wmh;
  params.Wmc = pum->Wmc;
  
  T1       = params.P*1.072054506210788e+02 + 8.966542633680793e+02;
  T2       = params.P*1.533210093204700e+02 + 8.433396814457046e+02;
  T3       = params.P*2.491790919291565e+02 + 8.375304594099354e+02;
  
  if (T1 < T2) {
    if (params.T < T1) {
      params.Kco2 = 32.884202059134026;
    } else if ((params.T >= T1) && (params.T < T2)) {
      params.Kco2 = 28.961173923316494;
    } else if ((params.T >= T2) && (params.T < T3)) {
      params.Kco2 = 14.752123772328618;
    } else {
      params.Kco2 = 0.323043468637191;
    }
  } else {
    if (params.T < T2) {
      params.Kco2 = 32.884202059134026;
    } else if ((params.T >= T2) && (params.T < T3)) {
      params.Kco2 = 14.752123772328618;
    } else {
      params.Kco2 = 0.323043468637191;
    }
  }
  
  params.Kco2 = params.Kco2/100.0;
  
  cmaxh2o  = PetscExpReal(cmaxh[0]*params.P + cmaxh[1]);
  
  Lrh2o    = PetscExpReal(
                logLrh[0]/P8
              + logLrh[1]/P7
              + logLrh[2]/P6
              + logLrh[3]/P5
              + logLrh[4]/P4
              + logLrh[5]/P3
              + logLrh[6]/P2
              + logLrh[7]/params.P
              + logLrh[8]);

  Tmh2o    = Tmh[0]*P2  + Tmh[1]*params.P + Tmh[2];
  
  params.Kh2o   = cmaxh2o*PetscExpReal(Lrh2o*(1.0/params.T - 1.0/Tmh2o))/100.0;
  
  //if (rocktype != 4) {
  //  params.Kco2   = cmaxco2*PetscExpReal(Lrco2*(1.0/params.T - 1.0/Tmco2))/100.0;
  //}
  
  // solve
  fc_guess[0] = 1e-6;
  fc_guess[1] = 7.5e-1;
  
  ierr = equilibrium_calc(&params,fc_guess,
                          phi,C_solid,C_liquid,&converged_reason,isValid);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartGetNumComponents"
PetscErrorCode NonIdealPartGetNumComponents(PetscInt *nc)
{
  *nc = 3;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartGetComponentName"
PetscErrorCode NonIdealPartGetComponentName(PetscInt index,const char **name)
{
  switch (index) {
    case 0:
      *name = nipart_component_names[index];
      break;
    case 1:
      *name = nipart_component_names[index];
      break;
    case 2:
      *name = nipart_component_names[index];
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"NonIdealPart defines 3 components. Index %D is not a valid component index",index);
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartGetComponentActivity"
PetscErrorCode NonIdealPartGetComponentActivity(PetscInt index,PetscBool *solid,PetscBool *liquid)
{
  switch (index) {
    case 0:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
    case 1:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
    case 2:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"NonIdealPart defines 3 components. Index %D is not a valid component index",index);
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ImportThermoData_sediment"
PetscErrorCode ImportThermoData_sediment(SedData sed)
{
  PetscReal *p;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  
  /*************  sed  ******************/
  ierr  =  PetscMalloc1(3, &sed->cmaxh);     CHKERRQ(ierr);
  p     =  sed->cmaxh;
  p[0]  =  -0.150662555544386;
  p[1]  =  0.301807008226067;
  p[2]  =  1.018673820928173;
  
  ierr  =  PetscMalloc1(6, &sed->logLrh);    CHKERRQ(ierr);
  p     =  sed->logLrh;
  p[0]  =  -3.533135743197558;
  p[1]  =  19.068737477212224;
  p[2]  =  -37.302837204809386;
  p[3]  =  31.324429026695910;
  p[4]  =  -10.317259468993946;
  p[5]  =  8.688808175310820;
  
  ierr  =  PetscMalloc1(6, &sed->Tmh);        CHKERRQ(ierr);
  p     =  sed->Tmh;
  p[0]  =  1.296369387210849;
  p[1]  =  -24.330963411722223;
  p[2]  =  1.697986731996718e+02;
  p[3]  =  -5.368530912624840e+02;
  p[4]  =  7.629895101455530e+02;
  p[5]  =  2.423527272231045e+02;
  
  sed->cmaxc    =  10.592282487499954;
  
  ierr  =  PetscMalloc1(6, &sed->logLrc);     CHKERRQ(ierr);
  p     =  sed->logLrc;
  p[0]  =  14.007787232073160;
  p[1]  =  -81.025362109897810;
  p[2]  =  1.746329337200755e+02;
  p[3]  =  -1.700791911486186e+02;
  p[4]  =  69.688713478378430;
  p[5]  =  1.378376757567256;
  
  ierr  =  PetscMalloc1(3, &sed->Tmc);        CHKERRQ(ierr);
  p     =  sed->Tmc;
  p[0]  =  -19.534288871002960;
  p[1]  =  2.116075955389894e+02;
  p[2]  =  7.026318145363642e+02;
  
  ierr  =  PetscMalloc1(3, &sed->deltaTc);   CHKERRQ(ierr);
  p     =  sed->deltaTc;
  p[0]  =  -17.780655913253780;
  p[1]  =  1.290578179906597e+02;
  p[2]  =  -10.089551673592354;
  
  sed->Wmh    =  0.0;
  sed->Wmc    =  -8000.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ImportThermoData_MORB"
PetscErrorCode ImportThermoData_MORB(MORBData morb)
{
  PetscReal *p;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  /*************  morb  ******************/
  ierr  =  PetscMalloc1(4, &morb->cmaxh);     CHKERRQ(ierr);
  p     =  morb->cmaxh;
  p[0]  =  0.010272471440474;
  p[1]  =  -0.115390189642000;
  p[2]  =  0.324451844950027;
  p[3]  =  1.415887869218784;
  
  ierr  =  PetscMalloc1(4, &morb->logLrh);    CHKERRQ(ierr);
  p     =  morb->logLrh;
  p[0]  =  1.312630638962006;
  p[1]  =  -3.941703113796766;
  p[2]  =  2.750798457184458;
  p[3]  =  8.431772535722878;
  
  ierr  =  PetscMalloc1(6, &morb->Tmh);        CHKERRQ(ierr);
  p     =  morb->Tmh;
  p[0]  =  0.271046787787927;
  p[1]  =  -8.445036183735617;
  p[2]  =  82.556558104728500;
  p[3]  =  -3.342487965612544e+02;
  p[4]  =  5.592352636804376e+02;
  p[5]  =  3.967635986627150e+02;
  
  morb->cmaxc =  19.045565762499937;
  
  ierr  =  PetscMalloc1(6, &morb->logLrc);     CHKERRQ(ierr);
  p     =  morb->logLrc;
  p[0]  =  1.367443166281999;
  p[1]  =  -18.846082066295384;
  p[2]  =  62.960752263466270;
  p[3]  =  -80.779851142775770;
  p[4]  =  40.165085572683090;
  p[5]  =  4.354032367607715;
  
  ierr  =  PetscMalloc1(3, &morb->Tmc);        CHKERRQ(ierr);
  p     =  morb->Tmc;
  p[0]  =  -17.658692732447540;
  p[1]  =  1.944885995727770e+02;
  p[2]  =  7.481072175613556e+02;
  
  ierr  =  PetscMalloc1(3, &morb->deltaTc);   CHKERRQ(ierr);
  p     =  morb->deltaTc;
  p[0]  =  -17.780655913253780;
  p[1]  =  1.290578179906597e+02;
  p[2]  =  -10.089551673592354;
  
  morb->Wmh =  0.0;
  morb->Wmc =  -8000.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ImportThermoData_Gabbro"
PetscErrorCode ImportThermoData_Gabbro(GabbroData gabbro)
{
  PetscReal *p;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  /*************  gabbro  ******************/
  ierr  =  PetscMalloc1(3, &gabbro->cmaxh);     CHKERRQ(ierr);
  p     =  gabbro->cmaxh;
  p[0]  =  -0.017667335340126;
  p[1]  =  0.089304408418261;
  p[2]  =  1.527316327453626;
  
  ierr  =  PetscMalloc1(5, &gabbro->logLrh);    CHKERRQ(ierr);
  p     =  gabbro->logLrh;
  p[0]  =  -1.009961038518608;
  p[1]  =  5.217453113389753;
  p[2]  =  -8.951498304566513;
  p[3]  =  4.747716716081742;
  p[4]  =  8.516063062352552;
  
  ierr  =  PetscMalloc1(5, &gabbro->Tmh);        CHKERRQ(ierr);
  p     =  gabbro->Tmh;
  p[0]  =  -3.000118134386033;
  p[1]  =  45.352910295461910;
  p[2]  =  -2.388331789925089e+02;
  p[3]  =  5.170575818336469e+02;
  p[4]  =  3.756317222662897e+02;
  
  gabbro->cmaxc =  19.379454537499885;
  
  ierr  =  PetscMalloc1(4, &gabbro->logLrc);     CHKERRQ(ierr);
  p     =  gabbro->logLrc;
  p[0]  =  2.739281957244409;
  p[1]  =  -9.851731701468713;
  p[2]  =  9.146439180475213;
  p[3]  =  9.055596526151467;
  
  ierr  =  PetscMalloc1(3, &gabbro->Tmc);        CHKERRQ(ierr);
  p     =  gabbro->Tmc;
  p[0]  =  -14.165093677306569;
  p[1]  =  1.809036929023690e+02;
  p[2]  =  7.757132892363031e+02;
  
  ierr  =  PetscMalloc1(3, &gabbro->deltaTc);   CHKERRQ(ierr);
  p     =  gabbro->deltaTc;
  p[0]  =  -11.794472521973619;
  p[1]  =  89.136690826367680;
  p[2]  =  1.711230429250124;
  
  gabbro->Wmh =  0.0;
  gabbro->Wmc =  -50000.0;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ImportThermoData_PUM"
PetscErrorCode ImportThermoData_PUM(PUMData pum)
{
  PetscReal *p;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;

  /*************  pum  ******************/
  ierr  =  PetscMalloc1(2, &pum->cmaxh);     CHKERRQ(ierr);
  p     =  pum->cmaxh;
  p[0]  =  0.001156281237125;
  p[1]  =  2.421787983195434;
  
  ierr  =  PetscMalloc1(9, &pum->logLrh);    CHKERRQ(ierr);
  p     =  pum->logLrh;
  p[0]  =  -19.060885857770412;
  p[1]  =  1.689826532012307e+02;
  p[2]  =  -6.300318077012928e+02;
  p[3]  =  1.281835967642164e+03;
  p[4]  =  -1.543141708645560e+03;
  p[5]  =  1.111879888618042e+03;
  p[6]  =  -4.591417538523027e+02;
  p[7]  =  95.414320683178630;
  p[8]  =  1.972456309737587;
  
  ierr  =  PetscMalloc1(3, &pum->Tmh);        CHKERRQ(ierr);
  p     =  pum->Tmh;
  p[0]  =  -15.113105096276987;
  p[1]  =  92.697643261252640;
  p[2]  =  6.389776673404846e+02;
  
  pum->Wmh =  0.0;
  pum->Wmc =  -50000.0;
  
  PetscFunctionReturn(0);  
  
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartCreate"
PetscErrorCode NonIdealPartCreate(PetscInt rock_type,NonIdealPart *_ctx)
{
  NonIdealPart ctx;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_NonIdealPart));CHKERRQ(ierr);
  
  ctx->range_T[0] = 273.0;
  ctx->range_T[1] = PETSC_MAX_REAL;

  ctx->range_P[0] = 0.0;
  ctx->range_P[1] = PETSC_MAX_REAL;

  ctx->sediment = NULL;
  ctx->morb     = NULL;
  ctx->gabbro   = NULL;
  ctx->pum      = NULL;
  
  ctx->rock_type = rock_type;
  switch (rock_type) {
    case 1: /* sediment */
      ierr = PetscMalloc1(1,&ctx->sediment);
      ierr = ImportThermoData_sediment(ctx->sediment);CHKERRQ(ierr);
      ctx->evaluate = NonIdealPartEvaluate_sediment;
      break;
    case 2: /* MORB */
      ierr = PetscMalloc1(1,&ctx->morb);
      ierr = ImportThermoData_MORB(ctx->morb);CHKERRQ(ierr);
      ctx->evaluate = NonIdealPartEvaluate_MORB;
      break;
    case 3: /* Gabbro */
      ierr = PetscMalloc1(1,&ctx->gabbro);
      ierr = ImportThermoData_Gabbro(ctx->gabbro);CHKERRQ(ierr);
      ctx->evaluate = NonIdealPartEvaluate_Gabbro;
      break;
    case 4: /* PUM */
      ierr = PetscMalloc1(1,&ctx->pum);
      ierr = ImportThermoData_PUM(ctx->pum);CHKERRQ(ierr);
      ctx->evaluate = NonIdealPartEvaluate_PUM;
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unknown rock type. Valid values are {1:sedimnent , 2:MORB , 3:Gabbro , 4:Primitive upper mantle");
      break;
  }
  
  ctx->get_num_components = NonIdealPartGetNumComponents;
  ctx->get_component_name = NonIdealPartGetComponentName;
  ctx->get_component_phase_activity = NonIdealPartGetComponentActivity;
  
  *_ctx = ctx;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ClearThermoData"
PetscErrorCode ClearThermoData(NonIdealPart ctx)
{
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  if (ctx->sediment) {
    ierr = PetscFree(ctx->sediment->cmaxh);    CHKERRQ(ierr);
    ierr = PetscFree(ctx->sediment->logLrh);   CHKERRQ(ierr);
    ierr = PetscFree(ctx->sediment->Tmh);      CHKERRQ(ierr);
    ierr = PetscFree(ctx->sediment->logLrc);   CHKERRQ(ierr);
    ierr = PetscFree(ctx->sediment->Tmc);      CHKERRQ(ierr);
    ierr = PetscFree(ctx->sediment->deltaTc);  CHKERRQ(ierr);
    ierr = PetscFree(ctx->sediment);           CHKERRQ(ierr);
    ctx->sediment = NULL;
  }
  if (ctx->morb) {
    ierr = PetscFree(ctx->morb->cmaxh);        CHKERRQ(ierr);
    ierr = PetscFree(ctx->morb->logLrh);       CHKERRQ(ierr);
    ierr = PetscFree(ctx->morb->Tmh);          CHKERRQ(ierr);
    ierr = PetscFree(ctx->morb->logLrc);       CHKERRQ(ierr);
    ierr = PetscFree(ctx->morb->Tmc);          CHKERRQ(ierr);
    ierr = PetscFree(ctx->morb->deltaTc);      CHKERRQ(ierr);
    ierr = PetscFree(ctx->morb);               CHKERRQ(ierr);
    ctx->morb = NULL;
  }
  if (ctx->gabbro) {
    ierr = PetscFree(ctx->gabbro->cmaxh);      CHKERRQ(ierr);
    ierr = PetscFree(ctx->gabbro->logLrh);     CHKERRQ(ierr);
    ierr = PetscFree(ctx->gabbro->Tmh);        CHKERRQ(ierr);
    ierr = PetscFree(ctx->gabbro->logLrc);     CHKERRQ(ierr);
    ierr = PetscFree(ctx->gabbro->Tmc);        CHKERRQ(ierr);
    ierr = PetscFree(ctx->gabbro->deltaTc);    CHKERRQ(ierr);
    ierr = PetscFree(ctx->gabbro);             CHKERRQ(ierr);
    ctx->gabbro = NULL;
  }
  if (ctx->pum) {
    ierr = PetscFree(ctx->pum->cmaxh);         CHKERRQ(ierr);
    ierr = PetscFree(ctx->pum->logLrh);        CHKERRQ(ierr);
    ierr = PetscFree(ctx->pum->Tmh);           CHKERRQ(ierr);
    ierr = PetscFree(ctx->pum);                CHKERRQ(ierr);
    ctx->pum = NULL;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "NonIdealPartDestroy"
PetscErrorCode NonIdealPartDestroy(NonIdealPart *_ctx)
{
  NonIdealPart ctx;
  PetscErrorCode ierr;
  
  if (!_ctx) PetscFunctionReturn(0);
  ctx = *_ctx;
  ierr = ClearThermoData(ctx);CHKERRQ(ierr);
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  *_ctx = NULL;
  PetscFunctionReturn(0);
}
