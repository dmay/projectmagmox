
#include <petsc.h>
#include <proj_init_finalize.h>
#include <nonidealpart.h>

/*
 The behaviour of this test is identical to
   subfusc/slab-0/equilibrium
*/
#undef __FUNCT__
#define __FUNCT__ "test_1"
PetscErrorCode test_1(void)
{
  PetscErrorCode ierr;
  NonIdealPart distf;
  PetscReal T, P, w_h2o, w_co2;   // input values
  PetscBool found, validtype;
  PetscInt rocktype = 0;   // 1 for sed, 2 for morb, 3 for gabbro, and 4 for primitive upper mantle
  PetscReal C_bulk[3],C_liquid[3],C_solid[3],phi;
  
  
  found = PETSC_FALSE; ierr = PetscOptionsGetInt(NULL, NULL, "-rocktype", &rocktype, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a rocktype (from 1 to 4) via option -rocktype");

  ierr = NonIdealPartCreate(rocktype,&distf);CHKERRQ(ierr);
  
  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-T", &T, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a temperature (K) via option -T");
  
  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-P", &P, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a Pressure (GPa) via option -P");
  
  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-wh2o", &w_h2o, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a bulk concentration of H2O (weight fraction) via option -wh2o");
  
  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-wco2", &w_co2, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a bulk concentration of CO2 (weight fraction) via option -wco2");

  C_bulk[0] = w_h2o;
  C_bulk[1] = w_co2;
  C_bulk[2] = 1.0 - C_bulk[0] - C_bulk[1];
  
  ierr = distf->evaluate(&P,&T,3,C_bulk,
                         C_liquid,C_solid,&phi,&validtype,(void*)distf);CHKERRQ(ierr);

  
  
  if (validtype) {
    ierr = PetscPrintf(PETSC_COMM_SELF, "Calculated weight fraction of liquid phase is: %5.4e, h2o weight fraction in liquid is: %5.4e, in solid is: %5.4e; and co2 weight fraction in liquid is: %5.4e, in solid is: %5.4e\n", \
                       phi, C_liquid[0], C_solid[0], C_liquid[1], C_solid[1]); CHKERRQ(ierr);
  }else{
    ierr = PetscPrintf(PETSC_COMM_SELF, "Thermodynamic computation is not valid.\n");
  }
  
  ierr = NonIdealPartDestroy(&distf);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}



int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = test_1();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

