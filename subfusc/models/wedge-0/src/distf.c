
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <petsc.h>
#include <distf.h>

#define DISTF_LOG_PARANOID

const char tagp[] = {'\0'};

const char *distf_component_names[] = { "H2O", "RF", "FT" };

PetscErrorCode DistFEvaluate(const PetscReal P[], const PetscReal T[],
                             const PetscInt ncomponents,
                             const PetscReal C_bulk[],
                             PetscReal C_liquid[],PetscReal C_solid[],
                             PetscReal *phi,
                             PetscBool *valid,
                             void *ctx);
PetscErrorCode DistFGetNumComponents(PetscInt *nc);
PetscErrorCode DistFGetComponentName(PetscInt index,const char **name);
PetscErrorCode DistFGetComponentActivity(PetscInt index,PetscBool *solid,PetscBool *liquid);


#undef __FUNCT__
#define __FUNCT__ "DistFCreate"
PetscErrorCode DistFCreate(DistF *_ctx)
{
  DistF ctx;
  PetscBool found;
  PetscErrorCode ierr;
  
  char info[] = { "o--- A parameterized thermodynamic model \"K_linear_water_quadratic_pressure_1 (KLWQP1)\"---o\n"\
    "* Temperature (T) should be provided in the units of degrees Celcius [C].\n"\
    "* Pressure (P) should be the lithostatic pressure (positive) and should be provided in units of [GPa]\n"\
    "* In the event of an invalid T (T < 0), (P < 0) the computed porosity will be returned as NaN\n"\
    "* The parameterization is calibrated over: 0 < p (GPa) <= 20; C_H20 <= 6e-3; C_rf >= 0.7\n"\
    "* If these bounds are exceeded, results might be non-physical\n"\
    "* By default, if the calibration bounds are exceeded a warning will be report. \n"\
    "* The warnings can be disabled with the command line option -distf_disable_check_bounds \n"};

  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_DistF));CHKERRQ(ierr);
  ierr = PetscSNPrintf(ctx->info,PETSC_MAX_PATH_LEN-1,"%s",info);CHKERRQ(ierr);
  ctx->check_bounds = PETSC_TRUE;
  ierr = PetscOptionsGetBool(NULL,NULL,"-distf_disable_check_bounds",&ctx->check_bounds,&found);CHKERRQ(ierr);
  if (found) { ctx->check_bounds = PETSC_FALSE; }
  ctx->p_min = 0.0;
  ctx->p_max = 20.0;
  ctx->Crf_min = 0.7;
  ctx->Crf_max = PETSC_MAX_REAL;
  ctx->CH2O_min = 0.0;
  ctx->CH2O_max = 6.0e-3;
  
  ctx->evaluate = DistFEvaluate;
  ctx->get_num_components = DistFGetNumComponents;
  ctx->get_component_name = DistFGetComponentName;
  ctx->get_component_phase_activity = DistFGetComponentActivity;
  
  *_ctx = ctx;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFDestroy"
PetscErrorCode DistFDestroy(DistF *_ctx)
{
  DistF ctx;
  PetscErrorCode ierr;
 
  if (!_ctx) PetscFunctionReturn(0);
  ctx = *_ctx;
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  *_ctx = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFInit"
PetscErrorCode DistFInit(DistF ctx)
{
  ctx->a00 = -6.060705 * 1.0e-3;
  ctx->a10 = -6.509998 * 1.0e-5;
  ctx->a01 =  2.768520 * 1.0e-2;
  ctx->a20 = -3.762214 * 1.0e-5;
  ctx->a11 =  1.301919 * 1.0e-1;
  
  ctx->b00 =  4.664752 * 1.0;
  ctx->b10 =  8.789233 * 1.0e-1;
  ctx->b01 = -1.305064 * 1.0e2 ;
  ctx->b20 =  3.208283 * 1.0e-2;
  ctx->b11 = -2.313619 * 1.0e2 ;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFView"
PetscErrorCode DistFView(DistF ctx)
{
  PetscPrintf(PETSC_COMM_SELF,"[DistF info]:\n");
  PetscPrintf(PETSC_COMM_SELF,"%s",ctx->info);
  PetscPrintf(PETSC_COMM_SELF,"[DistF paramaters]:\n");
  PetscPrintf(PETSC_COMM_SELF,"  a00 %+1.4e \n",ctx->a00);
  PetscPrintf(PETSC_COMM_SELF,"  a10 %+1.4e \n",ctx->a10);
  PetscPrintf(PETSC_COMM_SELF,"  a01 %+1.4e \n",ctx->a01);
  PetscPrintf(PETSC_COMM_SELF,"  a20 %+1.4e \n",ctx->a20);
  PetscPrintf(PETSC_COMM_SELF,"  a11 %+1.4e \n",ctx->a11);

  PetscPrintf(PETSC_COMM_SELF,"  b00 %+1.4e \n",ctx->b00);
  PetscPrintf(PETSC_COMM_SELF,"  b10 %+1.4e \n",ctx->b10);
  PetscPrintf(PETSC_COMM_SELF,"  b01 %+1.4e \n",ctx->b01);
  PetscPrintf(PETSC_COMM_SELF,"  b20 %+1.4e \n",ctx->b20);
  PetscPrintf(PETSC_COMM_SELF,"  b11 %+1.4e \n",ctx->b11);
  PetscPrintf(PETSC_COMM_SELF,"[DistF calibration bounds]:\n");
  PetscPrintf(PETSC_COMM_SELF,"  p_min %+1.4e \n",ctx->p_min);
  PetscPrintf(PETSC_COMM_SELF,"  p_max %+1.4e \n",ctx->p_max);
  PetscPrintf(PETSC_COMM_SELF,"  Crf_min %+1.4e \n",ctx->Crf_min);
  PetscPrintf(PETSC_COMM_SELF,"  Crf_max %+1.4e \n",ctx->Crf_max);
  PetscPrintf(PETSC_COMM_SELF,"  CH2O_min %+1.4e \n",ctx->CH2O_min);
  PetscPrintf(PETSC_COMM_SELF,"  CH2O_max %+1.4e \n",ctx->CH2O_max);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFComputeKft"
PetscErrorCode DistFComputeKft(DistF ctx,PetscReal T,PetscReal P,PetscReal C_H2O,PetscReal *Kft)
{
  PetscReal a,b;
  
  a = ctx->a00 + ctx->a10*P + ctx->a01*C_H2O + ctx->a20*P*P + ctx->a11*P*C_H2O;
  
  b = ctx->b00 + ctx->b10*P + ctx->b01*C_H2O + ctx->b20*P*P + ctx->b11*P*C_H2O;
  *Kft = PetscExpReal(T * a + b);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFComputePorosity"
PetscErrorCode DistFComputePorosity(DistF ctx,PetscReal T,PetscReal P,PetscReal C_H2O,PetscReal C_rf,PetscReal *phi,PetscBool *isvalid)
{
  PetscReal A,B,C,Kft;
  PetscErrorCode ierr;
  
  *isvalid = PETSC_TRUE;
  if (C_H2O < 0.0) {
#ifdef DISTF_LOG_PARANOID
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** C_H2O must be positive. Found value C_H2O = %1.4e \n",C_H2O);
#endif
    *isvalid = PETSC_FALSE;
    *phi = (PetscReal)nan(tagp);
    PetscFunctionReturn(0);
  }
  if (C_rf < 0.0) {
#ifdef DISTF_LOG_PARANOID
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** C_rf must be positive. Found value C_rf = %1.4e \n",C_rf);
#endif
    *isvalid = PETSC_FALSE;
    *phi = (PetscReal)nan(tagp);
    PetscFunctionReturn(0);
  }
  
  if (T < 0.0) {
#ifdef DISTF_LOG_PARANOID
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** T must be positive. Found value T = %1.4e [C]\n",T);
#endif
    *isvalid = PETSC_FALSE;
    *phi = (PetscReal)nan(tagp);
    PetscFunctionReturn(0);
  }
  if (P < 0.0) {
#ifdef DISTF_LOG_PARANOID
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** P must be positive. Found value P = %1.4e [GPa]\n",P);
#endif
    *isvalid = PETSC_FALSE;
    *phi = (PetscReal)nan(tagp);
    PetscFunctionReturn(0);
  }
  if (ctx->check_bounds) {
    if (P > ctx->p_max)        PetscPrintf(PETSC_COMM_SELF,"**** [DistF calibration warning] **** Recommended P <= %1.4e [GPa] : Found %1.4e\n",ctx->p_max,P);
    if (C_rf < ctx->Crf_min)   PetscPrintf(PETSC_COMM_SELF,"**** [DistF calibration warning] **** Recommended C_rf >= %1.4e : Found %1.4e\n",ctx->Crf_min,C_rf);
    if (C_H2O > ctx->CH2O_max) PetscPrintf(PETSC_COMM_SELF,"**** [DistF calibration warning] **** Recommended C_H2O <= %1.4e : Found %1.4e\n",ctx->CH2O_max,C_H2O);

    if (P < ctx->p_min)        PetscPrintf(PETSC_COMM_SELF,"**** [DistF calibration warning] **** Recommended P >= %1.4e [GPa]: Found %1.4e\n",ctx->p_min,P);
    if (C_rf > ctx->Crf_max)   PetscPrintf(PETSC_COMM_SELF,"**** [DistF calibration warning] **** Recommended C_rf <= %1.4e : Found %1.4e\n",ctx->Crf_max,C_rf);
    if (C_H2O < ctx->CH2O_min) PetscPrintf(PETSC_COMM_SELF,"**** [DistF calibration warning] **** Recommended C_H2O >= %1.4e : Found %1.4e\n",ctx->CH2O_min,C_H2O);
  }
  
  ierr = DistFComputeKft(ctx,T,P,C_H2O,&Kft);CHKERRQ(ierr);
  
  /* solve quadratic equation */
  A = 1.0 - Kft;
  B = Kft * (1.0 + C_H2O) - 1.0 + C_rf;
  C = -Kft * C_H2O;
  
  if (fabs(A) < 1.0e-14) { /* assume linear form */
    *phi = C_H2O / (C_H2O + C_rf);
  } else {
    PetscReal phi_p,phi_m,disc,s_disc;
    
    disc = B*B - 4.0*A*C;
    if (disc < 0.0) {
#ifdef DISTF_LOG_PARANOID
      PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** A complex valued porosity is predicted.\n");
      PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** Discriment = %1.4e\n",disc);
      PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** State: T = %1.4e, P = %1.4e, C_H2O = %1.4e, C_rf = %1.4e, Kft = %1.4e\n",T,P,C_H2O,C_rf,Kft);
#endif
      *isvalid = PETSC_FALSE;
      *phi = (PetscReal)nan(tagp);
      PetscFunctionReturn(0);
    }
    
    s_disc = PetscSqrtReal(disc);
    
    phi_p = (-B + s_disc)/(2.0 * A);
    *phi = phi_p;
    if ( (phi_p < 0.0) || (phi_p > 1.0) ) {
      phi_m = (-B - s_disc)/(2.0 * A);
      *phi = phi_m;
    }
  }
  
  if ( (*phi < 0.0) || (*phi > 1.0) ) {
#ifdef DISTF_LOG_PARANOID
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** An invalid porosity has been computed.\n");
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** Porosity must be >= 0 and <= 1. Found value phi = %1.4e\n",*phi);
    PetscPrintf(PETSC_COMM_SELF,"**** [DistF Error] **** State: T = %1.4e, P = %1.4e, C_H2O = %1.4e, C_rf = %1.4e, Kft = %1.4e\n",T,P,C_H2O,C_rf,Kft);
#endif
    *isvalid = PETSC_FALSE;
    //*phi = (PetscReal)nan(tagp);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFSetCheckBounds"
PetscErrorCode DistFSetCheckBounds(DistF ctx,PetscBool value)
{
  ctx->check_bounds = value;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFGetCheckBounds"
PetscErrorCode DistFGetCheckBounds(DistF ctx,PetscBool *value)
{
  if (value) { *value = ctx->check_bounds; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFEvaluate"
PetscErrorCode DistFEvaluate(const PetscReal P[], const PetscReal T[],
                             const PetscInt ncomponents,
                             const PetscReal C_bulk[],
                             PetscReal C_liquid[],PetscReal C_solid[],
                             PetscReal *phi,
                             PetscBool *valid,
                             void *ctx)
{
  DistF          df = (DistF)ctx;
  PetscReal      c_bulk_H2O,c_bulk_RF;
  PetscErrorCode ierr;
  
  c_bulk_H2O = C_bulk[0];
  c_bulk_RF  = C_bulk[1];
  ierr = DistFComputePorosity(df,T[0],P[0],c_bulk_H2O,c_bulk_RF,phi,valid);CHKERRQ(ierr);
  
  C_liquid[0] = c_bulk_H2O;
  if (*phi > 1.0e-24) {
    C_liquid[0] = C_liquid[0] / (*phi);
  }
  C_solid[0]  = 0.0;

  C_liquid[1] = 0.0;
  C_solid[1]  = c_bulk_RF;
  if ((1.0 - *phi) > 1.0e-24) {
    C_solid[1]  = C_solid[1] /(1.0 - *phi);
  }

  C_liquid[2] = 1.0 - C_liquid[0] - C_liquid[1];
  C_solid[2]  = 1.0 - C_solid[0]  - C_solid[1];
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFGetNumComponents"
PetscErrorCode DistFGetNumComponents(PetscInt *nc)
{
  *nc = 3;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFGetComponentName"
PetscErrorCode DistFGetComponentName(PetscInt index,const char **name)
{
  switch (index) {
    case 0:
      *name = distf_component_names[index];
      break;
    case 1:
      *name = distf_component_names[index];
      break;
    case 2:
      *name = distf_component_names[index];
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"DistF defines 3 components. Index %D is not a valid component index",index);
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DistFGetComponentActivity"
PetscErrorCode DistFGetComponentActivity(PetscInt index,PetscBool *solid,PetscBool *liquid)
{
  switch (index) {
    case 0:
      *solid  = PETSC_FALSE;
      *liquid = PETSC_TRUE;
      break;
    case 1:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_FALSE;
      break;
    case 2:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"DistF defines 3 components. Index %D is not a valid component index",index);
      break;
  }
  PetscFunctionReturn(0);
}


#if 0
int main(void)
{
  DistF df;
  PetscReal T,P,phi,C_H2O,C_rf;
  PetscInt i,j;
  PetscErrorCode ierr;
  
  ierr = DistFCreate(&df);CHKERRQ(ierr);
  ierr = DistFInit(df);CHKERRQ(ierr);
  //printf("%s",tdp->notes);
  
  /*
   T = 800.0;
   P = 1.0;
   C_H2O = 1.0e-1;
   C_rf = 0.8;
   DistFComputePorosity(df,T,P,C_H2O,C_rf,&phi);
   PetscPrintf(PETSC_COMM_SELF,"T %1.2e [K], P %1.2e [GPa],C(%1.2e,%1.2e) -> log10(phi) %1.4e\n",T,P,C_H2O,C_rf,log10(phi));
   */
  
  C_rf = 0.8;
  P = 6.0;
  //
  for (i=0; i<1000; i++) {
    for (j=0; j<1000; j++) {
      T = (2000.0/999.0)*i;
      C_H2O = PetscPowReal( 10.0, (4.9/999.0) * j -5.0 );
      ierr = DistFComputePorosity(df,T,P,C_H2O,C_rf,&phi);CHKERRQ(ierr);
      PetscPrintf(PETSC_COMM_SELF,"%1.4e %1.4e %1.4e\n",T,PetscLog10Real(C_H2O),PetscLog10Real(phi));
    }PetscPrintf(PETSC_COMM_SELF,"\n");
  }
  //
  return(0);
}
#endif
