
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <pde.h>
#include <pdestokes.h>
#include <pdestokesdarcy2field.h>
#include <quadrature.h>
#include <coefficient.h>
#include <fe_geometry_utils.h>
#include <tpctx.h>
#include <darcyctx.h>
#include <mec.h>
#include <pointwise_physics.h>
#include <pointwise_mech_coeff.h>

PetscErrorCode coeff_pointwise_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);

#undef __FUNCT__
#define __FUNCT__ "rhs_fp_QuadratureEvaluator"
PetscErrorCode rhs_fp_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  PetscReal val,*coeff;
  
  PetscFunctionBegin;
  ierr = QuadratureGetRule(quadrature,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fp",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  val = 0.0;
  ierr = QuadratureSetValues(quadrature,1,&val,coeff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "rhs_fv_QuadratureEvaluator"
PetscErrorCode rhs_fv_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  TPCtx ctx;
  DM dm;
  PetscInt i,q,e;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,*coeff_phi,delta_rho;
  PetscInt nqp;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  ctx = (TPCtx)data;
  dm = ctx->mechanics->dm_v_hat;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"fu",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  
  {
    PDE pde;
    Coefficient coeff_bform;
    Quadrature quadrature_bform;
    
    ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
    ierr = PDEStokesDarcy2FieldGetCoefficients(pde,&coeff_bform,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = CoefficientGetQuadrature(coeff_bform,&quadrature_bform);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature_bform,"phi",NULL,NULL,NULL,&coeff_phi);CHKERRQ(ierr);
  }
  
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  
  ierr = PetscMalloc1(2*nbasis,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  
  delta_rho = ctx->rho_s - ctx->rho_l;
  
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates and solution */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal xq,yq;
      
      /* get x,y coords at each quadrature point */
      xq = yq = 0.0;
      for (i=0; i<nbasis; i++) {
        xq = xq + tab_N[q][i] * elcoords[2*i+0];
        yq = yq + tab_N[q][i] * elcoords[2*i+1];
      }
      
      //coeff[2*(e*nqp + q)+0] = ctx->phi * delta_rho * ctx->g[0];
      //coeff[2*(e*nqp + q)+1] = ctx->phi * delta_rho * ctx->g[1];
      
      //if (e*nqp+q == 1000) printf("phi[1000] %1.10e\n",coeff_phi[e*nqp+q]);

      /* coefficient is (\phi * \Delta\rho * g) */
      coeff[2*(e*nqp + q)+0] = coeff_phi[e*nqp+q] * delta_rho * ctx->g[0];
      coeff[2*(e*nqp + q)+1] = coeff_phi[e*nqp+q] * delta_rho * ctx->g[1];
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* Define coefficients for the two-phase flow problem
 - note that kappa*, zeta*, e3 are contained "inside" the variable coeff
*/
#undef __FUNCT__
#define __FUNCT__ "MechanicsSetCoefficientEvaluators"
PetscErrorCode MechanicsSetCoefficientEvaluators(MCtx ctx,TPCtx tp)
{
  Coefficient coeff,rhs_fu,rhs_fp;
  PDE pde;
  PetscBool nonzero_bulk_momentum_bodyforce = PETSC_FALSE;
  PetscErrorCode ierr;
  
  pde = ctx->pde;
  ierr = PDEStokesDarcy2FieldGetCoefficients(pde,&coeff,&rhs_fu,&rhs_fp,NULL);CHKERRQ(ierr);
  ierr = CoefficientSetComputeQuadrature(coeff,   coeff_pointwise_QuadratureEvaluator,NULL,(void*)tp);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,NULL,"-nonzero_bulk_momentum_bodyforce",&nonzero_bulk_momentum_bodyforce,NULL);CHKERRQ(ierr);
  if (nonzero_bulk_momentum_bodyforce) {
    ierr = CoefficientSetComputeQuadrature(rhs_fu, rhs_fv_QuadratureEvaluator,NULL,(void*)tp);CHKERRQ(ierr);
  } else {
    ierr = CoefficientSetComputeQuadratureEmpty(rhs_fu);CHKERRQ(ierr);
  }
  
  ierr = CoefficientSetComputeQuadrature(rhs_fp, rhs_fp_QuadratureEvaluator,NULL,(void*)tp);CHKERRQ(ierr);
  
  ierr = CoefficientEvaluate(coeff);CHKERRQ(ierr);
  /*
  {
    char prefix[PETSC_MAX_PATH_LEN];
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/mechanics",tp->output_path);CHKERRQ(ierr);
    ierr = CoefficientView_GP(coeff,ctx->dm_p_hat,prefix);CHKERRQ(ierr);
  }
  */
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerCreate"
PetscErrorCode MechanicsCoeffContainerCreate(MechanicsCoeffContainer *c)
{
  PetscErrorCode ierr;
  MechanicsCoeffContainer container;
  
  ierr = PetscMalloc1(1,&container);CHKERRQ(ierr);
  ierr = PetscMemzero(container,sizeof(struct _p_MechanicsCoeffContainer));CHKERRQ(ierr);
  container->requires_coordinates = PETSC_FALSE;
  container->requires_pressure = PETSC_FALSE;
  container->requires_temperature = PETSC_FALSE;
  container->requires_porosity = PETSC_FALSE;
  container->requires_components = PETSC_FALSE;
  container->requires_strain_rate_inv = PETSC_FALSE;
  container->debug = PETSC_FALSE;
  ierr = PetscOptionsGetBool(NULL,NULL,"-point_wise_evaluator_debug",&container->debug,NULL);CHKERRQ(ierr);
  *c = container;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerDestroy"
PetscErrorCode MechanicsCoeffContainerDestroy(MechanicsCoeffContainer *c)
{
  PetscErrorCode ierr;
  MechanicsCoeffContainer container;
  
  if (!c) PetscFunctionReturn(0);
  container = *c;
  
  ierr = PWPhysicsDestroy(&container->pwp_eta);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&container->pwp_eta_limiter);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&container->pwp_zeta);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&container->pwp_zeta_limiter);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&container->pwp_k_on_mu);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&container->pwp_k_on_mu_limiter);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&container->pwp_rho);CHKERRQ(ierr);
  
  ierr = EContainerDestroy(&container->space_v);CHKERRQ(ierr);
  ierr = EContainerDestroy(&container->space_p);CHKERRQ(ierr);
  ierr = EContainerDestroy(&container->space_T);CHKERRQ(ierr);

  ierr = PetscFree(container->cell_qp_coor);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_p);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_T);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_Crf);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_CH2O);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_e2);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_phi);CHKERRQ(ierr);

  ierr = PetscFree(container->cell_qp_coor_si);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_p_si);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_T_si);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_Crf_si);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_CH2O_si);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_e2_si);CHKERRQ(ierr);
  ierr = PetscFree(container->cell_qp_phi_si);CHKERRQ(ierr);

  ierr = PetscFree(container);CHKERRQ(ierr);
  *c = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerSetup"
PetscErrorCode MechanicsCoeffContainerSetup(MechanicsCoeffContainer c,MCtx mctx)
{
  PetscErrorCode ierr;
  DM dmu,dmp,dmt;
  Coefficient coeff_bform;
  Quadrature quadrature_bform;
  PetscInt nqp;
  
  dmu = mctx->dm_v_hat;
  dmp = mctx->dm_p_hat;
  dmt = mctx->dm_v_hat;
  
  ierr = PDEStokesDarcy2FieldGetCoefficients(mctx->pde,&coeff_bform,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(coeff_bform,&quadrature_bform);CHKERRQ(ierr);
  ierr = QuadratureGetRule(quadrature_bform,&nqp,NULL,NULL);CHKERRQ(ierr);
  
  /* create spaces for different fields */
  ierr = EContainerCreate(dmu,quadrature_bform,&c->space_v);CHKERRQ(ierr);
  ierr = EContainerCreate(dmp,quadrature_bform,&c->space_p);CHKERRQ(ierr);
  ierr = EContainerCreate(dmt,quadrature_bform,&c->space_T);CHKERRQ(ierr);
  
  /* allocate buffers (even if some are not required) */
  ierr = PetscMalloc1(2*nqp,&c->cell_qp_coor);CHKERRQ(ierr);  ierr = PetscMemzero(c->cell_qp_coor,2*nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_p);CHKERRQ(ierr);       ierr = PetscMemzero(c->cell_qp_p,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_T);CHKERRQ(ierr);       ierr = PetscMemzero(c->cell_qp_T,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_Crf);CHKERRQ(ierr);     ierr = PetscMemzero(c->cell_qp_Crf,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_CH2O);CHKERRQ(ierr);    ierr = PetscMemzero(c->cell_qp_CH2O,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_e2);CHKERRQ(ierr);      ierr = PetscMemzero(c->cell_qp_e2,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_phi);CHKERRQ(ierr);     ierr = PetscMemzero(c->cell_qp_phi,nqp*sizeof(PetscReal));CHKERRQ(ierr);

  ierr = PetscMalloc1(2*nqp,&c->cell_qp_coor_si);CHKERRQ(ierr);  ierr = PetscMemzero(c->cell_qp_coor_si,2*nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_p_si);CHKERRQ(ierr);       ierr = PetscMemzero(c->cell_qp_p_si,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_T_si);CHKERRQ(ierr);       ierr = PetscMemzero(c->cell_qp_T_si,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_Crf_si);CHKERRQ(ierr);     ierr = PetscMemzero(c->cell_qp_Crf_si,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_CH2O_si);CHKERRQ(ierr);    ierr = PetscMemzero(c->cell_qp_CH2O_si,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_e2_si);CHKERRQ(ierr);      ierr = PetscMemzero(c->cell_qp_e2_si,nqp*sizeof(PetscReal));CHKERRQ(ierr);
  ierr = PetscMalloc1(nqp,&c->cell_qp_phi_si);CHKERRQ(ierr);     ierr = PetscMemzero(c->cell_qp_phi_si,nqp*sizeof(PetscReal));CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/*
 This function can be called multiple times to enable the evaluator to be altered.
 To faciliate multiple calls, we cannot parse the evaluator type from the command line.
*/
#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerSetupEta"
PetscErrorCode MechanicsCoeffContainerSetupEta(MechanicsCoeffContainer c,const char typename[],const char limiter_typename[])
{
  PetscBool issame,found;
  PetscBool use_si;
  const PetscReal *buffer;
  PetscErrorCode ierr;
  
  /* if evaluator exists - destroy it now */
  if (typename) {
    
    if (c->pwp_eta) {
      ierr = PWPhysicsDestroy(&c->pwp_eta);CHKERRQ(ierr);
    }

    found = PETSC_FALSE;
    
    ierr = PetscStrcmp(typename,"eta.Constant",&issame);CHKERRQ(ierr);
    if (issame) { ierr = EtaConstantCreate(PETSC_TRUE,0,"wedge",&c->pwp_eta);CHKERRQ(ierr); found = PETSC_TRUE; }

    ierr = PetscStrcmp(typename,"eta.DiffusionCreep",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = EtaDiffusionCreepCreate(PETSC_TRUE,0,"wedge",&c->pwp_eta);CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);

      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }

    ierr = PetscStrcmp(typename,"eta.DiffusionCreep.PvKPEPI2008",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = EtaDiffusionCreepCreate_PvKPEPI2008(0,&c->pwp_eta);CHKERRQ(ierr);
      ierr = PWPhysicsSetName(c->pwp_eta,"wedge_olivine");CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);

      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }

    ierr = PetscStrcmp(typename,"eta.DislocationCreep",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = EtaDislocationCreepCreate(PETSC_TRUE,0,"wedge",&c->pwp_eta);CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);

      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      
      c->requires_strain_rate_inv = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_SRATE_INV,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,1,(PetscReal*)buffer);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    ierr = PetscStrcmp(typename,"eta.DislocationCreep.PvKPEPI2008",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = EtaDislocationCreepCreate_PvKPEPI2008(0,&c->pwp_eta);CHKERRQ(ierr);
      ierr = PWPhysicsSetName(c->pwp_eta,"wedge_olivine");CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);

      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      
      c->requires_strain_rate_inv = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_SRATE_INV,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,1,(PetscReal*)buffer);CHKERRQ(ierr);

      found = PETSC_TRUE;
    }
    
    ierr = PetscStrcmp(typename,"eta.CompositeDiffDisl.PvKPEPI2008",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = EtaCompositeDiffDislCreepCreate_PvKPEPI2008(0,&c->pwp_eta);CHKERRQ(ierr);
      ierr = PWPhysicsSetName(c->pwp_eta,"wedge_olivine");CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);
      
      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      
      c->requires_strain_rate_inv = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_SRATE_INV,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_eta,1,(PetscReal*)buffer);CHKERRQ(ierr);
      
      found = PETSC_TRUE;
    }
    
    if (!found) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid pointwise evaluator for eta was not found");
  }
  
  if (limiter_typename) {
    if (c->pwp_eta_limiter) {
      ierr = PWPhysicsDestroy(&c->pwp_eta_limiter);CHKERRQ(ierr);
    }
    
    found = PETSC_FALSE;
    ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);

    ierr = PetscStrcmp(limiter_typename,"none",&issame);CHKERRQ(ierr);
    if (issame) {
      found = PETSC_TRUE;
    }
    
    ierr = PetscStrcmp(limiter_typename,"limiter.MinMax",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = ShearViscosityLimiterMinMaxCreate(use_si,0,&c->pwp_eta_limiter);CHKERRQ(ierr);
      ierr = PWPhysicsSetName(c->pwp_eta_limiter,"eta::wedge");CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    if (!found) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid limiter for eta was not found");
  }
  
  PetscFunctionReturn(0);
}

/*
 This function can be called multiple times to enable the evaluator to be altered.
 To faciliate multiple calls, we cannot parse the evaluator type from the command line.
 */
#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerSetupZeta"
PetscErrorCode MechanicsCoeffContainerSetupZeta(MechanicsCoeffContainer c,const char typename[],const char limiter_typename[])
{
  PetscBool issame,found;
  PetscBool use_si;
  const PetscReal *buffer;
  PetscErrorCode ierr;
  
  /* if evaluator exists - destroy it now */
  if (typename) {
    
    if (c->pwp_zeta) {
      ierr = PWPhysicsDestroy(&c->pwp_zeta);CHKERRQ(ierr);
    }
    
    found = PETSC_FALSE;
    
    ierr = PetscStrcmp(typename,"zeta.Constant",&issame);CHKERRQ(ierr);
    if (issame) { ierr = ZetaConstantCreate(PETSC_TRUE,0,"wedge",&c->pwp_zeta);CHKERRQ(ierr); found = PETSC_TRUE; }
    
    ierr = PetscStrcmp(typename,"zeta.Ratio",&issame);CHKERRQ(ierr);
    if (issame) {
      /* Note - 
       We need to force zeta.ratio to inherit use_si from shear viscosity flow law now,
       otherwise, we require seperate calls to MechanicsCoeffContainerSetupZeta() as currently zeta.ratio
       only enforces use_si when ZetaRatioSetParams() is called.
       
       The alternative calling pattern would look like this:
         MechanicsCoeffContainerSetupEta(c,"flowlaw.eta","flowlaw.eta.limiter");
         MechanicsCoeffContainerSetupZeta(c,"zeta.Ratio",NULL);
         ZetaRatioSetParams(c,factor,"flowlaw.eta");
         MechanicsCoeffContainerSetupZeta(c,NULL,"flowlaw.zeta.limiter");
       
       Probably the better long-term solution is to split this function into two,
       one function for setting flowlaw.zeta and another separate function for setting flowlaw.zeta.limiter
      */
      ierr = PWPhysicsGetUseSI(c->pwp_eta,&use_si);CHKERRQ(ierr);
      ierr = ZetaRatioCreate(use_si,0,"wedge",&c->pwp_zeta);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    ierr = PetscStrcmp(typename,"zeta.DiffusionCreep",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = ZetaDiffusionCreepCreate(PETSC_TRUE,0,"wedge",&c->pwp_zeta);CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_zeta,&use_si);CHKERRQ(ierr);
      
      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_zeta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }

    ierr = PetscStrcmp(typename,"zeta.DislocationCreep",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = ZetaDislocationCreepCreate(PETSC_TRUE,0,"wedge",&c->pwp_zeta);CHKERRQ(ierr);
      ierr = PWPhysicsGetUseSI(c->pwp_zeta,&use_si);CHKERRQ(ierr);
      
      c->requires_temperature = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_TEMPERATURE,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_zeta,0,(PetscReal*)buffer);CHKERRQ(ierr);
      
      c->requires_strain_rate_inv = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_SRATE_INV,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_zeta,1,(PetscReal*)buffer);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    if (!found) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid pointwise evaluator for zeta was not found");
  }
  
  if (limiter_typename) {
    if (c->pwp_eta_limiter) {
      ierr = PWPhysicsDestroy(&c->pwp_zeta_limiter);CHKERRQ(ierr);
    }
    
    found = PETSC_FALSE;
    ierr = PWPhysicsGetUseSI(c->pwp_zeta,&use_si);CHKERRQ(ierr);
    
    ierr = PetscStrcmp(limiter_typename,"none",&issame);CHKERRQ(ierr);
    if (issame) {
      found = PETSC_TRUE;
    }

    ierr = PetscStrcmp(limiter_typename,"limiter.MinMax",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = BulkViscosityLimiterMinMaxCreate(use_si,0,&c->pwp_zeta_limiter);CHKERRQ(ierr);
      ierr = PWPhysicsSetName(c->pwp_zeta_limiter,"zeta::wedge");CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    if (!found) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid limiter for zeta was not found");
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerSetupEffectivePermability"
PetscErrorCode MechanicsCoeffContainerSetupEffectivePermability(MechanicsCoeffContainer c,const char default_typename[],const char limiter_typename[])
{
  PetscBool issame,found = PETSC_FALSE;
  PetscBool use_si;
  const PetscReal *buffer;
  PetscErrorCode ierr;
  
  /* if evaluator exists - destroy it now */
  if (c->pwp_k_on_mu) {
    ierr = PWPhysicsDestroy(&c->pwp_k_on_mu);CHKERRQ(ierr);
  }
  
  ierr = PetscStrcmp(default_typename,"k.Constant",&issame);CHKERRQ(ierr);
  if (issame) { ierr = kConstantCreate(PETSC_TRUE,0,"wedge",&c->pwp_k_on_mu);CHKERRQ(ierr); found = PETSC_TRUE; }
  
  ierr = PetscStrcmp(default_typename,"k.PorosityDependent",&issame);CHKERRQ(ierr);
  if (issame) {
    ierr = kPorosityDependentCreate(PETSC_TRUE,0,"wedge",&c->pwp_k_on_mu);CHKERRQ(ierr);
    ierr = PWPhysicsGetUseSI(c->pwp_k_on_mu,&use_si);CHKERRQ(ierr);

    c->requires_porosity = PETSC_TRUE;
    ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_POROSITY,use_si,&buffer);CHKERRQ(ierr);
    ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_k_on_mu,0,(PetscReal*)buffer);CHKERRQ(ierr);
    found = PETSC_TRUE;
  }
  
  if (!found) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid pointwise evaluator for k/mu was not found");
  
  if (limiter_typename) {
    if (c->pwp_k_on_mu_limiter) {
      ierr = PWPhysicsDestroy(&c->pwp_k_on_mu_limiter);CHKERRQ(ierr);
    }
    
    found = PETSC_FALSE;
    ierr = PWPhysicsGetUseSI(c->pwp_k_on_mu,&use_si);CHKERRQ(ierr);
    
    ierr = PetscStrcmp(limiter_typename,"none",&issame);CHKERRQ(ierr);
    if (issame) {
      found = PETSC_TRUE;
    }
    
    ierr = PetscStrcmp(limiter_typename,"limiter.MinMax",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = LimiterMinMaxCreate(use_si,0,"k_on_mu::wedge",&c->pwp_k_on_mu_limiter);CHKERRQ(ierr);
      ierr = LimiterMinMaxSetParams(c->pwp_k_on_mu_limiter,"EffectivePermeability",0.0,PETSC_MAX_REAL);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    ierr = PetscStrcmp(limiter_typename,"limiter.SpatialX",&issame);CHKERRQ(ierr);
    if (issame) {
      ierr = EffectivePermeabilityLimiterSpatialXCreate(use_si,0,&c->pwp_k_on_mu_limiter);CHKERRQ(ierr);
      ierr = PWPhysicsSetName(c->pwp_k_on_mu_limiter,"k_on_mu::wedge");CHKERRQ(ierr);
      c->requires_coordinates = PETSC_TRUE;
      ierr = MechanicsCoeffContainerGetValidCellBuffer(c,MCBUFFER_COOR,use_si,&buffer);CHKERRQ(ierr);
      ierr = PWPhysicsSetAuxiliaryDataBuffer(c->pwp_k_on_mu_limiter,0,(PetscReal*)buffer);CHKERRQ(ierr);
      found = PETSC_TRUE;
    }
    
    if (!found) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"A valid limiter for k/mu was not found");
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pointwise_QuadratureEvaluator"
PetscErrorCode pointwise_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],TPCtx ctx)
{
  PetscErrorCode ierr;
  DM dmv,dmp,dmt;
  PetscInt i,q,e,nel,nqp;
  PetscReal *coords_v,*coords_p,*coords_t;
  PetscReal *elcoords_v,*el_u,*el_v,*el_p,*el_t;
  PetscInt *element_v,npe_v,nbasis_v;
  PetscInt *element_p,npe_p,nbasis_p;
  PetscInt *element_t,npe_t,nbasis_t;
  PetscReal *qp_coeff_eta,*qp_coeff_kstar,*qp_coeff_phi,*qp_coeff_zeta;
  EContainer space_v,space_p,space_t;
  MechanicsCoeffContainer mcoeff;
  Vec T_hat,T_hat_local,X_hat,V_hat_local,P_hat_local;
  const PetscScalar *_T_hat,*_V_hat,*_P_hat;
  PetscBool requires_si = PETSC_FALSE;
  
  PetscFunctionBegin;
  dmv = ctx->mechanics->dm_v_hat;
  dmp = ctx->mechanics->dm_p_hat;
  dmt = ctx->mechanics->dm_ve_hat;
  
  mcoeff = ctx->mechanics->coeff_container;
  
  space_v = mcoeff->space_v;
  space_p = mcoeff->space_p;
  space_t = mcoeff->space_T;
  
  T_hat = ctx->mechanics->T_hat;
  ierr = DMGetLocalVector(dmt,&T_hat_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmt,T_hat,INSERT_VALUES,T_hat_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmt,T_hat,INSERT_VALUES,T_hat_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(T_hat_local,&_T_hat);CHKERRQ(ierr);

  X_hat = ctx->mechanics->X_hat;
  ierr = DMCompositeGetLocalVectors(ctx->mechanics->dm_m,&V_hat_local,&P_hat_local);CHKERRQ(ierr);
  ierr = DMCompositeScatter(ctx->mechanics->dm_m,X_hat,V_hat_local,P_hat_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(V_hat_local,&_V_hat);CHKERRQ(ierr);
  ierr = VecGetArrayRead(P_hat_local,&_P_hat);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&qp_coeff_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&qp_coeff_kstar);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",NULL,NULL,NULL,&qp_coeff_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"zeta*",NULL,NULL,NULL,&qp_coeff_zeta);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmv,&nel,&nbasis_v,&element_v,&npe_v,NULL,&coords_v);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&nbasis_p,&element_p,&npe_p,NULL,&coords_p);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmt,&nel,&nbasis_t,&element_t,&npe_t,NULL,&coords_t);CHKERRQ(ierr);
  
  elcoords_v = space_v->buf_basis_2vector_a;
  el_u = space_v->buf_basis_scalar_a;
  el_v = space_v->buf_basis_scalar_b;
  el_p = space_p->buf_basis_scalar_a;
  el_t = space_t->buf_basis_scalar_a;
  
  /* check all evaluators if ANY of them need aux variables in SI units */
  if (mcoeff->pwp_eta->use_si) {     requires_si = PETSC_TRUE; }
  if (mcoeff->pwp_k_on_mu->use_si) { requires_si = PETSC_TRUE; }
  if (mcoeff->pwp_zeta->use_si) {    requires_si = PETSC_TRUE; }

  
  for (e=0; e<nel; e++) {
    PetscInt *el_bidx_v,*el_bidx_p,*el_bidx_t;
    
    /* get basis dofs */
    el_bidx_v = &element_v[nbasis_v*e];
    el_bidx_p = &element_p[nbasis_p*e];
    el_bidx_t = &element_t[nbasis_t*e];
    
    /* get element coordinates from velocity space (chosen as we may require these later for strain-rate) */
    for (i=0; i<nbasis_v; i++) {
      PetscInt bidx = el_bidx_v[i];
      
      elcoords_v[2*i+0] = coords_v[2*bidx+0];
      elcoords_v[2*i+1] = coords_v[2*bidx+1];
    }
    
    /* get coords at each quadrature point */
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis_v; i++) {
        x_qp[0] += space_v->N[q][i] * elcoords_v[2*i+0];
        x_qp[1] += space_v->N[q][i] * elcoords_v[2*i+1];
      }
      
      mcoeff->cell_qp_coor[2*q+0] = x_qp[0];
      mcoeff->cell_qp_coor[2*q+1] = x_qp[1];
    }
    
    /* get velocity then strain-rate */
    if (mcoeff->requires_strain_rate_inv && e == 0) { }
    for (i=0; i<nbasis_v; i++) {
      PetscInt bidx = el_bidx_v[i];
      
      el_u[i] = _V_hat[2*bidx+0];
      el_v[i] = _V_hat[2*bidx+1];
    }
    EvaluateBasisDerivatives(nqp,nbasis_v,elcoords_v,space_v->dNr1,space_v->dNr2,space_v->dNx1,space_v->dNx2);
    for (q=0; q<nqp; q++) {
      PetscReal eij[3],e2;
      
      eij[0] = eij[1] = eij[2] = 0.0;
      for (i=0; i<nbasis_v; i++) {
        eij[0] += space_v->dNx1[q][i] * el_u[i];
        eij[1] += space_v->dNx2[q][i] * el_v[i];
        eij[2] += 0.5 * (space_v->dNx1[q][i] * el_v[i] + space_v->dNx2[q][i] * el_u[i]);
      }
      e2 = PetscSqrtReal(0.5 * (eij[0]*eij[0] + eij[1]*eij[1] + 2.0*eij[2]*eij[2]) );
      
      mcoeff->cell_qp_e2[q] = e2;
    }
    
    /* get pressure */
    if (mcoeff->requires_pressure && e == 0) { }
    for (i=0; i<nbasis_p; i++) {
      PetscInt bidx = el_bidx_p[i];
      
      el_p[i] = _P_hat[bidx];
    }
    for (q=0; q<nqp; q++) {
      PetscReal p_qp;
      
      p_qp = 0.0;
      for (i=0; i<nbasis_p; i++) {
        p_qp += space_p->N[q][i] * el_p[i];
      }
      
      mcoeff->cell_qp_p[q] = p_qp;
    }

    /* get temperature */
    if (mcoeff->requires_temperature && e == 0) { }
    for (i=0; i<nbasis_t; i++) {
      PetscInt bidx = el_bidx_t[i];
      
      el_t[i] = _T_hat[bidx];
    }
    for (q=0; q<nqp; q++) {
      PetscReal T_qp;
      
      T_qp = 0.0;
      for (i=0; i<nbasis_t; i++) {
        T_qp += space_t->N[q][i] * el_t[i];
      }
      
      mcoeff->cell_qp_T[q] = T_qp;
    }

    /* get components */
    /* Use SL interpolation to get values of chemical components at quadrature points */
    if (mcoeff->requires_components) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requires components at quadrature points requires MC interpolant: TPMCInterpolateBulkConcentrationAtPoint(). TODO");
    }
    for (q=0; q<nqp; q++) {
      mcoeff->cell_qp_CH2O[q] = 0.0;
      mcoeff->cell_qp_Crf[q] = 0.0;
    }
    
    /* get porosity */
    if (mcoeff->requires_porosity && e == 0) { }
    for (q=0; q<nqp; q++) {
      mcoeff->cell_qp_phi[q] = qp_coeff_phi[e*nqp+q];
    }
    
    if (requires_si) {
      /* convert quantities */
      for (q=0; q<nqp; q++) {
        mcoeff->cell_qp_coor_si[2*q+0] = mcoeff->cell_qp_coor[2*q+0] * ctx->L;
        mcoeff->cell_qp_coor_si[2*q+1] = mcoeff->cell_qp_coor[2*q+1] * ctx->L;
        mcoeff->cell_qp_e2_si[q]       = mcoeff->cell_qp_e2[q]       / ctx->T; /* multiply for sec, divide for 1/sec */
        mcoeff->cell_qp_p_si[q]        = mcoeff->cell_qp_p[q]        * ctx->P;
        
        /* temperature is not non-dimensionalized */
        mcoeff->cell_qp_T_si[q]    = mcoeff->cell_qp_T[q];
        /* concentrations are not non-dimensionalized */
        mcoeff->cell_qp_Crf_si[q]  = mcoeff->cell_qp_Crf[q];
        mcoeff->cell_qp_CH2O_si[q] = mcoeff->cell_qp_CH2O[q];
        /* porosity is not non-dimensionalized */
        mcoeff->cell_qp_phi_si[q]  = mcoeff->cell_qp_phi[q];
      }
    }
    
    /* call pointwise evaluator */
    /* shear viscosity */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_eta,nqp,&qp_coeff_eta[e*nqp]);CHKERRQ(ierr);
    /* Apply limiter on shear viscosity */
    /* it is assumed that both pwp_eta and pwp_eta_limiter use the same value for SI or non-dim */
    if (mcoeff->pwp_eta_limiter) {
      if (mcoeff->pwp_eta->use_si != mcoeff->pwp_eta_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"shear viscosity evaluator and limiter must both use same setting for SI or non-dim");
      ierr = PWPhysicsEvaluate(mcoeff->pwp_eta_limiter,nqp,&qp_coeff_eta[e*nqp]);CHKERRQ(ierr);
    }

    /* permability */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_k_on_mu,nqp,&qp_coeff_kstar[e*nqp]);CHKERRQ(ierr);
    /* Apply limiter on the effective permeability */
    if (mcoeff->pwp_k_on_mu_limiter) {
      if (mcoeff->pwp_k_on_mu->use_si != mcoeff->pwp_k_on_mu_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"effective permeability evaluator and limiter must both use same setting for SI or non-dim");
      ierr = PWPhysicsEvaluate(mcoeff->pwp_k_on_mu_limiter,nqp,&qp_coeff_kstar[e*nqp]);CHKERRQ(ierr);
    }

    /* bulk-viscosity */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_zeta,nqp,&qp_coeff_zeta[e*nqp]);CHKERRQ(ierr);
    /* Apply limiter on the bulk viscosity */
    if (mcoeff->pwp_zeta_limiter) {
      if (mcoeff->pwp_zeta->use_si != mcoeff->pwp_zeta_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"bulk viscosity evaluator and limiter must both use same setting for SI or non-dim");
      ierr = PWPhysicsEvaluate(mcoeff->pwp_zeta_limiter,nqp,&qp_coeff_zeta[e*nqp]);CHKERRQ(ierr);
    }
  }

  /* Scale shear viscosity, bulk viscosity and permability if respective evaluators use SI */
  if (mcoeff->pwp_eta->use_si) {
    for (q=0; q<nel*nqp; q++) {
      /* compute viscosity is in SI units, non-dimensionalize now */
      qp_coeff_eta[q] /= ctx->E;
    }
  }

  if (mcoeff->pwp_zeta->use_si) {
    for (q=0; q<nel*nqp; q++) {
      /* compute viscosity is in SI units, non-dimensionalize now */
      qp_coeff_zeta[q] /= ctx->E;
    }
  }

  if (mcoeff->pwp_k_on_mu->use_si) {
    for (q=0; q<nel*nqp; q++) {
      /* compute permability is in SI units, non-dimensionalize now */
      qp_coeff_kstar[q] /= ctx->K;
    }
  }

  ierr = VecRestoreArrayRead(P_hat_local,&_P_hat);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(V_hat_local,&_V_hat);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(ctx->mechanics->dm_m,&V_hat_local,&P_hat_local);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T_hat_local,&_T_hat);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmt,&T_hat_local);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pointwise_QuadratureEvaluator_debug"
PetscErrorCode pointwise_QuadratureEvaluator_debug(Coefficient c,Quadrature quadrature,PetscInt eregion[],TPCtx ctx)
{
  PetscErrorCode ierr;
  DM dmv,dmp,dmt;
  PetscInt i,q,e,nel,nqp;
  PetscReal *coords_v,*coords_p,*coords_t;
  PetscReal *elcoords_v,*el_u,*el_v,*el_p,*el_t;
  PetscInt *element_v,npe_v,nbasis_v;
  PetscInt *element_p,npe_p,nbasis_p;
  PetscInt *element_t,npe_t,nbasis_t;
  PetscReal *qp_coeff_eta,*qp_coeff_kstar,*qp_coeff_phi,*qp_coeff_zeta;
  EContainer space_v,space_p,space_t;
  MechanicsCoeffContainer mcoeff;
  Vec T_hat,T_hat_local,X_hat,V_hat_local,P_hat_local;
  const PetscScalar *_T_hat,*_V_hat,*_P_hat;
  static PetscInt file_counter = 0;
  FILE *fp_eta = NULL;
  char filename_eta[PETSC_MAX_PATH_LEN];
  PetscBool requires_si = PETSC_FALSE;
  PetscInt index;
  
  PetscFunctionBegin;
  
  ierr = PetscSNPrintf(filename_eta,PETSC_MAX_PATH_LEN-1,"pwp-state-%D.gp",file_counter);CHKERRQ(ierr);
  fp_eta = fopen(filename_eta,"w");
  if (!fp_eta) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Failed to open file %s",filename_eta);
  
  
  dmv = ctx->mechanics->dm_v_hat;
  dmp = ctx->mechanics->dm_p_hat;
  dmt = ctx->mechanics->dm_ve_hat;
  
  mcoeff = ctx->mechanics->coeff_container;

  /* write header info */
  fprintf(fp_eta,"# =============================================\n");
  fprintf(fp_eta,"# Pointwise physics evaluator input/auxiallary/output data\n");

  fprintf(fp_eta,"# [pwp_eta]\n");
  fprintf(fp_eta,"#   type        %s \n",mcoeff->pwp_eta->type);
  fprintf(fp_eta,"#   name        %s \n",mcoeff->pwp_eta->name);
  fprintf(fp_eta,"#   region      %d \n",(int)mcoeff->pwp_eta->region);
  fprintf(fp_eta,"#   naux fields %d \n",(int)mcoeff->pwp_eta->naux);
  for (i=0; i<mcoeff->pwp_eta->naux; i++) {
    fprintf(fp_eta,"#     aux_field[%d]: name %s \n",i,mcoeff->pwp_eta->aux_data_names[i]);
  }
  
  fprintf(fp_eta,"# [pwp_k_on_mu]\n");
  fprintf(fp_eta,"#   type        %s \n",mcoeff->pwp_k_on_mu->type);
  fprintf(fp_eta,"#   name        %s \n",mcoeff->pwp_k_on_mu->name);
  fprintf(fp_eta,"#   region      %d \n",(int)mcoeff->pwp_k_on_mu->region);
  fprintf(fp_eta,"#   naux fields %d \n",(int)mcoeff->pwp_k_on_mu->naux);
  for (i=0; i<mcoeff->pwp_k_on_mu->naux; i++) {
    fprintf(fp_eta,"#     aux_field[%d]: name %s \n",i,mcoeff->pwp_k_on_mu->aux_data_names[i]);
  }
  
  fprintf(fp_eta,"# [pwp_zeta]\n");
  fprintf(fp_eta,"#   type        %s \n",mcoeff->pwp_zeta->type);
  fprintf(fp_eta,"#   name        %s \n",mcoeff->pwp_zeta->name);
  fprintf(fp_eta,"#   region      %d \n",(int)mcoeff->pwp_zeta->region);
  fprintf(fp_eta,"#   naux fields %d \n",(int)mcoeff->pwp_zeta->naux);
  for (i=0; i<mcoeff->pwp_zeta->naux; i++) {
    fprintf(fp_eta,"#     aux_field[%d]: name %s \n",i,mcoeff->pwp_zeta->aux_data_names[i]);
  }
  
  space_v = mcoeff->space_v;
  space_p = mcoeff->space_p;
  space_t = mcoeff->space_T;
  
  T_hat = ctx->mechanics->T_hat;
  ierr = DMGetLocalVector(dmt,&T_hat_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmt,T_hat,INSERT_VALUES,T_hat_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmt,T_hat,INSERT_VALUES,T_hat_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(T_hat_local,&_T_hat);CHKERRQ(ierr);
  
  X_hat = ctx->mechanics->X_hat;
  ierr = DMCompositeGetLocalVectors(ctx->mechanics->dm_m,&V_hat_local,&P_hat_local);CHKERRQ(ierr);
  ierr = DMCompositeScatter(ctx->mechanics->dm_m,X_hat,V_hat_local,P_hat_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(V_hat_local,&_V_hat);CHKERRQ(ierr);
  ierr = VecGetArrayRead(P_hat_local,&_P_hat);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"eta",NULL,NULL,NULL,&qp_coeff_eta);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"k*",NULL,NULL,NULL,&qp_coeff_kstar);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"phi",NULL,NULL,NULL,&qp_coeff_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"zeta*",NULL,NULL,NULL,&qp_coeff_zeta);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dmv,&nel,&nbasis_v,&element_v,&npe_v,NULL,&coords_v);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&nbasis_p,&element_p,&npe_p,NULL,&coords_p);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmt,&nel,&nbasis_t,&element_t,&npe_t,NULL,&coords_t);CHKERRQ(ierr);
  
  elcoords_v = space_v->buf_basis_2vector_a;
  el_u = space_v->buf_basis_scalar_a;
  el_v = space_v->buf_basis_scalar_b;
  el_p = space_p->buf_basis_scalar_a;
  el_t = space_t->buf_basis_scalar_a;
  
  /* check all evaluators if ANY of them need aux variables in SI units */
  if (mcoeff->pwp_eta->use_si) {     requires_si = PETSC_TRUE; }
  if (mcoeff->pwp_k_on_mu->use_si) { requires_si = PETSC_TRUE; }
  if (mcoeff->pwp_zeta->use_si) {    requires_si = PETSC_TRUE; }
  
  fprintf(fp_eta,"# [system]      require SI units for at least one coefficient %d \n",(int)requires_si);
  fprintf(fp_eta,"# [eta]         use SI units %d \n",(int)mcoeff->pwp_eta->use_si);
  fprintf(fp_eta,"# [permability] use SI units %d \n",(int)mcoeff->pwp_k_on_mu->use_si);
  fprintf(fp_eta,"# [zeta]        use SI units %d \n",(int)mcoeff->pwp_zeta->use_si);
  if (requires_si) {
    fprintf(fp_eta,"# [non-dimensional scales]\n");
    fprintf(fp_eta,"#   position    %1.4e \n",ctx->L);
    fprintf(fp_eta,"#   strain-rate %1.4e \n",1.0/ctx->T);
    fprintf(fp_eta,"#   pressure    %1.4e \n",ctx->P);
  }
  fprintf(fp_eta,"# =============================================\n");
  fprintf(fp_eta,"# Field legend \n");
  fprintf(fp_eta,"# COOR{cx[1], cy[2]} : INPUT{strain-rate-inv2[3], pressure[4], temperature[5], phi[6]}");
  index = 7;
  if (requires_si) {
    fprintf(fp_eta," : COOR[SI]{cx[%d], cy[%d]} : INPUT[SI]{strain-rate-inv2[%d], pressure[%d], temperature[%d], phi[%d]}",
            index,index+1, index+2,index+3,index+4,index+5);
    index = 13;
  }
  if (mcoeff->pwp_eta->naux != 0) { fprintf(fp_eta," : [eta] AUX_FIELDS{aux0[%d], ...}",index);               index += mcoeff->pwp_eta->naux; }
  else {                            fprintf(fp_eta," : [eta] AUX_FIELDS{none}"); }
  
  if (mcoeff->pwp_k_on_mu->naux != 0) { fprintf(fp_eta," : [permability] AUX_FIELDS{aux0[%d], ...}",index);   index += mcoeff->pwp_k_on_mu->naux; }
  else {                                fprintf(fp_eta," : [permability] AUX_FIELDS{none}"); }
  
  if (mcoeff->pwp_zeta->naux != 0) { fprintf(fp_eta," : [zeta] AUX_FIELDS{aux0[%d], ...}",index);             index += mcoeff->pwp_zeta->naux; }
  else {                             fprintf(fp_eta," : [zeta] AUX_FIELDS{none}"); }
  
  fprintf(fp_eta," : OUTPUT{eta[%d], k/mu[%d], zeta[%d], limited(eta)[%d], limited(k/mu)[%d], limited(zeta)[%d]}\n",
          index,index+1,index+2,index+3,index+4,index+5);
  
  for (e=0; e<nel; e++) {
    PetscInt *el_bidx_v,*el_bidx_p,*el_bidx_t;
    
    /* get basis dofs */
    el_bidx_v = &element_v[nbasis_v*e];
    el_bidx_p = &element_p[nbasis_p*e];
    el_bidx_t = &element_t[nbasis_t*e];
    
    /* get element coordinates from velocity space (chosen as we may require these later for strain-rate) */
    for (i=0; i<nbasis_v; i++) {
      PetscInt bidx = el_bidx_v[i];
      
      elcoords_v[2*i+0] = coords_v[2*bidx+0];
      elcoords_v[2*i+1] = coords_v[2*bidx+1];
    }
    
    /* get coords at each quadrature point */
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis_v; i++) {
        x_qp[0] += space_v->N[q][i] * elcoords_v[2*i+0];
        x_qp[1] += space_v->N[q][i] * elcoords_v[2*i+1];
      }
      
      mcoeff->cell_qp_coor[2*q+0] = x_qp[0];
      mcoeff->cell_qp_coor[2*q+1] = x_qp[1];
    }
    
    /* get velocity then strain-rate */
    for (i=0; i<nbasis_v; i++) {
      PetscInt bidx = el_bidx_v[i];
      
      el_u[i] = _V_hat[2*bidx+0];
      el_v[i] = _V_hat[2*bidx+1];
    }
    EvaluateBasisDerivatives(nqp,nbasis_v,elcoords_v,space_v->dNr1,space_v->dNr2,space_v->dNx1,space_v->dNx2);
    for (q=0; q<nqp; q++) {
      PetscReal eij[3],e2;
      
      eij[0] = eij[1] = eij[2] = 0.0;
      for (i=0; i<nbasis_v; i++) {
        eij[0] += space_v->dNx1[q][i] * el_u[i];
        eij[1] += space_v->dNx2[q][i] * el_v[i];
        eij[2] += 0.5 * (space_v->dNx1[q][i] * el_v[i] + space_v->dNx2[q][i] * el_u[i]);
      }
      e2 = PetscSqrtReal(0.5 * (eij[0]*eij[0] + eij[1]*eij[1] + 2.0*eij[2]*eij[2]) );
      
      mcoeff->cell_qp_e2[q] = e2;
    }
    
    /* get pressure */
    for (i=0; i<nbasis_p; i++) {
      PetscInt bidx = el_bidx_p[i];
      
      el_p[i] = _P_hat[bidx];
    }
    for (q=0; q<nqp; q++) {
      PetscReal p_qp;
      
      p_qp = 0.0;
      for (i=0; i<nbasis_p; i++) {
        p_qp += space_p->N[q][i] * el_p[i];
      }
      
      mcoeff->cell_qp_p[q] = p_qp;
    }
    
    /* get temperature */
    for (i=0; i<nbasis_t; i++) {
      PetscInt bidx = el_bidx_t[i];
      
      el_t[i] = _T_hat[bidx];
    }
    for (q=0; q<nqp; q++) {
      PetscReal T_qp;
      
      T_qp = 0.0;
      for (i=0; i<nbasis_t; i++) {
        T_qp += space_t->N[q][i] * el_t[i];
      }
      
      mcoeff->cell_qp_T[q] = T_qp;
    }
    
    /* get components */
    if (mcoeff->requires_components) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requires components at quadrature points requires MC interpolant: TPMCInterpolateBulkConcentrationAtPoint(). TODO");
    }
    for (q=0; q<nqp; q++) {
      mcoeff->cell_qp_CH2O[q] = 0.0;
      mcoeff->cell_qp_Crf[q] = 0.0;
    }
    
    /* get porosity */
    for (q=0; q<nqp; q++) {
      mcoeff->cell_qp_phi[q] = qp_coeff_phi[e*nqp+q];
    }
    
    if (requires_si) {
      /* convert quantities */
      for (q=0; q<nqp; q++) {
        mcoeff->cell_qp_coor_si[2*q+0] = mcoeff->cell_qp_coor[2*q+0] * ctx->L;
        mcoeff->cell_qp_coor_si[2*q+1] = mcoeff->cell_qp_coor[2*q+1] * ctx->L;
        mcoeff->cell_qp_e2_si[q]       = mcoeff->cell_qp_e2[q]       / ctx->T; /* multiply for sec, divide for 1/sec */
        mcoeff->cell_qp_p_si[q]        = mcoeff->cell_qp_p[q]        * ctx->P;
        
        /* temperature is not non-dimensionalized */
        mcoeff->cell_qp_T_si[q]    = mcoeff->cell_qp_T[q];
        /* concentrations are not non-dimensionalized */
        mcoeff->cell_qp_Crf_si[q]  = mcoeff->cell_qp_Crf[q];
        mcoeff->cell_qp_CH2O_si[q] = mcoeff->cell_qp_CH2O[q];
        /* porosity is not non-dimensionalized */
        mcoeff->cell_qp_phi_si[q]  = mcoeff->cell_qp_phi[q];
      }
    }
    
    /* call pointwise evaluator */
    /* shear viscosity */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_eta,nqp,&qp_coeff_eta[e*nqp]);CHKERRQ(ierr);
    /* permability */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_k_on_mu,nqp,&qp_coeff_kstar[e*nqp]);CHKERRQ(ierr);
    /* bulk viscosity */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_zeta,nqp,&qp_coeff_zeta[e*nqp]);CHKERRQ(ierr);

    /* write out ALL input | ALL aux | output */
    for (q=0; q<nqp; q++) {
      fprintf(fp_eta,"%+1.6e %+1.6e",mcoeff->cell_qp_coor[2*q+0],mcoeff->cell_qp_coor[2*q+1]);
      fprintf(fp_eta,"   %+1.6e %+1.6e %+1.6e %+1.6e",mcoeff->cell_qp_e2[q],mcoeff->cell_qp_p[q],mcoeff->cell_qp_T[q],mcoeff->cell_qp_phi[q]);

      if (requires_si) {
        fprintf(fp_eta,"   %+1.6e %+1.6e",mcoeff->cell_qp_coor_si[2*q+0],mcoeff->cell_qp_coor_si[2*q+1]);
        fprintf(fp_eta,"   %+1.6e %+1.6e %+1.6e %+1.6e",mcoeff->cell_qp_e2_si[q],mcoeff->cell_qp_p_si[q],mcoeff->cell_qp_T_si[q],mcoeff->cell_qp_phi_si[q]);
      }
      
      if (mcoeff->pwp_eta->naux != 0) fprintf(fp_eta,"  ");
      for (i=0; i<mcoeff->pwp_eta->naux; i++) {
        fprintf(fp_eta," %+1.6e",mcoeff->pwp_eta->aux_data[i][q]);
      }
      
      if (mcoeff->pwp_k_on_mu->naux != 0) fprintf(fp_eta,"  ");
      for (i=0; i<mcoeff->pwp_k_on_mu->naux; i++) {
        fprintf(fp_eta," %+1.6e",mcoeff->pwp_k_on_mu->aux_data[i][q]);
      }

      if (mcoeff->pwp_zeta->naux != 0) fprintf(fp_eta,"  ");
      for (i=0; i<mcoeff->pwp_zeta->naux; i++) {
        fprintf(fp_eta," %+1.6e",mcoeff->pwp_zeta->aux_data[i][q]);
      }

      fprintf(fp_eta,"   %+1.6e %+1.6e %+1.6e",qp_coeff_eta[e*nqp+q],qp_coeff_kstar[e*nqp+q],qp_coeff_zeta[e*nqp+q]);

      /* limit eta */
      if (mcoeff->pwp_eta_limiter) {
        if (mcoeff->pwp_eta->use_si != mcoeff->pwp_eta_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"shear viscosity evaluator and limiter must both use same setting for SI or non-dim");
        
        ierr = PWPhysicsEvaluate(mcoeff->pwp_eta_limiter,1,&qp_coeff_eta[e*nqp+q]);CHKERRQ(ierr);
      }

      /* limit zeta */
      if (mcoeff->pwp_zeta_limiter) {
        if (mcoeff->pwp_zeta->use_si != mcoeff->pwp_zeta_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"bulk viscosity evaluator and limiter must both use same setting for SI or non-dim");
        
        ierr = PWPhysicsEvaluate(mcoeff->pwp_zeta_limiter,1,&qp_coeff_zeta[e*nqp+q]);CHKERRQ(ierr);
      }
      
      /* Apply limiter on the effective permeability */
      if (mcoeff->pwp_k_on_mu_limiter) {
        if (mcoeff->pwp_k_on_mu->use_si != mcoeff->pwp_k_on_mu_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"effective permeability evaluator and limiter must both use same setting for SI or non-dim");
        
        ierr = PWPhysicsEvaluate(mcoeff->pwp_k_on_mu_limiter,1,&qp_coeff_kstar[e*nqp+q]);CHKERRQ(ierr);
      }

      fprintf(fp_eta,"   %+1.6e %+1.6e %+1.6e\n",qp_coeff_eta[e*nqp+q],qp_coeff_kstar[e*nqp+q],qp_coeff_zeta[e*nqp+q]);
      
    }
  }
  
  if (mcoeff->pwp_eta->use_si) {
    for (q=0; q<nel*nqp; q++) {
      /* compute viscosity is in SI units, non-dimensionalize now */
      qp_coeff_eta[q] /= ctx->E;
    }
  }

  if (mcoeff->pwp_k_on_mu->use_si) {
    for (q=0; q<nel*nqp; q++) {
      /* compute permability is in SI units, non-dimensionalize now */
      qp_coeff_kstar[q] /= ctx->K;
    }
  }

  if (mcoeff->pwp_zeta->use_si) {
    for (q=0; q<nel*nqp; q++) {
      /* compute bulk viscosity is in SI units, non-dimensionalize now */
      qp_coeff_zeta[q] /= ctx->E;
    }
  }

  ierr = VecRestoreArrayRead(P_hat_local,&_P_hat);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(V_hat_local,&_V_hat);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(ctx->mechanics->dm_m,&V_hat_local,&P_hat_local);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T_hat_local,&_T_hat);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmt,&T_hat_local);CHKERRQ(ierr);
  
  file_counter++;
  fclose(fp_eta);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "coeff_pointwise_QuadratureEvaluator"
PetscErrorCode coeff_pointwise_QuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  TPCtx ctx;
  PetscInt nen,*ridx,nqp;
  PetscReal *coeff_phi,*coeff,delta_rho;
  DM dm,dm_scalar;
  PetscReal e3[2];
  
  PetscFunctionBegin;
  ctx = (TPCtx)data;
  dm = ctx->mechanics->dm_v_hat;
  
  ierr = DMTetGeometryGetAttributes(dm,NULL,&ridx,NULL);CHKERRQ(ierr);
  
  /* porosity */
  ierr = QuadratureGetProperty(quadrature,"phi",&nen,&nqp,NULL,&coeff_phi);CHKERRQ(ierr);
  switch (ctx->phi_c_type) {
    case PHI_NONE:
      break;
    case PHI_CONST:
      ierr = QuadratureSetValues(quadrature,1,&ctx->phi,coeff_phi);CHKERRQ(ierr);
      break;
    case PHI_DISTF:
      //ierr = QuadratureEvaluator_porosity_MC(quadrature,nen,nqp,coeff_phi,eregion,ctx);CHKERRQ(ierr);
      //ierr = QuadratureEvaluator_PorosityMCP0(quadrature,nen,nqp,coeff_phi,eregion,ctx);CHKERRQ(ierr);
 
      ierr = DMTetCreateSharedSpace(ctx->mechanics->dm_v_hat,1,&dm_scalar);CHKERRQ(ierr);
      ierr = DMTetProjectField(dm_scalar,ctx->mechanics->T_hat,PRJTYPE_NATIVE,ctx->darcy->dm_c_phi_T,&ctx->darcy->T);CHKERRQ(ierr);
      ierr = DMDestroy(&dm_scalar);CHKERRQ(ierr);

      ierr = DarcyCtx_PoroDistF_VQuadrature(ctx->darcy,ctx->darcy->dm_c_phi_T,ctx->darcy->cl,ctx->darcy->cs,ctx->darcy->T,c);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unknown phi evaluator detected");
      break;
  }
  
  /* Following quantities evaluated inside pointwise_QuadratureEvaluator */
  /* shear viscosity */
  /* effective permability */
  /* bulk viscosity */
  if (!ctx->mechanics->coeff_container->debug) {
    ierr = pointwise_QuadratureEvaluator(c,quadrature,eregion,ctx);CHKERRQ(ierr);
  } else {
    ierr = pointwise_QuadratureEvaluator_debug(c,quadrature,eregion,ctx);CHKERRQ(ierr);
  }
  
  /* Darcy buouyancy */
  ierr = QuadratureGetProperty(quadrature,"e_3",NULL,NULL,NULL,&coeff);CHKERRQ(ierr);
  delta_rho = ctx->rho_s - ctx->rho_l;
  e3[0] = ( delta_rho ) * ctx->g[0];
  e3[1] = ( delta_rho ) * ctx->g[1];
  ierr = QuadratureSetValues(quadrature,2,e3,coeff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsCoeffContainerGetValidCellBuffer"
PetscErrorCode MechanicsCoeffContainerGetValidCellBuffer(MechanicsCoeffContainer c,MechanicsCoeffBufferType btype,PetscBool use_si,const PetscReal **buffer)
{
  switch (btype) {
    case MCBUFFER_COOR:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_coor_si;
      else        *buffer = (const PetscReal*)c->cell_qp_coor;
      break;
    case MCBUFFER_PRESSURE:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_p_si;
      else        *buffer = (const PetscReal*)c->cell_qp_p;
      break;
    case MCBUFFER_TEMPERATURE:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_T_si;
      else        *buffer = (const PetscReal*)c->cell_qp_T;
      break;
    case MCBUFFER_CRF:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_Crf_si;
      else        *buffer = (const PetscReal*)c->cell_qp_Crf;
      break;
    case MCBUFFER_CH2O:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_CH2O_si;
      else        *buffer = (const PetscReal*)c->cell_qp_CH2O;
      break;
    case MCBUFFER_SRATE_INV:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_e2_si;
      else        *buffer = (const PetscReal*)c->cell_qp_e2;
      break;
    case MCBUFFER_POROSITY:
      if (use_si) *buffer = (const PetscReal*)c->cell_qp_phi_si;
      else        *buffer = (const PetscReal*)c->cell_qp_phi;
      break;
      
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unknown buffer requested");
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pointwise_SurfaceQuadratureEvaluator"
PetscErrorCode pointwise_SurfaceQuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],TPCtx ctx)
{
  PetscErrorCode ierr;
  DM dmv,dmp,dmt;
  PetscInt i,q,eidx,nel,nqp;
  PetscReal *coords_v,*coords_p,*coords_t;
  PetscReal *el_t;//*elcoords_v,*el_u,*el_v,*el_p,;
  PetscInt *element_v,npe_v,nbasis_v;
  PetscInt *element_p,npe_p,nbasis_p;
  PetscInt *element_t,npe_t,nbasis_t;
  PetscReal *qp_coeff_kstar,*qp_coeff_phi,*qp_coeff_qN,*qp_coor;
  EContainer space_t;//space_v,space_p;
  MechanicsCoeffContainer mcoeff;
  Vec T_hat,T_hat_local,X_hat,V_hat_local,P_hat_local;
  const PetscScalar *_T_hat,*_V_hat,*_P_hat;
  PetscBool requires_si = PETSC_FALSE;
  BFacetList f;
  PetscInt *facet_to_element,*facet_element_face_id;
  PetscInt bf,nfacets,nbasis_face;
  

  PetscFunctionBegin;
  dmv = ctx->mechanics->dm_v_hat;
  dmp = ctx->mechanics->dm_p_hat;
  dmt = ctx->mechanics->dm_ve_hat;
  
  mcoeff = ctx->mechanics->coeff_container;
  
  //space_v = mcoeff->space_v;
  //space_p = mcoeff->space_p;
  space_t = mcoeff->space_T;
  
  T_hat = ctx->mechanics->T_hat;
  ierr = DMGetLocalVector(dmt,&T_hat_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dmt,T_hat,INSERT_VALUES,T_hat_local);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dmt,T_hat,INSERT_VALUES,T_hat_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(T_hat_local,&_T_hat);CHKERRQ(ierr);
  
  X_hat = ctx->mechanics->X_hat;
  ierr = DMCompositeGetLocalVectors(ctx->mechanics->dm_m,&V_hat_local,&P_hat_local);CHKERRQ(ierr);
  ierr = DMCompositeScatter(ctx->mechanics->dm_m,X_hat,V_hat_local,P_hat_local);CHKERRQ(ierr);
  ierr = VecGetArrayRead(V_hat_local,&_V_hat);CHKERRQ(ierr);
  ierr = VecGetArrayRead(P_hat_local,&_P_hat);CHKERRQ(ierr);
  
  ierr = QuadratureGetRule(quadrature,&nqp,NULL, NULL);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"coor",NULL,NULL,NULL,&qp_coor);CHKERRQ(ierr);  /* inputs */
  ierr = QuadratureGetProperty(quadrature,"phi", NULL,NULL,NULL,&qp_coeff_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"k*",  NULL,NULL,NULL,&qp_coeff_kstar);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,"q_N", NULL,NULL,NULL,&qp_coeff_qN);CHKERRQ(ierr);   /* output */
  
  ierr = DMTetSpaceElement(dmv,&nel,&nbasis_v,&element_v,&npe_v,NULL,&coords_v);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmp,&nel,&nbasis_p,&element_p,&npe_p,NULL,&coords_p);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(dmt,&nel,&nbasis_t,&element_t,&npe_t,NULL,&coords_t);CHKERRQ(ierr);
  
  //elcoords_v = space_v->buf_basis_2vector_a;
  //el_u = space_v->buf_basis_scalar_a;
  //el_v = space_v->buf_basis_scalar_b;
  //el_p = space_p->buf_basis_scalar_a;
  el_t = space_t->buf_basis_scalar_a;
  
  /* check evaluators being used: if ANY of them need aux variables in SI units */
  if (mcoeff->pwp_k_on_mu->use_si) { requires_si = PETSC_TRUE; }
  
  
  ierr = DMTetGetSpaceBFacetList(dmv,&f);CHKERRQ(ierr);
  ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
  
  /* return early if no facets have been defined */
  if (nfacets != 0) {
    PetscBool issetup;
    
    ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
    if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
  }
  
  ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
  ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);
  
  for (bf=0; bf<nfacets; bf++) {
    //PetscInt face_id;
    //PetscInt *el_bidx_v,*el_bidx_p;
    PetscInt *el_bidx_t;
    
    eidx    = facet_to_element[bf];
    //face_id = facet_element_face_id[bf];
    
    /* get basis dofs */
    //el_bidx_v = &element_v[nbasis_v*eidx];
    //el_bidx_p = &element_p[nbasis_p*eidx];
    el_bidx_t = &element_t[nbasis_t*eidx];
    
    
    /* get coords at each quadrature point */
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      x_qp[0] = qp_coor[2*(bf*nqp + q)+0];
      x_qp[1] = qp_coor[2*(bf*nqp + q)+1];
      
      mcoeff->cell_qp_coor[2*q+0] = x_qp[0];
      mcoeff->cell_qp_coor[2*q+1] = x_qp[1];
    }
    
    /* get element coordinates from velocity space (chosen as we may require these later for strain-rate) */

    /* get velocity then strain-rate */
    
    /* get pressure */
    
    /* get temperature */
    if (mcoeff->requires_temperature && eidx == 0) { }
    for (i=0; i<nbasis_t; i++) {
      PetscInt bidx = el_bidx_t[i];
      
      el_t[i] = _T_hat[bidx];
    }
    
    /* restrict temperature */
    // todo

    /* interpolate temperature */
    for (q=0; q<nqp; q++) {
      PetscReal T_qp;
      
      T_qp = 0.0;
      // update for facet basis interpolation
      //for (i=0; i<nbasis_t; i++) {
      //  T_qp += space_t->N[q][i] * el_t[i];
      //}
      
      mcoeff->cell_qp_T[q] = T_qp;
    }
    
    /* get components */
    /* Use SL interpolation to get values of chemical components at quadrature points */
    if (mcoeff->requires_components) {
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Requires components at quadrature points requires MC interpolant: TPMCInterpolateBulkConcentrationAtPoint(). TODO");
    }
    for (q=0; q<nqp; q++) {
      mcoeff->cell_qp_CH2O[q] = 0.0;
      mcoeff->cell_qp_Crf[q] = 0.0;
    }
    
    /* get porosity */
    if (mcoeff->requires_porosity && eidx == 0) { }
    for (q=0; q<nqp; q++) {
      mcoeff->cell_qp_phi[q] = qp_coeff_phi[bf*nqp+q];
    }
    
    if (requires_si) {
      /* convert quantities */
      for (q=0; q<nqp; q++) {
        mcoeff->cell_qp_coor_si[2*q+0] = mcoeff->cell_qp_coor[2*q+0] * ctx->L;
        mcoeff->cell_qp_coor_si[2*q+1] = mcoeff->cell_qp_coor[2*q+1] * ctx->L;
        
        /* temperature is not non-dimensionalized */
        mcoeff->cell_qp_T_si[q]    = mcoeff->cell_qp_T[q];

        /* concentrations are not non-dimensionalized */
        mcoeff->cell_qp_Crf_si[q]  = mcoeff->cell_qp_Crf[q];
        mcoeff->cell_qp_CH2O_si[q] = mcoeff->cell_qp_CH2O[q];
        
        /* porosity is not non-dimensionalized */
        mcoeff->cell_qp_phi_si[q]  = mcoeff->cell_qp_phi[q];
      }
    }
    
    /* call pointwise evaluator */
    /* shear viscosity */
    
    /* permability */
    ierr = PWPhysicsEvaluate(mcoeff->pwp_k_on_mu,nqp,&qp_coeff_kstar[bf*nqp]);CHKERRQ(ierr);
    
    /* Apply limiter on the effective permeability */
    /* removed this call as we "might" want to apply a different limiter to the surface k* cf. the interior */
    if (mcoeff->pwp_k_on_mu_limiter) {
      if (mcoeff->pwp_k_on_mu->use_si != mcoeff->pwp_k_on_mu_limiter->use_si) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"effective permeability evaluator and limiter must both use same setting for SI or non-dim");
      ierr = PWPhysicsEvaluate(mcoeff->pwp_k_on_mu_limiter,nqp,&qp_coeff_kstar[bf*nqp]);CHKERRQ(ierr);
    }
     
    /* hard coded limiter */
    /*
    for (q=0; q<nqp; q++) {
      PetscReal havg;

      havg = (1.0/qp_coeff_kstar[bf*nqp+q] + 1.0/50.0);
      qp_coeff_kstar[bf*nqp+q] = 1.0/havg;
      
      if (qp_coeff_kstar[bf*nqp+q] < 10.0) {
        qp_coeff_kstar[bf*nqp+q] = 10.0;
      }
    }
    */
     
    /* bulk-viscosity */
    /* Apply limiter on the bulk viscosity */
  }
  
  /* Scale permability if respective evaluators use SI */
  if (mcoeff->pwp_k_on_mu->use_si) {
    for (q=0; q<nfacets*nqp; q++) {
      /* compute permability is in SI units, non-dimensionalize now */
      qp_coeff_kstar[q] /= ctx->K;
    }
  }

  /* compute v_D = k* delta_rho g */
  {
    PetscReal delta_rho = ctx->rho_s - ctx->rho_l;

    for (q=0; q<nfacets*nqp; q++) {
      qp_coeff_qN[2*q+0] = -qp_coeff_kstar[q] * delta_rho * ctx->g[0];
      qp_coeff_qN[2*q+1] = -qp_coeff_kstar[q] * delta_rho * ctx->g[1];
    }
  }

  ierr = VecRestoreArrayRead(P_hat_local,&_P_hat);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(V_hat_local,&_V_hat);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(ctx->mechanics->dm_m,&V_hat_local,&P_hat_local);CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(T_hat_local,&_T_hat);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dmt,&T_hat_local);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "coeff_pointwise_SurfaceQuadratureEvaluator"
PetscErrorCode coeff_pointwise_SurfaceQuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  TPCtx          ctx;
  PDE            pde;
  Quadrature     quadrature_v;
  PetscInt       nen,nqp,nen_v,nqp_v;
  PetscReal      *q_coeff_phi,*q_coeff_phi_v,*q_coor;
  DM             dm;
  Coefficient    bform_vol,lform_flux;
  
  PetscFunctionBegin;
  ctx = (TPCtx)data;
  dm = ctx->mechanics->dm_p_hat;
  
  
  ierr = CoefficientGetPDE(c,&pde);CHKERRQ(ierr);
  ierr = PDEStokesDarcy2FieldGetCoefficients(pde,&bform_vol,NULL,NULL,&lform_flux);CHKERRQ(ierr);
  if (lform_flux != c) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Coefficient does not appear to be consistent with the Darcy flux coefficient");
  
  ierr = CoefficientGetQuadrature(bform_vol,&quadrature_v);CHKERRQ(ierr);
  /* porosity */
  ierr = QuadratureGetProperty(quadrature_v,"phi", &nen_v,&nqp_v,NULL,&q_coeff_phi_v);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,  "phi", &nen,  &nqp,  NULL,&q_coeff_phi);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,  "coor",&nen,  &nqp,  NULL,&q_coor);CHKERRQ(ierr);
  
  /*
  switch (ctx->phi_c_type) {
    case PHI_NONE:
      break;
    case PHI_CONST:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Constant porosity case is not supported");
      break;
    case PHI_DISTF:
      ierr = QuadratureEvaluator_porosity_MC(quadrature,nen,nqp,coeff_phi,eregion,ctx);CHKERRQ(ierr);
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Unknown phi evaluator detected");
      break;
  }
  */

  /*
   Description: 

   Use average phi from the cell to determine phi on the facet
   - Requires coeff("fu") has been already evaluated
   + Should reduce the amount of numerical diffusion occurring over the wedge-plate interface.
     The diffusion will be reduced once I update DMDASemiLagrangianInterpolate() to work with FictD-CSL
   
   Algorithm:
   
   for each facet f
     get element neighbour, e
     get e->volume_quadrature("phi"), phi_vol
     average phi values from the volume
     assign average to surface quadrature points in f
  */

  {
    BFacetList f;
    PetscInt *facet_to_element,*facet_element_face_id;
    PetscInt bf,nfacets,nbasis_face;

    ierr = DMTetGetSpaceBFacetList(dm,&f);CHKERRQ(ierr);
    ierr = BFacetListGetSizes(f,&nfacets,&nbasis_face);CHKERRQ(ierr);
    
    /* return early if no facets have been defined */
    if (nfacets != 0) {
      PetscBool issetup;
      
      ierr = BFacetListIsSetup(f,&issetup);CHKERRQ(ierr);
      if (!issetup) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Requires DMTet boundary facet list has been setup");
    }

    ierr = BFacetListGetFacetElementMap(f,&facet_to_element);CHKERRQ(ierr);
    ierr = BFacetListGetFacetId(f,&facet_element_face_id);CHKERRQ(ierr);

    for (bf=0; bf<nfacets; bf++) {
      PetscInt  q,e_id;
      //PetscInt  face_id;
      PetscReal phi_avg;
      
      e_id    = facet_to_element[bf];
      //face_id = facet_element_face_id[bf];

      phi_avg = 0.0;
      for (q=0; q<nqp_v; q++) {
        phi_avg += q_coeff_phi_v[e_id * nqp_v + q];
      }
      phi_avg = phi_avg / ((PetscReal)nqp_v);
      
      for (q=0; q<nqp; q++) {
        //PetscReal qp_coor[2];
        //qp_coor[0] = q_coor[2*(bf*nqp + q)+0];
        //qp_coor[1] = q_coor[2*(bf*nqp + q)+1];
        
        q_coeff_phi[bf*nqp + q] = phi_avg;
        //printf("%d %d %+1.3e %+1.3e %+1.3e\n",bf,q,qp_coor[0],qp_coor[1],phi_avg);
      }
      
    }
    
  }
  
  /* Following quantities evaluated inside pointwise_SurfaceQuadratureEvaluator */
  /* effective permability */
  ierr = pointwise_SurfaceQuadratureEvaluator(c,quadrature,eregion,ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
