
#include <pointwise_physics.h>

/* Permeability divided by fluid viscosity */

/* constant */
typedef struct {
  PetscReal k0_;
} k_Constant;

#undef __FUNCT__
#define __FUNCT__ "kConstant_evaluate"
static PetscErrorCode kConstant_evaluate(k_Constant *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal k[])
{
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    k[p] = c->k0_;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kConstant_view"
static PetscErrorCode kConstant_view(k_Constant *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Constant permeability divided by fluid viscosity (k0) %1.4e\n",c->k0_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kConstant_set_from_options"
static PetscErrorCode kConstant_set_from_options(const char opt_pre[],k_Constant *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-k0",&c->k0_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kConstantCreate"
PetscErrorCode kConstantCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal k0;
  PWPhysics phys;
  k_Constant *permeability;
  
  ierr = PWPhysicsCreate(sizeof(k_Constant),"k.Constant",name,use_si,region,&phys);CHKERRQ(ierr);
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,kConstant_evaluate);
  PWPhysicsSetView(phys,kConstant_view);
  PWPhysicsSetSetFromOptions(phys,kConstant_set_from_options);
  
  permeability = (k_Constant*)phys->data;
  // parameters
  k0 = 1.0;
  permeability->k0_ = k0;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kConstantSetParams"
PetscErrorCode kConstantSetParams(PWPhysics p,PetscReal k0)
{
  PetscErrorCode ierr;
  k_Constant *permeability;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"k.Constant",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  permeability = (k_Constant*)p->data;
  if (k0 != PETSC_DEFAULT) permeability->k0_ = k0;
  PetscFunctionReturn(0);
}

/* porosity-dependent (power law) */
typedef struct {
  PetscReal k0_;
  PetscReal n_;
} k_PorosityDependent;

#undef __FUNCT__
#define __FUNCT__ "kPorosityDependent_evaluate"
static PetscErrorCode kPorosityDependent_evaluate(k_PorosityDependent *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal k[])
{
  const PetscReal *aux_phi  = aux_fields[0];
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    k[p] = c->k0_ * PetscPowReal((aux_phi[p]/0.01),c->n_);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kPorosityDependent_view"
static PetscErrorCode kPorosityDependent_view(k_PorosityDependent *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Porosity-Dependent permeability divided by fluid viscosity (k0) %1.4e ; exponent (n) %1.4e\n",c->k0_,c->n_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kPorosityDependent_set_from_options"
static PetscErrorCode kPorosityDependent_set_from_options(const char opt_pre[],k_PorosityDependent *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-k0",&c->k0_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-n",&c->n_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kPorosityDependentCreate"
PetscErrorCode kPorosityDependentCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal k0;
  PetscReal n;
  PWPhysics phys;
  k_PorosityDependent *permeability;
  const char *aux_names[] = { "porosity" };

  
  ierr = PWPhysicsCreate(sizeof(k_PorosityDependent),"k.PorosityDependent",name,use_si,region,&phys);CHKERRQ(ierr);
  
  ierr = PWPhysicsSetNumberAuxiliaryFields(phys,1,aux_names);CHKERRQ(ierr); /* porosity */

  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,kPorosityDependent_evaluate);
  PWPhysicsSetView(phys,kPorosityDependent_view);
  PWPhysicsSetSetFromOptions(phys,kPorosityDependent_set_from_options);
  
  permeability = (k_PorosityDependent*)phys->data;
  // parameters (user choice)
  k0 = 1.0e-15; 
  n = 3.0;
  permeability->k0_ = k0;
  permeability->n_ = n;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kPorosityDependentSetParams"
PetscErrorCode kPorosityDependentSetParams(PWPhysics p,PetscReal k0,PetscReal n)
{
  PetscErrorCode ierr;
  k_PorosityDependent *permeability;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"k.PorosityDependent",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  permeability = (k_PorosityDependent*)p->data;
  if (k0 != PETSC_DEFAULT) permeability->k0_ = k0;
  if (n != PETSC_DEFAULT) permeability->n_ = n;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "kPorosityDependentGetParams"
PetscErrorCode kPorosityDependentGetParams(PWPhysics p,PetscReal *k0,PetscReal *n)
{
  PetscErrorCode ierr;
  k_PorosityDependent *permeability;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"k.PorosityDependent",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  permeability = (k_PorosityDependent*)p->data;
  if (k0) { *k0 = permeability->k0_; }
  if (n) { *n = permeability->n_; }
  PetscFunctionReturn(0);
}

/* Bulk viscosity */

/* constant */
typedef struct {
  PetscReal zeta0_;
} Zeta_Constant;

#undef __FUNCT__
#define __FUNCT__ "ZetaConstant_evaluate"
static PetscErrorCode ZetaConstant_evaluate(Zeta_Constant *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal zeta[])
{
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    zeta[p] = c->zeta0_;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaConstant_view"
static PetscErrorCode ZetaConstant_view(Zeta_Constant *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Constant bulk viscosity (zeta0) %1.4e\n",c->zeta0_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaConstant_set_from_options"
static PetscErrorCode ZetaConstant_set_from_options(const char opt_pre[],Zeta_Constant *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-zeta0",&c->zeta0_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaConstantCreate"
PetscErrorCode ZetaConstantCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal zeta0;
  PWPhysics phys;
  Zeta_Constant *creep;
  
  ierr = PWPhysicsCreate(sizeof(Zeta_Constant),"zeta.Constant",name,use_si,region,&phys);CHKERRQ(ierr);
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,ZetaConstant_evaluate);
  PWPhysicsSetView(phys,ZetaConstant_view);
  PWPhysicsSetSetFromOptions(phys,ZetaConstant_set_from_options);
  
  creep = (Zeta_Constant*)phys->data;
  // parameters
  zeta0 = 1.0;
  creep->zeta0_ = zeta0;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaConstantSetParams"
PetscErrorCode ZetaConstantSetParams(PWPhysics p,PetscReal zeta0)
{
  PetscErrorCode ierr;
  Zeta_Constant *creep;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"zeta.Constant",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Zeta_Constant*)p->data;
  creep->zeta0_ = zeta0;
  PetscFunctionReturn(0);
}

/* ratio (zeta = R * eta) */
typedef struct {
  PetscReal R0_;
  PWPhysics eta_flowlaw;
} Zeta_Ratio;

#undef __FUNCT__
#define __FUNCT__ "ZetaRatio_evaluate"
static PetscErrorCode ZetaRatio_evaluate(Zeta_Ratio *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal zeta[])
{
  PetscInt p;
  PetscErrorCode ierr;
  
  ierr = PWPhysicsEvaluate(c->eta_flowlaw,npoints,zeta);CHKERRQ(ierr);
  for (p=0; p<npoints; p++) {
    zeta[p] = c->R0_ * zeta[p];
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaRatio_view"
static PetscErrorCode ZetaRatio_view(Zeta_Ratio *c,PetscViewer v)
{
  PetscErrorCode ierr;
  
  PetscViewerASCIIPrintf(v,"Fixed bulk:shear ratio R %1.4e\n",c->R0_);
  PetscViewerASCIIPrintf(v,"[sub] eta flow law used:\n");
  PetscViewerASCIIPushTab(v);
  ierr = PWPhysicsView(c->eta_flowlaw,v);CHKERRQ(ierr);
  PetscViewerASCIIPopTab(v);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaRatio_set_from_options"
static PetscErrorCode ZetaRatio_set_from_options(const char opt_pre[],Zeta_Ratio *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-R0",&c->R0_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaRatioCreate"
PetscErrorCode ZetaRatioCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal R0;
  PWPhysics phys;
  Zeta_Ratio *ratio;
  
  ierr = PWPhysicsCreate(sizeof(Zeta_Ratio),"zeta.Ratio",name,use_si,region,&phys);CHKERRQ(ierr);
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,ZetaRatio_evaluate);
  PWPhysicsSetView(phys,ZetaRatio_view);
  PWPhysicsSetSetFromOptions(phys,ZetaRatio_set_from_options);
  
  ratio = (Zeta_Ratio*)phys->data;
  // parameters
  R0 = 1.0;
  ratio->R0_ = R0;
  ratio->eta_flowlaw = NULL;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaRatioSetParams"
PetscErrorCode ZetaRatioSetParams(PWPhysics p,PetscReal R0,PWPhysics eta_flowlaw)
{
  PetscErrorCode ierr;
  Zeta_Ratio *ratio;
  PetscInt n;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"zeta.Ratio",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  ratio = (Zeta_Ratio*)p->data;
  if (R0 != PETSC_DEFAULT) ratio->R0_ = R0;
  
  ratio->eta_flowlaw = eta_flowlaw;
  /* inheret aux fields (names and data) from eta flow law */
  ierr = PWPhysicsSetNumberAuxiliaryFields(p,eta_flowlaw->naux,(const char**)eta_flowlaw->aux_data_names);CHKERRQ(ierr);
  for (n=0; n<eta_flowlaw->naux; n++) {
    p->aux_data[n] = eta_flowlaw->aux_data[n];
  }
  p->use_si = eta_flowlaw->use_si;
  PetscFunctionReturn(0);
}


/* Shear viscosity */

/* constant */
typedef struct {
  PetscReal eta0_;
} Eta_Constant;

#undef __FUNCT__
#define __FUNCT__ "EtaConstant_evaluate"
static PetscErrorCode EtaConstant_evaluate(Eta_Constant *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal eta[])
{
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    eta[p] = c->eta0_;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaConstant_view"
static PetscErrorCode EtaConstant_view(Eta_Constant *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Constant viscosity (eta0) %1.4e\n",c->eta0_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaConstant_set_from_options"
static PetscErrorCode EtaConstant_set_from_options(const char opt_pre[],Eta_Constant *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-eta0",&c->eta0_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaConstantCreate"
PetscErrorCode EtaConstantCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal eta0;
  PWPhysics phys;
  Eta_Constant *creep;
  
  ierr = PWPhysicsCreate(sizeof(Eta_Constant),"eta.Constant",name,use_si,region,&phys);CHKERRQ(ierr);
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,EtaConstant_evaluate);
  PWPhysicsSetView(phys,EtaConstant_view);
  PWPhysicsSetSetFromOptions(phys,EtaConstant_set_from_options);
  
  creep = (Eta_Constant*)phys->data;
  // parameters
  eta0 = 1.0;
  creep->eta0_ = eta0;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaConstantSetParams"
PetscErrorCode EtaConstantSetParams(PWPhysics p,PetscReal eta0)
{
  PetscErrorCode ierr;
  Eta_Constant *creep;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"eta.Constant",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Eta_Constant*)p->data;
  creep->eta0_ = eta0;
  PetscFunctionReturn(0);
}


/* Diffusion creep flow law */
typedef struct {
  PetscReal A_;
  PetscReal E_;
} Generic_DiffusionCreep;

#undef __FUNCT__
#define __FUNCT__ "GenericDiffusionCreep_evaluate"
static PetscErrorCode GenericDiffusionCreep_evaluate(Generic_DiffusionCreep *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal eta[])
{
  const PetscReal *aux_T = aux_fields[0];
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    eta[p] = c->A_ * PetscExpReal(c->E_/(PWP_GAS_CONSTANT * aux_T[p]));
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GenericDiffusionCreep_view"
static PetscErrorCode GenericDiffusionCreep_view(Generic_DiffusionCreep *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Pre-exponential factor (A) %1.4e ; Activation energy (E) %1.4e\n",c->A_,c->E_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GenericDiffusionCreep_set_from_options"
static PetscErrorCode GenericDiffusionCreep_set_from_options(const char opt_pre[],Generic_DiffusionCreep *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-A",&c->A_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-E",&c->E_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DiffusionCreepCreate_Generic"
static PetscErrorCode DiffusionCreepCreate_Generic(const char variable_name[],const char name[],PetscBool use_si,PetscInt region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  const char *aux_names[] = { "temperature" };
  char thing[PETSC_MAX_PATH_LEN];
  
  ierr = PetscSNPrintf(thing,PETSC_MAX_PATH_LEN-1,"%s.DiffusionCreep",variable_name);CHKERRQ(ierr);
  ierr = PWPhysicsCreate(sizeof(Generic_DiffusionCreep),thing,name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsSetNumberAuxiliaryFields(phys,1,aux_names);CHKERRQ(ierr); /* temperature */
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,GenericDiffusionCreep_evaluate);
  PWPhysicsSetView(phys,GenericDiffusionCreep_view);
  PWPhysicsSetSetFromOptions(phys,GenericDiffusionCreep_set_from_options);
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaDiffusionCreepCreate"
PetscErrorCode EtaDiffusionCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal A,E;
  PWPhysics phys;
  Generic_DiffusionCreep *creep;

  ierr = DiffusionCreepCreate_Generic("eta",name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsAddReference(phys,"* Form of flow law taken from PvK et al, PEPI, 171, (2008)");CHKERRQ(ierr);
  creep = (Generic_DiffusionCreep*)phys->data;
  
  // parameters
  A = 1.32043 * 1.0e8;
  E = 335.0 * 1.0e2;
  creep->A_ = A;
  creep->E_ = E;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);

  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DiffusionCreepSetParams_Generic"
static PetscErrorCode DiffusionCreepSetParams_Generic(PWPhysics p,const char variable_name[],PetscReal A,PetscReal E)
{
  PetscErrorCode ierr;
  Generic_DiffusionCreep *creep;
  PetscBool same;
  char thing[PETSC_MAX_PATH_LEN];
  
  if (!p) PetscFunctionReturn(0);
  ierr = PetscSNPrintf(thing,PETSC_MAX_PATH_LEN-1,"%s.DiffusionCreep",variable_name);CHKERRQ(ierr);
  ierr = PWPhysicsTypeCompare(p,thing,&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Generic_DiffusionCreep*)p->data;
  if (A != PETSC_DEFAULT) creep->A_ = A;
  if (E != PETSC_DEFAULT) creep->E_ = E;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaDiffusionCreepSetParams"
PetscErrorCode EtaDiffusionCreepSetParams(PWPhysics p,PetscReal A,PetscReal E)
{
  PetscErrorCode ierr;
  if (!p) PetscFunctionReturn(0);
  ierr = DiffusionCreepSetParams_Generic(p,"eta",A,E);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaDiffusionCreepCreate_PvKPEPI2008"
PetscErrorCode EtaDiffusionCreepCreate_PvKPEPI2008(int region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  Generic_DiffusionCreep *creep;
  
  ierr = EtaDiffusionCreepCreate(PWP_SI_UNITS,region,"olivine",&phys);CHKERRQ(ierr);
  ierr = PWPhysicsAddReference(phys,"* Parameters given in Table 1 of PvK et al, PEPI, 171, (2008)");CHKERRQ(ierr);
  
  // over-ride parameters for Olivine
  creep = (Generic_DiffusionCreep*)phys->data;
  creep->A_ = 1.32043 * 1.0e9;
  creep->E_ = 335.0 * 1.0e3;
  /* 
   Note that a specific instance like this constructor,
   probably should not need to explicitly call PWPhysicsSetFromOptions().
   If you want to change these values, either call
   EtaDiffusionCreepSetParams() or
   PWPhysicsSetFromOptions() from your application code.
  */
  
  *p = phys;
  PetscFunctionReturn(0);
}


/* Dislocation creep flow law */
typedef struct {
  PetscReal A_;
  PetscReal E_;
  PetscReal n_;
} Generic_DislocationCreep;

#undef __FUNCT__
#define __FUNCT__ "GenericDislocationCreep_evaluate"
static PetscErrorCode GenericDislocationCreep_evaluate(Generic_DislocationCreep *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal eta[])
{
  const PetscReal *aux_T  = aux_fields[0];
  const PetscReal *aux_E2 = aux_fields[1];
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    eta[p] = c->A_ * PetscExpReal(c->E_/(c->n_ * PWP_GAS_CONSTANT * aux_T[p])) * PetscPowReal(aux_E2[p],(1.0 - c->n_)/c->n_);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GenericDislocationCreep_view"
static PetscErrorCode GenericDislocationCreep_view(Generic_DislocationCreep *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Pre-exponential factor (A) %1.4e ; Activation energy (E) %1.4e ; Exponent (n) %1.4e\n",c->A_,c->E_,c->n_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GenericDislocationCreep_set_from_options"
static PetscErrorCode GenericDislocationCreep_set_from_options(const char opt_pre[],Generic_DislocationCreep *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-A",&c->A_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-E",&c->E_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-n",&c->n_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DislocationCreepCreate_Generic"
static PetscErrorCode DislocationCreepCreate_Generic(const char variable_name[],const char name[],PetscBool use_si,PetscInt region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  const char *aux_names[] = { "temperature" , "strain-rate invariant" };
  char thing[PETSC_MAX_PATH_LEN];
  
  ierr = PetscSNPrintf(thing,PETSC_MAX_PATH_LEN-1,"%s.DislocationCreep",variable_name);CHKERRQ(ierr);
  ierr = PWPhysicsCreate(sizeof(Generic_DiffusionCreep),thing,name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsSetNumberAuxiliaryFields(phys,2,aux_names);CHKERRQ(ierr); /* (0) temperature ; (1) strain-rate invariant */
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,GenericDislocationCreep_evaluate);
  PWPhysicsSetView(phys,GenericDislocationCreep_view);
  PWPhysicsSetSetFromOptions(phys,GenericDislocationCreep_set_from_options);
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DislocationCreepSetParams_Generic"
static PetscErrorCode DislocationCreepSetParams_Generic(PWPhysics p,const char variable_name[],PetscReal A,PetscReal E,PetscReal n)
{
  PetscErrorCode ierr;
  Generic_DislocationCreep *creep;
  PetscBool same;
  char thing[PETSC_MAX_PATH_LEN];
  
  if (!p) PetscFunctionReturn(0);
  ierr = PetscSNPrintf(thing,PETSC_MAX_PATH_LEN-1,"%s.DislocationCreep",variable_name);CHKERRQ(ierr);
  ierr = PWPhysicsTypeCompare(p,thing,&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Generic_DislocationCreep*)p->data;
  if (A != PETSC_DEFAULT) creep->A_ = A;
  if (E != PETSC_DEFAULT) creep->E_ = E;
  if (n != PETSC_DEFAULT) creep->n_ = n;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaDislocationCreepCreate"
PetscErrorCode EtaDislocationCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal A,E,n;
  PWPhysics phys;
  Generic_DislocationCreep *creep;
  
  ierr = DislocationCreepCreate_Generic("eta",name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsAddReference(phys,"* Form of flow law taken from PvK et al, PEPI, 171, (2008)");CHKERRQ(ierr);

  creep = (Generic_DislocationCreep*)phys->data;
  // parameters
  A = 1.32043 * 1.0e8;
  E = 335.0 * 1.0e2;
  n = 1.0;
  creep->A_ = A;
  creep->E_ = E;
  creep->n_ = n;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaDislocationCreepSetParams"
PetscErrorCode EtaDislocationCreepSetParams(PWPhysics p,PetscReal A,PetscReal E,PetscReal n)
{
  PetscErrorCode ierr;
  if (!p) PetscFunctionReturn(0);
  ierr = DislocationCreepSetParams_Generic(p,"eta",A,E,n);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaDislocationCreepCreate_PvKPEPI2008"
PetscErrorCode EtaDislocationCreepCreate_PvKPEPI2008(int region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  Generic_DislocationCreep *creep;
  
  ierr = EtaDislocationCreepCreate(PWP_SI_UNITS,region,"olivine",&phys);CHKERRQ(ierr);
  ierr = PWPhysicsAddReference(phys,"* Parameters given in Table 1 of PvK et al, PEPI, 171, (2008)");CHKERRQ(ierr);
  
  // over-ride parameters for Olivine
  creep = (Generic_DislocationCreep*)phys->data;
  creep->A_ = 28968.6;
  creep->E_ = 540.0 * 1.0e3;
  creep->n_ = 3.5;
  
  *p = phys;
  PetscFunctionReturn(0);
}

/* Combined Diffusion and Dislocation creep flow law */
typedef struct {
  PetscReal Adiff_;
  PetscReal Ediff_;
  PetscReal Adisl_;
  PetscReal Edisl_;
  PetscReal ndisl_;
} Eta_CompositeDiffDislCreep;

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeDiffDislCreep_evaluate"
static PetscErrorCode EtaCompositeDiffDislCreep_evaluate(Eta_CompositeDiffDislCreep *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal eta[])
{
  const PetscReal *aux_T  = aux_fields[0];
  const PetscReal *aux_E2 = aux_fields[1];
  PetscInt p;
  
  for (p=0; p<npoints; p++) {
    PetscReal eta_diff, eta_disl;
    eta_diff = c->Adiff_ * PetscExpReal(c->Ediff_/(PWP_GAS_CONSTANT * aux_T[p]));
    eta_disl = c->Adisl_ * PetscExpReal(c->Edisl_/(c->ndisl_ * PWP_GAS_CONSTANT * aux_T[p])) * PetscPowReal(aux_E2[p],(1.0 - c->ndisl_)/c->ndisl_);
    eta[p] = 2/(1/eta_disl+1/eta_diff);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeDiffDislCreep_view"
static PetscErrorCode EtaCompositeDiffDislCreep_view(Eta_CompositeDiffDislCreep *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Diffusion Creep: Pre-exponential factor (Adiff) %1.4e ; Activation energy (Ediff) %1.4e\n",c->Adiff_,c->Ediff_);
  PetscViewerASCIIPrintf(v,"Dislocation Creep: Pre-exponential factor (Adisl) %1.4e ; Activation energy (Edisl) %1.4e ; Exponent (ndisl) %1.4e\n",c->Adisl_,c->Edisl_,c->ndisl_);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeDiffDislCreep_set_from_options"
static PetscErrorCode EtaCompositeDiffDislCreep_set_from_options(const char opt_pre[],Eta_CompositeDiffDislCreep *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-Adiff",&c->Adiff_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-Ediff",&c->Ediff_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-Adisl",&c->Adisl_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-Edisl",&c->Edisl_,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-ndisl",&c->ndisl_,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeDiffDislCreepCreate"
PetscErrorCode EtaCompositeDiffDislCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal Adiff,Ediff,Adisl,Edisl,ndisl;
  PWPhysics phys;
  Eta_CompositeDiffDislCreep *creep;
  const char *aux_names[] = { "temperature" , "strain-rate invariant" };
  
  ierr = PWPhysicsCreate(sizeof(Eta_CompositeDiffDislCreep),"eta.CompositeDiffDislCreep",name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsAddReference(phys,"* Composite (harmonic mean) of diffusion and dislocation creep flow laws taken from PvK et al, PEPI, 171, (2008)");CHKERRQ(ierr);
  
  ierr = PWPhysicsSetNumberAuxiliaryFields(phys,2,aux_names);CHKERRQ(ierr); /* (0) temperature ; (1) strain-rate invariant */
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,EtaCompositeDiffDislCreep_evaluate);
  PWPhysicsSetView(phys,EtaCompositeDiffDislCreep_view);
  PWPhysicsSetSetFromOptions(phys,EtaCompositeDiffDislCreep_set_from_options);
  
  creep = (Eta_CompositeDiffDislCreep*)phys->data;
  // parameters
  Adiff = 1.32043 * 1.0e8;
  Ediff = 335.0 * 1.0e2;
  creep->Adiff_ = Adiff;
  creep->Ediff_ = Ediff;
  Adisl = 1.32043 * 1.0e8;
  Edisl = 335.0 * 1.0e2;
  ndisl = 1.0;
  creep->Adisl_ = Adisl;
  creep->Edisl_ = Edisl;
  creep->ndisl_ = ndisl;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeDiffDislCreepSetParams"
PetscErrorCode EtaCompositeDiffDislCreepSetParams(PWPhysics p,PetscReal Adiff,PetscReal Ediff,PetscReal Adisl,PetscReal Edisl,PetscReal ndisl)
{
  PetscErrorCode ierr;
  Eta_CompositeDiffDislCreep *creep;
  PetscBool same;
  
  ierr = PWPhysicsTypeCompare(p,"eta.CompositeDiffDislCreep",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Eta_CompositeDiffDislCreep*)p->data;
  if (Adiff != PETSC_DEFAULT) creep->Adiff_ = Adiff;
  if (Ediff != PETSC_DEFAULT) creep->Ediff_ = Ediff;
  if (Adisl != PETSC_DEFAULT) creep->Adisl_ = Adisl;
  if (Edisl != PETSC_DEFAULT) creep->Edisl_ = Edisl;
  if (ndisl != PETSC_DEFAULT) creep->ndisl_ = ndisl;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeDiffDislCreepCreate_PvKPEPI2008"
PetscErrorCode EtaCompositeDiffDislCreepCreate_PvKPEPI2008(int region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  Eta_CompositeDiffDislCreep *creep;
  
  ierr = EtaCompositeDiffDislCreepCreate(PWP_SI_UNITS,region,"olivine",&phys);CHKERRQ(ierr);
  ierr = PWPhysicsAddReference(phys,"* Parameters given in Table 1 of PvK et al, PEPI, 171, (2008)");CHKERRQ(ierr);
  
  // over-ride parameters for Olivine
  creep = (Eta_CompositeDiffDislCreep*)phys->data;
  creep->Adiff_ = 1.32043 * 1.0e9;
  creep->Ediff_ = 335.0 * 1.0e3;
  creep->Adisl_ = 28968.6;
  creep->Edisl_ = 540.0 * 1.0e3;
  creep->ndisl_ = 3.5;
  
  *p = phys;
  PetscFunctionReturn(0);
}

/* Composite flow law */
typedef struct {
  PetscInt nlaws;
  PetscInt npoints;
  PetscReal *eta_buffer;
  PWPhysics *flow_law;
} Eta_Composite;

#undef __FUNCT__
#define __FUNCT__ "EtaComposite_evaluate"
static PetscErrorCode EtaComposite_evaluate(Eta_Composite *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal eta[])
{
  PetscInt p,k;
  PetscErrorCode ierr;

  for (p=0; p<npoints; p++) {
    eta[p] = 0.0;
  }

  for (k=0; k<c->nlaws; k++) {
    ierr = PWPhysicsEvaluate(c->flow_law[k],npoints,c->eta_buffer);CHKERRQ(ierr);
    
    for (p=0; p<npoints; p++) {
      eta[p] += 1.0 / c->eta_buffer[p];
    }
  }
  for (p=0; p<npoints; p++) {
    eta[p] = 1.0 / eta[p];
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaComposite_view"
static PetscErrorCode EtaComposite_view(Eta_Composite *c,PetscViewer v)
{
  PetscInt k;
  PetscErrorCode ierr;
  for (k=0; k<c->nlaws; k++) {
    PetscViewerASCIIPrintf(v,"Composite flow law [index %D]\n",k);
    PetscViewerASCIIPushTab(v);
    ierr = PWPhysicsView(c->flow_law[k],v);CHKERRQ(ierr);
    PetscViewerASCIIPopTab(v);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaComposite_destroy"
static PetscErrorCode EtaComposite_destroy(Eta_Composite *c)
{
  PetscErrorCode ierr;
  ierr = PetscFree(c->eta_buffer);CHKERRQ(ierr);
  ierr = PetscFree(c->flow_law);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeCreate"
PetscErrorCode EtaCompositeCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  Eta_Composite *creep;
  
  ierr = PWPhysicsCreate(sizeof(Eta_Composite),"eta.Composite",name,use_si,region,&phys);CHKERRQ(ierr);
  creep = (Eta_Composite*)phys->data;
  creep->nlaws = 0;
  creep->npoints = 0;
  creep->eta_buffer = NULL;
  creep->flow_law = NULL;
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,EtaComposite_evaluate);
  PWPhysicsSetView(phys,EtaComposite_view);
  PWPhysicsSetDestroy(phys,EtaComposite_destroy);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeSetFlowLaw"
PetscErrorCode EtaCompositeSetFlowLaw(PWPhysics p,PWPhysics sub)
{
  PetscErrorCode ierr;
  PetscInt k;
  Eta_Composite *creep;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"eta.Composite",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Eta_Composite*)p->data;
  
  if (p->use_si != sub->use_si) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Both flow laws must use same units");
  
  if (creep->nlaws == 0) {
    ierr = PetscMalloc1(1,&creep->flow_law);CHKERRQ(ierr);
  } else {
    PWPhysics *tmp;

    ierr = PetscMalloc1(creep->nlaws+1,&tmp);CHKERRQ(ierr);
    ierr = PetscMemzero(tmp,sizeof(PWPhysics)*(creep->nlaws+1));CHKERRQ(ierr);
    for (k=0; k<creep->nlaws; k++) {
      tmp[k] = creep->flow_law[k];
    }
    ierr = PetscFree(creep->flow_law);CHKERRQ(ierr);
    creep->flow_law = tmp;
  }
  creep->flow_law[creep->nlaws] = sub;
  creep->nlaws++;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EtaCompositeSetBufferLength"
PetscErrorCode EtaCompositeSetBufferLength(PWPhysics p,PetscInt L)
{
  PetscErrorCode ierr;
  Eta_Composite *creep;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"eta.Composite",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  creep = (Eta_Composite*)p->data;
  
  if (!creep->eta_buffer) {
    creep->npoints = L;
    ierr = PetscMalloc1(L,&creep->eta_buffer);CHKERRQ(ierr);
  } else {
    if (L != creep->npoints) {
      ierr = PetscFree(creep->eta_buffer);CHKERRQ(ierr);
      ierr = PetscMalloc1(L,&creep->eta_buffer);CHKERRQ(ierr);
      creep->npoints = L;
    }
  }
  
  PetscFunctionReturn(0);
}


/* spatial limiter in x direction */
typedef struct {
  char field_name[PETSC_MAX_PATH_LEN];
  PetscReal XLimit,XLimitWidth;
} LimiterSpatialX;

#undef __FUNCT__
#define __FUNCT__ "LimiterSpatialX_evaluate"
static PetscErrorCode LimiterSpatialX_evaluate(LimiterSpatialX *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal field[])
{
  const PetscReal *aux_coor = aux_fields[0];
  PetscInt p;

  for (p=0; p<npoints; p++) {
    if (PetscAbsReal(c->XLimitWidth)<1.0e-10) {
      if (aux_coor[2*p] > c->XLimit) {
        field[p]=0.0;
      }
    }
    else {
      field[p]=field[p]*0.5*(1.0+PetscTanhReal(-(aux_coor[2*p]-(c->XLimit))/c->XLimitWidth) );
    }   
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterSpatialX_view"
static PetscErrorCode LimiterSpatialX_view(LimiterSpatialX *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Generic spatial limiter (set field %s to zero when x>x_limit : (x_limit) %1.4e ; width (x_limit_width) %+1.4e\n",c->field_name,c->XLimit,c->XLimitWidth);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterSpatialX_set_from_options"
static PetscErrorCode LimiterSpatialX_set_from_options(const char opt_pre[],LimiterSpatialX *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-x_limit",&c->XLimit,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-x_limit_width",&c->XLimitWidth,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterSpatialXCreate"
PetscErrorCode LimiterSpatialXCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  LimiterSpatialX *limiter;
  const char *aux_names[] = { "coordinates" };

  
  ierr = PWPhysicsCreate(sizeof(LimiterSpatialX),"limiter.SpatialX",name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsSetNumberAuxiliaryFields(phys,1,aux_names);CHKERRQ(ierr); /* porosity */

  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,LimiterSpatialX_evaluate);
  PWPhysicsSetView(phys,LimiterSpatialX_view);
  PWPhysicsSetSetFromOptions(phys,LimiterSpatialX_set_from_options);
  
  limiter = (LimiterSpatialX*)phys->data;
  limiter->field_name[0] = '\0';
  if (name) {
    ierr = PetscSNPrintf(limiter->field_name,PETSC_MAX_PATH_LEN-1,"%s",name);CHKERRQ(ierr);
  }
  limiter->XLimit = PETSC_MAX_REAL; // +infinity
  limiter->XLimitWidth = 1.0e-3;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterSpatialXSetParams"
PetscErrorCode LimiterSpatialXSetParams(PWPhysics p,const char fieldname[],PetscReal XLimit)
{
  PetscErrorCode ierr;
  LimiterSpatialX *limiter;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"limiter.SpatialX",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  limiter = (LimiterSpatialX*)p->data;
  if (fieldname) {
    ierr = PetscSNPrintf(limiter->field_name,PETSC_MAX_PATH_LEN-1,"%s",fieldname);CHKERRQ(ierr);
  }
  if (XLimit != PETSC_DEFAULT) limiter->XLimit = XLimit;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EffectivePermeabilityLimiterSpatialXCreate"
PetscErrorCode EffectivePermeabilityLimiterSpatialXCreate(PetscBool use_si,PetscInt region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  
  ierr = LimiterSpatialXCreate(use_si,region,"default",&phys);CHKERRQ(ierr);
  ierr = LimiterSpatialXSetParams(phys,"EffectivePermeability",PETSC_MAX_REAL);CHKERRQ(ierr);
  *p = phys;
  PetscFunctionReturn(0);
}


/* constant */
typedef struct {
  char field_name[PETSC_MAX_PATH_LEN];
  PetscReal min,max;
} LimiterMinMax;

#undef __FUNCT__
#define __FUNCT__ "LimiterMinMax_evaluate"
static PetscErrorCode LimiterMinMax_evaluate(LimiterMinMax *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal field[])
{
  PetscInt p;
/*
  for (p=0; p<npoints; p++) {
    if (field[p] < c->min) { field[p] = c->min; }
    if (field[p] > c->max) { field[p] = c->max; }
  }
*/
  for (p=0; p<npoints; p++) {
    PetscReal havg;
    havg = (1.0/field[p] + 1.0/c->max);
    field[p] = 1.0/havg;
  }
  for (p=0; p<npoints; p++) {
    if (field[p] < c->min) { field[p] = c->min; }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterMinMax_view"
static PetscErrorCode LimiterMinMax_view(LimiterMinMax *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Generic min/max limiter (field) %s : (min) %+1.4e : (max) %+1.4e\n",c->field_name,c->min,c->max);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterMinMax_set_from_options"
static PetscErrorCode LimiterMinMax_set_from_options(const char opt_pre[],LimiterMinMax *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-min",&c->min,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-max",&c->max,NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterMinMaxCreate"
PetscErrorCode LimiterMinMaxCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  LimiterMinMax *limiter;
  
  ierr = PWPhysicsCreate(sizeof(LimiterMinMax),"limiter.MinMax",name,use_si,region,&phys);CHKERRQ(ierr);
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,LimiterMinMax_evaluate);
  PWPhysicsSetView(phys,LimiterMinMax_view);
  PWPhysicsSetSetFromOptions(phys,LimiterMinMax_set_from_options);
  
  limiter = (LimiterMinMax*)phys->data;
  limiter->field_name[0] = '\0';
  if (name) {
    ierr = PetscSNPrintf(limiter->field_name,PETSC_MAX_PATH_LEN-1,"%s",name);CHKERRQ(ierr);
  }
  limiter->min = PETSC_MIN_REAL;
  limiter->max = PETSC_MAX_REAL;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterMinMaxSetParams"
PetscErrorCode LimiterMinMaxSetParams(PWPhysics p,const char fieldname[],PetscReal min,PetscReal max)
{
  PetscErrorCode ierr;
  LimiterMinMax *limiter;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"limiter.MinMax",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  limiter = (LimiterMinMax*)p->data;
  if (fieldname) {
    ierr = PetscSNPrintf(limiter->field_name,PETSC_MAX_PATH_LEN-1,"%s",fieldname);CHKERRQ(ierr);
  }
  if (min != PETSC_DEFAULT) limiter->min = min;
  if (max != PETSC_DEFAULT) limiter->max = max;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterMinMaxGetParams"
PetscErrorCode LimiterMinMaxGetParams(PWPhysics p,PetscReal *min,PetscReal *max)
{
  PetscErrorCode ierr;
  LimiterMinMax *limiter;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"limiter.MinMax",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  limiter = (LimiterMinMax*)p->data;
  if (min) { *min = limiter->min; }
  if (max) { *max = limiter->max; }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ShearViscosityLimiterMinMaxCreate"
PetscErrorCode ShearViscosityLimiterMinMaxCreate(PetscBool use_si,PetscInt region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  
  ierr = LimiterMinMaxCreate(use_si,region,"default",&phys);CHKERRQ(ierr);
  ierr = LimiterMinMaxSetParams(phys,"shearViscosity",1.0e-20,PETSC_MAX_REAL);CHKERRQ(ierr);
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BulkViscosityLimiterMinMaxCreate"
PetscErrorCode BulkViscosityLimiterMinMaxCreate(PetscBool use_si,PetscInt region,PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  
  ierr = LimiterMinMaxCreate(use_si,region,"default",&phys);CHKERRQ(ierr);
  ierr = LimiterMinMaxSetParams(phys,"bulkViscosity",1.0e-20,PETSC_MAX_REAL);CHKERRQ(ierr);
  *p = phys;
  PetscFunctionReturn(0);
}

/* radial spatial limiter */
typedef struct {
  char field_name[PETSC_MAX_PATH_LEN];
  PetscReal origin[2],radius,reduction_factor,alpha;
} Limiter_RadialCtx;

#undef __FUNCT__
#define __FUNCT__ "LimiterRadial_evaluate"
static PetscErrorCode LimiterRadial_evaluate(Limiter_RadialCtx *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal field[])
{
  const PetscReal *aux_coor = aux_fields[0];
  PetscInt p;
  PetscReal radius2 = c->radius * c->radius;
  
  for (p=0; p<npoints; p++) {
    PetscReal r2;
    
    r2 = (aux_coor[2*p+0] - c->origin[0])*(aux_coor[2*p+0] - c->origin[0]) + (aux_coor[2*p+1] - c->origin[1])*(aux_coor[2*p+1] - c->origin[1]);
    
    if (r2 <= radius2) { /* apply limiter */
      PetscReal r2_scaled,reference,scale_factor,delta;
      
      r2_scaled = r2 / radius2;
      reference = 0.5 * (PetscTanhReal(c->alpha * (r2_scaled - 0.5)) + 1.0);
      
      delta = 1.0 - c->reduction_factor;
      scale_factor = reference * delta + c->reduction_factor;
      
      field[p] = field[p] * scale_factor;
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterRadial_view"
static PetscErrorCode LimiterRadial_view(Limiter_RadialCtx *c,PetscViewer v)
{
  PetscViewerASCIIPrintf(v,"Radial spatial limiter : reduces field %s by factor %+1.2e over radius %+1.2e centered at (%+1.2e,%+1.2e)\n",c->field_name,c->reduction_factor,c->radius,c->origin[0],c->origin[1]);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterRadial_set_from_options"
static PetscErrorCode LimiterRadial_set_from_options(const char opt_pre[],Limiter_RadialCtx *c)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-r",&c->radius,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-x",&c->origin[0],NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-y",&c->origin[1],NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-fac",&c->reduction_factor,NULL);CHKERRQ(ierr);
  if (c->reduction_factor > 1.0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"-fac must be <= 1.0");
  if (c->reduction_factor < 0.0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"-fac must be >= 0.0");
  ierr = PetscOptionsGetReal(NULL,opt_pre,"-alpha",&c->alpha,NULL);CHKERRQ(ierr);
  if (c->alpha < 5.0) PetscPrintf(PETSC_COMM_WORLD,"[warning] LimiterRadial recommended -alpha < 5.0, otherwise your target reduction factor will not be satisfied at the origina\n");
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterRadialCreate"
PetscErrorCode LimiterRadialCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  Limiter_RadialCtx *limiter;
  const char *aux_names[] = { "coordinates" };
  
  
  ierr = PWPhysicsCreate(sizeof(Limiter_RadialCtx),"limiter.Radial",name,use_si,region,&phys);CHKERRQ(ierr);
  ierr = PWPhysicsSetNumberAuxiliaryFields(phys,1,aux_names);CHKERRQ(ierr); /* porosity */
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,LimiterRadial_evaluate);
  PWPhysicsSetView(phys,LimiterRadial_view);
  PWPhysicsSetSetFromOptions(phys,LimiterRadial_set_from_options);
  
  limiter = (Limiter_RadialCtx*)phys->data;
  limiter->field_name[0] = '\0';
  if (name) {
    ierr = PetscSNPrintf(limiter->field_name,PETSC_MAX_PATH_LEN-1,"%s",name);CHKERRQ(ierr);
  }
  limiter->radius           = PETSC_MAX_REAL; // +infinity
  limiter->origin[0]        = PETSC_MAX_REAL;
  limiter->origin[1]        = PETSC_MAX_REAL;
  limiter->reduction_factor = 1.0;
  limiter->alpha            = 5.0;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterRadialSetParams"
PetscErrorCode LimiterRadialSetParams(PWPhysics p,const char fieldname[],PetscReal alpha,PetscReal rfactor,PetscReal radius,PetscReal origin[])
{
  PetscErrorCode ierr;
  Limiter_RadialCtx *limiter;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"limiter.Radial",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  limiter = (Limiter_RadialCtx*)p->data;
  if (fieldname) {
    ierr = PetscSNPrintf(limiter->field_name,PETSC_MAX_PATH_LEN-1,"%s",fieldname);CHKERRQ(ierr);
  }
  limiter->radius = radius;
  limiter->alpha = alpha;
  limiter->reduction_factor = rfactor;
  limiter->origin[0] = origin[0];
  limiter->origin[1] = origin[1];
  PetscFunctionReturn(0);
}

/* Composite flow law */
typedef struct {
  PetscInt nlaws;
  PetscInt npoints;
  PWPhysics *limiter;
} Limiter_Composite;

#undef __FUNCT__
#define __FUNCT__ "LimiterComposite_evaluate"
static PetscErrorCode LimiterComposite_evaluate(Limiter_Composite *c,PetscInt npoints,PetscInt naux,PetscReal *aux_fields[],PetscReal field[])
{
  PetscInt k;
  PetscErrorCode ierr;
  
  for (k=0; k<c->nlaws; k++) {
    ierr = PWPhysicsEvaluate(c->limiter[k],npoints,field);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterComposite_view"
static PetscErrorCode LimiterComposite_view(Limiter_Composite *c,PetscViewer v)
{
  PetscInt k;
  PetscErrorCode ierr;
  for (k=0; k<c->nlaws; k++) {
    PetscViewerASCIIPrintf(v,"Composite limiter [index %D]\n",k);
    PetscViewerASCIIPushTab(v);
    ierr = PWPhysicsView(c->limiter[k],v);CHKERRQ(ierr);
    PetscViewerASCIIPopTab(v);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterComposite_destroy"
static PetscErrorCode LimiterComposite_destroy(Limiter_Composite *c)
{
  PetscErrorCode ierr;
  ierr = PetscFree(c->limiter);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterCompositeCreate"
PetscErrorCode LimiterCompositeCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  Limiter_Composite *ctx;
  
  ierr = PWPhysicsCreate(sizeof(Limiter_Composite),"limiter.Composite",name,use_si,region,&phys);CHKERRQ(ierr);
  ctx = (Limiter_Composite*)phys->data;
  ctx->nlaws = 0;
  ctx->npoints = 0;
  ctx->limiter = NULL;
  
  // set methods for evaluation and view
  PWPhysicsSetEvaluate(phys,LimiterComposite_evaluate);
  PWPhysicsSetView(phys,LimiterComposite_view);
  PWPhysicsSetDestroy(phys,LimiterComposite_destroy);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LimiterCompositeSetLaw"
PetscErrorCode LimiterCompositeSetLaw(PWPhysics p,PWPhysics sub)
{
  PetscErrorCode ierr;
  PetscInt k;
  Limiter_Composite *ctx;
  PetscBool same;
  
  if (!p) PetscFunctionReturn(0);
  ierr = PWPhysicsTypeCompare(p,"limiter.Composite",&same);CHKERRQ(ierr);
  if (!same) PetscFunctionReturn(0);
  ctx = (Limiter_Composite*)p->data;
  
  if (p->use_si != sub->use_si) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Both flow laws must use same units");
  
  if (ctx->nlaws == 0) {
    ierr = PetscMalloc1(1,&ctx->limiter);CHKERRQ(ierr);
  } else {
    PWPhysics *tmp;
    
    ierr = PetscMalloc1(ctx->nlaws+1,&tmp);CHKERRQ(ierr);
    ierr = PetscMemzero(tmp,sizeof(PWPhysics)*(ctx->nlaws+1));CHKERRQ(ierr);
    for (k=0; k<ctx->nlaws; k++) {
      tmp[k] = ctx->limiter[k];
    }
    ierr = PetscFree(ctx->limiter);CHKERRQ(ierr);
    ctx->limiter = tmp;
  }
  ctx->limiter[ctx->nlaws] = sub;
  ctx->nlaws++;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaDiffusionCreepCreate"
PetscErrorCode ZetaDiffusionCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal A,E;
  PWPhysics phys;
  Generic_DiffusionCreep *creep;
  
  ierr = DiffusionCreepCreate_Generic("zeta",name,use_si,region,&phys);CHKERRQ(ierr);
  creep = (Generic_DiffusionCreep*)phys->data;
  // parameters
  A = 1.32043 * 1.0e8;
  E = 335.0 * 1.0e2;
  creep->A_ = A;
  creep->E_ = E;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaDiffusionCreepSetParams"
PetscErrorCode ZetaDiffusionCreepSetParams(PWPhysics p,PetscReal A,PetscReal E)
{
  PetscErrorCode ierr;
  if (!p) PetscFunctionReturn(0);
  ierr = DiffusionCreepSetParams_Generic(p,"zeta",A,E);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaDislocationCreepCreate"
PetscErrorCode ZetaDislocationCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p)
{
  PetscErrorCode ierr;
  PetscReal A,E,n;
  PWPhysics phys;
  Generic_DislocationCreep *creep;

  ierr = DislocationCreepCreate_Generic("zeta",name,use_si,region,&phys);CHKERRQ(ierr);

  creep = (Generic_DislocationCreep*)phys->data;
  // parameters
  A = 1.32043 * 1.0e8;
  E = 335.0 * 1.0e2;
  n = 1.0;
  creep->A_ = A;
  creep->E_ = E;
  creep->n_ = n;
  ierr = PWPhysicsSetFromOptions(phys);CHKERRQ(ierr);
  
  *p = phys;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ZetaDislocationCreepSetParams"
PetscErrorCode ZetaDislocationCreepSetParams(PWPhysics p,PetscReal A,PetscReal E,PetscReal n)
{
  PetscErrorCode ierr;
  if (!p) PetscFunctionReturn(0);
  ierr = DislocationCreepSetParams_Generic(p,"zeta",A,E,n);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
