
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <quadrature.h>
#include <coefficient.h>
#include <element_container.h>

#include <tpctx.h>
#include <mec.h>

#undef __FUNCT__
#define __FUNCT__ "m1"
PetscErrorCode m1(void)
{
  MCtx m;
  PetscErrorCode ierr;
  
  ierr = MechanicsContextCreate(&m);CHKERRQ(ierr);
  ierr = MechanicsSetUp(m,"meshes/examples/ref.1",2);CHKERRQ(ierr);
  ierr = MechanicsContextDestroy(&m);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "e1"
PetscErrorCode e1(void)
{
  ECtx e;
  PetscErrorCode ierr;
  
  ierr = EnergyContextCreate(&e);CHKERRQ(ierr);
  ierr = EnergySetUp(e,"meshes/examples/ref.1",2);CHKERRQ(ierr);
  ierr = EnergyContextDestroy(&e);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ScalarFunction_1"
PetscErrorCode ScalarFunction_1(PetscScalar pos[],PetscInt dof,PetscScalar val[],void *ctx)
{
  val[0] = PetscSinReal(PETSC_PI * pos[0]) * PetscCosReal(PETSC_PI * pos[1]) + 3.33*1.0e-1;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ScalarFunction_2"
PetscErrorCode ScalarFunction_2(PetscScalar pos[],PetscInt dof,PetscScalar val[],void *ctx)
{
  val[0] = PetscExpReal(4.4 * pos[0] * pos[1]) + 13.33*1.0e-1;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VectorFunction_1"
PetscErrorCode VectorFunction_1(PetscScalar pos[],PetscInt dof,PetscScalar val[],void *ctx)
{
  
  val[0] = pos[0] * pos[0] + pos[1] * PetscExpReal(pos[1]+0.1) + 17.6*1.0e-1;
  val[1] = pos[0] * pos[1] + PetscSinReal(PETSC_PI * pos[0]) + 4.9*1.0e-1;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VectorFunction_2"
PetscErrorCode VectorFunction_2(PetscScalar pos[],PetscInt dof,PetscScalar val[],void *ctx)
{
  
  val[0] = 1.0;
  val[1] = 1.2;
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "transfers"
PetscErrorCode transfers(void)
{
  PetscErrorCode ierr;
  TPCtx ctx;
  
  ierr = TPContextCreate(&ctx);CHKERRQ(ierr);
  ierr = TPSetUp_Default(ctx,"meshes/examples/ref.1",2,3);CHKERRQ(ierr);
  
  
  /* insert scalar by region */
  ierr = DMTetVecTraverseByRegionId(ctx->energy->dm_e,ctx->energy->X,2,-6,ScalarFunction_1,NULL);CHKERRQ(ierr);
  {
    const char *fields[] = { "insert_T" };
    char       prefix[PETSC_MAX_PATH_LEN];

    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_",ctx->output_path,fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->energy->dm_e,1,&ctx->energy->X,fields,prefix);CHKERRQ(ierr);
  }
  
  /* insert vector by region */
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_s_bc,1,-6,VectorFunction_1,NULL);CHKERRQ(ierr);
  {
    const char *fields[] = { "insert_vs" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_",ctx->output_path,fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->mechanics->dm_v,1,&ctx->mechanics->v_s_bc,fields,prefix);CHKERRQ(ierr);
  }

  /* Q_vc : interpolate v onto dmda */

  /* I_v : inject */
  {
    Vec v_s_hat,p_hat;
    
    ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    
    ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v_hat,v_s_hat,2,-6,VectorFunction_2,NULL);CHKERRQ(ierr);
   
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_s_bc);CHKERRQ(ierr);
    
    ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  }
  {
    const char *fields[] = { "insert_vs_hat" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_",ctx->output_path,fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->mechanics->dm_v,1,&ctx->mechanics->v_s_bc,fields,prefix);CHKERRQ(ierr);
  }
  
  /* W_ve : interp */
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,1,-6,VectorFunction_1,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,2,-6,VectorFunction_2,NULL);CHKERRQ(ierr);
  ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_bar,ctx->energy->v_bar_e);CHKERRQ(ierr);
  
  {
    const char *fields[] = { "interp_v_bar" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_",ctx->output_path,fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->energy->dm_e,1,&ctx->energy->v_bar_e,fields,prefix);CHKERRQ(ierr);
  }
  
  
  /* I_ve : inject */
  VecZeroEntries(ctx->energy->X);
  ierr = DMTetVecTraverseByRegionId(ctx->energy->dm_e,ctx->energy->X,1,-6,ScalarFunction_1,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(ctx->energy->dm_e,ctx->energy->X,2,-6,ScalarFunction_2,NULL);CHKERRQ(ierr);
  
  ierr = InterpT_FromTauEToTauM(ctx,ctx->energy->X,ctx->mechanics->T,ctx->mechanics->T_hat);CHKERRQ(ierr);

  {
    const char *fields[] = { "insert_T_hat" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_",ctx->output_path,fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(ctx->mechanics->dm_ve_hat,1,&ctx->mechanics->T_hat,fields,prefix);CHKERRQ(ierr);
  }

  
  ierr = TPContextDestroy(&ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "transfers_T"
PetscErrorCode transfers_T(void)
{
  PetscErrorCode ierr;
  DM dm,dm_hat;
  Mat inject_1;
  Vec T,That;
  VecScatter inject_2,inject_3;
  IS isTo,isFrom;
  PetscInt nregions = 1;
  PetscInt regionlist[] = {2};
  
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"meshes/examples/ref.1",
                                            1,DMTET_CWARP_AND_BLEND,2,&dm);CHKERRQ(ierr);
  DMCreateGlobalVector(dm,&T);

  /* Extract a sub velocity-pressure meshes from the wedge domain */
  ierr = DMTetCreateSubmeshByRegions(dm,nregions,regionlist,&dm_hat);CHKERRQ(ierr);
  DMCreateGlobalVector(dm_hat,&That);

  /* I_v : v_s = I_v v_s_hat */
  ierr = DMCreateInjection(dm,dm_hat,&inject_1);CHKERRQ(ierr);

  ierr = DMTetVecTraverseByRegionId(dm_hat,That,2,-6,ScalarFunction_2,NULL);CHKERRQ(ierr);
  {
    const char *fields[] = { "That" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_","./",fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_hat,1,&That,fields,prefix);CHKERRQ(ierr);
  }

  {
    VecScatter scat;
    
    ierr = MatScatterGetVecScatter(inject_1,&scat);CHKERRQ(ierr);
    
    ierr = VecScatterBegin(scat,That,T,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
    ierr = VecScatterEnd(scat,That,T,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  }
  {
    const char *fields[] = { "T" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_","./",fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm,1,&T,fields,prefix);CHKERRQ(ierr);
  }
  
  ierr = DMTetCreateInjectionScalarIS(dm,dm_hat,&isTo,&isFrom);CHKERRQ(ierr);

  ierr = VecScatterCreate(T,isTo,That,isFrom,&inject_2);CHKERRQ(ierr);
  ierr = ISDestroy(&isFrom);CHKERRQ(ierr);
  ierr = ISDestroy(&isTo);CHKERRQ(ierr);

  ierr = VecZeroEntries(T);CHKERRQ(ierr);
  ierr = VecScatterBegin(inject_2,That,T,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(inject_2,That,T,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  
  {
    const char *fields[] = { "T2" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_","./",fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm,1,&T,fields,prefix);CHKERRQ(ierr);
  }

  /* reversed */
  ierr = DMTetCreateInjectionScalarIS(dm_hat,dm,&isTo,&isFrom);CHKERRQ(ierr);
  ierr = VecScatterCreate(That,isTo,T,isFrom,&inject_3);CHKERRQ(ierr);
  ierr = ISDestroy(&isFrom);CHKERRQ(ierr);
  ierr = ISDestroy(&isTo);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(T);CHKERRQ(ierr);
  ierr = VecScatterBegin(inject_3,That,T,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(inject_3,That,T,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  
  {
    const char *fields[] = { "T3" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_","./",fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm,1,&T,fields,prefix);CHKERRQ(ierr);
  }

  ierr = VecScatterDestroy(&inject_3);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&inject_2);CHKERRQ(ierr);
  ierr = MatDestroy(&inject_1);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_hat);CHKERRQ(ierr);
  ierr = VecDestroy(&T);CHKERRQ(ierr);
  ierr = VecDestroy(&That);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "transfers_Tu"
PetscErrorCode transfers_Tu(void)
{
  PetscErrorCode ierr;
  DM dm,dm_hat,dmv,dmv_hat;
  Vec T,That;
  VecScatter inject_2;
  IS isTo,isFrom;
  PetscInt nregions = 1;
  PetscInt regionlist[] = {2};
  
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,"meshes/examples/ref.1",
                                            2,DMTET_CWARP_AND_BLEND,2,&dmv);CHKERRQ(ierr);
  
  /* Extract a sub velocity-pressure meshes from the wedge domain */
  ierr = DMTetCreateSubmeshByRegions(dmv,nregions,regionlist,&dmv_hat);CHKERRQ(ierr);

  ierr = DMTetCreateSharedSpace(dmv,1,&dm);
  ierr = DMTetCreateSharedSpace(dmv_hat,1,&dm_hat);
  
  ierr = DMCreateGlobalVector(dm,&T);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_hat,&That);CHKERRQ(ierr);
  
  ierr = DMTetVecTraverseByRegionId(dm_hat,That,2,-6,ScalarFunction_2,NULL);CHKERRQ(ierr);

  /* scatter wedge -> omega */
  ierr = DMTetCreateInjectionScalarIS(dmv,dmv_hat,&isTo,&isFrom);CHKERRQ(ierr);
  
  ierr = VecScatterCreate(T,isTo,That,isFrom,&inject_2);CHKERRQ(ierr);
  ierr = ISDestroy(&isFrom);CHKERRQ(ierr);
  ierr = ISDestroy(&isTo);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(T);CHKERRQ(ierr);
  ierr = VecScatterBegin(inject_2,That,T,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  ierr = VecScatterEnd(inject_2,That,T,INSERT_VALUES,SCATTER_REVERSE);CHKERRQ(ierr);
  
  {
    const char *fields[] = { "T2" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_","./",fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm,1,&T,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dm,1,&T,fields,prefix);CHKERRQ(ierr);
  }
  
  ierr = VecZeroEntries(T);CHKERRQ(ierr);
  ierr = VecZeroEntries(That);CHKERRQ(ierr);
  
  ierr = DMTetVecTraverseByRegionId(dm,T,2,-6,ScalarFunction_2,NULL);CHKERRQ(ierr);

  ierr = VecScatterBegin(inject_2,T,That,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(inject_2,T,That,INSERT_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  
  {
    const char *fields[] = { "T3" };
    char       prefix[PETSC_MAX_PATH_LEN];
    
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/%s_","./",fields[0]);CHKERRQ(ierr);
    ierr = DMTetViewFields_VTU(dm_hat,1,&That,fields,prefix);CHKERRQ(ierr);
    ierr = DMTetViewFields_PVTU(dm_hat,1,&That,fields,prefix);CHKERRQ(ierr);
  }
  
  ierr = VecScatterDestroy(&inject_2);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_hat);CHKERRQ(ierr);
  ierr = DMDestroy(&dmv);CHKERRQ(ierr);
  ierr = DMDestroy(&dmv_hat);CHKERRQ(ierr);
  ierr = VecDestroy(&T);CHKERRQ(ierr);
  ierr = VecDestroy(&That);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}



int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  /*
  ierr = m1();CHKERRQ(ierr);
  ierr = e1();CHKERRQ(ierr);
  */
  //ierr = transfers();CHKERRQ(ierr);
  ierr = transfers_T();CHKERRQ(ierr);
  //ierr = transfers_Tu();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

