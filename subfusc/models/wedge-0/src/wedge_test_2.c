
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <petscutils.h>
#include <pde.h>
#include <pdehelmholtz.h>
#include <pdestokesdarcy2field.h>
#include <dmbcs.h>
#include <dmsl.h>
#include <quadrature.h>
#include <coefficient.h>
#include <element_container.h>
#include <paraview_utils.h>
#include <pointwise_mech_coeff.h>

#include <tpctx.h>
#include <mec.h>


#undef __FUNCT__
#define __FUNCT__ "steady_state"
PetscErrorCode steady_state(void)
{
  PetscErrorCode ierr;
  TPCtx ctx;
  PetscReal E,L,V;
  
  ierr = TPContextCreate(&ctx);CHKERRQ(ierr);
  ierr = TPSetUp_Default(ctx,"meshes/examples/ref.1",2,3);CHKERRQ(ierr);
  ctx->slab_region_id = 1;
  ctx->wedge_region_id = 2;
  ctx->plate_region_id = 3;
  ctx->energy->wedge_region_id = 2;
  
  /* constant material properties */
  E = 1.0e21; /* viscosity scale */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  ierr = TPCtxSetDimensionalScales_ELV(ctx,E,L,V);CHKERRQ(ierr);
  ctx->kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  ctx->rho_s  = 3300.0;
  ctx->rho_l  = 2800.0;
  ctx->g[0] = 0.0;
  ctx->g[1] = 9.81;
  ctx->rho_plate = 3100.0;
  ierr = TPCtxApplyScaling(ctx);CHKERRQ(ierr);
  
  //ctx->phi_c_type = DIST_PT;
  
  ierr = MechanicsSetDirichletBC(ctx->mechanics,ctx);CHKERRQ(ierr);
  ierr = MechanicsSetCoefficientEvaluators(ctx->mechanics,ctx);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
  ierr = MechanicsView(ctx->mechanics,ctx->output_path,0);CHKERRQ(ierr);

  ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
  ierr = MechanicsView(ctx->mechanics,ctx->output_path,1);CHKERRQ(ierr);

  ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
  ierr = MechanicsView(ctx->mechanics,ctx->output_path,2);CHKERRQ(ierr);
  
  /* interpolate velocity from mechanical mesh onto energy mesh */
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,3,-6,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,1,-6,slab_uv,NULL);CHKERRQ(ierr);
  {
    Vec v_s_hat,p_hat;
    
    ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_bar);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  }
  ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_bar,ctx->energy->v_bar_e);CHKERRQ(ierr);
  
  /* config and solve thermal problem */
  ierr = EnergySetDirichletBC(ctx->energy,ctx);CHKERRQ(ierr);
  ierr = EnergySetCoefficientEvaluators_SteadyState(ctx->energy,ctx);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(ctx->energy->pde,ctx->energy->snes);CHKERRQ(ierr);
  ierr = EnergyView(ctx->energy,ctx->output_path,0);CHKERRQ(ierr);

  ierr = TPContextDestroy(&ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "time_dep"
PetscErrorCode time_dep(void)
{
  PetscErrorCode ierr;
  TPCtx ctx;
  PetscReal E,L,V;
  PetscInt k;
  
  ierr = TPContextCreate(&ctx);CHKERRQ(ierr);
  ierr = TPSetUp_Default(ctx,"meshes/examples/ref.1",2,2);CHKERRQ(ierr);
  ctx->slab_region_id = 1;
  ctx->wedge_region_id = 2;
  ctx->plate_region_id = 3;
  ctx->energy->wedge_region_id = 2;
  
  /* constant material properties */
  E = 1.0e21; /* viscosity scale */
  L = 600.0 * 1.0e3; /* length scale 600 km */
  V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */
  ierr = TPCtxSetDimensionalScales_ELV(ctx,E,L,V);CHKERRQ(ierr);
  ctx->kappa = 0.7272 * 1.0e-6; /* m^2 s^{-1} */
  ctx->rho_s  = 3300.0;
  ctx->rho_l  = 2800.0;
  ctx->g[0] = 0.0;
  ctx->g[1] = 9.81;
  ctx->rho_plate = 3100.0;
  ierr = TPCtxApplyScaling(ctx);CHKERRQ(ierr);
  
  ierr = MechanicsSetDirichletBC(ctx->mechanics,ctx);CHKERRQ(ierr);
  ierr = MechanicsSetCoefficientEvaluators(ctx->mechanics,ctx);CHKERRQ(ierr);
  
  ierr = PDESNESSolve(ctx->mechanics->pde,ctx->mechanics->snes);CHKERRQ(ierr);
  ierr = MechanicsView(ctx->mechanics,ctx->output_path,0);CHKERRQ(ierr);
  
  /* interpolate velocity from mechanical mesh onto energy mesh */
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,3,-6,plate_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetVecTraverseByRegionId(ctx->mechanics->dm_v,ctx->mechanics->v_bar,1,-6,slab_uv,NULL);CHKERRQ(ierr);
  {
    Vec v_s_hat,p_hat;
    
    ierr = DMCompositeGetAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
    ierr = InjectV_FromOmegaWedgeToOmega(ctx,v_s_hat,ctx->mechanics->v_bar);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(ctx->mechanics->dm_m_hat,ctx->mechanics->X_hat,&v_s_hat,&p_hat);CHKERRQ(ierr);
  }
  ierr = InterpV_FromTauMOmegaToTauEOmega(ctx,ctx->mechanics->v_bar,ctx->energy->v_bar_e);CHKERRQ(ierr);
  
  /* config and solve thermal problem */
  ierr = EnergySetTimestep(ctx->energy,0.05);CHKERRQ(ierr);

  ierr = EnergySetDirichletBC(ctx->energy,ctx);CHKERRQ(ierr);
  
  ierr = EnergySetCoefficientEvaluators_TimeDependent(ctx->energy,ctx);CHKERRQ(ierr);

  ierr = VecSet(ctx->energy->X,273.0);CHKERRQ(ierr);
  ierr = BCListInsert(ctx->energy->bc_T,ctx->energy->X);CHKERRQ(ierr);

  ierr = VecScale(ctx->energy->v_bar_e,ctx->energy->delta_t);CHKERRQ(ierr);
  
  ierr = EnergyView(ctx->energy,ctx->output_path,0);CHKERRQ(ierr);
  
  for (k=1; k<=10; k++) {
    PetscPrintf(PETSC_COMM_WORLD,"Step %.4D\n",k);

    ierr = PDEUpdateLocalSolution(ctx->energy->pde);CHKERRQ(ierr);
    ierr = VecCopy(ctx->energy->X,ctx->energy->X_prev);CHKERRQ(ierr); /* x --> x_old */
    ierr = BCListInsert(ctx->energy->bc_T,ctx->energy->X_prev);CHKERRQ(ierr);
    
    ierr = PDESNESSolve(ctx->energy->pde,ctx->energy->snes);CHKERRQ(ierr);

    ierr = EnergyView(ctx->energy,ctx->output_path,k);CHKERRQ(ierr);
  }
  
  ierr = TPContextDestroy(&ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = steady_state();CHKERRQ(ierr);
  ierr = time_dep();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

