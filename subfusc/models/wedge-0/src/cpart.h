
#if !defined(__CPart_H)
#define __CPart_H

#include <petsc.h>

typedef struct _p_CPart *CPart;

struct _p_CPart {
  PetscReal range_T[2];
  PetscReal range_P[2];
  PetscReal range_CH2O[2];
  PetscReal range_CFT[2];
  PetscReal range_CRF[2];
  
  PetscErrorCode (*evaluate)(const PetscReal*, const PetscReal*,
                             const PetscInt,
                             const PetscReal*,
                             PetscReal*,PetscReal*,
                             PetscReal*,
                             PetscBool*,
                             void*);
  PetscErrorCode (*get_num_components)(int*);
  PetscErrorCode (*get_component_name)(int,const char**);
  PetscErrorCode (*get_component_phase_activity)(int,PetscBool*,PetscBool*);
};

PetscErrorCode CPartCreate(CPart *_ctx);
PetscErrorCode CPartDestroy(CPart *_ctx);

#endif
