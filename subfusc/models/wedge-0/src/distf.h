
#if !defined(__DistF_H)
#define __DistF_H

#include <petsc.h>

typedef struct _p_DistF *DistF;

struct _p_DistF {
  PetscReal p_min,p_max,Crf_min,Crf_max,CH2O_min,CH2O_max;
  PetscReal a00,a10,a01,a20,a11;
  PetscReal b00,b10,b01,b20,b11;
  char info[PETSC_MAX_PATH_LEN];
  PetscBool check_bounds;
  PetscErrorCode (*evaluate)(const PetscReal*, const PetscReal*,
                   const PetscInt,
                   const PetscReal*,
                   PetscReal*,PetscReal*,
                   PetscReal*,
                   PetscBool*,
                   void*);
  PetscErrorCode (*get_num_components)(int*);
  PetscErrorCode (*get_component_name)(int,const char**);
  PetscErrorCode (*get_component_phase_activity)(int,PetscBool*,PetscBool*);
};

PetscErrorCode DistFCreate(DistF *ctx);
PetscErrorCode DistFDestroy(DistF *ctx);
PetscErrorCode DistFView(DistF ctx);
PetscErrorCode DistFInit(DistF ctx);
PetscErrorCode DistFComputeKft(DistF ctx,PetscReal T,PetscReal P,PetscReal C_H2O,PetscReal *Kft);
PetscErrorCode DistFComputePorosity(DistF ctx,PetscReal T,PetscReal P,PetscReal C_H2O,PetscReal C_rf,PetscReal *phi,PetscBool *isvalid);
PetscErrorCode DistFSetCheckBounds(DistF ctx,PetscBool value);
PetscErrorCode DistFGetCheckBounds(DistF ctx,PetscBool *value);

#endif
