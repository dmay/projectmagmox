
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petscsnes.h>

#include <petscdmtet.h>
#include <pde.h>
#include <pdestokes.h>
#include <pdestokesdarcy2field.h>
#include <dmbcs.h>
#include <quadrature.h>
#include <coefficient.h>

#include <mec.h>
#include <tpctx.h>

#undef __FUNCT__
#define __FUNCT__ "MechanicsContextCreate"
PetscErrorCode MechanicsContextCreate(MCtx *_m)
{
  MCtx m;
  PetscErrorCode ierr;
  ierr = PetscMalloc1(1,&m);CHKERRQ(ierr);
  ierr = PetscMemzero(m,sizeof(struct _p_MCtx));CHKERRQ(ierr);
  SFUSCSetClassId(m,MECHANICS_CLASSID);
  m->viewer_active = PETSC_TRUE;
  *_m = m;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsContextDestroy"
PetscErrorCode MechanicsContextDestroy(MCtx *_m)
{
  MCtx m;
  PetscErrorCode ierr;
  if (!_m) PetscFunctionReturn(0);
  m = *_m;

  ierr = DMDestroy(&m->dm_v);CHKERRQ(ierr);
  ierr = DMDestroy(&m->dm_p);CHKERRQ(ierr);
  ierr = DMDestroy(&m->dm_v_hat);CHKERRQ(ierr);
  ierr = DMDestroy(&m->dm_p_hat);CHKERRQ(ierr);
  ierr = DMDestroy(&m->dm_m_hat);CHKERRQ(ierr);
  ierr = DMDestroy(&m->dm_ve);CHKERRQ(ierr);
  ierr = DMDestroy(&m->dm_ve_hat);CHKERRQ(ierr);
  
  ierr = BCListDestroy(&m->bc_v_hat);CHKERRQ(ierr);
  
  ierr = VecDestroy(&m->F_hat);CHKERRQ(ierr);
  ierr = VecDestroy(&m->X_hat);CHKERRQ(ierr);
  ierr = MatDestroy(&m->J_hat);CHKERRQ(ierr);
  
  ierr = PDEDestroy(&m->pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&m->snes);CHKERRQ(ierr);
  
  /* point wise coeff */
  ierr = MechanicsCoeffContainerDestroy(&m->coeff_container);CHKERRQ(ierr);
  
  /* auxillary */
  ierr = VecDestroy(&m->T);CHKERRQ(ierr);
  ierr = VecDestroy(&m->T_hat);CHKERRQ(ierr);
  ierr = VecDestroy(&m->v_s_bc);CHKERRQ(ierr);
  ierr = VecDestroy(&m->v_l_star);CHKERRQ(ierr);
  ierr = VecDestroy(&m->v_bar);CHKERRQ(ierr);
  ierr = VecDestroy(&m->v_l_hat);CHKERRQ(ierr);
  
  ierr = PetscFree(m);CHKERRQ(ierr);
  *_m = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_MechanicsCreateDMs"
PetscErrorCode _MechanicsCreateDMs(MCtx m,const char meshfilename[],PetscInt pk)
{
  PetscErrorCode ierr;
  PetscInt nregions = 1;
  PetscInt regionlist[] = {2};

  ierr = DMTetCreatePartitionedFromTriangleWithRef(PETSC_COMM_WORLD,meshfilename,
                                   2,DMTET_CWARP_AND_BLEND,pk,&m->dm_v);CHKERRQ(ierr);
  /*
  ierr = DMTetCreatePartitionedFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,pk-1,&m->dm_p);CHKERRQ(ierr);
  */
  ierr = DMTetCreateSharedGeometry(m->dm_v,1,DMTET_CWARP_AND_BLEND,pk-1,&m->dm_p);CHKERRQ(ierr);

  //ierr = DMTetCreateSharedGeometry(m->dm_v,1,DMTET_CWARP_AND_BLEND,pk,&m->dm_ve);CHKERRQ(ierr);
  ierr = DMTetCreatePartitionedFromTriangleWithRef(PETSC_COMM_WORLD,meshfilename,
                                            1,DMTET_CWARP_AND_BLEND,pk,&m->dm_ve);CHKERRQ(ierr);
  
  /* Extract a sub velocity-pressure meshes from the wedge domain */
  ierr = DMTetCreateSubmeshByRegions(m->dm_v,nregions,regionlist,&m->dm_v_hat);CHKERRQ(ierr);
  ierr = DMTetCreateSubmeshByRegions(m->dm_p,nregions,regionlist,&m->dm_p_hat);CHKERRQ(ierr);

  //ierr = DMTetCreateSharedGeometry(m->dm_v_hat,1,DMTET_CWARP_AND_BLEND,pk,&m->dm_ve_hat);CHKERRQ(ierr);
  ierr = DMTetCreateSubmeshByRegions(m->dm_ve,nregions,regionlist,&m->dm_ve_hat);CHKERRQ(ierr);
  /* check partitions match */
  {
    PetscInt nel_v,nel_e;
    ierr = DMTetSpaceElement(m->dm_v_hat,&nel_v,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    ierr = DMTetSpaceElement(m->dm_ve_hat,&nel_e,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
    if (nel_e != nel_v) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"The partitions for velocity and temperature do not match");
  }
  
  ierr = DMTetBFacetSetup(m->dm_v_hat);CHKERRQ(ierr);
  ierr = DMTetBFacetSetup(m->dm_p_hat);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_MechanicsCreateBCs"
PetscErrorCode _MechanicsCreateBCs(MCtx m)
{
  PetscErrorCode ierr;
  
  /* Define velocity Dirichlet boundary conditions on the sub mesh */
  ierr = DMBCListCreate(m->dm_v_hat,&m->bc_v_hat);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_MechanicsCreatePDE"
PetscErrorCode _MechanicsCreatePDE(MCtx m)
{
  Coefficient bform,lform_flux;
  Quadrature quadrature_v,quadrature_s;
  PetscInt nelements_v,nelements_s;
  PetscErrorCode ierr;
  
  ierr = PDECreate(PETSC_COMM_WORLD,&m->pde);CHKERRQ(ierr);
  ierr = PDESetType(m->pde,PDESTKDCY2F);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(m->pde,"stkdcy_");CHKERRQ(ierr);
  ierr = PDEStokesDarcy2FieldSetData(m->pde,m->dm_v_hat,m->dm_p_hat,m->bc_v_hat,NULL);CHKERRQ(ierr);
  ierr = PDEStokesDarcy2FieldGetDM(m->pde,&m->dm_m);CHKERRQ(ierr);
  
  ierr = PDEStokesDarcy2FieldGetCoefficients(m->pde,&bform,NULL,NULL,&lform_flux);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(bform,&quadrature_v);CHKERRQ(ierr);
  ierr = QuadratureGetSizes(quadrature_v,NULL,NULL,NULL,&nelements_v);CHKERRQ(ierr);
  //ierr = DMTetSpaceElement(m->dm_v_hat,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature_v,nelements_v,"phi",1);CHKERRQ(ierr);
  
  ierr = CoefficientGetQuadrature(lform_flux,&quadrature_s);CHKERRQ(ierr);
  ierr = QuadratureGetSizes(quadrature_s,NULL,NULL,NULL,&nelements_s);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature_s,nelements_s,"phi",1);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature_s,nelements_s,"k*",1);CHKERRQ(ierr);
  
  ierr = PDEStokesDarcy2FieldGetDM(m->pde,&m->dm_m_hat);CHKERRQ(ierr);
  ierr = PetscObjectReference((PetscObject)m->dm_m_hat);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(m->dm_m_hat,&m->J_hat);CHKERRQ(ierr);
  ierr = MatCreateVecs(m->J_hat,&m->X_hat,&m->F_hat);CHKERRQ(ierr);
  ierr = PDESetSolution(m->pde,m->X_hat);CHKERRQ(ierr);
  {
    Vec xp[2];
    
    ierr = DMCreateLocalVector(m->dm_v_hat,&xp[0]);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(m->dm_p_hat,&xp[1]);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolutions(m->pde,2,xp);CHKERRQ(ierr);
    
    ierr = VecDestroy(&xp[0]);CHKERRQ(ierr);
    ierr = VecDestroy(&xp[1]);CHKERRQ(ierr);
  }

  /* Create and configure the two-phase flow solver */
  ierr = PDESetOptionsPrefix(m->pde,"vp_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&m->snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(m->pde,m->snes,m->F_hat,m->J_hat,m->J_hat);CHKERRQ(ierr);
  ierr = SNESSetType(m->snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(m->snes);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsSetUp"
PetscErrorCode MechanicsSetUp(MCtx m,const char meshfilename[],PetscInt pk)
{
  PetscErrorCode ierr;
  DM             dm_tmp;
  ierr = PetscOptionsGetBool(NULL,NULL,"-subfusc.mechanics.viewer_active",&m->viewer_active,NULL);CHKERRQ(ierr);
  ierr = _MechanicsCreateDMs(m,meshfilename,pk);CHKERRQ(ierr);
  ierr = _MechanicsCreateBCs(m);CHKERRQ(ierr);
  ierr = _MechanicsCreatePDE(m);CHKERRQ(ierr);
  
  /* point wise coefficient */
  ierr = MechanicsCoeffContainerCreate(&m->coeff_container);CHKERRQ(ierr);
  ierr = MechanicsCoeffContainerSetup(m->coeff_container,m);CHKERRQ(ierr);
  ierr = MechanicsCoeffContainerSetupEta(m->coeff_container,"eta.Constant",NULL);CHKERRQ(ierr);
  ierr = MechanicsCoeffContainerSetupEffectivePermability(m->coeff_container,"k.Constant",NULL);CHKERRQ(ierr);
  ierr = MechanicsCoeffContainerSetupZeta(m->coeff_container,"zeta.Constant",NULL);CHKERRQ(ierr);
  
  /* auxillary */
  ierr = DMCreateGlobalVector(m->dm_v,&m->v_s_bc);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(m->dm_v,&m->v_l_star);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(m->dm_v,&m->v_bar);CHKERRQ(ierr);

  ierr = DMTetCreateSharedSpace(m->dm_p_hat,2,&dm_tmp);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm_tmp,&m->v_l_hat);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(m->dm_ve,&m->T);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(m->dm_ve_hat,&m->T_hat);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsView"
PetscErrorCode MechanicsView(MCtx m,const char dir[],PetscInt step)
{
  PetscErrorCode ierr;
  char prefix[PETSC_MAX_PATH_LEN];
  const char *field_name_u[] = { "vs" };
  const char *field_name_p[] = { "pf" };
  Vec Xu,Xp;
  
  if (!m->viewer_active) PetscFunctionReturn(0);
  
  ierr = DMCompositeGetAccess(m->dm_m,m->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  if (dir) {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_",dir,step,field_name_u[0]);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_%s_",step,field_name_u[0]);CHKERRQ(ierr);
  }
#ifdef SUBFUSC_USE_BINARY_PV
  ierr = DMTetViewFields_VTU_b(m->dm_v_hat,1,&Xu,field_name_u,prefix);CHKERRQ(ierr);
#elif
  ierr = DMTetViewFields_VTU(m->dm_v_hat,1,&Xu,field_name_u,prefix);CHKERRQ(ierr);
#endif
  //ierr = DMTetViewFields_PVTU(m->dm_v_hat,1,&Xu,field_name_u,prefix);CHKERRQ(ierr);
  ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_%s_",step,field_name_u[0]);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU2(m->dm_v_hat,1,&Xu,field_name_u,dir,prefix,NULL);CHKERRQ(ierr);

  if (dir) {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_",dir,step,field_name_p[0]);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_%s_",step,field_name_p[0]);CHKERRQ(ierr);
  }
#ifdef SUBFUSC_USE_BINARY_PV
  ierr = DMTetViewFields_VTU_b(m->dm_p_hat,1,&Xp,field_name_p,prefix);CHKERRQ(ierr);
#elif
  ierr = DMTetViewFields_VTU(m->dm_p_hat,1,&Xp,field_name_p,prefix);CHKERRQ(ierr);
#endif
  //ierr = DMTetViewFields_PVTU(m->dm_p_hat,1,&Xp,field_name_p,prefix);CHKERRQ(ierr);
  ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_%s_",step,field_name_p[0]);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU2(m->dm_p_hat,1,&Xp,field_name_p,dir,prefix,NULL);CHKERRQ(ierr);
  
  ierr = DMCompositeRestoreAccess(m->dm_m,m->X_hat,&Xu,&Xp);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

