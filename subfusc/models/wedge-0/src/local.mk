
model—src-y.c += $(call thisdir, \
    distf.c cpart.c nonidealpart.c \
    mechanics.c \
    energy.c \
    tpctx.c \
    mechanics_bc.c \
    mechanics_coeff.c \
    scaling.c \
    energy_coeff.c \
    energy_bc.c \
    components_ic.c \
    pointwise_physics.c \
    pointwise_mech_coeff.c \
    thermodyn.c \
    compat_u_c.c \
)

model—exec-y.c += $(call thisdir, \
    wedge_test_1.c \
    wedge_test_2.c \
    wedge.c \
    pointwise_physics_test.c \
    nonidealpart_test.c \
    wedge_projection_test.c \
)

PROJ_INC += -I$(abspath $(call thisdir,.))
