
#if !defined(__PWMECH_COEFF_H)
#define __PWMECH_COEFF_H

#include <petsc.h>
#include <pointwise_physics.h>

/* === Shear viscosity coefficient evaluators === */

/* constant */
PetscErrorCode EtaConstantCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode EtaConstantSetParams(PWPhysics p,PetscReal eta0);

/* Diffusion creep flow law */
PetscErrorCode EtaDiffusionCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode EtaDiffusionCreepCreate_PvKPEPI2008(int region,PWPhysics *p);
PetscErrorCode EtaDiffusionCreepSetParams(PWPhysics p,PetscReal A,PetscReal E);

/* Dislocation creep flow law */
PetscErrorCode EtaDislocationCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode EtaDislocationCreepCreate_PvKPEPI2008(int region,PWPhysics *p);
PetscErrorCode EtaDislocationCreepSetParams(PWPhysics p,PetscReal A,PetscReal E,PetscReal n);

/* Combined Diffusion and Dislocation creep flow law */
PetscErrorCode EtaCompositeDiffDislCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode EtaCompositeDiffDislCreepCreate_PvKPEPI2008(int region,PWPhysics *p);
PetscErrorCode EtaCompositeDiffDislCreepSetParams(PWPhysics p,PetscReal Adiff,PetscReal Ediff,PetscReal Adisl,PetscReal Edisl,PetscReal ndisl);

/* Composite flow law */
PetscErrorCode EtaCompositeCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode EtaCompositeSetFlowLaw(PWPhysics p,PWPhysics sub);
PetscErrorCode EtaCompositeSetBufferLength(PWPhysics p,PetscInt L);

/* shear viscosity limiters */
PetscErrorCode ShearViscosityLimiterMinMaxCreate(PetscBool use_si,PetscInt region,PWPhysics *p);


/* === Bulk viscosity coefficient evaluators === */

/* constant */
PetscErrorCode ZetaConstantCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode ZetaConstantSetParams(PWPhysics p,PetscReal zeta0);

/* constant shear to bulk ratio */
PetscErrorCode ZetaRatioCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode ZetaRatioSetParams(PWPhysics p,PetscReal R0,PWPhysics eta_flowlaw);

/* Diffusion creep flow law */
PetscErrorCode ZetaDiffusionCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode ZetaDiffusionCreepSetParams(PWPhysics p,PetscReal A,PetscReal E);

/* Dislocation creep flow law */
PetscErrorCode ZetaDislocationCreepCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode ZetaDislocationCreepSetParams(PWPhysics p,PetscReal A,PetscReal E,PetscReal n);

/* bulkd viscosity limiters */
PetscErrorCode BulkViscosityLimiterMinMaxCreate(PetscBool use_si,PetscInt region,PWPhysics *p);


/* === Effective permeability coefficient evaluators === */

/* constant */
PetscErrorCode kConstantCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode kConstantSetParams(PWPhysics p,PetscReal k0);

/* Porosity dependent (power law, exponent n) */
PetscErrorCode kPorosityDependentCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode kPorosityDependentSetParams(PWPhysics p,PetscReal k0,PetscReal n);
PetscErrorCode kPorosityDependentGetParams(PWPhysics p,PetscReal *k0,PetscReal *n);

/* permeability limiters */
PetscErrorCode EffectivePermeabilityLimiterSpatialXCreate(PetscBool use_si,PetscInt region,PWPhysics *p);


/* === Limiters === */
/* Sets field to f_min if f < fmin, or f_max if f > f_max  */
PetscErrorCode LimiterMinMaxCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode LimiterMinMaxSetParams(PWPhysics p,const char fieldname[],PetscReal min,PetscReal max);
PetscErrorCode LimiterMinMaxGetParams(PWPhysics p,PetscReal *min,PetscReal *max);

/* Sets field to zero when x > x_limit */
PetscErrorCode LimiterSpatialXCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode LimiterSpatialXSetParams(PWPhysics p,const char fieldname[],PetscReal XLimit);

/* Reduces field value by a factor of rfactor if ||x[] - origin[]|| < radius */
PetscErrorCode LimiterRadialCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode LimiterRadialSetParams(PWPhysics p,const char fieldname[],PetscReal alpha,PetscReal rfactor,PetscReal radius,PetscReal origin[]);

/* Chain multiple limiters together. Composite laws are applied in the order they were added */
PetscErrorCode LimiterCompositeCreate(PetscBool use_si,PetscInt region,const char name[],PWPhysics *p);
PetscErrorCode LimiterCompositeSetLaw(PWPhysics p,PWPhysics sub);

#endif
