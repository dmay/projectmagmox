

#include <petsc.h>
#include <tpctx.h>

#undef __FUNCT__
#define __FUNCT__ "TPCtxSetDimensionalScales_ELV"
PetscErrorCode TPCtxSetDimensionalScales_ELV(TPCtx ctx,PetscReal E,PetscReal L,PetscReal V)
{
  /* Length, velocity and time scales used: x = L x' */
  //E = 1.0e21; /* viscosity scale */
  //L = 600.0 * 1.0e3; /* length scale 600 km */
  //V = 5.0 * 1.0e-2 / (365.0 * 24.0 * 60.0 * 60.0); /* velocity scale - 5 cm/yr */

  ctx->E = E; // viscosity
  ctx->L = L; // length
  ctx->V = V; // velocity

  ctx->T = L/V; // time (not temperature
  ctx->Ka = (L*L)/ctx->T; // diffusivity
  ctx->K = (L*L)/E; // permability/mu
  
  ctx->R = E * ctx->T / (L*L); // density
  ctx->A = V/ctx->T; // acceleration
 
  ctx->P = (E*V)/L; // pressure
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TPCtxApplyScaling"
PetscErrorCode TPCtxApplyScaling(TPCtx ctx)
{
  PetscPrintf(PETSC_COMM_WORLD,"o --------------- Scales (SI) -------------- o\n");
  PetscPrintf(PETSC_COMM_WORLD,"|  viscosity   %1.4e\n",ctx->E);
  PetscPrintf(PETSC_COMM_WORLD,"|  length      %1.4e\n",ctx->L);
  PetscPrintf(PETSC_COMM_WORLD,"|  velocity    %1.4e\n",ctx->V);
  PetscPrintf(PETSC_COMM_WORLD,"|  time        %1.4e\n",ctx->T);
  PetscPrintf(PETSC_COMM_WORLD,"o --------------- Input (SI) --------------- o\n");
  PetscPrintf(PETSC_COMM_WORLD,"|  kappa       %1.4e\n",ctx->kappa);
  PetscPrintf(PETSC_COMM_WORLD,"|  rho_s       %1.4e\n",ctx->rho_s);
  PetscPrintf(PETSC_COMM_WORLD,"|  rho_l       %1.4e\n",ctx->rho_l);
  PetscPrintf(PETSC_COMM_WORLD,"|  rho_plate   %1.4e\n",ctx->rho_plate);
  PetscPrintf(PETSC_COMM_WORLD,"|  g           (%1.4e , %1.4e)\n",ctx->g[0],ctx->g[1]);
  PetscPrintf(PETSC_COMM_WORLD,"|  (L/Cp)      %1.4e\n",ctx->energy->L_on_Cp);
  PetscPrintf(PETSC_COMM_WORLD,"|  plate vel.  %1.4e\n",ctx->mechanics->plate_velocity);

  ctx->kappa = ctx->kappa / ctx->Ka;
  ctx->rho_s = ctx->rho_s / ctx->R;
  ctx->rho_l = ctx->rho_l / ctx->R;
  ctx->g[0] = ctx->g[0] / ctx->A;
  ctx->g[1] = ctx->g[1] / ctx->A;
  ctx->rho_plate = ctx->rho_plate / ctx->R;
  ctx->energy->L_on_Cp = ctx->energy->L_on_Cp / 1.0;
  ctx->mechanics->plate_velocity = ctx->mechanics->plate_velocity / ctx->V;
  ctx->mechanics->slab_decoupling_depth = ctx->mechanics->slab_decoupling_depth / ctx->L;
  ctx->mechanics->slab_decoupling_distance = ctx->mechanics->slab_decoupling_distance / ctx->L;

  PetscPrintf(PETSC_COMM_WORLD,"o --------------- Input (ND) --------------- o\n");
  PetscPrintf(PETSC_COMM_WORLD,"|  kappa'      %1.4e\n",ctx->kappa);
  PetscPrintf(PETSC_COMM_WORLD,"|  rho_s'      %1.4e\n",ctx->rho_s);
  PetscPrintf(PETSC_COMM_WORLD,"|  rho_l'      %1.4e\n",ctx->rho_l);
  PetscPrintf(PETSC_COMM_WORLD,"|  rho_plate'  %1.4e\n",ctx->rho_plate);
  PetscPrintf(PETSC_COMM_WORLD,"|  g'          (%1.4e , %1.4e)\n",ctx->g[0],ctx->g[1]);
  PetscPrintf(PETSC_COMM_WORLD,"|  (L/Cp)'     %1.4e\n",ctx->energy->L_on_Cp);
  PetscPrintf(PETSC_COMM_WORLD,"|  plate vel.' %1.4e\n",ctx->mechanics->plate_velocity);
  PetscPrintf(PETSC_COMM_WORLD,"o ------------------------------------------ o\n");
  
  PetscFunctionReturn(0);
}
