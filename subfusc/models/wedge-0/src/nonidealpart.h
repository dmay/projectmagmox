
#if !defined(__NonIdealPart_H)
#define __NonIdealPart_H

#include <petsc.h>

typedef struct _p_NonIdealPart *NonIdealPart;
typedef struct _SedData    *SedData;
typedef struct _MORBData   *MORBData;
typedef struct _GabbroData *GabbroData;
typedef struct _PUMData    *PUMData;

struct _p_NonIdealPart {
  PetscReal range_T[2];
  PetscReal range_P[2];

  PetscInt   rock_type;
  SedData    sediment;
  MORBData   morb;
  GabbroData gabbro;
  PUMData    pum;
  
  PetscErrorCode (*evaluate)(const PetscReal*, const PetscReal*,
                             const PetscInt,
                             const PetscReal*,
                             PetscReal*,PetscReal*,
                             PetscReal*,
                             PetscBool*,
                             void*);
  PetscErrorCode (*get_num_components)(int*);
  PetscErrorCode (*get_component_name)(int,const char**);
  PetscErrorCode (*get_component_phase_activity)(int,PetscBool*,PetscBool*);
};


struct _SedData {
  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal Wmh;
  PetscReal Wmc;
};

struct _MORBData {
  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal Wmh;
  PetscReal Wmc;
};

struct _GabbroData {
  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal Wmh;
  PetscReal Wmc;
};

struct _PUMData {
  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal Wmh;
  PetscReal Wmc;
};


PetscErrorCode NonIdealPartCreate(PetscInt rock_type,NonIdealPart *_ctx);
PetscErrorCode NonIdealPartDestroy(NonIdealPart *_ctx);

#endif
