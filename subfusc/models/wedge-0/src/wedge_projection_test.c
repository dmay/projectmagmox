
#include <petsc.h>
#include <petscdm.h>
#include <petscdmtet.h>
#include <petscdmtet.h>

#include <proj_init_finalize.h>
#include <pde.h>
#include <dmbcs.h>
#include <quadrature.h>
#include <coefficient.h>
#include <inorms.h>
#include <cslpoly.h>


#include <tpctx.h>
#include <mec.h>

/*
 [Test 1]
 Choose function: ul
 Analytically compute: div(ul)

 Project ul onto a DMTET mesh (aka pressure space) ul*
 Interpolate result onto a CSL mesh
 Compute L2 error of ul and div(ul) on the CSL mesh
*/


/* Test 1 */
PetscErrorCode t1_eval_ul(PetscReal coor[],PetscReal value[],void *ctx)
{
  PetscReal x,y;
  
  x = coor[0];
  y = coor[1];
  value[0] = PetscSinReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y);
  value[1] = -PetscSinReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y)*(y+1);
  PetscFunctionReturn(0);
}

PetscErrorCode t1_eval_divul(PetscReal coor[],PetscReal value[],void *ctx)
{
  PetscReal x,y;
  PetscReal dudx,dvdy;

  x = coor[0];
  y = coor[1];
  dudx = 4.2*PETSC_PI * PetscCosReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y);
  
  dvdy = 0.0;
  dvdy += -PetscSinReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y);
  dvdy += 1.1*PETSC_PI * PetscSinReal(4.2*PETSC_PI*x)*PetscSinReal(1.1*PETSC_PI*y)*(y+1);
  value[0] = dudx + dvdy;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "fu_quadrature"
PetscErrorCode fu_quadrature(Quadrature quadrature,DM dm,const char field[],
                             PetscErrorCode (*eval_at_point)(PetscReal*,PetscReal*,void*),
                             void *ctx)
{
  PetscErrorCode ierr;
  PetscInt i,q,e,c;
  PetscReal *coords,*elcoords,**tab_N;
  PetscInt *element,nen,npe,nbasis;
  PetscReal *coeff,Fu[10];
  PetscInt nqp,nc;
  PetscReal *xi,*w;
  
  PetscFunctionBegin;
  
  ierr = QuadratureGetRule(quadrature,&nqp,&xi,&w);CHKERRQ(ierr);
  ierr = QuadratureGetProperty(quadrature,field,NULL,NULL,&nc,&coeff);CHKERRQ(ierr);
  ierr = DMTetTabulateBasis(dm,nqp,xi,&nbasis,&tab_N);CHKERRQ(ierr);
  ierr = PetscMalloc1(nbasis*2,&elcoords);CHKERRQ(ierr);
  
  ierr = DMTetSpaceElement(dm,&nen,0,&element,&npe,0,&coords);CHKERRQ(ierr);
  for (e=0; e<nen; e++) {
    PetscInt *idx;
    
    /* get basis dofs */
    idx = &element[npe*e];
    
    /* get element coordinates */
    for (i=0; i<nbasis; i++) {
      PetscInt nidx = idx[i];
      
      elcoords[2*i+0] = coords[2*nidx+0];
      elcoords[2*i+1] = coords[2*nidx+1];
    }
    
    for (q=0; q<nqp; q++) {
      PetscReal x_qp[2];
      
      /* get x,y coords at each quadrature point */
      x_qp[0] = x_qp[1] = 0.0;
      for (i=0; i<nbasis; i++) {
        x_qp[0] += tab_N[q][i] * elcoords[2*i+0];
        x_qp[1] += tab_N[q][i] * elcoords[2*i+1];
      }
      ierr = eval_at_point(x_qp,Fu,ctx);CHKERRQ(ierr);
      
      for (c=0; c<nc; c++) {
        coeff[nc*(e*nqp + q)+c] = Fu[c];
      }
    }
  }
  
  ierr = PetscFree(elcoords);CHKERRQ(ierr);
  for (q=0; q<nqp; q++) {
    ierr = PetscFree(tab_N[q]);CHKERRQ(ierr);
  }
  ierr = PetscFree(tab_N);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

PetscErrorCode csl_integrate_div(CSLPoly sl,PetscInt degree,
                                 PetscErrorCode (*eval_divu)(PetscReal*,PetscReal*,void*),void *ctx,
                                 PetscReal _l2[])
{
  PetscReal l2[2],diffsq;
  PetscInt inside_domain,inside_csl_domain;
  PetscReal dv,dJ_q;
  PetscReal div_q,div_a,ccnt = 0.0;
  PetscInt q,e,k;
  PetscReal w[9];
  PetscInt nqp;
  PetscReal xi[2*9];
  
  switch (degree) {
    case 1:
      nqp = 1;
      xi[0] = 0.0; xi[1] = 0.0;
      w[0] = 4.0;
      break;
    case 2:
      nqp = 4;
      xi[2*0+0] = -0.57735026918963; xi[2*0+1] = -0.57735026918963;
      xi[2*1+0] =  0.57735026918963; xi[2*1+1] = -0.57735026918963;
      xi[2*2+0] = -0.57735026918963; xi[2*2+1] =  0.57735026918963;
      xi[2*3+0] =  0.57735026918963; xi[2*3+1] =  0.57735026918963;
      w[0] = 1.0;
      w[1] = 1.0;
      w[2] = 1.0;
      w[3] = 1.0;
      break;
    case 3:
      nqp = 1;
      xi[0] = 0.0; xi[1] = 0.0;
      w[0] = 4.0;
      break;
      
    default:
      nqp = 1;
      xi[0] = 0.0; xi[1] = 0.0;
      w[0] = 4.0;
      break;
  }
  
  dv = sl->dx[0] * sl->dx[1];
  dJ_q = 0.25 * dv;
  l2[0] = 0.0;
  l2[1] = 0.0;
  for (e=0; e<sl->ncells; e++) {
    PetscReal cell_val = 0.0;
    
    if (sl->cell_list[e]->type != COOR_INTERIOR) continue;

    for (q=0; q<nqp; q++) {
      PetscReal coor_q[2];
      PetscReal x0[2],Ni[4],coor_v[2*4];
      
      // get quadrature coordinates
      x0[0] =  sl->cell_list[e]->coor[0] - 0.5 * sl->dx[0];
      x0[1] =  sl->cell_list[e]->coor[1] - 0.5 * sl->dx[1];
      
      Ni[0] = 0.25 * (1.0 - xi[2*q]) * (1.0 - xi[2*q+1]);
      Ni[1] = 0.25 * (1.0 + xi[2*q]) * (1.0 - xi[2*q+1]);
      Ni[2] = 0.25 * (1.0 - xi[2*q]) * (1.0 + xi[2*q+1]);
      Ni[3] = 0.25 * (1.0 + xi[2*q]) * (1.0 + xi[2*q+1]);
      
      coor_v[2*0+0] = x0[0];            coor_v[2*0+1] = x0[1];
      coor_v[2*1+0] = x0[0]+sl->dx[0];  coor_v[2*1+1] = x0[1];
      coor_v[2*2+0] = x0[0];            coor_v[2*2+1] = x0[1]+sl->dx[1];
      coor_v[2*3+0] = x0[0]+sl->dx[0];  coor_v[2*3+1] = x0[1]+sl->dx[1];
      
      
      coor_q[0] = coor_q[1] = 0.0;
      for (k=0; k<4; k++) {
        coor_q[0] += Ni[k] * coor_v[2*k+0];
        coor_q[1] += Ni[k] * coor_v[2*k+1];
      }
      
      // get value
      CSLPolyInterpolateAtPointWithArray(sl,I_CSPLINE,
                                         coor_q,
                                         1,(const PetscReal*)sl->divu_c,
                                         NULL,NULL,
                                         &div_q,&inside_domain,&inside_csl_domain);
      eval_divu(coor_q,&div_a,ctx);
      
      diffsq = w[q] * (div_q - div_a)*(div_q - div_a) * dJ_q;
      
      cell_val += w[q] * (div_a) * dJ_q;
      
      // compute l2
      l2[0] = l2[0] + diffsq;
    }
    l2[1] = l2[1] + (cell_val/dv - sl->divu_c[e])*(cell_val/dv - sl->divu_c[e]) * 1.0;
    ccnt += 1.0;
  }
  l2[0] = PetscSqrtReal(l2[0]);
  l2[1] = PetscSqrtReal(l2[1]/ccnt);
  _l2[0] = l2[0];
  _l2[1] = l2[1];
  
  PetscFunctionReturn(0);
}

PetscErrorCode test1(void)
{
  char meshfilename[PETSC_MAX_PATH_LEN];
  PetscInt mxy = 4,pk = 1;
  DM dm,dm_tmp;
  Quadrature quadrature;
  Vec dm_vl,dm_div;
  PetscInt nen;
  PetscReal Ep[Q_NUM_ENTRIES];
  PetscErrorCode ierr;
  
  
  PetscOptionsGetInt(NULL,NULL,"-pk",&pk,NULL);
  
  PetscSNPrintf(meshfilename,PETSC_MAX_PATH_LEN-1,"/Users/dmay/codes/subfusc-dev/subfusc-v1.0/subfusc/models/wedge-0/recodehangups/subduction.2");
  /*
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   2,DMTET_CWARP_AND_BLEND,pk,&dm);CHKERRQ(ierr);
  */
  ierr = PetscOptionsGetInt(NULL,NULL,"-mxy",&mxy,NULL);CHKERRQ(ierr);
  ierr = DMTetCreate2dSquareGeometry(PETSC_COMM_WORLD,0.0,1.1,-1.0,0.0, mxy,mxy, PETSC_TRUE,
                                     2,DMTET_CWARP_AND_BLEND,pk,&dm);CHKERRQ(ierr);
  
  ierr = QuadratureCreate(&quadrature);CHKERRQ(ierr);
  
  //ierr = QuadratureSetUpFromDM(quadrature,dm);CHKERRQ(ierr);
  ierr = QuadratureSetParameters(quadrature,2,QUADRATURE_VOLUME,GAUSS_LEGENDRE_WARPED_TENSOR,pk+4);CHKERRQ(ierr);
  ierr = QuadratureSetUp(quadrature);
  
  ierr = DMTetSpaceElement(dm,&nen,0,0,0,0,0);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature,nen,"vl",2);CHKERRQ(ierr);
  
  ierr = fu_quadrature(quadrature,dm,"vl",
                       t1_eval_ul,NULL);CHKERRQ(ierr);
  
  dm_vl = NULL;
  ierr = DMTetProjectQuadratureField(quadrature,"vl",dm,&dm_vl);CHKERRQ(ierr);

  ierr = ComputeINorms(Ep,dm,dm_vl,
                       t1_eval_ul,
                       NULL,NULL);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"dm[vl]  pk %.2D l2 %+1.12e\n",pk,Ep[Q_L2]);
  
  ierr = DMTetCreateSharedSpace(dm,1,&dm_tmp);CHKERRQ(ierr);
  dm_div = NULL;
  ierr = DMTetProjectField(dm,dm_vl,PRJTYPE_DIVERGENCE,dm_tmp,&dm_div);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);
  
  ierr = DMTetCreateSharedSpace(dm,1,&dm_tmp);CHKERRQ(ierr);
  ierr = ComputeINorms(Ep,dm_tmp,dm_div,
                       t1_eval_divul,
                       NULL,NULL);CHKERRQ(ierr);
  ierr = DMDestroy(&dm_tmp);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"dm[div] pk %.2D l2 %+1.12e\n",pk,Ep[Q_L2]);

  //
  {
    const char *field[] = { "vl","div" };
    Vec ff[2];
    ff[0] = dm_vl;
    ff[1] = dm_div;
    ierr = DMTetViewFields_VTU(dm,2,ff,field,"t1_vl_");CHKERRQ(ierr);
  }
  //
  
  ierr = VecDestroy(&dm_vl);CHKERRQ(ierr);
  ierr = VecDestroy(&dm_div);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


/* Test 2 */
PetscErrorCode t2_eval_pf(PetscReal coor[],PetscReal value[],void *ctx)
{
  PetscReal x,y;
  
  x = coor[0];
  y = coor[1];
  value[0] = PetscSinReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y);
  PetscFunctionReturn(0);
}

PetscErrorCode t2_eval_gradpf(PetscReal coor[],PetscReal value[],void *ctx)
{
  PetscReal x,y;
  PetscReal dudx,dudy;
  
  x = coor[0];
  y = coor[1];
  dudx = 4.2*PETSC_PI * PetscCosReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y);
  dudy = -1.1*PETSC_PI * PetscSinReal(4.2*PETSC_PI*x)*PetscSinReal(1.1*PETSC_PI*y);
  
  value[0] = dudx + x*x*x*y;
  value[1] = dudy + y*y*y*y*x*x;
  PetscFunctionReturn(0);
}

PetscErrorCode t2_eval_divgradpf(PetscReal coor[],PetscReal value[],void *ctx)
{
  PetscReal x,y;
  //PetscReal dudx,dudy;
  PetscReal dudx2,dudy2;
  
  x = coor[0];
  y = coor[1];
  //dudx = 4.2*PETSC_PI * PetscCosReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y);
  //dudy = -1.1*PETSC_PI * PetscSinReal(4.2*PETSC_PI*x)*PetscSinReal(1.1*PETSC_PI*y);

  dudx2 = -4.2*PETSC_PI*4.2*PETSC_PI * PetscSinReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y) + 3.0 * x*x * y;
  dudy2 = -1.1*PETSC_PI*1.1*PETSC_PI * PetscSinReal(4.2*PETSC_PI*x)*PetscCosReal(1.1*PETSC_PI*y) + 4.0 * y*y*y * x*x;

  value[0] = dudx2 + dudy2;
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  ierr = test1();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}
