
#include <petsc.h>
#include <cpart.h>

const char *cpart_component_names[] = { "H2O", "FT", "RF" };

#undef __FUNCT__
#define __FUNCT__ "CPartEvaluate"
PetscErrorCode CPartEvaluate(const PetscReal P[], const PetscReal T[],
                              const PetscInt ncomponents,
                              const PetscReal C_bulk[],
                              PetscReal C_liquid[],PetscReal C_solid[],
                              PetscReal *phi,
                              PetscBool *valid,
                              void *ctx)
{
  const PetscReal d0 = 1226.0+273.0; // K
  const PetscReal d1 = 132.9; // K/Gpa
  const PetscReal d2 = -5.1; // K/Gpa^2
  const PetscReal M_ft = 700.0*2.0; //K
  const PetscReal M_h2o = 2.0e5; //K
  const PetscReal DeltaC_ft = 0.6/2.0;
  const PetscReal D_h2o = 0.01;
  PetscReal porosity = 0.0; /* default porosity */
  PetscReal A,B,C;
  PetscReal T_solidus,T_liquidus;
  PetscReal T_ol;
  
  T_ol       = (d0 + P[0]*(d1 + d2*P[0]));
  T_solidus  = T_ol - M_ft*C_bulk[1] - M_h2o*C_bulk[0];
  T_liquidus = T_ol - M_ft*C_bulk[1] + M_ft*DeltaC_ft - D_h2o*M_h2o*C_bulk[0];
  
  if (T[0] <= T_solidus) {
    porosity = 0.0;
    C_liquid[0] = 0.0; // h2o
    C_solid[0]  = C_bulk[0];
    
    C_liquid[1] = 0.0; // ft
    C_solid[1]  = C_bulk[1];
    
    C_liquid[2] = 0.0; // rf
    C_solid[2]  = C_bulk[2];
    *valid = PETSC_TRUE;
    
  } else {
    if (T[0] >= T_liquidus) {
      porosity = 1.0;
      C_liquid[0] = C_bulk[0]; // h2o
      C_solid[0]  = 0.0;
      
      C_liquid[1] = C_bulk[1]; // ft
      C_solid[1]  = 0.0;
      
      C_liquid[2] = C_bulk[2]; // rf
      C_solid[2]  = 0.0;
      *valid = PETSC_TRUE;
      
    } else {
      PetscReal disc;

      A = M_h2o;
      B = T[0] - T_ol + M_ft*(C_bulk[1]+DeltaC_ft/(1.0/D_h2o-1.0));
      C = -M_ft*DeltaC_ft*C_bulk[0]/(1.0/D_h2o-1.0);
      disc = B*B-4.0*A*C;
      
      if (disc > 0.0) {
        // h2o
        C_solid[0]  = (-B+PetscSqrtReal(disc))/(2.0*A);
        C_liquid[0] = C_solid[0]/D_h2o;
        
        porosity = (C_bulk[0]/C_solid[0] -1.0)/(1.0/D_h2o-1.0);
        
        // ft
        C_solid[1]  = C_bulk[1] - porosity*DeltaC_ft;
        C_liquid[1] = C_solid[1] + DeltaC_ft;
        
        // rf
        /* The third component is eliminated - apply unity sum */
        C_liquid[2] = 1.0 - C_liquid[0] - C_liquid[1];
        C_solid[2]  = 1.0 - C_solid[0]  - C_solid[1];
        *valid = PETSC_TRUE;
      } else {
        *valid = PETSC_FALSE;
      }
    }
  }
  *phi = porosity;
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "CPartGetNumComponents"
PetscErrorCode CPartGetNumComponents(PetscInt *nc)
{
  *nc = 3;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CPartGetComponentName"
PetscErrorCode CPartGetComponentName(PetscInt index,const char **name)
{
  switch (index) {
    case 0:
      *name = cpart_component_names[index];
      break;
    case 1:
      *name = cpart_component_names[index];
      break;
    case 2:
      *name = cpart_component_names[index];
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"CPart defines 3 components. Index %D is not a valid component index",index);
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CPartGetComponentActivity"
PetscErrorCode CPartGetComponentActivity(PetscInt index,PetscBool *solid,PetscBool *liquid)
{
  switch (index) {
    case 0:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
    case 1:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
    case 2:
      *solid  = PETSC_TRUE;
      *liquid = PETSC_TRUE;
      break;
      
    default:
      SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"CPart defines 3 components. Index %D is not a valid component index",index);
      break;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CPartCreate"
PetscErrorCode CPartCreate(CPart *_ctx)
{
  CPart ctx;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&ctx);CHKERRQ(ierr);
  ierr = PetscMemzero(ctx,sizeof(struct _p_CPart));CHKERRQ(ierr);
  
  ctx->range_T[0] = 273.0;
  ctx->range_T[1] = PETSC_MAX_REAL;

  ctx->range_P[0] = 0.0;
  ctx->range_P[1] = PETSC_MAX_REAL;

  ctx->range_CH2O[0] = 0.0;
  ctx->range_CH2O[1] = 1.0;

  ctx->range_CRF[0] = 0.0;
  ctx->range_CRF[1] = 1.0;

  ctx->range_CFT[0] = 0.0;
  ctx->range_CFT[1] = 1.0;

  ctx->evaluate = CPartEvaluate;
  ctx->get_num_components = CPartGetNumComponents;
  ctx->get_component_name = CPartGetComponentName;
  ctx->get_component_phase_activity = CPartGetComponentActivity;
  
  *_ctx = ctx;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CPartDestroy"
PetscErrorCode CPartDestroy(CPart *_ctx)
{
  CPart ctx;
  PetscErrorCode ierr;
  
  if (!_ctx) PetscFunctionReturn(0);
  ctx = *_ctx;
  ierr = PetscFree(ctx);CHKERRQ(ierr);
  *_ctx = NULL;
  PetscFunctionReturn(0);
}
