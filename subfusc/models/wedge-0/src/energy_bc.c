
#include <petsc.h>
#include <petscdm.h>

#include <dmbcs.h>

#include <tpctx.h>
#include <mec.h>


#undef __FUNCT__
#define __FUNCT__ "EvalTemperature_Case1A"
PetscErrorCode EvalTemperature_Case1A(PetscScalar coor[],PetscInt dof,
                                      PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_s = 273.0;
  PetscReal T_0 = 1573.0;
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  TPCtx tp;
  PetscErrorCode ierr;
  
  tp = (TPCtx)ctx;
  
  *constrain = PETSC_FALSE;
  
  depth = PetscAbsReal(coor[1]);
  /*
   Note this is a thermal parameter so it may be set differently to the definition of the actual geometry of the mechanical model.
  */
  plate_thickness =  30.0 * 1.0e3 / tp->L;
  
  kappa = tp->kappa * tp->Ka; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_s;
    
    PetscFunctionReturn(0);
  }
  
  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_s + (T_0 - T_s) * erf( depth * tp->L / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;
    
    PetscFunctionReturn(0);
  }
  
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */
      
      T = T_s + (T_0 - T_s) * depth / plate_thickness;
      *constrain = PETSC_TRUE;
      *val = T;
      
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      PetscReal vel[2];
      
      ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val = T_0;
        PetscFunctionReturn(0);
      }
    }
  }
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "EvalTemperature_ContinentalGeotherm"
PetscErrorCode EvalTemperature_ContinentalGeotherm(PetscScalar coor[],PetscInt dof,
                                      PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_surface   = 273.0; // [K]
  PetscReal T_plate     = 650.0 + 273.0; // [K]
  PetscReal T_600,T_grad_cont = 9.8; // [K/km]
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  TPCtx tp;
  //PetscReal time;
  PetscErrorCode ierr;
  
  tp = (TPCtx)ctx;
  
  *constrain = PETSC_FALSE;
  
  //time = tp->time;
  
  depth = PetscAbsReal(coor[1]) * tp->L; // [m]
  plate_thickness =  50.0 * 1.0e3; // [m]
  
  kappa = tp->kappa * tp->Ka; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  //T_grad_cont = 1573.0 / 600.0;
  //T_plate = (plate_thickness / 1.0e3) * T_grad_cont;
  
  T_600 = 1900.0;
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_surface;
    
    PetscFunctionReturn(0);
  }
  
  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_surface + (T_600 - T_surface) * erf( depth / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;
    
    PetscFunctionReturn(0);
  }
  
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) { /* right boundary */
    PetscReal vel[2];
    
    ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
    
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */
      
      T = T_surface + (T_plate - T_surface) * (depth / plate_thickness);
      
    } else { // below plate
      T = T_plate + T_grad_cont * (depth - plate_thickness)/1.0e3;
    }
    if (T > T_600) { T = T_600; }
    *constrain = PETSC_TRUE;
    *val = T;
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TemperatureBC_ThickPlate"
PetscErrorCode TemperatureBC_ThickPlate(PetscScalar coor[],PetscInt dof,
                                      PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal T_s = 273.0;
  PetscReal T_0 = 1573.0;
  PetscReal kappa;
  PetscReal t_50;
  PetscReal arg;
  PetscReal T;
  PetscReal depth,plate_thickness;
  TPCtx tp;
  PetscErrorCode ierr;
  
  tp = (TPCtx)ctx;
  
  *constrain = PETSC_FALSE;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  120.0 * 1.0e3 / tp->L; // normalized
  
  kappa = tp->kappa * tp->Ka; /* m^2 s^{-1} */
  t_50 = 50.0 * 1.0e6 * 365.0 * 24.0 * 60.0 * 60.0; /* 50 Myr in sec */
  arg = 2.0 * PetscSqrtReal( kappa * t_50 );
  
  if (depth < 1.0e-10) { /* top */
    *constrain = PETSC_TRUE;
    *val = T_s;
    
    PetscFunctionReturn(0);
  }
  
  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    T = T_s + (T_0 - T_s) * erf( depth * tp->L / arg ); /* depth in m - model normalized by 600 km */
    *constrain = PETSC_TRUE;
    *val = T;
    
    PetscFunctionReturn(0);
  }
  
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) { /* right boundary */
    if (depth <= plate_thickness) { /* over-riding plate - linear gradient between T_s and T_0 */
      
      T = T_s + (T_0 - T_s) * depth / plate_thickness;
      *constrain = PETSC_TRUE;
      *val = T;
      
      //printf("%+1.4e %+1.4e\n",coor[1],T);
      PetscFunctionReturn(0);
    } else { /* inflow from the mantle */
      PetscReal vel[2];
      
      ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val = T_0;
        //printf("%+1.4e %+1.4e\n",coor[1],T_0);
        PetscFunctionReturn(0);
      }
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergySetDirichletBC"
PetscErrorCode EnergySetDirichletBC(ECtx ctx,TPCtx tp)
{
  BCList bc_T;
  PetscErrorCode ierr;
  
  bc_T = ctx->bc_T;
  
  //ierr = DMTetBCListTraverse(bc_T,0,EvalTemperature_Case1A,(void*)tp);CHKERRQ(ierr);
  ierr = DMTetBCListTraverse(bc_T,0,TemperatureBC_ThickPlate,(void*)tp);CHKERRQ(ierr);
  ierr = VecSet(ctx->X,273.0);CHKERRQ(ierr);
  ierr = BCListInsert(bc_T,ctx->X);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}
