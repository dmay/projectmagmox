
#if !defined(__DARCY_CTX_H__)
#define __DARCY_CTX_H__


struct _p_DarcyCtx {
  DM     dm_ref; /* triangulation used for the Darcy system - encodes geometry only with P1 space */
  DM     dm_v,dm_p;
  PDE    pdedarcy;
  SNES   snesdarcy;
  Mat    J;
  Vec    X,F; /* solution, residual for Darcy */
  Vec    g,h; /* rhs of Darcy system */
  Mat    Iv_m2d; /* interpolate vs from the mechanical system to the darcy system */
  Mat    Ip_m2d; /* interpolate pf from the mechanical system to the darcy system */
  Mat    IT_e2d; /* interpolate temperature from the mechanical system to the darcy system */
  BCList bc_v,bc_p;
  TPCtx  tpctx;

  DM dm_c_phi_T,dm_adv_v; /* same as velocity space */
  Vec cl,cs,T,phi,phi0,vl,vs,vbulk,gamma;
  
  /* concentrations */
  PDE    pde_cl,pde_cs;
  BCList bc_cl,bc_cs;
  SNES   snes_adv,snes_adv_cs;
  Mat    J_adv;
  Vec    F_adv; /* jacobian and residual for advection system (shared between cl,cs) */
  /* gamma utilities */
  Mat M_dg;
  KSP proj_dg;
  DM  dm_dg;
  
  PetscReal kappa_cl,kappa_cs,kappa_phi;
  PetscBool issetup;
  // output data types
  PetscBool output_hdf;
};

PetscErrorCode DarcyCtxCreate(DarcyCtx *ctx);
PetscErrorCode DarcyCtxDestroy(DarcyCtx *ctx);
PetscErrorCode DarcyCtxSetUp(DarcyCtx d,TPCtx tpctx);
PetscErrorCode DarcyCtxSolve(DarcyCtx d,PetscBool *pass);
PetscErrorCode DarcyCtxView(DarcyCtx d,const char dir[],PetscInt step);
PetscErrorCode DarcyCtx_PoroDistF(DarcyCtx darcy,DM dm,Vec cl,Vec cs,Vec T,Vec phi);
PetscErrorCode DarcyCtx_PoroDistF_VQuadrature(DarcyCtx darcy,DM dm,Vec cl,Vec cs,Vec T,Coefficient c);
PetscErrorCode DarcyCtx_PoroDistF_SQuadrature(DarcyCtx darcy,DM dm,Vec cl,Vec cs,Vec T,Coefficient c);
PetscErrorCode DarcyComputeBulkVelocity_OmegaM(DarcyCtx darcy,MCtx mechanics,Mat I_v,PetscInt plate_region_id,PetscInt slab_region_id,PetscBool use_vs);

PetscErrorCode DMConfigureWedgeBoundaries_Default(DM dm);
PetscErrorCode DMConfigureWedgeBoundaries(DM dm);
PetscErrorCode SDAdvectionPDECreate(const char name[],DM dm,BCList bclist,DM dm_v,Vec velocity,
                                    PetscReal dt,PetscReal kappa,PDE *_pde);
PetscErrorCode SDAdvection_BoundaryConditionSetup_Cl(DarcyCtx darcy);
PetscErrorCode SDAdvection_BoundaryConditionSetup_Cs(DarcyCtx darcy);
PetscErrorCode SDAdvection_SetInitialCondition_Cl(DarcyCtx d);
PetscErrorCode SDAdvection_SetInitialCondition_Cs(DarcyCtx d);

PetscErrorCode DarcyReconstructGamma(DarcyCtx darcy,PetscReal kappa_phi,PetscReal dt);
PetscErrorCode QuadratureEvaluator_MeltingRate_Darcy(Quadrature quadrature,DM dmT,TPCtx ctx);
PetscErrorCode SDAdvection_ComputeCellDiffusion(PDE pde,Vec vel,PetscReal target_cell_Pe,PetscReal *_kappa);
PetscErrorCode DivergenceView(DM dmpack,Vec X,const char dir[],PetscInt step);

PetscErrorCode DarcyComputeSolidLiquidVelocityCFLTimestep(DarcyCtx darcy,PetscReal target_cfl,PetscReal *_dt);

#endif
