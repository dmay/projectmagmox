
#include <petsc.h>
#include <thermodyn.h>

const char td_msg_valid[] = "[ThermoDyn] Status: valid";

const char td_msg_invalid_input_min[] = "[ThermoDyn] Status: invalid input (range min): Field";
const char td_msg_invalid_input_max[] = "[ThermoDyn] Status: invalid input (range max): Field";
const char td_msg_invalid_input_complex[] = "[ThermoDyn] Status: invalid input (complex valued): Field";

const char td_msg_invalid_output_min[] = "[ThermoDyn] Status: invalid output (range min): Field";
const char td_msg_invalid_output_max[] = "[ThermoDyn] Status: invalid output (range max): Field";
const char td_msg_invalid_output_complex[] = "[ThermoDyn] Status: invalid output (complex valued): Field";


#undef __FUNCT__
#define __FUNCT__ "ThermoDynGetStatus"
PetscErrorCode ThermoDynGetStatus(PetscInt code,char message[PETSC_MAX_PATH_LEN])
{
  PetscBool found = PETSC_FALSE;
  
  switch (code) {
    case TD_STATUS_VALID:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s",td_msg_valid);
      found = PETSC_TRUE;
      break;

    case TD_STATUS_INVALID_INPUT_P_RANGE_MIN:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (pressure)",td_msg_invalid_input_min);
      found = PETSC_TRUE;
      break;
    case TD_STATUS_INVALID_INPUT_P_RANGE_MAX:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (pressure)",td_msg_invalid_input_max);
      found = PETSC_TRUE;
      break;

    case TD_STATUS_INVALID_INPUT_T_RANGE_MIN:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (temperature)",td_msg_invalid_input_min);
      found = PETSC_TRUE;
      break;
    case TD_STATUS_INVALID_INPUT_T_RANGE_MAX:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (temperature)",td_msg_invalid_input_max);
      found = PETSC_TRUE;
      break;

    case TD_STATUS_INVALID_OUTPUT_POROSITY_RANGE_MIN:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (porosity)",td_msg_invalid_output_min);
      found = PETSC_TRUE;
      break;
    case TD_STATUS_INVALID_OUTPUT_POROSITY_RANGE_MAX:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (porosity)",td_msg_invalid_output_max);
      found = PETSC_TRUE;
      break;
    case TD_STATUS_INVALID_OUTPUT_POROSITY_COMPLEX_VALUED:
      PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (porosity)",td_msg_invalid_output_complex);
      found = PETSC_TRUE;
      break;
      
    default:
      break;
  }

  if (!found && (code >= MAX_COMPONENTS)) {
    PetscInt code_100;
    
    code_100 = code/MAX_COMPONENTS;
    
    switch (code_100) {
      case 1:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_bulk[%D])",td_msg_invalid_input_min,code-TD_STATUS_INVALID_INPUT_CBULK_RANGE_MIN);
        found = PETSC_TRUE;
        break;
      case 2:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_bulk[%D])",td_msg_invalid_input_max,code-TD_STATUS_INVALID_INPUT_CBULK_RANGE_MAX);
        found = PETSC_TRUE;
        break;
      case 3:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_bulk[%D])",td_msg_invalid_input_complex,code-TD_STATUS_INVALID_INPUT_CBULK_COMPLEX_VALUED);
        found = PETSC_TRUE;
        break;

      case 4:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_liquid[%D])",td_msg_invalid_output_min,code-TD_STATUS_INVALID_OUTPUT_CLIQUID_RANGE_MIN);
        found = PETSC_TRUE;
        break;
      case 5:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_liquid[%D])",td_msg_invalid_output_max,code-TD_STATUS_INVALID_OUTPUT_CLIQUID_RANGE_MAX);
        found = PETSC_TRUE;
        break;
      case 6:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_liquid[%D])",td_msg_invalid_output_complex,code-TD_STATUS_INVALID_OUTPUT_CLIQUID_COMPLEX_VALUED);
        found = PETSC_TRUE;
        break;

      case 7:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_solid[%D])",td_msg_invalid_output_min,code-TD_STATUS_INVALID_OUTPUT_CSOLID_RANGE_MIN);
        found = PETSC_TRUE;
        break;
      case 8:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_solid[%D])",td_msg_invalid_output_max,code-TD_STATUS_INVALID_OUTPUT_CSOLID_RANGE_MAX);
        found = PETSC_TRUE;
        break;
      case 9:
        PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"%s (C_solid[%D])",td_msg_invalid_output_complex,code-TD_STATUS_INVALID_OUTPUT_CSOLID_COMPLEX_VALUED);
        found = PETSC_TRUE;
        break;

      default:
        break;
    }
  }
  
  if (!found) {
    PetscSNPrintf(message,PETSC_MAX_PATH_LEN-1,"[ThermoDyn] Status: Unknown error code %D encountered. Please consult thermodyn.h for definitions.",code);
  }
  
  PetscFunctionReturn(0);
}
