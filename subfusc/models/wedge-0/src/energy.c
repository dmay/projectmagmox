
#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petscsnes.h>

#include <petscdmtet.h>
#include <pde.h>
#include <quadrature.h>
#include <coefficient.h>
#include <pdehelmholtz.h>
#include <dmbcs.h>

#include <tpctx.h>

#undef __FUNCT__
#define __FUNCT__ "EnergyContextCreate"
PetscErrorCode EnergyContextCreate(ECtx *_e)
{
  ECtx e;
  PetscErrorCode ierr;
  ierr = PetscMalloc1(1,&e);CHKERRQ(ierr);
  ierr = PetscMemzero(e,sizeof(struct _p_ECtx));CHKERRQ(ierr);
  SFUSCSetClassId(e,ENERGY_CLASSID);
  e->output_hdf = PETSC_FALSE;
  e->viewer_active = PETSC_TRUE;
  *_e = e;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergyContextDestroy"
PetscErrorCode EnergyContextDestroy(ECtx *_e)
{
  ECtx e;
  PetscErrorCode ierr;
  if (!_e) PetscFunctionReturn(0);
  e = *_e;
  
  ierr = DMDestroy(&e->dm_e);CHKERRQ(ierr);
  ierr = DMDestroy(&e->dm_ev);CHKERRQ(ierr);
  
  ierr = BCListDestroy(&e->bc_T);CHKERRQ(ierr);
  
  ierr = VecDestroy(&e->F);CHKERRQ(ierr);
  ierr = VecDestroy(&e->X);CHKERRQ(ierr);
  ierr = MatDestroy(&e->J);CHKERRQ(ierr);

  ierr = VecDestroy(&e->v_bar_e);CHKERRQ(ierr);
  ierr = VecDestroy(&e->v_s_e);CHKERRQ(ierr);
  ierr = VecDestroy(&e->X_prev);CHKERRQ(ierr);
  ierr = VecDestroy(&e->phi);CHKERRQ(ierr);
  ierr = VecDestroy(&e->phi_prev);CHKERRQ(ierr);

  ierr = PDEDestroy(&e->pde);CHKERRQ(ierr);
  ierr = SNESDestroy(&e->snes);CHKERRQ(ierr);
  
  ierr = PetscFree(e);CHKERRQ(ierr);
  *_e = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EnergyCreateDMs"
PetscErrorCode _EnergyCreateDMs(ECtx e,const char meshfilename[],PetscInt pk)
{
  PetscErrorCode ierr;
  
  ierr = DMTetCreatePartitionedFromTriangleWithRef(PETSC_COMM_WORLD,meshfilename,
                                   1,DMTET_CWARP_AND_BLEND,pk,&e->dm_e);CHKERRQ(ierr);
  /*
  ierr = DMTetCreate2dFromTriangle(PETSC_COMM_WORLD,meshfilename,
                                   2,DMTET_CWARP_AND_BLEND,pk,&e->dm_ev);CHKERRQ(ierr);
  */
  ierr = DMTetCreateSharedGeometry(e->dm_e,2,DMTET_CWARP_AND_BLEND,pk,&e->dm_ev);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EnergyCreateBCs"
PetscErrorCode _EnergyCreateBCs(ECtx e)
{
  PetscErrorCode ierr;
  
  /* Define velocity Dirichlet boundary conditions on the sub mesh */
  ierr = DMBCListCreate(e->dm_e,&e->bc_T);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_EnergyCreatePDE"
PetscErrorCode _EnergyCreatePDE(ECtx e)
{
  PetscErrorCode ierr;
  Coefficient g;
  PetscInt nelements;
  Quadrature quadrature;
  
  ierr = PDECreate(PETSC_COMM_WORLD,&e->pde);CHKERRQ(ierr);
  ierr = PDESetType(e->pde,PDEHELMHOLTZ);CHKERRQ(ierr);
  ierr = PDESetOptionsPrefix(e->pde,"engy_");CHKERRQ(ierr);
  ierr = PDEHelmholtzSetData(e->pde,e->dm_e,e->bc_T);CHKERRQ(ierr);
  
  ierr = PDEHelmholtzGetCoefficient_g(e->pde,&g);CHKERRQ(ierr);
  ierr = CoefficientGetQuadrature(g,&quadrature);CHKERRQ(ierr);
  ierr = DMTetSpaceElement(e->dm_e,&nelements,NULL,NULL,NULL,NULL,NULL);CHKERRQ(ierr);
  /*
  ierr = QuadratureSetProperty(quadrature,nelements,"phi",1);CHKERRQ(ierr);
  ierr = QuadratureSetProperty(quadrature,nelements,"phi_prev",1);CHKERRQ(ierr);
  */
  {
    PetscInt nelements_e;
    
    ierr = QuadratureGetSizes(quadrature,NULL,NULL,NULL,&nelements_e);CHKERRQ(ierr);
    ierr = QuadratureSetProperty(quadrature,nelements_e,"gamma",1);CHKERRQ(ierr);
  }
  
  ierr = DMCreateGlobalVector(e->dm_ev,&e->v_bar_e);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(e->dm_ev,&e->v_s_e);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetVelocity(e->pde,e->dm_ev,e->v_bar_e);CHKERRQ(ierr);
  ierr = PDEHelmholtzSetAssumeVelocityIncompressible(e->pde,PETSC_TRUE);CHKERRQ(ierr);
  
  ierr = DMCreateMatrix(e->dm_e,&e->J);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(e->dm_e,&e->X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(e->dm_e,&e->F);CHKERRQ(ierr);
  ierr = PDESetSolution(e->pde,e->X);CHKERRQ(ierr);
  {
    Vec x;
    
    ierr = DMCreateLocalVector(e->dm_e,&x);CHKERRQ(ierr);
    
    ierr = PDESetLocalSolution(e->pde,x);CHKERRQ(ierr);
   
    ierr = VecDestroy(&x);CHKERRQ(ierr);
  }
  
  /* Create and configure the two-phase flow solver */
  ierr = PDESetOptionsPrefix(e->pde,"T_");CHKERRQ(ierr);
  ierr = SNESCreate(PETSC_COMM_WORLD,&e->snes);CHKERRQ(ierr);
  ierr = PDEConfigureSNES(e->pde,e->snes,e->F,e->J,e->J);CHKERRQ(ierr);
  ierr = SNESSetType(e->snes,SNESKSPONLY);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(e->snes);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergySetUp"
PetscErrorCode EnergySetUp(ECtx e,const char meshfilename[],PetscInt pk)
{
  PetscErrorCode ierr;
  ierr = PetscOptionsGetBool(NULL,NULL,"-subfusc.energy.viewer_active",&e->viewer_active,NULL);CHKERRQ(ierr);
  ierr = _EnergyCreateDMs(e,meshfilename,pk);CHKERRQ(ierr);
  ierr = _EnergyCreateBCs(e);CHKERRQ(ierr);
  ierr = _EnergyCreatePDE(e);CHKERRQ(ierr);

  /* auxillary */
  ierr = DMCreateGlobalVector(e->dm_e,&e->X_prev);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(e->dm_e,&e->phi);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(e->dm_e,&e->phi_prev);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergyView"
PetscErrorCode EnergyView(ECtx e,const char dir[],PetscInt step)
{
  PetscErrorCode ierr;
  char prefix[PETSC_MAX_PATH_LEN];
  const char *field_name[] = { "T" };
  char h5filename[PETSC_MAX_PATH_LEN];
  char stepname[PETSC_MAX_PATH_LEN];
  char stepfieldname[PETSC_MAX_PATH_LEN];
  
  if (!e->viewer_active) PetscFunctionReturn(0);
  
  if (e->output_hdf) {
    if (dir) {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"%s/energy.h5",dir);CHKERRQ(ierr);
    } else {
      ierr = PetscSNPrintf(h5filename,PETSC_MAX_PATH_LEN-1,"energy.h5");CHKERRQ(ierr);
    }
    ierr = PetscSNPrintf(stepname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D",step);CHKERRQ(ierr);
    ierr = DMTetSpaceAddGroupNameHDF5(h5filename,stepname);CHKERRQ(ierr);
    

    ierr = PetscSNPrintf(stepfieldname,PETSC_MAX_PATH_LEN-1,"timestep-%.4D/%s",step,field_name[0]);CHKERRQ(ierr);
    ierr = DMTetFieldSpaceView_HDF(e->dm_e,e->X,stepfieldname,h5filename);CHKERRQ(ierr);
  } else {
    
    if (dir) { ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_energy_",dir,step);CHKERRQ(ierr); }
    else {     ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_energy_",step);CHKERRQ(ierr); }
    
    {
      const char *energy_field_name[] = { "T", "vbar" };
      Vec fields[2];
      
      fields[0] = e->X;
      fields[1] = e->v_bar_e;
#ifdef SUBFUSC_USE_BINARY_PV
      ierr = DMTetViewFields_VTU_b(e->dm_e,2,fields,energy_field_name,prefix);CHKERRQ(ierr);
#elif
      ierr = DMTetViewFields_VTU(e->dm_e,2,fields,energy_field_name,prefix);CHKERRQ(ierr);
#endif
      //ierr = DMTetViewFields_PVTU(e->dm_e,2,fields,energy_field_name,prefix);CHKERRQ(ierr);
      ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_energy_",step);CHKERRQ(ierr);
      ierr = DMTetViewFields_PVTU2(e->dm_e,2,fields,energy_field_name,dir,prefix,NULL);CHKERRQ(ierr);
    }
  }
  
#ifdef SUBFUSC_DEBUG_OUTPUT
  if (dir) {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"%s/step-%.4D_%s_omega_t_",dir,step,field_name_phi[0]);CHKERRQ(ierr);
  } else {
    ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_%s_omega_t_",step,field_name_phi[0]);CHKERRQ(ierr);
  }
#ifdef SUBFUSC_USE_BINARY_PV
  ierr = DMTetViewFields_VTU(e->dm_e,1,&e->phi,field_name_phi,prefix);CHKERRQ(ierr);
#elif
  ierr = DMTetViewFields_VTU(e->dm_e,1,&e->phi,field_name_phi,prefix);CHKERRQ(ierr);
#endif
  //ierr = DMTetViewFields_PVTU(e->dm_e,1,&e->phi,field_name_phi,prefix);CHKERRQ(ierr);
  ierr = PetscSNPrintf(prefix,PETSC_MAX_PATH_LEN-1,"step-%.4D_%s_omega_t_",step,field_name_phi[0]);CHKERRQ(ierr);
  ierr = DMTetViewFields_PVTU2(e->dm_e,1,&e->phi,field_name_phi,dir,prefix,NULL);CHKERRQ(ierr);
#endif
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EnergySetTimestep"
PetscErrorCode EnergySetTimestep(ECtx e,PetscReal dt)
{
  e->delta_t = dt;
  PetscFunctionReturn(0);
}
