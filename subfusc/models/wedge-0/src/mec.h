
#if !defined(__MEC_H)
#define __MEC_H

#include <petsc.h>
#include <tpctx.h>

typedef enum {
  MCBUFFER_COOR=0,
  MCBUFFER_PRESSURE,
  MCBUFFER_TEMPERATURE,
  MCBUFFER_CRF,
  MCBUFFER_CH2O,
  MCBUFFER_SRATE_INV,
  MCBUFFER_POROSITY
} MechanicsCoeffBufferType;

/* Mechanics */
PetscErrorCode MechanicsContextCreate(MCtx *_m);
PetscErrorCode MechanicsContextDestroy(MCtx *_m);
PetscErrorCode MechanicsSetUp(MCtx m,const char meshfilename[],PetscInt pk);
PetscErrorCode MechanicsView(MCtx m,const char dir[],PetscInt step);

PetscErrorCode zero_uv(PetscScalar coor[],PetscInt dof,PetscScalar *val,void *ctx);
PetscErrorCode plate_uv(PetscScalar coor[],PetscInt dof,PetscScalar *val,void *ctx);
PetscErrorCode slab_uv(PetscScalar coor[],PetscInt dof,PetscScalar *val,void *ctx);
PetscErrorCode EvalVelocity_vanKeken(PetscReal coor[],PetscReal vel[]);
PetscErrorCode EvalVelocity_vanKeken_ramp(PetscReal coor[],PetscReal vel[],PetscReal d_depth,PetscReal d_length);
PetscErrorCode MechanicsSetCoefficientEvaluators(MCtx ctx,TPCtx tp);
PetscErrorCode MechanicsSetNeumannBC_DarcyFlux(MCtx ctx,TPCtx tpctx);
PetscErrorCode MechanicsSetDirichletBC(MCtx ctx,TPCtx tpctx);

PetscErrorCode MechanicsCoeffContainerCreate(MechanicsCoeffContainer *c);
PetscErrorCode MechanicsCoeffContainerDestroy(MechanicsCoeffContainer *c);
PetscErrorCode MechanicsCoeffContainerSetup(MechanicsCoeffContainer c,MCtx mctx);
PetscErrorCode MechanicsCoeffContainerSetupEta(MechanicsCoeffContainer c,const char default_typename[],const char limiter_typename[]);
PetscErrorCode MechanicsCoeffContainerSetupZeta(MechanicsCoeffContainer c,const char default_typename[],const char limiter_typename[]);
PetscErrorCode MechanicsCoeffContainerSetupEffectivePermability(MechanicsCoeffContainer c,const char default_typename[],const char limiter_typename[]);
PetscErrorCode MechanicsCoeffContainerGetValidCellBuffer(MechanicsCoeffContainer c,MechanicsCoeffBufferType btype,PetscBool use_si,const PetscReal **buffer);

/* Energy */
PetscErrorCode EnergyContextCreate(ECtx *_e);
PetscErrorCode EnergyContextDestroy(ECtx *_e);
PetscErrorCode EnergySetUp(ECtx e,const char meshfilename[],PetscInt pk);
PetscErrorCode EnergyView(ECtx e,const char dir[],PetscInt step);
PetscErrorCode EnergySetTimestep(ECtx e,PetscReal dt);

PetscErrorCode EnergySetCoefficientEvaluators_SteadyState(ECtx ctx,TPCtx tp);
PetscErrorCode EnergySetCoefficientEvaluators_TimeDependent(ECtx ctx,TPCtx tp);
PetscErrorCode EnergySetCoefficientEvaluators_TimeDependentSource(ECtx ctx,TPCtx tp);
PetscErrorCode EnergySetDirichletBC(ECtx ctx,TPCtx tp);

#endif
