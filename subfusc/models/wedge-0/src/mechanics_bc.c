
#include <petsc.h>
#include <petscdm.h>

#include <dmbcs.h>
#include <Batchelor_corner_flow_soln.h>
#include <tpctx.h>
#include <pdestokesdarcy2field.h>


#undef __FUNCT__
#define __FUNCT__ "zero_uv"
PetscErrorCode zero_uv(PetscScalar coor[],PetscInt dof,PetscScalar *val,void *ctx)
{
  if (dof < 0) {
    val[0] = 0.0;
    val[1] = 0.0;
  } else if (dof == 0) {
    *val = 0.0;
  } else if (dof == 1) {
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "plate_uv"
PetscErrorCode plate_uv(PetscScalar coor[],PetscInt dof,PetscScalar *val,void *ctx)
{
  if (dof < 0) {
    val[0] = 0.0;
    val[1] = 0.0;
  } else if (dof == 0) {
    *val = 0.0;
  } else if (dof == 1) {
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_uv"
PetscErrorCode slab_uv(PetscScalar coor[],PetscInt dof,PetscScalar *val,void *ctx)
{
  PetscReal slab_dip;

  slab_dip = SLAB_DIP*PETSC_PI/180.0;
  if (dof < 0) {
    val[0] =  PetscCosReal(slab_dip);
    val[1] = -PetscSinReal(slab_dip);
  } else if (dof == 0) {
    *val =  PetscCosReal(slab_dip);
  } else if (dof == 1) {
    *val = -PetscSinReal(slab_dip);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalVelocity_vanKeken"
PetscErrorCode EvalVelocity_vanKeken(PetscReal coor[],PetscReal vel[])
{
  double plate_thickness,slab_dip,depth,slab_depth,origin[2],veld[2],coord[2];
  
  plate_thickness = (double)PLATE_THICKNESS; // normalized
  slab_dip = SLAB_DIP*PETSC_PI/180.0;        // radians
  origin[0] = plate_thickness/tan(slab_dip);
  origin[1] = -plate_thickness;
  coord[0] = (double)coor[0];
  coord[1] = (double)coor[1];
  
  evaluate_BatchelorCornerFlowSoln(slab_dip,origin,coord,veld);

  depth = -coord[1];
  if (depth <= plate_thickness) { /* crust */
    veld[0] = 0.0;
    veld[1] = 0.0;
  }
  slab_depth = coord[0]*tan(slab_dip);
  if (depth > slab_depth - 1.0e-6) { /* slab */
    veld[0] =  cos(slab_dip);
    veld[1] = -sin(slab_dip);
  }
  vel[0] = (PetscReal)veld[0];
  vel[1] = (PetscReal)veld[1];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvalVelocity_vanKeken_ramp"
PetscErrorCode EvalVelocity_vanKeken_ramp(PetscReal coor[],PetscReal vel[],PetscReal decoupling_depth,PetscReal decoupling_ramp_length)
{
  double plate_thickness,slab_dip,depth,slab_depth,tanh_ramp,origin[2],veld[2],coord[2];
  
  plate_thickness = (double)PLATE_THICKNESS; // normalized
  slab_dip = SLAB_DIP*PETSC_PI/180.0;        // radians
  origin[0] = plate_thickness/tan(slab_dip);
  origin[1] = -plate_thickness;
  coord[0] = (double)coor[0];
  coord[1] = (double)coor[1];
  
  evaluate_BatchelorCornerFlowSoln(slab_dip,origin,coord,veld);

  depth = -coord[1];
  if (depth <= plate_thickness) { /* crust */
    veld[0] = 0.0;
    veld[1] = 0.0;
  }
  slab_depth = coord[0]*tan(slab_dip);
  tanh_ramp  = 0.5*(1.0+tanh((depth-decoupling_depth)/decoupling_ramp_length));
  if (depth > slab_depth - 1.0e-6) { /* slab */
    veld[0] =  cos(slab_dip)*tanh_ramp;
    veld[1] = -sin(slab_dip)*tanh_ramp;
  }
  vel[0] = (PetscReal)veld[0];
  vel[1] = (PetscReal)veld[1];
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "plate_boundary_base_uv"
PetscErrorCode plate_boundary_base_uv(PetscScalar coor[],PetscInt dof,
                                      PetscScalar *val,PetscBool *constrain,void *ctx)
{
  PetscReal depth,plate_thickness;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  PLATE_THICKNESS; // normalized
  *constrain = PETSC_FALSE;
  
  if (depth < plate_thickness+1.0e-10) { /* bottom of plate boundary */
    *constrain = PETSC_TRUE;
    *val = 0.0;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_uv2_ramp"
PetscErrorCode slab_boundary_uv2_ramp(PetscScalar coor[],PetscInt dof,
                                 PetscScalar *val,PetscBool *constrain,void *data)
{
  MCtx ctx = (MCtx)data;
  PetscReal vel[2],depth,slab_dip;
  PetscErrorCode ierr;
  
  *constrain = PETSC_FALSE;
  ierr = EvalVelocity_vanKeken_ramp(coor,vel,ctx->slab_decoupling_depth,ctx->slab_decoupling_distance);CHKERRQ(ierr);
  vel[0] = ctx->plate_velocity * vel[0];
  vel[1] = ctx->plate_velocity * vel[1];
 
  slab_dip = SLAB_DIP*PETSC_PI/180.0;

  if (coor[1] + 1.0e-10 > DOMAIN_YMIN) {
    if (coor[0] > PetscAbsReal(DOMAIN_YMIN)/PetscTanReal(slab_dip)) { /* bottom boundary */
      PetscFunctionReturn(0);
    }
  }
  
  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }

  depth = -coor[1];
  if (depth > coor[0]*PetscTanReal(slab_dip) - 1.0e-6) { /* slab */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "slab_boundary_uv2"
PetscErrorCode slab_boundary_uv2(PetscScalar coor[],PetscInt dof,
                                 PetscScalar *val,PetscBool *constrain,void *data)
{
  MCtx ctx = (MCtx)data;
  PetscReal vel[2],depth,slab_dip;
  PetscErrorCode ierr;
  
  *constrain = PETSC_FALSE;
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  vel[0] = ctx->plate_velocity * vel[0];
  vel[1] = ctx->plate_velocity * vel[1];
  
  slab_dip = SLAB_DIP*PETSC_PI/180.0;
  
  if (coor[1] + 1.0e-10 > DOMAIN_YMIN) {
    if (coor[0] > PetscAbsReal(DOMAIN_YMIN)/PetscTanReal(slab_dip)) { /* bottom boundary */
      PetscFunctionReturn(0);
    }
  }
  
  if (coor[0] < 1.0e-10) { /* slab inflow - left boundary */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  depth = -coor[1];
  if (depth > coor[0]*PetscTanReal(slab_dip) - 1.0e-6) { /* slab */
    *constrain = PETSC_TRUE;
    *val = vel[dof];
    PetscFunctionReturn(0);
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "wedge_inflow_Batchelor"
PetscErrorCode wedge_inflow_Batchelor(PetscScalar coor[],PetscInt dof,
                            PetscScalar *val,PetscBool *constrain,void *data)
{
  MCtx ctx = (MCtx)data;
  PetscReal vel[2],depth,plate_thickness;
  PetscErrorCode ierr;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  PLATE_THICKNESS; // normalized
  
  *constrain = PETSC_FALSE;
  
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  vel[0] = ctx->plate_velocity * vel[0];
  vel[1] = ctx->plate_velocity * vel[1];

  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) { /* right boundary */
    if (depth < plate_thickness) {
      if (dof == 0) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      } else if (dof == 1) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      }
    }
    if (depth > plate_thickness) { /* inflow from the mantle */
      
      if (vel[0] < 0.0) { /* inflow */
        *constrain = PETSC_TRUE;
        *val  = vel[dof];
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "wedge_inflow"
PetscErrorCode wedge_inflow(PetscScalar coor[],PetscInt dof,
                            PetscScalar *val,PetscBool *constrain,void *data)
{
  MCtx ctx = (MCtx)data;
  PetscReal vel[2],depth,plate_thickness;
  PetscErrorCode ierr;
  
  depth = PetscAbsReal(coor[1]);
  plate_thickness =  PLATE_THICKNESS; // normalized
  
  *constrain = PETSC_FALSE;
  
  ierr = EvalVelocity_vanKeken(coor,vel);CHKERRQ(ierr);
  vel[0] = ctx->plate_velocity * vel[0];
  vel[1] = ctx->plate_velocity * vel[1];
  
  if (coor[0] > (DOMAIN_XMAX - 1.0e-10)) { /* right boundary */
    if (depth < plate_thickness) {
      if (dof == 0) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      } else if (dof == 1) {
        *constrain = PETSC_TRUE;
        *val  = 0.0;
      }
    }
    if (depth > plate_thickness) {
      /* stress free under the plate throughout the mantle */
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsSetDirichletBC"
PetscErrorCode MechanicsSetDirichletBC(MCtx ctx,TPCtx tpctx)
{
  PetscInt region_plate,region_slab,region_wedge;
  BCList bc_V;
  PetscBool regularize = PETSC_TRUE;
  PetscErrorCode ierr;
  
  bc_V = ctx->bc_v_hat;
  
  region_plate = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_plate,0,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_plate,1,plate_boundary_base_uv,NULL);CHKERRQ(ierr);
  
  region_slab = 2;
  ierr = PetscOptionsGetBool(NULL,"mechanics.slab_bc.","-regularize_velocity",&regularize,NULL);CHKERRQ(ierr);
  if (regularize) {
    PetscReal value,value_si;
    PetscBool found;
    
    found  = PETSC_FALSE;
    ierr = PetscOptionGetReal_GeoLength("mechanics.slab_bc.","-decoupling_depth",&value,&value_si,&found);CHKERRQ(ierr);
    if (found) {
      value_si = value_si / tpctx->L;
      ctx->slab_decoupling_depth = value_si;
    }

    found  = PETSC_FALSE;
    ierr = PetscOptionGetReal_GeoLength("mechanics.slab_bc.","-decoupling_distance",&value,&value_si,&found);CHKERRQ(ierr);
    if (found) {
      value_si = value_si / tpctx->L;
      ctx->slab_decoupling_distance = value_si;
    }

    ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,0,slab_boundary_uv2_ramp,(void*)ctx);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,1,slab_boundary_uv2_ramp,(void*)ctx);CHKERRQ(ierr);
  } else {
    ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,0,slab_boundary_uv2,(void*)ctx);CHKERRQ(ierr);
    ierr = DMTetBCListTraverseByRegionId(bc_V,region_slab,1,slab_boundary_uv2,(void*)ctx);CHKERRQ(ierr);
  }
  
  region_wedge = 2;
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_wedge,0,wedge_inflow,(void*)ctx);CHKERRQ(ierr);
  ierr = DMTetBCListTraverseByRegionId(bc_V,region_wedge,1,wedge_inflow,(void*)ctx);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_null"
PetscErrorCode DarcyFluxEvaluator_null(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  flux[0] = 0.0;
  flux[1] = 0.0;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_slab_wedge_const"
PetscErrorCode DarcyFluxEvaluator_slab_wedge_const(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  TPCtx ctx = (TPCtx)data;
  PetscReal depth,slab_dip;

  depth = -point->coor[1];
  slab_dip = SLAB_DIP*PETSC_PI/180.0;
  if (depth > point->coor[0]*PetscTanReal(slab_dip) - 1.0e-6) { /* slab - wedge */
    
    /* the negative sign next to the normal - this is required as normal[] refers to the outward pointing normal to the boundary */
    flux[0] = -1.0 * (ctx->Vdarcy) * (-point->normal[0]);
    flux[1] = -1.0 * (ctx->Vdarcy) * (-point->normal[1]);
  } else {
    point->values_set = PETSC_FALSE;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_slab_wedge_gaussian"
PetscErrorCode DarcyFluxEvaluator_slab_wedge_gaussian(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  TPCtx ctx = (TPCtx)data;
  PetscReal gaussian_weight;
  
  if (point->coor[1] < -point->coor[0] + 1.0e-10) { /* slab - wedge */
    gaussian_weight = PetscExpReal(-(-point->coor[1]-ctx->gaussian_depth)*(-point->coor[1]-ctx->gaussian_depth)/(ctx->gaussian_width * ctx->gaussian_width));

    
    /* the negative sign next to the normal - this is required as normal[] refers to the outward pointing normal to the boundary */
    flux[0] = -1.0 * (ctx->Vdarcy) * (-point->normal[0]) * gaussian_weight;
    flux[1] = -1.0 * (ctx->Vdarcy) * (-point->normal[1]) * gaussian_weight;
  } else {
    point->values_set = PETSC_FALSE;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyFluxEvaluator_slab_inflow_outflow_const"
PetscErrorCode DarcyFluxEvaluator_slab_inflow_outflow_const(EvaluationPoint point,PetscReal time,PetscReal flux[],void *data)
{
  PetscReal slab_dip = SLAB_DIP*PETSC_PI/180.0;

  if (point->coor[0] > DOMAIN_XMAX - 1.0e-10) { /* right */
    flux[0] = 0.0;
    flux[1] = 0.0;
  } else if (point->coor[1] < DOMAIN_YMIN + 1e-10) { /* bottom */
    if (point->coor[0] > PetscAbsReal(DOMAIN_YMIN)/PetscTanReal(slab_dip)) { /* right-most segment */
      flux[0] = 0.0;
      flux[1] = 0.0;
    }
  } else {
    point->values_set = PETSC_FALSE;
  }
  PetscFunctionReturn(0);
}

PetscErrorCode coeff_pointwise_SurfaceQuadratureEvaluator(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data);

#undef __FUNCT__
#define __FUNCT__ "DarcyVel_SurfaceQuadratureEvaluator_method_b"
PetscErrorCode DarcyVel_SurfaceQuadratureEvaluator_method_b(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  TPCtx ctx;
  
  PetscFunctionBegin;
  ctx = (TPCtx)data;
  dm = ctx->mechanics->dm_p_hat;
  
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_null,NULL);CHKERRQ(ierr);

  ierr = coeff_pointwise_SurfaceQuadratureEvaluator(c,quadrature,eregion,data);CHKERRQ(ierr);

  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_slab_wedge_const,(void*)ctx);CHKERRQ(ierr);
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_slab_inflow_outflow_const,(void*)ctx);CHKERRQ(ierr);
  
  /*
  {
    PetscReal *q_coor,*q_phi,*q_k,*q_flux;
    PetscInt nqp,nen,q;
    
    ierr = QuadratureGetProperty(quadrature,"coor",&nen,&nqp,NULL,&q_coor);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature,"phi", &nen,&nqp,NULL,&q_phi);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature,"k*",  &nen,&nqp,NULL,&q_k);CHKERRQ(ierr);
    ierr = QuadratureGetProperty(quadrature,"q_N", &nen,&nqp,NULL,&q_flux);CHKERRQ(ierr);
    printf("# (x y)   phi k vd[0] vd[1]\n");
    for (q=0; q<nqp*nen; q++) {
      printf("MB %+1.4e %+1.4e   %+1.4e %+1.4e %+1.4e %+1.4e\n",q_coor[2*q],q_coor[2*q+1],q_phi[q],q_k[q],q_flux[2*q],q_flux[2*q+1]);
    }
  }
  */
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DarcyVel_SurfaceQuadratureEvaluator_method_c"
PetscErrorCode DarcyVel_SurfaceQuadratureEvaluator_method_c(Coefficient c,Quadrature quadrature,PetscInt eregion[],void *data)
{
  PetscErrorCode ierr;
  DM dm;
  TPCtx ctx;
  
  PetscFunctionBegin;
  ctx = (TPCtx)data;
  dm = ctx->mechanics->dm_p_hat;
  
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_null,NULL);CHKERRQ(ierr);
  
  ierr = coeff_pointwise_SurfaceQuadratureEvaluator(c,quadrature,eregion,data);CHKERRQ(ierr);
  
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_slab_wedge_gaussian,(void*)ctx);CHKERRQ(ierr);
  ierr = SurfaceQuadratureIterator(quadrature,dm,"q_N",0.0,DarcyFluxEvaluator_slab_inflow_outflow_const,(void*)ctx);CHKERRQ(ierr);
  
  /*
   {
   PetscReal *q_coor,*q_phi,*q_k,*q_flux;
   PetscInt nqp,nen,q;
   
   ierr = QuadratureGetProperty(quadrature,"coor",&nen,&nqp,NULL,&q_coor);CHKERRQ(ierr);
   ierr = QuadratureGetProperty(quadrature,"phi", &nen,&nqp,NULL,&q_phi);CHKERRQ(ierr);
   ierr = QuadratureGetProperty(quadrature,"k*",  &nen,&nqp,NULL,&q_k);CHKERRQ(ierr);
   ierr = QuadratureGetProperty(quadrature,"q_N", &nen,&nqp,NULL,&q_flux);CHKERRQ(ierr);
   printf("# (x y)   phi k vd[0] vd[1]\n");
   for (q=0; q<nqp*nen; q++) {
   printf("MB %+1.4e %+1.4e   %+1.4e %+1.4e %+1.4e %+1.4e\n",q_coor[2*q],q_coor[2*q+1],q_phi[q],q_k[q],q_flux[2*q],q_flux[2*q+1]);
   }
   }
   */
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MechanicsSetNeumannBC_DarcyFlux"
PetscErrorCode MechanicsSetNeumannBC_DarcyFlux(MCtx ctx,TPCtx tpctx)
{
  PetscInt bc_type = 0;
  Coefficient qN;
  PetscErrorCode ierr;
  
  ierr = PetscOptionsGetInt(NULL,NULL,"-mechanics.darcy_flux.bc_type",&bc_type,NULL);CHKERRQ(ierr);
  ierr = PDEStokesDarcy2FieldGetCoefficients(ctx->pde,NULL,NULL,NULL,&qN);CHKERRQ(ierr);
  switch (bc_type) {
    case 0:
      /* domain is closed */
      break;
      
    case 1:
    {
      PetscReal Vdarcy;
      
      /* set a non-zero default */
      Vdarcy = 1.0 / (1.0e3 * 365.0 * 24.0 * 60.0 * 60.0); /* 1.0 m/kyr */
      ierr = PetscOptionsGetReal(NULL,NULL,"-mechanics.darcy_flux.slab_wedge",&Vdarcy,NULL);CHKERRQ(ierr);
      Vdarcy = Vdarcy / tpctx->V;
      tpctx->Vdarcy = Vdarcy;
      
      ierr = CoefficientSetComputeQuadrature(qN,DarcyVel_SurfaceQuadratureEvaluator_method_b,NULL,(void*)tpctx);CHKERRQ(ierr);
    }
      break;
      
    case 2:
    {
      PetscReal Vdarcy;
      PetscReal gaussian_depth;
      PetscReal gaussian_width;

      
      /* set a non-zero default */
      Vdarcy = 1.0 / (1.0e3 * 365.0 * 24.0 * 60.0 * 60.0); /* 1.0 m/kyr */
      gaussian_depth = 150.0e3;
      gaussian_width = 40.0e3;
      
      ierr = PetscOptionsGetReal(NULL,NULL,"-mechanics.darcy_flux.gaussian.magnitude",&Vdarcy,NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(NULL,NULL,"-mechanics.darcy_flux.gaussian.depth",&gaussian_depth,NULL);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(NULL,NULL,"-mechanics.darcy_flux.gaussian.width",&gaussian_width,NULL);CHKERRQ(ierr);

      Vdarcy = Vdarcy / tpctx->V;
      gaussian_depth = gaussian_depth / tpctx->L;
      gaussian_width = gaussian_width / tpctx->L;

      tpctx->Vdarcy = Vdarcy;
      tpctx->gaussian_depth = gaussian_depth;
      tpctx->gaussian_width = gaussian_width;

      ierr = CoefficientSetComputeQuadrature(qN,DarcyVel_SurfaceQuadratureEvaluator_method_c,NULL,(void*)tpctx);CHKERRQ(ierr);
    }
      break;
      
    default:
      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Valid values for -mechanics.wedge.darcy_flux.bc_type are {0,1,2}");
      break;
  }
  if (bc_type == 0) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Implementation limitation: Currently only -mechanics.darcy_flux.bc_type {1,2} is supported by the DarcyCtx");
  }
  
  PetscFunctionReturn(0);
}
