
#ifndef __PointWisePhysics_h__
#define __PointWisePhysics_h__

/* define useful constants here */
#define PWP_GAS_CONSTANT 8.3145 // [j/mol K]

#include <petsc.h>

#define PWP_ND_UNITS PETSC_FALSE
#define PWP_SI_UNITS PETSC_TRUE
#define PWP_MAX_REFS 10

typedef struct _p_PWPhysics *PWPhysics;


struct _p_PWPhysics {
  PetscBool use_si;
  char type[PETSC_MAX_PATH_LEN];
  char name[PETSC_MAX_PATH_LEN];
  PetscInt nreferences;
  char **reference;
  PetscInt region;
  PetscInt naux;
  PetscReal **aux_data;
  char **aux_data_names; /* used as hints for debugging only */
  size_t size;
  void *data;
  PetscBool aux_data_owned;
  PetscErrorCode (*evaluate)(void*,PetscInt,PetscInt,PetscReal**,PetscReal*);
  PetscErrorCode (*view)(void*,PetscViewer);
  PetscErrorCode (*destroy)(void*);
  PetscErrorCode (*setfromoptions)(const char*,void*);
 };

///
/// use_si: indicates whether parameters are defined in SI units
/// type: coefficient + name for physics to be evaluated
/// name: string identifying a particular instance of the physics, e.g. name="olivine" might imply params are specified for olivine
/// reference: user note, and or citation to parameters or the definition of the physics
/// region: flag to indicate if evaluation should only occur in a particular region
/// evaluate(): method to evaluate physics
/// view(): method to display parameters
///

PetscErrorCode PWPhysicsCreate(size_t size,const char type[],const char name[],PetscBool use_si,PetscInt region,PWPhysics *p);

PetscErrorCode PWPhysicsSetName(PWPhysics f,const char name[]);

PetscErrorCode PWPhysicsSetNumberAuxiliaryFields(PWPhysics f,PetscInt naux,const char *aux_names[]);

PetscErrorCode PWPhysicsAddReference(PWPhysics f,const char reference[]);

PetscErrorCode PWPhysicsEvaluate(PWPhysics p,PetscInt npoints,PetscReal value[]);

PetscErrorCode PWPhysicsView(PWPhysics f,PetscViewer v);

PetscErrorCode PWPhysicsSetFromOptions(PWPhysics f);

#define PWPhysicsSetEvaluate(p,method) (p)->evaluate = ( PetscErrorCode (*)(void*,PetscInt,PetscInt,PetscReal**,PetscReal*) )(method);
#define PWPhysicsSetView(p,method) (p)->view = ( PetscErrorCode (*)(void*,PetscViewer) )(method);
#define PWPhysicsSetDestroy(p,method) (p)->destroy = ( PetscErrorCode (*)(void*) )(method);
#define PWPhysicsSetSetFromOptions(p,method) (p)->setfromoptions = ( PetscErrorCode (*)(const char*,void*) )(method);

PetscErrorCode PWPhysicsDestroy(PWPhysics *f);

PetscErrorCode PWPhysicsTypeCheck(PWPhysics p,const char type[]);

PetscErrorCode PWPhysicsTypeCompare(PWPhysics p,const char type[],PetscBool *same);

PetscErrorCode PWPhysicsGetUseSI(PWPhysics p,PetscBool *use_si);

PetscErrorCode PWPhysicsDuplicate(PWPhysics p,PWPhysics *dup);

PetscErrorCode PWPhysicsSetAuxiliaryDataOwnership(PWPhysics phys,PetscInt len);

PetscErrorCode PWPhysicsSetAuxiliaryDataBuffer(PWPhysics phys,PetscInt index,PetscReal *buffer);

PetscErrorCode PWPhysicsGetAuxiliaryDataIndexByName(PWPhysics phys,const char name[],PetscInt *index);
PetscErrorCode PWPhysicsGetAuxiliaryData(PWPhysics phys,PetscInt index,PetscReal **buffer);
PetscErrorCode PWPhysicsGetAuxiliaryDataRead(PWPhysics phys,PetscInt index,const PetscReal **buffer);

#endif
