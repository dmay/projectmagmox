
#include <petsc.h>
#include <proj_init_finalize.h>
#include <pointwise_physics.h>
#include <pointwise_mech_coeff.h>

#undef __FUNCT__
#define __FUNCT__ "pwphys_test_1"
PetscErrorCode pwphys_test_1(void)
{
  PetscErrorCode ierr;
  PWPhysics eta_creep_const;
  PWPhysics eta_creep_diff;
  PetscReal aux_T[4],eta[4];
  
  ierr = EtaConstantCreate(PETSC_FALSE,0,"default",&eta_creep_const);CHKERRQ(ierr);
  ierr = PWPhysicsView(eta_creep_const,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&eta_creep_const);CHKERRQ(ierr);
  

  ierr = EtaDiffusionCreepCreate_PvKPEPI2008(0,&eta_creep_diff);CHKERRQ(ierr);
  //ierr = EtaDiffusionCreepCreate(PETSC_FALSE,0,"default",&eta_creep_diff);CHKERRQ(ierr);
  ierr = PWPhysicsView(eta_creep_diff,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  
  aux_T[0] = 3000.0;
  aux_T[1] = 1300.0;
  aux_T[2] = 1000.0;
  aux_T[3] = 800.0;
  eta_creep_diff->aux_data[0] = aux_T;
  ierr = PWPhysicsEvaluate(eta_creep_diff,4,&eta[0]);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"eta { %1.4e %1.4e %1.4e %1.4e }\n",eta[0],eta[1],eta[2],eta[3]);
  
  ierr = PWPhysicsDestroy(&eta_creep_diff);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pwphys_test_2"
PetscErrorCode pwphys_test_2(void)
{
  PetscErrorCode ierr;
  PWPhysics eta_creep_const;
  PWPhysics eta_creep_diff;
  PWPhysics eta_creep_comp;
  PetscReal aux_T[4],eta[4];

  
  /* Obtain temperature values */
  aux_T[0] = 3000.0;
  aux_T[1] = 1300.0;
  aux_T[2] = 1000.0;
  aux_T[3] = 800.0;

  ierr = EtaConstantCreate(PETSC_TRUE,0,"ref",&eta_creep_const);CHKERRQ(ierr);
  ierr = EtaConstantSetParams(eta_creep_const,1.0e30);CHKERRQ(ierr);
  /* configure from the command line using -pwp.eta.Constant.ref.eta0 1.0e32 */
  ierr = PWPhysicsSetFromOptions(eta_creep_const);CHKERRQ(ierr);

  ierr = EtaDiffusionCreepCreate_PvKPEPI2008(0,&eta_creep_diff);CHKERRQ(ierr);
  eta_creep_diff->aux_data[0] = aux_T;

  ierr = EtaCompositeCreate(PETSC_TRUE,0,"comp",&eta_creep_comp);CHKERRQ(ierr);
  ierr = EtaCompositeSetFlowLaw(eta_creep_comp,eta_creep_const);CHKERRQ(ierr);
  ierr = EtaCompositeSetFlowLaw(eta_creep_comp,eta_creep_diff);CHKERRQ(ierr);
  ierr = EtaCompositeSetBufferLength(eta_creep_comp,4);CHKERRQ(ierr);
  
  ierr = PWPhysicsView(eta_creep_comp,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  
  ierr = PWPhysicsEvaluate(eta_creep_comp,4,&eta[0]);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"eta { %1.4e %1.4e %1.4e %1.4e }\n",eta[0],eta[1],eta[2],eta[3]);
  
  ierr = PWPhysicsDestroy(&eta_creep_const);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&eta_creep_diff);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* radial limiter */
#undef __FUNCT__
#define __FUNCT__ "pwphys_test_3"
PetscErrorCode pwphys_test_3(void)
{
  PetscErrorCode ierr;
  PWPhysics limiter_radial;
  PetscReal *aux_c,*field,*field0;
  PetscInt npoints,nx,i,j,p;
  PetscReal dx,origin[2];
  
  nx = 33;
  npoints = nx * nx;
  dx = 12.0 / ((PetscReal)(nx-1));
  
  ierr = PetscMalloc1(npoints*2,&aux_c);CHKERRQ(ierr);
  ierr = PetscMalloc1(npoints,&field0);CHKERRQ(ierr);
  ierr = PetscMalloc1(npoints,&field);CHKERRQ(ierr);
  
  p = 0;
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      aux_c[2*p+0] = -1.0 + i * dx;
      aux_c[2*p+1] = -1.0 + j * dx;
      p++;
    }
  }

  p = 0;
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      field0[p] = 20.0 + aux_c[2*p+0] + aux_c[2*p+1];
      field[p] = field0[p];
      p++;
    }
  }
  
  ierr = LimiterRadialCreate(PETSC_TRUE,0,"ref",&limiter_radial);CHKERRQ(ierr);
  ierr = PWPhysicsSetAuxiliaryDataBuffer(limiter_radial,0,aux_c);CHKERRQ(ierr);

  origin[0] = 5.0;
  origin[1] = 5.0;
  ierr = LimiterRadialSetParams(limiter_radial,"default",5.0,0.01,3.0,origin);CHKERRQ(ierr);
  /* configure from the command line using -pwp.eta.Constant.ref.eta0 1.0e32 */
  ierr = PWPhysicsSetFromOptions(limiter_radial);CHKERRQ(ierr);
  
  ierr = PWPhysicsEvaluate(limiter_radial,npoints,field);CHKERRQ(ierr);

  p = 0;
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      printf("%+1.4e %+1.4e %+1.4e %+1.4e\n",aux_c[2*p+0],aux_c[2*p+1],field0[p],field[p]);
      p++;
    }
    printf("\n");
  }
  
  ierr = PWPhysicsView(limiter_radial,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = PWPhysicsDestroy(&limiter_radial);CHKERRQ(ierr);
  ierr = PetscFree(aux_c);CHKERRQ(ierr);
  ierr = PetscFree(field0);CHKERRQ(ierr);
  ierr = PetscFree(field);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* composite limiter */
#undef __FUNCT__
#define __FUNCT__ "pwphys_test_4"
PetscErrorCode pwphys_test_4(void)
{
  PetscErrorCode ierr;
  PWPhysics limiter_minmax,limiter_radial,limiter_composite;
  PetscReal *aux_c,*field,*field0;
  PetscInt npoints,nx,i,j,p;
  PetscReal dx,origin[2];
  
  nx = 33;
  npoints = nx * nx;
  dx = 12.0 / ((PetscReal)(nx-1));
  
  ierr = PetscMalloc1(npoints*2,&aux_c);CHKERRQ(ierr);
  ierr = PetscMalloc1(npoints,&field0);CHKERRQ(ierr);
  ierr = PetscMalloc1(npoints,&field);CHKERRQ(ierr);
  
  p = 0;
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      aux_c[2*p+0] = -1.0 + i * dx;
      aux_c[2*p+1] = -1.0 + j * dx;
      p++;
    }
  }
  
  p = 0;
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      field0[p] = 20.0 + aux_c[2*p+0] + aux_c[2*p+1];
      field[p] = field0[p];
      p++;
    }
  }

  ierr = LimiterMinMaxCreate(PETSC_TRUE,0,"f0",&limiter_minmax);CHKERRQ(ierr);
  ierr = LimiterMinMaxSetParams(limiter_minmax,"default",25.0,1000.0);CHKERRQ(ierr);
  ierr = PWPhysicsSetFromOptions(limiter_minmax);CHKERRQ(ierr);
  
  ierr = LimiterRadialCreate(PETSC_TRUE,0,"f1",&limiter_radial);CHKERRQ(ierr);
  ierr = PWPhysicsSetAuxiliaryDataBuffer(limiter_radial,0,aux_c);CHKERRQ(ierr);
  origin[0] = 2.0;
  origin[1] = 2.0;
  ierr = LimiterRadialSetParams(limiter_radial,"default",5.0,0.01,4.0,origin);CHKERRQ(ierr);
  /* configure from the command line using -pwp.eta.Constant.ref.eta0 1.0e32 */
  ierr = PWPhysicsSetFromOptions(limiter_radial);CHKERRQ(ierr);
  
  
  ierr = LimiterCompositeCreate(PETSC_TRUE,0,"f_limited",&limiter_composite);CHKERRQ(ierr);
  ierr = LimiterCompositeSetLaw(limiter_composite,limiter_minmax);CHKERRQ(ierr);
  ierr = LimiterCompositeSetLaw(limiter_composite,limiter_radial);CHKERRQ(ierr);
  
  ierr = PWPhysicsView(limiter_composite,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  
  ierr = PWPhysicsEvaluate(limiter_composite,npoints,field);CHKERRQ(ierr);
  
  p = 0;
  for (j=0; j<nx; j++) {
    for (i=0; i<nx; i++) {
      printf("%+1.4e %+1.4e %+1.4e %+1.4e\n",aux_c[2*p+0],aux_c[2*p+1],field0[p],field[p]);
      p++;
    }
    printf("\n");
  }
  
  ierr = PWPhysicsView(limiter_composite,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  
  ierr = PWPhysicsDestroy(&limiter_composite);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&limiter_radial);CHKERRQ(ierr);
  ierr = PWPhysicsDestroy(&limiter_minmax);CHKERRQ(ierr);
  ierr = PetscFree(aux_c);CHKERRQ(ierr);
  ierr = PetscFree(field0);CHKERRQ(ierr);
  ierr = PetscFree(field);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}


int main(int argc,char **argv)
{
  PetscErrorCode ierr;
  
  ierr = projInitialize(&argc,&argv,(char *)0,NULL);CHKERRQ(ierr);
  
  //ierr = pwphys_test_1();CHKERRQ(ierr);
  //ierr = pwphys_test_2();CHKERRQ(ierr);
  //ierr = pwphys_test_3();CHKERRQ(ierr);
  ierr = pwphys_test_4();CHKERRQ(ierr);
  
  ierr = projFinalize();CHKERRQ(ierr);
  return 0;
}

