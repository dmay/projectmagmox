
#include <petsc.h>
#include <pointwise_physics.h>

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsCreate"
PetscErrorCode PWPhysicsCreate(size_t size,const char type[],const char name[],PetscBool use_si,PetscInt region,PWPhysics *p)
{
  PWPhysics f;
  PetscInt k;
  PetscErrorCode ierr;
  
  ierr = PetscMalloc1(1,&f);CHKERRQ(ierr);
  ierr = PetscMemzero(f,sizeof(struct _p_PWPhysics));CHKERRQ(ierr);
  
  f->use_si = use_si;
  f->region = region;
  if (!type) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"A type must be provided");
  PetscSNPrintf(f->type,PETSC_MAX_PATH_LEN-1,"%s",type);
  if (name) {
    PetscSNPrintf(f->name,PETSC_MAX_PATH_LEN-1,"%s",name);
  }
  f->nreferences = 0;
  ierr = PetscMalloc1(PWP_MAX_REFS,&f->reference);CHKERRQ(ierr);
  for (k=0; k<PWP_MAX_REFS; k++) {
    f->reference[k] = NULL;
  }
  f->data = NULL;
  f->size = size;
  ierr = PetscMalloc(size,&f->data);CHKERRQ(ierr);
  ierr = PetscMemzero(f->data,size);CHKERRQ(ierr);
  f->naux = 0;
  f->aux_data = NULL;
  f->aux_data_names = NULL;
  f->evaluate = NULL;
  f->view = NULL;
  f->aux_data_owned = PETSC_FALSE;
  *p = f;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsSetName"
PetscErrorCode PWPhysicsSetName(PWPhysics f,const char name[])
{
  if (name) { PetscSNPrintf(f->name,PETSC_MAX_PATH_LEN-1,"%s",name); }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsSetNumberAuxiliaryFields"
PetscErrorCode PWPhysicsSetNumberAuxiliaryFields(PWPhysics f,PetscInt naux,const char *aux_names[])
{
  PetscErrorCode ierr;
  PetscInt a;
  
  if (f->aux_data) {
    ierr = PetscFree(f->aux_data);CHKERRQ(ierr);
    for (a=0; a<f->naux; a++) {
      ierr = PetscFree(f->aux_data_names[a]);CHKERRQ(ierr);
    }
    ierr = PetscFree(f->aux_data_names);CHKERRQ(ierr);
  } else {
    ierr = PetscMalloc1(naux,&f->aux_data);CHKERRQ(ierr);
    for (a=0; a<naux; a++) {
      f->aux_data[a] = NULL;
    }
    ierr = PetscMalloc1(naux,&f->aux_data_names);CHKERRQ(ierr);
    for (a=0; a<naux; a++) {
      ierr = PetscStrallocpy(aux_names[a],&f->aux_data_names[a]);CHKERRQ(ierr);
    }
  }
  f->naux = naux;
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsAddReference"
PetscErrorCode PWPhysicsAddReference(PWPhysics f,const char reference[])
{
  PetscErrorCode ierr;
  
  if (reference) {
    char *note;
    
    if ( f->nreferences == PWP_MAX_REFS) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cannot add more than %D references",PWP_MAX_REFS);
    
    ierr = PetscMalloc1(PETSC_MAX_PATH_LEN,&f->reference[f->nreferences]);CHKERRQ(ierr);
    note = f->reference[f->nreferences];
    ierr = PetscSNPrintf(note,PETSC_MAX_PATH_LEN-1,"%s",reference);CHKERRQ(ierr);
    f->nreferences++;
    
    /*
    if (f->reference[0] == '\0') {
      PetscSNPrintf(f->reference,PETSC_MAX_PATH_LEN-1,"%s",reference);
    } else {
      char note[PETSC_MAX_PATH_LEN];
      
      PetscSNPrintf(note,PETSC_MAX_PATH_LEN-1,"%s\n",f->reference);
      PetscSNPrintf(f->reference,PETSC_MAX_PATH_LEN-1,"%s%s",note,reference);
    }
    */
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsEvaluate"
PetscErrorCode PWPhysicsEvaluate(PWPhysics p,PetscInt npoints,PetscReal value[])
{
  PetscErrorCode ierr;
  PetscInt a;
  
  for (a=0; a<p->naux; a++) {
    if (p->aux_data[a] == NULL) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Requires %D auxillary fields: Slot %D is NULL",p->naux,a);
  }
  
  ierr = p->evaluate(p->data,npoints,p->naux,p->aux_data,value);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsView"
PetscErrorCode PWPhysicsView(PWPhysics f,PetscViewer v)
{
  PetscErrorCode ierr;
  char pre_opt[PETSC_MAX_PATH_LEN];

  if (!f) {
    PetscViewerASCIIPrintf(v,"[PWPhysics] is NULL\n");
    PetscFunctionReturn(0);
  }
  PetscViewerASCIIPrintf(v,"[PWPhysicsView]\n");
  ierr = PetscViewerASCIIPushTab(v);CHKERRQ(ierr);
  PetscViewerASCIIPrintf(v,"type:          \"%s\"\n",f->type);
  PetscViewerASCIIPrintf(v,"name:          \"%s\"\n",f->name);
  PetscViewerASCIIPrintf(v,"region:        %D\n",f->region);
  if (f->use_si) {
    PetscViewerASCIIPrintf(v,"SI units:      true\n");
  } else {
    PetscViewerASCIIPrintf(v,"SI units:      false\n");
  }

  PetscSNPrintf(pre_opt,PETSC_MAX_PATH_LEN-1,"pwp.%s.",f->type);
  if (f->name[0] != '\0') {
    PetscSNPrintf(pre_opt,PETSC_MAX_PATH_LEN-1,"pwp.%s.%s.",f->type,f->name);
  }
  PetscViewerASCIIPrintf(v,"option prefix: -%s\n",pre_opt);

  if (f->nreferences > 0) {
    PetscInt k;
    
    PetscViewerASCIIPrintf(v,"References:\n");
    ierr = PetscViewerASCIIPushTab(v);CHKERRQ(ierr);
    for (k=0; k<f->nreferences; k++) {
      PetscViewerASCIIPrintf(v,"%s\n",f->reference[k]);
    }
    ierr = PetscViewerASCIIPopTab(v);CHKERRQ(ierr);
  }
  if (f->view) {
    PetscViewerASCIIPrintf(v,"Parameters:\n");
    ierr = PetscViewerASCIIPushTab(v);CHKERRQ(ierr);
    ierr = f->view(f->data,v);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(v);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(v);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsDestroy"
PetscErrorCode PWPhysicsDestroy(PWPhysics *f)
{
  PetscErrorCode ierr;
  PWPhysics phys;
  PetscInt a;
  
  if (!f) PetscFunctionReturn(0);
  phys = *f;
  if (!phys) PetscFunctionReturn(0);
  if (phys->naux != 0) {
    if (phys->aux_data_owned) {
      for (a=0; a<phys->naux; a++) {
        ierr = PetscFree(phys->aux_data[a]);CHKERRQ(ierr);
      }
    }
    ierr = PetscFree(phys->aux_data);CHKERRQ(ierr);
    for (a=0; a<phys->naux; a++) {
      ierr = PetscFree(phys->aux_data_names[a]);CHKERRQ(ierr);
    }
    ierr = PetscFree(phys->aux_data_names);CHKERRQ(ierr);
  }
  for (a=0; a<phys->nreferences; a++) {
    ierr = PetscFree(phys->reference[a]);
  }
  ierr = PetscFree(phys->reference);CHKERRQ(ierr);
  if (phys->destroy) {
    ierr = phys->destroy(phys->data);CHKERRQ(ierr);
  }
  ierr = PetscFree(phys->data);CHKERRQ(ierr);
  ierr = PetscFree(phys);CHKERRQ(ierr);
  *f = NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsSetFromOptions"
PetscErrorCode PWPhysicsSetFromOptions(PWPhysics f)
{
  PetscErrorCode ierr;
  char pre_opt[PETSC_MAX_PATH_LEN];
  
  if (!f) {
    PetscPrintf(PETSC_COMM_SELF,"[PWPhysics] is NULL\n");
    PetscFunctionReturn(0);
  }
  if (f->setfromoptions) {
    PetscSNPrintf(pre_opt,PETSC_MAX_PATH_LEN-1,"pwp.%s.",f->type);
    if (f->name[0] != '\0') {
      PetscSNPrintf(pre_opt,PETSC_MAX_PATH_LEN-1,"pwp.%s.%s.",f->type,f->name);
    }
    ierr = f->setfromoptions((const char*)pre_opt,f->data);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsTypeCheck"
PetscErrorCode PWPhysicsTypeCheck(PWPhysics p,const char type[])
{
  PetscErrorCode ierr;
  PetscBool issame = PETSC_FALSE;
  ierr = PetscStrcmp(p->type,type,&issame);CHKERRQ(ierr);
  if (!issame) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Types do not match. Input has type \"%s\"\n",p->type);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsTypeCompare"
PetscErrorCode PWPhysicsTypeCompare(PWPhysics p,const char type[],PetscBool *same)
{
  PetscErrorCode ierr;
  *same = PETSC_FALSE;
  ierr = PetscStrcmp(p->type,type,same);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsGetUseSI"
PetscErrorCode PWPhysicsGetUseSI(PWPhysics p,PetscBool *use_si)
{
  if (use_si) {
    *use_si = p->use_si;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsDuplicate"
PetscErrorCode PWPhysicsDuplicate(PWPhysics p,PWPhysics *dup)
{
  PWPhysics pp;
  PetscInt r;
  PetscErrorCode ierr;
  
  ierr = PWPhysicsCreate(p->size,p->type,p->name,p->use_si,p->region,&pp);CHKERRQ(ierr);
  for (r=0; r<p->nreferences; r++) {
    ierr = PWPhysicsAddReference(pp,p->reference[r]);CHKERRQ(ierr);
  }
  ierr = PWPhysicsSetNumberAuxiliaryFields(pp,p->naux,(const char**)p->aux_data_names);CHKERRQ(ierr);
  ierr = PetscMemcpy(pp->data,p->data,p->size);CHKERRQ(ierr);
  pp->evaluate = p->evaluate;
  pp->view     = p->view;
  pp->destroy  = p->destroy;
  pp->setfromoptions = p->setfromoptions;
  *dup = pp;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsSetAuxiliaryDataOwnership"
PetscErrorCode PWPhysicsSetAuxiliaryDataOwnership(PWPhysics phys,PetscInt len)
{
  PetscInt a;
  PetscErrorCode ierr;
  
  phys->aux_data_owned = PETSC_TRUE;
  for (a=0; a<phys->naux; a++) {
    ierr = PetscMalloc1(len,&phys->aux_data[a]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsSetAuxiliaryDataBuffer"
PetscErrorCode PWPhysicsSetAuxiliaryDataBuffer(PWPhysics phys,PetscInt index,PetscReal *buffer)
{
  if (phys->aux_data_owned) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set a user buffer as an auxiliary field if PWPhysics allocated the auxiliary data");
  if (!phys->aux_data) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. No auxiliary fields have been registered. Must call PWPhysicsSetNumberAuxiliaryFields() first",index);
  if (index < phys->naux) {
    if (phys->aux_data[index]) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. A non-null auxiliary already exists in this slot",index);
    phys->aux_data[index] = buffer;
  } else SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. Max registered number of auxiliary fields is %D",phys->naux,index);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsGetAuxiliaryDataIndexByName"
PetscErrorCode PWPhysicsGetAuxiliaryDataIndexByName(PWPhysics phys,const char name[],PetscInt *index)
{
  PetscInt a;
  PetscErrorCode ierr;
  
  if (!phys->aux_data) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot get auxiliary field index \"%s\" as none have been registered. Must call PWPhysicsSetNumberAuxiliaryFields() first",name);
  *index = -1;
  for (a=0; a<phys->naux; a++) {
    PetscBool issame = PETSC_FALSE;
    
    ierr = PetscStrcmp(phys->aux_data_names[a],name,&issame);CHKERRQ(ierr);
    if (issame) {
      *index = a;
      break;
    }
  }
  if (*index == -1) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"No auxiliary data named \"%s\" was found",name);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsGetAuxiliaryData"
PetscErrorCode PWPhysicsGetAuxiliaryData(PWPhysics phys,PetscInt index,PetscReal **buffer)
{
  *buffer = NULL;
  if (!phys->aux_data) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. No auxiliary fields have been registered. Must call PWPhysicsSetNumberAuxiliaryFields() first",index);
  if (index < phys->naux) {
    if (phys->aux_data[index]) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. A non-null auxiliary already exists in this slot",index);
    *buffer = phys->aux_data[index];
  } else SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. Max registered number of auxiliary fields is %D",phys->naux,index);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PWPhysicsGetAuxiliaryDataRead"
PetscErrorCode PWPhysicsGetAuxiliaryDataRead(PWPhysics phys,PetscInt index,const PetscReal **buffer)
{
  *buffer = NULL;
  if (!phys->aux_data) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. No auxiliary fields have been registered. Must call PWPhysicsSetNumberAuxiliaryFields() first",index);
  if (index < phys->naux) {
    if (phys->aux_data[index]) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. A non-null auxiliary already exists in this slot",index);
    *buffer = (const PetscReal*)phys->aux_data[index];
  } else SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot set user buffer into index %D. Max registered number of auxiliary fields is %D",phys->naux,index);
  PetscFunctionReturn(0);
}
