## ==============================================================================
##
##  Makefile for subfusc model wedge-0
##  Notes:
##  (1) Set the location of your PETGS build via
##    make all PETGS_DIR=/path/to/root/dir/of/petgs
##  (2) Set the compiler flags via
##    make all PROJ_CFLAGS='-your-go-fast-options -should-appear-here'
##
## ==============================================================================
.SECONDEXPANSION:		# to expand $$(@D)/.DIR
.SUFFIXES:	                # Clear .SUFFIXES because we don't use implicit rules
.DELETE_ON_ERROR:               # Delete likely-corrupt target file if rule fails

include $(PETSC_DIR)/$(PETSC_ARCH)/lib/petsc/conf/petscvariables

PETGS_DIR := ${PWD}/../../..
PETGS_INC := -I$(PETGS_DIR)/include
PETGS_LIB := -L$(PETGS_DIR)/$(PETSC_ARCH)/lib -lproj

PROJ_CFLAGS := -O0 -g -Wall -Wno-unused-function -Wno-unused-but-set-variable

all : klwqp1

OBJDIR ?= $(PETSC_ARCH)/obj
BINDIR ?= $(PETSC_ARCH)/bin

# directory that contains most recently-parsed makefile (current)
thisdir = $(addprefix $(dir $(lastword $(MAKEFILE_LIST))),$(1))
incsubdirs = $(addsuffix /local.mk,$(call thisdir,$(1)))

model—src-y.c :=
model—exec-y.c :=
PROJ_INC := $(PETSC_CC_INCLUDES) $(PETGS_INC) -I${PWD}/include

# Recursively include files for all targets
include local.mk

#### Rules ####
ifeq ($(V),)
  quiet_HELP := "Use \"$(MAKE) V=1\" to see the verbose compile lines.\n"
  quiet = @printf $(quiet_HELP)$(eval quiet_HELP:=)"  %10s %s\n" "$1$2" "$@"; $($1)
else ifeq ($(V),0)		# Same, but do not print any help
  quiet = @printf "  %10s %s\n" "$1$2" "$@"; $($1)
else				# Show the full command line
  quiet = $($1)
endif


%.$(AR_LIB_SUFFIX) : | $$(@D)/.DIR
	$(call quiet,AR) $(AR_FLAGS) $@ $^
	$(call quiet,RANLIB) $@

ifeq ($(PETSC_LANGUAGE),CXXONLY)
  cc_name := CXX
else
  cc_name := CC
endif

# gcc/gfortran style dependency flags; these are set in petscvariables starting with petsc-3.5
C_DEPFLAGS ?= -MMD -MP
FC_DEPFLAGS ?= -MMD -MP

PROJ_COMPILE.c = $(call quiet,$(cc_name)) -c $(PCC_FLAGS) $(CCPPFLAGS) $(PROJ_CFLAGS) $(PROJ_INC) $(CFLAGS) $(C_DEPFLAGS)


klwqp1: $(model—exec-y.c:%.c=$(BINDIR)/%.app)
$(model—exec-y.c:%.c=$(BINDIR)/%.app) : $(model—src-y.c:%.c=$(OBJDIR)/%.o)
.SECONDARY: $(model—exec-y.c:%.c=$(OBJDIR)/%.o) # don't delete the intermediate files

$(BINDIR)/%.app : $(OBJDIR)/%.o | $$(@D)/.DIR
	$(call quiet,PCC_LINKER) $(PROJ_CFLAGS) -o $@ $^ $(PETGS_LIB) $(PETSC_SNES_LIB)
	@ln -sf $(abspath $@) $(BINDIR)

$(OBJDIR)/%.o: %.c | $$(@D)/.DIR
	$(PROJ_COMPILE.c) $(abspath $<) -o $@

%/.DIR :
	@mkdir -p $(@D)
	@touch $@

.PRECIOUS: %/.DIR
.SUFFIXES: # Clear .SUFFIXES because we don't use implicit rules
.DELETE_ON_ERROR:               # Delete likely-corrupt target file if rule fails

.PHONY: clean all

clean:
	rm -rf $(OBJDIR) $(LIBDIR) $(BINDIR)

# make print VAR=the-variable
print:
	@echo $($(VAR))

srcs.c := $(model—src-y.c) $(model—exec-y.c)
srcs.o := $(srcs.c:%.c=$(OBJDIR)/%.o)
srcs.d := $(srcs.o:%.o=%.d)
# Tell make that srcs.d are all up to date.  Without this, the include
# below has quadratic complexity, taking more than one second for a
# do-nothing build of PETSc (much worse for larger projects)
$(srcs.d) : ;

-include $(srcs.d)
