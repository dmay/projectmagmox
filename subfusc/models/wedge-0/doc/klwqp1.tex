% SIAM Article Template
\documentclass[final]{siamart1116}

% Information that is shared between the article and the supplement
% (title and author information, macros, packages, etc.) goes into
% ex_shared.tex. If there is no supplement, this file can be included
% directly.

% ===========================
% SIAM Shared Information Template
% This is information that is shared between the main document and any
% supplement. If no supplement is required, then this information can
% be included directly in the main document.


% Packages and macros go here
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{natbib}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{epstopdf}
\ifpdf
  \DeclareGraphicsExtensions{.eps,.pdf,.png,.jpg}
\else
  \DeclareGraphicsExtensions{.eps}
\fi

\newcommand{\cvo}[1]{\boldsymbol{#1}}
\newcommand{\dvo}[1]{\mathbf{#1}}

% Declare title and authors, without \thanks
\newcommand{\TheTitle}{A two-phase formulation with a parameterised distribution function: \\Implementation and solvers} 
\newcommand{\TheAuthors}{D.~A. May}

% Sets running headers as well as PDF title and authors
\headers{\TheTitle}{\TheAuthors}

% Title. If the supplement option is on, then "Supplementary Material"
% is automatically inserted before the title.
\title{{\TheTitle}
%\funding{This work was funded by the Fog Research Institute under contract no.~FRI-454.}
}

% Authors: full names plus addresses.
\author{
  Dave A. May\footnotemark[2]
}



%%% Local Variables: 
%%% mode:latex
%%% TeX-master: "ex_article"
%%% End: 
% ===========================

% Optional PDF information
\ifpdf
\hypersetup{
  pdftitle={\TheTitle},
  pdfauthor={\TheAuthors}
}
\fi

% The next statement enables references to information in the
% supplement. See the xr-hyperref package for details.

% FundRef data to be entered by SIAM
%<funding-group>
%<award-group>
%<funding-source>
%<named-content content-type="funder-name"> 
%</named-content> 
%<named-content content-type="funder-identifier"> 
%</named-content>
%</funding-source>
%<award-id> </award-id>
%</award-group>
%</funding-group>

\begin{document}

\maketitle

\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\footnotetext[2]{University of Oxford, Department of Earth Sciences, South Parks Road, Oxford, OX1 3AN, United Kingdom}
\renewcommand{\thefootnote}{\arabic{footnote}}

% REQUIRED
%\begin{abstract}
%I define the  governing equations and forms for subfusc
%\end{abstract}



\section{Governing equations}
\subsection{Mechanics}
\begin{equation}
\nabla \cdot \left( 2 \eta(T,\phi) \dot{\cvo \epsilon}_s \right) + \nabla \left( \zeta(\phi) \nabla \cdot \cvo v_s \right) - \nabla p = \phi \Delta \rho \cvo g
\end{equation}
\begin{equation}
	-\nabla \cdot \cvo v_s + \nabla \cdot \left( \frac{k}{\mu} \left( \nabla p + \Delta \rho \cvo g \right) \right) = 0.
\end{equation}
Denote the non-linear residual of these equations via $\cvo F_m(\cdot) = (\cvo F_m^u, F_m^p) = \cvo 0$.

\subsubsection*{Coefficients}
Shear viscosity $\eta(\cdot)$; bulk viscosity $\zeta(\cdot)$; density difference $\Delta \rho$; permability $k$.
The likely form for the shear viscosity will be an Arrhenius (diffusion creep) law
$$
	\eta_\text{diff} = A \, \text{exp} \left( \frac{E + p_\text{litho}V}{RT} \right),
$$
where $A \sim 1.32043 \times 10^9$ Pa s, $R = 8.3145$ J/mol K and the shear viscosity will be capped according to a harmonic average
$$
	\eta = \left( \frac{1}{\eta_\text{diff}} + \frac{1}{\eta_\text{max}} \right)^{-1},
$$
where $\eta_\text{max} \sim 10^{26}$ Pa s.
The likely form for the bulk viscosity will be 
$$
	\zeta = B \, \frac{\eta}{\phi},
$$
where $B \sim 100$.

\subsubsection*{Dependencies}
Porosity $\phi$, temperature $T$.

\subsubsection*{Diagnostics}
The liquid velocity $\cvo v_l$, computed as:
\begin{equation}
	\phi \left( \cvo v_l - \cvo v_s \right) = - \frac{k}{\mu} \left( \nabla p + \Delta \rho \cvo g \right),
\end{equation}
and the bulk velocity $\bar{\cvo v}$ computed as:
\begin{equation}
	\bar{\cvo v} = \cvo v_s (1-\phi) + \cvo v_l \phi.
\end{equation}

\subsection{Energy}
\begin{equation}
\frac{\partial T}{\partial t} + \nabla \cdot \left( \bar{\cvo v} T \right)
	= -\frac{L}{c_p} \left( \frac{\partial \phi}{\partial t} - \nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right) \right)
	+ \kappa \nabla^2 T.
\end{equation}
Denote the non-linear residual of this equations via $F_e(\cdot) = 0$.

\subsubsection*{Coefficients}
Bulk velocity $\bar{\cvo v}$; the source term $\frac{\partial \phi}{\partial t} - \nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right)$.

\subsubsection*{Dependencies}
Porosity $\phi$, solid and liquid velocity $\cvo v_s, \cvo v_l$.


\subsection{Components}
\begin{equation}
	\frac{\partial C_\text{H$_2$O}}{\partial t} + \nabla \cdot \left( \cvo v_l \, C_\text{H$_2$O} \right) = 0
\end{equation}
\begin{equation}
	\frac{\partial C_\text{rf}}{\partial t} + \nabla \cdot \left(  \cvo v_s \, C_\text{rf} \right) = 0
\end{equation}


\subsubsection*{Coefficients}
Solid and liquid velocity $\cvo v_s, \cvo v_l$.


\subsection{Diagnostics}
The porosity is determined uniquely from $T$, $p_\text{litho}$, $C_\text{H$_2$O}$ and $C_\text{rf}$ according to 
\begin{equation}
	K_\text{ft}\left(T,p_\text{litho}, C_\text{H$_2$O}\right) = \frac{1 - C_\text{rf}/(1-\phi)}{1 - C_\text{H$_2$O}/\phi},
\end{equation}
where $K_\text{ft}(\cdot)$ is an analytic function. Porosity is obtained by solving the quadratic equation
\begin{equation}
	\left[ 1- K_\text{ft} \right] \phi^2 + \left[ K_\text{ft}(1 + C_\text{H$_2$O}) + C_\text{rf} - 1\right] \phi + C_\text{H$_2$O}-K_\text{ft} = 0
\end{equation}


\section{Formalism}
The state variables for the coupled non-linear problem are $\cvo X = \left(\cvo v_s, p, T, C_\text{H$_2$O}, C_\text{rf} \right)$. The diagnostic variables are $\cvo Y = \left( \cvo v_l, \bar{\cvo v} \right)$.


\section{Semi-Discrete Problem}

\subsection{Energy}
I will consider using BDF1 (backward Euler):
\begin{equation}
\frac{T - T^k}{\Delta t} + \nabla \cdot \left( \bar{\cvo v} T \right)
	= -\frac{L}{c_p} \left( \frac{\phi - \phi^k}{\Delta t} - \nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right) \right)
	+ \kappa \nabla^2 T.
\end{equation}
BDF2 might be a better choice to deal with the stiff coupling with $\cvo v_s$. 
I'm not particularly concerned by the choice of time integrator at this point in time.
Alternatives can be introduced without too much additional complexity.

The term $\nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right)$ is problematic. 
In essence we have to evaluate
$$
	\int s \nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right) \, dV.
$$
Choices to treat this term include: \newline
(i) project or interpolate $(1-\phi) \cvo v_s$ into a nodal field to yield $\cvo v_p$ then the above becomes
$$
	\int s \nabla \cdot \cvo v_p \, dV \approx \sum_q w_q s(\cvo x_q) ( \nabla \cdot \cvo v_p )|_{\cvo x_q}.
$$
Interpolating has the disadvantage that the quantity might not be well represented by a polynomial. \newline
(ii) Integrate by parts and deal with this thing 
$$
	\int s \nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right) \, dV = -\int \nabla s \cdot (1-\phi) \cvo v_s \, dV + \int s (1-\phi) \cvo v_s \cdot \cvo n \, dS,
$$
which will require $\phi$ to be evaluated at interior quadrature points and the the surface quadrature points. 
It does avoid have to project or interpolate quantities. The distribution function can be evaluated directly at each quadrature point location.\newline
(iii) Interpolate or project $\phi$.


\section{Evaluate Coefficients}
Given $\cvo X = \left(\cvo v_s, p, T, C_\text{H$_2$O}, C_\text{rf} \right)$, $\cvo Y$ and a time step $\Delta t$, the following quantities are required to be evaluated:
\begin{enumerate}
\item Compute $\phi$
\item Compute $\eta, \zeta, \Delta \rho$ [mechanics]
\item Compute $\frac{\phi - \phi^k}{\Delta t} - \nabla \cdot \left(\left(1-\phi\right) \cvo v_s \right)$ [energy] 
\end{enumerate}

\section{Update Coefficient State}
Upon a successful time step, the following variables must be stored:
\begin{enumerate}
\item Compute $T^k \leftarrow T$
\item Compute $\phi^k \leftarrow \phi$
\end{enumerate}


\section{Implementation}
The model is comprised of three domains $\Omega_s$ (slab domain), $\Omega_w$ (wedge domain) and $\Omega_p$ (plate domain).
The model domain is denoted via $\Omega =  \Omega_s \cup \Omega_w \cup \Omega_p$.
In $\Omega_s$ and  $\Omega_p$ it will be enforced that $C_\text{H$_2$O, rf} = 0$ and thus $\phi = 0$.
This implies that $\bar{\cvo v} = \cvo  v_s$ and $\cvo v_l = 0$ in these domains.
In $\Omega_s$ and $\Omega_p$, the solid velocity is prescribed according to $\cvo v_s^\text{bc}$.
It will also be assumed that $p = 0$ in the non dynamically deforming regions $\Omega_s, \Omega_p$ (e.g. regions where the system is driven kinematically by prescribing $\cvo v_s$.

\subsection{Mechanics}
The two-phase system will be solved in $\Omega_w$ using a $P^k$-$P^{k-1}$ mixed finite elements with a triangulation denoted via $\mathcal \tau_m(\Omega_w)$.
There is an additional representation of the solid and liquid velocity in the full domain which is denoted by the triangulation $\mathcal \tau_m(\Omega)$

It makes the most sense to compute the diagnostic quantity $\dvo v_l$ on the space $P^k$. 
Direct projection of $\nabla p \in P^{k-2}$ onto $P^k$ is problematic primarily for low order spaces as $\nabla p$ can/will be cell-wise discontinuous.
There is also the issue that depending on how the projection is defined, $\phi$ may have to be evaluated multiple times, and in different locations.
Another issue stems from the inherent risk of having to compute $1/\phi$ which will be problematic when $\phi = 0$.

I propose to this in the following way: (i) compute an approximation of the Darcy velocity on $P^k$, $\dvo v_D^*$; (ii) construct a nodal representation of $\phi \rightarrow \phi^n$; (iii) compute $\dvo v_l^*$ using $\phi^n, \dvo v_s$.
\begin{enumerate}
\item On each cell $e$ in $\tau_m$, perform a local (discontinuous) $L_2$ projection of the Darcy velocity into $P^{k-1}$. Call this cell wise projected quantity $\dvo v_D^e(\cvo x)$.
Specifically evaluate
$$
	\left( \int_{\Omega^e} N^T N \, dV \right) \Phi^e = \int_{\Omega^e} N^T \left(  -\frac{k}{\mu} \left[ \nabla p + \Delta \rho \cvo g \right] \right) dV
$$
where
$$
	\dvo v_D^e(\cvo x) = \sum_i N_i(\cvo x) \Phi^e_i
$$
\item Construct an averaging operator over $P^k$ which essentially ``counts'' the connectivity of the velocity basis coupling. Call this vector $\dvo R^\text{avg}$.
\item On each cell $e$, evaluate $\dvo v_D^e(\dvo x^e_i)$ where $\dvo x^e_i$ is the coordinate of each velocity basis in element $e$. 
\item Sum the results $\tilde{\dvo v}_D = \sum_e \left( \dvo v_D^e(\dvo x^e_i) \right)$ into a continuous vector $\tilde{\dvo v}_D$ defined over $\tau_m$.
\item Compute $\left[ \dvo v_D^*\right]_i = \left[ \tilde{\dvo v}_D \right]_i / \left[ \dvo R^\text{avg} \right]_i$
\item Compute $\left[ \dvo v_l^*\right]_i = \left[ \frac{1}{\phi^n} \right]_i \left[ \dvo v_D^* \right]_i + \left[ \dvo v_s \right]_i$ if $[\phi^n]_i > \phi_\text{min}$.
\item Set $\left[ \dvo v_l^*\right]_i = 0$ if $[\phi^n]_i \le \phi_\text{min}$.
\end{enumerate}

The cut-off approach involving $\phi_\text{min}$ is safe in the sense it avoids a potential divide by zero.
However, if a typical power-law form is assumed for $k(\phi)$, e.g. $k \sim \phi^m$, then in reality no such problems actually occur, e.g.
\begin{equation}
	\cvo v_l  = \cvo v_s - \frac{\phi^{m-1}}{\mu} \left[ \nabla p + \Delta \rho \cvo g \right],
\end{equation}
which is perfectly valid for $m \ge 1$ (typically $m = 2$). If $k = \text{const.}$ potential problems will arise in locations where $\phi = 0$. 
Thus when $k \sim \phi^m, m \ge 1$ it would suffice to do the following:
\begin{enumerate}
\item On each cell $e$ in $\tau_m$, perform a local (discontinuous) $L_2$ projection of the liquid velocity into $P^{k-1}$. Call this cell wise projected quantity $\dvo v_l^e(\cvo x)$.
That is, evaluate
$$
	\left( \int_{\Omega^e} N^T N \, dV \right) \Phi^e = \int_{\Omega^e} N^T \left(  \dvo v_s -\frac{\phi^{m-1}}{\mu} \left[ \nabla p + \Delta \rho \cvo g \right] \right) dV
$$
where
$$
	\dvo v_l^e(\cvo x) = \sum_i N_i(\cvo x) \Phi^e_i
$$
\item Construct an averaging operator over $P^k$ which essentially ``counts'' the connectivity of the velocity basis coupling. Call this vector $\dvo R^\text{avg}$.
\item On each cell $e$, evaluate $\dvo v_l^e(\dvo x^e_i)$ where $\dvo x^e_i$ is the coordinate of each velocity basis in element $e$. 
\item Sum the results $\tilde{\dvo v}_l = \sum_e \left( \dvo v_l^e(\dvo x^e_i) \right)$ into a continuous vector $\tilde{\dvo v}_D$ defined over $\tau_m$.
\item Compute $\left[ \dvo v_l^*\right]_i = \left[ \tilde{\dvo v}_l \right]_i / \left[ \dvo R^\text{avg} \right]_i$
\end{enumerate}

Note that in both cases, a nodal representation of $\phi$ will always be required to compute the bulk velocity using our reconstructed liquid velocity $\dvo v_l^*$;
\begin{equation*}
	\bar{\cvo v} = \cvo v_s (1-\phi^n) + \cvo v_l^* \phi^n.
\end{equation*}


\subsection{Energy}
The conservation of energy will be solved in $\Omega$ using a $P^{k_e}$ finite elements with a triangulation denoted via $\mathcal \tau_e$. It is likely that the parabolic system can be solved directly with standard (non-stabilized) finite elements if sufficiently high spatial resolution is used. Hence I'd advocate using $k_e > k$. If this appears to be incorrect, an operating splitting approach combing diffusion with FE and treating advection via a non-conservative semi-Lagrangian method will be used. 
The time step used for the energy equation will be denoted via $\Delta t_e$.
Also note that no explicit residual evaluation method is available when using semi-Lagrangian. Computing the update in essence defines the residual (and thus it is expensive).

\subsection{Components}
The two PDEs for $C_\text{H$_2$O, rf}$ will be solved using a conservative semi-Lagrangian approach. The ``triangulation'' used for the SL method will be denoted via $\tau_c$.
The time step used for $C_\text{H$_2$O, rf}$ will be denoted via $\Delta t_l$ and $\Delta t_s$ to reflect the dependence on the liquid and solid velocity respectively. For simplicity here I will assume $\Delta t_s = M \Delta t_l$ where $M$ is an integer.
It is possible that despite the different magnitude of velocity (cf. liquid to solid), the semi-Lagrangian method might be sufficiently accurate with $M=1$.


\subsection{Definitions}
\begin{itemize}
\item[$\circ$] $\dvo v_s$, $\dvo v_l$ are the solid and liquid velocity defined over $\Omega$.
\item[$\circ$] $\hat{\dvo v}_s$, $\hat{\dvo v}_l$, $\hat{\bar{\dvo v}}$ are the solid, liquid and bulk velocity defined over $\Omega_w$.
\item[$\circ$] $\dvo v_l^*$ is the liquid velocity represented on the same function space as $\dvo v_s \in P^{k}$.
\item[$\circ$] $\dvo R_v$ is the restriction operator such that $\hat{\dvo v}_s = \dvo R_v \dvo v_s$ and $\hat{\dvo v}_l = \dvo R_l \dvo v_l$
\item[$\circ$] $\dvo I_v$ is the injection operator such that ${\dvo v}_s = \dvo I_v \hat{\dvo v_s}$ %and ${\dvo v}_l = \dvo I_l \hat{\dvo v_l}$
 for all $\cvo x \in \Omega_w$.
\item[$\circ$] $\dvo P_v$ interpolates $\hat{\dvo v}_l \in P^{k-1}$ onto the function space used to define $\hat{\dvo v}_s \in P^{k}$.
\item[$\circ$] $\dvo Q_{vc}$ interpolates $\dvo v_l^*, \dvo v_s$ onto the triangulation $\tau_c$ used by the components $C_\text{H$_2$O, rf}$. Note that since these velocities could be discontinuous across domain boundaries (e.g. wedge--slab), it might not be wise to simply use interpolants from $P^k$ as this could lead to unwanted Gibbs phenomena. An alternative would be to interpolate domain wise (multiplicatively), e.g. first compute $\tilde{\dvo v}_s = \dvo Q_{vc} \dvo v_s^\text{bc}$ and then compute
$$
%\dvo v_s^c = \tilde{\dvo v}_s \stackrel{\text{\tiny inject}}{\longleftarrow} (\dvo Q_{vc} \hat{\dvo v}_s)
\dvo v_s^c = \tilde{\dvo v}_s \xleftarrow{\text{inject}} (\dvo Q_{vc} \hat{\dvo v}_s).
$$
Not sure how to write this, but the intent is that the result of interpolating $\hat{\dvo v}_s$ within the wedge domain overrides the interpolated values obtained from the plate/slab domains (here called $\tilde{\dvo v}_s$).
\item[$\circ$] $\dvo w_{ve}$ interpolates $\hat{\dvo v}_s \in \Omega_w$ onto the triangulation $\tau_e$ used by the energy solver.
\item[$\circ$] $\dvo W_{ve}$ interpolates ${\dvo v}_s \in \Omega$ onto the triangulation $\tau_e$ used by the energy solver.
\item[$\circ$] $\dvo P_{Tv}$ interpolates $\dvo T$ onto the function space used to define $\hat{\dvo v}_s$.
%\item[$\circ$] $\dvo P_{Tc}$ interpolates $\dvo T$ onto the triangulation $\tau_c$ used by the components.
\item[$\circ$] $\dvo P_c(\dvo x,C)$ performs conservative semi-Lagrangian interpolation of component $C$ at position $\dvo x$.
\item[$\circ$] Let $\dvo x(\Omega_2) = \mathbb I[\dvo y(\Omega_1)]$ define the injection of $\dvo y$ into $\dvo x$. Assume that $\dvo x$ and $\dvo y$ are in the same function space, and that $\Omega_2$ is contained within $\Omega_1$. 
\end{itemize}

\subsection{Residuals and Updates}
Assume that following is available $\dvo X = \left(\dvo v_s, \dvo p, \dvo T, \dvo C_\text{H$_2$O}, \dvo C_\text{rf} \right)$, $\dvo Y = (\dvo v_l^*, \bar{\dvo v})$ and a time step $\Delta t$.
$(\dvo v_s, \dvo p)$ are defined on $\tau_m^u, \tau_m^p$.
$\dvo T$ is defined on $\tau_e$.
$\dvo C_\text{H$_2$O}, \dvo C_\text{rf}$ are defined on $\tau_c$.
$(\dvo v_l^*, \dvo p)$ are defined on $\tau_m$ and are represented by the function space $P^k$.

\subsubsection*{Mechanics}
\begin{enumerate}
%\item Extract sub-domain velocity $\hat{\dvo v}_s = \dvo R_v \dvo v_s$.
\item Extract sub-domain temperature $\hat{\dvo T} = \dvo P_{Tv} \dvo T \in P^k$.
\item Evaluate $\dvo C_\text{H$_2$O}$ and $\dvo C_\text{rf}$ at all quadrature points in $\tau_m$ using $\dvo P_c(\dvo x,\cdot)$.
\item Evaluate $\phi$ at all quadrature points using $\hat{\dvo T}$.
\item Evaluate $\eta, \zeta, \Delta \rho$ at all quadrature points.
\end{enumerate}
Upon either solving $\dvo F_m = \dvo 0$ for $\hat{\dvo v}_s, \hat{\dvo p}$, or after performing a Newton step, do the following:
\begin{enumerate}
\item Compute $\hat{\dvo v}_l \in P^{k-1}$.
\item Compute $\hat{\dvo v}_l^* = \dvo P_v \hat{\dvo v}_l \in P^{k}$.
\item Set $\dvo v_l^* = \dvo 0 + \dvo I_v \hat{\dvo v}_l^*$.
\item Evaluate $\dvo C_\text{H$_2$O}$, $\dvo C_\text{rf}$ (using $\dvo P_c(\dvo x,\cdot)$), $\hat{\dvo T}$ and at all nodal basis in $\tau_m$ used by $\hat{\dvo v}_s$ to compute a nodal porosity $\dvo \phi^n$.
\item Compute $\hat{\bar{\dvo v}} = \hat{\dvo v}_s (1-\dvo \phi_n) + \hat{\dvo v}_l^* \dvo \phi_n$.
\item Set $\bar{\dvo v} = \dvo v_s^\text{bc} + \dvo I_v \hat{\bar{\dvo v}}$.
\item Set $\dvo v_s = \dvo v_s^\text{bc} + \dvo I_v \hat{\dvo v}_s$.
\end{enumerate}

\subsubsection*{Components}
\begin{enumerate}
\item Compute $\dvo v_s^c = \dvo Q_{vc} \dvo v_s$.
\item Compute $\dvo v_l^c = \dvo Q_{vc} \dvo v_l^*$.
%\item Compute $\dvo T^c = \dvo P_{Tc} \dvo T$.
\item Solve de-coupled transport problems for $\dvo C_\text{H$_2$O}$ and $\dvo C_\text{rf}$.
\end{enumerate}


\subsubsection*{Energy}
\begin{enumerate}
\item Insert $\bar{\cvo v}^{bc} = \cvo v_s^{bc}$ into $\bar{\dvo v}^e$. Compute $\bar{\dvo v}^e = \dvo w_{ve} \hat{\bar{\dvo v}} \in P^{k_e}$.
\item Insert $\cvo v_s^{bc}$ into $\dvo v_s^e$. Compute $\dvo v_s^e = \dvo w_{ve} \hat{\dvo v}_s \in P^{k_e}$.
\item Evaluate $\dvo C_\text{H$_2$O}$ and $\dvo C_\text{rf}$ at all quadrature points in $\tau_e$ using $\dvo P_c(\dvo x, \cdot)$.
\item Evaluate $\phi$ at all quadrature points using interpolated values of $\dvo T$.
\item Solve $\dvo F_e = \dvo 0$ for $\dvo T$.
\end{enumerate}

\section{Non-linear Solvers}
I'll assume that all non-linear solves will be initialized with a flow and thermal field satisfying the single-phase, steady state equations - aka. ``van Keken's community benchmark model''.
Assume that the solution is to be advanced from time $t_0$ to time $t_1$, with $\Delta t = t_1 - t_0$.

Assume that the vectors $\dvo v_s, \dvo p, \dvo T$ satisfying the state-state single-phase problem are available (or have been computed).
Now prescribe initial condition for $\dvo C_\text{H$_2$O}$ and $\dvo C_\text{rf}$.

\subsection{Monolithic Newton}
it can be instructive to examine the structure of the Jacobian arising from monolithic Newton. 
In so much as to better understanding splittings introduced in ``SNESFIELDSPLIT'' type of solves.
For simplicity I'll consider the continuous Jacobian. For clarity, I'll denote $C_\text{H$_2$O}$ via just $C$ and $C_\text{rf}$ via $C_r$.
\begin{equation}
\begin{bmatrix}
\cvo A_{uu}       &\cvo A_{up}    &\cvo J_{u{c}}   &\cvo J_{u{c_r}}     &\cvo J_{uT}\\
\cvo A_{pu}       &A_{pp}           & J_{p{c}}         & J_{p{c_r}}             & J_{pT} \\
\cvo J_{cu}  & J_{cp}   &  A_{c}            & J_{cc_{r}}       & J_{cT} \\
%\cvo A_{c_{2}u}  & J_{c_{2}p}   & J_{c_2c_1}       & A_{c_{2}}               & J_{c_{2}T} \\
\cvo A_{c_{r}u}  & 0   & 0       & A_{c_{r}}               & 0 \\
\cvo J_{Tu}         & J_{Tp}        & J_{Tc}            & J_{Tc_{r}}             & A_{T}
\end{bmatrix}
\begin{bmatrix}
\cvo  {\delta v}_s \\
\delta p \\
\delta C \\
\delta C_r \\
\delta T
\end{bmatrix}
= -
\begin{bmatrix}
\cvo F_m^u \\
F_m^p \\
F_C \\
F_{C_r} \\
F_e
\end{bmatrix}
\end{equation}

Comments: 
\begin{itemize}
\item[$\circ$] $ A_{ij}$ are the linear operators.
\item[$\circ$] There is no explicit $p$ non-linearity in the momentum equation for $\cvo  v_s$ since $p_\text{litho}$ is used to compute $\phi$.
\item[$\circ$] $\cvo J_{u{c}}$, $\cvo J_{u{c_r}}$ arises only if there is a $\phi$ dependence in the shear viscosity $\eta$. 
\item[$\circ$] $\cvo J_{uT}$ is non-zero due to the Arrhenius form of the flow law and or any $\phi$ dependence of $\eta$.
\item[$\circ$] $ J_{pc}$, $ J_{p{c_r}}$ arises only if there is a $\phi$ dependence in the permability $k$. 
\end{itemize}

As a sanity check of the non-linear coupling, I'll consider a problem with $v_l$ and $\phi$ treated as independent unknowns.

\begin{equation}
\begin{bmatrix}
\cvo A_{uu}       &\cvo A_{up}  &\cvo 0             &\cvo 0       &\cvo 0        &\cvo X_{u\phi}   &\cvo J_{uT}\\
\cvo A_{pu}       &A_{pp}         &\cvo 0              &0              &0               &X_{p\phi}           &J_{pT} \\
\cvo J_{ls}         &\cvo J_{p}    &\cvo A_{l}         &\cvo 0      &\cvo 0        &\cvo J_{l\phi}    &\cvo 0 \\
\cvo 0                &0                 &\cvo A_{cl}       &A_c         &0                &0                      &0 \\
\cvo A_{c_{r}u} &0                 &\cvo 0               &0              &A_{c_{r}}  &0                      &0 \\
\cvo 0                &0                 &\cvo 0               &J_c          &J_{c_{r}}  &A_\phi              &J_{\phi T} \\
\cvo J_{Tu}        &0                 &\cvo J_{Tu_{l}} &0              &0               &J_{T\phi}          &A_{T}
\end{bmatrix}
\begin{bmatrix}
\cvo {\delta v}_s \\
\delta p \\
\cvo {\delta v}_l \\
\delta C \\
\delta C_r \\
\delta \phi \\
\delta T
\end{bmatrix}
= -
\begin{bmatrix}
\cvo F_s^u \\
F_s^p \\
\cvo F_l^u \\
F_C \\
F_{C_r} \\
F_{\phi} \\
F_e
\end{bmatrix}
\end{equation}
Eliminating the following variables
$$
\cvo {\delta v}_l = \cvo A_l^{-1}\left( -\cvo F_l^u - \cvo J_{ls} \delta v_s - \cvo J_p \delta p - \cvo J_{l \phi} \delta \phi \right),
$$
$$
\delta \phi = A_{\phi}^{-1} \left( -F_\phi - J_c \delta C - J_{c_r}  \delta C_r - J_{\phi T} \delta T \right),
$$
one finds the original dependence of unknowns in the $\cvo X = (\cvo  {\delta v}_s,\delta p,\delta C,\delta C_r,\delta T)$ form above.
Specifically the residual for $C$ is dense as it depends on the liquid velocity (and thus porosity and temperature), e.g.
$$
\cvo A_{cl} \cvo A_l^{-1}\left( - \cvo J_{ls} \delta v_s - \cvo J_p \delta p \right) 
- \cvo A_{cl} \cvo A_l^{-1}\cvo J_{l \phi} \delta \phi 
+ A_c  \delta C 
= -F_C + \cvo A_{cl} \cvo A_l^{-1} \cvo F_l^u \\
$$
\begin{equation}
\begin{split}
\cvo A_{cl} \cvo A_l^{-1}\left( - \cvo J_{ls} \delta v_s - \cvo J_p \delta p \right) \\
- \cvo A_{cl} \cvo A_l^{-1}\cvo J_{l \phi}  A_{\phi}^{-1} \left( - J_c \delta C - J_{c_r}  \delta C_r - J_{\phi T} \delta T \right)
+ A_c  \delta C  \\
= -F_C + \cvo A_{cl} \cvo A_l^{-1} \cvo F_l^u 
+ \cvo A_{cl} \cvo A_l^{-1}\cvo J_{l \phi}  A_{\phi}^{-1} F_\phi 
\end{split}
\end{equation}

Conversely, the residual for $C_r$ is not dense (due to a solid velocity dependence only):
$$
\cvo A_{c_{r}u} \delta v_s    + A_{c_{r}} \delta C_r = - F_{C_r}.
$$
This might suggest that local non-linear updates involving $C_r$ could be performed in conjunction with any updates of the solid velocity.

\subsection{Non-linear Richardson}
\begin{enumerate}
\item Set $\dvo T' = \dvo T$.
\item Solve $\dvo F_m = \dvo 0$ for $\dvo v'_s, \dvo p'$ with $\dvo T'$ frozen. Compute diagnostics $\dvo v_l$ and $\bar{\dvo v}$.
\item Solve the transport problem for $\dvo C_\text{H$_2$O}$ using $\alpha$ time steps of size $\Delta t_l$ such that $\Delta t = \alpha \Delta t_l$.
\item Solve the transport problem for $\dvo C_\text{rf}$ using $\beta$ time steps of size $\Delta t_s$ such that $\Delta t = \beta \Delta t_s$.
\item Solve $\dvo F_e = \dvo 0$ for $\dvo T'$ using $\gamma$ time steps of size $\Delta t_e$ such that $\Delta t = \gamma \Delta t_e$.
\item Compute the residual $\dvo F_m(\dvo v'_s,\dvo p', \dvo T')$.
\item If $|| \dvo F_m ||_2 < \epsilon$, set $\dvo T = \dvo T', \dvo v_s = \dvo v_s', \dvo p = \dvo p'$ and advance the solution to $t_1$. Otherwise goto step 2.
\end{enumerate}


\subsection{Non-linear Gauss-Seidel}
\begin{enumerate}
\item Set $\dvo T' = \dvo T$, $\dvo C'_\text{H$_2$O} = \dvo C_\text{H$_2$O}$, $\dvo C'_\text{rf} = \dvo C_\text{rf}$.
\item Solve $\dvo F_m = \dvo 0$ for $\dvo v'_s, \dvo p'$ with $\dvo T'$ frozen. Compute diagnostics $\dvo v_l$ and $\bar{\dvo v}$.
\item Solve $\dvo F_e = \dvo 0$ for $\dvo T'$ using $\gamma$ time steps of size $\Delta t_e$ such that $\Delta t = \gamma \Delta t_e$.
\item Compute the residual $\dvo F_m(\dvo v'_s,\dvo p', \dvo T')$.
\item If $|| \dvo F_m ||_2 < \epsilon$, continue. Otherwise goto step 2.

\item Solve the transport problem for $\dvo C'_\text{H$_2$O}$ using $\alpha$ time steps of size $\Delta t_l$ such that $\Delta t = \alpha \Delta t_l$.
\item Solve the transport problem for $\dvo C'_\text{rf}$ using $\beta$ time steps of size $\Delta t_s$ such that $\Delta t = \beta \Delta t_s$.
\item Compute the residual $\dvo F_e(\dvo v'_s,\dvo p', \dvo T', \dvo C'_\text{H$_2$O},\dvo C'_\text{rf})$.
\item If $|| \dvo F_e ||_2 < \epsilon$, goto step 11.
\item Solve $\dvo F_e = \dvo 0$ for $\dvo T'$ using $\gamma$ time steps of size $\Delta t_e$ such that $\Delta t = \gamma \Delta t_e$. Goto step 6.

\item Compute the residual $\dvo F_m(\dvo v'_s,\dvo p', \dvo T')$.
\item If $|| \dvo F_m ||_2 < \epsilon$, set $\dvo T = \dvo T', \dvo v_s = \dvo v_s', \dvo p = \dvo p'$ and advance the solution to $t_1$. Otherwise goto step 2.
\end{enumerate}


A slightly different variant, with slightly different notation (possibly clearer) is the following: \newline
Given initial vectors $\dvo v_s^{[k]}, \dvo p^{[k]}, \dvo C^{[k]}, \dvo C_r^{[k]}, \dvo T^{[k]}$. \newline
[a] Solve the non-linear problem $\dvo F_m(\dvo v_s', \dvo p', \dvo C^{[k]}, \dvo C_r^{[k]}, \dvo T^{[k]}) = \dvo 0$ for $\dvo v_s', \dvo p'$ using:
$$
\begin{bmatrix}
\dvo A_{uu}       &\dvo A_{up} \\
\dvo A_{pu}       &\dvo A_{pp} 
\end{bmatrix}
\begin{bmatrix}
\dvo  {\delta v}_s \\
\dvo {\delta p} \\
\end{bmatrix}
= -
\begin{bmatrix}
{\dvo F}_m^u \\
{\dvo F}_m^p
\end{bmatrix}
$$
with the solution updates
$$
	\dvo v_s' = \dvo v_s'+ \dvo{\delta v}_s, \quad
	\dvo p' = \dvo p' + \dvo {\delta p}.
$$
Compute the associated liquid velocity $\dvo v_l'$ and bulk velocity $\bar{\dvo v}'$ using $\dvo C^{[k]}, \dvo C_r^{[k]}, \dvo T^{[k]}$. \newline
%Once converged, set $v_s^{[k+1]} = v_s^*$.\newline
[b] Solve $\dvo F_e(\dvo v_s',\dvo  p', \dvo C^{[k]}, \dvo C_r^{[k]}, \dvo T') = \dvo 0$ for $\dvo T'$ using 
$$
	\dvo A_T \dvo {\delta T} = -\dvo F_e
$$
with the state vector $(\dvo v_s', \dvo p', \dvo C^{[k]}, \dvo C_r^{[k]}, \dvo T')$ and perform the following solution updates
$$
	\dvo T' = \dvo T' + \dvo{\delta T}.
$$
%Once converged, set $T^{[k+1]} = T^*$.\newline
[c] Solve the transport problem using $\dvo C^{[k]}, \dvo C_r^{[k]}, \dvo v_s', \dvo v_l'$. Denote the updated values as $\dvo C'$, $\dvo C'_r$.\newline
[d] Compute the residuals
$$
	 \dvo F_e(\dvo v'_s,\dvo p', \dvo C',\dvo C'_r,\dvo T'), \qquad
	\dvo F_m(\dvo v'_s,\dvo p', \dvo C',\dvo C'_r, \dvo T')
$$
[e] If not converged, set 
$$
	\dvo v_s^{[k+1]} = \dvo v_s', \medspace\medspace
	\dvo p^{[k+1]} = \dvo p', \medspace\medspace
	\dvo C^{[k+1]} = \dvo C',  \medspace\medspace
	\dvo C_r^{[k+1]} = \dvo C'_r, \medspace\medspace
	\dvo T^{[k+1]} = \dvo T', \qquad k = k + 1
$$
Goto to step [a].

\section{Open questions}
\begin{enumerate}

\item Does it make sense to compute the liquid velocity and feed it into the component evolution if the residual for the mechanics is not zero? E.g. suppose I took a single Newton step and I have $(v_s, p)^{n+1} = (v_s, p)^n + (\delta v_s, \delta p)$ where $n$ is my Newton iteration. If $|| F_m(v_s^{n+1}, p^{n+1}) ||_2$ is large and I compute my diagnostics $v_l, \bar{v}$ what will the consequence be wrt the coupling with the other equations? 
I'm worried that using diagnostics which have been determined from non-converged solutions will drive the system in unpredictable directions (all of which are wrong directions).

\item Which form of the liquid velocity is preferred? Should a hard choice be made in regards to using power-law dependence of $k$ on $\phi$?

\end{enumerate}


\section{Non-dimensional Scaling}
In general I adopt the following definitions. A quantity $X$ is decomposed as
$$
	X = \tilde{X} X'
$$
where $X, \tilde{X}$ have units (the same units) and $X'$ is a dimensionless quantity.

To obtain numerical solutions to geodynamic models, robustly using iterative methods, I advocate normalizing the elliptic operator $\nabla \cdot \tau$ 
so that it is scales as $O(h^2)$ where $h < 1$ is the element edge length. 
Accordingly, I consider non-dimensionalising the governing equations by choosing characteristic scales for velocity $\tilde{V}$, viscosity $\tilde{E}$ and length $\tilde{L}$. Hence the scales are
$$
	\cvo v = \tilde{V} \cvo v', \quad
	\eta = \tilde{E} \eta', \quad
	\cvo x = \tilde{L} \cvo x' \,\,\text{(position)}, \quad
	t = \tilde{T} t' \,\,\text{(time)}.
$$	
I will also choose
$$
	\zeta = \tilde{E} \zeta'.
$$
As a rule of thumb I define $\tilde L$ as the largest dimension of the model domain, $\tilde{E}$ is taken as the estimated largest shear viscosity which will occur over the model evolution and $\tilde V$ is taken as either the largest magnitude of any imposed Dirichlet velocity boundary conditions, or a typical velocity which could be expected from the geodynamic context (e.g. subduction model might use $\tilde V$ = 10 cm/yr.) 
%The characteristic time scale is given by $\tilde{T} = \tilde{L} / \tilde{V}$ and the characteristic pressure by $\tilde{E} / \tilde{T} = \tilde{E} \tilde{V}/ \tilde{L}$.

Inserting the above yields, for the momentum equation:
\begin{equation}
\frac{\tilde{E}\tilde{V}}{\tilde{L}^2}
\left[\nabla' \cdot \left( 2 \eta'(T,\phi) \dot{\cvo \epsilon}'_s \right) + \nabla' \left( \zeta'(\phi) \nabla' \cdot \cvo v'_s \right)\right]
 - \frac{1}{\tilde{L}} \nabla' p = \phi \Delta \rho \cvo g,
\end{equation}
which after collecting terms becomes
\begin{equation}
\nabla' \cdot \left( 2 \eta'(T,\phi) \dot{\cvo \epsilon}'_s \right) + \nabla' \left( \zeta'(\phi) \nabla' \cdot \cvo v'_s \right)
 - \left(\frac{\tilde{L}}{\tilde{E}\tilde{V}}\right)  \nabla' p = \left(\frac{\tilde{L^2}}{\tilde{E}\tilde{V}} \right) \phi \Delta \rho \cvo g.
\end{equation}
We define $p = \tilde{P} p'$ and define $\tilde{P} = \frac{\tilde{E}\tilde{V}}{\tilde{L}}$.
%Given $\phi \in [0,1]$ I do not consider scaling this variable. 
Noting that the acceleration scales as $\tilde{V}/\tilde{T}$, the RHS becomes
$$
	\left(\frac{\tilde{L^2}}{\tilde{E}\tilde{T}} \right) \phi \Delta \rho \cvo g',
$$
and lastly I choose the density to scale as
$$
	\rho = \tilde{R} \rho' = \left( \frac{\tilde{E}\tilde{T}}{\tilde{L^2}} \right) \rho'.
$$
The final form of the equation is thus
\begin{equation}
\nabla' \cdot \left( 2 \eta'(T,\phi) \dot{\cvo \epsilon}'_s \right) + \nabla' \left( \zeta'(\phi) \nabla' \cdot \cvo v'_s \right)
 - \nabla' p' = \phi \Delta \rho' \cvo g',
\end{equation}


When substituting the characteristic scales in the pressure equation, one obtains:
\begin{equation}
	-\frac{\tilde{V}}{\tilde{L}} \nabla' \cdot \cvo v'_s 
	%+ \frac{1}{\tilde{L}^2} .\frac{1}{\tilde{E}} . \frac{E.V}{\tilde{L}}  \nabla' \cdot \left( \frac{k}{\mu'} \nabla' p' \right)	% \frac{V}{\tilde{L}^3}
	+ \frac{\tilde{V}}{\tilde{L}^3}  \nabla' \cdot \left( \frac{k}{\mu'} \nabla' p' \right)
	%+  \frac{1}{\tilde{L}} . \frac{1}{\tilde{E}} . \frac{E.T}{\tilde{L}^2} . \frac{V}{\tilde{T}} \nabla' \cdot \left( \frac{k}{\mu'} \Delta \rho' \cvo g'  \right) % \frac{E.T.V}{E.\tilde{L}^3.T}
	+  \frac{\tilde{V}}{\tilde{L}^3} \nabla' \cdot \left( \frac{k}{\mu'} \Delta \rho' \cvo g'  \right)
	  = 0.
\end{equation}
This implies that
$$
	k = \tilde{K} k' = L^2 k'.
$$
In the code I define the variable \texttt{k\_hat} to be $\hat{k} = k/\mu$, hence the non-dimensional scaling for $k/\mu$ is $\tilde{L}^2/\tilde{E}$.
The non-dimensional form is then
\begin{equation}
	-\nabla' \cdot \cvo v'_s 
	+ \nabla' \cdot \left( \frac{k'}{\mu'} \nabla' p' \right)
	+  \nabla' \cdot \left( \frac{k'}{\mu'} \Delta \rho' \cvo g'  \right)
	  = 0,
\end{equation}
or
\begin{equation}
	-\nabla' \cdot \cvo v'_s 
	+ \nabla' \cdot \left( \hat{k}' \nabla' p' \right)
	+  \nabla' \cdot \left( \hat{k}' \Delta \rho' \cvo g'  \right)
	  = 0.
\end{equation}

With respect to the energy, we adopt the scales
$$
	T = \tilde{T}_a T' = 1 T', \quad 
	\bar{\cvo v} = \tilde{V} \bar{\cvo v}'.
$$
Within the energy equation, the characteristic scales for $\tilde L, \tilde V$ are identical to those used in the mechanics problem.
Inserting the scales we obtain
\begin{equation}
\left( \frac{\tilde V \tilde T_a}{\tilde L} \right) \frac{\partial T'}{\partial t'} 
+ \left( \frac{\tilde V \tilde T_a}{\tilde L} \right) \nabla' \cdot \left( \bar{\cvo v}' T' \right)
	= -\left( \frac{\tilde V}{\tilde L} \right) \frac{L}{c_p} \left( \frac{\partial \phi}{\partial t'} 
	- \nabla' \cdot \left(\left(1-\phi\right) \cvo v_s' \right) \right)
	+ \left( \frac{\tilde T_a}{\tilde{L}^2} \right) \kappa \nabla^{'2} T',
\end{equation}
which leads to the non-dimensional equation
\begin{equation}
\frac{\partial T'}{\partial t'} 
+ \nabla' \cdot \left( \bar{\cvo v}' T' \right)
	= 
	- \left( \frac{L}{c_p} \right)' \left( \frac{\partial \phi}{\partial t'} 
	- \nabla' \cdot \left(\left(1-\phi\right) \cvo v_s' \right) \right)
	+  \kappa' \nabla^{'2} T',
\end{equation}
where we have used that $\tilde V = \tilde L / \tilde T$, and the characteristic diffusivity
$$
\kappa = \tilde{K}_a \kappa' = \left( \frac{\tilde{L}^2}{\tilde T} \right) \kappa'.
$$
%We note that the term $(1 / \tilde T_a)(L / c_p)$ is non-dimensional and due to our choice of ${\tilde T}_a = 1$, the non-dimensional form of $(L / c_p)$ (denoted via 
The parameter grouping $L/c_p$ has form
$$
	\left( \frac{L}{c_p} \right)  = \tilde T_a \left( \frac{L}{c_p} \right)'.
$$
We note that due to our choice of $\tilde T_a = 1$ K, the value of $(L/c_p)'$ is identical to the magnitude of $(L/c_p)$. 
Hence in the code, the dimensional and non-dimensional value of this parameter grouping are identical.


%\bibliographystyle{siamplain}
%\bibliographystyle{elsarticle-harv} 
%\bibliography{references}

\end{document}
