# Mechanics


## Summary  

We solve the mechanical aspects of two phase flow in two stages.

1. Two-field M<sup>c</sup>Kenzie equations for solid velocity vs and fluid pressure p. 
This uses the library functionality described [here](../../../../docs/pde-StokesDarcy2field.md).
2. Given div(vs), we solve a separate Darcy problem for the Darcy velocity and fluid pressure. 
We then evaluate the liquid velocity.
This uses the [following](../../../../docs/pde-Darcy.md) library functionality.

## User choices
The selection of options is described in [Options](options.md). 
In terms of mechanics, the user can specify:

* The polynomial order of the solid velocity (`-pk_v`)
* The polynomial order of the Darcy velocity and pressure (`-dcy_pk_v` and `-dcy_pk_p`)

We found that it was best to choose equal order for the Darcy velocity and pressure, 
and this order should be at least as high as the order of the solid velocity. 
The order of the liquid pressure in the two-field form is determined automatically 
(one smaller than the order of the solid velocity). 

The user can also choose between several constituitive models. 
These are specified using various *Pointwise Physics* objects. 
Currently supported options include:

* Shear and bulk viscosity 
 	- Diffusion creep
 	- Dislocation creep
 	- Composite diffusion+dislocation creep
* Fluid mobility (permeability divided by liquid viscosity)
 	- A power-law porosity-permeability relationship

The user can also specify limits (e.g. to the limit the overall range of viscosities). 







