# A first run


## 1. Preliminaries

0. Install [PETSc](https://www.mcs.anl.gov/petsc/) 
and ensure the environment variables `PETSC_DIR`, `PETSC_ARCH` have been set.
1. Obtain the repository
```git clone https://<USERNAME>@bitbucket.org/dmay/leasz.git```
and move into it the root directory  
``` cd leasz```
2. Set the environment variable `SUBFUSC_CORE_DIR`.
In bash do the following
```export SUBFUSC_CORE_DIR=${PWD}```  
in tcsh do this  
```setenv SUBFUSC_CORE_DIR ${PWD}```
3. Build the library. 
For faster execution you may wish to change the compiler flags in `makefile.arch` 
(and compile against an optimized installation of PETSc).
	- ```make clean``` (not always needed but safest)
	- ```make releaseinfo``` (will write git commit into logged options file)
	- ```make all```
4. Move into the directory of the wedge model ```cd subfusc/models/wedge-0/```
5. Build the wedge model. For faster execution you may wish to change the compiler flags in `Makefile`.
To reiterate, it is essential to compile first the library and then the wedge model.

N.B. installation of library is described in more detail [here](../../../../docs/GettingStarted.md).

## 2. Run a model
Running a model involves calling the wedge.app and specifying an options file (in this case demo1.opts). 
Options files are discussed in more detail [here](options.md). 
Run
```$PETSC_ARCH/bin/src/wedge.app -options_file examples/demo1.opts```

The output can be piped into a log file, e.g.
```$PETSC_ARCH/bin/src/wedge.app -options_file examples/demo1.opts > log.txt 2>&1 &```

Additional or replacement options can be specified as follows. 
For example to use 4th order polynomials for [temperature](energy.md), run 
```$PETSC_ARCH/bin/src/wedge.app -options_file examples/demo1.opts -pk_t 4 ```

Note that the examples are only intended for illustration and are chosen to execute successfully 
and reasonably quickly (although that may mean hours). 
They are not intended to be scientifically interesting. 
More challenging problems will probably require a refined [mesh](../../../../docs/mesh.md) and/or higher-order polynomials. 

## 3. View the output
The output is (by default) written into the directory `output`. 
This can be changed by the option `-output_path`. 

The output directory should contain

* File called `proj.options` which stores the options used to run the model. 
Assuming you ran `make releaseinfo` earlier it will report the git branch and commit. 
* Output data. By default this will consist of a VTU file for each timestep and for several sets of fields. These are:
	- `darcy` (output associated with the [Darcy solve](mechanics.md), including liquid and darcy velocity, composition)
	- `div_vs` (the divergence of the [solid velocity](mechanics.md), sometimes called the compaction rate)
	- `energy` ([temperature](energy.md) and bulk velocity on the whole rectangle including slab, plate and wedge)
	- `pf` (fluid pressure from the 2-field Stokes-Darcy [solve](mechanics.md))
	- `vs` (solid velocity from the 2-field Stokes-Darcy [solve](mechanics.md))
	- `wedge` **<Deprecated>** (various fields projected onto a low-order mesh, some are incorrectly reported)
* PVD files to allow the output data to be easily read into [Paraview](https://www.paraview.org/).
Note that the darcy.pvd is currently missing but can be generated as follows. 
First move into the output directory you specified above.
Then run the script:

~~~
#!/bin/bash
# copy wedge.pvd to darcy.pvd and replace all instances of wedge with darcy

echo Making the darcy.pvd file.
cp wedge.pvd darcy.pvd
ex -sc '%s/wedge/darcy/g|x' darcy.pvd
echo Finished.
~~~

It is also possible to write (some of) the data out in HDF format through the optional flag `-output_hdf`. 
The library also has functions to write XDMF files. 




