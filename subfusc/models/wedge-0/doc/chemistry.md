# Chemistry and Thermodynamics


## Summary  

The solution of the equations of chemical species conservation 
are currently intimately related to the choice of thermodynamic model. 
Thus we describe both together.

## Thermodynamic model (distf)

We use a simple model (called **distf**) 
designed to capture the essence of mantle melting in the presence of water. 
In the model, there are two phases (solid rock and liquid melt) and three thermodynamic components:

* A refractory species (denoted RF)
* A fertile species (denoted FT)
* A hydrous species (denoted H2O)

We assume that the hydrous species is perfectly incompatible 
(only resides in the melt phase) 
while the refractory species is perfectly compatible 
(only resides in the solid phase). 

The only species that participates in melting is the fertile species. 
The thermodynamic behaviour of the model is determined by a distribution function `K(T,P_lith)`  
of temperature `T` and lithostatic pressure `P_lith`. 
We choose `K(T,P_lith)` to roughly match the hydrous melting parameterization of Katz et al. (2003). 

Given that the sum of concentrations in each phase must equal one, 
we can calculate the concentration of fertile species in each phase 
in terms of the porosity and bulk concentrations.

~~~ tex
C_{s,FT} = 1 - C_{s,RF} = 1-C_{bulk,RF}/(1-\phi)
C_{l,FT} = 1 - C_{l,H2O} = 1-C_{bulk,H2O}/\phi
~~~

Then given `K \equiv C_{s,FT}/C_{l,FT}` we solve for `phi` 
(upon rearrangemnt this becomes a quadratic equation) 
and also infer `C_{s,FT}` and `C_{l,FT}`.

## Chemical transport

Under the simplifying assumptions of the **distf** thermodynamic model, 
the chemical transport problem is greatly reduced. 

There are 3 thermodynamic components, so we need to solve 2 transport equations 
(the third component can be recovered by a unity sum). 
The simplest choice is to solve for the evolution of bulk concentration of the refractory and hydrous species 
(since these are only carried in one phase). 

~~~
\partial_t C_{bulk,H2O} + \div [v_l * C_{bulk,H2O}] = 0
\partial_t C_{bulk,FT} + \div [v_s * C_{bulk,H2O}] = 0
~~~

In practice, we add a small artificial diffusion 
to allow the stable solution of these equations by the continuous finite element method.

  

## User choices
The selection of options is described in [Options](options.md). 
In terms of chemistry, the user can specify:

* Initial and boundary conditions for the refractory and hydrous species. 
These are specified with options beginning `-component.RF` and `-component.CH2O` respectively.
* Control the artificial diffusivity used:
	- Fixed diffusivity used in updates involving liquid velocity `-darcy.adv.kappa.cl` 
	- Fixed diffusivity used in updates involving solid velocity `-darcy.adv.kappa.cs` 
	- Fixed diffusivity used in updates involving porosity (used in reconstruction of melting rate \Gamma) `-darcy.adv.kappa.cl` 
	- Optional flag to use adaptive diffusivity `-darcy.adv.kappa.adaptive` based on a target cell Peclet number set by `-darcy.adv.kappa.adaptive.target_pe` (which should be around 1). 

Recalibrating the thermodynamic model requires editing the parameters within the `distf` functions.


## References
Katz, R. F., M. Spiegelman, and C. H. Langmuir (2003), 
A new parameterization of hydrous mantle melting, 
Geochem. Geophys. Geosyst., 
4, 1073, 
[(Link)](https://doi.org/10.1029/2002GC000433).



