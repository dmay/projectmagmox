# SubMaFEC:Wedge User Guide

Authors: 

* Dave A. May (david.may@earth.ox.ac.uk)
* David W. Rees Jones (david.reesjones@earth.ox.ac.uk)
* Richard F. Katz (richard.katz@earth.ox.ac.uk)


## Overview

This user guide will give you an initial overview the wedge model, 
which solves for two-phase flow in the mantle wedge above a subducting slab.

### Sections


* [A first run](first_run.md)
* Physics
	- [Mechanics](mechanics.md)
	- [Energy](energy.md)
	- [Chemistry and Thermodynamics](chemistry.md)
* [Options](options.md)


## References

Please cite the following reference if you use the wedge model in a publication or presentation: 

* [Reference will be added, please contact the authors.]


