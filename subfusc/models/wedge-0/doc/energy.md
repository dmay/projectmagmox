# Energy


## Summary  

Energy conservation is solved for using a temperature equation.
We account for transport of heat by:

1. Liquid and solid advection in terms of a phase-weighted bulk velocity
2. Diffusion
3. Latent heat associated with solidification and melting.

Note that we do not currently consider adiabatic effects, viscous dissipation, radiogenic heating. 

## User choices
The selection of options is described in [Options](options.md). 
In terms of energy, the user can specify:

* The polynomial order for temperautre (`-pk_t 3`)
* The latent to specific heat ratio (`-L_on_Cp`)

Generally, we find that it is advisable to choose a polynomial order for temperature slightly greater than that used for solid velocity.
By setting `-L_on_Cp 0.0`, the user can eliminate the coupling between phase change and the energy equation.




