# User-configurable options


## Overview
Options files are used to configure model runs and facilitate reproducible research. 
They are called as follows
```$PETSC_ARCH/bin/src/wedge.app -options_file examples/demo1.opts```

There are of course many modelling choices that are currently hard-coded. 
These can be changed by changing the source code and recompiling the library.


### Available options
We provide a commented options file in [examples/demo1.opts](../examples/demo1.opts). 
These comments are intended to explain the functionality of (most of) the options available in the library. 

Other options files are designed to illustrate or test different modelling choices. 
For example [examples/demo_dislocation-creep.opts](../examples/demo_dislocation-creep.opts) uses a dislocation creep rheology.

Note that the hyperlinks will only function properly on bitbucket.org if in the master branch.


### Remark
Note that the examples are only intended for illustration 
and are chosen to execute successfully and reasonably quickly (although that may mean hours). 
They are not intended to be scientifically interesting. 
More challenging problems will probably require a refined [mesh](../../../../docs/mesh.md) and/or higher-order polynomials. 