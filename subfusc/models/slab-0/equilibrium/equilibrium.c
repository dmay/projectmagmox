#include <stdio.h>
#include <math.h>
#include <petsc.h>
#include <petscsnes.h>

# define GAS_CONSTANT 8.314   // gas constant

typedef struct{

  PetscReal c_blkh2o;
  PetscReal c_blkco2;
  PetscReal Kh2o;
  PetscReal Kco2;
  PetscReal Wmh;
  PetscReal Wmc;
  PetscReal T;

} ThermoParameters;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal *Wmh;
  PetscReal *Wmc;

} SedData;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal *Wmh;
  PetscReal *Wmc;

} MorbData;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal *Wmh;
  PetscReal *Wmc;

} GabbroData;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *Wmh;
  PetscReal *Wmc;

} PumData;

typedef struct{

  SedData     *sed;
  MorbData    *morb;
  GabbroData  *gabbro;
  PumData     *pum;
  
} ThermoData;


PetscErrorCode equilibrium_calc(ThermoData *, PetscReal, PetscReal, PetscReal, PetscReal, PetscReal *, PetscReal *, PetscReal *, PetscReal *, PetscInt, PetscBool*);
PetscErrorCode FunctionF(SNES, Vec, Vec, void *);
PetscErrorCode JacobianF(SNES, Vec, Mat, Mat, void *);
PetscErrorCode FormResidual(PetscScalar *, PetscScalar *, ThermoParameters *);
PetscErrorCode FormJacobian(PetscScalar *, PetscScalar *, ThermoParameters *);
PetscErrorCode SolveJacobian(PetscScalar *, PetscScalar *, PetscScalar *);
PetscErrorCode NewtonSolver(PetscScalar *, PetscScalar *, PetscScalar *, ThermoParameters *, PetscReal, PetscReal, PetscReal, PetscInt);
PetscErrorCode ImportThermoData(ThermoData *); 
PetscErrorCode ClearThermoData(ThermoData *); 

#undef __FUNCT__
#define __FUNCT__ "ImportThermoData"
PetscErrorCode ImportThermoData(ThermoData *ctx)
{
  PetscReal *p;
  PetscErrorCode ierr;

  PetscFunctionBegin;  

  ierr  =  PetscMalloc1(1, &(ctx->sed));     CHKERRQ(ierr);
  ierr  =  PetscMalloc1(1, &(ctx->morb));    CHKERRQ(ierr);
  ierr  =  PetscMalloc1(1, &(ctx->gabbro));  CHKERRQ(ierr);
  ierr  =  PetscMalloc1(1, &(ctx->pum));     CHKERRQ(ierr);

  /*************  sed  ******************/
  ierr  =  PetscMalloc1(3, &(ctx->sed->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->sed->cmaxh;
  p[0]  =  -0.150662555544386;
  p[1]  =  0.301807008226067;
  p[2]  =  1.018673820928173;

  ierr  =  PetscMalloc1(6, &(ctx->sed->logLrh));    CHKERRQ(ierr);
  p     =  ctx->sed->logLrh;
  p[0]  =  -3.533135743197558;
  p[1]  =  19.068737477212224;
  p[2]  =  -37.302837204809386;
  p[3]  =  31.324429026695910;
  p[4]  =  -10.317259468993946;
  p[5]  =  8.688808175310820;

  ierr  =  PetscMalloc1(6, &(ctx->sed->Tmh));        CHKERRQ(ierr);
  p     =  ctx->sed->Tmh;  
  p[0]  =  1.296369387210849;
  p[1]  =  -24.330963411722223; 
  p[2]  =  1.697986731996718e+02;
  p[3]  =  -5.368530912624840e+02;
  p[4]  =  7.629895101455530e+02;
  p[5]  =  2.423527272231045e+02;

  ierr  =  PetscMalloc1(1, &(ctx->sed->cmaxc));      CHKERRQ(ierr); 
  p     =  ctx->sed->cmaxc;    
  *p    =  10.592282487499954;

  ierr  =  PetscMalloc1(6, &(ctx->sed->logLrc));     CHKERRQ(ierr);
  p     =  ctx->sed->logLrc;   
  p[0]  =  14.007787232073160;
  p[1]  =  -81.025362109897810;
  p[2]  =  1.746329337200755e+02;
  p[3]  =  -1.700791911486186e+02;
  p[4]  =  69.688713478378430;
  p[5]  =  1.378376757567256;
  
  ierr  =  PetscMalloc1(3, &(ctx->sed->Tmc));        CHKERRQ(ierr);
  p     =  ctx->sed->Tmc;
  p[0]  =  -19.534288871002960;
  p[1]  =  2.116075955389894e+02;
  p[2]  =  7.026318145363642e+02;

  ierr  =  PetscMalloc1(3, &(ctx->sed->deltaTc));   CHKERRQ(ierr);
  p     =  ctx->sed->deltaTc;
  p[0]  =  -17.780655913253780;
  p[1]  =  1.290578179906597e+02;
  p[2]  =  -10.089551673592354;
  
  ierr  =  PetscMalloc1(1, &(ctx->sed->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->sed->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->sed->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->sed->Wmc;    
  *p    =  -8000.0; 

  /*************  morb  ******************/
  ierr  =  PetscMalloc1(4, &(ctx->morb->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->morb->cmaxh;
  p[0]  =  0.010272471440474;
  p[1]  =  -0.115390189642000;
  p[2]  =  0.324451844950027;
  p[3]  =  1.415887869218784;

  ierr  =  PetscMalloc1(4, &(ctx->morb->logLrh));    CHKERRQ(ierr);
  p     =  ctx->morb->logLrh;
  p[0]  =  1.312630638962006;
  p[1]  =  -3.941703113796766;
  p[2]  =  2.750798457184458;
  p[3]  =  8.431772535722878;

  ierr  =  PetscMalloc1(6, &(ctx->morb->Tmh));        CHKERRQ(ierr);
  p     =  ctx->morb->Tmh;  
  p[0]  =  0.271046787787927;
  p[1]  =  -8.445036183735617; 
  p[2]  =  82.556558104728500;
  p[3]  =  -3.342487965612544e+02;
  p[4]  =  5.592352636804376e+02;
  p[5]  =  3.967635986627150e+02;

  ierr  =  PetscMalloc1(1, &(ctx->morb->cmaxc));      CHKERRQ(ierr); 
  p     =  ctx->morb->cmaxc;    
  *p    =  19.045565762499937;

  ierr  =  PetscMalloc1(6, &(ctx->morb->logLrc));     CHKERRQ(ierr);
  p     =  ctx->morb->logLrc;   
  p[0]  =  1.367443166281999;
  p[1]  =  -18.846082066295384;
  p[2]  =  62.960752263466270;
  p[3]  =  -80.779851142775770;
  p[4]  =  40.165085572683090;
  p[5]  =  4.354032367607715;
  
  ierr  =  PetscMalloc1(3, &(ctx->morb->Tmc));        CHKERRQ(ierr);
  p     =  ctx->morb->Tmc;
  p[0]  =  -17.658692732447540;
  p[1]  =  1.944885995727770e+02;
  p[2]  =  7.481072175613556e+02;

  ierr  =  PetscMalloc1(3, &(ctx->morb->deltaTc));   CHKERRQ(ierr);
  p     =  ctx->morb->deltaTc;
  p[0]  =  -17.780655913253780;
  p[1]  =  1.290578179906597e+02;
  p[2]  =  -10.089551673592354;
  
  ierr  =  PetscMalloc1(1, &(ctx->morb->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->morb->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->morb->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->morb->Wmc;    
  *p    =  -8000.0;  

  /*************  gabbro  ******************/
  ierr  =  PetscMalloc1(3, &(ctx->gabbro->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->gabbro->cmaxh;
  p[0]  =  -0.017667335340126;
  p[1]  =  0.089304408418261;
  p[2]  =  1.527316327453626;

  ierr  =  PetscMalloc1(5, &(ctx->gabbro->logLrh));    CHKERRQ(ierr);
  p     =  ctx->gabbro->logLrh;
  p[0]  =  -1.009961038518608;
  p[1]  =  5.217453113389753;
  p[2]  =  -8.951498304566513;
  p[3]  =  4.747716716081742;
  p[4]  =  8.516063062352552;

  ierr  =  PetscMalloc1(5, &(ctx->gabbro->Tmh));        CHKERRQ(ierr);
  p     =  ctx->gabbro->Tmh;  
  p[0]  =  -3.000118134386033;
  p[1]  =  45.352910295461910;
  p[2]  =  -2.388331789925089e+02;
  p[3]  =  5.170575818336469e+02;
  p[4]  =  3.756317222662897e+02;

  ierr  =  PetscMalloc1(1, &(ctx->gabbro->cmaxc));      CHKERRQ(ierr); 
  p     =  ctx->gabbro->cmaxc;    
  *p    =  19.379454537499885;

  ierr  =  PetscMalloc1(4, &(ctx->gabbro->logLrc));     CHKERRQ(ierr);
  p     =  ctx->gabbro->logLrc;   
  p[0]  =  2.739281957244409;
  p[1]  =  -9.851731701468713;
  p[2]  =  9.146439180475213;
  p[3]  =  9.055596526151467;
  
  ierr  =  PetscMalloc1(3, &(ctx->gabbro->Tmc));        CHKERRQ(ierr);
  p     =  ctx->gabbro->Tmc;
  p[0]  =  -14.165093677306569;
  p[1]  =  1.809036929023690e+02;
  p[2]  =  7.757132892363031e+02;

  ierr  =  PetscMalloc1(3, &(ctx->gabbro->deltaTc));   CHKERRQ(ierr);
  p     =  ctx->gabbro->deltaTc;
  p[0]  =  -11.794472521973619;
  p[1]  =  89.136690826367680;
  p[2]  =  1.711230429250124;
  
  ierr  =  PetscMalloc1(1, &(ctx->gabbro->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->gabbro->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->gabbro->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->gabbro->Wmc;    
  *p    =  -50000.0; 


   /*************  pum  ******************/
  ierr  =  PetscMalloc1(2, &(ctx->pum->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->pum->cmaxh;
  p[0]  =  0.001156281237125;
  p[1]  =  2.421787983195434;

  ierr  =  PetscMalloc1(9, &(ctx->pum->logLrh));    CHKERRQ(ierr);
  p     =  ctx->pum->logLrh;
  p[0]  =  -19.060885857770412;
  p[1]  =  1.689826532012307e+02;
  p[2]  =  -6.300318077012928e+02;
  p[3]  =  1.281835967642164e+03;
  p[4]  =  -1.543141708645560e+03;
  p[5]  =  1.111879888618042e+03;
  p[6]  =  -4.591417538523027e+02;
  p[7]  =  95.414320683178630;
  p[8]  =  1.972456309737587;

  ierr  =  PetscMalloc1(3, &(ctx->pum->Tmh));        CHKERRQ(ierr);
  p     =  ctx->pum->Tmh;  
  p[0]  =  -15.113105096276987;
  p[1]  =  92.697643261252640;
  p[2]  =  6.389776673404846e+02;

  ierr  =  PetscMalloc1(1, &(ctx->pum->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->pum->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->pum->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->pum->Wmc;    
  *p    =  -50000.0;  

  PetscFunctionReturn(0);  
  
}

#undef __FUNCT__
#define __FUNCT__ "ClearThermoData"
PetscErrorCode ClearThermoData(ThermoData *ctx)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;  
  
  ierr = PetscFree(ctx->sed->cmaxh);         CHKERRQ(ierr);
  ierr = PetscFree(ctx->sed->logLrh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Tmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->cmaxc);         CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->logLrc);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Tmc);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->deltaTc);       CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Wmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Wmc);           CHKERRQ(ierr);  
  ierr = PetscFree(ctx->sed);                CHKERRQ(ierr); 
  
  ierr = PetscFree(ctx->morb->cmaxh);        CHKERRQ(ierr);
  ierr = PetscFree(ctx->morb->logLrh);       CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Tmh);          CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->cmaxc);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->logLrc);       CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Tmc);          CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->deltaTc);      CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Wmh);          CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Wmc);          CHKERRQ(ierr);  
  ierr = PetscFree(ctx->morb);               CHKERRQ(ierr); 

  ierr = PetscFree(ctx->gabbro->cmaxh);      CHKERRQ(ierr);
  ierr = PetscFree(ctx->gabbro->logLrh);     CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Tmh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->cmaxc);      CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->logLrc);     CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Tmc);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->deltaTc);    CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Wmh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Wmc);        CHKERRQ(ierr);  
  ierr = PetscFree(ctx->gabbro);             CHKERRQ(ierr); 

  ierr = PetscFree(ctx->pum->cmaxh);         CHKERRQ(ierr);
  ierr = PetscFree(ctx->pum->logLrh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->pum->Tmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->pum->Wmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->pum->Wmc);           CHKERRQ(ierr);  
  ierr = PetscFree(ctx->pum);                CHKERRQ(ierr);  

  PetscFunctionReturn(0);
  
}


int main(int argc, char **argv){

  PetscReal T, P, w_h2o, w_co2;   // input values
  PetscBool found, validtype;
  PetscInt rocktype;   // 1 for sed, 2 for morb, 3 for gabbro, and 4 for primitive upper mantle
  PetscErrorCode ierr;
  PetscReal fc_guess[2];
  PetscReal frac, composition_s[2], composition_l[2];  // output results, first component is for h2o, second is for co2.

  ThermoData ctx;

  PetscInitialize(&argc,&argv,(char *)0,NULL);

//  ierr        = PetscOptionsInsertString(NULL, "-eq_snes_fd"); CHKERRQ(ierr);

  ierr        = ImportThermoData(&ctx);                        CHKERRQ(ierr);

  fc_guess[0] = 1e-6;
  fc_guess[1] = 7.5e-1;

  found = PETSC_FALSE; ierr = PetscOptionsGetInt(NULL, NULL, "-rocktype", &rocktype, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a rocktype (from 1 to 4) via option -rocktype");
  
  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-T", &T, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a temperature (K) via option -T");

  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-P", &P, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a Pressure (GPa) via option -P");

  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-wh2o", &w_h2o, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a bulk concentration of H2O (weight fraction) via option -wh2o");

  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-wco2", &w_co2, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a bulk concentration of CO2 (weight fraction) via option -wco2");
  
  ierr = equilibrium_calc(&ctx, T, P, w_h2o, w_co2, fc_guess, &frac, composition_s, composition_l, rocktype, &validtype); CHKERRQ(ierr);

  if (validtype) {
    ierr = PetscPrintf(PETSC_COMM_SELF, "Calculated weight fraction of liquid phase is: %5.4e, h2o weight fraction in liquid is: %5.4e, in solid is: %5.4e; and co2 weight fraction in liquid is: %5.4e, in solid is: %5.4e\n", \
		       frac, composition_l[0], composition_s[0], composition_l[1], composition_s[1]); CHKERRQ(ierr);
  }else{
    ierr = PetscPrintf(PETSC_COMM_SELF, "No devolatilization occurs! Try another P/T pair.\n");
  }

  ierr        = ClearThermoData(&ctx);                        CHKERRQ(ierr);  

  ierr = PetscFinalize();
  return 0;
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "equilibrium_calc"
PetscErrorCode equilibrium_calc(ThermoData *ctx, PetscReal T, PetscReal P, PetscReal w_h2o, PetscReal w_co2, PetscReal *fc_guess, PetscReal *frac, PetscReal *composition_s, PetscReal *composition_l, PetscInt rocktype, PetscBool *validtype)
/* ------------------------------------------------------------------- */
{  
  // Thermodynamic parameters
  PetscReal         cmaxh2o, Lrh2o, Tmh2o, cmaxco2, Lrco2, Tmco2, deltaTco2;
  ThermoParameters  *p;
  PetscReal         *cmaxh, *logLrh, *Tmh, *logLrc, *Tmc, *deltaTc;
  PetscReal         cmaxc;
      
  // Nonlinear Solver
  PetscScalar    f_array[2];
  PetscScalar    fc_array[2];
  PetscScalar    A[4];
  PetscErrorCode ierr;

  PetscFunctionBegin;

  ierr = PetscMalloc1(1, &p); CHKERRQ(ierr);
  p->c_blkh2o = w_h2o;
  p->c_blkco2 = w_co2;
  p->T        = T;

  switch (rocktype) {
    case 1: {       //sed
     
      cmaxh    = ctx->sed->cmaxh;
      logLrh   = ctx->sed->logLrh;
      Tmh      = ctx->sed->Tmh;      
      cmaxc    = *(ctx->sed->cmaxc);
      logLrc   = ctx->sed->logLrc;
      Tmc      = ctx->sed->Tmc;
      deltaTc  = ctx->sed->deltaTc;   
      p->Wmh  = *(ctx->sed->Wmh);
      p->Wmc  = *(ctx->sed->Wmc);  
      
      cmaxh2o  = PetscExpReal(cmaxh[0]*(log(P)*log(P)) + cmaxh[1]*log(P) + cmaxh[2]);
      Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 5) + logLrh[1]/PetscPowReal(P, 4)  + logLrh[2]/PetscPowReal(P, 3) + logLrh[3]/(P*P) + logLrh[4]/P + logLrh[5]);
      Tmh2o    = Tmh[0]*PetscPowReal(P, 5) + Tmh[1]*PetscPowReal(P, 4)  + Tmh[2]*PetscPowReal(P, 3) + Tmh[3]*(P*P) + Tmh[4]*P + Tmh[5];
    
      cmaxco2  = cmaxc;
      Lrco2    = PetscExpReal(logLrc[0]/PetscPowReal(P, 5)  + logLrc[1]/PetscPowReal(P, 4) + logLrc[2]/PetscPowReal(P, 3) + logLrc[3]/(P*P) + logLrc[4]/P + logLrc[5]);
      Tmco2    = Tmc[0]*(P*P) + Tmc[1]*P + Tmc[2];
    
      deltaTco2 = deltaTc[0]*(P*P) + deltaTc[1]*P + deltaTc[2];
      Tmco2     = Tmco2 - deltaTco2;
    } break;

    case 2: {    //morb

      cmaxh    = ctx->morb->cmaxh;
      logLrh   = ctx->morb->logLrh;
      Tmh      = ctx->morb->Tmh;      
      cmaxc    = *(ctx->morb->cmaxc);
      logLrc   = ctx->morb->logLrc;
      Tmc      = ctx->morb->Tmc;
      deltaTc  = ctx->morb->deltaTc;   
      p->Wmh  = *(ctx->morb->Wmh);
      p->Wmc  = *(ctx->morb->Wmc);      
     
      cmaxh2o  = PetscExpReal(cmaxh[0]*PetscPowReal(P, 3) + cmaxh[1]*PetscPowReal(P, 2) + cmaxh[2]*P + cmaxh[3]);
      Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 3) + logLrh[1]/PetscPowReal(P, 2)  + logLrh[2]/P + logLrh[3]);
      Tmh2o    = Tmh[0]*PetscPowReal(P, 5) + Tmh[1]*PetscPowReal(P, 4) + Tmh[2]*PetscPowReal(P, 3) + Tmh[3]*(P*P) + Tmh[4]*P + Tmh[5];
  
      cmaxco2  = cmaxc;
      Lrco2    = PetscExpReal(logLrc[0]/PetscPowReal(P, 5)  + logLrc[1]/PetscPowReal(P, 4) + logLrc[2]/PetscPowReal(P, 3) + logLrc[3]/(P*P) + logLrc[4]/P + logLrc[5]);
      Tmco2    = Tmc[0]*(P*P) + Tmc[1]*P + Tmc[2];
  
      deltaTco2 = deltaTc[0]*(P*P) + deltaTc[1]*P + deltaTc[2];
      Tmco2     = Tmco2 - deltaTco2;
    } break; 

    case 3: {  //gabbro
	
      cmaxh    = ctx->gabbro->cmaxh;
      logLrh   = ctx->gabbro->logLrh;
      Tmh      = ctx->gabbro->Tmh;      
      cmaxc    = *(ctx->gabbro->cmaxc);
      logLrc   = ctx->gabbro->logLrc;
      Tmc      = ctx->gabbro->Tmc;
      deltaTc  = ctx->gabbro->deltaTc;   
      p->Wmh  = *(ctx->gabbro->Wmh);
      p->Wmc  = *(ctx->gabbro->Wmc);      
      
      cmaxh2o  = PetscExpReal(cmaxh[0]*(log(P)*log(P)) + cmaxh[1]*log(P) + cmaxh[2]);
      Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 4) + logLrh[1]/PetscPowReal(P, 3)  + logLrh[2]/(P*P) + logLrh[3]/P + logLrh[4]);
      Tmh2o    = Tmh[0]*PetscPowReal(P, 4) + Tmh[1]*PetscPowReal(P, 3)  + Tmh[2]*(P*P) + Tmh[3]*P + Tmh[4];
  
      cmaxco2  = cmaxc;
      Lrco2    = PetscExpReal(logLrc[0]/PetscPowReal(P, 3)  + logLrc[1]/(P*P) + logLrc[2]/P + logLrc[3]);
      Tmco2    = Tmc[0]*(P*P) + Tmc[1]*P + Tmc[2];
  
      deltaTco2 = deltaTc[0]*(P*P) + deltaTc[1]*P + deltaTc[2];
      Tmco2     = Tmco2 - deltaTco2;
    } break; 

    case 4: {   // primitive upper mantle
      
      PetscReal T1, T2, T3;

      cmaxh    = ctx->pum->cmaxh;
      logLrh   = ctx->pum->logLrh;
      Tmh      = ctx->pum->Tmh;
      p->Wmh    = *(ctx->pum->Wmh);
      p->Wmc    = *(ctx->pum->Wmc);     

      T1       = P*1.072054506210788e+02 + 8.966542633680793e+02; 
      T2       = P*1.533210093204700e+02 + 8.433396814457046e+02; 
      T3       = P*2.491790919291565e+02 + 8.375304594099354e+02;
  
      if (T1 < T2) {
  
        if (p->T < T1) {
  
          p->Kco2 = 32.884202059134026;
  
        } else if ((p->T >= T1)&&(p->T < T2)) {
          
          p->Kco2 = 28.961173923316494;
        
        } else if ((p->T >= T2)&&(p->T < T3)) {
        
          p->Kco2 = 14.752123772328618;
  
        } else {
  
          p->Kco2 = 0.323043468637191;      
  
        }
      } else {
      
        if (p->T < T2) {
        
          p->Kco2 = 32.884202059134026; 
  
        } else if ((p->T >= T2)&&(p->T < T3)) {
  
          p->Kco2 = 14.752123772328618;
        
        } else {
        
          p->Kco2 = 0.323043468637191;
  
        }
      
      }
  
      p->Kco2 = p->Kco2/100.0;
  
      cmaxh2o  = PetscExpReal(cmaxh[0]*P + cmaxh[1]);
      Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 8) + logLrh[1]/PetscPowReal(P, 7)  + logLrh[2]/PetscPowReal(P, 6) + logLrh[3]/PetscPowReal(P, 5) + logLrh[4]/PetscPowReal(P, 4) + logLrh[5]/PetscPowReal(P, 3) + logLrh[6]/PetscPowReal(P, 2) + logLrh[7]/P + logLrh[8]);
      Tmh2o    = Tmh[0]*P*P  + Tmh[1]*P + Tmh[2];

    } break;

  }

  p->Kh2o   = cmaxh2o*PetscExpReal(Lrh2o*(1.0/T - 1.0/Tmh2o))/100.0;
  
  if (rocktype != 4) {
    p->Kco2   = cmaxco2*PetscExpReal(Lrco2*(1.0/T - 1.0/Tmco2))/100.0;
  }

  fc_array[0] = fc_guess[0];
  fc_array[1] = fc_guess[1];

  ierr        = NewtonSolver(fc_array, f_array, A, p, 1e-12, 1e-24, 1e-12, 100);  CHKERRQ(ierr);
  
  PetscPrintf(PETSC_COMM_SELF, "%lf, %lf\n", fc_array[0], fc_array[1]);

  if (fc_array[0]>0.0&&fc_array[0]<1.0&&fc_array[1]>0.0&&fc_array[1]<1.0){

    *frac = fc_array[0];
    composition_l[0] = fc_array[1];
    composition_l[1] = 1 - fc_array[1];
    composition_s[0] = p->Kh2o*PetscExpReal(p->Wmh*composition_l[1]*composition_l[1]/(GAS_CONSTANT*p->T))*composition_l[0];
    composition_s[1] = p->Kco2*PetscExpReal(p->Wmc*composition_l[0]*composition_l[0]/(GAS_CONSTANT*p->T))*composition_l[1];

    *validtype       = PETSC_TRUE;
  
  }else{

    *frac = 0.0;
    composition_l[0] = 0.0;
    composition_l[1] = 0.0;
    composition_s[0] = w_h2o;
    composition_s[1] = w_co2;

    *validtype       = PETSC_FALSE;
  }
  
  ierr = PetscFree(p);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FunctionF"
PetscErrorCode FunctionF(SNES snes, Vec x, Vec f, void *ptr)
/* ------------------------------------------------------------------- */
{
  ThermoParameters   *p = (ThermoParameters*)ptr;
  const PetscScalar  *fc_array;
  PetscScalar *f_array;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  
  ierr = VecGetArrayRead(x, &fc_array); CHKERRQ(ierr);
  ierr = VecGetArray(f, &f_array); CHKERRQ(ierr);

  f_array[0] = p->c_blkh2o - fc_array[0]*fc_array[1] - (1.0 - fc_array[0])*fc_array[1]*p->Kh2o*PetscExpReal((p->Wmh*((1.0 - fc_array[1])*(1.0 - fc_array[1])))/(GAS_CONSTANT*p->T));
  f_array[1] = p->c_blkco2 - fc_array[0]*(1.0 - fc_array[1]) - (1.0 - fc_array[0])*(1.0 - fc_array[1])*p->Kco2*PetscExpReal((p->Wmc*(fc_array[1]*fc_array[1]))/(GAS_CONSTANT*p->T));

  ierr = VecRestoreArrayRead(x, &fc_array); CHKERRQ(ierr);
  ierr = VecRestoreArray(f, &f_array); CHKERRQ(ierr);
  
  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "JacobianF"
PetscErrorCode JacobianF(SNES snes, Vec x, Mat Jac, Mat PJac, void *ptr)
/* ------------------------------------------------------------------- */
{
  ThermoParameters   *p = (ThermoParameters*)ptr;
  const PetscScalar  *xx;
  PetscScalar        A[4];
  PetscInt           idx[2]={0,1};
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  
  ierr = VecGetArrayRead(x, &xx); CHKERRQ(ierr);

  A[0] = -xx[1] + xx[1]*p->Kh2o*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[1] = -xx[0] - (1 - xx[0])*p->Kh2o*(1 - 2.0*xx[1]*(1 - xx[0])*p->Wmh/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[2] = -(1 - xx[1]) + (1 - xx[1])*p->Kco2*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));
  A[3] =  xx[0] - (1 - xx[0])*p->Kco2*(-1 + 2.0*xx[1]*(1 - xx[1])*p->Wmc/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));

  ierr = MatSetValues(Jac, 2, idx, 2, idx, A, INSERT_VALUES); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(x, &xx); CHKERRQ(ierr);

  ierr = MatAssemblyBegin(Jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

  if (PJac!=Jac) {
    ierr = MatAssemblyBegin(PJac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(PJac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  }

  //  MatView(PJac, PETSC_VIEWER_STDOUT_SELF);
  
  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormResidual"
PetscErrorCode FormResidual(PetscScalar *fc_array, PetscScalar *f_array, ThermoParameters *p)
/* ------------------------------------------------------------------- */
{
  PetscFunctionBegin;
  
  f_array[0] = p->c_blkh2o - fc_array[0]*fc_array[1] - (1.0 - fc_array[0])*fc_array[1]*p->Kh2o*PetscExpReal((p->Wmh*((1.0 - fc_array[1])*(1.0 - fc_array[1])))/(GAS_CONSTANT*p->T));
  f_array[1] = p->c_blkco2 - fc_array[0]*(1.0 - fc_array[1]) - (1.0 - fc_array[0])*(1.0 - fc_array[1])*p->Kco2*PetscExpReal((p->Wmc*(fc_array[1]*fc_array[1]))/(GAS_CONSTANT*p->T));

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormJacobian"
PetscErrorCode FormJacobian(PetscScalar *xx, PetscScalar *A, ThermoParameters *p)
/* ------------------------------------------------------------------- */
{
  PetscFunctionBegin;

  A[0] = -xx[1] + xx[1]*p->Kh2o*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[1] = -xx[0] - (1 - xx[0])*p->Kh2o*(1 - 2.0*xx[1]*(1 - xx[0])*p->Wmh/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[2] = -(1 - xx[1]) + (1 - xx[1])*p->Kco2*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));
  A[3] =  xx[0] - (1 - xx[0])*p->Kco2*(-1 + 2.0*xx[1]*(1 - xx[1])*p->Wmc/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "SolveJacobian"
PetscErrorCode SolveJacobian(PetscScalar *A, PetscScalar *f_array, PetscScalar *dx)
/* ------------------------------------------------------------------- */
{
  PetscScalar det, inv_det;
  PetscFunctionBegin;
  
  det     = A[0]*A[3] - A[1]*A[2];
  inv_det = 1.0/det;


  dx[0]   = inv_det * (A[3] * f_array[0] - A[1] * f_array[1]);
  dx[1]   = inv_det * (-A[2] * f_array[0] + A[0] * f_array[1]);
  
  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "NewtonSolver"
PetscErrorCode NewtonSolver(PetscScalar *fc_array, PetscScalar *f_array, PetscScalar *A, ThermoParameters *p, PetscReal atol, PetscReal rtol, PetscReal stol, PetscInt maxit)
/* ------------------------------------------------------------------- */
{
  PetscScalar     dx[2];
  PetscInt        its = 0;
  PetscReal       a0, a1, r, d;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  

  d    = 1.0;   // norm of solution change
  r    = 1.0;   // ratio of residual norm between current and last iterations

  ierr = FormResidual(fc_array, f_array, p);  
  a0   = sqrt(f_array[0] * f_array[0] + f_array[1] * f_array[1]);
  printf("%d:  ||F|| % 1.12e \n", its, a0);
  a1 = a0;

  if (a1 < atol) {goto converged;} 
  
  while (a1 >= atol && r >= rtol && d >= stol && its <= maxit) {
    
    its++;

    ierr  = FormJacobian(fc_array, A, p);      CHKERRQ(ierr);
    ierr  = SolveJacobian(A, f_array, dx);     CHKERRQ(ierr);

    fc_array[0] = fc_array[0] - dx[0];
    fc_array[1] = fc_array[1] - dx[1];
    
    ierr = FormResidual(fc_array, f_array, p);
    a1   = sqrt(f_array[0] * f_array[0] + f_array[1] * f_array[1]);
    r    = a1/a0;
    d    = sqrt(dx[0] * dx[0] + dx[1] * dx[1]);

    printf("%d:  ||F|| % 1.12e \n", its, a1);

  }

  converged:

  PetscFunctionReturn(0);

}
