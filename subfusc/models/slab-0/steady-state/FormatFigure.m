function  []  =  FormatFigure(titlename,map)

FS  =  14;

shading flat; 
axis ij equal tight; 

title(titlename,'FontName','Helvetica','FontSize',FS)

ylabel('Depth [km]','FontName','Helvetica','FontSize',FS)
xlabel('Width [km]','FontName','Helvetica','FontSize',FS)
set(gca,'FontName','Helvetica','FontSize',FS)

colorbar('FontName','Helvetica','FontSize',FS)
colormap(map);

box on

end