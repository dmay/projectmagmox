function [] = RunResults( s, interval, e )

workdir      =  pwd;
addpath('/Applications/Utilities/petsc/petsc-3.7.4/share/petsc/matlab/')
addpath(pwd)

%*****  choose output data directory and filename to plot  ****************

datadir      =  '/Users/eart0381/Desktop/code/leasz/src/steady-state/';

%*****  select output fields (must correspond to PETSc DoOutput  **********

VCsol        =  {'v_f';'c_blk';'h_blk'};
PHICsol      =  {'phi';'c_f';'c_s';'h_f';'h_s'};
PTXsol       =  {'P';'T';'X'};

cd(datadir);

for i=s:interval:e
  
    filename  =  ['steadystate_' num2str(i)];
    
    currfile  =  filename;
    OP        =  ReadPETScResults(currfile,VCsol,PHICsol,PTXsol);
    
    [X,Z]     =  meshgrid(-1.5*OP.PAR.h:OP.PAR.h:(OP.PAR.nx-2)*OP.PAR.h,-1.5*OP.PAR.h:OP.PAR.h:(OP.PAR.nz-2)*OP.PAR.h);
    
    
    figure(1)
    clf
    pcolor(X,Z,flipud(OP.PHIC.phi)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('Porosity');
    colormap jet; colorbar;
    drawnow
    
    figure(2)
    clf
    pcolor(X,Z,flipud(OP.VC.v_f)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('fluid velocity');
    colormap jet; colorbar;
    drawnow
    
    figure(3)
    clf
    pcolor(X,Z,flipud(OP.PHIC.c_f)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('CO_2 in liquid phase');
    colormap jet; colorbar;
    drawnow
    
    figure(4)
    clf
    pcolor(X,Z,flipud(OP.PHIC.h_f)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('H_2O in liquid phase');
    colormap jet; colorbar;
    drawnow
    
    figure(5)
    clf
    pcolor(X,Z,flipud(OP.VC.c_blk)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('Bulk CO_2 in rock');
    colormap jet; colorbar;
    drawnow
    
    figure(6)
    clf
    pcolor(X,Z,flipud(OP.VC.h_blk)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('Bulk H_2O in rock');
    colormap jet; colorbar;
    drawnow
    
    figure(7)
    clf
    pcolor(X,Z,flipud(OP.PTX.P)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('Pressure field (GPa)');
    colormap jet; colorbar;
    drawnow
    
    figure(8)
    clf
    pcolor(X,Z,flipud(OP.PTX.T - 273)); shading interp; axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('Temperature field (^oC)');
    colormap jet; colorbar;
    drawnow

end

end

