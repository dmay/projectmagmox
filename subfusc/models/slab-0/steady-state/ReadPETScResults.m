
function [OP]  =  ReadPETScResults(filename,VCsolfields,PHICsolfields,PTXsolfields)

OP.filename  =  filename;
fd           =  PetscOpenFile(OP.filename);

%***  read parameter bag

OP.PAR       =  PetscBinaryRead(fd);


%***  read solution fields

nf           =  length(VCsolfields);
tmp          =  PetscBinaryRead(fd);
tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
OP.VC        =  {};
    
for n = 1:nf
    OP.VC  =  setfield(OP.VC,VCsolfields{n},squeeze(tmp(n,:,:))');
end

%***  read old solution fields

nf           =  length(VCsolfields);
tmp          =  PetscBinaryRead(fd);
tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
OP.VCo        =  {};
    
for n = 1:nf
    OP.VCo  =  setfield(OP.VCo,VCsolfields{n},squeeze(tmp(n,:,:))');
end

%***  read RVC fields

nf           =  length(VCsolfields);
tmp          =  PetscBinaryRead(fd);
tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
OP.RVC       =  {};
    
for n = 1:nf
    OP.RVC   =  setfield(OP.RVC,VCsolfields{n},squeeze(tmp(n,:,:))');
end

%***  read PHIC fields

nf           =  length(PHICsolfields);
tmp          =  PetscBinaryRead(fd);
tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
OP.PHIC      =  {};
    
for n = 1:nf
    OP.PHIC  =  setfield(OP.PHIC,PHICsolfields{n},squeeze(tmp(n,:,:))');
end

%***  read PHICo fields

nf           =  length(PHICsolfields);
tmp          =  PetscBinaryRead(fd);
tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
OP.PHICo      =  {};
    
for n = 1:nf
    OP.PHICo  =  setfield(OP.PHICo,PHICsolfields{n},squeeze(tmp(n,:,:))');
end

%***  read PTX fields

nf           =  length(PTXsolfields);
tmp          =  PetscBinaryRead(fd);
tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
OP.PTX       =  {};
    
for n = 1:nf
    OP.PTX   =  setfield(OP.PTX,PTXsolfields{n},squeeze(tmp(n,:,:))');
end


%***  read solution residuals

% nf           =  length(VPsolfields);
% tmp          =  PetscBinaryRead(fd);
% tmp          =  reshape(tmp,nf,OP.PAR.nx,OP.PAR.nz);
% OP.RVP       =  {};
%     
% for n = 1:nf
%     OP.RVP   =  setfield(OP.RVP,VPsolfields{n},squeeze(tmp(n,:,:))');
% end


clear tmp;

close(fd);

end

