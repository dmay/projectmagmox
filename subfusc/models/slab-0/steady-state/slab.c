#include <stdio.h>
#include <math.h>
#include <petsc.h>
#include <petscsnes.h>

# define GAS_CONSTANT 8.314   // gas constant
# define FNAME_LENGTH 120
# define YEARTOSEC 31557600.0

#define REG_REAL(A,B,C,D,E)   ierr=PetscBagRegisterReal(A,B,C,D,E);      CHKERRQ(ierr)
#define REG_INTG(A,B,C,D,E)   ierr=PetscBagRegisterInt(A,B,C,D,E);       CHKERRQ(ierr)
#define REG_STRG(A,B,C,D,E,F) ierr=PetscBagRegisterString(A,B,C,D,E,F);  CHKERRQ(ierr)

typedef struct{

  PetscReal c_blkh2o;
  PetscReal c_blkco2;
  PetscReal cmaxh2o;
  PetscReal Lrh2o;
  PetscReal Tmh2o;
  PetscReal Kh2o;
  PetscReal cmaxco2;
  PetscReal Lrco2;
  PetscReal Tmco2;
  PetscReal Kco2;
  PetscReal Wmh;
  PetscReal Wmc;
  PetscReal T;

} ThermoParameters;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal *Wmh;
  PetscReal *Wmc;

} SedData;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal *Wmh;
  PetscReal *Wmc;

} MorbData;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *cmaxc;
  PetscReal *logLrc;
  PetscReal *Tmc;
  PetscReal *deltaTc;
  PetscReal *Wmh;
  PetscReal *Wmc;

} GabbroData;

typedef struct{

  PetscReal *cmaxh;
  PetscReal *logLrh;
  PetscReal *Tmh;
  PetscReal *Wmh;
  PetscReal *Wmc;

} PumData;

typedef struct{

  SedData     *sed;
  MorbData    *morb;
  GabbroData  *gabbro;
  PumData     *pum;
  
} ThermoData;


typedef struct{
  PetscReal v_f;  
  PetscReal c_blk;
  PetscReal h_blk;
  
} VCField;

typedef struct{
  PetscReal phi;
  PetscReal c_f;
  PetscReal c_s;
  PetscReal h_f;
  PetscReal h_s;
  
} PhiCField;

typedef struct{
  PetscReal P;
  PetscReal T;
  PetscInt  X;
  
} PTXField;

typedef struct{
  PetscReal   v_s;
  PetscReal   angle;
  PetscReal   rhof;
  PetscReal   rhos;

  PetscInt    ndim, nx, nz;
  PetscReal   depth, width, h, ucos, wsin;
  PetscReal   phi0, flim;

  PetscReal   T1, l, Re, alpha;   // parameters for slab temperature calc from McKenzie (1969)

  PetscReal   lscale, vscale, tscale, rhoscale;

  PetscReal   t, dt, tmax;
  PetscInt    nt, nop, frame;

  PetscInt    maxit;
  PetscReal   atol, rtol;
  char        filename[FNAME_LENGTH];
  
} Parameter;

typedef struct{
  Parameter           *par;
  ThermoData          *ctx;
  ThermoParameters    *tp;
  PetscBag            bag;
  DM                  da_vc, da_phic, da_ptx;
  SNES                snes_vc;
  Vec                 VC, RVC, VCo, PHIC, PHICo, PTX;
  PetscScalar         f_array[2], fc_array[2], A[4];   // arrays for storing residual, solution, and Jacobian of equilibrium calc
  MPI_Comm            comm;
  
} AppCtx;

PetscErrorCode SetupParameters(AppCtx *);
PetscErrorCode ScaleParameters(AppCtx *);
PetscErrorCode CreateInitialCondition(AppCtx *);
PetscErrorCode CreateDataStructures(AppCtx *);
PetscErrorCode TimeSteppingSolver(AppCtx *);
PetscReal AdaptiveTimestep(AppCtx *);
PetscErrorCode DMDAGetGhostedArray(DM, Vec , Vec *, void *);
PetscErrorCode DMDARestoreGhostedArray(DM, Vec, Vec *, void *);
PetscErrorCode DestroyDataStructures(AppCtx *);
PetscErrorCode DMDAGetGridInfo(DM, int *, int *, int *, int *, 
			       int *, int *, int *, int *, int *, 
			       int *);
PetscErrorCode FormVCResidual(SNES, Vec, Vec, void *);
PetscErrorCode equilibrium_calc(AppCtx *, PetscReal, PetscReal, PetscReal, PetscReal, PetscReal *, PetscReal *, PetscReal *, PetscReal *, PetscReal *, PetscReal *, PetscInt, PetscBool *);
PetscErrorCode DoOutput(AppCtx *);

PetscErrorCode FormResidual(PetscScalar *, PetscScalar *, ThermoParameters *);
PetscErrorCode FormJacobian(PetscScalar *, PetscScalar *, ThermoParameters *);
PetscErrorCode SolveJacobian(PetscScalar *, PetscScalar *, PetscScalar *);
PetscErrorCode NewtonSolver(PetscScalar *, PetscScalar *, PetscScalar *, ThermoParameters *, PetscReal, PetscReal, PetscReal, PetscInt);
PetscErrorCode FormResidual_Sol(PetscScalar *, PetscScalar *, ThermoParameters *);
PetscErrorCode FormJacobian_Sol(PetscScalar *, PetscScalar *, ThermoParameters *);
PetscErrorCode NewtonSolver_Sol(PetscScalar *, PetscScalar *, PetscScalar *, ThermoParameters *, PetscReal, PetscReal, PetscReal, PetscInt);
PetscErrorCode ImportThermoData(ThermoData *); 
PetscErrorCode ClearThermoData(ThermoData *); 


/*---------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
/*---------------------------------------------------------------------*/
{
  AppCtx            user;
  Parameter         *p;
  PetscErrorCode    ierr;
  PetscReal         fnorm;
  
  PetscInitialize(&argc, &argv, (char *)0, NULL);
  user.comm = PETSC_COMM_WORLD;

  ierr  = PetscOptionsInsertString(NULL, "-snes_monitor");     CHKERRQ(ierr);

  ierr  =  SetupParameters(&user);           CHKERRQ(ierr);
  ierr  =  CreateDataStructures(&user);      CHKERRQ(ierr);
  ierr  =  CreateInitialCondition(&user);    CHKERRQ(ierr);

  ierr  =  TimeSteppingSolver(&user);        CHKERRQ(ierr);
  
//  ierr  =  DoOutput(&user);                   CHKERRQ(ierr);

  //  PetscPrintf(PETSC_COMM_SELF, "\n\nInitialization finished.\n\n");

    /* clean up and finalize PETSc */
  ierr  =  DestroyDataStructures(&user);   CHKERRQ(ierr);
  ierr  =  PetscFinalize();
  return 0;
  
}


/*---------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "SetupParameters"
PetscErrorCode SetupParameters(AppCtx *user)
/*---------------------------------------------------------------------*/
{
  PetscBag          bag;
  PetscBool         found;
  PetscReal         vs, theta;
  Parameter         *p;
  PetscErrorCode    ierr;
  PetscFunctionBegin;

  ierr  =  PetscBagCreate(PETSC_COMM_WORLD, sizeof(Parameter), &user->bag);   CHKERRQ(ierr);
  ierr  =  PetscBagGetData(user->bag, (void **)&user->par);                   CHKERRQ(ierr);
  bag   =  user->bag;
  p     =  user->par;
  ierr  =  PetscBagSetName(bag, "par", "*****  parameters for simulation run  *****"); CHKERRQ(ierr);

  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-vs", &vs, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a slab speed(m/year) via option -vs");

  found = PETSC_FALSE; ierr = PetscOptionsGetReal(NULL, NULL, "-theta", &theta, &found); CHKERRQ(ierr);
  if(!found) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER, "Must specify a fluid flow angle (in degree) relative to slab via option -theta");  

  p->v_s    =  vs/(YEARTOSEC);
  p->angle  =  theta*PETSC_PI/(180.0);
  p->ucos   =  cos(p->angle);
  p->wsin   =  sin(p->angle);
  
  REG_REAL(bag,   &p->v_s,      p->v_s,             "v_s",       "solid phase velocity, m/s");
  REG_REAL(bag,   &p->angle,    p->angle,           "angle",     "angle (radian) of fluid phase flow relative to slab");
  REG_REAL(bag,   &p->rhof,     990,                "rhof",      "constant fluid phase density");
  REG_REAL(bag,   &p->rhos,     3000,               "rhos",      "constant solid phase density");

  REG_INTG(bag,   &p->ndim,     2,                  "ndim",      "dimension of the model");
  REG_INTG(bag,   &p->nx,       354,                "nx",        "number of finite volumes in x direction, including buffering ones");
  REG_INTG(bag,   &p->nz,       14,                 "nz",        "number of finite volumes in z direction, including buffering ones");
  REG_REAL(bag,   &p->depth,    8,                  "depth",     "model depth, km");

    /* timing parameters */
  REG_INTG(bag,   &p->nt,       1e4,                "nt",        "[TIMING] Max. number of time steps");
  REG_REAL(bag,   &p->tmax,     6e6,                "tmax",      "[TIMING] Max. simulation time, yr");
  REG_REAL(bag,   &p->dt,       1e2,                "dt",        "[TIMING] initial time step, yr");
  REG_REAL(bag,   &p->t,        0,                  "t",         "[TIMING] <DO NOT SET> simulation time, yr");
  
  p->h      =  p->depth/(p->nz - 4);
  p->width  =  p->h * (p->nx - 4);
  
  REG_REAL(bag,   &p->width,    p->width,           "width",     "model width, km");
  REG_REAL(bag,   &p->h,        p->h,               "h",         "grid step, km");

  REG_REAL(bag,   &p->ucos,     p->ucos,            "ucos",      "x component of the directional vector of fluid velocity");
  REG_REAL(bag,   &p->wsin,     p->wsin,            "wsin",      "z component of the directional vector of fluid velocity");
  REG_REAL(bag,   &p->phi0,     1e-4,               "phi0",      "minimum finite porosity in the model");

  p->flim   =  p->rhof*p->phi0/(p->rhof*p->phi0 + p->rhos*(1 - p->phi0));  
  REG_REAL(bag,   &p->flim,     p->flim,            "flim",      "minimum weight fraction of liquid phase consistent with phi0");  

  REG_REAL(bag,   &p->T1,       800.0,              "T1",        "basal T of slab lithosphere from McKenzie (1969), C");
  REG_REAL(bag,   &p->l,        50e3,               "l",         "lithosphere thickness used for scaling from McKenzie (1969), m");
  REG_REAL(bag,   &p->Re,       31.25,              "Re",        "Reynolds number used for slab T calc, from McKenzie (1969)");
  REG_REAL(bag,   &p->alpha,    45.0,               "alpha",     "slab dipping angle used for P/T calc");

  p->rhoscale =  p->rhos;
  p->lscale   =  p->depth*1000.0;
  p->vscale   =  p->v_s;
  p->tscale   =  p->lscale/p->vscale;
  REG_REAL(bag,   &p->rhoscale,   p->rhoscale,       "rhoscale",    "density scale used for nondimensionalization");  
  REG_REAL(bag,   &p->lscale,     p->lscale,         "lscale",      "length scale used for nondimensionalization");
  REG_REAL(bag,   &p->vscale,     p->vscale,         "vscale",      "velocity scale used for nondimensionalization");
  REG_REAL(bag,   &p->tscale,     p->tscale,         "tscale",      "time scale used for nondimensionalization");
  
  REG_INTG(bag,   &p->maxit,    100.0,              "maxit",     "max non-linear iterations");
  REG_REAL(bag,   &p->atol,     1e-6,               "atol",      "absolute tolerance for convergence of snes");
  REG_REAL(bag,   &p->rtol,     1e-12,              "rtol",      "relative tolerance for convergence of snes");
  
  REG_STRG(bag,   &p->filename, FNAME_LENGTH,       "steadystate",      "filename",  "Name of output file, set with -filename <output file name>");
  REG_INTG(bag,   &p->nop,      1,                  "nop",              "[I/O] do output frame after nop time steps");
  REG_INTG(bag,   &p->frame,    0,                  "frame",            "[I/O] <DO NOT SET> output frame numbering");
  
  ierr  =  PetscPrintf(user->comm, "------------------------------------\n");  CHKERRQ(ierr);
  ierr  =  PetscBagView(user->bag, PETSC_VIEWER_STDOUT_WORLD);                 CHKERRQ(ierr);
  ierr  =  PetscPrintf(user->comm, "------------------------------------\n");  CHKERRQ(ierr);

  ierr  =  ScaleParameters(user);                                              CHKERRQ(ierr);

  PetscFunctionReturn(0);
  
}

/*---------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "ScaleParameters"
PetscErrorCode ScaleParameters(AppCtx *user)
/*---------------------------------------------------------------------*/
{
  Parameter      *p = user->par;
  PetscFunctionBegin;

  /* domain parameters */
  p->depth   =  p->depth *1000 / p->lscale;   // [km->1]
  p->width   =  p->width *1000 / p->lscale;   // [km->1]  
  p->h       =  p->h     *1000 / p->lscale;   // [km->1]

  /* timing parameters */
  p->tmax    =  p->tmax*YEARTOSEC / p->tscale; // [yr->1]
  p->dt      =  p->dt  *YEARTOSEC / p->tscale; // [yr->1]

  p->v_s      =  p->v_s / p->vscale;           //  [m/s->1]
  p->rhof     =  p->rhof / p->rhoscale;        //  [deg C->1]
  p->rhos     =  p->rhos / p->rhoscale;        //  [deg C->1]

  PetscFunctionReturn(0);
}


/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "CreateInitialCondition"
PetscErrorCode CreateInitialCondition(AppCtx *user)
/* ------------------------------------------------------------------- */
{
  Parameter      *p = user->par;
  PetscErrorCode ierr;
  PetscInt       i, is, ie, j, js, je;
  PetscReal      x, z, h = p->h;
  VCField        **vc;
  PhiCField      **phic;
  PTXField       **ptx;

  PetscBool      validtype;
  PetscReal      fc_guess[2];
  PetscReal      frac, cf, cs, hf, hs;  // output results

  PetscFunctionBegin;

  /* get the solution array WITHOUT ghost points */
  ierr  =  DMDAVecGetArray(user->da_vc,user->VC,&vc);                  CHKERRQ(ierr);

  ierr  =  DMDAVecGetArray(user->da_phic,user->PHIC,&phic);            CHKERRQ(ierr);  

  /* get the solution array WITHOUT ghost points */
  ierr  =  DMDAVecGetArray(user->da_ptx,user->PTX,&ptx);               CHKERRQ(ierr);

  /* get dimensions of LOCAL piece of grid (in parallel, this is not full grid) */
  ierr  =  DMDAGetGridInfo(user->da_vc,&is,&js,0,&ie,&je,0,0,0,0,0);   CHKERRQ(ierr);

  fc_guess[0] = 2e-5;
  fc_guess[1] = 5e-1;

  /* apply initial condition at z=0 (bottom) */
  if (js==0) {
    js += 2;
    for (j=0;j<2;j++) {
      for (i=is;i<ie;i++) {
	x = PetscMin(PetscMax(-0.5*h,(i-1.5)*h), p->width + 0.5*h)*p->lscale;      // in m
	z = PetscMin(PetscMax(-0.5*h, p->depth - (j - 1.5)*h), p->depth + 0.5*h)*p->lscale;   // in m

//	ptx[j][i].P     =  1.5;    // in GPa
//	ptx[j][i].T     =  273 + 900 - z*1e-3*10;   // in K, and assumes 10K/km

//	ptx[j][i].P     =  0.8;    // in GPa
//	ptx[j][i].T     =  273 + 200 + x*1e-3*10;   // in K, and assumes 10K/km

	ptx[j][i].P     =  1.5 + x*sin(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0 + z*cos(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0;    // in GPa
	ptx[j][i].T     =  273 + 800*(1 - (2.0/PETSC_PI)*sin(PETSC_PI*(p->l - z - 10e3)/p->l)*PetscExpReal((p->Re - PetscSqrtReal(p->Re*p->Re + PETSC_PI*PETSC_PI))*x/p->l));   // in K

	ptx[j][i].X     =  3;                       // rocktype

        vc[j][i].c_blk  =  0.0284;
        vc[j][i].h_blk  =  0.0258;
	
	ierr            = equilibrium_calc(user, ptx[j][i].T, ptx[j][i].P, vc[j][i].h_blk, vc[j][i].c_blk, fc_guess, &frac, &cf, &cs, &hf, &hs, ptx[j][i].X, &validtype); CHKERRQ(ierr);

	vc[j][i].v_f    =  0;
	
	phic[j][i].phi  =  frac;
	phic[j][i].c_f  =  cf;
	phic[j][i].h_f  =  hf;
	phic[j][i].c_s  =  cs;
	phic[j][i].h_s  =  hs;
					   
      }
    }
  }

  /* apply initial condition at z=z_max (surface) */
  if (je==p->nz) {
    je -= 2;
    for (j=p->nz-2;j<p->nz;j++) {
      for (i=is;i<ie;i++) {
	x = PetscMin(PetscMax(-0.5*h,(i-1.5)*h), p->width + 0.5*h)*p->lscale;      // in m
	z = PetscMin(PetscMax(-0.5*h, p->depth - (j - 1.5)*h), p->depth + 0.5*h)*p->lscale;   // in m

//	ptx[j][i].P     =  1.5;    // in GPa
//	ptx[j][i].T     =  273 + 900 - z*1e-3*10;   // in K, and assumes 10K/km

//	ptx[j][i].P     =  0.8;    // in GPa
//	ptx[j][i].T     =  273 + 200 + x*1e-3*10;   // in K, and assumes 10K/km

	ptx[j][i].P     =  1.5 + x*sin(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0 + z*cos(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0;    // in GPa
	ptx[j][i].T     =  273 + 800*(1 - (2.0/PETSC_PI)*sin(PETSC_PI*(p->l - z - 10e3)/p->l)*PetscExpReal((p->Re - PetscSqrtReal(p->Re*p->Re + PETSC_PI*PETSC_PI))*x/p->l));   // in K

	ptx[j][i].X     =  3;                       // rocktype

        vc[j][i].c_blk  =  0.0284;
        vc[j][i].h_blk  =  0.0258;
	
	ierr            = equilibrium_calc(user, ptx[j][i].T, ptx[j][i].P, vc[j][i].h_blk, vc[j][i].c_blk, fc_guess, &frac, &cf, &cs, &hf, &hs, ptx[j][i].X, &validtype); CHKERRQ(ierr);

	vc[j][i].v_f    =  0;	
					  
	phic[j][i].phi  =  frac;
	phic[j][i].c_f  =  cf;
	phic[j][i].h_f  =  hf;
	phic[j][i].c_s  =  cs;
	phic[j][i].h_s  =  hs;
      }
    }
  }

  /* apply initial condition at x=0 (left) */
  if (is==0) {
    is += 2;
    for (j=js;j<je;j++) {
      for (i=0;i<2;i++) {
	x = PetscMin(PetscMax(-0.5*h,(i-1.5)*h), p->width + 0.5*h)*p->lscale;      // in m
	z = PetscMin(PetscMax(-0.5*h, p->depth - (j - 1.5)*h), p->depth + 0.5*h)*p->lscale;   // in m

//	ptx[j][i].P     =  1.5;    // in GPa
//	ptx[j][i].T     =  273 + 900 - z*1e-3*10;   // in K, and assumes 10K/km

//	ptx[j][i].P     =  0.8;    // in GPa
//	ptx[j][i].T     =  273 + 200 + x*1e-3*10;   // in K, and assumes 10K/km

	ptx[j][i].P     =  1.5 + x*sin(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0 + z*cos(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0;    // in GPa
	ptx[j][i].T     =  273 + 800*(1 - (2.0/PETSC_PI)*sin(PETSC_PI*(p->l - z - 10e3)/p->l)*PetscExpReal((p->Re - PetscSqrtReal(p->Re*p->Re + PETSC_PI*PETSC_PI))*x/p->l));   // in K

	ptx[j][i].X     =  3;                         // rocktype

        vc[j][i].c_blk  =  0.0284;
        vc[j][i].h_blk  =  0.0258;
	
	ierr            = equilibrium_calc(user, ptx[j][i].T, ptx[j][i].P, vc[j][i].h_blk, vc[j][i].c_blk, fc_guess, &frac, &cf, &cs, &hf, &hs, ptx[j][i].X, &validtype); CHKERRQ(ierr);
					   
	vc[j][i].v_f    =  0;
	
	phic[j][i].phi  =  frac;
	phic[j][i].c_f  =  cf;
	phic[j][i].h_f  =  hf;
	phic[j][i].c_s  =  cs;
	phic[j][i].h_s  =  hs;
      }
    }
  }

  /* apply initial condition at x=x_max (right) */
  if (ie==p->nx) {
    ie -= 2;
    for (j=js;j<je;j++) {
      for (i=p->nx-2;i<p->nx;i++) {
	x = PetscMin(PetscMax(-0.5*h,(i-1.5)*h), p->width + 0.5*h)*p->lscale;      // in m
	z = PetscMin(PetscMax(-0.5*h, p->depth - (j - 1.5)*h), p->depth + 0.5*h)*p->lscale;   // in m

//	ptx[j][i].P     =  1.5;    // in GPa
//	ptx[j][i].T     =  273 + 900 - z*1e-3*10;   // in K, and assumes 10K/km

//	ptx[j][i].P     =  0.8;    // in GPa
//	ptx[j][i].T     =  273 + 200 + x*1e-3*10;   // in K, and assumes 10K/km
	
	ptx[j][i].P     =  1.5 + x*sin(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0 + z*cos(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0;    // in GPa
	ptx[j][i].T     =  273 + 800*(1 - (2.0/PETSC_PI)*sin(PETSC_PI*(p->l - z - 10e3)/p->l)*PetscExpReal((p->Re - PetscSqrtReal(p->Re*p->Re + PETSC_PI*PETSC_PI))*x/p->l));   // in K

	ptx[j][i].X     =  3;                        // rocktype

        vc[j][i].c_blk  =  0.0284;
        vc[j][i].h_blk  =  0.0258;
	
	ierr            = equilibrium_calc(user, ptx[j][i].T, ptx[j][i].P, vc[j][i].h_blk, vc[j][i].c_blk, fc_guess, &frac, &cf, &cs, &hf, &hs, ptx[j][i].X, &validtype); CHKERRQ(ierr);
					  
	vc[j][i].v_f    =  0;
	
	phic[j][i].phi  =  frac;
	phic[j][i].c_f  =  cf;
	phic[j][i].h_f  =  hf;
	phic[j][i].c_s  =  cs;
	phic[j][i].h_s  =  hs;
      }
    }
  }

  /* apply initial condition to interior of domain */
  for (j=js;j<je;j++) {
    for (i=is;i<ie;i++) {
	x = PetscMin(PetscMax(-0.5*h,(i-1.5)*h), p->width + 0.5*h)*p->lscale;      // in m
	z = PetscMin(PetscMax(-0.5*h, p->depth - (j - 1.5)*h), p->depth + 0.5*h)*p->lscale;   // in m

//	ptx[j][i].P     =  1.5;    // in GPa
//	ptx[j][i].T     =  273 + 900 - z*1e-3*10;   // in K, and assumes 10K/km

//	ptx[j][i].P     =  0.8;    // in GPa
//	ptx[j][i].T     =  273 + 200 + x*1e-3*10;   // in K, and assumes 10K/km
	
	ptx[j][i].P     =  1.5 + x*sin(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0 + z*cos(p->alpha*PETSC_PI/(180.0))*0.3*1e-3/10.0;    // in GPa
	ptx[j][i].T     =  273 + 800*(1 - (2.0/PETSC_PI)*sin(PETSC_PI*(p->l - z - 10e3)/p->l)*PetscExpReal((p->Re - PetscSqrtReal(p->Re*p->Re + PETSC_PI*PETSC_PI))*x/p->l));   // in K

	ptx[j][i].X     =  3;                        // rocktype

        vc[j][i].c_blk  =  0.0284;
        vc[j][i].h_blk  =  0.0258;
	
	ierr            = equilibrium_calc(user, ptx[j][i].T, ptx[j][i].P, vc[j][i].h_blk, vc[j][i].c_blk, fc_guess, &frac, &cf, &cs, &hf, &hs, ptx[j][i].X, &validtype); CHKERRQ(ierr);
					   
	vc[j][i].v_f    =  0;
	
	phic[j][i].phi  =  frac;
	phic[j][i].c_f  =  cf;
	phic[j][i].h_f  =  hf;
	phic[j][i].c_s  =  cs;
	phic[j][i].h_s  =  hs;
    }
  }
  
  //ierr = PetscPrintf(user->comm,"%i, %i \n",i,j);  CHKERRQ(ierr); //print for debugging

  ierr  =  DMDAVecRestoreArray(user->da_vc,user->VC,&vc);              CHKERRQ(ierr);
  ierr  =  VecCopy(user->VC,user->VCo);                                CHKERRQ(ierr);
  ierr  =  DMDAVecRestoreArray(user->da_phic,user->PHIC,&phic);        CHKERRQ(ierr);
  ierr  =  VecCopy(user->PHIC,user->PHICo);                            CHKERRQ(ierr);
  ierr  =  DMDAVecRestoreArray(user->da_ptx,user->PTX,&ptx);           CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/*---------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "DestroyDataStructures"
PetscErrorCode DestroyDataStructures(AppCtx *user)
/*---------------------------------------------------------------------*/
{
  PetscErrorCode ierr;
  PetscFunctionBegin; 

  ierr  =  VecDestroy(&user->VC);                 CHKERRQ(ierr);
  ierr  =  VecDestroy(&user->RVC);                CHKERRQ(ierr);
  ierr  =  VecDestroy(&user->VCo);                CHKERRQ(ierr);
  ierr  =  VecDestroy(&user->PHIC);               CHKERRQ(ierr);  
  ierr  =  VecDestroy(&user->PHICo);              CHKERRQ(ierr);    
  ierr  =  VecDestroy(&user->PTX);                CHKERRQ(ierr);
  ierr  =  SNESDestroy(&user->snes_vc);           CHKERRQ(ierr);
  ierr  =  DMDestroy(&user->da_vc);               CHKERRQ(ierr);
  ierr  =  DMDestroy(&user->da_phic);             CHKERRQ(ierr);  
  ierr  =  DMDestroy(&user->da_ptx);              CHKERRQ(ierr);
  ierr  =  PetscBagDestroy(&user->bag);           CHKERRQ(ierr);
  ierr  =  PetscFree(user->tp);                   CHKERRQ(ierr);
  ierr  =  ClearThermoData(user->ctx);            CHKERRQ(ierr);  
  ierr  =  PetscFree(user->ctx);                  CHKERRQ(ierr);  

  PetscFunctionReturn(0);
}

/*---------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "CreateDataStructures"
PetscErrorCode CreateDataStructures(AppCtx *user)
/*---------------------------------------------------------------------*/
{
  PetscErrorCode     ierr;
  PetscInt           dofs;
  Parameter          *p = user->par;
  PetscFunctionBegin;

  ierr  =  PetscMalloc1(1, &(user->ctx));   CHKERRQ(ierr);
  ierr  =  ImportThermoData(user->ctx);     CHKERRQ(ierr);  

  dofs  =  (PetscInt)(sizeof(VCField)/sizeof(PetscReal));
  ierr  =  DMDACreate2d(user->comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_STAR, p->nx, p->nz, PETSC_DECIDE, PETSC_DECIDE, dofs, 2, NULL, NULL, &user->da_vc);   CHKERRQ(ierr);

  ierr  =  DMDASetFieldName(user->da_vc, 0, "v_f");             CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_vc, 1, "c_blk");           CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_vc, 2, "h_blk");           CHKERRQ(ierr);
  ierr  =  DMCreateGlobalVector(user->da_vc, &user->VC);        CHKERRQ(ierr);

  ierr  =  PetscObjectSetName((PetscObject)user->VC, "solution vector");               CHKERRQ(ierr);
  ierr  =  VecDuplicate(user->VC, &user->RVC);                                         CHKERRQ(ierr);
  ierr  =  PetscObjectSetName((PetscObject)user->RVC, "residuals of solution");        CHKERRQ(ierr);
  ierr  =  VecDuplicate(user->VC, &user->VCo);                                         CHKERRQ(ierr);
  ierr  =  PetscObjectSetName((PetscObject)user->VCo, "previous timestep solution");   CHKERRQ(ierr);  

  ierr  =  SNESCreate(user->comm, &user->snes_vc);                              CHKERRQ(ierr);
  ierr  =  SNESSetDM(user->snes_vc, user->da_vc);                               CHKERRQ(ierr);
  ierr  =  SNESSetFunction(user->snes_vc, user->RVC, FormVCResidual, user);     CHKERRQ(ierr);
  ierr  =  SNESSetFromOptions(user->snes_vc);                                   CHKERRQ(ierr);

  dofs  =  (PetscInt)(sizeof(PhiCField)/sizeof(PetscReal));
  ierr  =  DMDACreate2d(user->comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_STAR, p->nx, p->nz, PETSC_DECIDE, PETSC_DECIDE, dofs, 2, NULL, NULL, &user->da_phic);   CHKERRQ(ierr);
  
  ierr  =  DMDASetFieldName(user->da_phic, 0, "phi");             CHKERRQ(ierr);  
  ierr  =  DMDASetFieldName(user->da_phic, 1, "c_f");             CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_phic, 2, "c_s");             CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_phic, 3, "h_f");             CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_phic, 4, "h_s");             CHKERRQ(ierr);
  ierr  =  DMCreateGlobalVector(user->da_phic, &user->PHIC);    CHKERRQ(ierr);

  ierr  =  PetscObjectSetName((PetscObject)user->PHIC, "solution of thermodynamic calc");       CHKERRQ(ierr);
  ierr  =  VecDuplicate(user->PHIC, &user->PHICo);                                              CHKERRQ(ierr);
  ierr  =  PetscObjectSetName((PetscObject)user->PHICo, "previous timestep thermo solution");   CHKERRQ(ierr);    

  dofs  =  (PetscInt)(sizeof(PTXField)/sizeof(PetscReal));
  ierr  =  DMDACreate2d(user->comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_STAR, p->nx, p->nz, PETSC_DECIDE, PETSC_DECIDE, dofs, 2, NULL, NULL, &user->da_ptx);   CHKERRQ(ierr);

  ierr  =  DMDASetFieldName(user->da_ptx, 0, "P");             CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_ptx, 1, "T");             CHKERRQ(ierr);
  ierr  =  DMDASetFieldName(user->da_ptx, 2, "X");             CHKERRQ(ierr);  
  ierr  =  DMCreateGlobalVector(user->da_ptx, &user->PTX);     CHKERRQ(ierr);
  ierr  =  PetscObjectSetName((PetscObject)user->PTX, "pressure temperature field");   CHKERRQ(ierr);

  ierr  =  PetscMalloc1(1, &user->tp);                                 CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DMDAGetGhostedArray"
/* Gets array from a vector associated with a DMDA, with ghost points */
PetscErrorCode DMDAGetGhostedArray(DM da, Vec globvec, Vec *locvec, void *arr)
/* ------------------------------------------------------------------- */
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = DMGetLocalVector(da,locvec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,globvec,INSERT_VALUES,*locvec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,globvec,INSERT_VALUES,*locvec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,*locvec,arr); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DMDARestoreGhostedArray"
/* Restores array from a vector associated with a DMDA, with ghost points */
PetscErrorCode DMDARestoreGhostedArray(DM da, Vec globvec, Vec *locvec, void *arr)
/* ------------------------------------------------------------------- */
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  ierr = DMDAVecRestoreArray(da,*locvec,arr); CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,locvec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DMDAGetGridInfo"
/* Get useful information from the DMDA */
PetscErrorCode DMDAGetGridInfo(DM da, int *is, int *js, int *ks, int *ie, 
			       int *je, int *ke, int *ni, int *nj, int *nk, 
			       int *dim)
/* ------------------------------------------------------------------- */
{
  PetscErrorCode ierr;
  PetscInt       im, jm, km;
  PetscFunctionBegin;
  ierr = DMDAGetCorners(da,is,js,ks,&im,&jm,&km); CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,dim,ni,nj,nk,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  if (ie) *ie = *is + im; 
  if (je) *je = *js + jm; 
  if (ke) *ke = *ks + km;
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormVCResidual"
PetscErrorCode FormVCResidual(SNES snes, Vec X, Vec R, void *ptr)
/* ------------------------------------------------------------------- */
{
  AppCtx         *user = (AppCtx*)ptr;
  Parameter      *p = user->par;
  PetscReal      h  = p->h, v_b;
  PetscErrorCode ierr;
  PetscInt       i, is, ie, j, js, je;
  VCField        **rvc, **vc, **vco;
  PhiCField      **phic, **phico;
  PTXField       **ptx;
  Vec            lv_vc, lv_phic, lv_ptx;

  PetscBool      validtype;
  PetscReal      fc_guess[2];
  PetscReal      frac, cf, cs, hf, hs, flim;  // output results
  
  PetscFunctionBegin;

  ierr   =  DMDAVecGetArray(user->da_vc, R, &rvc);                                CHKERRQ(ierr);

  ierr   =  DMDAGetGhostedArray(user->da_vc, X, &lv_vc, &vc);                     CHKERRQ(ierr);

  ierr   =  DMDAVecGetArray(user->da_vc, user->VCo, &vco);                        CHKERRQ(ierr);  

  ierr   =  DMDAVecGetArray(user->da_phic, user->PHICo, &phico);                  CHKERRQ(ierr);    

  ierr   =  DMDAGetGhostedArray(user->da_ptx, user->PTX, &lv_ptx, &ptx);          CHKERRQ(ierr);

  ierr   =  DMDAGetGridInfo(user->da_vc, &is, &js, 0, &ie, &je, 0, 0, 0, 0, 0);   CHKERRQ(ierr);

  /*  Update thermodynamic properties */
  ierr   =  DMDAVecGetArray(user->da_phic, user->PHIC, &phic);                    CHKERRQ(ierr);

  flim   =  p->flim;
  
  for (j=js+2;j<je;j++) {
    for (i=is+2;i<ie;i++) {

      fc_guess[0]     = p->rhof*phic[j][i].phi/(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi));
      fc_guess[1]     = phic[j][i].h_f;

      ierr            = equilibrium_calc(user, ptx[j][i].T, ptx[j][i].P, vc[j][i].h_blk, vc[j][i].c_blk, fc_guess, &frac, &cf, &cs, &hf, &hs, ptx[j][i].X, &validtype); CHKERRQ(ierr);

      phic[j][i].phi  = frac;
      phic[j][i].c_f  = cf;
      phic[j][i].h_f  = hf;
      phic[j][i].c_s  = cs;
      phic[j][i].h_s  = hs;

    }
  }

  if (js==0) {
     for (j=0; j<2; j++) {
       for (i=is; i<ie; i++) {

         phic[j][i].phi  = p->phi0;
         phic[j][i].c_f  = 0.0;
         phic[j][i].h_f  = 0.0;
 //        phic[j][i].phi  = phic[2][i].phi;
 //        phic[j][i].c_f  = phic[2][i].c_f;
 //        phic[j][i].h_f  = phic[2][i].h_f;
         phic[j][i].c_s  = phic[2][i].c_s;
         phic[j][i].h_s  = phic[2][i].h_s;
       }
     }
   }

  if (is==0) {
     for (i=0; i<2; i++) {
       for (j=js; j<je; j++) {

        phic[j][i].phi  = phico[j][i].phi;
        phic[j][i].c_f  = phico[j][i].c_f;
        phic[j][i].h_f  = phico[j][i].h_f;
        phic[j][i].c_s  = phico[j][i].c_s;
        phic[j][i].h_s  = phico[j][i].h_s;
       }
     }
   }

  ierr = DMDAVecRestoreArray(user->da_phic,user->PHIC,&phic);                     CHKERRQ(ierr);
  /*  Update thermodynamic properties */

  //  PetscPrintf(PETSC_COMM_SELF, "\nThermodynamic loop finished.\n");  
  
  ierr = DMDAGetGhostedArray(user->da_phic, user->PHIC, &lv_phic, &phic);         CHKERRQ(ierr);

  if (PetscAbsReal(p->angle - 90.0*PETSC_PI/(180.0)) <= 1e-6) {

//    v_b = 0.0;
    v_b = p->v_s;
    
  } else if (p->ucos > 0) {

    v_b = p->v_s/p->ucos;

  } else {

    v_b = - p->v_s/p->ucos;

  }

  /* z = 0 bottom boundary */
  if (js==0) {
    js += 2;
    for (j=0;j<2;j++) {
      for (i=is;i<ie;i++) {

	rvc[j][i].v_f   = vc[j][i].v_f - v_b;
//      rvc[j][i].v_f   = vc[j][i].v_f - vc[j+1][i].v_f;
	
	rvc[j][i].c_blk = vc[j][i].c_blk - vc[j+1][i].c_blk;
	rvc[j][i].h_blk = vc[j][i].h_blk - vc[j+1][i].h_blk;
	/* rvc[j][i].c_blk = vc[j][i].c_blk - vco[j][i].c_blk;
	rvc[j][i].h_blk = vc[j][i].h_blk - vco[j][i].h_blk; */
      }
    }
  }

  /* top boundary */
  if (je==p->nz) {
    je -= 2;
    for (j=p->nz-2;j<p->nz;j++) {
      for (i=is;i<ie;i++) {
	
	rvc[j][i].v_f   = vc[j][i].v_f - vc[j-1][i].v_f;
	
        rvc[j][i].c_blk = vc[j][i].c_blk - vc[j-1][i].c_blk;
  	rvc[j][i].h_blk = vc[j][i].h_blk - vc[j-1][i].h_blk;
  	/* rvc[j][i].c_blk = vc[j][i].c_blk - vco[j][i].c_blk; */
  	/* rvc[j][i].h_blk = vc[j][i].h_blk - vco[j][i].h_blk; */
      }
    }
  }

  /* left boundary */
  if (is==0) {
    is += 2;
    for (j=js;j<je;j++) {
      for (i=0;i<2;i++) {
	
	if (p->ucos > 0) {
	  
	  rvc[j][i].v_f   = vc[j][i].v_f - v_b;

  	  rvc[j][i].c_blk = vc[j][i].c_blk - vco[j][i].c_blk;
  	  rvc[j][i].h_blk = vc[j][i].h_blk - vco[j][i].h_blk;

 //   	  rvc[j][i].c_blk = vc[j][i].c_blk - vc[j][i+1].c_blk;
 // 	  rvc[j][i].h_blk = vc[j][i].h_blk - vc[j][i+1].h_blk;

	} else {

	  rvc[j][i].v_f   = vc[j][i].v_f - vc[j][i+1].v_f;

    	  rvc[j][i].c_blk = vc[j][i].c_blk - vc[j][i+1].c_blk;
  	  rvc[j][i].h_blk = vc[j][i].h_blk - vc[j][i+1].h_blk;
	  
	}		
      }
    }
  }

  /* right boundary */
  if (ie==p->nx) {
    ie -= 2;
    for (j=js;j<je;j++) {
      for (i=p->nx-2;i<p->nx;i++) {

	if (p->ucos > 0) {
	  
	  rvc[j][i].v_f   = vc[j][i].v_f - vc[j][i-1].v_f;

  	  rvc[j][i].c_blk = vc[j][i].c_blk - vc[j][i-1].c_blk;
  	  rvc[j][i].h_blk = vc[j][i].h_blk - vc[j][i-1].h_blk;
	  
	} else {

	  rvc[j][i].v_f   = vc[j][i].v_f - v_b;

  	  rvc[j][i].c_blk = vc[j][i].c_blk - vc[j][i-1].c_blk;
  	  rvc[j][i].h_blk = vc[j][i].h_blk - vc[j][i-1].h_blk;
	  /* rvc[j][i].c_blk = vc[j][i].c_blk - vco[j][i].c_blk; */
  	  /* rvc[j][i].h_blk = vc[j][i].h_blk - vco[j][i].h_blk; */	
	  
	}
      }
    }
  }

  /* interior of domain */
  for (j=js;j<je;j++) {
    for (i=is;i<ie;i++) {

      if (p->ucos > 0) {

	rvc[j][i].v_f   = p->rhof*(p->ucos*(vc[j][i].v_f*phic[j][i].phi - vc[j][i-1].v_f*phic[j][i-1].phi)/h \
				   + p->wsin*(vc[j][i].v_f*phic[j][i].phi - vc[j-1][i].v_f*phic[j-1][i].phi)/h) \
	                  - p->rhos*p->v_s*(phic[j][i].phi - phic[j][i-1].phi)/h \
			  - (p->rhos - p->rhof)*(phic[j][i].phi - phico[j][i].phi)/p->dt;

//        if (vc[j][i].v_f >= 0) {

  	  rvc[j][i].c_blk = (vc[j][i].c_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].c_blk*(p->rhof*phico[j][i].phi + p->rhos*(1 - phico[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f - vc[j][i-1].v_f*phic[j][i-1].phi*phic[j][i-1].c_f)/h \
		       + p->wsin*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f - vc[j-1][i].v_f*phic[j-1][i].phi*phic[j-1][i].c_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].c_s - (1-phic[j][i-1].phi)*phic[j][i-1].c_s)/h;

  	  rvc[j][i].h_blk = (vc[j][i].h_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].h_blk*(p->rhof*phico[j][i].phi + p->rhos*(1 - phico[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f - vc[j][i-1].v_f*phic[j][i-1].phi*phic[j][i-1].h_f)/h \
		       + p->wsin*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f - vc[j-1][i].v_f*phic[j-1][i].phi*phic[j-1][i].h_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].h_s - (1-phic[j][i-1].phi)*phic[j][i-1].h_s)/h;

//	} else {
	
//  	  rvc[j][i].c_blk = (vc[j][i].c_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].c_blk*(p->rhof*phico[j][i].phi + p->rhos*(1 - phico[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i+1].v_f*phic[j][i+1].phi*phic[j][i+1].c_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f)/h \
		       + p->wsin*(vc[j+1][i].v_f*phic[j+1][i].phi*phic[j+1][i].c_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].c_s - (1-phic[j][i-1].phi)*phic[j][i-1].c_s)/h;

//  	  rvc[j][i].h_blk = (vc[j][i].h_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].h_blk*(p->rhof*phico[j][i].phi + p->rhos*(1 - phico[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i+1].v_f*phic[j][i+1].phi*phic[j][i+1].h_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f)/h \
		       + p->wsin*(vc[j+1][i].v_f*phic[j+1][i].phi*phic[j+1][i].h_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].h_s - (1-phic[j][i-1].phi)*phic[j][i-1].h_s)/h;

//	}

      } else {

	rvc[j][i].v_f   = p->rhof*(p->ucos*(vc[j][i+1].v_f*phic[j][i+1].phi - vc[j][i].v_f*phic[j][i].phi)/h \
				   + p->wsin*(vc[j][i].v_f*phic[j][i].phi - vc[j-1][i].v_f*phic[j-1][i].phi)/h) \
	                  - p->rhos*p->v_s*(phic[j][i].phi - phic[j][i-1].phi)/h \
			  - (p->rhos - p->rhof)*(phic[j][i].phi - phico[j][i].phi)/p->dt;

//        if (vc[j][i].v_f >= 0) {

	  rvc[j][i].c_blk = (vc[j][i].c_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].c_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i+1].v_f*phic[j][i+1].phi*phic[j][i+1].c_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f)/h \
		       + p->wsin*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f - vc[j-1][i].v_f*phic[j-1][i].phi*phic[j-1][i].c_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].c_s - (1-phic[j][i-1].phi)*phic[j][i-1].c_s)/h;

	  rvc[j][i].h_blk = (vc[j][i].h_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].h_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i+1].v_f*phic[j][i+1].phi*phic[j][i+1].h_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f)/h \
		       + p->wsin*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f - vc[j-1][i].v_f*phic[j-1][i].phi*phic[j-1][i].h_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].h_s - (1-phic[j][i-1].phi)*phic[j][i-1].h_s)/h;

//	} else {
	
//	  rvc[j][i].c_blk = (vc[j][i].c_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].c_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f - vc[j][i-1].v_f*phic[j][i-1].phi*phic[j][i-1].c_f)/h \
		       + p->wsin*(vc[j+1][i].v_f*phic[j+1][i].phi*phic[j+1][i].c_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].c_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].c_s - (1-phic[j][i-1].phi)*phic[j][i-1].c_s)/h;

//	  rvc[j][i].h_blk = (vc[j][i].h_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)) - vco[j][i].h_blk*(p->rhof*phic[j][i].phi + p->rhos*(1 - phic[j][i].phi)))/p->dt \
	    + p->rhof*(p->ucos*(vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f - vc[j][i-1].v_f*phic[j][i-1].phi*phic[j][i-1].h_f)/h \
		       + p->wsin*(vc[j+1][i].v_f*phic[j+1][i].phi*phic[j+1][i].h_f - vc[j][i].v_f*phic[j][i].phi*phic[j][i].h_f)/h) \
	    + p->rhos*p->v_s*((1-phic[j][i].phi)*phic[j][i].h_s - (1-phic[j][i-1].phi)*phic[j][i-1].h_s)/h;

//	}
	
      }
    }
  }

  /* clean up */
  ierr = DMDARestoreGhostedArray(user->da_vc,X,&lv_vc,&vc);                  CHKERRQ(ierr);
  ierr = DMDARestoreGhostedArray(user->da_phic,user->PHIC,&lv_phic,&phic);   CHKERRQ(ierr);
  ierr = DMDARestoreGhostedArray(user->da_ptx,user->PTX,&lv_ptx,&ptx);       CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(user->da_vc,R,&rvc);                            CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(user->da_vc,user->VCo,&vco);                    CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(user->da_phic,user->PHICo,&phico);              CHKERRQ(ierr);  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "equilibrium_calc"
PetscErrorCode equilibrium_calc(AppCtx *user, PetscReal T, PetscReal P, PetscReal w_h2o, PetscReal w_co2, PetscReal *fc_guess, PetscReal *frac, PetscReal *cf, PetscReal *cs, PetscReal *hf, PetscReal *hs, PetscInt rocktype, PetscBool *validtype)
/* ------------------------------------------------------------------- */
{
  // Thermodynamic parameters
  PetscReal         deltaTco2;
  ThermoParameters  *p;
  PetscReal         *cmaxh, *logLrh, *Tmh, *logLrc, *Tmc, *deltaTc;
  PetscReal         cmaxc;
  PetscReal         rhof, rhos;
  PetscReal         flim;

  PetscErrorCode    ierr;

  ThermoData        *ctx;
  PetscScalar       *f_array;
  PetscScalar       *fc_array;
  PetscScalar       *A;

  PetscFunctionBegin;


  rhof        = user->par->rhof;
  rhos        = user->par->rhos;
  flim        = user->par->flim;
  f_array     = user->f_array;
  fc_array    = user->fc_array;
  A           = user->A;

  p           = user->tp;
  p->c_blkh2o = w_h2o;
  p->c_blkco2 = w_co2;
  p->T        = T;

  ctx         = user->ctx;
 
  switch (rocktype) {
    case 1: {       //sed
     
      cmaxh    = ctx->sed->cmaxh;
      logLrh   = ctx->sed->logLrh;
      Tmh      = ctx->sed->Tmh;      
      cmaxc    = *(ctx->sed->cmaxc);
      logLrc   = ctx->sed->logLrc;
      Tmc      = ctx->sed->Tmc;
      deltaTc  = ctx->sed->deltaTc;   
      p->Wmh  = *(ctx->sed->Wmh);
      p->Wmc  = *(ctx->sed->Wmc);  
      
      p->cmaxh2o  = PetscExpReal(cmaxh[0]*(log(P)*log(P)) + cmaxh[1]*log(P) + cmaxh[2]);
      p->Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 5) + logLrh[1]/PetscPowReal(P, 4)  + logLrh[2]/PetscPowReal(P, 3) + logLrh[3]/(P*P) + logLrh[4]/P + logLrh[5]);
      p->Tmh2o    = Tmh[0]*PetscPowReal(P, 5) + Tmh[1]*PetscPowReal(P, 4)  + Tmh[2]*PetscPowReal(P, 3) + Tmh[3]*(P*P) + Tmh[4]*P + Tmh[5];
    
      p->cmaxco2  = cmaxc;
      p->Lrco2    = PetscExpReal(logLrc[0]/PetscPowReal(P, 5)  + logLrc[1]/PetscPowReal(P, 4) + logLrc[2]/PetscPowReal(P, 3) + logLrc[3]/(P*P) + logLrc[4]/P + logLrc[5]);
      p->Tmco2    = Tmc[0]*(P*P) + Tmc[1]*P + Tmc[2];
    
      deltaTco2   = deltaTc[0]*(P*P) + deltaTc[1]*P + deltaTc[2];
      p->Tmco2    = p->Tmco2 - deltaTco2;
    } break;

    case 2: {    //morb

      cmaxh    = ctx->morb->cmaxh;
      logLrh   = ctx->morb->logLrh;
      Tmh      = ctx->morb->Tmh;      
      cmaxc    = *(ctx->morb->cmaxc);
      logLrc   = ctx->morb->logLrc;
      Tmc      = ctx->morb->Tmc;
      deltaTc  = ctx->morb->deltaTc;   
      p->Wmh  = *(ctx->morb->Wmh);
      p->Wmc  = *(ctx->morb->Wmc);      
     
      p->cmaxh2o  = PetscExpReal(cmaxh[0]*PetscPowReal(P, 3) + cmaxh[1]*PetscPowReal(P, 2) + cmaxh[2]*P + cmaxh[3]);
      p->Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 3) + logLrh[1]/PetscPowReal(P, 2)  + logLrh[2]/P + logLrh[3]);
      p->Tmh2o    = Tmh[0]*PetscPowReal(P, 5) + Tmh[1]*PetscPowReal(P, 4) + Tmh[2]*PetscPowReal(P, 3) + Tmh[3]*(P*P) + Tmh[4]*P + Tmh[5];
  
      p->cmaxco2  = cmaxc;
      p->Lrco2    = PetscExpReal(logLrc[0]/PetscPowReal(P, 5)  + logLrc[1]/PetscPowReal(P, 4) + logLrc[2]/PetscPowReal(P, 3) + logLrc[3]/(P*P) + logLrc[4]/P + logLrc[5]);
      p->Tmco2    = Tmc[0]*(P*P) + Tmc[1]*P + Tmc[2];
  
      deltaTco2   = deltaTc[0]*(P*P) + deltaTc[1]*P + deltaTc[2];
      p->Tmco2    = p->Tmco2 - deltaTco2;
    } break; 

    case 3: {  //gabbro
	
      cmaxh    = ctx->gabbro->cmaxh;
      logLrh   = ctx->gabbro->logLrh;
      Tmh      = ctx->gabbro->Tmh;      
      cmaxc    = *(ctx->gabbro->cmaxc);
      logLrc   = ctx->gabbro->logLrc;
      Tmc      = ctx->gabbro->Tmc;
      deltaTc  = ctx->gabbro->deltaTc;   
      p->Wmh  = *(ctx->gabbro->Wmh);
      p->Wmc  = *(ctx->gabbro->Wmc);      
      
      p->cmaxh2o  = PetscExpReal(cmaxh[0]*(log(P)*log(P)) + cmaxh[1]*log(P) + cmaxh[2]);
      p->Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 4) + logLrh[1]/PetscPowReal(P, 3)  + logLrh[2]/(P*P) + logLrh[3]/P + logLrh[4]);
      p->Tmh2o    = Tmh[0]*PetscPowReal(P, 4) + Tmh[1]*PetscPowReal(P, 3)  + Tmh[2]*(P*P) + Tmh[3]*P + Tmh[4];
  
      p->cmaxco2  = cmaxc;
      p->Lrco2    = PetscExpReal(logLrc[0]/PetscPowReal(P, 3)  + logLrc[1]/(P*P) + logLrc[2]/P + logLrc[3]);
      p->Tmco2    = Tmc[0]*(P*P) + Tmc[1]*P + Tmc[2];
  
      deltaTco2   = deltaTc[0]*(P*P) + deltaTc[1]*P + deltaTc[2];
      p->Tmco2    = p->Tmco2 - deltaTco2;
    } break; 

    case 4: {   // primitive upper mantle
      
      PetscReal T1, T2, T3;

      cmaxh    = ctx->pum->cmaxh;
      logLrh   = ctx->pum->logLrh;
      Tmh      = ctx->pum->Tmh;
      p->Wmh    = *(ctx->pum->Wmh);
      p->Wmc    = *(ctx->pum->Wmc);     

      T1       = P*1.072054506210788e+02 + 8.966542633680793e+02; 
      T2       = P*1.533210093204700e+02 + 8.433396814457046e+02; 
      T3       = P*2.491790919291565e+02 + 8.375304594099354e+02;
  
      if (T1 < T2) {
  
        if (p->T < T1) {
  
          p->Kco2 = 32.884202059134026;
  
        } else if ((p->T >= T1)&&(p->T < T2)) {
          
          p->Kco2 = 28.961173923316494;
        
        } else if ((p->T >= T2)&&(p->T < T3)) {
        
          p->Kco2 = 14.752123772328618;
  
        } else {
  
          p->Kco2 = 0.323043468637191;      
  
        }
      } else {
      
        if (p->T < T2) {
        
          p->Kco2 = 32.884202059134026; 
  
        } else if ((p->T >= T2)&&(p->T < T3)) {
  
          p->Kco2 = 14.752123772328618;
        
        } else {
        
          p->Kco2 = 0.323043468637191;
  
        }
      
      }
  
      p->Kco2 = p->Kco2/100.0;
  
      p->cmaxh2o  = PetscExpReal(cmaxh[0]*P + cmaxh[1]);
      p->Lrh2o    = PetscExpReal(logLrh[0]/PetscPowReal(P, 8) + logLrh[1]/PetscPowReal(P, 7)  + logLrh[2]/PetscPowReal(P, 6) + logLrh[3]/PetscPowReal(P, 5) + logLrh[4]/PetscPowReal(P, 4) + logLrh[5]/PetscPowReal(P, 3) + logLrh[6]/PetscPowReal(P, 2) + logLrh[7]/P + logLrh[8]);
      p->Tmh2o    = Tmh[0]*P*P  + Tmh[1]*P + Tmh[2];

    } break;

  }

  p->Kh2o   = p->cmaxh2o*PetscExpReal(p->Lrh2o*(1.0/p->T - 1.0/p->Tmh2o))/100.0;
  
  if (rocktype != 4) {
    p->Kco2   = p->cmaxco2*PetscExpReal(p->Lrco2*(1.0/p->T - 1.0/p->Tmco2))/100.0;
  }
  
  fc_array[0] = fc_guess[0];
  fc_array[1] = fc_guess[1];

  ierr        = NewtonSolver(fc_array, f_array, A, p, 1e-12, 1e-24, 1e-12, 100);  CHKERRQ(ierr);
  
  if (fc_array[0]>flim&&fc_array[0]<1.0&&fc_array[1]>0.0&&fc_array[1]<1.0){

    *frac = fc_array[0]*rhos/(rhof + fc_array[0]*(rhos-rhof));
    *hf   = fc_array[1];
    *cf   = 1 - fc_array[1];
    *hs   = p->Kh2o*PetscExpReal(p->Wmh*(*cf)*(*cf)/(GAS_CONSTANT*p->T))*(*hf);
    *cs   = p->Kco2*PetscExpReal(p->Wmc*(*hf)*(*hf)/(GAS_CONSTANT*p->T))*(*cf);

    *validtype       = PETSC_TRUE;
  
  }else{

    fc_array[0] = 573;           // guessed initial devolatilization temperature, in K
    fc_array[1] = 0.9;           // guessed H2O concentration in the liquid phase at initial devolatilization

    ierr        = NewtonSolver_Sol(fc_array, f_array, A, p, 1e-12, 1e-24, 1e-12, 100);  CHKERRQ(ierr);

    *frac = user->par->phi0;
    *hf   = fc_array[1];
    *cf   = 1.0 - *hf;
    *hs   = w_h2o/(1 - flim);
    *cs   = w_co2/(1 - flim);

    *validtype       = PETSC_FALSE;
  }
  
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "DoOutput"
/* To make an output file */
PetscErrorCode DoOutput(AppCtx *user)
/* ------------------------------------------------------------------- */
{
  Parameter      *p = user->par;
  char*          filename = NULL;
  PetscViewer    viewer;
  PetscErrorCode ierr;
  PetscFunctionBegin;

  asprintf(&filename,"%s_%d",p->filename, p->frame);

  ierr  =  PetscPrintf(user->comm,"*****  Generating output file: \"%s\"\n\n\n",filename);  CHKERRQ(ierr);
  ierr  =  PetscViewerBinaryOpen(user->comm,filename,FILE_MODE_WRITE,&viewer);    CHKERRQ(ierr);
  ierr  =  PetscViewerPushFormat(viewer,PETSC_VIEWER_BINARY_MATLAB);              CHKERRQ(ierr);
  ierr  =  PetscBagView(user->bag,viewer);                                        CHKERRQ(ierr); // output bag
  ierr  =  VecView(user->VC,viewer);                                              CHKERRQ(ierr); // output solution fields
  ierr  =  VecView(user->VCo,viewer);                                             CHKERRQ(ierr); // output old solution fields
  ierr  =  VecView(user->RVC,viewer);                                             CHKERRQ(ierr); // output residual fields
  ierr  =  VecView(user->PHIC,viewer);                                            CHKERRQ(ierr); // output thermodynamic fields
  ierr  =  VecView(user->PHICo,viewer);                                           CHKERRQ(ierr); // output thermodynamic fields  
  ierr  =  VecView(user->PTX,viewer);                                             CHKERRQ(ierr); // output PTX fields
  ierr  =  PetscViewerDestroy(&viewer);                                           CHKERRQ(ierr);

  PetscFunctionReturn(0);
}


/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "TimeSteppingSolver"
PetscErrorCode TimeSteppingSolver(AppCtx *user)
/* ------------------------------------------------------------------- */
{
  Parameter      *p = user->par;
  PetscErrorCode ierr;
  PetscInt       step = 0;
  PetscReal      fnorm;
  
  PetscFunctionBegin;

  /* write out initial condition */
  ierr  =  DoOutput(user);                                              CHKERRQ(ierr);
  p->frame++;

  /* do time stepping until ns steps or time tmax */
  step  =  1;
  while (p->t <= p->tmax && step <= p->nt) {

    /* print time step information */
    ierr  =  PetscPrintf(user->comm,"*****  step: %i;  time = %e years;  dt = %e years\n\n",
			 step,p->t*p->tscale/YEARTOSEC,p->dt*p->tscale/YEARTOSEC);            CHKERRQ(ierr);

    /* copy solution from last time step into "old" vector */
    ierr  =  VecCopy(user->VC,user->VCo);                                 CHKERRQ(ierr);
    ierr  =  VecCopy(user->PHIC,user->PHICo);                             CHKERRQ(ierr);    
    
    /* solve (non-linear) system of equations for V,P */
    ierr  =  SNESSetTolerances(user->snes_vc,p->atol,p->rtol,1e-16,p->maxit,10000); CHKERRQ(ierr);
    ierr  =  SNESSolve(user->snes_vc,NULL,user->VC);                CHKERRQ(ierr);  

    ierr  =  SNESComputeFunction(user->snes_vc,user->VC,user->RVC);       CHKERRQ(ierr);
    ierr  =  VecNorm(user->RVC,NORM_2,&fnorm);                            CHKERRQ(ierr);
    ierr  =  PetscPrintf(user->comm,"---  VC norm = %e\n\n",fnorm);       CHKERRQ(ierr);
      
    /* update total time */
    p->t  =  p->t + p->dt;
    ierr  =  PetscPrintf(user->comm,"\n\n");                              CHKERRQ(ierr);

    /* update adaptive time step */
    p->dt =  AdaptiveTimestep(user);

    /* periodically write output file */
    if (step % p->nop == 0) {
      ierr  =  DoOutput(user);                                            CHKERRQ(ierr);
      p->frame++;
    }
    step++;
  }

  ierr = PetscPrintf(user->comm,"Computation stopped after step %d, at time %e\n",step-1,p->t);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "AdaptiveTimestep"
/* To make an output file */
PetscReal AdaptiveTimestep(AppCtx *user)
/* ------------------------------------------------------------------- */
{
  Parameter      *p = user->par;
  PetscReal       h = p->h;
  PetscReal       vmax, dt, dt_advn;
  Vec             vf;
  PetscErrorCode  ierr;

 /* adapt time step to max velocity */
  ierr     =  VecDuplicate(user->VC, &vf);                            CHKERRQ(ierr);  
  ierr     =  VecCopy(user->VC, vf);                                 CHKERRQ(ierr);
  ierr     =  VecAbs(vf);                                            CHKERRQ(ierr);
  ierr     =  VecStrideMax(vf,0,NULL,&vmax);                         CHKERRQ(ierr);
  dt_advn  =  0.25*h / PetscMax(p->ucos*vmax,p->wsin*vmax);
  ierr     =  VecDestroy(&vf);                                       CHKERRQ(ierr);

  dt       =  dt_advn;

  //ierr     =  PetscPrintf(user->comm,"diff. time step = %e; adv. time step = %e, adaptive time step = %e\n",
  //			  dt_difn*p->tscale/SECPERYR,dt_advn*p->tscale/SECPERYR,p->dt*p->tscale/SECPERYR);  CHKERRQ(ierr);

  return  dt;
}

#undef __FUNCT__
#define __FUNCT__ "ImportThermoData"
PetscErrorCode ImportThermoData(ThermoData *ctx)
{
  PetscReal *p;
  PetscErrorCode ierr;

  PetscFunctionBegin;  

  ierr  =  PetscMalloc1(1, &(ctx->sed));     CHKERRQ(ierr);
  ierr  =  PetscMalloc1(1, &(ctx->morb));    CHKERRQ(ierr);
  ierr  =  PetscMalloc1(1, &(ctx->gabbro));  CHKERRQ(ierr);
  ierr  =  PetscMalloc1(1, &(ctx->pum));     CHKERRQ(ierr);

  /*************  sed  ******************/
  ierr  =  PetscMalloc1(3, &(ctx->sed->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->sed->cmaxh;
  p[0]  =  -0.150662555544386;
  p[1]  =  0.301807008226067;
  p[2]  =  1.018673820928173;

  ierr  =  PetscMalloc1(6, &(ctx->sed->logLrh));    CHKERRQ(ierr);
  p     =  ctx->sed->logLrh;
  p[0]  =  -3.533135743197558;
  p[1]  =  19.068737477212224;
  p[2]  =  -37.302837204809386;
  p[3]  =  31.324429026695910;
  p[4]  =  -10.317259468993946;
  p[5]  =  8.688808175310820;

  ierr  =  PetscMalloc1(6, &(ctx->sed->Tmh));        CHKERRQ(ierr);
  p     =  ctx->sed->Tmh;  
  p[0]  =  1.296369387210849;
  p[1]  =  -24.330963411722223; 
  p[2]  =  1.697986731996718e+02;
  p[3]  =  -5.368530912624840e+02;
  p[4]  =  7.629895101455530e+02;
  p[5]  =  2.423527272231045e+02;

  ierr  =  PetscMalloc1(1, &(ctx->sed->cmaxc));      CHKERRQ(ierr); 
  p     =  ctx->sed->cmaxc;    
  *p    =  10.592282487499954;

  ierr  =  PetscMalloc1(6, &(ctx->sed->logLrc));     CHKERRQ(ierr);
  p     =  ctx->sed->logLrc;   
  p[0]  =  14.007787232073160;
  p[1]  =  -81.025362109897810;
  p[2]  =  1.746329337200755e+02;
  p[3]  =  -1.700791911486186e+02;
  p[4]  =  69.688713478378430;
  p[5]  =  1.378376757567256;
  
  ierr  =  PetscMalloc1(3, &(ctx->sed->Tmc));        CHKERRQ(ierr);
  p     =  ctx->sed->Tmc;
  p[0]  =  -19.534288871002960;
  p[1]  =  2.116075955389894e+02;
  p[2]  =  7.026318145363642e+02;

  ierr  =  PetscMalloc1(3, &(ctx->sed->deltaTc));   CHKERRQ(ierr);
  p     =  ctx->sed->deltaTc;
  p[0]  =  -17.780655913253780;
  p[1]  =  1.290578179906597e+02;
  p[2]  =  -10.089551673592354;
  
  ierr  =  PetscMalloc1(1, &(ctx->sed->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->sed->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->sed->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->sed->Wmc;    
  *p    =  -8000.0; 

  /*************  morb  ******************/
  ierr  =  PetscMalloc1(4, &(ctx->morb->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->morb->cmaxh;
  p[0]  =  0.010272471440474;
  p[1]  =  -0.115390189642000;
  p[2]  =  0.324451844950027;
  p[3]  =  1.415887869218784;

  ierr  =  PetscMalloc1(4, &(ctx->morb->logLrh));    CHKERRQ(ierr);
  p     =  ctx->morb->logLrh;
  p[0]  =  1.312630638962006;
  p[1]  =  -3.941703113796766;
  p[2]  =  2.750798457184458;
  p[3]  =  8.431772535722878;

  ierr  =  PetscMalloc1(6, &(ctx->morb->Tmh));        CHKERRQ(ierr);
  p     =  ctx->morb->Tmh;  
  p[0]  =  0.271046787787927;
  p[1]  =  -8.445036183735617; 
  p[2]  =  82.556558104728500;
  p[3]  =  -3.342487965612544e+02;
  p[4]  =  5.592352636804376e+02;
  p[5]  =  3.967635986627150e+02;

  ierr  =  PetscMalloc1(1, &(ctx->morb->cmaxc));      CHKERRQ(ierr); 
  p     =  ctx->morb->cmaxc;    
  *p    =  19.045565762499937;

  ierr  =  PetscMalloc1(6, &(ctx->morb->logLrc));     CHKERRQ(ierr);
  p     =  ctx->morb->logLrc;   
  p[0]  =  1.367443166281999;
  p[1]  =  -18.846082066295384;
  p[2]  =  62.960752263466270;
  p[3]  =  -80.779851142775770;
  p[4]  =  40.165085572683090;
  p[5]  =  4.354032367607715;
  
  ierr  =  PetscMalloc1(3, &(ctx->morb->Tmc));        CHKERRQ(ierr);
  p     =  ctx->morb->Tmc;
  p[0]  =  -17.658692732447540;
  p[1]  =  1.944885995727770e+02;
  p[2]  =  7.481072175613556e+02;

  ierr  =  PetscMalloc1(3, &(ctx->morb->deltaTc));   CHKERRQ(ierr);
  p     =  ctx->morb->deltaTc;
  p[0]  =  -17.780655913253780;
  p[1]  =  1.290578179906597e+02;
  p[2]  =  -10.089551673592354;
  
  ierr  =  PetscMalloc1(1, &(ctx->morb->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->morb->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->morb->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->morb->Wmc;    
  *p    =  -8000.0;  

  /*************  gabbro  ******************/
  ierr  =  PetscMalloc1(3, &(ctx->gabbro->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->gabbro->cmaxh;
  p[0]  =  -0.017667335340126;
  p[1]  =  0.089304408418261;
  p[2]  =  1.527316327453626;

  ierr  =  PetscMalloc1(5, &(ctx->gabbro->logLrh));    CHKERRQ(ierr);
  p     =  ctx->gabbro->logLrh;
  p[0]  =  -1.009961038518608;
  p[1]  =  5.217453113389753;
  p[2]  =  -8.951498304566513;
  p[3]  =  4.747716716081742;
  p[4]  =  8.516063062352552;

  ierr  =  PetscMalloc1(5, &(ctx->gabbro->Tmh));        CHKERRQ(ierr);
  p     =  ctx->gabbro->Tmh;  
  p[0]  =  -3.000118134386033;
  p[1]  =  45.352910295461910;
  p[2]  =  -2.388331789925089e+02;
  p[3]  =  5.170575818336469e+02;
  p[4]  =  3.756317222662897e+02;

  ierr  =  PetscMalloc1(1, &(ctx->gabbro->cmaxc));      CHKERRQ(ierr); 
  p     =  ctx->gabbro->cmaxc;    
  *p    =  19.379454537499885;

  ierr  =  PetscMalloc1(4, &(ctx->gabbro->logLrc));     CHKERRQ(ierr);
  p     =  ctx->gabbro->logLrc;   
  p[0]  =  2.739281957244409;
  p[1]  =  -9.851731701468713;
  p[2]  =  9.146439180475213;
  p[3]  =  9.055596526151467;
  
  ierr  =  PetscMalloc1(3, &(ctx->gabbro->Tmc));        CHKERRQ(ierr);
  p     =  ctx->gabbro->Tmc;
  p[0]  =  -14.165093677306569;
  p[1]  =  1.809036929023690e+02;
  p[2]  =  7.757132892363031e+02;

  ierr  =  PetscMalloc1(3, &(ctx->gabbro->deltaTc));   CHKERRQ(ierr);
  p     =  ctx->gabbro->deltaTc;
  p[0]  =  -11.794472521973619;
  p[1]  =  89.136690826367680;
  p[2]  =  1.711230429250124;
  
  ierr  =  PetscMalloc1(1, &(ctx->gabbro->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->gabbro->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->gabbro->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->gabbro->Wmc;    
  *p    =  -50000.0; 


   /*************  pum  ******************/
  ierr  =  PetscMalloc1(2, &(ctx->pum->cmaxh));     CHKERRQ(ierr);
  p     =  ctx->pum->cmaxh;
  p[0]  =  0.001156281237125;
  p[1]  =  2.421787983195434;

  ierr  =  PetscMalloc1(9, &(ctx->pum->logLrh));    CHKERRQ(ierr);
  p     =  ctx->pum->logLrh;
  p[0]  =  -19.060885857770412;
  p[1]  =  1.689826532012307e+02;
  p[2]  =  -6.300318077012928e+02;
  p[3]  =  1.281835967642164e+03;
  p[4]  =  -1.543141708645560e+03;
  p[5]  =  1.111879888618042e+03;
  p[6]  =  -4.591417538523027e+02;
  p[7]  =  95.414320683178630;
  p[8]  =  1.972456309737587;

  ierr  =  PetscMalloc1(3, &(ctx->pum->Tmh));        CHKERRQ(ierr);
  p     =  ctx->pum->Tmh;  
  p[0]  =  -15.113105096276987;
  p[1]  =  92.697643261252640;
  p[2]  =  6.389776673404846e+02;

  ierr  =  PetscMalloc1(1, &(ctx->pum->Wmh));     CHKERRQ(ierr); 
  p     =  ctx->pum->Wmh;    
  *p    =  0.0;

  ierr  =  PetscMalloc1(1, &(ctx->pum->Wmc));     CHKERRQ(ierr); 
  p     =  ctx->pum->Wmc;    
  *p    =  -50000.0;  

  PetscFunctionReturn(0);  
  
}

#undef __FUNCT__
#define __FUNCT__ "ClearThermoData"
PetscErrorCode ClearThermoData(ThermoData *ctx)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;  
  
  ierr = PetscFree(ctx->sed->cmaxh);         CHKERRQ(ierr);
  ierr = PetscFree(ctx->sed->logLrh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Tmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->cmaxc);         CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->logLrc);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Tmc);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->deltaTc);       CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Wmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->sed->Wmc);           CHKERRQ(ierr);  
  ierr = PetscFree(ctx->sed);                CHKERRQ(ierr); 
  
  ierr = PetscFree(ctx->morb->cmaxh);        CHKERRQ(ierr);
  ierr = PetscFree(ctx->morb->logLrh);       CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Tmh);          CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->cmaxc);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->logLrc);       CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Tmc);          CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->deltaTc);      CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Wmh);          CHKERRQ(ierr); 
  ierr = PetscFree(ctx->morb->Wmc);          CHKERRQ(ierr);  
  ierr = PetscFree(ctx->morb);               CHKERRQ(ierr); 

  ierr = PetscFree(ctx->gabbro->cmaxh);      CHKERRQ(ierr);
  ierr = PetscFree(ctx->gabbro->logLrh);     CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Tmh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->cmaxc);      CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->logLrc);     CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Tmc);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->deltaTc);    CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Wmh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->gabbro->Wmc);        CHKERRQ(ierr);  
  ierr = PetscFree(ctx->gabbro);             CHKERRQ(ierr); 

  ierr = PetscFree(ctx->pum->cmaxh);         CHKERRQ(ierr);
  ierr = PetscFree(ctx->pum->logLrh);        CHKERRQ(ierr); 
  ierr = PetscFree(ctx->pum->Tmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->pum->Wmh);           CHKERRQ(ierr); 
  ierr = PetscFree(ctx->pum->Wmc);           CHKERRQ(ierr);  
  ierr = PetscFree(ctx->pum);                CHKERRQ(ierr);  

  PetscFunctionReturn(0);
  
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormResidual"
PetscErrorCode FormResidual(PetscScalar *fc_array, PetscScalar *f_array, ThermoParameters *p)
/* ------------------------------------------------------------------- */
{
  PetscFunctionBegin;
  
  f_array[0] = p->c_blkh2o - fc_array[0]*fc_array[1] - (1.0 - fc_array[0])*fc_array[1]*p->Kh2o*PetscExpReal((p->Wmh*((1.0 - fc_array[1])*(1.0 - fc_array[1])))/(GAS_CONSTANT*p->T));
  f_array[1] = p->c_blkco2 - fc_array[0]*(1.0 - fc_array[1]) - (1.0 - fc_array[0])*(1.0 - fc_array[1])*p->Kco2*PetscExpReal((p->Wmc*(fc_array[1]*fc_array[1]))/(GAS_CONSTANT*p->T));

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormJacobian"
PetscErrorCode FormJacobian(PetscScalar *xx, PetscScalar *A, ThermoParameters *p)
/* ------------------------------------------------------------------- */
{
  PetscFunctionBegin;

  A[0] = -xx[1] + xx[1]*p->Kh2o*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[1] = -xx[0] - (1 - xx[0])*p->Kh2o*(1 - 2.0*xx[1]*(1 - xx[0])*p->Wmh/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*p->T));
  A[2] = -(1 - xx[1]) + (1 - xx[1])*p->Kco2*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));
  A[3] =  xx[0] - (1 - xx[0])*p->Kco2*(-1 + 2.0*xx[1]*(1 - xx[1])*p->Wmc/(GAS_CONSTANT*p->T))*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*p->T));

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "SolveJacobian"
PetscErrorCode SolveJacobian(PetscScalar *A, PetscScalar *f_array, PetscScalar *dx)
/* ------------------------------------------------------------------- */
{
  PetscScalar det, inv_det;
  PetscFunctionBegin;
  
  det     = A[0]*A[3] - A[1]*A[2];
  inv_det = 1.0/det;


  dx[0]   = inv_det * (A[3] * f_array[0] - A[1] * f_array[1]);
  dx[1]   = inv_det * (-A[2] * f_array[0] + A[0] * f_array[1]);
  
  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "NewtonSolver"
PetscErrorCode NewtonSolver(PetscScalar *fc_array, PetscScalar *f_array, PetscScalar *A, ThermoParameters *p, PetscReal atol, PetscReal rtol, PetscReal stol, PetscInt maxit)
/* ------------------------------------------------------------------- */
{
  PetscScalar     dx[2];
  PetscInt        its = 0;
  PetscReal       a0, a1, r, d;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  

  d    = 1.0;   // norm of solution change
  r    = 1.0;   // ratio of residual norm between current and last iterations

  ierr = FormResidual(fc_array, f_array, p);  
  a0   = sqrt(f_array[0] * f_array[0] + f_array[1] * f_array[1]);
//  printf("%d:  ||F|| % 1.12e \n", its, a0);
  a1 = a0;

  if (a1 < atol) {goto converged;} 
  
  while (a1 >= atol && r >= rtol && d >= stol && its <= maxit) {
    
    its++;

    ierr  = FormJacobian(fc_array, A, p);      CHKERRQ(ierr);
    ierr  = SolveJacobian(A, f_array, dx);     CHKERRQ(ierr);

    fc_array[0] = fc_array[0] - dx[0];
    fc_array[1] = fc_array[1] - dx[1];
    
    ierr = FormResidual(fc_array, f_array, p);
    a1   = sqrt(f_array[0] * f_array[0] + f_array[1] * f_array[1]);
    r    = a1/a0;
    d    = sqrt(dx[0] * dx[0] + dx[1] * dx[1]);

//    printf("%d:  ||F|| % 1.12e \n", its, a1);

  }

  converged:

//  printf("%d:  ||F|| % 1.12e \n", its, a1);  

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormResidual_Sol"
PetscErrorCode FormResidual_Sol(PetscScalar *fc_array, PetscScalar *f_array, ThermoParameters *p)
/* ------------------------------------------------------------------- */
{
  PetscFunctionBegin;
  
  f_array[0] = (p->cmaxh2o/100.0)*fc_array[1]*PetscExpReal(p->Lrh2o*(1.0/fc_array[0] - 1.0/p->Tmh2o))*PetscExpReal(p->Wmh*(1.0 - fc_array[1])*(1.0 - fc_array[1])/(GAS_CONSTANT*fc_array[0])) - p->c_blkh2o;
  f_array[1] = (p->cmaxco2/100.0)*(1.0 - fc_array[1])*PetscExpReal(p->Lrco2*(1.0/fc_array[0] - 1.0/p->Tmco2))*PetscExpReal(p->Wmc*fc_array[1]*fc_array[1]/(GAS_CONSTANT*fc_array[0])) - p->c_blkco2;

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "FormJacobian_Sol"
PetscErrorCode FormJacobian_Sol(PetscScalar *xx, PetscScalar *A, ThermoParameters *p)
/* ------------------------------------------------------------------- */
{
  PetscFunctionBegin;

  A[0] = (p->cmaxh2o/100.0)*xx[1]*PetscExpReal(p->Lrh2o*(1.0/xx[0] - 1.0/p->Tmh2o))*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*xx[0])) \
	 *(-p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*xx[0]*xx[0]) - p->Lrh2o/(xx[0]*xx[0]));
  A[1] = (p->cmaxh2o/100.0)*PetscExpReal(p->Lrh2o*(1.0/xx[0] - 1.0/p->Tmh2o))*PetscExpReal(p->Wmh*(1.0 - xx[1])*(1.0 - xx[1])/(GAS_CONSTANT*xx[0])) \
	 *(1.0 - 2*p->Wmh*xx[1]*(1.0 - xx[1])/(GAS_CONSTANT*xx[0]));
  A[2] = (p->cmaxco2/100.0)*(1.0 - xx[1])*PetscExpReal(p->Lrco2*(1.0/xx[0] - 1.0/p->Tmco2))*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*xx[0])) \
	 *(-p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*xx[0]*xx[0]) - p->Lrco2/(xx[0]*xx[0]));
  A[3] = (p->cmaxco2/100.0)*PetscExpReal(p->Lrco2*(1.0/xx[0] - 1.0/p->Tmco2))*PetscExpReal(p->Wmc*xx[1]*xx[1]/(GAS_CONSTANT*xx[0])) \
	 *(-1.0 + 2*p->Wmc*xx[1]*(1.0 - xx[1])/(GAS_CONSTANT*xx[0]));

  PetscFunctionReturn(0);

}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "NewtonSolver_Sol"
PetscErrorCode NewtonSolver_Sol(PetscScalar *fc_array, PetscScalar *f_array, PetscScalar *A, ThermoParameters *p, PetscReal atol, PetscReal rtol, PetscReal stol, PetscInt maxit)
/* ------------------------------------------------------------------- */
{
  PetscScalar     dx[2];
  PetscInt        its = 0;
  PetscReal       a0, a1, r, d;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  

  d    = 1.0;   // norm of solution change
  r    = 1.0;   // ratio of residual norm between current and last iterations

  ierr = FormResidual_Sol(fc_array, f_array, p);  
  a0   = sqrt(f_array[0] * f_array[0] + f_array[1] * f_array[1]);
//  printf("%d:  ||F|| % 1.12e \n", its, a0);
  a1 = a0;

  if (a1 < atol) {goto converged;} 
  
  while (a1 >= atol && r >= rtol && d >= stol && its <= maxit) {
    
    its++;

    ierr  = FormJacobian_Sol(fc_array, A, p);  CHKERRQ(ierr);
    ierr  = SolveJacobian(A, f_array, dx);     CHKERRQ(ierr);

    fc_array[0] = fc_array[0] - dx[0];
    fc_array[1] = fc_array[1] - dx[1];
    
    ierr = FormResidual_Sol(fc_array, f_array, p);
    a1   = sqrt(f_array[0] * f_array[0] + f_array[1] * f_array[1]);
    r    = a1/a0;
    d    = sqrt(dx[0] * dx[0] + dx[1] * dx[1]);

//    printf("%d:  ||F|| % 1.12e \n", its, a1);

  }

  converged:

//  printf("%d:  ||F|| % 1.12e \n", its, a1);  

  PetscFunctionReturn(0);

}

