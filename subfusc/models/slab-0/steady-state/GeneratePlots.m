
%*****  Routine for plotting PETSc results  *******************************

workdir      =  pwd;
addpath('/Applications/Utilities/petsc/petsc-3.7.4/share/petsc/matlab/')
addpath(pwd)

%*****  choose output data directory and filename to plot  ****************

datadir      =  '/Users/eart0381/Desktop/code/leasz/src/steady-state/';

%*****  select output fields (must correspond to PETSc DoOutput  **********

VCsol        =  {'v_f';'c_blk';'h_blk'};
PHICsol      =  {'phi';'c_f';'c_s';'h_f';'h_s'};
PTXsol       =  {'P';'T';'X'};

%*****  choose file printing options  *************************************

format       = '-dpdf';       %'-dpng','-dtiff','-dpdf'
resolution   = '-r200';       % in dpi

%*****  set up loop over frames to plot  **********************************

% ending       =  ['.' format(3:end)];
% secpyr       =  3600*24*365.25;
% load TKColorMap;
cd(datadir);

yr2s = 364.25*24*3600;
HA = {'HorizontalAlignment','left'};
VA = {'VerticalAlignment','bottom'};
UN = {'Units','normalized'};
TX = {'Interpreter','Latex'};
LW = {'LineWidth',2};
FS = {'FontSize',14};
AP = {'ActivePositionProperty', 'outerposition'};

vscale = 0.3;
di = 20;
dj = 8;

%***  read output data structures from PETSc files
frame     =  50;
currfile  =  ['steadystate_', num2str(frame)];
OP        =  ReadPETScResults(currfile,VCsol,PHICsol,PTXsol);


%***  extract current time for plotting

time  =  ['t = ',num2str(round(OP.PAR.t*OP.PAR.tscale/yr2s/1000)),' kyr'];

%***  create grid coordinates for plotting

dip       = OP.PAR.alpha;

[X,Z]     =  meshgrid(-1.5*OP.PAR.h:OP.PAR.h:(OP.PAR.nx-2)*OP.PAR.h,-1.5*OP.PAR.h:OP.PAR.h:(OP.PAR.nz-2)*OP.PAR.h);    

X_slab    =  X*cos(dip*pi/180);
Z_slab    =  X*sin(dip*pi/180) + Z*cos(dip*pi/180);

xlimit    =  [X(1,3), X(1,OP.PAR.nx-2)]*OP.PAR.lscale*1e-3;
zlimit    =  [Z(3,1), Z(OP.PAR.nz-2, 1)]*OP.PAR.lscale*1e-3;

X         =  X(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.lscale*1e-3;
Z         =  Z(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.lscale*1e-3; 

X_slab    =  X_slab(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.lscale*1e-3;
Z_slab    =  Z_slab(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.lscale*1e-3;     
%*** plot and print selected solution fields

f = figure(1);
clf

fw = 8.5;
fh = 11;
set(f, 'Units', 'Inches', 'Position', [0.1, 0.1, fw, fh]);
set(f, 'PaperPosition', [0.1, 0.1, fw, fh], 'PaperSize', [fw, fh]);
set(f, 'Color', 'w', 'InvertHardcopy', 'off', 'MenuBar', 'none');
set(f, 'Resize', 'off', 'Toolbar', 'none');

subplot(4,1,1)
pcolor(X,Z,flipud(OP.PHIC.phi(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
hold on;
% quiver(X(1:dj:OP.PAR.nz-4, 1:di:OP.PAR.nx-4),Z(1:dj:OP.PAR.nz-4, 1:di:OP.PAR.nx-4),flipud(OP.VC.v_f(3:dj:OP.PAR.nz-2, 3:di:OP.PAR.nx-2)*OP.PAR.ucos), -flipud(OP.VC.v_f(3:dj:OP.PAR.nz-2, 3:di:OP.PAR.nx-2)*OP.PAR.wsin), vscale);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('Porosity', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(a)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow
hold off;

subplot(4,1,2)
pcolor(X,Z,flipud(OP.VC.v_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*OP.PAR.vscale*yr2s); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('fluid velocity (m/yr)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(b)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

subplot(4,1,3)
pcolor(X,Z,flipud(OP.VC.c_blk(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*100 - 2.84); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('Bulk CO_2 change (wt%)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(c)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

subplot(4,1,4)
pcolor(X,Z,flipud(OP.VC.h_blk(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*100 - 2.58); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('Bulk H_2O change (wt%)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(d)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

print(f, 'figure1',format, resolution);

f=figure(2);
clf

fw = 8.5;
fh = 11;
set(f, 'Units', 'Inches', 'Position', [8.7, 0.1, fw, fh]);
set(f, 'PaperPosition', [0.1, 0.1, fw, fh], 'PaperSize', [fw, fh]);
set(f, 'Color', 'w', 'InvertHardcopy', 'off', 'MenuBar', 'none');
set(f, 'Resize', 'off', 'Toolbar', 'none');

subplot(4,1,1)
pcolor(X,Z,flipud(OP.PHIC.c_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*100); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('CO_2 in liquid phase (wt%)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(a)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar; caxis([0, 100]);
set(cax.Label, FS{:});
drawnow

subplot(4,1,2)
pcolor(X,Z,flipud(OP.PHIC.h_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*100); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('H_2O in liquid phase (wt%)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(b)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;  caxis([0, 100]);
set(cax.Label, FS{:});
drawnow

subplot(4,1,3)
pcolor(X,Z,flipud(OP.PHIC.c_s(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*100); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('CO_2 in solid phase (wt%)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(c)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

subplot(4,1,4)
pcolor(X,Z,flipud(OP.PHIC.h_s(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*100); shading interp;
axis ij; set(gca, FS{:}, AP{:});
% axis equal;
xlim(xlimit); ylim(zlimit);
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('H_2O in solid phase (wt%)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(d)', 'Position', [0.01, 0.85]);
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

print(f, 'figure2',format, resolution);

f=figure(3);
clf

fw = 8.5;
fh = 11;
set(f, 'Units', 'Inches', 'Position', [17.3, 0.1, fw, fh]);
set(f, 'PaperPosition', [0.1, 0.1, fw, fh], 'PaperSize', [fw, fh]);
set(f, 'Color', 'w', 'InvertHardcopy', 'off', 'MenuBar', 'none');
set(f, 'Resize', 'off', 'Toolbar', 'none');

subplot(4,1,1)
plot(X(1,:), OP.VC.v_f(OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.vscale*yr2s, LW{:});
xlim(xlimit);
% ylim([-10, zlimit+10]);
set(gca, FS{:}, AP{:});
xlabel('x(km)', FS{:}); ylabel('fluid velocity', FS{:});
title('Slab surface fluid velocity (m/yr)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(a)', 'Position', [0.01, 0.85]);
drawnow

subplot(4,1,2)
plot(X(1,:), OP.PHIC.phi(OP.PAR.nz-2, 3:OP.PAR.nx-2), LW{:});
xlim(xlimit);
% ylim([-10, zlimit+10]);
xlabel('x(km)', FS{:}); ylabel('\phi', FS{:});
set(gca, FS{:}, AP{:});
title('Slab surface porosity', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(b)', 'Position', [0.01, 0.85]);
drawnow

surfcflux = OP.PAR.rhof*OP.PAR.rhoscale*OP.PAR.vscale*yr2s*...
            OP.PHIC.c_f(OP.PAR.nz-2, 3:OP.PAR.nx-2).*...
            OP.PHIC.phi(OP.PAR.nz-2, 3:OP.PAR.nx-2).*...
            OP.VC.v_f(OP.PAR.nz-2, 3:OP.PAR.nx-2);
subplot(4,1,3)
plot(X(1,:), surfcflux, LW{:});
xlim(xlimit);
% ylim([-10, zlimit+10]);
xlabel('x(km)', FS{:}); ylabel('CO_2 flux', FS{:});
set(gca, FS{:}, AP{:});
title('Slab surface CO_2 flux (kg yr^-^1 m^-^2)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(c)', 'Position', [0.01, 0.85]);
drawnow

surfhflux = OP.PAR.rhof*OP.PAR.rhoscale*OP.PAR.vscale*yr2s*...
            OP.PHIC.h_f(OP.PAR.nz-2, 3:OP.PAR.nx-2).*...
            OP.PHIC.phi(OP.PAR.nz-2, 3:OP.PAR.nx-2).*...
            OP.VC.v_f(OP.PAR.nz-2, 3:OP.PAR.nx-2);
subplot(4,1,4)
plot(X(1,:), surfhflux, LW{:});
xlim(xlimit);
% ylim([-10, zlimit+10]);
xlabel('x(km)', FS{:}); ylabel('H_2O flux', FS{:});
set(gca, FS{:}, AP{:});
title('Slab surface H_2O flux (kg yr^-^1 m^-^2)', FS{:});
t = text(); set(t, UN{:},FS{:}, 'String', '(d)', 'Position', [0.01, 0.85]);
drawnow

print(f, 'figure3',format, resolution);

f=figure(4);
clf

fw = 4;
fh = 4;
set(f, 'Units', 'Inches', 'Position', [0.1, 12, fw, fh]);
set(f, 'PaperPosition', [0.1, 0.1, fw, fh], 'PaperSize', [fw, fh]);
set(f, 'Color', 'w', 'InvertHardcopy', 'off', 'MenuBar', 'none');
set(f, 'Resize', 'off', 'Toolbar', 'none');

pcolor(X_slab,Z_slab,flipud(OP.PTX.P(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
axis ij; set(gca, FS{:});
axis equal;
an = annotation('textarrow', [0.45, 0.65], [0.6, 0.6]);
set(an, LW{:}, 'Color', 'b');
t  = text(); set(t, 'String', 'case a', UN{:}, 'Position',[0.55, 0.55], FS{:});
an = annotation('textarrow', [0.45, 0.45], [0.6, 0.8]);
set(an, LW{:}, 'Color', 'b');
t  = text(); set(t, 'String', 'case b', UN{:}, 'Position',[0.22, 0.8], FS{:});
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('Pressure(GPa)', FS{:});
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

print(f, 'figure4', '-dtiff', '-r300');

f=figure(5);
clf

fw = 4;
fh = 4;
set(f, 'Units', 'Inches', 'Position', [4.2, 12, fw, fh]);
set(f, 'PaperPosition', [0.1, 0.1, fw, fh], 'PaperSize', [fw, fh]);
set(f, 'Color', 'w', 'InvertHardcopy', 'off', 'MenuBar', 'none');
set(f, 'Resize', 'off', 'Toolbar', 'none');

pcolor(X_slab,Z_slab,flipud(OP.PTX.T(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)) - 273); shading interp;
axis ij; set(gca, FS{:});
axis equal;
xlabel('x(km)', FS{:}); ylabel('Depth(km)', FS{:});
title('Temperature(^oC)', FS{:});
colormap jet; 
cax = colorbar;
set(cax.Label, FS{:});
drawnow

print(f, 'figure5', '-dtiff', '-r300');

cd(workdir)