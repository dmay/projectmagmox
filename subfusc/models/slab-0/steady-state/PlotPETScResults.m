
%*****  Routine for plotting PETSc results  *******************************

workdir      =  pwd;
addpath('/Applications/Utilities/petsc/petsc-3.7.4/share/petsc/matlab/')
addpath(pwd);

%*****  choose output data directory and filename to plot  ****************

datadir      =  './';
% filename     =  'steadystate_0';


%*****  select output fields (must correspond to PETSc DoOutput  **********

VCsol        =  {'v_f';'c_blk';'h_blk'};
PHICsol      =  {'phi';'c_f';'c_s';'h_f';'h_s'};
PTXsol       =  {'P';'T';'X'};

%*****  choose file printing options  *************************************

% format       = '-dpng';       %'-dpng','-dtiff'
% resolution   = '-r450';       % in dpi

%*****  set up loop over frames to plot  **********************************

% ending       =  ['.' format(3:end)];
% secpyr       =  3600*24*365.25;
% load TKColorMap;
cd(datadir);

yr2s = 364.25*24*3600;

OP_ini = ReadPETScResults('steadystate_0',VCsol,PHICsol,PTXsol);

 for frame =2000:2000
    
    %***  read output data structures from PETSc files
    
    currfile  =  ['steadystate_', num2str(frame)];
    OP        =  ReadPETScResults(currfile,VCsol,PHICsol,PTXsol);  
    
    %***  extract current time for plotting

%     time  =  ['t = ',num2str(round(OP.PAR.t*OP.PAR.tscale/secpyr/1000)),' kyr'];
    
    
    %***  create grid coordinates for plotting
    
    dip       = OP.PAR.alpha;
    rhof      = OP.PAR.rhof;
    rhos      = OP.PAR.rhos;
    vs        = OP.PAR.v_s;
    
    [X,Z]     =  meshgrid(-1.5*OP.PAR.h:OP.PAR.h:(OP.PAR.nx-2)*OP.PAR.h,-1.5*OP.PAR.h:OP.PAR.h:(OP.PAR.nz-2)*OP.PAR.h);    
     
    X         =  X*cos(dip*pi/180);
    Z         =  X*sin(dip*pi/180) + Z*cos(dip*pi/180);
    
    X         =  X(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.lscale*1e-3;
    Z         =  Z(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.lscale*1e-3 + 50;    
    %*** plot and print selected solution fields
    
    figure(1)
    clf
    pcolor(X,Z,flipud(OP.PHIC.phi(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
    axis ij;
    hold on;
%     quiver(X,Z,flipud(OP.VC.v_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.ucos), -flipud(OP.VC.v_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)*OP.PAR.wsin));
    xlabel('x(km)'); ylabel('depth(km)');
    title('Porosity');
    colormap jet; colorbar; caxis([0 0.075]);
    drawnow
    hold off;
    
    figure(2)
    clf
    pcolor(X,Z,flipud(OP.PHIC.phi(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)).*flipud(OP.VC.v_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*OP.PAR.vscale*yr2s); shading flat;
%     pcolor(X,Z,flipud(OP.VC.v_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))*OP.PAR.vscale*yr2s); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('fluid velocity');
    colormap(bluewhitered); colorbar;
    drawnow
    
    figure(3)
    clf
    pcolor(X,Z,flipud(OP.PHIC.c_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('CO_2 in liquid phase');
    colormap jet; colorbar; 
%     caxis([0, 1]);
    drawnow
    
    figure(4)
    clf
    pcolor(X,Z,flipud(OP.PHIC.h_f(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('H_2O in liquid phase');
    colormap jet; colorbar;  
%     caxis([0, 1]);
    drawnow
    
    figure(5)
    clf
    h = pcolor(X,Z,flipud(OP.VC.c_blk(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))-flipud(OP.VCo.c_blk(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
    h.EdgeColor = 'none';
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('Bulk CO_2 change in rock');
    colormap(bluewhitered(256)); colorbar;
    drawnow
    
    figure(6)
    clf
    h = pcolor(X,Z,flipud(OP.VC.h_blk(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))-flipud(OP.VCo.h_blk(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading interp;
    axis ij;
    h.EdgeColor = 'none';
    xlabel('x(km)'); ylabel('depth(km)');
    title('Bulk H_2O change in rock');
    colormap(bluewhitered(256)); colorbar;
    drawnow

% ----- plot co2 and h2o concentration changes in solid phase
%     figure(7)
%     clf
%     pcolor(X,Z,flipud(OP.PHIC.c_s(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)) - OP.PHIC.c_s(1:1)); shading interp;
%     axis ij;
%     xlabel('x(km)'); ylabel('depth(km)');
%     title('CO_2 change in solid phase');
%     colormap(jet); colorbar;
%     drawnow
%     
%     figure(8)
%     clf
%     pcolor(X,Z,flipud(OP.PHIC.h_s(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)) - OP.PHIC.h_s(1:1)); shading interp;
%     axis ij;
%     xlabel('x(km)'); ylabel('depth(km)');
%     title('H_2O change in solid phase');
%     colormap(jet); colorbar;
%     drawnow
    
% ----- plot flux divergence of chemical species   

    lfluxc = rhof*flipud(OP.PHIC.phi(2:OP.PAR.nz-2, 3:OP.PAR.nx-2)).*flipud(OP.VC.v_f(2:OP.PAR.nz-2, 3:OP.PAR.nx-2))...
            .*flipud(OP.PHIC.c_f(2:OP.PAR.nz-2, 3:OP.PAR.nx-2));   
    
    lfluxc = lfluxc(1:OP.PAR.nz-4,:) - lfluxc(2:OP.PAR.nz-3,:); 
    
    sfluxc = rhos*vs*(1 - flipud(OP.PHIC.phi(3:OP.PAR.nz-2, 2:OP.PAR.nx-2)))...
            .*flipud(OP.PHIC.c_s(3:OP.PAR.nz-2, 2:OP.PAR.nx-2));
    
    sfluxc = sfluxc(:,2:OP.PAR.nx-3) - sfluxc(:,1:OP.PAR.nx-4);   
    
    fluxc  = lfluxc + sfluxc;
            
    figure(7)
    clf
    pcolor(X,Z,sfluxc); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('CO_2 flux divergence');
    colormap(bluewhitered); colorbar;
    drawnow
    
    figure(9)
    pcolor(X,Z,fluxc); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('CO_2 flux divergence');
    colormap(bluewhitered); colorbar;
    drawnow
    
    lfluxh = rhof*flipud(OP.PHIC.phi(2:OP.PAR.nz-2, 3:OP.PAR.nx-2)).*flipud(OP.VC.v_f(2:OP.PAR.nz-2, 3:OP.PAR.nx-2))...
            .*flipud(OP.PHIC.h_f(2:OP.PAR.nz-2, 3:OP.PAR.nx-2));     
    
    lfluxh = lfluxh(1:OP.PAR.nz-4,:) - lfluxh(2:OP.PAR.nz-3,:);
    
    sfluxh = rhos*vs*(1 - flipud(OP.PHIC.phi(3:OP.PAR.nz-2, 2:OP.PAR.nx-2)))...
            .*flipud(OP.PHIC.h_s(3:OP.PAR.nz-2, 2:OP.PAR.nx-2));
    
    sfluxh = sfluxh(:,2:OP.PAR.nx-3) - sfluxh(:,1:OP.PAR.nx-4);
    
    fluxh  = lfluxh + sfluxh;
        
    figure(8)
    clf
    pcolor(X,Z,sfluxh); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('H_2O flux divergence');
    colormap(bluewhitered); colorbar;
    drawnow    
    
    figure(10)
    pcolor(X,Z,fluxh); shading interp;
    axis ij;
    xlabel('x(km)'); ylabel('depth(km)');
    title('H_2O flux divergence');
    colormap(bluewhitered); colorbar;
    drawnow     

% ------ plot pressure and temperature    
%     figure(7)
%     clf
%     pcolor(X,Z,flipud(OP.PTX.P(3:OP.PAR.nz-2, 3:OP.PAR.nx-2))); shading flat;
%     axis ij;
%     xlabel('x(km)'); ylabel('depth(km)');
%     title('Pressure(GPa)');
%     colormap jet; colorbar;
%     drawnow
%     
%     figure(8)
%     clf
%     pcolor(X,Z,flipud(OP.PTX.T(3:OP.PAR.nz-2, 3:OP.PAR.nx-2)) - 273); shading flat;
%     axis ij;
%     xlabel('x(km)'); ylabel('depth(km)');
%     title('Temperature(C)');
%     colormap jet; colorbar;
%     drawnow
    
    
    
%     if printframes
%         print(gcf,format,resolution,[filename '-tmp' num2str(frame) ending]);
%     end

 end

cd(workdir)



